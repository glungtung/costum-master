<?php if( 
	Authorisation::canEdit(Yii::app()->session["userId"] , $this->costum["contextId"], $this->costum["contextType"] ) ||
	Authorisation::canEditItem(Yii::app()->session["userId"] , $this->costum["contextId"], $this->costum["contextType"] ) ||
	Authorisation::canEdit(Yii::app()->session["userId"] , $this->costum["contextId"], ucfirst(Element::getControlerByCollection($this->costum["contextType"])) ) )
{ ?>
	<br>
	<?php if( $edit == "create" ){ ?>
	    <a href="javascript:;" data-idPath="<?= $id ?>" data-page="<?= @$page ?>" data-tag="<?php echo $tag ?>" class="createBlockBtn btn btn-xs btn-primary">Créer du contenu</a>
	    <a class="deleteThisBtn btn btn-xs btn-danger gova" data-idPath="<?= @$idToPath ?>" data-id="<?php echo (string)$id ?>" data-type="cms" href="javascript:;" class="btn btn-xs btn-primary"><i class="fa fa-trash"></i> Supprimer</a> 
	<?php } else if ( $edit == "update") { ?>
	    
	    <a href="javascript:;" data-idPath="<?= @$idToPath ?>"  data-id="<?php echo (string)$id ?>" data-type="cms" onclick=""  class="editThisBtn btn btn-xs btn-primary"><i class="fa fa-edit"></i> Modifier</a> 

	    <a class="deleteThisBtn btn btn-xs btn-danger gova" data-idPath="<?= @$idToPath ?>" data-id="<?php echo (string)$id ?>" data-type="cms" href="javascript:;" class="btn btn-xs btn-primary"><i class="fa fa-trash"></i> Supprimer</a> 
	<?php } 
?>

<script type="text/javascript">
	jQuery(document).ready(function() {
		mylog.log("render","costum.views.tpls.openFormBtn",'tag : <?php echo $tag ?>');
	});
</script>



<?php
} ?>

