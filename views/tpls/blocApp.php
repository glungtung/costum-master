<?php 
    $keyTpl = "app";

    $paramsData = [
        "search"        =>  false,
        "live"          =>  false,
        "annonces"      =>  false,
        "agenda"        =>  false,
        "dda"           =>  false,
        "map"           =>  false,
        "projects"      =>  false,
        "annuaire"      =>  false,
        "pages"       => [""=>""],
        "form"        => [] 
    ];
    $y = 0;
    
    foreach($paramsData as $i => $v) {
        if(isset($this->costum[$keyTpl]["#".$i])) 
        $paramsData[$i] =  (!empty($this->costum[$keyTpl]["#".$i])) ? true : false;      
    }

    if(isset($this->costum[$keyTpl])){
        foreach($this->costum[$keyTpl] as $k => $v){
            if(isset($this->costum[$keyTpl][$k]["staticPage"])){
                array_push($paramsData["pages"], array("key"=>$k, "icon"=>@$v["icon"], "name"=>$v["subdomainName"]));
            }
        }
    }
?>
<?php if($canEdit){ ?>
    <a class='edit<?php echo $keyTpl ?>Params' href='javascript:;' data-id='<?= $this->costum["contextId"]; ?>' data-collection='<?= $this->costum["contextType"]; ?>' data-key='<?php echo $keyTpl ?>' data-path='costum.<?= $keyTpl ?>'>
     App (Menu) <i class="fa fa-desktop" aria-hidden="true"></i>
</a><?php }?>


<script type="text/javascript">
var tplCtx={};
jQuery(document).ready(function() {
    sectionDyf.<?php echo $keyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    mylog.log("sectionParamsData----------------", sectionDyf.<?= $keyTpl ?>ParamsData);
    sectionDyf.<?php echo $keyTpl ?>Params = {
        "jsonSchema" : {    
            "title" : "Configuration des modules d'applications",
            "description" : "Choissisez quelle modules vous souhaitez activer et créer votre prorpre page statisque",
            "icon" : "fa-cog",
            "properties" : {
                "search" : {
                            "inputType" : "checkboxSimple",
                            "label" : "Activer la recherche",
                            "params" : {
                                "onText" : "Oui",
                                "offText" : "Non",
                                "onLabel" : "Oui",
                                // "inputTrue" : ".amendementDateEnddatetime"
                                "offLabel" : "Non",
                                "labelText" : "Activer la recherche"
                            },
                            "checked" : sectionDyf.<?php echo $keyTpl ?>ParamsData.search
                        },
                "live" : {
                        "inputType" : "checkboxSimple",
                        "label" : "Activer le live",
                        "params" : {
                            "onText" : "Oui",
                            "offText" : "Non",
                            "onLabel" : "Oui",
                            "offLabel" : "Non",
                            "labelText" : "Activer le live"
                        },
                        "checked" : sectionDyf.<?php echo $keyTpl ?>ParamsData.live
                    },
                "annonces" : {
                    "inputType" : "checkboxSimple",
                        "label" : "Activer les annonces",
                        "params" : {
                            "onText" : "Oui",
                            "offText" : "Non",
                            "onLabel" : "Oui",
                            "offLabel" : "Non",
                            "labelText" : "Activer les annonces"
                        },
                        "checked" :  sectionDyf.<?php echo $keyTpl ?>ParamsData.annonces
                },
                "agenda" : {
                    "inputType" : "checkboxSimple",
                        "label" : "Activer l'agenda",
                        "params" : {
                            "onText" : "Oui",
                            "offText" : "Non",
                            "onLabel" : "Oui",
                            "offLabel" : "Non",
                            "labelText" : "Activer l'agenda"
                        },
                        "checked" : sectionDyf.<?php echo $keyTpl ?>ParamsData.agenda
                },
                "dda" : {
                    "inputType" : "checkboxSimple",
                        "label" : "Activer les sondages",
                        "params" : {
                            "onText" : "Oui",
                            "offText" : "Non",
                            "onLabel" : "Oui",
                            "offLabel" : "Non",
                            "labelText" : "Activer les sondages"
                        },
                        "checked" : sectionDyf.<?php echo $keyTpl ?>ParamsData.dda
                },
                "map" : {
                    "inputType" : "checkboxSimple",
                        "label" : "Activer la map",
                        "params" : {
                            "onText" : "Oui",
                            "offText" : "Non",
                            "onLabel" : "Oui",
                            "offLabel" : "Non",
                            "labelText" : "Activer la map"
                        },
                        "checked" : sectionDyf.<?php echo $keyTpl ?>ParamsData.map
                },
                "projects" : {
                    "inputType" : "checkboxSimple",
                        "label" : "Activer les projects",
                        "params" : {
                            "onText" : "Oui",
                            "offText" : "Non",
                            "onLabel" : "Oui",
                            "offLabel" : "Non",
                            "labelText" : "Activer les projects"
                        },
                        "checked" : sectionDyf.<?php echo $keyTpl ?>ParamsData.projects
                },
                "annuaire" : {
                    "inputType" : "checkboxSimple",
                        "label" : "Activer les annuaires",
                        "params" : {
                            "onText" : "Oui",
                            "offText" : "Non",
                            "onLabel" : "Oui",
                            "offLabel" : "Non",
                            "labelText" : "Activer les annuaires"
                        },
                        "checked" : sectionDyf.<?php echo $keyTpl ?>ParamsData.annuaire
                },
                "pages" : {
                    "label" : "Page Statique(s)",
                    "inputType" : "lists",
                    "entries":{
                        "key":{
                            "type":"hidden",
                            "class":""
                        },
                        "icon":{
                            "label":"Icon",
                            "type":"select",
                            "class":"col-xs-3 no-padding"
                        },
                        "name":{
                            "label":"Nom de la page",
                            "type":"text",
                            "class":"col-xs-8"
                        }
                    },
                }/*,
                form : {
                    inputType : "array",
                    label : "Form Personnalisable",
                    values : sectionDyf.<?php echo $keyTpl ?>ParamsData.form
                }*/
            },
            save : function (data) { 
                mylog.log("before",tplCtx);
                // mylog.log(document.body.getElementsByTagName("input"));
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) { 
                    // mylog.log($("#"+k).val());
                    if($("#"+k).val() === "true"){
                        // mylog.log("qu'est ce que s'est trop bien la vie d'artiste");
                      //  var inMenu = $("#"+k).val() == "true" ? true : false;
                        if(k=="search"){
                            tplCtx.value["#"+k] = {
                                "results":{
                                    "smartGrid":true,
                                    "renderView":"directory.elementPanelHtml"
                                }
                            }
                        }
                        else if(k=="projects"){
                            tplCtx.value["#"+k] = {
                                "hash" : "#app.search",
                                "filterObj" : "costum.views.custom.costumize.filters",
                                "urlExtra" : "/page/projects",
                                "subdomainName" : "Projects",
                                "icon" : "lightbulb-o",
                                "img" : "",
                                "placeholderMainSearch" : "",
                                "useFooter" : "true",
                                "useFilter" : "true",
                                "filters" : {
                                    "types" : [ 
                                        "projects"
                                    ]
                                },
                                "searchObject" : {
                                    "indexStep" : "10"
                                }
                            }
                        }
                        else if(k=="annuaire"){
                            tplCtx.value["#"+k] = {
                                "hash" : "#app.search",
                                "filterObj" : "costum.views.custom.costumize.filters",
                                "urlExtra" : "/page/annuaire",
                                "subdomainName" : "Annuaire",
                                "icon" : "phone",
                                "img" : "",
                                "placeholderMainSearch" : "",
                                "useFooter" : "true",
                                "useFilter" : "true",
                                "filters" : {
                                    "types" : [ 
                                        "organizations"
                                    ]
                                },
                                "searchObject" : {
                                    "indexStep" : "10",
                                    "links" : [ 
                                        "memberOf"
                                    ]
                                }
                            }
                        }
                        else{
                            tplCtx.value["#"+k] = true ;
                        }
                    }
                    if(k=="pages"){ 
                        $.each(data.pages,function(inc,va){
                            nameKeyPage=(typeof va.key != "undefined" && notEmpty(va.key)) ? va.key : "#"+va.name.replace(/[^\w]/gi, '').toLowerCase();
                            tplCtx.value[nameKeyPage] = {
                                hash : "#app.view",
                                icon : va.icon,
                                urlExtra : "/page/"+nameKeyPage.replace("#","")+"/url/costum.views.tpls.staticPage",
                                useHeader : true,
                                isTemplate : true,
                                useFooter : true,
                                useFilter : false,
                                subdomainName : va.name,
                                staticPage : true
                            }
                        });
                    }
              
                        
                 });
                mylog.log("save tplCtx",tplCtx);
                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {

                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#ajax-modal").modal('hide');
                        toastr.success("App bien ajouté");
                        location.reload();
                    } );
                }

            }
        }
    };

    $(".edit<?php echo $keyTpl ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        createFontAwesomeSelect();
        dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?>Params,null, sectionDyf.<?php echo $keyTpl ?>ParamsData);
    });
});


function createFontAwesomeSelect(){
    dyFObj.init.buildEntryList = function(num, entry,field, value){
        mylog.log("buildEntryList num",num,"entry", entry,"field",field,"value", value);
        countEntry=Object.keys(entry).length;
        defaultClass= (countEntry==1) ? "col-xs-10" : "col-xs-5";
        
        str="<div class='listEntry col-xs-12 no-padding' data-num='"+incEntry+"'>";
            $.each(entry, function(e, v){
                name=field+e+num;
                classEntry=(typeof v.class != "undefined") ? v.class : defaultClass;
                placeholder=(typeof v.placeholder != "undefined") ? v.placeholder : ""; 
                str+="<div class='"+classEntry+"'>";
                if(typeof v.label != "undefined"){
                    str+='<div class="padding-5 col-xs-12">'+
                        '<h6>'+v.label.replace("{num}", (num+1))+'</h6>'+
                    '</div>'+
                    '<div class="space5"></div>';
                }
                valueEntry=(notNull(value) && typeof value[e] != "undefined") ? value[e] : "";
                if(v.type=="hidden")
                    str+='<input type="hidden" name="'+name+'" data-entry="'+e+'" id="'+field+num+'" class="addmultifield addmultifield0 form-control input-md no-padding" value="'+valueEntry+'" placeholder="'+placeholder+'"/>';
                else if(v.type=="text")
                    str+='<input type="text" name="'+name+'" data-entry="'+e+'" id="'+field+num+'" class="addmultifield addmultifield0 form-control input-md col-xs-12 no-padding" value="'+valueEntry+'" placeholder="'+placeholder+'"/>';
                else if(v.type=="textarea")
                    str+='<textarea name="'+name+'" data-entry="'+e+'" id="'+field+num+'" class="addmultifield form-control input-md col-xs-12 no-padding">'+valueEntry+'</textarea>';
                else if(v.type=="select"){
                    str+='<select type="text" name="'+name+'" data-entry="'+e+'" id="'+field+num+'" class="addmultifield addmultifield0 form-control input-md col-xs-12 no-padding" >';
                    //This is the select Options-----------------//
                    str+= createSelectOptions(valueEntry,fontAwesome);
                    str+='</select>';             
                }
                str+="</div>";
            });
            str+='<div class="col-sm-1">'+
                    '<button class="pull-right removeListLineBtn btn bg-red text-white tooltips pull-right" data-num="'+num+'" data-original-title="Retirer cette ligne" data-placement="bottom" style="margin-top: 43px;"><i class=" fa fa-times"></i></button>'+
                '</div>'+
            '</div>';
        return str;
    }; 
}

function createSelectOptions(current,Obj){
    var html = "";
    $.each(Obj,function(k,v){
        html+='<option value="'+k+'" '+((current == k) ? "selected" : "" )+' >'+v+'</option>';
    });
    return html;
}
</script>