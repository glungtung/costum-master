<?php 
    $me = isset(Yii::app()->session['userId']) ? Person::getById(Yii::app()->session['userId']) : null;
    $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;
    HtmlHelper::registerCssAndScriptsFiles([
      "/js/blockcms/fontAwesome/fontAwesome.js"
    ], $assetsUrl);

    $thisContextId = isset($this->costum["contextId"]) ? $this->costum["contextId"] : (string)$el["_id"];
    $thisContextType = isset($this->costum["contextType"]) ? $this->costum["contextType"] : $el["collection"];
    $thisContextSlug = isset($thisContextSlug) ? $thisContextSlug : $el["slug"];
    $el             = Element::getByTypeAndId($thisContextType, $thisContextId );
    $thisContextName = $el["name"];

    $tplUsingId     = "";
    $tplInitImage   = "";
    $nbrTplUser     = 0; 
    $nbrTplViewer   = 0;
    $paramsData     = [];
    $insideTplInUse = array();
    $cmsList        = array();
    $newCmsId       = array();
    $cmsInUseId     = array();

    $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;

?>
 <style>
   <?php if(!Authorisation::isUserSuperAdmin(Yii::app()->session["userId"])){ ?>
      .socialEntityBtnActions .editDescBlock, .socialEntityBtnActions .deleteCms{
        display: none;
      }
   <?php } ?>
 </style>

<script type="text/javascript">
    var isInterfaceAdmin = false;
    var thisContextId = notNull(costum) ? costum.contextId : contextId;
    var thisContextType = notNull(costum) ? costum.contextType : contextType ;
    var thisContextSlug = notNull(costum) ? costum.contextSlug : slug ;
    var thisContextName ="<?= $thisContextName ?>";
    var costumAssetsPath = "<?php echo $assetsUrl ?>";
    var itemClassArray = [];
    var sectionDyf = {};
    var tplCtx = {};
    var isInterfaceAdmin = false;
    var page = "<?= $page ?>"
  <?php if(Authorisation::isInterfaceAdmin()){  ?>
    isInterfaceAdmin = true;
  <?php } ?>
    var sectionDyf = {};
    var tplCtx = {};
    var DFdata = {
      'tpl'  : '<?php echo $tpl ?>',
      "id"   : "<?php echo $thisContextId ?>"
    };
    if(notNull(costum)){
      costum.col = "<?php echo $thisContextType ?>";
      costum.ctrl = "<?php echo Element::getControlerByCollection($thisContextType) ?>";
    }
   

    var configDynForm = <?php echo json_encode((isset($this->costum['dynForm'])) ? $this->costum['dynForm']:null); ?>;
    var tplsList = <?php echo json_encode((isset($this->costum['tpls'])) ? $this->costum['tpls']:null); ?>;
    var tplAuthorisation = <?php echo json_encode((isset($me["roles"]["superAdmin"])) ? $me["roles"]["superAdmin"]:false); ?>;
</script>

<?php
// Get page name to specify blocks-----------------------------------------
$page = ( isset($page) && !empty($page) ) ? $page : "welcome";

// Get all template except back-up-----------------------------------------
$listTemplate = Cms::getCmsByWhere(array(
        "type" => "template",
        "tplsUser.".$thisContextId.".".$page => array('$ne' => "backup"))
);

// Get tpl in use----------------------------------------------------------
$using = Cms::getCmsByWhere(array("tplsUser.".$thisContextId.".".$page => "using"));

// Get cms haven't tpl parent----------------------------------------------
$newCms = Cms::getCmsByWhere(array(
    "page"=>$page,
    "haveTpl" => "false",
    "parent.".$thisContextId => array('$exists'=>1)),
     array("position" => 1)
   );

// Get template in use-----------------------------------------------------
if (!empty($using)) {
  $tplUsingId = isset(array_keys($using)['0']) ? array_keys($using)['0'] : "";
  $insideTplInUse = $using[$tplUsingId]; 
  $tplInitImage = Document::getListDocumentsWhere(array("id"=> $tplUsingId, "type"=>'cms'), "file");
// Get cms liste in use----------------------------------------------------
  $cmsList = Cms::getCmsByWhere(array(
      "tplParent" => $tplUsingId,
      "parent.".$thisContextId => array('$exists'=>1),
      "page" => $page),
      array("position" => 1)
  );
}
$tplCreator   = isset($insideTplInUse["parent"]) ? $insideTplInUse["parent"] : [];
$tplCreatorId = isset(array_keys($tplCreator)["0"]) ? array_keys($tplCreator)["0"] : "";
$tplCms       = isset($insideTplInUse["cmsList"]) ? $insideTplInUse["cmsList"] : [];

// Get template's page original needed for update button condition-----------------------------
$tplPage = isset($insideTplInUse["page"]) ? $insideTplInUse["page"] : "";

// Get each Id of cms in use------------------------------------------------------------------
foreach (array_filter($cmsList) as $key => $value) {
 $cmsInUseId[] = (string)$value['_id'];
}  
// Get each cms haven't template (newCms)-----------------------------------------------------
foreach ($newCms as $key => $value) {
  $cmsList[] = $value;
  $newCmsId[] = (string)$value["_id"];
}

// Merge cms id inside template and new cms---------------------------------------------------
  $idCmsMerged = array_merge($cmsInUseId,$newCmsId);

// Update button Condition--------------------------------------------------------------------
  $tplEdited = false;
  if ($newCmsId != []) {
   $tplEdited = true;
  }

  $btnSave = false;
  $btnSaveChange = false;
  if ($tplUsingId != "") {
    $btnSave = ($idCmsMerged != $tplCms && $idCmsMerged != []);
    $btnSaveChange = ($tplCreatorId == $thisContextId && $idCmsMerged != $tplCms && $tplPage == $page);
  }elseif ($newCmsId != null){
    $btnSave = true;
  }
?>

  <?php
   if(Authorisation::isInterfaceAdmin()){  ?>
<div class="saveTemplate whole-page">
   <div id="saveTemplate">
      <nav class="tpl-engine-btn-list">
         <ul>
            <li class="a">
               <a href="javascript:;" id="choix" class="tryOtherTpl"> Choose a template 
               <i class="fa fa-list"></i>
               </a>
            </li>
            <?php if ($btnSave == true ) { ?>               
            <li class="b">
               <a href="javascript:;" class="saveThisTpls" onclick="tplObj.saveThisTpls()"> Save as new template 
               <i class="fa fa-save"></i>
               </a>
            </li>
            <?php } ?>
            <?php if ($btnSaveChange == true && $idCmsMerged != "") { ?> 
            <li class="c">
               <a href="javascript:;" class="saveChangeTpl">Save change
               <i class="fa fa-refresh"></i>
               </a>
            </li>
            <?php } ?>
            <li class="d" onclick="tplObj.previewTpl()">
               <a class="" href="javascript:;"> Preview
               <i class="fa fa-eye"></i>
               </a>
            </li>
            <li class="e">
               <a id="go-to-communecter" href="" data-hash="#page.type.<?= $this->costum["contextType"] ?>.id.<?= $this->costum["contextId"] ?>" target="_blank"> Go to communecter
               <i class="fa fa-id-card-o" aria-hidden="true"></i> 
               </a>
            </li>
            <li class="g"><?php echo $this->renderPartial("costum.views.tpls.jsonEditor.app.app2", array("canEdit" => $canEdit)); ?></li>
            <li class="g gCoForm"><?php echo $this->renderPartial("costum.views.tpls.blocCoForm", array("canEdit" => $canEdit)); ?></li>            
            <li class="i">
              <a class="" data-toggle="modal" data-target="#menuJson" href="javascript:;">
                Advanced settings<i class="fa fa-wrench"> </i>
              </a>
            </li>
            <?php //} ?>
            <!-- <li class="j">
                <a class="openFormDocumentation" data-toggle="modal" data-target="" href="javascript:;">
                  Documentation<i class="fa fa-info"> </i>
                </a>
            </li> -->

         </ul>
      </nav>
   </div>
</div>
<?php } ?>

<div class="row" id="all-block-container" style="margin-right: 0;margin-left:0">
    <?php
    $param=[
      "page"       => $page,
      "canEdit"    => true,
      "cmsList"    => $cmsList,
      "me"         => $me,
      "paramsData" => $paramsData,
      "el"         => $el
    ]; 
    echo $this->renderPartial("costum.views.tpls.blockCms.cmsEngine",$param); 
    ?>
</div>

<?php 
  $categoryUnique = array();
  $sumCat = 0;
  foreach ($listTemplate as $key => $value) { 
    $categoryUnique[] = isset($value["category"]) ? $value["category"]: "Autre" ;
    $sumCat += count($key);
  }
  $numberCat = array_count_values($categoryUnique);
?>

<!-- menu json -->
<?php if(Authorisation::isInterfaceAdmin() && isset($this->costum["contextId"]) && !empty($this->costum["contextId"])){  
  echo $this->renderPartial("costum.views.tpls.jsonEditor.menuJson",array("canEdit" => $canEdit)); 
}
?>
<!-- Documentation -->
<?php // echo $this->renderPartial("costum.views.tpls.blockDocumentation",array("canEdit" => $canEdit)); ?>

<?php 
    $descriptionTpl = isset($insideTplInUse["description"]) ? $insideTplInUse["description"] : "";
    $nameTpl = isset($insideTplInUse["name"]) ? $insideTplInUse["name"] : "";
    $categoryTpl = isset($insideTplInUse["category"]) ? $insideTplInUse["category"] : "";
?>

<script type="text/javascript"> 
  tplUsingId = "<?php echo $tplUsingId; ?>";
   var tplMenu = `
  <?php if(Authorisation::isInterfaceAdmin()){  ?>
  <div id="listeTemplate">
    <div id="tplFiltered"></div>
     <div class="row">
      <div class="col-lg-12" style="border-bottom: 1px solid rgba(100,100,100,0.1);text-align: center;">
         <div class="col-xs-12 padding-20" style="z-index: 1">
            <div style="width: 100%">
              <h3 class="bg-azure" style="padding: 20px; width: 50%; border-radius: 50px; margin : auto; ">Choisir une template</h3>
            </div>
         </div>
      </div>
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="text-left">
              <div class="btn-group">
                <button type="button" class="btn btn-default btn-filter" data-target="all">Tout<span class="filter-badge"><?= $sumCat ?></span></button>
                <?php  foreach (array_unique($categoryUnique) as $tpl_filtered) { ?>
                <button type="button" class="btn btn-default btn-filter" data-target="<?= preg_replace("/[^a-zA-Z]+/", "",$tpl_filtered) ?>"><?= $tpl_filtered ?>
                <span class="filter-badge"><?= $numberCat[$tpl_filtered] ?></span>
              </button>
               <?php } ?>               
                <button type="button" class="btn btn-default btn-filter btnRctlyTpl" data-target="backup" onclick="tplObj.backup()">Ancien template</button>
              </div>
            </div>
            <div class="">
              <div class="table table-filter">
                <div class="text-left">
                  <?php foreach ($listTemplate as $kTpl => $value) { 
                    $valCategory = isset($value["category"]) ? $value["category"]: "Autre" ;
                    $listCms = isset($value["cmsList"])?$value["cmsList"]:"";
                    $img = isset($value["img"])?$value["img"]:"";                     
                    $description = isset($value['description'])?$value['description']:'';
                    $name = isset($value['name'])? $value['name']:'';
                    $type = isset($value["type"])? $value["type"]:"";
                    $profil = isset($value["profilImageUrl"]) ?$value["profilImageUrl"]:"";
                    $currentTplsUser = isset($value["tplsUser"]) ?$value["tplsUser"]:[];
                    $counts = array();
                    foreach ($currentTplsUser as $key=>$subarr) {
                      $counts[] = $subarr;
                    }          
                    $countUsing = 0;
                    $countBackup = 0;

                    forEach($counts as $k => $v){
                      $arrayVal = array_values($v);
                      forEach($arrayVal as $k => $v){
                        if($v =="using") $countUsing ++;
                        if($v =="backup") $countBackup ++;
                      }

                    }
                    $tplKeys = $kTpl;
                    $initImage = Document::getListDocumentsWhere(array("id"=> $tplKeys, "type"=>'cms'), "file");
                    $arrayImg = [];
                    foreach ($initImage as $k => $v) {
                      $arrayImg[]= $v["docPath"];
                    }

                     ?>
                  <div class="tplItems" data-status="<?= preg_replace("/[^a-zA-Z]+/", "", $valCategory) ?>">
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 smartgrid-slide-element classifieds">          
                    <?php if($tplUsingId == $value["_id"]){ ?>      
                       <div class="item-slide" style="box-shadow: 0px 0px 2px 3px #5dd55d, 0 2px 10px 0 rgb(63, 78, 88)!important;">
                    <?php }elseif($listCms == ""){?>  
                       <div class="item-slide" style="box-shadow: 0px 0px 2px 3px rgb(198 43 43), 0 2px 10px 0 rgb(63, 78, 88)!important;">
                    <?php }else{ ?>                     
                       <div class="item-slide">
                    <?php } ?> 
                          <div class="entityCenter">
                             <a href="javascript:;" class="pull-right  lbh-preview-element"><i class="fa fa-laptop bg-azure"></i>
                             </a>
                          </div>
                          <div class="img-back-card">
                             <div class="div-img">
                                <img src="<?= $profil?>">
                             </div>
                             <div class="text-wrap searchEntity">
                                <div class="entityRight profil no-padding">
                                   <a href="#" class="entityName letter-orange">
                                     <font >
                                     <?= $name ?>                  
                                     </font>
                                   </a>    
                                </div>
                             </div>
                          </div>
                          <!-- hover -->
                          <div class="slide-hover co-scroll">
                             <div class="text-wrap">
                                   <div class="entityPrice">
                                         <font class="letter-orange" ><?= $name ?></font>
                                   </div>
                                   <div class="entityType col-xs-12 no-padding">
                                      <p class="p-short">
                                         <font >
                                         <?= $description ?>                    
                                         </font>
                                      </p>
                                   </div>
                                   <hr>
                                   <ul class="tag-list">
                                      <span class="badge bg-turq btn-tag tag padding-5" data-tag-value="appareilphoto" data-tag-label="appareilphoto">
                                      <font >
                                      <i class="fa fa-user"></i> <?= $countUsing ?> Utilisateurs
                                      </font>
                                      </span>
                                      <span class="badge bg-turq btn-tag tag padding-5" data-tag-value="appareilphoto" data-tag-label="appareilphoto">
                                      <font >
                                          <i class="fa fa-eye"></i> <?= $countUsing+$countBackup ?> Vu
                                      </font>
                                      </span>
                                   </ul>
                                <hr>
                                <div class="desc">
                                   <div class="socialEntityBtnActions btn-link-content">                                   
                                      <a href="<?= $profil ?>" class="btn btn-info btn-link thumb-info" data-title="Aperçu de <?= $name ?>" data-lightbox="all"><font style="vertical-align: inherit;"><i class="fa fa-eye"></i>Aperçu</font>
                                      </a>
                                       <?php
                                        if ($me['_id'] == $value['creator'] || @$me["roles"]["superAdmin"]){ ?>
                                        <a href="javascript:;" class="btn btn-info btn-link editDesc" data-id="<?= $value["_id"]?>" data-category="<?= @$value["category"]?>" data-name='<?= $name ?>' data-desc='<?php echo $description ?>' data-img='<?= json_encode($initImage) ?>'>
                                          <font > Modifier</font>
                                        </a> 
                                      <?php } ?>   
                                      <?php if($tplUsingId == $value["_id"]){ ?>      
                                        <p class="text-green bold">Votre template actuel</p>
                                      <?php }elseif($listCms == ""){?>  
                                        <p class="text-red bold">Non disponible</p>
                                      <?php }else{ ?>                             
                                      <a href="javascript:;" class="btn btn-info btn-link" onclick='tplObj.chooseConfirm(<?php echo json_encode($listCms)?>,"<?php echo $value["_id"] ?>");'>
                                        <font >Choisir</font>
                                      </a> 
                                      <?php } ?>    
                                   </div>
                                </div>
                             </div>
                          </div>
                       </div>
                    </div>
                  </div>
                  <?php }?>              
                </div>
              </div>
              <div id="backupTplsContainer"></div> 
            </div>
          </div>
        </div>
     </div>
  </div> 
  <?php } ?>`;

var tplObj = {
    backup : function (){
      $('#backupTplsContainer').fadeIn();
      params = {
        "contextId" : DFdata.id,
        "page" : page
      };
      $.ajax({
        type : 'POST',
        data : params,
        url : baseUrl+"/costum/blockcms/gettemplatebycategory",
        dataType : "json",
        async : false,
        success : function(data){
          var backupStr = ""
          if (data != "") {
             $.each(data, function(keyBackup,valueBackup) {
           backupStr +=`
           <div class="tplItems" data-status="backup">
              <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 smartgrid-slide-element classifieds">  
                <div class="item-slide">
                  <div class="entityCenter">
                    <a href="javascript:;" class="pull-right  lbh-preview-element"><i class="fa fa-laptop bg-azure"></i>
                    </a>
                  </div>
                  <div class="img-back-card">
                    <div class="div-img">
                      <img src="`+valueBackup.profilImageUrl+`">
                    </div>
                    <div class="text-wrap searchEntity">
                      <div class="entityRight profil no-padding">
                        <a href="#" class="entityName letter-orange">
                          <font >`+valueBackup.name+`</font>
                        </a>    
                      </div>
                    </div>
                  </div>
                  <div class="slide-hover co-scroll">
                    <div class="text-wrap">
                      <div class="entityPrice">
                        <font class="letter-orange" >`+valueBackup.name+`</font>
                      </div>
                      <div class="entityType col-xs-12 no-padding">
                        <p class="p-short">
                          <font >`+valueBackup.description+`</font>
                        </p>
                      </div>
                      <hr>
                      <ul class="tag-list">
                        <span class="badge btn-tag tag padding-5">
                          <font >                       
                          </font>
                        </span>
                      </ul>
                      <hr>
                      <div class="desc">
                        <div class="socialEntityBtnActions btn-link-content">                                   
                          <a href="`+valueBackup.profilImageUrl+`" class="btn btn-info btn-link thumb-info" data-title="Aperçu de Creercostum2" data-lightbox="all"><font style="vertical-align: inherit;"><i class="fa fa-eye"></i>Aperçu</font>
                          </a>
                          <a href="javascript:;" class="btn btn-info btn-link" onclick="tplObj.switchTemplate('`+keyBackup+`')">
                            <font > Restaurer</font>
                          </a> 
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
           `;
         }); 
          }else{
            backupStr = `<div class='tplItems' data-status='backup'>Vous n'avez pas aucun template récent sur cette page!</div>`
          }          
            $('#backupTplsContainer').html(backupStr);     
          }
      });

    },

    //Save template user-------------------------------------------
    saveTplUser : function (idTpl, stat) {
       var tplCtx = {};
       tplCtx.id = idTpl;
       tplCtx.path = "tplsUser."+thisContextId+".<?= $page?>";
       tplCtx.collection = "cms"; 
       tplCtx.value = stat;
       dataHelper.path2Value( tplCtx, function(params) {
        //toastr.success("Terminé!");
      }); 
     },

    //Chose template-----------------------------------------------
    chooseConfirm : function(ide,tplSelected){
    if (<?php echo json_encode($tplEdited); ?>) {
      var adjF = "une"
      var t = "."
      if (<?php echo json_encode($newCmsId); ?>.length > 1) {
        adjF = "des";
        t = "s.";
      }
      bootbox.dialog({
        message: `<p class="">Il semble que vous ayez ajouté `+adjF+` section`+t+`<br>Veuillez enregistrer ou fussionner votre template.</p> 
        <div class="alert alert-danger" role="alert">
          <p><b><i class="fa fa-exclamation-triangle"></i> NB</b>: Assurez-vous que si vous écrasez, votre section sera perdu!</p> 
        </div>`,
        title: "Selection de template",
        onEscape: function() {},
        show: true,
        backdrop: true,
        closeButton: true,
        animate: true,
        className: "my-modal",
        buttons: {
          success: {   
            label: "Enregistrer",
            className: "btn-success",
            callback: function() {
              tplObj.saveThisTpls();
            }
          },
          "Fusionner": {
            className: "btn-primary",
            callback: function() {
              tplObj.chooseThis(ide,tplSelected);
            }
          },
          "Ecraser": {
            className: "btn-danger",
            callback: function() {
             bootbox.confirm(`<div role="alert">
          <p><b>Supprimer pour toute l'éternité?</b><br><br>
          Vous perdrez toutes les sections que vous avez ajouté. Nous ne pouvons pas les récupérer une fois que vous les supprimez.<br><br>
          Êtes-vous sûr de vouloir supprimer définitivement ce(s) section(s)??
          </p> 
        </div> `,function(result){
              if (!result) {
                return;
              }else{            
                tplObj.deleteCms(<?php echo json_encode($newCmsId); ?>);            
                tplObj.chooseThis(ide,tplSelected);
              }
              urlCtrl.loadByHash(location.hash);
              }); 
            }
          },
          "Annuler": function() {}
        }

      }
      );
      }else{    
          tplObj.chooseThis(ide,tplSelected);
      } 
    },

    chooseThis : function(ide,tplSelected){
          var tplUsedData = Object.keys("");
          //Check if this tpl already used before 
          if($.inArray(tplSelected,tplUsedData) != -1){       
            tplObj.switchTemplate(tplSelected);
          }else{
            tplObj.duplicateCms(ide,tplSelected);
                  //Check if page never uses a template
                  if(tplUsingId == ""){
                    tplObj.saveTplUser(tplSelected,"using");
                  }else{
                    tplObj.switchTemplate(tplSelected);
                  }
                  
                  urlCtrl.loadByHash(location.hash);
                  
                }
      },

    //Switch template used------------------------------------------
    switchTemplate : function (tplSelected){ 
      tplObj.saveTplUser(tplSelected,"using");
      tplObj.saveTplUser(tplUsingId,"backup");
      urlCtrl.loadByHash(location.hash);
    },

    //Delete cms----------------------------------------------------
    deleteCms : function (ide){
      var params = {
        "ide" : <?php echo json_encode($idCmsMerged) ?>,
        "action" : "delete" 
      }
      $.ajax({
        type : 'POST',
        data : params,
        url : baseUrl+"/costum/blockcms/getcmsaction",
        dataType : "json",
        async : false,

        success : function(data){
          toastr.success("Element effacé!");
        }
      });
    },

    upCmsList : function (idTpl){  
      var params = {
        "tplId" : idTpl,
        "ide" : <?php echo json_encode($idCmsMerged) ?>,
        "action" : "upCmsList" 
      }
      $.ajax({
        type : 'POST',
        data : params,
        url : baseUrl+"/costum/blockcms/getcmsaction",
        dataType : "json",
        async : false,

        success : function(data){
          toastr.success("Terminé!!");
        }
      });
    },

    // Remove cmsList inside tpl if cms is empty--------------------
    removeCmsList : function (idTpl){  
      var params = {
        "tplId" : idTpl,
        "action" : "tplEmpty" 
      }
      $.ajax({
        type : 'POST',
        data : params,
        url : baseUrl+"/costum/blockcms/getcmsaction",
        dataType : "json",
        async : false,

        success : function(data){
          toastr.success("Terminé!!");
        }
      });
    },

    // Remove cms' stat---------------------------------------------
    removeCmsStat : function (idTpl,elementId){  
      var params = {
        "tplId" : idTpl,
        "ide" : elementId,
        "action" : "removestat" 
      }
      $.ajax({
        type : 'POST',
        data : params,
        url : baseUrl+"/costum/blockcms/getcmsaction",
        dataType : "json",
        async : false,

        success : function(data){
          toastr.success("Terminé!!");
        }
      });
    },

    //Duplicate cms-------------------------------------------------
    duplicateCms : function (ide, tplSelected){  
      var params = {
        "ide" : ide,
        "tplParent"  : tplSelected,
        "page"       : "<?= $page?>",
        "parentId"   : thisContextId,
        "parentSlug" : thisContextSlug,
        "parentType" : thisContextType,
        "action" : "duplicate" 
      }
      $.ajax({
        type : 'POST',
        data : params,
        url : baseUrl+"/costum/blockcms/getcmsaction",
        dataType : "json",
        async : false,

        success : function(data){
          toastr.success("Terminé!!");
        }
      });
    },

    saveThisTpls : function(){
      if (<?php echo json_encode($idCmsMerged) ?> == "") {
          alert("Vous ne pouvez pas enregistrer un template vide! Veuillez ajouter au moins une block")
        }else{
          var tplCtx = {};
          var activeForm = {
            "jsonSchema" : {
              "title" : "Enregistrement du template",
              "type" : "object",
              "properties" : {
                page : {
                  inputType : "hidden",
                  value : "<?= $page?>"
                },
                name : {
                  label: "Nom du template",
                  inputType : "text",
                  value : "<?php echo ucfirst($tpl) ?>"
                },
                type : { 
                  "inputType" : "hidden",
                  value : "template" 
                },
                category : {
                  label : "Catégorie",
                  inputType : "select",
                  rules:{
                    "required":true
                  },
                  options : {
                    "Art Culture"    : "Art & Culture",
                    "Animals & Pets" : "Animals & Pets",
                    "Design & Photography"  :"Design & Photography",
                    "Electronics"  :"Electronics",
                    "Education & Books"  :"Education & Books",
                    "Business & Services"  :"Business & Services",
                    "Cars & Motorcycles"  :"Cars & Motorcycles",
                    "Sports,_Outdoors & Travel"  :"Sports, Outdoors & Travel",
                    "Fashion & Beauty"  :"Fashion & Beauty",
                    "Computers & Internet"  :"Computers & Internet",
                    "Food & Restaurant"  :"Food & Restaurant",
                    "Home & Family"  :"Home & Family",
                    "Entertainment,_Games & Nightlife"  :"Entertainment, Games & Nightlife",
                    "Holidays,_Gifts & Flowers"  :"Holidays, Gifts & Flowers",
                    "Society & People"  :"Society & People",
                    "Medical_(Healthcare)"  :"Medical (Healthcare)",
                    "Society & People"  :"Society & People",
                    "Other"  :"Others"
                  },
                  value : ""
                },
               image : dyFInputs.image(),
                description : {
                  label: "Déscription",
                  inputType : "text",
                  value : ""
                }
              },
              beforeBuild : function(){
                dyFObj.setMongoId('cms',function(data){
                  uploadObj.gotoUrl = location.hash;
                  tplCtx.id=dyFObj.currentElement.id;
                  tplCtx.collection=dyFObj.currentElement.type;
                });
              },
              save : function () {
                tplCtx.value = {};
                $.each( activeForm.jsonSchema.properties , function(k,val) { 
                  tplCtx.value[k] = $("#"+k).val();
                });
                tplCtx.value.parent={};
                tplCtx.value.parent[thisContextId] = {
                  type : thisContextType, 
                  name : "<?php echo $tpl ?>"
                };
                tplCtx.value.cmsList = <?php echo json_encode($idCmsMerged) ?>;
                mylog.log(tplCtx);
                tplCtx.value.tplsUser={};
                tplCtx.value.tplsUser[thisContextId] = {
                  "<?= $page?>": "using"
                };
                if(typeof tplCtx.value == "undefined")
                  toastr.error('value cannot be empty!');
                else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("Élément bien ajouter");
                      $("#ajax-modal").modal('hide');                  
                    });
                  } );
                  if(tplUsingId != ""){
                   tplObj.saveTplUser(tplUsingId,"backup");
                  }
                  //if tpl parent choose "save as new", duplicate and leave currently cms then save it into the new template
                  if (<?php echo json_encode($cmsInUseId) ?> !="") {                
                    tplObj.duplicateCms(<?php echo json_encode($cmsInUseId) ?>,tplCtx.id);
                  }
                  tplObj.removeCmsStat(tplCtx.id,<?php echo json_encode($newCmsId) ?>);
                 urlCtrl.loadByHash(location.hash);
                }
              }
            }
          };    
      dyFObj.openForm( activeForm );
      }
    },

    //Preview template-------------------------------------------------------------
    previewTpl :function () { 
        $(hideOnPreview).fadeOut();
        var styleHideTitleOnPreview = 
        `<style id="hideBlocksCmsTitleOnHover">
          .block-parent:hover .title:not(:empty):before,.block-parent:hover .title-1:not(:empty):before,
          .block-parent:hover .subtitle:not(:empty):before,.block-parent:hover .title-2:not(:empty):before,
          .block-parent .description:not(:empty):before,.block-parent .title-3:not(:empty):before,
          .block-parent:hover .other:not(:empty):before,.block-parent:hover .title-4:not(:empty):before,
          .block-parent .title-5:not(:empty):before,
          .block-parent:hover .title-6:not(:empty):before{
              display: none !important;
          }
        </style>`;
        $('head').append(styleHideTitleOnPreview);
        previewMode = true;
        if(localStorage.getItem("forgetPreview") != "true"){
          bootbox.alert("<p class='text-center'>Appuyer sur la touche <kbd>Escape</kbd> pour désactiver le mode preview</p>"+
            "<input type='checkbox' name='forgetPreview' id='forgetPreview'>  Ne plus repéter !",function(){
              if($("#forgetPreview").prop('checked') == true)
                localStorage.setItem("forgetPreview","true");
              else
                localStorage.removeItem("forgetPreview");
            });
        }
    }
}

jQuery(document).ready(function() {
if(location.href.indexOf('/costum/co/index/') != -1)
  $('#go-to-communecter').attr("href",baseUrl+"/#@"+costum.contextSlug);
else
  $('#go-to-communecter').attr("href","https://www.communecter.org/#@"+costum.contextSlug);

$(".btn_tpl_filter").click(function(){
    params = {
    "category" : $(this).data("tplcategory"),
    "type" : "template"
  };
  $.ajax({
    type : 'POST',
    data : params,
    url : baseUrl+"/costum/blockcms/gettemplatebycategory",
    dataType : "JSON",
    async : false,
    success : function(data){
      $.each(data,function(key,value){
        console.log("Template"+ value.name);
      })
    }
  });
});

//$(".editDesc").click(function(){
$('body').on('click',".editDesc", function () {
  mylog.log("sary", $(this).data("img"));
  var tplCtx = {};
  var tplId = $(this).data("id");
  tplCtx.collection = "cms";
  var activeFormEdit = {
        "jsonSchema" : {
          "title" : "Modification du template",
          "type" : "object",
          "properties" : {
            name : {
              label: "Nom du template",
              inputType : "text",
              value : $(this).data("name")
            },
            type : { 
              "inputType" : "hidden",
              value : "template" 
            },
            category : {
              label : "Catégorie",
              inputType : "select",
              rules:{
                "required":true
              },
              options : {
                "Art & Culture"    : "Art & Culture",
                "Animals & Pets" : "Animals & Pets",
                "Design & Photography"  :"Design & Photography",
                "Electronics"  :"Electronics",
                "Education & Books"  :"Education & Books",
                "Business & Services"  :"Business & Services",
                "Cars & Motorcycles"  :"Cars & Motorcycles",
                "Sports,_Outdoors & Travel"  :"Sports, Outdoors & Travel",
                "Fashion & Beauty"  :"Fashion & Beauty",
                "Computers & Internet"  :"Computers & Internet",
                "Food & Restaurant"  :"Food & Restaurant",
                "Home & Family"  :"Home & Family",
                "Entertainment,_Games & Nightlife"  :"Entertainment, Games & Nightlife",
                "Holidays,_Gifts & Flowers"  :"Holidays, Gifts & Flowers",
                "Society & People"  :"Society & People",
                "Medical_(Healthcare)"  :"Medical (Healthcare)",
                "Society & People"  :"Society & People",
                "Other"  :"Others"
              },
              value : $(this).data("category")
            },
            image : {
            "inputType" : "uploader",
            "label" : "Image en fond",
            "docType": "image",
            "itemLimit" : 1,
            "filetypes": ["jpeg", "jpg", "gif", "png"],
            "showUploadBtn": false,
            initList : $(this).data("img")
          },
            description : {
              label: "Déscription",
              inputType : "text",
              value : $(this).data("desc")
            }
          },

          beforeBuild : function(){
            uploadObj.set("cms",tplId);
          },

          save : function () {
            tplCtx.id = tplId;
            tplCtx.value = {};
            $.each( activeFormEdit.jsonSchema.properties , function(k,val) { 
              tplCtx.value[k] = $("#"+k).val();
            });
            mylog.log(tplCtx);
            if(typeof tplCtx.value == "undefined")
              toastr.error('value cannot be empty!');
            else {
              tplCtx.value.name = tplCtx.value.name.replace(/'/g, '’');
              tplCtx.value.description = tplCtx.value.description.replace(/'/g, '’');
             dataHelper.path2Value( tplCtx, function(params) {
              dyFObj.commonAfterSave(params,function(){
                if(params.result){
                  toastr.success("Modification enregistré!");
                  $("#ajax-modal").modal('hide');
                urlCtrl.loadByHash(location.hash);
              }else
              toastr.error(params.msg);
            });
            } );
           }
         }
       }
     }; 
  dyFObj.openForm( activeFormEdit );
});

$(".saveChangeTpl").click(function(){
  tplCtx.id = tplUsingId;
  tplCtx.collection = "cms";
    var activeForm = {
      "jsonSchema" : {
        "title" : "Enregistrement du template",
        "type" : "object",
        onLoads : {
          onload : function(data){
            $(".parentfinder").css("display","none");
          }
        },
        "properties" : {
          name : {
            label: "Nom du template",
            inputType : "text",
            value : `<?php echo $nameTpl ?>`
          }, 
          category : {
              label : "Catégorie",
              inputType : "select",
              rules:{
                "required":true
              },
              options : {
                "Art & Culture"    : "Art & Culture",
                "Animals & Pets" : "Animals & Pets",
                "Design & Photography"  :"Design & Photography",
                "Electronics"  :"Electronics",
                "Education & Books"  :"Education & Books",
                "Business & Services"  :"Business & Services",
                "Cars & Motorcycles"  :"Cars & Motorcycles",
                "Sports,_Outdoors & Travel"  :"Sports, Outdoors & Travel",
                "Fashion & Beauty"  :"Fashion & Beauty",
                "Computers & Internet"  :"Computers & Internet",
                "Food & Restaurant"  :"Food & Restaurant",
                "Home & Family"  :"Home & Family",
                "Entertainment,_Games & Nightlife"  :"Entertainment, Games & Nightlife",
                "Holidays,_Gifts & Flowers"  :"Holidays, Gifts & Flowers",
                "Society & People"  :"Society & People",
                "Medical_(Healthcare)"  :"Medical (Healthcare)",
                "Society & People"  :"Society & People",
                "Other"  :"Others"
              },
              value : "<?php echo htmlspecialchars_decode($categoryTpl) ?>"
            },
          image : {
            "inputType" : "uploader",
            "label" : "Image en fond",
            "docType": "image",
            "itemLimit" : 1,
            "filetypes": ["jpeg", "jpg", "gif", "png"],
            "showUploadBtn": false,
            initList : <?php echo json_encode($tplInitImage); ?>
          },
          description : {
            label: "Description",
            inputType : "text",
            value : `<?php echo $descriptionTpl ?>`
          }
        }
      }
    };          
     activeForm.jsonSchema.afterBuild = function(){     
            uploadObj.set("cms",tplUsingId);
      }; 

    activeForm.jsonSchema.save = function () {
      tplCtx.path = "allToRoot";
      tplCtx.value = {};
      $.each( activeForm.jsonSchema.properties , function(k,val) { 
        tplCtx.value[k] = $("#"+k).val();
      });
      tplCtx.value.cmsList = [];
      tplCtx.value.cmsList = <?php echo json_encode($idCmsMerged) ?>;
      if(typeof tplCtx.value == "undefined")
        toastr.error('value cannot be empty!');
      else {
        mylog.log("activeForm save tplCtx",tplCtx);
        tplCtx.value.name = tplCtx.value.name.replace(/'/g, '’');
        tplCtx.value.description = tplCtx.value.description.replace(/'/g, '’');
        dataHelper.path2Value( tplCtx, function(params) {
          dyFObj.commonAfterSave(params,function(){
            if(params.result){
              toastr.success("Modification enregistré!");
              $("#ajax-modal").modal('hide'); 
              urlCtrl.loadByHash(location.hash);
              smallMenu.open(tplMenu);
            }  else {
              toastr.error(params.msg);
            tplObj.removeCmsStat(tplCtx.id,<?php echo json_encode($newCmsId) ?>);
            }
          });
        } );
      } 
    }
    dyFObj.openForm( activeForm );
});

$(".editBtn").off().on("click",function() { 
  var activeForm = {
    "jsonSchema" : {
      "title" : "Template config",
      "type" : "object",
      "properties" : {
      }
    }
  };
  if(configDynForm.jsonSchema.properties[ $(this).data("key") ])
    activeForm.jsonSchema.properties[ $(this).data("key") ] = configDynForm.jsonSchema.properties[ $(this).data("key") ];
  else
    activeForm.jsonSchema.properties[ $(this).data("key") ] = { label : $(this).data("label") };

  if($(this).data("label"))
    activeForm.jsonSchema.properties[ $(this).data("key") ].label = $(this).data("label");
  if($(this).data("type")){
    activeForm.jsonSchema.properties[ $(this).data("key") ].inputType = $(this).data("type");
    if($(this).data("type") == "textarea" && $(this).data("markdown") )
      activeForm.jsonSchema.properties[ $(this).data("key") ].markdown = true;
  }
  tplCtx.id = contextData.id;
  tplCtx.collection = contextData.type;
  tplCtx.key = $(this).data("key");
  tplCtx.path = $(this).data("path");

  activeForm.jsonSchema.save = function () {  
    tplCtx.value = $( "#"+tplCtx.key ).val();
    mylog.log("activeForm save tplCtx",tplCtx);
    if(typeof tplCtx.value == "undefined")
      toastr.error('value cannot be empty!');
    else {
      dataHelper.path2Value( tplCtx, function(params) { 
        $("#ajax-modal").modal('hide');
      } );
    }
  }
  dyFObj.openForm( activeForm );
});


   $(".editTpl").off().on("click",function() { 
    var activeForm = {
      "jsonSchema" : {
        "title" : "Edit Question config",
        "type" : "object",
        "properties" : {}
      }
    };

    $.each( formTpls [ $(this).data("key") ] , function(k,val) { 
      mylog.log("formTpls",k,val); 
      activeForm.jsonSchema.properties[ k ] = {
        label : k,
        value : val
      };
    });

    mylog.log("formTpls activeForm.jsonSchema.properties",activeForm.jsonSchema.properties); 
    tplCtx.id = $(this).data("id");
    tplCtx.key = $(this).data("key");
    tplCtx.collection = $(this).data("collection");            
    tplCtx.path = $(this).data("path");

    activeForm.jsonSchema.save = function () {  
      tplCtx.value = {};
      $.each( formTpls [ tplCtx.key ] , function(k,val) { 
        tplCtx.value[k] = $("#"+k).val();
      });
      mylog.log("save tplCtx",tplCtx);
      if(typeof tplCtx.value == "undefined")
        toastr.error('value cannot be empty!');
      else {
        dataHelper.path2Value( tplCtx, function(params) { 
          $("#ajax-modal").modal('hide');
        } );
      }

    }
    dyFObj.openForm( activeForm );
  });

//check if new costum
if(localStorage.getItem("newCostum"+thisContextSlug) == thisContextSlug){
      bootbox.confirm({
          message: "<h5 class='text-success text-center'>Bienvenue sur votre nouvel costum !</h5>"+
                    "<p class='text-success text-center'>Vous avez <b>4 étapes</b> pour créer votre costum :</p>"+
                    "<h6 class='text-center'><u class='text-danger'>Etape 1:</u> Choisir un template</h6>"+
                    "<h6 class='text-center'><u class='text-danger'>Etape 2:</u> Ajouter du section ou bloc CMS</h6>"+
                    "<h6 class='text-center'><u class='text-danger'>Etape 3:</u> Ajouter des APP ou MENU</h6>"+
                    "<h6 class='text-center'><u class='text-danger'>Etape 4:</u> Ajouter de données dans le bloc CMS</h6>",
          buttons: {
              confirm: {
                  label: 'Ok',
                  className: 'btn-success'
              },
              cancel: {
                  label: 'No',
                  className: 'hidden'
              }
          },
          callback: function (result) {
              if(result){
                  localStorage.removeItem("newCostum"+thisContextSlug);
                  $(".tpl-engine-btn-list ul li.a").addClass("btn-pulse").on('click',function(){
                      $(this).removeClass("btn-pulse");
                      $(".openListTpls").addClass("btn-pulse").on('click',function(){
                          $(this).removeClass("btn-pulse");
                          $(".tpl-engine-btn-list ul li.g").addClass("btn-pulse").on('click',function(){
                              $(this).removeClass("btn-pulse");
                          })
                      })
                  });

              }
          }
      });
   }
});

var hideOnPreview = '#saveTemplate,.editSectionBtns,.editBtn,.editThisBtn,.editQuestion,.addQuestion,.previewTpl,.openListTpls,.handleDrag,.content-btn-action,.hiddenPreview';
var previewMode = false;
window.onhashchange = function() {
  if($(hideOnPreview).is(':visible') !=true){
    $(hideOnPreview).fadeIn(); 
    previewMode = false;
  }
}
$(document).keyup(function(e) {
  if (e.key === "Escape") { 
      $(hideOnPreview).fadeIn();
      $("#hideBlocksCmsTitleOnHover").remove();
      previewMode = false;
    }
  });

$(".block-parent").mouseover(function(){
  if(previewMode==false){
     $(this).find('.handleDrag').show();
  } 
});
$(".block-parent").mouseout(function(){
    if(previewMode==false){
        $(this).find('.handleDrag').hide();
    } 
})  



$(".tryOtherTpl").click(function(){
  smallMenu.open(tplMenu);
});

$('body').on('click',"button.btn-filter", function () {
  var $target = $(this).data('target');
  if ($target != 'all') {
    $('.tplItems').css('display', 'none');
    $('.tplItems[data-status="' + $target + '"]').fadeIn();
  } else {
    $('#backupTplsContainer').hide();
    $('.tplItems').css('display', 'none').fadeIn();
  }
});

  $("#openModal").modal("hide");
 //In case of empty cmsId, delete cmsList inside tpl to avoid conflict when a user choose it 
if (<?= json_encode($idCmsMerged) ?> == "" && <?= json_encode($btnSaveChange) ?>) {
  tplObj.removeCmsList(tplUsingId);
}
</script>