<!-- By RAMIANDRISON Jean Dieu Donné -->
<!-- email: ramiandrison.jdd@gmail.com -->
<?php 
    $keyTpl = "htmlConstruct";
    $paramsData = [

    ];
    if( isset($this->costum[$keyTpl]) )
        $paramsData =  $this->costum[$keyTpl];

?>

<?php if($canEdit){ ?> 
    <a class='edit<?php echo $keyTpl ?>Params' href='javascript:;' 
        data-id='<?= $this->costum["contextId"]; ?>'
        data-collection='<?= $this->costum["contextType"]; ?>'
        data-key='<?php echo $keyTpl ?>'
        data-path='costum.<?php echo $keyTpl ?>'>
        <i class="fa fa-html5" aria-hidden="true"></i> CONSTRUCTION HTML
    </a>
<?php }?>
<style>
    .htmlConstruct.left:before{
        content:"Menu haut à gauche";
        text-transform: uppercase;
        /*border: 2px solid black;*/
        background-color: #008037;
        color: #fff;
        padding: 8px;
        border-radius: 12px;
        margin-top: -20px;
    }
    .htmlConstruct.right:before{
        content:"Menu haut à droite";
        text-transform: uppercase;
        /*border: 2px solid black;*/
        background-color: #2C3E50;
        color: #fff;
        padding: 8px;
        border-radius: 12px;
        margin-top: -20px;
    }
    .htmlConstruct.menuLeft:before{
        content:"Menu à gauche";
        text-transform: uppercase;
        /*border: 2px solid black;*/
        background-color: #742a09;
        color: #fff;
        padding: 8px;
        border-radius: 12px;
        margin-top: -20px;
    }
    .htmlConstruct.menuRightt:before{
        content:"Menu à droite";
        text-transform: uppercase;
       /*border: 2px solid black;*/
       background-color: #2441c1;
       color: #fff;
        padding: 8px;
        border-radius: 12px;
        margin-top: -20px;
    }
    .htmlConstruct.right{
        margin-top:78px;
        border: 8px solid #2C3E50;
    }
    .htmlConstruct.left{
        margin-top:78px;
        border: 8px solid #008037;
    }
    .htmlConstruct.menuLeft{
        margin-top:78px;
        border: 8px solid #742a09;
    }
    .htmlConstruct.menuRightt{
        margin-top:78px;
        border: 8px solid #2441c1;
    }

    .htmlConstruct.left-xsMenu:before,
    .htmlConstruct.right-xsMenu:before{
        content:"xsMenu";
    }
    .htmlConstruct.left-app:before,
    .htmlConstruct.right-app:before,
    .htmlConstruct.menuLeft-app:before,
    .htmlConstruct.menuRightt-app:before{
        content:"App (Menu)";
    }
    .htmlConstruct.left-dropdown:before,
    .htmlConstruct.right-dropdown:before{
        content:"Menu déroulant";
    }
    .htmlConstruct.left-buttons:before,
    .htmlConstruct.right-buttons:before,
    .htmlConstruct.menuLeft-buttons:before,
    .htmlConstruct.menuRightt-buttons:before{
        content:"Bouttons";
    }
    .htmlConstruct.left-logo:before,
    .htmlConstruct.right-logo:before,
    .htmlConstruct.menuLeft-logo:before,
    .htmlConstruct.menuRightt-logo:before{
        content:"Logo";
    }
    .htmlConstruct.left-userProfil:before,
    .htmlConstruct.right-userProfil:before,
    .htmlConstruct.menuLeft-userProfil:before,
    .htmlConstruct.menuRightt-userProfil:before{
        content:"Profil d'utilisateur";
    }
    .checkbox-htmlConstruct input[type=checkbox]{
      -ms-transform: scale(2); /* IE */
      -moz-transform: scale(2); /* FF */
      -webkit-transform: scale(2); /* Safari and Chrome */
      -o-transform: scale(2); /* Opera */
      transform: scale(2);
      padding: 10px;
    }
    .checkbox-htmlConstruct .label-htmlConstruct{
        margin-top: 4px !important;
        margin-left: 15px !important;
        text-transform: none !important;
    }
</style>

<script>
$(function(){
    var htmlConstructParamsData = <?php echo json_encode($paramsData) ?>;
    sectionDyf.<?php echo $keyTpl ?>ParamsData = htmlConstructParamsData;
mylog.log("htmlConstructParamsData",htmlConstructParamsData);
    //top left-------------------
    var hasMenuTopLeft = (exists(htmlConstructParamsData.header) && 
                        exists(htmlConstructParamsData.header.menuTop) &&
                        (htmlConstructParamsData.header.menuTop != null) &&
                        exists(htmlConstructParamsData.header.menuTop.left) &&
                        exists(htmlConstructParamsData.header.menuTop.left.buttonList));
    var optionsButtonListLeft = {};
    if(hasMenuTopLeft){
        var menuTopLeftPath = htmlConstructParamsData.header.menuTop.left.buttonList;
        if(exists(htmlConstructParamsData.header.menuTop.left.buttonList) && exists(htmlConstructParamsData.header.menuTop.left.buttonList.app) && exists(htmlConstructParamsData.header.menuTop.left.buttonList.app.buttonList))
        $.each(htmlConstructParamsData.header.menuTop.left.buttonList.app.buttonList,function(k,v){
            optionsButtonListLeft[k] = k;
        })
    }
    //------------------------------

    //top-right------------------
    var hasMenuTopRight = (exists(htmlConstructParamsData.header) && 
                        exists(htmlConstructParamsData.header.menuTop) &&
                        (htmlConstructParamsData.header.menuTop != null) &&
                        exists(htmlConstructParamsData.header.menuTop.right) &&
                        exists(htmlConstructParamsData.header.menuTop.right.buttonList));
    var optionsButtonListRight = {};
    if(hasMenuTopRight){
        var menuTopRightPath = htmlConstructParamsData.header.menuTop.right.buttonList;
        if(exists(htmlConstructParamsData.header.menuTop.right.buttonList) && exists(htmlConstructParamsData.header.menuTop.right.buttonList.app) && exists(htmlConstructParamsData.header.menuTop.right.buttonList.app.buttonList))
        $.each(htmlConstructParamsData.header.menuTop.right.buttonList.app.buttonList,function(k,v){
            optionsButtonListRight[k] = k;
        })
    }
    //--------------------------------

    //menu left-----------------------
    var hasMenuLeft = (exists(htmlConstructParamsData.menuLeft) && 
                        exists(htmlConstructParamsData.menuLeft.buttonList));
    var optionsButtonListMenuLeft = {};
    if(hasMenuLeft){
        var menuLeftPath = htmlConstructParamsData.menuLeft.buttonList;
        if(exists(htmlConstructParamsData.menuLeft.buttonList) && htmlConstructParamsData.menuLeft.buttonList != null && exists(htmlConstructParamsData.menuLeft.buttonList.app) && exists(htmlConstructParamsData.menuLeft.buttonList.app.buttonList))
        $.each(htmlConstructParamsData.menuLeft.buttonList.app.buttonList,function(k,v){
            optionsButtonListMenuLeft[k] = k;
        })
    }
    //--------------------------------

    //menu right-----------------------
    var hasMenuRight = (exists(htmlConstructParamsData.menuRight) && 
                        exists(htmlConstructParamsData.menuRight.buttonList));
    var optionsButtonListMenuRight = {};
    if(hasMenuRight){
        var menuRightPath = htmlConstructParamsData.menuRight.buttonList;

        if(exists(htmlConstructParamsData.menuRight.buttonList) && htmlConstructParamsData.menuRight.buttonList != null && exists(htmlConstructParamsData.menuRight.buttonList.app) && exists(htmlConstructParamsData.menuRight.buttonList.app.buttonList))
        $.each(htmlConstructParamsData.menuRight.buttonList.app.buttonList,function(k,v){
            optionsButtonListMenuRight[k] = k;
        })
    }
    //--------------------------------

    var checkBoxParams = {
                        "onText" : "Oui",
                        "offText" : "Non",
                        "onLabel" : "Oui",
                        "offLabel" : "Non",
                        "labelText" : "label"
                    }
    //important variable menuProps
    var menuProps = {};

    //change themeParams seitting href
    if(notNull(userConnected))
        themeParams["mainMenuButtons"]["settings"]["href"] = "#@"+userConnected.slug+".view.settings";

    //get current app
    var optionsApp = {};
    $.each(costum.app,function(k,v){
        optionsButtonListLeft[k] = k;
        optionsButtonListRight[k] = k;
        optionsButtonListMenuLeft[k] = k;
        optionsButtonListMenuRight[k] = k;
    })



    //create dropdown options
    var dropdownOption = {}
    $.each(themeParams["mainMenuButtons"],function(k,v){
        dropdownOption[k] = k
    })
    delete dropdownOption["dropdown"];
    delete dropdownOption["xsMenu"];
    delete dropdownOption["app"];
        
    /****************begin function which generate menu properties***********/
    function generateMenuProps(key,ifExists,path){
                    if(key=="left") optionsApp = optionsButtonListLeft;
                    if(key=="right") optionsApp = optionsButtonListRight;
                    if(key == "menuLeft") optionsApp = optionsButtonListMenuLeft;
                    if(key == "menuRight") optionsApp = optionsButtonListMenuRight;
        $.each(themeParams["mainMenuButtons"],function(k,v){
            if(k != "app" && k!="xsMenu" && k!="dropdown" && k!="logo" && k!= "userProfil"){
                setTimeout(function(){
                    var keys ="";
                    if(key =="left" || key == "right")
                        keys = "header-menuTop-"+key+"-buttonList-";
                    else if(key == "menuLeft" || key == "menuRight")
                        keys = key+"-buttonList-";
                     menuProps[keys+k] = {
                        "inputType" : "checkboxSimple",
                        "label" : k,
                        "params" : checkBoxParams,
                        "checked" : (ifExists && path!=null && exists(path[k])) ? true : false
                    };
                },100)
            }else if(k == "xsMenu" && key != "menuLeft" && key != "menuRight"){
                    var keys = "header-menuTop-"+key+"-buttonList-"+k+"-buttonList-app-";

                    menuProps[keys+"label"] = {
                        "inputType" : "checkboxSimple",
                        "label" : "Afficher le label",
                        "params" : checkBoxParams,
                        "checked" : (ifExists && path!=null && exists(path[k]) && exists(path[k].buttonList) && exists(path[k].buttonList.app) && exists(path[k].buttonList.app.label) ) ? path[k].buttonList.app.label : false
                    };
                    menuProps[keys+"icon"] = {
                        "inputType" : "checkboxSimple",
                        "label" : "Afficher l'icon",
                        "params" : checkBoxParams,
                        "checked" : (ifExists && path!=null && exists(path[k]) && exists(path[k].buttonList) && exists(path[k].buttonList.app) && exists(path[k].buttonList.app.icon) ) ? path[k].buttonList.app.icon : false
                    };
                    menuProps[keys+"spanTooltip"] = {
                        "inputType" : "checkboxSimple",
                        "label" : "Afficher le tooltip",
                        "params" : checkBoxParams,
                        "checked" : (ifExists && path!=null && exists(path[k]) && exists(path[k].buttonList) && exists(path[k].buttonList.app) && exists(path[k].buttonList.app.spanTooltip) ) ? path[k].buttonList.app.spanTooltip : false
                    };
                    menuProps[keys+"keyListButton"] = {
                        "inputType" : "hidden",
                        "params" : checkBoxParams,
                        "checked" : (ifExists && path!=null && exists(path[k]) && exists(path[k].buttonList) && exists(path[k].buttonList.app) && exists(path[k].buttonList.app.keyListButton) ) ? path[k].buttonList.app.keyListButton : "pages"
                    };
                    menuProps[keys+"class"] = {
                        "inputType" : isDeveloper ? "text" : "hidden",
                        "label" : isDeveloper ? "App Xs class <span class='text-danger'>(Developer only)</span>" : "",
                        value : (ifExists && path!=null && exists(path[k]) && exists(path[k].buttonList) && exists(path[k].buttonList.app) && exists(path[k].buttonList.app.class) ) ? path[k].buttonList.app.class : ""
                    };

                    menuProps[keys+"labelClass"] = {
                        "inputType" : isDeveloper ? "text" : "hidden",
                        "label" : isDeveloper ? "Label class <span class='text-danger'>(Developer only)</span>" : "",
                        value : (ifExists && path!=null && exists(path[k]) && exists(path[k].buttonList) && exists(path[k].buttonList.app) && exists(path[k].buttonList.app.labelClass) ) ? path[k].buttonList.app.labelClass : "padding-left-10"
                    };
                    menuProps[keys+"buttonList"] = {
                        "inputType" : "selectMultiple",
                        /*"list" : keys+"buttonList",
                        "optionsValueAsKey" : true,
                        "isSelect2" : true,*/
                        "noOrder" :true,
                        "label" : "Cliquer,Choisir et organiser les menus xs (drag et drop pour organiser)",
                        "options" : optionsApp,
                        value : (ifExists && path!=null && exists(path[k]) && exists(path[k].buttonList) && exists(path[k].buttonList.app) && exists(path[k].buttonList.app.buttonList) ) ? Object.keys(path[k].buttonList.app.buttonList) : Object.keys(optionsApp)
                    };                    
            }else if(k == "app"){
                    if(key =="left" || key == "right")
                        var keys = "header-menuTop-"+key+"-buttonList-"+k+"-";
                    else if(key == "menuLeft" || key == "menuRight")
                        var keys = key+"-buttonList-"+k+"-";

                    menuProps[keys+"label"] = {
                        "inputType" : "checkboxSimple",
                        "label" : "Afficher le label",
                        "params" : checkBoxParams,
                        "checked" : (ifExists && path!=null && exists(path[k]) && exists(path[k].label)) ? path[k].label : false
                    };
                    menuProps[keys+"icon"] = {
                        "inputType" : "checkboxSimple",
                        "label" : "Afficher l'icon",
                        "params" : checkBoxParams,
                        "checked" : (ifExists && path!=null && exists(path[k]) && exists(path[k].icon)) ? path[k].icon : false
                    };
                    menuProps[keys+"spanTooltip"] = {
                        "inputType" : "checkboxSimple",
                        "label" : "Afficher le tooltip",
                        "params" : checkBoxParams,
                        "checked" : (ifExists && path!=null && exists(path[k]) && exists(path[k].spanTooltip)) ? path[k].spanTooltip : false
                    };
                    menuProps[keys+"class"] = {
                        "inputType" : isDeveloper ? "text" : "hidden",
                        "label" : isDeveloper ? "App class <span class='text-danger'>(Developer only)</span>" : "",
                        value : (ifExists && path!=null && exists(path[k]) && exists(path[k].class)) ? path[k].class : false
                    };

                    menuProps[keys+"labelClass"] = {
                        "inputType" : isDeveloper ? "text" : "hidden",
                        "label" : isDeveloper ? "Label class <span class='text-danger'>(Developer only)</span>" : "",
                        value : (ifExists && path!=null && exists(path[k]) && exists(path[k].labelClass)) ? path[k].labelClass : "padding-left-10"
                    };

                    menuProps[keys+"buttonList"] = {
                        "inputType" : "selectMultiple",
                        /*"list" : keys+"buttonList",
                        "optionsValueAsKey" : true,
                        "isSelect2" : true,*/
                        "label" : "Cliquer,Choisir et organiser les menus (drag et drop pour organiser)",
                        "noOrder" :true,
                         "options" : optionsApp,
                        value : (ifExists && path!=null && exists(path[k]) && exists(path[k].buttonList)) ? Object.keys(path[k].buttonList) : Object.keys(optionsApp)
                    };                        

            }else if(k == "dropdown" && key !="menuLeft" && key !="menuRight"){
                setTimeout(function(){
                    menuProps["header-menuTop-"+key+"-buttonList-"+k+"-buttonList"] = {
                        "inputType" : "selectMultiple",
                        "label" : "Choisir des bouttons",
                         "options" : dropdownOption,
                        value : (ifExists && path!=null && exists(path[k]) && exists(path[k].buttonList)) ? Object.keys(path[k].buttonList) : Object.keys(dropdownOption)
                    }; 
                },200)       
            }else if(k == "logo"){
                    if(key =="left" || key == "right")
                        var keys = "header-menuTop-"+key+"-buttonList-"+k+"-";
                    else if(key == "menuLeft" || key == "menuRight")
                        var keys = key+"-buttonList-"+k+"-";

                    menuProps[keys+"width"] = {
                        "inputType" : "text",
                        "label" : "Largeur",
                        value : (ifExists && path!=null && exists(path[k]) && exists(path[k].width)) ? path[k].width : 50
                    };
                    menuProps[keys+"height"] = {
                        "inputType" : "text",
                        "label" : "Hauteur",
                        value : (ifExists && path!=null && exists(path[k]) && exists(path[k].height)) ? path[k].height : 50
                    };                 
            }else{
                var j = "userProfil";
                 setTimeout(function(){
                    if(key =="left" || key == "right")
                        var keys = "header-menuTop-"+key+"-buttonList-"+j+"-";
                    else if(key == "menuLeft" || key == "menuRight")
                        var keys = key+"-buttonList-"+j+"-";

                    menuProps[keys+"name"] = {
                        "inputType" : "checkboxSimple",
                        "label" : "Afficher le nom",
                        "params" : checkBoxParams,
                        checked : (ifExists && path!=null && exists(path[j]) && exists(path[j].name)) ? path[j].name : false
                    };

                    menuProps[keys+"img"] = {
                        "inputType" : "checkboxSimple",
                        "label" : "Afficher l'image",
                        "params" : checkBoxParams,
                        "label" : "Afficher la photo",
                        checked : (ifExists && path!=null && exists(path[j]) && exists(path[j].img)) ? path[j].img : false
                    };
                },150)               
            }
        })

        //setTimeout(function(){
            if(key =="menuLeft")
                menuProps[key+"-addClass"] = {
                    "inputType" : (isDeveloper ? "text" : "hidden"),
                    "placeholder" : "pos-top-0 fit-content padding-top-20",
                    "label" : (isDeveloper ? "Class <span class='text-danger'>(Developer only)</span>" : ""),
                    value : (exists(htmlConstructParamsData.menuLeft) && exists(htmlConstructParamsData.menuLeft.addClass)) ?
                        htmlConstructParamsData.menuLeft.addClass : "pos-top-0 fit-content padding-top-20"
                }
            if(key =="menuRight")
                menuProps[key+"-addClass"] = {
                    "inputType" : (isDeveloper ? "text" : "hidden"),
                    "placeholder" : "pos-top-0 fit-content padding-top-20",
                    "label" : (isDeveloper ? "Class <span class='text-danger'>(Developer only)</span>" : ""),
                    value : (exists(htmlConstructParamsData.menuRight) && exists(htmlConstructParamsData.menuRight.addClass)) ?
                        htmlConstructParamsData.menuRight.addClass : "fit-content"
                }

            if(key =="left"){
                    menuProps["header-menuTop-"+key+"-addClass"] = {
                        "inputType" : "text",
                        "placeholder" : "text-right col-sm-6 col-lg-7 navigator",
                        "label" : "Class <span class='text-danger'>(Developer only)</span>",
                        value : (exists(htmlConstructParamsData.header) && exists(htmlConstructParamsData.header.menuTop) && (htmlConstructParamsData.header.menuTop != null) && exists(htmlConstructParamsData.header.menuTop.left) && exists(htmlConstructParamsData.header.menuTop.left.addClass)) ?
                            htmlConstructParamsData.header.menuTop.left.addClass : ""
                    }
            }

             if(key =="right")
                 menuProps["header-menuTop-"+key+"-addClass"] = {
                     "inputType" : "text",
                     "placeholder" : "",
                     "label" : "Class <span class='text-danger'>(Developer only)</span>",
                     value : (exists(htmlConstructParamsData.header) && exists(htmlConstructParamsData.header.menuTop) && (htmlConstructParamsData.header.menuTop != null) && exists(htmlConstructParamsData.header.menuTop.right) && exists(htmlConstructParamsData.header.menuTop.right.addClass)) ?
                         htmlConstructParamsData.header.menuTop.right.addClass : ""
                 }
       // },500)

            //buttons



        
    }
    /******************end function which generate properties***********/


    //choose menu**********************************
    var possibleMenu = "";
    if(!hasMenuTopLeft)
    possibleMenu += '<div class="checkbox checkbox-htmlConstruct">'+
      '<label><input type="checkbox" name="htmlConstruct" value="left"><h5 class="label-htmlConstruct">Menu haut gauche</h5></label>'+
    '</div>';
    if(!hasMenuTopRight)
    possibleMenu+= '<div class="checkbox checkbox-htmlConstruct">'+
      '<label><input type="checkbox" name="htmlConstruct" value="right"><h5 class="label-htmlConstruct">Menu haut droite</h5></label>'+
    '</div>';
    if(!hasMenuLeft)
    possibleMenu+= '<div class="checkbox checkbox-htmlConstruct">'+
      '<label><input type="checkbox" name="htmlConstruct" value="menuLeft"><h5 class="label-htmlConstruct">Menu gauche</h5></label>'+
    '</div>';
    if(!hasMenuRight)
    possibleMenu+='<div class="checkbox checkbox-htmlConstruct">'+
      '<label><input type="checkbox" name="htmlConstruct" value="menuRight"><h5 class="label-htmlConstruct">Menu droite</h5></label>'+
    '</div>';
    //choose menu**********************************

    /***************begin dyfObj*************************/
    var htmlConstructParams = {
        "jsonSchema" : {    
            "title" : "CONSTRUIRE VOTRE MENU",
            "icon" : "fa-cog",
            "properties" : menuProps,//important
            save : function (data) { 
                tplCtx.value = {};
                $.each( htmlConstructParams.jsonSchema.properties , function(k,val) {
                    var isActiveDivParent = ($("#"+k).parent().parent().data("activated") != false);
                    var arrayKey = k.split("-");
                    var parsedPath = arrayKey.join("][");
                    var targetKey = arrayKey[arrayKey.length -1 ]; 

                    if(isActiveDivParent && $("#"+k).val() != "" && $("#"+k).val() != null){
                        if(arrayKey[4] != "app" && arrayKey[2] != "app" && arrayKey[4] != "xsMenu" && arrayKey[4] !="dropdown" && arrayKey[4] !="userProfil" && arrayKey[2] !="userProfil" && arrayKey[2] != "logo" && arrayKey[4] != "logo" && $("#"+k).val() != "false"){
                             if(arrayKey[1] == "addClass" || arrayKey[3] == "addClass")
                               tplCtx.value[parsedPath] = $("#"+k).val();
                            else if(arrayKey[2] == "right")                         
                                tplCtx.value[parsedPath] = true;
                            else
                                tplCtx.value[parsedPath] = themeParams["mainMenuButtons"][targetKey];
                        } 
                        else if(arrayKey[4] == "xsMenu" || arrayKey[3] == "addClass" || arrayKey[1] == "addClass" || arrayKey[4] == "app" || arrayKey[2] == "app" || arrayKey[4] == "dropdown" || arrayKey[4] == "logo" || arrayKey[2] == "logo" || arrayKey[4] =="userProfil" || arrayKey[2] =="userProfil" ){
                            if(targetKey == "buttonList"){
                                if($("#"+k).val() !=null){
                                    var obj = {};
                                    $.each($("#"+k).val(),function(key,value){
                                        obj[value] = true
                                    });
                                    if(Object.keys(obj).length != 0)
                                        tplCtx.value[parsedPath] = obj;
                                }
                            }else if(arrayKey[4] == "xsMenu" || arrayKey[4] == "app" || arrayKey[2] == "app"){
                                    tplCtx.value[parsedPath] = $("#"+k).val();
                            }else{
                                if( $("#"+k).val() != "false")
                                    tplCtx.value[parsedPath] = $("#"+k).val();
                            }
                        }
                    }
                    
                });

                    mylog.log(tplCtx.value,"gisagisa");
                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    tplCtx.updatePartial=true;
                    tplCtx.format=true;
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#ajax-modal").modal('hide');
                        toastr.success("Bien ajouté");
                        location.reload();
                    } );
                }
            }
        }
    };
    /**************end dyfObj ***********************/

    /********************Begin show dyFObj in on click *****************/
    $(".edit<?php echo $keyTpl ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        bootbox.dialog({
            /*size: "small",*/
            title: '<h6 class="text-center">Ajouter des élements</h6>',
            message: possibleMenu,
            buttons: {
                cancel: {
                    label: "Annuler",
                    className: 'btn-danger',
                    callback: function(){
                        console.log('Custom cancel clicked');
                    }
                },
                noclose: {
                    label: "Gérer existants",
                    className: 'bg-dark text-white',
                    callback: function(){
                        htmlConstructAddNewMenu(false,function(){
                            openHtmlConsctructForm(function(){
                                callSelectSortable();
                            });
                        })
                    }
                },
                ok: {
                    label: "Ajouter",
                    className: 'text-white bg-green-k htmlc-add-menu',
                    callback: function(){
                        htmlConstructAddNewMenu(true,function(){
                            openHtmlConsctructForm(function(){
                                callSelectSortable();
                            });
                        })
                    }
                }
            }
        }); 
        $(".htmlc-add-menu").prop("disabled","true");
        $("input[name='htmlConstruct']").click(function(){
            if($("input[name='htmlConstruct']").is(':checked'))
                $(".htmlc-add-menu").removeAttr("disabled");
            else
                $(".htmlc-add-menu").prop("disabled","true");
        })
    })
    //($("input[name='htmlConstruct']").prop('checked') == true)

    /********************End show dyFObj in on click *****************/

    function openHtmlConsctructForm(callback){
        setTimeout(function(){
            dyFObj.openForm(htmlConstructParams,null, htmlConstructParamsData);

            var arrTopLeft = [],childxsMenuTopLeft = [],childAppTopLeft = [],childDropdownTopLeft=[],childButtonTopLeft=[],childClassTopLeft=[],arrLogoTopLeft=[],arrLogoTopLeft=[],arrUserProfilLeft = [],
                arrTopRight = [],childxsMenuTopRight = [],childAppTopRight = [],childDropdownTopRight=[],childButtonTopRight=[],childClassTopRight =[],arrLogoTopRight=[],arrUserProfilRight = [],
                arrMenuLeft = [],childAppMenuLeft = [],childButtonsMenuLeft = [],childClassMenuLeft = [],arrLogoMenuLeft=[],arrUserProfilMenuLeft = [],
                arrMenuRight = [],childAppMenuRight = [],childButtonsMenuRight = [],childClassMenuRight = [],arrLogoMenuRight=[],arrUserProfilMenuRight = [];

            $.each(htmlConstructParams.jsonSchema.properties , function(k,val) {
                var key = k.split("-");
                //alert(key);
                if(key[2] == "left"){
                    if(key[4] == "xsMenu")
                        childxsMenuTopLeft.push('.'+k+val.inputType);
                    else if(key[4] == "app")
                        childAppTopLeft.push('.'+k+val.inputType);
                    else if(key[4] == "dropdown")
                        childDropdownTopLeft.push('.'+k+val.inputType);
                    else if(key[4] == "logo")
                        arrLogoTopLeft.push('.'+k+val.inputType);
                    else if(key[4] == "userProfil")
                        arrUserProfilLeft.push('.'+k+val.inputType);
                    else if(key[3] == "addClass")
                        childClassTopLeft.push('.'+k+val.inputType);
                    else
                        childButtonTopLeft.push('.'+k+val.inputType);
                    arrTopLeft.push('.'+k+val.inputType);
                }

                else if(key[2] == "right"){
                    if(key[4] == "xsMenu")
                        childxsMenuTopRight.push('.'+k+val.inputType);
                    else if(key[4] == "app")
                        childAppTopRight.push('.'+k+val.inputType);
                     else if(key[4] == "dropdown")
                        childDropdownTopRight.push('.'+k+val.inputType);
                    else if(key[4] == "logo")
                        arrLogoTopRight.push('.'+k+val.inputType);
                    else if(key[4] == "userProfil")
                        arrUserProfilRight.push('.'+k+val.inputType);
                    else if(key[3] == "addClass")
                        childClassTopRight.push('.'+k+val.inputType);
                    else
                        childButtonTopRight.push('.'+k+val.inputType);
                    arrTopRight.push('.'+k+val.inputType);
                }

                else if(key[0] == "menuLeft"){
                    if(key[1] == "buttonList" && key[2] == "app")
                        childAppMenuLeft.push('.'+k+val.inputType);
                    else if(key[1] == "buttonList" && key[2] == "logo")
                        arrLogoMenuLeft.push('.'+k+val.inputType);
                    else if(key[1] == "buttonList" && key[2] == "userProfil")
                        arrUserProfilMenuLeft.push('.'+k+val.inputType);
                    else if(key[1] == "addClass")
                        childClassMenuLeft.push('.'+k+val.inputType);
                    else 
                        childButtonsMenuLeft.push('.'+k+val.inputType);
                    arrMenuLeft.push('.'+k+val.inputType);
                }

                else if(key[0] == "menuRight"){
                    if(key[1] == "buttonList" && key[2] == "app")
                        childAppMenuRight.push('.'+k+val.inputType);
                    else if(key[1] == "buttonList" && key[2] == "logo")
                        arrLogoMenuRight.push('.'+k+val.inputType);
                    else if(key[1] == "buttonList" && key[2] == "userProfil")
                        arrUserProfilMenuRight.push('.'+k+val.inputType);
                    else if(key[1] == "addClass")
                        childClassMenuRight.push('.'+k+val.inputType);
                    else 
                        childButtonsMenuRight.push('.'+k+val.inputType);
                    arrMenuRight.push('.'+k+val.inputType);
                }
            })


                wrapToDiv(arrTopLeft,"htmlConstruct","left",12,"",tplCtx.path,'header.menuTop.left');
                wrapToDiv(childxsMenuTopLeft,"htmlConstruct","left-xsMenu",3,"",tplCtx.path,'header.menuTop.left.buttonList.xsMenu');
                wrapToDiv(childAppTopLeft,"htmlConstruct","left-app",3,"",tplCtx.path,'header.menuTop.left.buttonList.app');
                wrapToDiv(childDropdownTopLeft,"htmlConstruct","left-dropdown",3,"",tplCtx.path,'header.menuTop.left.buttonList.dropdown');
                wrapToDiv(childButtonTopLeft,"htmlConstruct","left-buttons",3,"");
                if(isDeveloper) wrapToDiv(childClassTopLeft,"htmlConstruct","left-class",12,"");
                wrapToDiv(arrLogoTopLeft,"htmlConstruct","left-logo",6,"",tplCtx.path,'header.menuTop.left.buttonList.logo');
                wrapToDiv(arrUserProfilLeft,"htmlConstruct","left-userProfil",6,"",tplCtx.path,'header.menuTop.left.buttonList.userProfil');
                $(".htmlConstruct.left-buttons,.htmlConstruct.left-class").children().show();     



            wrapToDiv(arrTopRight,"htmlConstruct","right",12,"",tplCtx.path,'header.menuTop.right');
                wrapToDiv(childxsMenuTopRight,"htmlConstruct","right-xsMenu",3,"",tplCtx.path,'header.menuTop.right.buttonList.xsMenu');
                wrapToDiv(childAppTopRight,"htmlConstruct","right-app",3,"",tplCtx.path,'header.menuTop.right.buttonList.app');
                wrapToDiv(childDropdownTopRight,"htmlConstruct","right-dropdown",3,"",tplCtx.path,'header.menuTop.right.buttonList.dropdown');
                wrapToDiv(childButtonTopRight,"htmlConstruct","right-buttons",3,"");
                if(isDeveloper) wrapToDiv(childClassTopRight,"htmlConstruct","right-class",12,"");
                wrapToDiv(arrLogoTopRight,"htmlConstruct","right-logo",6,"",tplCtx.path,'header.menuTop.right.buttonList.logo');
                wrapToDiv(arrUserProfilRight,"htmlConstruct","right-userProfil",6,"",tplCtx.path,'header.menuTop.right.buttonList.userProfil');
                $(".htmlConstruct.right-buttons,.htmlConstruct.right-class").children().show();
            

            wrapToDiv(arrMenuLeft,"htmlConstruct","menuLeft",12,"",tplCtx.path,'menuLeft');
                wrapToDiv(childAppMenuLeft,"htmlConstruct","menuLeft-app",3,"",tplCtx.path,'menuLeft.buttonList.app');
                wrapToDiv(childButtonsMenuLeft,"htmlConstruct","menuLeft-buttons",3,"");
                if(isDeveloper) wrapToDiv(childClassMenuLeft,"htmlConstruct","menuLeft-class",12,"");
                wrapToDiv(arrLogoMenuLeft,"htmlConstruct","menuLeft-logo",6,"",tplCtx.path,'menuLeft.buttonList.logo');
                wrapToDiv(arrUserProfilMenuLeft,"htmlConstruct","menuLeft-userProfil",6,"",tplCtx.path,'menuLeft.buttonList.userProfil');
                $(".htmlConstruct.menuLeft-buttons,.htmlConstruct.menuLeft-class").children().show();       


            wrapToDiv(arrMenuRight,"htmlConstruct","menuRightt",12,"",tplCtx.path,'menuRight');
                wrapToDiv(childAppMenuRight,"htmlConstruct","menuRightt-app",3,"",tplCtx.path,'menuRight.buttonList.app');
                wrapToDiv(childButtonsMenuRight,"htmlConstruct","menuRightt-buttons",3,"");
                if(isDeveloper) wrapToDiv(childClassMenuRight,"htmlConstruct","menuRightt-class",12,"");
                wrapToDiv(arrLogoMenuRight,"htmlConstruct","menuRightt-logo",6,"",tplCtx.path,'menuRight.buttonList.logo');
                wrapToDiv(arrUserProfilMenuRight,"htmlConstruct","menuRightt-userProfil",6,"",tplCtx.path,'menuRight.buttonList.userProfil');
                $(".htmlConstruct.menuRightt-buttons,.htmlConstruct.menuRightt-class").children().show();
            
                $(".menuLeft-buttonList-app-buttonListselectMultiple,.menuRight-buttonList-app-buttonListselectMultiple,.header-menuTop-left-buttonList-app-buttonListselectMultiple,.header-menuTop-left-buttonList-xsMenu-buttonList-app-buttonListselectMultiple,.header-menuTop-right-buttonList-app-buttonListselectMultiple,.header-menuTop-right-buttonList-xsMenu-buttonList-app-buttonListselectMultiple").removeClass("col-md-3").addClass("col-md-12")

            if(typeof callback =="function")
                callback()

        },500);


    }

    function htmlConstructAddNewMenu(add=true,callback){
        if(add)
            $("input:checkbox[name=htmlConstruct]:checked").each(function(){
                if($(this).val() == "left")
                    generateMenuProps("left",hasMenuTopLeft,menuTopLeftPath);
                if($(this).val() == "right")
                    generateMenuProps("right",hasMenuTopRight,menuTopRightPath);
                if($(this).val() == "menuLeft")
                    generateMenuProps("menuLeft",hasMenuLeft,menuLeftPath);
                if($(this).val() == "menuRight")
                    generateMenuProps("menuRight",hasMenuRight,menuRightPath);
                                         
            });

        if(hasMenuTopLeft)
           generateMenuProps("left",hasMenuTopLeft,menuTopLeftPath);
        if(hasMenuTopRight)
            generateMenuProps("right",hasMenuTopRight,menuTopRightPath);
        if(hasMenuLeft)
               generateMenuProps("menuLeft",hasMenuLeft,menuLeftPath);
        if(hasMenuRight)
           generateMenuProps("menuRight",hasMenuRight,menuRightPath);

        if(typeof callback =="function")
            callback();
    }

    function callSelectSortable(){
         if($("#header-menuTop-left-buttonList-app-buttonList").length != 0)
             selSortableObj.init("#header-menuTop-left-buttonList-app-buttonList");
        if($("#header-menuTop-right-buttonList-app-buttonList").length != 0)
            selSortableObj.init("#header-menuTop-right-buttonList-app-buttonList");
        if($("#header-menuTop-left-buttonList-xsMenu-buttonList-app-buttonList").length != 0) 
            selSortableObj.init("#header-menuTop-left-buttonList-xsMenu-buttonList-app-buttonList");
        if($("#header-menuTop-right-buttonList-xsMenu-buttonList-app-buttonList").length != 0) 
            selSortableObj.init("#header-menuTop-right-buttonList-xsMenu-buttonList-app-buttonList");
        if($("#menuLeft-buttonList-app-buttonList").length !=0)
            selSortableObj.init("#menuLeft-buttonList-app-buttonList");
        if($("#menuRight-buttonList-app-buttonList").length !=0)
            selSortableObj.init("#menuRight-buttonList-app-buttonList");

    }

})
</script>
