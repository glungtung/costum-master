
<?php 
    $keyTpl = "isCostumSinglePage";

    $paramsData = [
        "isCostumSinglePage" => false
    ];
    
    if( isset($this->costum["isCostumSinglePage"]) ) {
    	 $paramsData["isCostumSinglePage"] = $this->costum["isCostumSinglePage"];
    }
?>
<?php if($canEdit){ ?> 
    <a class='edit<?php echo $keyTpl ?>Params' href='javascript:;' 
        data-id='<?= $this->costum["contextId"]; ?>' 
        data-collection='<?= $this->costum["contextType"]; ?>' 
        data-path='costum'>
        <i class="fa fa-cog" aria-hidden="true"></i>&nbsp; Est-ce un costum d'une seule page ?
    </a>
<?php }?>
<script type="text/javascript">
jQuery(document).ready(function() {
    sectionDyf.<?php echo $keyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

    sectionDyf.<?php echo $keyTpl ?>Params = {
        "jsonSchema" : {    
            "title" : "Est-ce un costum d'une seule page ?",
            "icon" : "fa-cog",
            "properties" : {
                "isCostumSinglePage" : {
                    "inputType" : "checkboxSimple",
                        "label" : "Est ce que c'est un costum avec un seul page ?",
                        "params" : {
                            "onText" : "Oui",
                            "offText" : "Non",
                            "onLabel" : "Oui",
                            "offLabel" : "Non",
                            "labelText" : "accessMember"
                    },
                    "checked" : (exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.isCostumSinglePage)  ? sectionDyf.<?php echo $keyTpl ?>ParamsData.isCostumSinglePage : false)
                }
            },
            save : function (data) { 
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) { 
					tplCtx.value[k] = $("#"+k).val();
                });

                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    tplCtx.updatePartial=true;
                    tplCtx.format=true;
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#ajax-modal").modal('hide');
                        toastr.success("Bien ajouté");
                        location.reload();
                    } );
                }

            }
        }
    };

    $(".edit<?php echo $keyTpl ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?>Params,null, sectionDyf.<?php echo $keyTpl ?>ParamsData);
    });

});
</script>
