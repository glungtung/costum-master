<!-- By RAMIANDRISON Jean Dieu Donné -->
<!-- email: ramiandrison.jdd@gmail.com -->
<?php 
    $keyTpl = "typeObj";

    $paramsData = [
        "typeObj" =>  [""=>""]
    ];

    if( isset($this->costum[$keyTpl]) ) {
        $paramsData = $this->costum[$keyTpl];
    }

    if(count($paramsData)==0)
        $paramsData = ["" =>  ""];
    // $cssAnsScriptFilesModule = array(
    //         '/plugins/font-awesome/list.js',
    //       );
    //     HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->request->baseUrl);
    ?>
<?php if($canEdit){ ?> 
    <a class='edit<?php echo $keyTpl ?>Params' href='javascript:;' 
        data-id='<?= $this->costum["contextId"]; ?>' 
        data-collection='<?= $this->costum["contextType"]; ?>'
        data-key='<?php echo $keyTpl ?>' 
        data-path='costum.<?php echo $keyTpl ?>'>
        <i class="fa fa-check-square-o"></i> Formulaires
    </a>

<?php }
    $elementsArr = ["organization", "projects", "poi", "ressource", "job", "classified", "proposals"];
?>
<style>

<?php 
    foreach($paramsData as $i => $v) { ?>
        .<?php echo $keyTpl ?>.<?php echo $i ?>-typeObj:before {
            content: "<?php echo $i ?>";
        }
<?php  } ?> 



</style>

<script>
jQuery(document).ready(function() {
    sectionDyf.<?php echo $keyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    //sectionDyf.<?php //echo $keyTpl ?>element = <?php //echo json_encode($elementsArr); ?>;
    var colorOption = {
        "black" : "black",
        "blue" : "blue",
        "brown" : "brown",
        "lightblue" : "lightblue",
        "lightpurple" : "lightpurple",
        "darkblue" : "darkblue",
        "green" : "green",
        "orange" : "orange",
        "red" : "red",
        "yellow" : "yellow",
        "yellow-k" : "yellow-k",
        "purple" : "purple",
        "azure" : "azure",
        "pink" : "pink",
        "phink" : "phink",
        "dark" : "dark",
        "green-k" : "green-k",
        "red-k" : "red-k",
        "blue-k" : "blue-k",
        "nightblue" : "nightblue",
        "turq" : "turq",
        "green-k" : "green-k",
    };

    var props = {};
    sectionDyf.<?php echo $keyTpl ?>init = function(k){
        props[k+"-color"]= {
            "inputType" : "select",
            "label" : "Couleur",
            "options" : colorOption,
            "rules" : {
                "required" :true
            },
            value : (typeof sectionDyf.<?php echo $keyTpl ?>ParamsData !="undefined"
                && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData[k] !="undefined"
                && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData[k].color !="undefined") ?
                sectionDyf.<?php echo $keyTpl ?>ParamsData[k].color : ""
        };
        props[k+"-name"]= {
            "inputType" : "text",
            "label" : "Nom de l’element",
            "rules" : {
                "required" :true
            },
            value : (typeof sectionDyf.<?php echo $keyTpl ?>ParamsData !="undefined"
                && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData[k] !="undefined"
                && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData[k].name !="undefined") ?
                sectionDyf.<?php echo $keyTpl ?>ParamsData[k].name : ""
        };
        props[k+"-createLabel"]= {
            "inputType" : "text",
            "label" : "Label de création",
            "rules" : {
                "required" :true
            },
            value : (typeof sectionDyf.<?php echo $keyTpl ?>ParamsData !="undefined"
                 && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData[k] !="undefined"
                 && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData[k].createLabel !="undefined") ?
                sectionDyf.<?php echo $keyTpl ?>ParamsData[k].createLabel : ""
        };
        props[k+"-icon"]= {
            "inputType" : "select",
            "label" : "icon",
            "options" : fontAwesome,
            "rules" : {
                "required" :true
            },
            value : (typeof sectionDyf.<?php echo $keyTpl ?>ParamsData !="undefined"
                 && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData[k] !="undefined"
                 && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData[k].icon !="undefined") ?
                sectionDyf.<?php echo $keyTpl ?>ParamsData[k].icon : ""
        };
        /*props[k+"-add"]= {
            "inputType" : "select",
            "label" : "add",
            "options" : {
                "onlyAdmin" : "onlyAdmin",
                "true" : "libre"
            },
            value : (typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData !="undefined"
                && typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData[k] !="undefined"
                && typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData[k].add !="undefined") ?
                sectionDyf.<?php //echo $keyTpl ?>ParamsData[k].add : ""
        };*/
        props[k+"-add"]= {
            "inputType" : "hidden",
            value : true
        };
    }

    sectionDyf.<?php echo $keyTpl ?>elementClassArray = function(paramsData,typeObjName = null){
        $.each(paramsData,function(k,v){
            sectionDyf[k+"<?php echo $keyTpl ?>"]=[];
            mylog.log("andana22",k," et ",sectionDyf[k+"<?php echo $keyTpl ?>"]);
            //sectionDyf.<?php //echo $keyTpl ?>init(k);
        })
    }

    sectionDyf.<?php echo $keyTpl ?>PushToClassArray = function(){
        $.each( sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) {
            var kk= k.split("-");
            $.each(sectionDyf.<?php echo $keyTpl ?>ParamsData,function(kd,vd){
                  mylog.log('kd',kd,kk[0]);
                if(kk[0] == kd)
                    sectionDyf[kd+"<?php echo $keyTpl ?>"].push('.'+k+val.inputType);
            });                   
        });
        
    }

    sectionDyf.<?php echo $keyTpl ?>select =  
    '<select class="form-control" id="typeObjName" name="typeObjName">'+
        '<option value="organization">organization</option>'+
        '<option value="projects">Projet</option>'+
        '<option value="poi">Point d\'intérêt</option>'+
        '<option value="events">événement</option>'+
        '<option value="ressource">ressource</option>'+
        '<option value="job">job</option>'+
        '<option value="classified">Annonce</option>'+
        '<option value="proposals">Proposition</option>'+
    '</select>';

    sectionDyf.<?php echo $keyTpl ?>addForm =  
        '<form id="appForm" class="form" role="form">'+
            '<div class="form-group">'+
              '<label for="typeObjName">Choisir un formulaire</label>'+
              sectionDyf.<?php echo $keyTpl ?>select+
            '</div>'+
        '</form>';


     $.each(sectionDyf.<?php echo $keyTpl ?>ParamsData,function(k,v){
        if(k != "")
            sectionDyf.<?php echo $keyTpl ?>init(k);
     });
        sectionDyf.<?php echo $keyTpl ?>Params = {
            "jsonSchema" : {    
                "title" : "Personnaliser les formulaires",
                "icon" : "fa-cog",
                "properties" : props,
                save : function (data) { 
                    tplCtx.value = {};
                    $.each( sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) { 
                        if($("#"+k).parent().parent().data("activated")!=false){
                            var kk= k.split("-").join("][");
                            tplCtx.value[kk] = $("#"+k).val();
                        }
                    });

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        tplCtx.updatePartial=true;
                        tplCtx.format=true;
                        dataHelper.path2Value( tplCtx, function(params) { 
                            $("#ajax-modal").modal('hide');
                            toastr.success("Bien ajouté");
                            location.reload();
                        } );
                    }
                }
            }
        };

        $(".edit<?php echo $keyTpl ?>Params").off().on("click",function() {
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = $(this).data("path");
            var existTypeObj = Object.keys(sectionDyf.<?php echo $keyTpl ?>ParamsData).join(", ");

        bootbox.dialog({
            title: "<h5 class='text-success text-center'>Les formulaires ajoutés</h5>"+
                   "<h6 class='text-danger text-bold text-center'>"+existTypeObj+"</h6>",
            message: sectionDyf.<?php echo $keyTpl ?>addForm,
            buttons: {
                cancle: {
                   label: "Annuler",
                   className: 'btn-danger',
                },
                noclose: {
                    label: "Gérer existants",
                    className: 'bg-dark',
                    callback: function(){
                        if(exists(sectionDyf.<?php echo $keyTpl ?>ParamsData[""]))
                            delete sectionDyf.<?php echo $keyTpl ?>ParamsData[""];
                        mylog.log("aaaaaa",sectionDyf.<?php echo $keyTpl ?>ParamsData);
                        sectionDyf.<?php echo $keyTpl ?>elementClassArray(sectionDyf.<?php echo $keyTpl ?>ParamsData);
                         dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?>Params,null, sectionDyf.<?php echo $keyTpl ?>ParamsData);
                        sectionDyf.<?php echo $keyTpl ?>PushToClassArray();
                        $.each(sectionDyf.<?php echo $keyTpl ?>ParamsData,function(k,v){
                            wrapToDiv(sectionDyf[k+"<?php echo $keyTpl ?>"],"<?php echo $keyTpl ?>",k+"-typeObj",4,"",tplCtx.path,k);
                        })
                    }
                },
                ok: {
                    label: "Créer nouveau",
                    className: 'bg-green-k',
                    callback: function(){
                        var typeObjName = $('#typeObjName').val();
                        if(typeObjName != ''){
                            if(exists(sectionDyf.<?php echo $keyTpl ?>ParamsData[""]))
                                delete sectionDyf.<?php echo $keyTpl ?>ParamsData[""];
                            sectionDyf.<?php echo $keyTpl ?>ParamsData[typeObjName] = {};
                            sectionDyf.<?php echo $keyTpl ?>init(typeObjName);
                            sectionDyf.<?php echo $keyTpl ?>elementClassArray(sectionDyf.<?php echo $keyTpl ?>ParamsData);
                            dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?>Params,null, sectionDyf.<?php echo $keyTpl ?>ParamsData);
                            sectionDyf.<?php echo $keyTpl ?>PushToClassArray();
                            $.each(sectionDyf.<?php echo $keyTpl ?>ParamsData,function(k,v){
                                wrapToDiv(sectionDyf[k+"<?php echo $keyTpl ?>"],"<?php echo $keyTpl ?>",k+"-typeObj",4,"",tplCtx.path,k);
                            })
                             $('body').append('<style>.<?php echo $keyTpl ?>.'+typeObjName+'-typeObj:before {content : '+"\'"+typeObjName+"\'"+'}</style>');
                        }
                    }
                }
            }
        });
            
        });

    })
</script>
