<style>
/*	.modal{
      z-index: 999999 !important;
    }*/
    #menuJson{
      z-index: 100001;margin-top: 59px;
    }
    .list-group-item{
      padding-left: 50px !important;
    }
    .list-group {
        padding-left: 0;
        margin-bottom: 0px !important;
    }
    .list-group-item:last-child {
        border-bottom: 0 !important;
    }
    .wrapped{
        border: 1px solid black;
        padding: 40px 15px;
        margin-top: 24px;
    }
    .wrapped:before {
        position: absolute;
        top: -14px;
        transform: translate(-50%);
        background: white;
        font-size: medium;
        font-weight: bold;
        padding: 0 15px;
    }
    .wrapped .fa-chevron-down{
        display: none !important;
    }
	.col-status{
	  position: absolute;
	  background: #f9f9f9;
	  /*width: 77px;*/
	  top: -14px;
	  left: -10px;
	  /*transform: rotate(-48deg);*/
	}
	.col-status .btn-state,.col-status .btn-activator{
	  box-shadow: 0px 0px 7px black;
	  /*width: 100%;*/
	}
	.info-state{
	position: absolute;
	top: -14px;
	right: 6px;
	background: white;
	}
	.pulse-button {
	  box-shadow: 0 0 0 0 rgba(99, 239, 83, 0.7);
	  cursor: pointer;
	  -webkit-animation: pulse 1.25s infinite cubic-bezier(0.66, 0, 0, 1);
	  -moz-animation: pulse 1.25s infinite cubic-bezier(0.66, 0, 0, 1);
	  -ms-animation: pulse 1.25s infinite cubic-bezier(0.66, 0, 0, 1);
	  animation: pulse 1.25s infinite cubic-bezier(0.66, 0, 0, 1);
	}
.pulse-button:hover 
{
  -webkit-animation: none;-moz-animation: none;-ms-animation: none;animation: none;
}

@-webkit-keyframes pulse {to {box-shadow: 0 0 0 45px rgba(232, 76, 61, 0);}}
@-moz-keyframes pulse {to {box-shadow: 0 0 0 45px rgba(232, 76, 61, 0);}}
@-ms-keyframes pulse {to {box-shadow: 0 0 0 45px rgba(232, 76, 61, 0);}}
@keyframes pulse {to {box-shadow: 0 0 0 45px rgba(232, 76, 61, 0);}}

</style>

<div class="modal co-scroll" id="menuJson">
    <div class="modal-dialog /modal-sm/">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center">PERSONNALISEZ <?php echo $this->costum["title"] ?></h4>
        </div>
        <div class="modal-body ask-developer">
          <h2 class="text-center">Je suis un développeur ?</h2>
          <p class="text-center text-bold">
            <input type='checkbox' name='forgetAskIfDeveloper' id='forgetAskIfDeveloper'>  Ne plus repéter !
          </p>
          <p class="text-center margin-top-35">
            <button class="btn bg-dark text-white developer" data-value="true">OUI</button>
            <button class="btn bg-dark text-white developer" data-value="false">NON</button>
          </p>
        </div>
        <div class="modal-body all-menu-json" style="display: none">
            <div class="panel-group">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title" data-toggle="collapse" data-dismiss="modal" data-toggle="collapse" data-dismiss="modal" href="#collapse1">
                      <?php echo $this->renderPartial("costum.views.tpls.jsonEditor.blocFavIconImgTitle", array("canEdit" => $canEdit)); ?>
                  </h4>
                </div>
              </div>

               <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title" data-toggle="collapse" data-dismiss="modal" data-toggle="collapse" data-dismiss="modal" href="#collapse1">
                      <?php // echo $this->renderPartial("costum.views.tpls.jsonEditor.colors", array("canEdit" => $canEdit)); ?>
                      <?php echo $this->renderPartial("costum.views.tpls.jsonEditor.css.color", array("canEdit" => $canEdit)); ?>
                  </h4>
                </div>
              </div>               

              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title" data-toggle="collapse" data-dismiss="modal">
                    <?php echo $this->renderPartial("costum.views.tpls.jsonEditor.admin", array("canEdit" => $canEdit)); ?>
                  </h4>
                </div>
              </div>

              <div class="panel panel-default only-developer">
                <div class="panel-heading">
                  <h4 class="panel-title" data-toggle="collapse" data-dismiss="modal">
                    <?php echo $this->renderPartial("costum.views.tpls.jsonEditor.adminPanel", array("canEdit" => $canEdit)); ?>
                  </h4>
                </div>
              </div>

<!--          <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title" data-toggle="collapse" data-dismiss="modal">
                    <?php // echo $this->renderPartial("costum.views.tpls.jsonEditor.mailsConfig", array("canEdit" => $canEdit)); ?>
                  </h4>
                </div>
              </div> -->

<!--               <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title" data-toggle="collapse" data-dismiss="modal">
                     <?php //echo $this->renderPartial("costum.views.tpls.jsonEditor.preferences", array("canEdit" => $canEdit)); ?>
                  </h4>
                </div>
              </div>

              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title" data-toggle="collapse" data-dismiss="modal">
                    <?php //echo $this->renderPartial("costum.views.tpls.jsonEditor.class", array("canEdit" => $canEdit)); ?>
                  </h4>
                </div>
              </div> -->

              <div class="panel panel-default only-developer">
                <div class="panel-heading">
                  <h4 class="panel-title" data-toggle="collapse" data-dismiss="modal">
                    <?php echo $this->renderPartial("costum.views.tpls.jsonEditor.js", array("canEdit" => $canEdit)); ?>
                  </h4>
                </div>
              </div>

              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <i class="fa fa-css3"></i> <a data-toggle="collapse" href="#collapsecss">Apparence(CSS)</a>
                  </h4>
                </div>
                <div id="collapsecss" class="panel-collapse collapse">
                  <ul class="list-group">
                      <li class="list-group-item" data-toggle="collapse" data-dismiss="modal">
                        <?php echo $this->renderPartial("costum.views.tpls.jsonEditor.isCostumSinglePage", array("canEdit" => $canEdit)); ?>
                      </li>
                      <li class="list-group-item" data-toggle="collapse" data-dismiss="modal">
                        <?php echo $this->renderPartial("costum.views.tpls.jsonEditor.css.urlAndFont", array("canEdit" => $canEdit)); ?>
                      </li>
                      <li class="list-group-item" data-toggle="collapse" data-dismiss="modal">
                        <?php echo $this->renderPartial("costum.views.tpls.jsonEditor.css.color", array("canEdit" => $canEdit)); ?>
                      </li>
                      <li class="list-group-item" data-toggle="collapse" data-dismiss="modal">
                        <?php echo $this->renderPartial("costum.views.tpls.jsonEditor.css.loaderProgress", array("canEdit" => $canEdit)); ?>
                      </li>

                      <?php if(isset($this->costum["htmlConstruct"]["header"]["menuTop"])){ ?>
                        <li class="list-group-item" data-toggle="collapse" data-dismiss="modal">
                            <?php echo $this->renderPartial("costum.views.tpls.jsonEditor.css.menu", array("canEdit" => $canEdit,"type" => "menuTop")); ?>
                        </li>
                      <?php } ?>

                      <?php if(isset($this->costum["htmlConstruct"]["menuLeft"])){ ?>
                        <li class="list-group-item" data-toggle="collapse" data-dismiss="modal">
                            <?php echo $this->renderPartial("costum.views.tpls.jsonEditor.css.menu", array("canEdit" => $canEdit,"type" => "menuLeft")); ?>
                        </li>
                      <?php } ?>

<!--                       <li class="list-group-item" data-toggle="collapse" data-dismiss="modal">
                          <?php //echo $this->renderPartial("costum.views.tpls.jsonEditor.css.menuApp", array("canEdit" => $canEdit)); ?>
                      </li> -->
                  </ul>
                </div>
              </div>
              
              <style> .app .hide-in-menu-json{ display: none}</style>
              <div class="panel panel-default app">
                <div class="panel-heading">
                  <h4 class="panel-title" data-toggle="collapse" data-dismiss="modal" data-toggle="collapse" data-dismiss="modal" href="#collapse1">
                       <i class="fa fa-desktop" aria-hidden="true"></i> <?php echo $this->renderPartial("costum.views.tpls.jsonEditor.app.app2", array("canEdit" => $canEdit)); ?>
                  </h4>
                </div>
              </div> 

              <div class="panel panel-default app">
                <div class="panel-heading">
                  <h4 class="panel-title" data-toggle="collapse" data-dismiss="modal" data-toggle="collapse" data-dismiss="modal" href="#collapse1">
                       <?php echo $this->renderPartial("costum.views.tpls.jsonEditor.htmlConstruct.htmlConstruct", array("canEdit" => $canEdit)); ?>
                  </h4>
                </div>
              </div>

              <div class="panel panel-default app">
                <div class="panel-heading">
                  <h4 class="panel-title" data-toggle="collapse" data-dismiss="modal" data-toggle="collapse" data-dismiss="modal" href="#collapse1">
                       <?php echo $this->renderPartial("costum.views.tpls.jsonEditor.htmlConstruct.element", array("canEdit" => $canEdit)); ?>
                  </h4>
                </div>
              </div>

              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title" data-toggle="collapse" data-dismiss="modal">
                    <?php echo $this->renderPartial("costum.views.tpls.jsonEditor.typeObj.typeObj3", array("canEdit" => $canEdit)); ?>
                  </h4>
                </div>
              </div>
              
            </div>
        </div>
      </div>
      
    </div>
</div> 

<?php echo $this->renderPartial("costum.views.tpls.jsonEditor.partial.logo", array("canEdit" => $canEdit)); ?>

<script>
  var isDeveloper = false;
	var poiFilters = {};
		$.each(poi.filters,function(k,v){
		    poiFilters[k] = typeof tradCategory[k] != "undefined" ? tradCategory[k] : k;
	})
  //communecter color
  var colorCommunecter = ["black","blue","brown","lightblue","lightpurple","darkblue","green","orange","red","yellow","yellow-k","purple","azure","pink","phink","dark","green-k","red-k","blue-k","nightblue","turq","green-poi"];

  //convert text type to color type
	function wrapToDiv(arrayClass,parentClass,classe,colMd,offset="",path=null,secondPath=null,pathSpecialChar=null){
      var parentClasses = '.'+parentClass+'.'+classe;
	    var arrayClassToString = arrayClass.join(",").split("[").join("\[").split("]").join("\]");
	    $(arrayClassToString).wrapAll("<div class='col-xs-12 wrapped "+parentClass+" "+classe+"'></div>");
	    $(arrayClassToString).removeClass('form-group').addClass('col-md-'+colMd+' wrapped-child col-md-offset-'+offset+' col-xs-12');
	    $(".wrapped-child[class$='hidden'],.wrapped-child[class*='hidden']").hide();

	    if (path != null && path !=="" && secondPath != null && secondPath !="" ) {
	      $(parentClasses).prepend(
	        '<div class="col-status">'+   
	            '<button type="button" class="btn bg-red text-white btn-sm btn-state" data-state="true"><i class="fa fa-times"></i></button>'+
	        '</div>'+
	        '<h6 class="text-danger info-state"></h6>'
	        );

	            $(parentClasses+' .btn-state').on('click',function(){
	                bootbox.confirm({
	                    message: "<h5 class='text-danger text-center'>Supprimer : "+path+"."+secondPath+" ?</h5>",
	                    buttons: {
	                        confirm: {
	                            label: 'OK',
	                            className: 'btn-success'
	                        },
	                        cancel: {
	                            label: 'Annuler',
	                            className: 'btn-danger'
	                        }
	                    },
	                    callback: function (result) {
	                      if(result === true){
	                        $(parentClasses).remove();
	                        var tpl={};
	                        tpl.path = path+"."+((pathSpecialChar != null)?pathSpecialChar:"")+secondPath;
	                        tpl.id = costum.contextId;
	                        tpl.collection = costum.contextType;
                          tpl.value = null;
                          tpl.format = true;
                          if(tpl.path == "costum.htmlConstruct.header.menuTop.left"){
                            tpl.path = "costum.htmlConstruct.header.menuTop"
                            tpl.value = {left : false};
                          }
                          if(tpl.path == "costum.htmlConstruct.header.menuTop.right"){
                            tpl.path = "costum.htmlConstruct.header.menuTop"
                            tpl.value = {right : false};
                          }
	                        dataHelper.path2Value( tpl, function(params) { 
                            if(params.result)
	                             toastr.success("Bien supprimé");
	                            //location.reload();
	                        } );
	                      }
	                    }
	                });
	            });
	            
	            if(typeof jsonHelper.getValueByPath(sectionDyf[parentClass+"ParamsData"], secondPath) == "undefined" || jsonHelper.getValueByPath(sectionDyf[parentClass+"ParamsData"], secondPath) == ""  ){
	                  $(parentClasses).children().hide();
	                  $(parentClasses+' .info-state').text("Pas encore ajouté");
	                  $(parentClasses).attr("data-activated","false");
	                  $(parentClasses+' .col-status').append(
	                      '<button type="button" class="btn bg-red text-white btn-sm btn-activator" data-state="true"><i class="fa fa-lock"></i></button>'
	                  );
	                  $(parentClasses+' .col-status').show();
	                  $(parentClasses+' .btn-state').hide();

	                  $(parentClasses+' .col-status .btn-activator').click(function(){
                      if($(parentClasses).attr("data-activated") == "false"){
	                        $(parentClasses).attr("data-activated","true");
	                        $(this).removeClass('bg-red').addClass('bg-green-k').html("<i class='fa fa-unlock'></i>");
	                        $(parentClasses).children().show();
                      }else if($(parentClasses).attr("data-activated") == "true"){
                          $(parentClasses).attr("data-activated","false");
                          $(parentClasses).children().hide();
                           $(this).removeClass('bg-green-k').addClass('bg-red').html("<i class='fa fa-lock'></i>").show();
                           $(parentClasses+' .col-status').show();
                      }
	                  })
	          }
	    }
	}

  var  selSortableObj = {
    init : function(myselect){
            $(myselect).select2({
                placeholder: 'Selectioner et ordonner votre menus'
            }).on("select2:select", function (evt) {
                    var id = evt.params.data.id;
                    var element = $(this).children("option[value="+id+"]");
                    selSortableObj.moveElementToEndOfParent(element);
                    $(this).trigger("change");
            });

            var ele=$(myselect).parent().find("ul.select2-choices");
            ele.sortable({
                containment: 'parent',
                update: function() {
                    selSortableObj.orderSortedValues(myselect);
                    console.log("eo ary hoe"+$(myselect).val())
                }
            });
    },

    orderSortedValues : function(myselect) {
    var value = ''
        $(myselect).parent().find("ul.select2-choices").children("li").children("div").each(function(i, obj){
            var element = $(myselect).children('option').filter(function () { 
              return $(this).html() == $(obj).text() });
            selSortableObj.moveElementToEndOfParent(element)
        });
    },

    moveElementToEndOfParent : function(element) {
        var parent = element.parent();
        element.detach();
        parent.append(element);
    }    
  };




  $(function(){
    if(localStorage.getItem("forgetAskIfDeveloper") == "true"){
        $('.ask-developer').remove();
        $('.all-menu-json').show();
    }
    if(localStorage.getItem("isDeveloper") == "true")
        isDeveloper = true;
    else if(localStorage.getItem("isDeveloper") == "false"){
        isDeveloper = false;
        $('.only-developer').remove();
    }

    $(".developer").click(function(){
      if($("#forgetAskIfDeveloper").prop('checked') == true)
        localStorage.setItem("forgetAskIfDeveloper","true");
      else
        localStorage.removeItem("forgetAskIfDeveloper");
      //$('.all-menu-json').hide();
      if($(this).data("value")==false){
        localStorage.setItem("isDeveloper", "false");
        $('.only-developer,.ask-developer').remove();
        $('.all-menu-json').show();
      }else if($(this).data("value")==true){
        isDeveloper = true;
        localStorage.setItem("isDeveloper", "true");
        $('.ask-developer').hide();
        $('.all-menu-json').show();
      }
    })
  })
</script>



