<?php 
    $keyTpl = "adminPanel";

    $paramsData = [
        "adminPanel" => []
    ];
    
    if( isset($this->costum["htmlConstruct"][$keyTpl]) ) {
        foreach($paramsData as $i => $v) {
            if(isset($this->costum["htmlConstruct"][$keyTpl])) 
                $paramsData[$i] =  $this->costum["htmlConstruct"][$keyTpl];   
        }
    }
?>
<?php if($canEdit){ ?> 
    <a class='edit<?php echo $keyTpl ?>Params' href='javascript:;' 
        data-id='<?= $this->costum["contextId"]; ?>' 
        data-collection='<?= $this->costum["contextType"]; ?>' 
        data-path='costum.htmlConstruct'>
        <i class="fa fa-cog" aria-hidden="true"></i> Portail d'administration <span class="text-danger">(Developer only)</span>
    </a>
<?php }?>

<style>
    .<?php echo $keyTpl ?>.adminPanel:before {
        content: "panneaud d'administration (Developer only)";
        color: red;
    }
    .<?php echo $keyTpl ?>.adminPanelMenu:before {
        content: "Menu (Developer only)";
        color: red;
    }
</style>
<script type="text/javascript">
jQuery(document).ready(function() {
    sectionDyf.<?php echo $keyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

    sectionDyf.<?php echo $keyTpl ?>Params = {
        "jsonSchema" : {    
            "title" : "Panel d'administration",
            "icon" : "fa-cog",
            "properties" : {
                "adminPanel-js" : {
                    "inputType" : "checkboxSimple",
                        "label" : "Javascript",
                        "params" : {
                            "onText" : "Oui",
                            "offText" : "Non",
                            "onLabel" : "Oui",
                            "offLabel" : "Non",
                            "labelText" : "accessMember"
                    },
                    "checked" : (exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.adminPanel) && exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.adminPanel.js)) ? sectionDyf.<?php echo $keyTpl ?>ParamsData.adminPanel.js : true
                },
                "adminPanel-add" : {
                    "inputType" : "checkboxSimple",
                        "label" : "Récupérer les formulaires des éléments",
                        "params" : {
                            "onText" : "Oui",
                            "offText" : "Non",
                            "onLabel" : "Oui",
                            "offLabel" : "Non",
                            "labelText" : "accessMember"
                    },
                    "checked" : (exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.adminPanel) && exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.adminPanel.add)) ? sectionDyf.<?php echo $keyTpl ?>ParamsData.adminPanel.add : true
                },
            },
            save : function (data) { 
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) { 
                    var kk= k.split("-").join("][");
                    if($("#"+k).parent().parent().data("activated")!=false){
                        if($("#"+k).val() != "false" && $("#"+k).val() != false )
                            tplCtx.value[kk] = $("#"+k).val();
                        else
                            tplCtx.value[kk] = false
                    }   
                });

                if(typeof tplCtx.value == "undefined" || Object.keys(tplCtx.value).length == 0)
                    toastr.error('value cannot be empty!');
                else {
                    tplCtx.updatePartial=true;
                    tplCtx.format=true;
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#ajax-modal").modal('hide');
                        toastr.success("Bien ajouté");
                        location.reload();
                    } );
                }

            }
        }
    };

    var adminPanel<?= $keyTpl ?> = {
      "directory": {
        "label": "Directory",
        "super": true,
        "class": "text-yellow",
        "view": "directory",
        "icon": "user"
      },
      "reference": {
        "label": "Reference",
        "sourceKey": true,
        "init": [
          "organizations",
          "events",
          "projects"
        ],
        "class": "text-azure",
        "view": "reference",
        "show": false,
        "icon": "creative-commons"
      },
      "community": {
        "label": "Community",
        "show": false,
        "costumAdmin": true,
        "class": "letter-blue",
        "view": "community",
        "icon": "group"
      },
      "converter": {
        "label": "Converter",
        "class": "text-green",
        "view": "import",
        "icon": "upload"
      },
      "import": {
        "label": "IMPORT DATA",
        "class": "letter-blue",
        "view": "addData",
        "icon": "plus"
      },
      "mails": {
        "label": "Mails simulator",
        "costumAdmin": true,
        "class": "text-purple",
        "view": "mailslist",
        "icon": "at"
      },
      "log": {
        "label": "LOG",
        "super": true,
        "class": "text-dark",
        "view": "log",
        "icon": "list"
      },
      "moderation": {
        "label": "MODERATION",
        "class": "text-red",
        "view": "moderate",
        "icon": "check"
      },
      "statistic": {
        "label": "Statitics",
        "class": "text-orange",
        "view": "statistic",
        "icon": "bar-chart"
      },
      "mailerror": {
        "label": "Mail error",
        "super": true,
        "class": "text-yellow",
        "view": "mailerror",
        "icon": "envelope"
      },
      "notsendmail": {
        "label": "Not send mail",
        "super": true,
        "class": "text-red",
        "view": "notsendmail",
        "icon": "envelope"
      }
    }
    $.each(adminPanel<?= $keyTpl ?>,function(k,v){
        sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties["adminPanel-menu-"+k] = {
            "inputType" : "checkboxSimple",
                "label" : v["label"],
                "params" : {
                    "onText" : "Oui",
                    "offText" : "Non",
                    "onLabel" : "Oui",
                    "offLabel" : "Non",
                    "labelText" : v["label"]
            },
            "checked" : (exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.adminPanel) && exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.adminPanel.menu) && exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.adminPanel.menu[k])) ? sectionDyf.<?php echo $keyTpl ?>ParamsData.adminPanel.menu[k] : true
        }
    })



    $(".edit<?php echo $keyTpl ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?>Params,null, sectionDyf.<?php echo $keyTpl ?>ParamsData);
        var arrAdminPanel = [],arrAdminPanelMenu=[];
        $.each( sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) {
            var kk= k.split("-");
            if( kk[0] =="adminPanel" && (kk[1] == "add" || kk[1] == "js"))
                arrAdminPanel.push('.'+k+val.inputType);
            else if(kk[1] == "menu")
                arrAdminPanelMenu.push('.'+k+val.inputType);
        });
        // wrapToDiv(list of class[],"parent class","sub class of parent","col-md-x","col-md-offset-x","tplCtx.path","buttonList.app");
        wrapToDiv(arrAdminPanel,"<?php echo $keyTpl ?>","adminPanel",4,"",tplCtx.path,'adminPanel.add');
        wrapToDiv(arrAdminPanelMenu,"<?php echo $keyTpl ?>","adminPanelMenu",4,"",tplCtx.path,'adminPanel.menu');
    });

});
</script>
