<?php
    function getData($spot, $start_date, $end_date=NULL){
        if($end_date !== NULL){
            $where = [
                "date"=> [
                    '$gte'=> strtotime($start_date),
                    '$lte'=> strtotime($end_date)
                ]
            ];
        }else{
            $where = [
                "date" => strtotime($start_date)
            ];
        }
        $where["spot"] = $spot;
        $result = PHDB::find('--meteolamer-forecast', $where);
        return $result;
    }

    function getSpotByName($name){
        return PHDB::findOne('--meteolamer-spots',  ["name" => $name], ["label"=>1]);
    }
    
    $spotName = Yii::app()->getRequest()->getQuery('spot');
    $spotName = $spotName?$spotName:'home';
    $spot = getSpotByName($spotName);

    $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;
    $times = ["00","06","12","18"];

    $results = getData($spotName, "2020-10-26", "2020-11-01");
    $weekly_data = [];
    foreach($results as $result){
        $weekly_data[] = $result;
    }
?>
<?php
    for($i=0;$i<7;$i++){
        foreach($times as $time){
            $waveData = $weekly_data[$i]["data"]["h".$time]["wave"];
?>
    <link rel="preload" href="http://www.meteolamer.re/dev/<?= str_replace("./","",$waveData["map"]) ?>" as="image">
<?php }} ?>

<style>
    button{
        outline: none !important;
    }

    .meteolamer-wave-header{
        width:100%;
        display:flex;
        justify-content:space-between;
        align-items:center;
    }
    .meteolamer-wave-header h1{
        font-size: 34px;
        color:#708c8c;
    }
    .meteolamer-wave-container{
        display:flex;
    }
    .wave-map-container{
        width:420px;
        height:420px;
        position:relative;
        background-size:cover;
        transition: .4s;
        border-radius:6px;
    }
    .wave-map-container img{
        width:100%;
        height:100%;
        position:absolute;
        border-radius:6px;
    }
    .wave-map-container img:last-child{
        z-index:2;
    }
    .wave-map-baseline-actions{
        width:100%;
        height:100%;
        position:absolute;
        z-index:3;
        border-radius:6px;
    }
    .wave-table-container{
        width:calc(100% - 420px);
    }
    .wave-table-container table{
        width: 100%;
        display: table;
        border-collapse: separate;
        box-sizing: border-box;
        text-indent: initial;
        border-spacing: 2px;
        border-color: grey;
    }
    .wave-table-container table th{
        text-align: center;
        padding: 2px;
        font-size: 11px;
        color: #707070;
    }
    .wave-table-container table th span{
        display: inline-block;
        width: 100%;
        padding: 4px 1px;
        color: #344848;
        border: 1px solid #eaeaea;
        border-radius: 4px;
        background-color: #c1d7d7;
        white-space:nowrap;
    }
    .wave-table-container table tbody{
        border-top: 15px solid transparent;
        background: #f4f8f8;
    }
    .wave-table-container table tr td:first-child{
        background:white;
    }

    .wave-table-container table tbody tr{
        height:30.75px;
    }

    .wave-table-container table tbody tr:nth-child(6), .wave-table-container table tbody tr:nth-child(7){
        height: 56px;
    }

    .wave-tr-heght-bar ul{
        height: 120px;
        display: flex;
        flex-direction: column;
        justify-content: space-between;
        text-align:right;
    }
    .wave-tr-heght-bar ul li{
        font-size: 12px;
        list-style: none;
        color: #707070;
        position: relative;
        line-height: 0;
    }
    .wave-td-height-bar{
        position:relative;
        overflow-y:hidden;
    }
    .wave-td-height-bar div{
        position:absolute;
        bottom:0;
        margin-left: auto;
        margin-right: auto;
        left: 0;
        right: 0;
        width: calc(100% - 4px);
        margin: 0px 2px;
    }
    .wave-td-height-bar .wave-bar-height{
        background: #ed7322;
    }
    .wave-td-height-bar .wave-bar-swellheight{
        background:#f7ce3b;
    }

    .wave-tr-title span{
        display:block;
        padding: 4px;
        font-size:12px;
        color:#666666;
    }
    .wave-tr-hsig span, .wave-tr-period span{
        display:block;
        font-size:14px;
        padding: 2px 4px;
        text-align:center;
    }

    .wave-tr-tide p{
        padding:0;
        margin:0;
        font-size:14px;
        padding: 2px 4px;
        text-align:center;
    }

    .wave-td-data{
        cursor:pointer;
        transition: .3s;
    }

    .wave-td-data.active{
        background: #e4e6e6;
    }

    .data-title{
        text-align:right;
        width:40px;
    }

    .data-title span{
        text-align:right;
        font-size: 12px;
        white-space: wrape;
        color:#707070;
    }

    /* map baseline actions styles =============> */
    .btn-map-zoom{
        position: absolute;
        border: none;
        width: 45px;
        height: 45px;
        border-radius: 100%;
        font-size: 1.8em;
        text-align: center;
        line-height: 45px;
        left: 7px;
        top: 5px;
        transition: .3s;
    }
    .btn-map-zoom:hover{
        background-color: #0093d0;
        color: white;
    }
    .btn-map-spot-m2{
        position: absolute;
        border: none;
        width: 88px;
        height: 88px;
        text-align: center;
        line-height: 88px;
        opacity: 0;
        cursor:pointer;
        transition: .3s;
        background-color: #0093d0;
        color: white;
    }
    .btn-map-spot-m2:hover{
        opacity: .95;
    }
    .btn-map-spot-m0{
        font-weight: bold;
        position: absolute;
        border: none;
        text-align: center;
        transition: .3s;
        font-size: 12px;
        border-radius: 50px;
        background-color: #0093d0;
        color: white;
        opacity:0;
    }
    .btn-map-spot-m0:hover{
        opacity: 1;
    }
    .btn-map-spot-navigation {
        position: absolute;
        width: 38px;
        border: none;
        height: 28px;
        transition:.3s;
    }
    .btn-map-spot-navigation-up{
        border-radius: 0px 0px 10px 10px;
    }
    .btn-map-spot-navigation-down{
        border-radius: 10px 10px 0px 0px;
    }
    .btn-map-spot-navigation-top{
        border-radius: 0px 0px 10px 10px;
    }
    .btn-map-spot-navigation-right{
        width: 30px;
        height: 36px;
        border-radius: 10px 0px 0px 10px;
    }
    .btn-map-spot-navigation-left{
        width: 30px;
        height: 36px;
        border-radius: 0px 10px 10px 0px;
    }
    .btn-map-spot-navigation:hover{
        background-color: #0093d0;
        color: white;
    }
    /* <============== map baseline actions styles */

    @media only screen and (max-width: 1300px){
        .meteolamer-wave-container{
            flex-direction:column;
        }
        .wave-table-container{
            width:100%;
            overflow-x:auto;
        }

        .wave-map-container{
            position: relative;
            width: 100%;
            height: 0;
            padding-bottom: 100%;
            margin-bottom:20px;
        }

        .meteolamer-wave-header{
            display:flex;
            flex-direction:column;
            align-items: flex-start;
            margin-bottom: 15px;
        }

        .meteolamer-wave-header h1:first-child{
            font-size:20px;
            font-weight: bold;
        }

        .meteolamer-wave-header h1{
            font-size:16px;
            font-weight: 500;
            margin: 5px;
        }
    }
</style>

<div class="container">
    <div class="meteolamer-wave-header">
        <h1 class="meteolamer-active-spot-name"><?= $spot["label"] ?></h1>
        <h1 class="meteolamer-active-time"></h1>
    </div>
    <div class="meteolamer-wave-container">
        <div class="wave-map-container">
            <img src="" class="wave-map">
            <img src="" class="wave-map-baseline">
            <div class="wave-map-baseline-actions"></div>
        </div>
        <div class="wave-table-container">
            <table id="table-weekly-forecast">
                <thead>
                    <tr>
                        <th colspan="4"></th>
                        <?php setlocale(LC_TIME,'fr_FR.utf8','fra') ?>
                        <?php foreach($weekly_data as $data) { ?>
                        <th colspan="4">
                            <span><?= strftime('%A %d', $data["date"]); ?></span>
                        </th>
                        <?php } ?>
                    </tr>
                    <tr>
                        <th colspan="4"></th>
                        <?php  for($i=0; $i<7;$i++) { ?>
                            <th>00</th>
                            <th>06</th>
                            <th>12</th>
                            <th>18</th>
                        <?php } ?>
                    </tr>
                </thead>
                <tbody>
                    <tr class="wave-tr-heght-bar">
                        <td colspan="4">
                            <ul>
                                <li>4 -</li>
                                <li>3 -</li>
                                <li>2 -</li>
                                <li>1 -</li>
                                <li>0 -</li>
                            </ul>
                        </td>
                        <?php 
                            for($i=0;$i<7;$i++){ 
                                foreach($times as $time){
                                    $waveData = $weekly_data[$i]["data"]["h".$time]["wave"];
                        ?>
                        <td 
                            class="wave-td-height-bar wave-td-data wave-td-data-day<?=$i?>-h<?=$time?>"
                            data-day="<?=$i?>" 
                            data-time="h<?=$time?>"
                            data-map="<?= $waveData["map"] ?>"
                            data-imap="<?= $waveData["imap"] ?>"
                            data-date="<?= $weekly_data[$i]["date"] ?>"
                        > 
                            <div class="wave-bar-height" style="height:<?= $waveData["height"]*30 ?>px;"></div>
                            <div class="wave-bar-swellheight" style="height:<?= $waveData["swellHeight"]*30 ?>px;"></div>
                        </td>
                        <?php }} ?>
                    </tr>
                    <tr class="wave-tr-hsig">
                        <td colspan="4" class="data-title"><span>Hsig (m)</span></td>
                        <?php for($i=0;$i<7;$i++){ 
                            foreach(["h00","h12"] as $time){
                                $waveData = $waveData = $weekly_data[$i]["data"][$time]["wave"];
                        ?>
                            <td 
                                colspan="2" 
                                class="wave-td-data wave-td-data-day<?=$i?>-<?= $time ?>"
                                data-day="<?=$i?>" 
                                data-time="<?= $time ?>"
                                data-map="<?= $waveData["map"] ?>"
                                data-imap="<?= $waveData["imap"] ?>"
                                data-date="<?= $weekly_data[$i]["date"] ?>"
                            >
                                <span><?= number_format($waveData["height"], 1) ?></span>
                            </td>
                        <?php }} ?>
                    </tr>
                    <tr>
                        <td colspan="4" class="data-title"><span>Direction</span></td>
                        <?php for($i=0;$i<7;$i++){ 
                            foreach(["h00","h12"] as $time){
                                $waveData = $waveData = $weekly_data[$i]["data"][$time]["wave"];
                        ?>
                            <td 
                                colspan="2" 
                                class="text-center wave-td-data wave-td-data-day<?=$i?>-<?= $time ?>"
                                data-day="<?=$i?>" 
                                data-time="<?= $time ?>"
                                data-map="<?= $waveData["map"] ?>"
                                data-imap="<?= $waveData["imap"] ?>"
                                data-date="<?= $weekly_data[$i]["date"] ?>"
                            >
                                <img src="<?= $assetsUrl ?>/images/meteolamer/dirwave/<?= $waveData["direction"] ?>.png" alt="">
                            </td>
                        <?php }} ?>
                    </tr>
                    <tr class="wave-tr-period">
                        <td colspan="4" class="data-title"><span>Période</span></td>
                        <?php for($i=0;$i<7;$i++){ 
                            foreach(["h00","h12"] as $time){
                                $waveData = $waveData = $weekly_data[$i]["data"][$time]["wave"];
                        ?>
                            <td 
                                colspan="2" 
                                class="wave-td-data wave-td-data wave-td-data-day<?=$i?>-<?= $time ?>"
                                data-day="<?=$i?>" 
                                data-time="<?= $time ?>"
                                data-map="<?= $waveData["map"] ?>"
                                data-imap="<?= $waveData["imap"] ?>"
                                data-date="<?= $weekly_data[$i]["date"] ?>"
                            >
                                <span><?= str_pad(round($waveData["period"]), 2, "0", STR_PAD_LEFT) ?>s</span>
                            </td>
                        <?php }} ?>
                    </tr>
                    <tr>
                        <td colspan="4" class="data-title"><span>Vent</span></td>
                        <?php for($i=0;$i<7;$i++){ 
                            foreach(["h00","h12"] as $time){
                                $waveData = $waveData = $weekly_data[$i]["data"][$time]["wave"];
                        ?>
                            <td 
                                colspan="2" 
                                class="text-center wave-td-data wave-td-data-day<?=$i?>-<?= $time ?>"
                                data-day="<?=$i?>" 
                                data-time="<?= $time ?>"
                                data-map="<?= $waveData["map"] ?>"
                                data-imap="<?= $waveData["imap"] ?>"
                                data-date="<?= $weekly_data[$i]["date"] ?>"
                            >
                                <img src="<?= $assetsUrl ?>/images/meteolamer/dirwind/<?= $waveData["windDirection"] ?>.png" alt="">
                            </td>
                        <?php }} ?>
                    </tr>
                    <tr class="wave-tr-tide">
                        <td colspan="4" class="data-title"><span>Marée haute</span></td>
                        <?php for($i=0;$i<7;$i++){
                            $waveData = $weekly_data[$i]["data"]["h00"]["wave"];
                            $tideData = $weekly_data[$i]["data"]["h00"]["tide"]["high"][0];
                        ?>
                            <td 
                                colspan="4" 
                                class="text-center wave-td-data wave-td-data-day<?=$i?>-h00"
                                data-day="<?=$i?>" 
                                data-time="h00"
                                data-map="<?= $waveData["map"] ?>"
                                data-imap="<?= $waveData["imap"] ?>"
                                data-date="<?= $weekly_data[$i]["date"] ?>"
                            >
                                <p><?= $tideData["level"] ?></p>
                                <p><?= $tideData["time"] ?></p>
                            </td>
                        <?php } ?>
                    </tr>
                    <tr class="wave-tr-tide">
                        <td colspan="4" class="data-title"><span>Marée basse</span></td>
                        <?php for($i=0;$i<7;$i++){
                            $waveData = $weekly_data[$i]["data"]["h00"]["wave"];
                            $tideData = $weekly_data[$i]["data"]["h00"]["tide"]["low"][0];
                        ?>
                            <td 
                                colspan="4"
                                class="text-center wave-td-data wave-td-data-day<?=$i?>-h00"
                                data-day="<?=$i?>" 
                                data-time="h00"
                                data-map="<?= $waveData["map"] ?>"
                                data-imap="<?= $waveData["imap"] ?>"
                                data-date="<?= $weekly_data[$i]["date"] ?>"
                            >
                                <p><?= $tideData["level"] ?></p>
                                <p><?= $tideData["time"] ?></p>
                            </td>
                        <?php } ?>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    var mapBaselineActions = {
        actions: {
            m0:[
                {
                    className:"btn-map-zoom",
                    content:"<i class='fa fa-plus'></i>",
                    spot:"home",
                    position:{ top:0.0125, left: 0.0175},
                    size:{ width:0.1125, height:0.1125 }
                },
                {
                    className:"btn-map-spot-m0",
                    content:"Seychelles",
                    spot:"seychelles",
                    position:{ top:0.095, left: 0.4875}
                },
                {
                    className:"btn-map-spot-m0",
                    content:"Mayotte",
                    spot:"mayotte",
                    position:{ top:0.195, left: 0.25}
                },
                {
                    className:"btn-map-spot-m0",
                    content:"Toamasina",
                    spot:"toamasina",
                    position:{ top:0.34, left: 0.375}
                },
                {
                    className:"btn-map-spot-m0",
                    content:"Maurice",
                    spot:"maurice",
                    position:{ top:0.365, left: 0.54}
                },
                {
                    className:"btn-map-spot-m0",
                    content:"Reunion",
                    spot:"reunion",
                    position:{ top:0.4375, left: 0.4875}
                },
                {
                    className:"btn-map-spot-m0",
                    content:"Toliara",
                    spot:"toliara",
                    position:{ top:0.445, left: 0.145}
                },
                {
                    className:"btn-map-spot-m0",
                    content:"Durban",
                    spot:"durban",
                    position:{ top:0.515, left: 0.05}
                },
                {
                    className:"btn-map-spot-m0",
                    content:"Kerguelen",
                    spot:"kerguelen",
                    position:{ top:0.945, left: 0.7125}
                }
            ],
            m1:[
                {
                    className:"btn-map-spot-m0",
                    content:"Seychelles",
                    spot:"seychelles",
                    position:{ top:0.095, left: 0.495}
                },
                {
                    className:"btn-map-spot-m0",
                    content:"Mayotte",
                    spot:"mayotte",
                    position:{ top:0.21, left: 0.25}
                },
                {
                    className:"btn-map-spot-m0",
                    content:"Toamasina",
                    spot:"toamasina",
                    position:{ top:0.355, left: 0.385}
                },
                {
                    className:"btn-map-spot-m0",
                    content:"Maurice",
                    spot:"maurice",
                    position:{ top:0.385, left: 0.54}
                },
                {
                    className:"btn-map-spot-m0",
                    content:"Reunion",
                    spot:"reunion",
                    position:{ top:0.46, left: 0.4875}
                },
                {
                    className:"btn-map-spot-m0",
                    content:"Toliara",
                    spot:"toliara",
                    position:{ top:0.47, left: 0.15}
                },
                {
                    className:"btn-map-spot-m0",
                    content:"Durban",
                    spot:"durban",
                    position:{ top:0.545, left: 0.05}
                },
                {
                    className:"btn-map-spot-m0",
                    content:"Kerguelen",
                    spot:"kerguelen",
                    position:{ top:0.99, left: 0.7125}
                }
            ],
            m2:[
                {
                    className:"btn-map-zoom",
                    content:"<i class='fa fa-plus'></i>",
                    spot:"lhermit",
                    position:{ top:0.0125, left: 0.0175},
                    size:{ width:0.1125, height:0.1125 }
                },
                {
                    className:"btn-map-zoom",
                    content:"<i class='fa fa-minus'></i>",
                    spot:"reunion",
                    position:{ top:0.0125, left: 0.1375},
                    size:{ width:0.1125, height:0.1125 }
                },
                {
                    className:"btn-map-spot-m2",
                    content:"St Denis",
                    spot:"stdenis",
                    position:{ top:0.075, left: 0.3125},
                    size:{ width:0.22, height:0.22 }
                },
                {
                    className:"btn-map-spot-m2",
                    content:"St Paul",
                    spot:"stpaul",
                    position:{ top:0.17, left: 0.095},
                    size:{ width:0.22, height:0.22 }
                },
                {
                    className:"btn-map-spot-m2",
                    content:"L'Hermitage",
                    spot:"lhermit",
                    position:{ top:0.39, left: 0.095},
                    size:{ width:0.22, height:0.22 }
                },
                {
                    className:"btn-map-spot-m2",
                    content:"St Pierre",
                    spot:"stpierre",
                    position:{ top:0.565, left: 0.315},
                    size:{ width:0.22, height:0.22 }
                },
                {
                    className:"btn-map-spot-m2",
                    content:"Manapany",
                    spot:"manapany",
                    position:{ top:0.665, left: 0.5325},
                    size:{ width:0.22, height:0.22 }
                },
                {
                    className:"btn-map-spot-m2",
                    content:"St Benoit",
                    spot:"stbenoit",
                    position:{ top:0.32, left: 0.6425},
                    size:{ width:0.22, height:0.22 }
                }
            ],
            m3:[
                {
                    className:"btn-map-zoom",
                    content:"<i class='fa fa-minus'></i>",
                    spot:"home",
                    position:{ top:0.0125, left: 0.0175},
                    size:{ width:0.1125, height:0.1125 }
                },
                {
                    className:"btn-map-spot-navigation btn-map-spot-navigation-down",
                    content:"<i class='fa fa-chevron-down'></i>",
                    spot:"stleu",
                    position:{ top:0.925, left: 0.455}
                },
                {
                    className:"btn-map-spot-navigation btn-map-spot-navigation-right",
                    content:"<i class='fa fa-chevron-right'></i>",
                    spot:"stdenis",
                    position:{ top:0.25, left: 0.92}
                }
            ],
            m4:[
                {
                    className:"btn-map-zoom",
                    content:"<i class='fa fa-minus'></i>",
                    spot:"home",
                    position:{ top:0.0125, left: 0.0175},
                    size:{ width:0.1125, height:0.1125 }
                },
                {
                    className:"btn-map-spot-navigation btn-map-spot-navigation-up",
                    content:"<i class='fa fa-chevron-up'></i>",
                    spot:"stpaul",
                    position:{ top:0, left: 0.455}
                },
                {
                    className:"btn-map-spot-navigation btn-map-spot-navigation-down",
                    content:"<i class='fa fa-chevron-down'></i>",
                    spot:"stpierre",
                    position:{ top:0.925, left: 0.455}
                }
            ],
            m5:[
                {
                    className:"btn-map-zoom",
                    content:"<i class='fa fa-minus'></i>",
                    spot:"home",
                    position:{ top:0.0125, left: 0.0175},
                    size:{ width:0.1125, height:0.1125 }
                },
                {
                    className:"btn-map-spot-navigation btn-map-spot-navigation-top",
                    content:"<i class='fa fa-chevron-up'></i>",
                    spot:"stleu",
                    position:{ top:0, left: 0.455}
                },
                {
                    className:"btn-map-spot-navigation btn-map-spot-navigation-right",
                    content:"<i class='fa fa-chevron-right'></i>",
                    spot:"manapany",
                    position:{ top:0.25, left: 0.92}
                }
            ],
            m6:[
                {
                    className:"btn-map-zoom",
                    content:"<i class='fa fa-minus'></i>",
                    spot:"home",
                    position:{ top:0.0125, left: 0.0175},
                    size:{ width:0.1125, height:0.1125 }
                },
                {
                    className:"btn-map-spot-navigation btn-map-spot-navigation-left",
                    content:"<i class='fa fa-chevron-left'></i>",
                    spot:"stpierre",
                    position:{ top:0.25, left: 0}
                },
                {
                    className:"btn-map-spot-navigation btn-map-spot-navigation-top",
                    content:"<i class='fa fa-chevron-up'></i>",
                    spot:"stbenoit",
                    position:{ top:0, left: 0.455}
                }
            ],
            m7:[
                {
                    className:"btn-map-zoom",
                    content:"<i class='fa fa-minus'></i>",
                    spot:"home",
                    position:{ top:0.0125, left: 0.0175},
                    size:{ width:0.1125, height:0.1125 }
                },
                {
                    className:"btn-map-spot-navigation btn-map-spot-navigation-left",
                    content:"<i class='fa fa-chevron-left'></i>",
                    spot:"stdenis",
                    position:{ top:0.25, left: 0}
                },
                {
                    className:"btn-map-spot-navigation btn-map-spot-navigation-down",
                    content:"<i class='fa fa-chevron-down'></i>",
                    spot:"manapany",
                    position:{ top:0.925, left: 0.455}
                }
            ],
            m8:[
                {
                    className:"btn-map-zoom",
                    content:"<i class='fa fa-minus'></i>",
                    spot:"home",
                    position:{ top:0.0125, left: 0.0175},
                    size:{ width:0.1125, height:0.1125 }
                },
                {
                    className:"btn-map-spot-navigation btn-map-spot-navigation-left",
                    content:"<i class='fa fa-chevron-left'></i>",
                    spot:"stpaul",
                    position:{ top:0.25, left: 0}
                },
                {
                    className:"btn-map-spot-navigation btn-map-spot-navigation-right",
                    content:"<i class='fa fa-chevron-right'></i>",
                    spot:"stbenoit",
                    position:{ top:0.25, left: 0.92}
                }
            ]
        },
        render: function(container, imap){
            var $container = $(container);
            var actions = mapBaselineActions.actions["m"+imap];

            actions.forEach(function(action){
                var buttonStyle = `
                    top:${ action.position.top * $container.width() }px;
                    left:${ action.position.left * $container.width() }px; 
                `
                if(action.size){
                    buttonStyle += `
                        width:${action.size.width * $container.width()}px;
                        height:${action.size.height * $container.width()}px;
                        line-height:${action.size.height * $container.width()}px;
                    `
                }
                var button = $(`<button class="${action.className}" style="${buttonStyle}">${action.content}</button>`)
                button.click(function(){
                    location.href = `/costum/co/index/slug/meteolamer#vague?spot=${action.spot}`
                    location.reload(true);
                })
                $container.append(button);
            })
        }
    }

    function renderMap(map, imap){
        $(".wave-map").attr("src", `http://www.meteolamer.re/dev/${map.replace("./","")}`)
        $(".wave-map-baseline").attr("src", `http://www.meteolamer.re/img/map/baseline_m${imap}.png`)
    }

    $(function(){
        $(".wave-td-data").hover(function(){
            var time = $(this).data("time"),
                day = $(this).data("day"),
                map = $(this).data("map"),
                imap = $(this).data("imap"),
                date = $(this).data("date");

            $(".wave-td-data").removeClass("active")
            $(`.wave-td-data-day${day}-${time}`).addClass("active")
            $(".meteolamer-active-time").text(moment.unix(date).format("dddd DD MMMM"))
            renderMap(map, imap)
            mapBaselineActions.render(".wave-map-baseline-actions", imap);
        })

        setTimeout(() => {
            $(".wave-td-data").first().trigger("mouseenter")
        }, 500);
    })
</script>