<?php
    function getData($spot, $start_date, $end_date=NULL){
        if($end_date !== NULL){
            $where = [
                "date"=> [
                    '$gte'=> strtotime($start_date),
                    '$lte'=> strtotime($end_date)
                ]
            ];
        }else{
            $where = [
                "date" => strtotime($start_date)
            ];
        }
        $where["spot"] = $spot;
        $result = PHDB::find('--meteolamer-forecast', $where);
        $data = [];
        foreach($result as $value){
            $data[] = $value;
        }
        return $data;
    }

    function getSpotsData($spots, $start_date, $end_date=NULL){
        $data = [];
        foreach($spots as $spot){
            $data[$spot["name"]] = getData($spot["name"], $start_date, $end_date);
        }
        return $data;
    }

    $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;
    $spots = [
        [
            "name"=>"stdenis",
            "label"=>"St Denis"
        ],
        [
            "name"=>"stleu",
            "label"=>"St Leu"
        ],
        [
            "name"=>"stpierre",
            "label"=>"St Pierre"
        ],
        [
            "name"=>"stbenoit",
            "label"=>"St Benoit"
        ]
    ];
    $spotsData = getSpotsData($spots, "2020-10-26", "2020-11-01");
    $dataReunion = getData("reunion", "2020-10-26", "2020-11-01");
?>

<style>
    .meteolamer-tide-header h1{
        font-size: 34px;
        color:#708c8c;
    }
    .meteolamer-tide-container{
        display:flex;
        margin-bottom: 50px;
    }
    .meteolamer-tide-map-container{
        width:400px;
    }
    .meteolamer-tide-map-container img{
        width:100%;
    }
    .meteolamer-tide-table-container{
        width: calc(100% - 350px);
        margin-left:50px;
    }
    .meteolamer-tide-table-container table{
        width: 100%;
        display: table;
        border-collapse: separate;
        box-sizing: border-box;
        text-indent: initial;
        border-spacing: 2px;
        border-color: grey;
    }
    .meteolamer-tide-table-container table th{
        background:#c1d7d7;
        padding: 15px 10px;
        color:#708c8c;
        white-space:nowrap;
    }
    .meteolamer-tide-table-container table td{
        padding: 10px;
        background:#f4f8f8;
        text-align:center;
    }
    .meteolamer-tide-table-container table tr td:first-child{
        text-align: left;
    }

    @media only screen and (max-width: 1000px){
        .meteolamer-tide-container{
            flex-direction:column;
        }
        .meteolamer-tide-table-container{
            margin-left:0;

        }

        .meteolamer-tide-map-container{
            width:100%;
        }
        .meteolamer-tide-table-container{
            width:100%;
            margin-left:0;
            overflow: auto;
            margin-top:20px;
        }

        .meteolamer-tide-header h1{
            text-align: left !important;
            font-size:22px;
            padding-bottom:10px;
        }
    }
</style>

<div class="container">
    <?php for($i=0;$i<7;$i++){ ?>
    <div class="text-right meteolamer-tide-header">
        <h1>
            <?php
                setlocale(LC_TIME,'fr_FR.utf8','fra');
                echo strftime('%A %d %B', $dataReunion[$i]["date"]); 
            ?>
        </h1>
    </div>
    <div class="meteolamer-tide-container">
        <div class="meteolamer-tide-map-container">
            <img src="http://www.meteolamer.re/dev/<?= str_replace("./","",$dataReunion[$i]["data"]["h00"]["tide"]["map"]) ?>" alt="">
        </div>
        <div class="meteolamer-tide-table-container">
            <table>
                <thead>
                    <tr>
                        <th></th>
                        <th>Marée Haute</th>
                        <th>Amplitude</th>
                        <th>Marée Basse</th>
                        <th>Amplitude</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($spots as $spot){
                        $data = $spotsData[$spot["name"]][$i]["data"];
                    ?>
                    <tr>
                        <td><?= $spot["label"] ?></td>
                        <td><?= $data["h00"]["tide"]["high"][0]["time"] ?> <?= $data["h00"]["tide"]["high"][1]["time"] ?></td>
                        <td><?= $data["h00"]["tide"]["high"][0]["level"] ?>m</td>
                        <td><?= $data["h00"]["tide"]["low"][0]["time"] ?> <?= $data["h00"]["tide"]["low"][1]["time"] ?></td>
                        <td><?= $data["h00"]["tide"]["low"][0]["level"] ?>m</td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php } ?>
</div>