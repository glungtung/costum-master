<?php
    /* Functions ==============> */
    function getData($spot, $start_date, $end_date=NULL){
        if($end_date !== NULL){
            $where = [
                "date"=> [
                    '$gte'=> strtotime($start_date),
                    '$lte'=> strtotime($end_date)
                ]
            ];
        }else{
            $where = [
                "date" => strtotime($start_date)
            ];
        }
        $where["spot"] = $spot;
        $result = PHDB::find('--meteolamer-forecast', $where);
        return $result;
    }

    function getSpotByName($name){
        return PHDB::findOne('--meteolamer-spots',  ["name" => $name], ["label"=>1]);
    }

    function pascalToHectopascal($value){
        return $value / 100;
    }

    function kelvinToDegreeCelsius($value){
        return $value - 273.15;
    }

    function kmTokn($value){
        return $value / 1.852;
    }
    /* <========= Functions */
    $spotName = Yii::app()->getRequest()->getQuery('spot');
    $spotName = $spotName?$spotName:"reunion";
    $spot = getSpotByName($spotName);

    $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;
    $times = ["00","06","12","18"];

    $results = getData($spotName, "2020-10-26", "2020-11-01");
    $weekly_data = [];
    foreach($results as $result){
        $weekly_data[] = $result;
    }
?>

<style>
    .meteolamer-wind-header{
        width:100%;
        display:flex;
        justify-content:space-between;
        align-items:center;
    }
    .meteolamer-wind-header h1{
        font-size: 34px;
        color:#708c8c;
        white-space: nowrap;
    }
    .meteolemer-wind-container{
        display:flex;
    }
    .wind-map-container{
        width:420px;
        position:relative;
        background-size:cover;
        transition: .4s;
        border-radius:6px;
    }
    .wind-map-container img{
        width:100%;
        height:100%;
        position:absolute;
        border-radius:6px;
    }
    .wind-map-container img:last-child{
        z-index:2;
    }
    .wind-map-baseline-actions{
        width:100%;
        height:100%;
        position:absolute;
        z-index:3;
        border-radius:6px;
    }
    .wind-table-container{
        width:calc(100% - 420px);
    }
    .wind-table-container table{
        width: 100%;
        display: table;
        border-collapse: separate;
        box-sizing: border-box;
        text-indent: initial;
        border-spacing: 2px;
        border-color: grey;
    }
    .wind-table-container table th{
        text-align: center;
        padding: 2px;
        font-size: 11px;
        color: #707070;
    }
    .wind-table-container table th span{
        display: inline-block;
        width: 100%;
        padding: 4px 1px;
        color: #344848;
        border: 1px solid #eaeaea;
        border-radius: 4px;
        background-color: #c1d7d7;
        white-space:nowrap;
    }
    .wind-table-container table tbody{
        border-top: 15px solid transparent;
        background: #f4f8f8;
    }
    .wind-table-container table tr td:first-child{
        background: white;
    }
    .wind-tr-speed-bar ul{
        height: 120px;
        display: flex;
        flex-direction: column;
        justify-content: space-between;
        text-align:right;
    }
    .wind-tr-speed-bar ul li{
        font-size: 12px;
        list-style: none;
        color: #707070;
        position: relative;
        line-height: 0;
    }
    .wind-td-speed-bar{
        position:relative;
        overflow-y:hidden;
    }
    .wind-td-speed-bar div{
        position:absolute;
        bottom:0;
        margin-left: auto;
        margin-right: auto;
        left: 0;
        right: 0;
        width: calc(100% - 4px);
        background: rgb(68,152,155);
        background: linear-gradient(0deg, rgba(68,152,155,1) 30%, rgba(116,250,254,1) 100%);
        margin: 0px 2px;
    }

    .wind-tr-title span{
        display:block;
        padding: 4px;
        font-size:12px;
        color:#666666;
    }

    .wind-tr-meteo{
        text-align:center;
    }

    .wind-tr-meteo p{
        margin:0;
        padding:0;
        font-size: 14px;
    }

    .wind-tr-meteo img{
        height: 40px;
    }

    .wind-tr-speed-km span, .wind-tr-speed-kn span{
        display:block;
        font-size:14px;
        padding: 2px 4px;
        text-align:center;
    }

    .wind-td-data{
        cursor:pointer;
        transition: .3s;
    }

    .wind-td-data.active{
        background: #e4e6e6;
    }

    /* map baseline actions styles =============> */
    .btn-map-zoom{
        position: absolute;
        border: none;
        width: 45px;
        height: 45px;
        border-radius: 100%;
        font-size: 1.8em;
        text-align: center;
        line-height: 45px;
        left: 7px;
        top: 5px;
        transition: .3s;
    }
    .btn-map-zoom:hover{
        background-color: #0093d0;
        color: white;
    }
    .btn-map-spot-m2{
        position: absolute;
        border: none;
        width: 88px;
        height: 88px;
        text-align: center;
        line-height: 88px;
        opacity: 0;
        cursor:pointer;
        transition: .3s;
        background-color: #0093d0;
        color: white;
    }
    .btn-map-spot-m2:hover{
        opacity: .95;
    }
    .btn-map-spot-m0{
        font-weight: bold;
        position: absolute;
        border: none;
        text-align: center;
        transition: .3s;
        font-size: 12px;
        border-radius: 50px;
        background-color: #0093d0;
        color: white;
        opacity:0;
    }
    .btn-map-spot-m0:hover{
        opacity: 1;
    }
    .btn-map-spot-navigation {
        position: absolute;
        width: 38px;
        border: none;
        height: 28px;
        transition:.3s;
    }
    .btn-map-spot-navigation-up{
        border-radius: 0px 0px 10px 10px;
    }
    .btn-map-spot-navigation-down{
        border-radius: 10px 10px 0px 0px;
    }
    .btn-map-spot-navigation-top{
        border-radius: 0px 0px 10px 10px;
    }
    .btn-map-spot-navigation-right{
        width: 30px;
        height: 36px;
        border-radius: 10px 0px 0px 10px;
    }
    .btn-map-spot-navigation-left{
        width: 30px;
        height: 36px;
        border-radius: 0px 10px 10px 0px;
    }
    .btn-map-spot-navigation:hover{
        background-color: #0093d0;
        color: white;
    }
    /* <============== map baseline actions styles */

    @media only screen and (max-width: 1300px){
        .meteolemer-wind-container{
            flex-direction:column;
        }
        .wind-table-container{
            width:100%;
            overflow-x:auto;
        }

        .wind-map-container{
            position: relative;
            width: 100%;
            height: 0;
            padding-bottom: 100%;
            margin-bottom:20px;
        }

        .meteolamer-wind-header{
            display:flex;
            flex-direction:column;
            align-items: flex-start;
            margin-bottom: 15px;
        }

        .meteolamer-wind-header h1:first-child{
            font-size:20px;
            font-weight: bold;
        }

        .meteolamer-wind-header h1{
            font-size:16px;
            font-weight: 500;
            margin: 5px;
        }
    }
</style>
<div class="container">
    <div class="meteolamer-wind-header">
        <h1 class="meteolamer-active-spot-name"><?= $spot["label"] ?></h1>
        <h1 class="meteolamer-active-time"></h1>
    </div>
    <div class="meteolemer-wind-container">
        <div class="wind-map-container">
            <img src="" class="wind-map">
            <img src="" class="wind-map-baseline">
            <div class="wind-map-baseline-actions"></div>
        </div>
        <div class="wind-table-container">
            <table id="table-weekly-forecast">
                <thead>
                    <tr>
                        <th colspan="4"></th>
                        <?php setlocale(LC_TIME,'fr_FR.utf8','fra') ?>
                        <?php foreach($weekly_data as $data) { ?>
                        <th colspan="4">
                            <span><?= strftime('%A %d', $data["date"]); ?></span>
                        </th>
                        <?php } ?>
                    </tr>
                    <tr>
                        <th colspan="4"></th>
                        <?php  for($i=0; $i<7;$i++) { ?>
                            <th>00</th>
                            <th>06</th>
                            <th>12</th>
                            <th>18</th>
                        <?php } ?>
                    </tr>
                </thead>
                <tbody>
                    <tr class="wind-tr-speed-bar">
                        <td colspan="4">
                            <ul>
                                <li>60 -</li>
                                <li>40 -</li>
                                <li>20 -</li>
                                <li>0 -</li>
                            </ul>
                        </td>
                        <?php for($i=0; $i<7;$i++) { ?>
                            <?php foreach($times as $time) { ?>
                            <td 
                                class="wind-td-speed-bar wind-td-data wind-td-data-day<?=$i?>-h<?=$time?>" 
                                data-day="<?=$i?>" 
                                data-time="h<?=$time?>"
                                data-map="<?= str_replace("./","",$weekly_data[$i]["data"]["h".$time]["wind"]["map"]) ?>"
                                data-imap="<?= $weekly_data[$i]["data"]["h".$time]["wind"]["imap"] ?>"
                                data-date="<?= $weekly_data[$i]["date"] ?>"
                            >
                                <?php if(isset($weekly_data[$i]) && $weekly_data[$i]["data"]["h".$time]["wind"]) { 
                                    $windData = $weekly_data[$i]["data"]["h".$time]["wind"];
                                ?>
                                <div style="height: <?= $windData["speed"]*2 ?>px;"></div>
                                <?php } else { ?>
                                -
                                <?php } ?>
                            </td>
                            <?php } ?>
                        <?php } ?>
                    </tr>
                    <tr class="wind-tr-title">
                        <td colspan="4"></td>
                        <td colspan="28">
                            <span>Vent(km)</span>
                        </td>   
                    </tr>
                    <tr class="wind-tr-speed-km">
                        <td colspan="4"></td>
                        <?php for($i=0;$i<7;$i++){ 
                            foreach(["h00","h12"] as $time){
                                if(isset($weekly_data[$i]) && isset($weekly_data[$i]["data"][$time]["wind"])){ 
                        ?>
                            <td 
                                colspan="2" 
                                class="wind-td-data wind-td-data-day<?=$i?>-<?= $time ?>" 
                                data-day="<?=$i?>" 
                                data-time="<?= $time ?>"
                                data-map="<?= str_replace("./","",$weekly_data[$i]["data"][$time]["wind"]["map"]) ?>"
                                data-imap="<?= $weekly_data[$i]["data"][$time]["wind"]["imap"] ?>"
                                data-date="<?= $weekly_data[$i]["date"] ?>"
                            >
                                <span><?= str_pad(round($weekly_data[$i]["data"][$time]["wind"]["speed"]),2, "0", STR_PAD_LEFT) ?></span>
                            </td>
                            <?php }else{ ?>
                            <td><span>-</span></td>
                        <?php }}} ?>
                    </tr>
                    <tr class="wind-tr-title">
                        <td colspan="4"></td>
                        <td colspan="28">
                            <span>Vent(kn)</span>
                        </td>   
                    </tr>
                    <tr class="wind-tr-speed-kn">
                        <td colspan="4"></td>
                        <?php for($i=0;$i<7;$i++){ 
                            foreach(["h00","h12"] as $time){
                                if(isset($weekly_data[$i]) && isset($weekly_data[$i]["data"][$time]["wind"])){ 
                        ?>
                            <td 
                                colspan="2" 
                                class="wind-td-data wind-td-data-day<?=$i?>-<?= $time ?>" 
                                data-day="<?=$i?>" 
                                data-time="<?=$time?>"
                                data-map="<?= str_replace("./","",$weekly_data[$i]["data"][$time]["wind"]["map"]) ?>"
                                data-imap="<?= $weekly_data[$i]["data"][$time]["wind"]["imap"] ?>"
                                data-date="<?= $weekly_data[$i]["date"] ?>"
                            >
                                <span><?= str_pad(round(kmTokn($weekly_data[$i]["data"][$time]["wind"]["speed"])),2, "0", STR_PAD_LEFT) ?></span>
                            </td>
                            <?php }else{ ?>
                            <td><span>-</span></td>
                        <?php }}} ?>
                    </tr>
                    <tr class="wind-tr-title">
                        <td colspan="4"></td>
                        <td colspan="28">
                            <span>Direction</span>
                        </td>   
                    </tr>
                    <tr>
                        <td colspan="4"></td>
                        <?php for($i=0; $i<7;$i++){ 
                            foreach($times as $time){
                                if(isset($weekly_data[$i]) && isset($weekly_data[$i]["data"]["h".$time]["wind"])){ 
                        ?>
                            <td 
                                class="text-center wind-td-data wind-td-data-day<?=$i?>-h<?= $time ?>" 
                                data-day="<?=$i?>" 
                                data-time="h<?= $time ?>"
                                data-map="<?= str_replace("./","",$weekly_data[$i]["data"]["h".$time]["wind"]["map"]) ?>"
                                data-imap="<?= $weekly_data[$i]["data"]["h".$time]["wind"]["imap"] ?>"
                                data-date="<?= $weekly_data[$i]["date"] ?>"
                            >
                                <img src='<?= $assetsUrl ?>/images/meteolamer/dirwind/<?= $weekly_data[$i]["data"]["h".$time]["wind"]["direction"] ?>.png'/>
                            </td>
                            <?php }else { ?>
                            <td>-</td>
                        <?php }}} ?>
                    </tr>
                    <tr class="wind-tr-title">
                        <td colspan="4"></td>
                        <td colspan="28">
                            <span>Meteo</span>
                        </td>   
                    </tr>
                    <tr class="wind-tr-meteo">
                        <td colspan="4"></td>
                        <?php for($i=0; $i<7; $i++) { ?>
                        <td 
                            colspan="4" 
                            class="wind-td-data wind-td-data-day<?=$i?>-h00" 
                            data-day="<?=$i?>" 
                            data-time="h00"
                            data-map="<?= str_replace("./","",$weekly_data[$i]["data"]["h00"]["wind"]["map"]) ?>"
                            data-imap="<?= $weekly_data[$i]["data"]["h00"]["wind"]["imap"] ?>"
                            data-date="<?= $weekly_data[$i]["date"] ?>"
                        >
                            <?php if(isset($weekly_data[$i]) && isset($weekly_data[$i]["data"]["h00"]["wind"])) { 
                                $windData = $weekly_data[$i]["data"]["h00"]["wind"]
                            ?>
                            <p><?= round(pascalToHectopascal($windData["atmosphericPressure"])) ?>hPa</p>
                            <img src="<?= $assetsUrl ?>/images/meteolamer/meteo/<?= strtolower($windData["cloud"]) ?>.png" alt="">
                            <p><?= round(kelvinToDegreeCelsius($windData["temperature"])) ?>°C</p>
                            <?php }else{ ?>
                                -
                            <?php } ?>
                        </td>
                        <?php } ?>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    var mapBaselineActions = {
        actions: {
            m0:[
                {
                    className:"btn-map-zoom",
                    content:"<i class='fa fa-plus'></i>",
                    spot:"home",
                    position:{ top:0.0125, left: 0.0175},
                    size:{ width:0.1125, height:0.1125 }
                },
                {
                    className:"btn-map-spot-m0",
                    content:"Seychelles",
                    spot:"seychelles",
                    position:{ top:0.095, left: 0.4875}
                },
                {
                    className:"btn-map-spot-m0",
                    content:"Mayotte",
                    spot:"mayotte",
                    position:{ top:0.195, left: 0.25}
                },
                {
                    className:"btn-map-spot-m0",
                    content:"Toamasina",
                    spot:"toamasina",
                    position:{ top:0.34, left: 0.375}
                },
                {
                    className:"btn-map-spot-m0",
                    content:"Maurice",
                    spot:"maurice",
                    position:{ top:0.365, left: 0.54}
                },
                {
                    className:"btn-map-spot-m0",
                    content:"Reunion",
                    spot:"reunion",
                    position:{ top:0.4375, left: 0.4875}
                },
                {
                    className:"btn-map-spot-m0",
                    content:"Toliara",
                    spot:"toliara",
                    position:{ top:0.445, left: 0.145}
                },
                {
                    className:"btn-map-spot-m0",
                    content:"Durban",
                    spot:"durban",
                    position:{ top:0.515, left: 0.05}
                },
                {
                    className:"btn-map-spot-m0",
                    content:"Kerguelen",
                    spot:"kerguelen",
                    position:{ top:0.945, left: 0.7125}
                }
            ],
            m1:[
                {
                    className:"btn-map-spot-m0",
                    content:"Seychelles",
                    spot:"seychelles",
                    position:{ top:0.095, left: 0.495}
                },
                {
                    className:"btn-map-spot-m0",
                    content:"Mayotte",
                    spot:"mayotte",
                    position:{ top:0.21, left: 0.25}
                },
                {
                    className:"btn-map-spot-m0",
                    content:"Toamasina",
                    spot:"toamasina",
                    position:{ top:0.355, left: 0.385}
                },
                {
                    className:"btn-map-spot-m0",
                    content:"Maurice",
                    spot:"maurice",
                    position:{ top:0.385, left: 0.54}
                },
                {
                    className:"btn-map-spot-m0",
                    content:"Reunion",
                    spot:"reunion",
                    position:{ top:0.46, left: 0.4875}
                },
                {
                    className:"btn-map-spot-m0",
                    content:"Toliara",
                    spot:"toliara",
                    position:{ top:0.47, left: 0.15}
                },
                {
                    className:"btn-map-spot-m0",
                    content:"Durban",
                    spot:"durban",
                    position:{ top:0.545, left: 0.05}
                },
                {
                    className:"btn-map-spot-m0",
                    content:"Kerguelen",
                    spot:"kerguelen",
                    position:{ top:0.99, left: 0.7125}
                }
            ],
            m2:[
                {
                    className:"btn-map-zoom",
                    content:"<i class='fa fa-plus'></i>",
                    spot:"lhermit",
                    position:{ top:0.0125, left: 0.0175},
                    size:{ width:0.1125, height:0.1125 }
                },
                {
                    className:"btn-map-zoom",
                    content:"<i class='fa fa-minus'></i>",
                    spot:"reunion",
                    position:{ top:0.0125, left: 0.1375},
                    size:{ width:0.1125, height:0.1125 }
                },
                {
                    className:"btn-map-spot-m2",
                    content:"St Denis",
                    spot:"stdenis",
                    position:{ top:0.075, left: 0.3125}
                },
                {
                    className:"btn-map-spot-m2",
                    content:"St Paul",
                    spot:"stpaul",
                    position:{ top:0.17, left: 0.095}
                },
                {
                    className:"btn-map-spot-m2",
                    content:"L'Hermitage",
                    spot:"lhermit",
                    position:{ top:0.39, left: 0.095}
                },
                {
                    className:"btn-map-spot-m2",
                    content:"St Pierre",
                    spot:"stpierre",
                    position:{ top:0.565, left: 0.315}
                },
                {
                    className:"btn-map-spot-m2",
                    content:"Manapany",
                    spot:"manapany",
                    position:{ top:0.665, left: 0.5325}
                },
                {
                    className:"btn-map-spot-m2",
                    content:"St Benoit",
                    spot:"stbenoit",
                    position:{ top:0.32, left: 0.6425}
                }
            ],
            m3:[
                {
                    className:"btn-map-zoom",
                    content:"<i class='fa fa-minus'></i>",
                    spot:"home",
                    position:{ top:0.0125, left: 0.0175},
                    size:{ width:0.1125, height:0.1125 }
                },
                {
                    className:"btn-map-spot-navigation btn-map-spot-navigation-down",
                    content:"<i class='fa fa-chevron-down'></i>",
                    spot:"stleu",
                    position:{ top:0.925, left: 0.455}
                },
                {
                    className:"btn-map-spot-navigation btn-map-spot-navigation-right",
                    content:"<i class='fa fa-chevron-right'></i>",
                    spot:"stdenis",
                    position:{ top:0.25, left: 0.92}
                }
            ],
            m4:[
                {
                    className:"btn-map-zoom",
                    content:"<i class='fa fa-minus'></i>",
                    spot:"home",
                    position:{ top:0.0125, left: 0.0175},
                    size:{ width:0.1125, height:0.1125 }
                },
                {
                    className:"btn-map-spot-navigation btn-map-spot-navigation-up",
                    content:"<i class='fa fa-chevron-up'></i>",
                    spot:"stpaul",
                    position:{ top:0, left: 0.455}
                },
                {
                    className:"btn-map-spot-navigation btn-map-spot-navigation-down",
                    content:"<i class='fa fa-chevron-down'></i>",
                    spot:"stpierre",
                    position:{ top:0.925, left: 0.455}
                }
            ],
            m5:[
                {
                    className:"btn-map-zoom",
                    content:"<i class='fa fa-minus'></i>",
                    spot:"home",
                    position:{ top:0.0125, left: 0.0175},
                    size:{ width:0.1125, height:0.1125 }
                },
                {
                    className:"btn-map-spot-navigation btn-map-spot-navigation-top",
                    content:"<i class='fa fa-chevron-up'></i>",
                    spot:"stleu",
                    position:{ top:0, left: 0.455}
                },
                {
                    className:"btn-map-spot-navigation btn-map-spot-navigation-right",
                    content:"<i class='fa fa-chevron-right'></i>",
                    spot:"manapany",
                    position:{ top:0.25, left: 0.92}
                }
            ],
            m6:[
                {
                    className:"btn-map-zoom",
                    content:"<i class='fa fa-minus'></i>",
                    spot:"home",
                    position:{ top:0.0125, left: 0.0175},
                    size:{ width:0.1125, height:0.1125 }
                },
                {
                    className:"btn-map-spot-navigation btn-map-spot-navigation-left",
                    content:"<i class='fa fa-chevron-left'></i>",
                    spot:"stpierre",
                    position:{ top:0.25, left: 0}
                },
                {
                    className:"btn-map-spot-navigation btn-map-spot-navigation-top",
                    content:"<i class='fa fa-chevron-up'></i>",
                    spot:"stbenoit",
                    position:{ top:0, left: 0.455}
                }
            ],
            m7:[
                {
                    className:"btn-map-zoom",
                    content:"<i class='fa fa-minus'></i>",
                    spot:"home",
                    position:{ top:0.0125, left: 0.0175},
                    size:{ width:0.1125, height:0.1125 }
                },
                {
                    className:"btn-map-spot-navigation btn-map-spot-navigation-left",
                    content:"<i class='fa fa-chevron-left'></i>",
                    spot:"stdenis",
                    position:{ top:0.25, left: 0}
                },
                {
                    className:"btn-map-spot-navigation btn-map-spot-navigation-down",
                    content:"<i class='fa fa-chevron-down'></i>",
                    spot:"manapany",
                    position:{ top:0.925, left: 0.455}
                }
            ],
            m8:[
                {
                    className:"btn-map-zoom",
                    content:"<i class='fa fa-minus'></i>",
                    spot:"home",
                    position:{ top:0.0125, left: 0.0175},
                    size:{ width:0.1125, height:0.1125 }
                },
                {
                    className:"btn-map-spot-navigation btn-map-spot-navigation-left",
                    content:"<i class='fa fa-chevron-left'></i>",
                    spot:"stpaul",
                    position:{ top:0.25, left: 0}
                },
                {
                    className:"btn-map-spot-navigation btn-map-spot-navigation-right",
                    content:"<i class='fa fa-chevron-right'></i>",
                    spot:"stbenoit",
                    position:{ top:0.25, left: 0.92}
                }
            ]
        },
        render: function(container, imap){
            var $container = $(container);
            var actions = mapBaselineActions.actions["m"+imap];

            actions.forEach(function(action){
                var buttonStyle = `
                    top:${ action.position.top * $container.width() }px;
                    left:${ action.position.left * $container.width() }px; 
                `
                if(action.size){
                    buttonStyle += `
                        width:${action.size.width * $container.width()}px;
                        height:${action.size.height * $container.width()}px;
                    `
                }
                var button = $(`<button class="${action.className}" style="${buttonStyle}">${action.content}</button>`)
                $container.append(button);
            })
        }
    }

    function getMapBaselineId(mapUrl){
        var baseline_id = /\/m[0-9]\//.exec(mapUrl)
        if(baseline_id !== null){
            return baseline_id[0].split("/").join("")
        }
        return "";
    }

    function renderMap(map, imap){
        $(".wind-map").attr("src", `http://www.meteolamer.re/dev/${map}`)
        $(".wind-map-baseline").attr("src", `http://www.meteolamer.re/img/map/baseline_m${imap}.png`)
    }

    $(function(){
        $(".wind-td-data").hover(function(){
            var time = $(this).data("time"),
                day = $(this).data("day"),
                map = $(this).data("map"),
                imap = $(this).data("imap"),
                date = $(this).data("date");

            $(".wind-td-data").removeClass("active");
            $(`.wind-td-data-day${day}-${time}`).addClass("active");
            $(".meteolamer-active-time").text(moment.unix(date).format("dddd DD MMMM"));

            renderMap(map, imap);
            mapBaselineActions.render(".wind-map-baseline-actions", imap)
        })
        $(".wind-td-data").first().trigger("mouseenter")
    })
</script>