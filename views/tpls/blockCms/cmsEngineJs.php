<script type="text/javascript">
  var itemArray = [];
  var page = <?php echo json_encode($page); ?>;
  jQuery(document).ready(function(){
    $.each($(".markdown"), function(k,v){
      descHtml = dataHelper.markdownToHtml($(v).html());
      $(v).html(descHtml);
    });
    setTimeout(function(){
      $('[data-toggle="tooltip"]').tooltip();
    },2500)
    $(".dropdown-menu .btn-menu-connect").attr("data-toggle","modal").attr("data-target","#modalLogin");
   setTitle(costum.title);  
   cmsBuilder.init();
   urlCtrl.loadByAnchor();
   var contextData = <?php echo json_encode($el); ?>;
  var contextId=<?php echo json_encode((string) $el["_id"]); ?>;
  var contextType=<?php echo json_encode($this->costum["contextType"]); ?>;
  var contextName=<?php echo json_encode($el["name"]); ?>;
   
        
   contextData.id=<?php echo json_encode((string) $el["_id"]); ?>;   
          $(".new").click(function() {         
            var tplCtx = {};
            var activeForm = {
              "jsonSchema" : {
                "title" : "Ajouter un nouveau bloc CMS",
                "type" : "object",
                onLoads : {
                  onload : function(data){
                    $(".parentfinder").css("display","none");
                  }
                },
                "properties" : {
                  
                  path : {
                    label : "Chemin",
                    "inputType" : "text",
                    value : "tpls.blockCms.[type de block].[nom du bloc]"
                  },
                  name : { 
                    label : "Nom du bloc cms",
                    "inputType" : "text",
                    value : "" 
                  },
                    image : dyFInputs.image(),
                    description : {
                      label : "Description",
                      inputType : "text",
                      value : ""
                    },

                    parent : {
                      inputType : "finder",
                      label : tradDynForm.whoiscarrypoint,
                      multiple : true,
                      rules : { lengthMin:[1, "parent"]}, 
                      initType: ["organizations", "projects", "events"],
                      openSearch :true
                    }
                  }
                },

              };          

              activeForm.jsonSchema.afterBuild = function(){
                dyFObj.setMongoId('cms',function(data){
                  uploadObj.gotoUrl = location.hash;
                  tplCtx.id=dyFObj.currentElement.id;
                  tplCtx.collection=dyFObj.currentElement.type;
                });
              };


              activeForm.jsonSchema.save = function () {
                tplCtx.value = {};
                $.each( activeForm.jsonSchema.properties , function(k,val) { 
                  tplCtx.value[k] = $("#"+k).val();
                });
                if(typeof tplCtx.value == "undefined")
                  toastr.error('value cannot be empty!');
                else {
                  mylog.log("activeForm save tplCtx",tplCtx);
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("Élément bien ajouter");
                      $("#ajax-modal").modal('hide');
                      urlCtrl.loadByHash(location.hash);
                    });
                  } );
                }
              }
              dyFObj.openForm( activeForm );
            });

        $(".thisTpls").off().on("click",function() { 
            //tplCtx.key = $(this).data("key");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            tplCtx.value = {
              name : $(this).data("name"),
              path : $(this).data("path"),
              page : page
            };

            tplCtx.value.parent={};
            tplCtx.value.parent[thisContextId] = {
              type : thisContextType, 
              name : thisContextSlug
            };
            tplCtx.value.haveTpl = false;
            if(typeof tplCtx.value == "undefined")
              toastr.error("value cannot be empty!");
            else {
              mylog.log("activeForm save tplCtx",tplCtx);
              dataHelper.path2Value( tplCtx, function(params) {
                if(params.result){
                  toastr.success("Bloc bien ajouter");
                  $("#ajax-modal").modal("hide");
                  urlCtrl.loadByHash(location.hash);
                }
                else 
                  toastr.error(params.msg);
                  
                  
              } );
            }
        });


      $(".deleteLine").off().on("click",function (){
          $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
          var id = $(this).data("id");
          var type = $(this).data("collection");
          var urlToSend = baseUrl+"/co2/cms/delete/id/"+id;
          bootbox.confirm(trad.areyousuretodelete,
            function(result){
              if (!result) {
                return;
              } 
              else {
                $.ajax({
                  type: "POST",
                  url: urlToSend,
                  dataType : "json"
                })
                .done(function (data) {
                  if ( data && data.result ) {
                    toastr.success("Element effacé");
                    $("#"+type+id).remove();
                  } else {
                   toastr.error("something went wrong!! please try again.");
                 }
                 urlCtrl.loadByHash(location.hash);
               });
              }
            }
            );
        });
    lazyWelcomeDyFObj(0);  
});
</script>

