<?php 
$keyTpl = "menu";
$paramsData = [
  "filiere" =>[],
  "background"=> "#0A96B5"
];

if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (  isset($blockCms[$e]) ) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
}  
$parent = Element::getElementSimpleById($this->costum["contextId"],$this->costum["contextType"],null,["name", "description","shortDescription","slug","profilMediumImageUrl","email","socialNetwork","profilBannerUrl","filiere"]);

$listFielere = isset($parent["filiere"])?$parent["filiere"]:[];
?>
<style type="text/css">
  .menuThemeCocity_<?= $kunik?>{
    background: <?= $paramsData["background"]?>;
    width: 100%;
    height: 100px;
    padding: 2%;
    margin-left: 2%;
    color: white;
  }
  .menuThemeCocity_<?= $kunik?> a{
    color: #ccc;
    font-size: 15px;
    text-transform: none;
    text-align: center;
    padding: 5px;
    height: 15%;
  }
  .menuThemeCocity_<?= $kunik?> .copieCostum<?= $blockCms['_id'] ?>{
    color: #000;
  }
  .menuThemeCocity_<?= $kunik?> .initialise<?= $blockCms['_id'] ?>{
    color: darkgrey;

  }
  .menuThemeCocity_<?= $kunik?> .openCostum<?= $blockCms['_id'] ?>{
    color: white;
  }
  .block-container-<?= $kunik?>{
    min-height: 50px !important; 
  }
  .menuThemeCocity_<?= $kunik?> img{

    font-size: 50px;
    height: 40px;
    width: 40px;
  }
  .mainCocity_<?= $kunik?>{
    padding: 3%;
    background-image: url(<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/cocity/photo/bg_ville.png);
    background-position: bottom;
    background-repeat: no-repeat;    
    padding-bottom: 15%;
  }
  .mainCocity_<?= $kunik?> p{
   font-size: 18px ;
   margin-top: 40px;
   color: #000;
   margin-left: 30%;
   margin-right: 30%;
 }
 .mainCocity_<?= $kunik?> h1{
   font-size: 25px;
   color: #005E6F;

 }
 .bloc-img_<?= $kunik?>{
  padding-top: 20%;
  padding-bottom: 20%;
  width: 40%;
  margin-left: 7%;
  background-size: cover;
  font-size: 19px;
  box-shadow: 0px 0px 6px silver;
  
}
.bloc-img_<?= $kunik?> a{
  font-size: 18px;
  text-decoration: none;
  color: #fff;
  
}

.menuThemeCocity_<?= $kunik?> .filiere{
  padding: 0px;
  margin-left: 3%;
  color:#005E6F;
}
  .block-container-<?= $kunik?>{
    min-height: 50px !important; 
  }
@media (max-width: 978px) {
  .menuThemeCocity_<?= $kunik?>{
    height: 120px;
    padding: 2%;
    margin-left: 2%;
  }
  .mainCocity_<?= $kunik?> h1{
    font-size: 20px;
  }
  .mainCocity_<?= $kunik?> p{
   font-size: 16px;
   margin-top: 5%;
   margin-left: 5%;
   margin-right: 5%;
  }
  .bloc-img_<?= $kunik?>{
    margin-top: 3%;
    padding-top: 20%;
    padding-bottom: 20%;
    width: 100%;
    margin-left: 0;
    background-size: cover;
    font-size: 17px;
  }
}
.addElement<?= $blockCms['_id'] ?> {
  display: none;
}
.menuThemeCocity_<?= $kunik?>:hover .addElement<?= $blockCms['_id'] ?>{
  display: block;
  position: absolute;
  top:2%;
  left: 50%;
  transform: translate(-50%,-50%);
}

</style>
<div class="menuThemeCocity_<?= $kunik?> ">
  
  <div class="text-center  " >
   <!-- <button class="btn btn-primary addElement<?= $blockCms['_id'] ?>">Ajouter une nouvelle filière</button> -->
    <div class="listFil_<?= $kunik?>"> </div>
   
  </div>
</div> 
 

  <script type="text/javascript">
   
    sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
      listeFiliere();
      sectionDyf.<?php echo $kunik?>Params = {
        "jsonSchema" : {    
          "title" : "Configurer votre section",
          "description" : "Personnaliser votre section1",
          "icon" : "fa-cog",
          "properties" : {

          },
          save : function (data) {  
            tplCtx.value = {};
            $.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
              tplCtx.value[k] = $("#"+k).val();
            });

            mylog.log("save tplCtx",tplCtx);

            if(typeof tplCtx.value == "undefined")
              toastr.error('value cannot be empty!');
            else {
              dataHelper.path2Value( tplCtx, function(params) { 
                $("#ajax-modal").modal('hide');
                toastr.success("élement mis à jour");
                urlCtrl.loadByHash(location.hash);
              } );
            }
          }
        }

      };
      mylog.log("paramsData",sectionDyf);
      $(".edit<?php echo $kunik?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = "allToRoot";
        dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
      });
      $(".addElement<?= $blockCms['_id'] ?>").click(function() {  
        var tplCtx = {};
        var activeForm = {
          "jsonSchema" : {
            "title" : "Ajouter nouvelle filière",
            "type" : "object",
            "properties" : {
              name : {
                label : "Nom de la filière",
                "inputType" : "text",
                values : ""
              }
          }
        }
      };          

      activeForm.jsonSchema.save = function () {
        tplCtx.id = costum.contextId;
        tplCtx.collection = costum.contextType;
        //tplCtx.path = "filiere."+$("#test").val();
        tplCtx.path="allToRoot";
        tplCtx.value = {};

        $.each(activeForm.jsonSchema.properties , function(k,val) {
          filiere = "filiere."+$("#name").val()+"."+k;
          tplCtx.value[filiere] = $("#"+k).val();
        });
      //  tplCtx.value.filiere={};
      //  tplCtx.value.filiere = {
      //   icone : $("#icone").val(),
      //   name : $("#name").val()
      // };
      mylog.log("ediit addElement", tplCtx);
      if(typeof tplCtx.value == "undefined")
        toastr.error('value cannot be empty!');
      else {
        dataHelper.path2Value( tplCtx, function(params) { 
          mylog.log("ediit addElement11", tplCtx);
          toastr.success('Modification enregistrer');
          $("#ajax-modal").modal('hide');
          urlCtrl.loadByHash(location.hash);
        } );
      } 
    }
    dyFObj.openForm( activeForm );
  });

   
  });
      


  function listeFiliere(){
    var params = {
      "contextId" : costum.contextId
    }
    ajaxPost(
      null,
      baseUrl+"/costum/cocity/getfiliere",
      params,
      function(data){
        $.each(data, function( index, value ) {
          var nameTheme = typeof tradTags[value.name.toLowerCase()] != "undefined" ? 
              tradTags[value.name.toLowerCase()].charAt(0).toUpperCase()+tradTags[value.name.toLowerCase()].slice(1) 
              : (value.name).charAt(0).toUpperCase() + (value.name).slice(1);
          nameFil1 = nameTheme.replace('É', 'E');
          nameFil= nameFil1.replace("é", "e");
          nameOrga = ("<?= $parent["name"]?>").toLowerCase()+nameFil;
          existOrgaFiliere(nameOrga,value);
        })       
      }
    )
  }
  var src ="";
  function existOrgaFiliere(nameOrga,value){
    var nameTheme = typeof tradTags[value.name.toLowerCase()] != "undefined" ? 
              tradTags[value.name.toLowerCase()].charAt(0).toUpperCase()+tradTags[value.name.toLowerCase()].slice(1) 
              : (value.name).charAt(0).toUpperCase() + (value.name).slice(1);
    //alert(nameOrga);
    mylog.log("valuecocity", value);
      
      var params = {
        "slug" : nameOrga
      };
      ajaxPost(
      null,
       baseUrl+"/costum/cocity/getorgafiliere",
        params,
        function(data){
        //alert("sucess");
        if (data.length!=0){
          $.each(data, function( index, valueFil ) {
              if(typeof valueFil.costum !="undefined" && typeof valueFil.costum.slug != "undefined" && valueFil.costum.slug == "filiereGenerique" && typeof valueFil.cocity !="undefined" && valueFil.cocity == costum.contextId) {               
                src += 
                "<a  href='<?php  echo Yii::app()->createUrl("/costum")?>/co/index/slug/"+ valueFil.slug+"' class='col-sm-1 col-xs-2 btn btn-select-filliaire btn-menu-xs openCostum<?= $blockCms['_id'] ?>' target='_blank' >" +
                "<i class='fa "+value.icon+" '></i><br>" + 
                          nameTheme +
                "</a>";
              } 
          });
        } else {
          src += 
            "<a  href='javascript:;' class='col-sm-1 col-xs-2 btn-menu-xs initialise<?= $blockCms['_id'] ?>' data-id='' data-tag = '"+value.icon +"'data-name ='"+ value.name+"' >"  +
                "<i class='fa "+value.icon+" '></i><br>" + 
                      nameTheme +
            "</a>";
        }
        $(".listFil_<?= $kunik?>").html(src);
        $(".initialise<?= $blockCms['_id'] ?>").click(function() { 
          nameOrga = "<?= $parent["name"]?>"+" "+$(this).data("name");
          nameFiliere = $(this).data("name");
          // alert(nameOrga);
          bootbox.confirm("Cet filière n'existe pas, vous voulez le créez ?",
            function(result){
              if (!result) {
                return;
              }else {
                window.open("<?php  echo Yii::app()->createUrl('/costum')?>/co/index/slug/filierePrez#"+costum.contextId+".<?= $parent["name"]?>."+nameFiliere, '_blank'); 
              }
            }); 
        });
      }
    )
  }
  </script>