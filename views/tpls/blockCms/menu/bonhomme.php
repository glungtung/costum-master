<?php 
    $keyTpl ="bonhomme";
    $paramsData = [
        "title"=>"Les marterritoires",
        "content" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Dicta voluptas explicabo nesciunt exercitationem placeat? Cupiditate harum debitis nesciunt deleniti hic? Similique obcaecati dicta hic aspernatur officia dolor eum ut. Sit.",
        "fontSize" => "20"
    ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) {
        $paramsData[$e] = $blockCms[$e];
      }
    }
  }
?>

<style>
    

    .text-<?= $kunik?>{
        font-size: <?=$paramsData["fontSize"]?>px !important;
        margin-top: 20px;
        margin-bottom: 20px;
    }
</style>

<?php 
    $colorSection1 = "#9ac21e";
?>

<div class="container content-<?= $kunik?>">
    <div style="background-color: <?php echo $colorSection1?>" class="col-xs-12">
        <h1 style="color: white" class="text-center title">Les smarterritoires</h1>
    </div>
    <div style="margin-top: 3vw;" id="resultSmaterritories"></div>
</div>         

<div id="bonhomme">
  <?php echo $this->renderPartial("costum.views.custom.smarterre.elements.bonhomme"); ?>
</div>

<script type="text/javascript">
    $("polygon").each(function(){
        $(".text"+$(this).attr('id')).css("filter", "invert(100%)");
        // $(".text"+$(this).attr('id')).css("visibility", "hidden");
    });
</script>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {    
            "title" : "Configurer votre bloc",
            "description" : "Personnaliser votre bloc",
            "icon" : "fa-cog",
            
            "properties" : {
                "title" : {
                    "inputType" : "text",
                    "label" : "Titre du section",
                    value :  sectionDyf.<?php echo $kunik ?>ParamsData.title
                },
                "content":{
                    "inputType" : "textarea",
                    "markdown" : true,
                    "label" : "Contenu du section",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.content
                },
                "fontSize":{
                    "inputType" : "number",
                    "label" : "Taille de caratère",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.fontSize
                }
            },
        beforeBuild : function(){
                uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;

                if(k == "items")
                  tplCtx.value[k] = data.items;
                  mylog.log("andrana",data.items)
              });
              console.log("save tplCtx",tplCtx);

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("Élément bien ajouter");
                      $("#ajax-modal").modal('hide');
                      urlCtrl.loadByHash(location.hash);
                    });
                  } );
              }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
    });
</script>
