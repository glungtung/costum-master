<?php 
$keyTpl = "linkWithbg";
$paramsData = [
  "filiere" =>[],
  "background"=> "#0A96B5",
  "color" =>"#000000",
  "colorBtn" => "#ffffff",
  "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut"
];

if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (  isset($blockCms[$e]) ) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
}  
$orga = Element::getElementSimpleById($this->costum["contextId"],"organizations",null,["name", "description","shortDescription","slug","profilMediumImageUrl","email","socialNetwork","profilBannerUrl","filiere"]);
 $blockKey = (string)$blockCms["_id"];
  $initImageOne = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>'imgOne',
    ), "image"
  );
  $arrayImgOne = [];
  
  foreach ($initImageOne as $key => $value) {
    $arrayImgOne[]= $value["imagePath"];
  }
  
  $initImageTwo = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>'imgTwo',
    ), "image"
  );
  $arrayImgTwo = [];
  foreach ($initImageTwo as $key => $value) {
    $arrayImgTwo[]= $value["imagePath"];
  }
  $initImagebg= Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>'background',
    ), "image"
  );
  $background = [];
  foreach ($initImagebg as $key => $value) {
    $background[]= $value["imagePath"];
  }
  
?>
<style type="text/css">
  .mainCocity_<?= $kunik?>{
    padding: 3%;
    background-size: 100% 100%;    
    background-position: bottom;
    background-repeat: no-repeat;    
    padding-bottom: 2%;
  }
  .mainCocity_<?= $kunik?> p{
   font-size: 18px ;
    margin-top: 3%;
    margin-bottom: 3%;
   color: <?= $paramsData["color"]?>;
  }
 .mainCocity_<?= $kunik?> h1{
   font-size: 25px;
   color: #005E6F;

 }
  .bloc-img_<?= $kunik?>{
    padding-top: 10%;
    padding-bottom: 10%;  
    height: 300px;
    width: 40%;
    margin-left: 7%;
    background-size: cover;
    font-size: 19px;
    box-shadow: 0px 0px 6px silver;
    
  }
 

.bloc-img_<?= $kunik?> a{
  position: absolute;
  top: 35%;
  left: 0;
  right: 0;
  text-align: center;
  z-index: 1;
  font-size: 20px;
  color: <?= $paramsData["colorBtn"]?>;
}
 @media (max-width: 414px) {
  .bloc-img_<?= $kunik?> a{
    position: absolute;
    top: 35%;
    left: 0;
    right: 0;
    text-align: center;
    z-index: 1;
    font-size: 16px;
  }
  .mainCocity_<?= $kunik?> h1{
    font-size: 20px;
  }
  .mainCocity_<?= $kunik?> p{
   font-size: 16px;
   margin-top: 5%;
   margin-left: 5%;
   margin-right: 5%;
  }
  .bloc-img_<?= $kunik?>{
    margin-top: 3%;
    padding-top: 10%;
    height: 200px;
    padding-bottom: 10%;
    width: 100%;
    margin-left: 0;
    background-size: cover;
    font-size: 17px;
  }
}
.mainCocity_<?= $kunik?> .menu1:after, .mainCocity_<?= $kunik?> .menu2:after {
    content: " ";
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    background: rgba(0,0,0,.7);
  }
.mainCocity_<?= $kunik?> .menu1{
  <?php if (count($arrayImgOne ) !=0) {  ?>
      background-image: url('<?php echo $arrayImgOne[0] ?>');
  <?php } else { ?>
    background-image: url("<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/cocity/NewMeth.jpg ");
    <?php } ?>;
  }
.mainCocity_<?= $kunik?> .menu2{
  <?php if (count($arrayImgTwo ) !=0) {  ?>
      background-image: url('<?php echo $arrayImgTwo[0] ?>');
  <?php } else { ?>
    background-image: url("<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/cocity/NewMeth.jpg ");
    <?php } ?>;
}
</style>
<div class="mainCocity_<?= $kunik?> row">
  <div class="col-xs-12 col-md-12 text-center">
    <h1 class="title" >
      <i class="fa fa-user" aria-hidden="true"></i>
      Vous êtes de <?= $orga["name"]?>
    </h1>
    <div class="markdown"> <?= $paramsData["description"]?>  </div>
    <div class="bloc-img_<?= $kunik?> menu1 col-md-6 col-xs-12" >
      <a href="#page.type.citoyens.id.<?= @Yii::app()->session['userId'] ?>" class="lbh " data-toggle="dropdown">
        <span class="img-text-bloc description">
          <i class="fa fa-pencil"> </i> <br>
          Je souhaite accéder a mon profil 
        </span>
      </a>
    </div>
    <div class="bloc-img_<?= $kunik?> menu2 col-md-6 col-xs-12" >
    <a href="#element.invite.type.citoyens.id.<?= @Yii::app()->session['userId'] ?>" class="lbh " data-placement="bottom" data-original-title="Inviter des personnes "> 
      <span class="img-text-bloc description">
        <i class="fa fa-user-plus"> </i> 
        <br> 
        Je souhaites inviter des personnes à la plateforme
      </span></a>
    </div>

  </div>
  <script type="text/javascript">
    sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
      sectionDyf.<?php echo $kunik?>Params = {
        "jsonSchema" : {    
          "title" : "Configurer votre section",
          "description" : "Personnaliser votre section1",
          "icon" : "fa-cog",
          "properties" : {
            "description":{
              label : "Description",
              inputType : "textarea",
              markdown : true,
              values :  sectionDyf.<?php echo $kunik?>ParamsData.description
            },            
            "color":{
              label : "Couleur du text",
              inputType : "colorpicker",
              values :  sectionDyf.<?php echo $kunik?>ParamsData.color
            },         
            "colorBtn":{
              label : "Couleur du text dans le bouton",
              inputType : "colorpicker",
              values :  sectionDyf.<?php echo $kunik?>ParamsData.colorBtn
            },
            "imageOne" :{
              "inputType" : "uploader",
              "label" : "Image en fond 1",
              "docType": "image",
            "contentKey" : "slider",
              "domElement" : "imgOne",
              "itemLimit" : 1,
              "filetypes": ["jpeg", "jpg", "gif", "png"],
              "showUploadBtn": false,              
                "endPoint" :"/subKey/imgOne",
              initList : <?php echo json_encode($initImageOne) ?>
            },
            "imageTwo" :{
              "inputType" : "uploader",
              "label" : "Image en fond 2",
              "docType": "image",
              "contentKey" : "slider",
              "itemLimit" : 1,              
              "domElement" : "imgTwo",
              "filetypes": ["jpeg", "jpg", "gif", "png"],
              "showUploadBtn": false,
              "endPoint" :"/subKey/imgTwo",
              initList : <?php echo json_encode($initImageTwo) ?>
            }

          },
           beforeBuild : function(){
                uploadObj.set("cms","<?php echo $blockKey ?>");
            },
          save : function (data) {  
            tplCtx.value = {};
            $.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
              tplCtx.value[k] = $("#"+k).val();
            });

            mylog.log("save tplCtx",tplCtx);

            if(typeof tplCtx.value == "undefined")
              toastr.error('value cannot be empty!');
            else {
              dataHelper.path2Value( tplCtx, function(params) {
                dyFObj.commonAfterSave(params,function(){
                  toastr.success("Élément bien ajouter");
                  $("#ajax-modal").modal('hide');
                  dyFObj.closeForm();
                    urlCtrl.loadByHash(location.hash);
                });
              } );
            }
          }
        }
      };
      mylog.log("paramsData",sectionDyf);
      $(".edit<?php echo $kunik?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = "allToRoot";
        dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
      });
    })
  </script>