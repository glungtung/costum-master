
<script>
  $(function(){
      $(".btnMenuTpl").click(function(){
        $("#menuTpl").modal("show");
      });
  })
</script>


<script type="text/javascript"> 
  function detail(nbr){
    document.getElementById("userNbr").innerHTML = nbr;
  }
  tplUsingId = "<?php echo $tplUsingId; ?>"  

  function saveThisCostum () { 
    if ("<?= $this->costum['slug'] ?>" != "costumize") {
      data = {
        collection : '<?= ($this->costum["contextType"]); ?>',
        id : '<?= ((string) $el["_id"]); ?>',
        path : "costum.slug",
        value : "costumize"
      }
      dataHelper.path2Value( data, function(params) { 
        urlCtrl.loadByHash(location.hash);
      } );
    }    
  }     

  function saveTplCostum (costumSlug) { 
    currentlySlug = "<?= $this->costum['slug'] ?>";
    if (currentlySlug != "costumize") { 
      newSlug = {
        collection : '<?= ($this->costum["contextType"]); ?>',
        id : '<?= ((string) $el["_id"]); ?>',
        path : "costum.slug",
        value : "costumize"
      }
      dataHelper.path2Value( newSlug, function(params) { 
        urlCtrl.loadByHash(location.hash);
     } ); 
    } else if (currentlySlug = "costumize") {}{
      newSlug = {
        collection : '<?= ($this->costum["contextType"]); ?>',
        id : '<?= ((string) $el["_id"]); ?>',
        path : "costum.slug",
        value : costumSlug
      }
      dataHelper.path2Value( newSlug, function(params) { 
        urlCtrl.loadByHash(location.hash); 
      } ); 
    }

  }

  function saveTplUser (idTpl, stat) {
   var tplCtx = {};
   tplCtx.id = idTpl;
   tplCtx.path = "tplsUser."+thisContextId+".<?= $page?>";
   tplCtx.collection = "cms"; 
   tplCtx.value = stat;

 dataHelper.path2Value( tplCtx, function(params) {
 // alert(idTpl+" Set "+ stat);
      //urlCtrl.loadByHash(location.hash);
      toastr.success("Changement enregistré!");
      // if(url.slice(-1)=="/")
      // {    
      //   location.href = newUrl+'test/openCostumMira';
      //    // saveTplCostum(thisContextSlug,newUrl+'test/openCostumMira');
      //  }else if(url.indexOf("test") > -1)
      //  {    
      //   location.href = newUrl.substring( 0, url.indexOf("test"))+'test/openCostumMira';
      //    // saveTplCostum(thisContextSlug,newUrl.substring( 0, url.indexOf("test"))+'test/openCostumMira');
      //  }else{
      //   location.href = newUrl+'/test/openCostumMira';
      //    // saveTplCostum(thisContextSlug,newUrl+'/test/openCostumMira');
      //  }
  }); 
}

function existCms(ide,tplSelected,backup){
  if (backup == "false") {
   // alert(backup);
    params = {
      "contextId" : thisContextId
    };
    $.ajax({
      type : 'POST',
      data : params,
      url : baseUrl+"/costum/getcms/getcmsexistaction",
      dataType : "json",
      async : false,
      success : function(data){
        mylog.log(data);
        if ((data.element).length==0){  
         duplicateCms(ide,tplSelected);
         saveTplUser(tplSelected,"using");
         localStorage.setItem("backup", tplUsingId);
         saveTplCostum ("costumize");
       }
       else{   
        bootbox.confirm("Voullez-vous vraiment changer votre template?",
          function(result){
            if (!result) {
             return;
           } 
           else {
            $(data.element).each(function(key,value){
                    //deleteCms(value.id);
                  });
            duplicateCms(ide,tplSelected);
            saveTplUser(tplSelected,"using");
            localStorage.setItem("backup", tplUsingId);
            urlCtrl.loadByHash(location.hash);
          }
        });            
      }
    }
  });
  }else if (backup == "inUse"){     
    alert("Votre template actuel");
  }else{
        //Only switch to tpl backup if it has a backup
        saveTplUser(tplSelected,"using");
        localStorage.setItem("backup", tplUsingId);
        //alert(backup);
      }
    }
    function coExistCms(idCms,costumSlug){
      params = {
        "contextId" : thisContextId
      };
      $.ajax({
        type : 'POST',
        data : params,
        url : baseUrl+"/costum/getcms/getcmsexistaction",
        dataType : "json",
        async : false,
        success : function(data){
          if ((data.element).length==0){ 
           saveTplCostum("costumize");
         }
         else{   
          bootbox.confirm("Voullez-vous vraiment changer votre template?",
            function(result){
              if (!result) {
               return;
             } 
             else {
              $(data.element).each(function(key,value){
                mylog.log(value.id);
               // deleteCms(value.id);
             });
              saveTplCostum(costumSlug);              
              saveTplUser(tplUsingId,"backup");
            }
          });            
        }
      }
    });
    }

    function switchTemplate(tplSelected){ 
      if ("<?= $this->costum['slug'] ?>" != "costumize") {
        saveTplUser(tplSelected,"using");
        newSlug = {
          collection : '<?= ($this->costum["contextType"]); ?>',
          id : '<?= ((string) $el["_id"]); ?>',
          path : "costum.slug",
          value : "costumize"
        }
        dataHelper.path2Value( newSlug, function(params) { 
          urlCtrl.loadByHash(location.hash);
        } ); 
      }else{
        saveTplUser(tplSelected,"using");
        saveTplUser(tplUsingId,"backup");
        urlCtrl.loadByHash(location.hash);
      }      
    }

    function deleteCms(ide){
      $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
      var id = ide;
      var type = "cms";
      var urlToSend = baseUrl+"/co2/element/delete/type/"+type+"/id/"+id;
      $.ajax({
        type: "POST",
        url: urlToSend,
        dataType : "json"
      })
      .done(function (data) {
        if ( data && data.result ) {
          toastr.success("Elément effacé");
          $("#"+type+id).remove();
          //location.href = newUrl+'test/openCostumMira';
        } else {
         toastr.error("something went wrong!! please try again.");
       }
     });
    }

    function removeCmsStat(ide){
      for (var i = 0; i < ide.length; i++) {
        params = {
          "ider" : ide[i]
        };
        tplCtx = {};
        tplCtx.id = ide[i];
        tplCtx.path = "haveTpl";
        tplCtx.collection = "cms"; 
        tplCtx.value = "true";
        dataHelper.path2Value( tplCtx, function(params) {
          toastr.success("Changement enregistré!");
        }); 
      }
    }

//tplId = (string)$template["_id"];

function duplicateCms(ide, tplSelected){  
  for (var i = 0; i < ide.length; i++) {
    params = {
      "ider" : ide[i]
    };
    $.ajax({
      type : 'POST',
      data : params,
      url : baseUrl+"/costum/getcms/getcmsaction",
      dataType : "json",
      async : false,
      success : function(data){
        mylog.log("success", data);
        delete data._id;
        delete data.parent;
        tplCtx.collection ="cms";
        tplCtx.path = "";
        tplCtx.value = data;            
        tplCtx.value.page = "<?= $page?>";
        tplCtx.value.parent={};
        tplCtx.value.parent[thisContextId] = {
          collection : "organizations", 
          name : thisContextSlug
        }; 
        tplCtx.value.tplParent = tplSelected;                      
        if(typeof tplCtx.value == "undefined")
          toastr.error("value cannot be empty!");
        else {
          mylog.log("activeForm save tplCtx",tplCtx);
          dataHelper.path2Value( tplCtx, function(params) {
            toastr.success("Template bien ajouter");
            $("#ajax-modal").modal("hide");
            $("#choix").hide();
          } );
        }
        mylog.log(tplCtx);
      }
    });
  }
}



/* template page statique*/
var listStaticPage = <?= json_encode($listStaticPage)?>;
function saveTemplateStatic(tplId){
  $.each( listStaticPage , function(k,valeur) { 
    checkTemplateStatic(valeur,tplId);
  });
}
function checkTemplateStatic(nomPage,tplId){
  params = {
    "contextId" : thisContextId,
    "idTplParent" : tplId,
    "nameTpl" :nomPage
  };
  $.ajax({
    type : 'POST',
    data : params,
    url : baseUrl+"/costum/blockcms/getlisttemplatestaticaction",
    dataType : "json",
    async : false,
    success : function(data){
      mylog.log("listetplStatic", data);
        if((data.element).length == 0){
         createTemplateStatic(nomPage,tplId);
       }
       else{
        $(data.element).each(function(key,valuecms){
         saveChangeTemplateStatic(nomPage,valuecms.id);
       });
      }

    }
  })
}
function createTemplateStatic(nomPage,tplId){
  alert(tplId);
  mylog.log("liste page statique", listStaticPage);
  params = {
    "contextId" : thisContextId,
    "page" : nomPage
  };
  $.ajax({
    type : 'POST',
    data : params,
    url : baseUrl+"/costum/blockcms/getcmsstaticexistaction",
    dataType : "json",
    async : false,
    success : function(data){
      mylog.log("listeCms", data.element);
      var tplCtxStatic = {};
      delete tplCtxStatic.id;
      tplCtxStatic.collection ="cms";
      tplCtxStatic.value = {
        name : nomPage,
        type :"template",
        tplParent : tplId
      };
      tplCtxStatic.value.parent={};
      tplCtxStatic.value.parent[thisContextId] = {
        collection : "organizations", 
        name : "<?php echo $tpl ?>"
      };
      tplCtxStatic.value.cmsList =[];
      $(data.element).each(function(key,valuecms){
       tplCtxStatic.value.cmsList.push(valuecms.id);
     });
      if(typeof tplCtxStatic.value == "undefined")
        toastr.error("value cannot be empty!");
      else {
        mylog.log("activeForm save static tplCtx",tplCtxStatic);
        dataHelper.path2Value( tplCtxStatic, function(params) {
          toastr.success("Template page static bien ajouter");
          $("#ajax-modal").modal("hide");
        } );
      }
    }
  });
}
function saveChangeTemplateStatic(nomPage,id){
  //alert(id);
  params = {
    "contextId" : thisContextId,
    "page" : nomPage
  };
  $.ajax({
    type : 'POST',
    data : params,
    url : baseUrl+"/costum/blockcms/getcmsstaticexistaction",
    dataType : "json",
    async : false,
    success : function(data){
      mylog.log("listeCms", data.element);
      tplCtx.id = id;
      tplCtx.collection ="cms";
      tplCtx.path="allToRoot";
      tplCtx.value = {};
      tplCtx.value.cmsList =[];
      $(data.element).each(function(key,valuecms){
       tplCtx.value.cmsList.push(valuecms.id);
     });
      if(typeof tplCtx.value == "undefined")
        toastr.error("value cannot be empty!");
      else {
        mylog.log("save static tplCtx",tplCtx);
        dataHelper.path2Value( tplCtx, function(params) {
         mylog.log("save static tplCtx1",tplCtx);
         toastr.success("Modification bien enregistrer");
         $("#ajax-modal").modal("hide");
       } );
      }
    }
  });

}
jQuery(document).ready(function() {
  if (localStorage.getItem("backup")) {
   saveTplUser(localStorage.getItem("backup"),"backup");
   localStorage.removeItem("backup")
 }
 mylog.log("render","/modules/costum/views/tpls/saveTemplate.php");
 $(".pop").on("click", function() {
           $('#imagepreview').attr('src', '<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/blockCmsImg/'+$(this).data("img")); // here asign the image to the modal when the user click the enlarge link
           $('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
         });

 $("#recentlyTpl").hide();
 $("#listeTemplate").hide();
 $(".btnAllTpl").click(function(){
  $("#allTpl").show();
  $("#recentlyTpl").hide();
});
 $(".btnRctlyTpl").click(function(){
  $("#allTpl").hide();
  $("#recentlyTpl").show();
});

 $(".saveThisTpls").click(function(){
  if (<?php echo json_encode($cmsId) ?> == "") {
    alert("Vous ne pouvez pas enregistrer un template vide! Veuillez ajouter au moins une block")
  }else{
   var tplCtx = {};
   var activeForm = {
    "jsonSchema" : {
      "title" : "Enregistrement du template",
      "type" : "object",
      onLoads : {
        onload : function(data){
          $(".parentfinder").css("display","none");
        }
      },
      "properties" : {
        name : {
          label: "Nom du template",
          inputType : "text",
          value : "<?php echo $nameTpl ?>"
        },
        type : { 
          "inputType" : "hidden",
          value : "template" 
        },
        img : { 
          "inputType" : "hidden",
          value : "" 
        },
        Screenshot : {
          "inputType" : "uploader",
          "label" : "Screenshot (5Mb max)",
          "showUploadBtn" : false,
          "docType" : "file",
          "itemLimit" : 15,
          "contentKey" : "file",
          "order" : 8,
          "domElement" : "documentationFile",
          "placeholder" : "Le pdf",
          "afterUploadComplete" : null,
          "template" : "qq-template-manual-trigger",
          "filetypes" : [
          "png","jpg","jpeg","gif"
          ]
        },
        description : {
          label: "Déscription",
          inputType : "text",
          value : ""
        }
      }
    }
  };          

  activeForm.jsonSchema.afterBuild = function(){
    dyFObj.setMongoId(activeForm,function(){
      uploadObj.gotoUrl
    });
  };

  tplCtx.collection = "cms";
  activeForm.jsonSchema.save = function () {
    tplCtx.value = {};
    $.each( activeForm.jsonSchema.properties , function(k,val) { 
      tplCtx.value[k] = $("#"+k).val();
    });
    tplCtx.value.parent={};
    tplCtx.value.parent[thisContextId] = {
      collection : "organizations", 
      name : "<?php echo $tpl ?>"
    };
    tplCtx.value.cmsList = <?php echo json_encode($cmsId) ?>;
    mylog.log(tplCtx);
    tplCtx.value.tplsUser={};
    tplCtx.value.tplsUser[thisContextId] = {
     "<?= $page?>": "using"
   };
   if(typeof tplCtx.value == "undefined")
    toastr.error('value cannot be empty!');
  else {
    mylog.log("activeForm save tplCtx",tplCtx);
    dataHelper.path2Value( tplCtx, function(params) {
      toastr.success("Elément bien ajouter");
      $("#ajax-modal").modal('hide');
      //saveThisCostum ();
    } );
    saveTplUser(tplUsingId,"backup");
    removeCmsStat(<?php echo json_encode($cmsId) ?>);
    urlCtrl.loadByHash(location.hash);
  }
}
dyFObj.openForm( activeForm );
}
});
 $(".saveChangeTpl").click(function(){
    var activeForm = {
      "jsonSchema" : {
        "title" : "Enregistrement du template",
        "type" : "object",
        onLoads : {
          onload : function(data){
            $(".parentfinder").css("display","none");
          }
        },
        "properties" : {
          img : { 
            "inputType" : "hidden",
            value : "" 
          },
          name : {
            label: "Nom du template",
            inputType : "text",
            value : "<?php echo $nameTpl ?>"
          },
          Screenshot : {
            "inputType" : "uploader",
            "label" : "Screenshot (5Mb max)",
            "showUploadBtn" : false,
            "docType" : "file",
            "itemLimit" : 15,
            "contentKey" : "file",
            "order" : 8,
            "domElement" : "documentationFile",
            "placeholder" : "Le pdf",
            "afterUploadComplete" : null,
            "template" : "qq-template-manual-trigger",
            "filetypes" : [
            "png","jpg","jpeg","gif"
            ]
          },
          description : {
            label: "Description",
            inputType : "text",
            value : "<?php echo $descriptionTpl ?>"
          }
        }
      }
    };          
    activeForm.jsonSchema.afterBuild = function(){
      dyFObj.setMongoId(activeForm,function(){
        uploadObj.gotoUrl
      });
    };
    tplCtx.collection = "cms";
    activeForm.jsonSchema.save = function () {
      tplCtx.id = tplUsingId;
      tplCtx.path = "allToRoot";
      tplCtx.value = {};
      tplCtx.value.app = <?php echo json_encode($app) ?>;
      $.each( activeForm.jsonSchema.properties , function(k,val) { 
        tplCtx.value[k] = $("#"+k).val();
      });
      tplCtx.value.cmsList = <?php echo json_encode($cmsId) ?>;
      if(typeof tplCtx.value == "undefined")
        toastr.error('value cannot be empty!');
      else {
        mylog.log("activeForm save tplCtx",tplCtx);
        dataHelper.path2Value( tplCtx, function(params) {
          toastr.success("Element bien ajouter");
          $("#ajax-modal").modal('hide'); 
          saveTemplateStatic(tplUsingId);             
          urlCtrl.loadByHash(location.hash);
        } );
        removeCmsStat(<?php echo json_encode($cmsId) ?>);
      }
    }
    dyFObj.openForm( activeForm );
});


 $(".tryOtherTpl").click(function(){
  $("#listeTemplate").show();
  smallMenu.open($("#listeTemplate"));

});

 $(".editBtn").off().on("click",function() { 
  var activeForm = {
    "jsonSchema" : {
      "title" : "Template config",
      "type" : "object",
      "properties" : {

      }
    }
  };
  if(configDynForm.jsonSchema.properties[ $(this).data("key") ])
    activeForm.jsonSchema.properties[ $(this).data("key") ] = configDynForm.jsonSchema.properties[ $(this).data("key") ];
  else
    activeForm.jsonSchema.properties[ $(this).data("key") ] = { label : $(this).data("label") };

  if($(this).data("label"))
    activeForm.jsonSchema.properties[ $(this).data("key") ].label = $(this).data("label");
  if($(this).data("type")){
    activeForm.jsonSchema.properties[ $(this).data("key") ].inputType = $(this).data("type");
    if($(this).data("type") == "textarea" && $(this).data("markdown") )
      activeForm.jsonSchema.properties[ $(this).data("key") ].markdown = true;
  }


  tplCtx.id = contextData.id;
  tplCtx.collection = contextData.type;
  tplCtx.key = $(this).data("key");
  tplCtx.path = $(this).data("path");

  activeForm.jsonSchema.save = function () {  
    tplCtx.value = $( "#"+tplCtx.key ).val();
    mylog.log("activeForm save tplCtx",tplCtx);
    if(typeof tplCtx.value == "undefined")
      toastr.error('value cannot be empty!');
    else {
      dataHelper.path2Value( tplCtx, function(params) { 
        $("#ajax-modal").modal('hide');
                        // urlCtrl.loadByHash(location.hash);
                      } );
    }

  }
  dyFObj.openForm( activeForm );
});

   $(".deleteLine").off().on("click",function (){
    $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
    var id = $(this).data("id");
    var type = $(this).data("collection");
    var urlToSend = baseUrl+"/co2/element/delete/type/"+type+"/id/"+id;

    bootbox.confirm(trad.areyousuretodelete,
      function(result){
        if (!result) {
          return;
        } 
        else {
          $.ajax({
            type: "POST",
            url: urlToSend,
            dataType : "json"
          })
          .done(function (data) {
            if ( data && data.result ) {
              toastr.success("Element effacé");
              $("#"+type+id).remove();
            } else {
             toastr.error("something went wrong!! please try again.");
           }
           urlCtrl.loadByHash(location.hash);
         });
        }
      }
      );
  });

   $(".editTpl").off().on("click",function() { 
    var activeForm = {
      "jsonSchema" : {
        "title" : "Edit Question config",
        "type" : "object",
        "properties" : {}
      }
    };

    $.each( formTpls [ $(this).data("key") ] , function(k,val) { 
      mylog.log("formTpls",k,val); 
      activeForm.jsonSchema.properties[ k ] = {
        label : k,
        value : val
      };
    });

    mylog.log("formTpls activeForm.jsonSchema.properties",activeForm.jsonSchema.properties); 
    tplCtx.id = $(this).data("id");
    tplCtx.key = $(this).data("key");
    tplCtx.collection = $(this).data("collection");            
    tplCtx.path = $(this).data("path");

    activeForm.jsonSchema.save = function () {  
      tplCtx.value = {};
      $.each( formTpls [ tplCtx.key ] , function(k,val) { 
        tplCtx.value[k] = $("#"+k).val();
      });
      mylog.log("save tplCtx",tplCtx);
      if(typeof tplCtx.value == "undefined")
        toastr.error('value cannot be empty!');
      else {
        dataHelper.path2Value( tplCtx, function(params) { 
          $("#ajax-modal").modal('hide');
        } );
      }

    }
    dyFObj.openForm( activeForm );
  });

   //check if new costum
   if(localStorage.getItem("newCostum"+thisContextSlug) == thisContextSlug){
      bootbox.confirm({
          message: "<h5 class='text-success text-center'>Bienvenue sur votre nouvel costum !</h5>"+
                    "<p class='text-success text-center'>Vous avez <b>4 étapes</b> pour créer votre costum :</p>"+
                    "<h6 class='text-center'><u class='text-danger'>Etape 1:</u> Choisir un template</h6>"+
                    "<h6 class='text-center'><u class='text-danger'>Etape 2:</u> Ajouter du section ou bloc CMS</h6>"+
                    "<h6 class='text-center'><u class='text-danger'>Etape 3:</u> Ajouter des APP ou MENU</h6>"+
                    "<h6 class='text-center'><u class='text-danger'>Etape 4:</u> Ajouter de données dans le bloc CMS</h6>",
          buttons: {
              confirm: {
                  label: 'Ok',
                  className: 'btn-success'
              },
              cancel: {
                  label: 'No',
                  className: 'hidden'
              }
          },
          callback: function (result) {
              if(result){
                  localStorage.removeItem("newCostum"+thisContextSlug);
                  $(".tpl-engine-btn-list ul li.a").addClass("btn-pulse").on('click',function(){
                      $(this).removeClass("btn-pulse");
                      $(".openListTpls").addClass("btn-pulse").on('click',function(){
                          $(this).removeClass("btn-pulse");
                          $(".tpl-engine-btn-list ul li.g").addClass("btn-pulse").on('click',function(){
                              $(this).removeClass("btn-pulse");
                          })
                      })
                  });

              }
          }
      });
   }

});

var hideOnPreview = '#saveTemplate,.editSectionBtns,.editBtn,.editThisBtn,.editQuestion,.addQuestion,.deleteLine,.previewTpl,.openListTpls,.handleDrag';
var previewMode = false;
window.onhashchange = function() {
  if($(hideOnPreview).is(':visible') !=true){
    $(hideOnPreview).fadeIn(); 
    previewMode = false;
  }
}
$(document).keyup(function(e) {
  if (e.key === "Escape") { // escape key maps to keycode `27`
      $(hideOnPreview).fadeIn(); 
      previewMode = false;
    }
  });

function previewTpl () { 
  $(hideOnPreview).fadeOut();
  previewMode = true;
  if(localStorage.getItem("forgetPreview") != "true"){
    bootbox.alert("<p class='text-center'>Appuyer sur la touche <kbd>Escape</kbd> pour désactiver le mode preview</p>"+
      "<input type='checkbox' name='forgetPreview' id='forgetPreview'>  Ne plus repéter !",function(){
        if($("#forgetPreview").prop('checked') == true)
          localStorage.setItem("forgetPreview","true");
        else
          localStorage.removeItem("forgetPreview");
    });
  }
};


$(".block-parent").mouseover(function(){
  if(previewMode==false){
     $(this).find('.handleDrag').show();
  } 
});
$(".block-parent").mouseout(function(){
    if(previewMode==false){
        $(this).find('.handleDrag').hide();
    } 
})  

</script>
