<?php 
$keyTpl = "blockOrganisation";
$paramsData=[
    "title" => "Liste des Organisations",
    "colorTitle" =>"",
    "borderLogo1" => "#f33",
    "borderLogo2" => "#000",
    "aparence" => "50%",
    "role" => "Organisateur"
];


if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}

 $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;

    HtmlHelper::registerCssAndScriptsFiles(["/js/blockcms/slick.js"], $assetsUrl);
?>

<style type="text/css">

.containblock_<?= $kunik?> {
    margin-bottom: 30px;
}
.containblock_<?= $kunik?> h2{
  text-align:center;
  padding: 20px;
  color: <?= $paramsData["colorTitle"]?>;
}
/* Slider */

.containblock_<?= $kunik?> .slick-slide {
    margin: 0px 20px;
}

.containblock_<?= $kunik?> .slick-slide img {
    width: 100%;
}

.containblock_<?= $kunik?> .slick-slider
{
    position: relative;
    display: block;
    box-sizing: border-box;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
            user-select: none;
    -webkit-touch-callout: none;
    -khtml-user-select: none;
    -ms-touch-action: pan-y;
        touch-action: pan-y;
    -webkit-tap-highlight-color: transparent;
}

.containblock_<?= $kunik?> .slick-list
{
    position: relative;
    display: block;
    overflow: hidden;
    margin: 0;
    padding: 0;
    padding-bottom: 10px;
}
.containblock_<?= $kunik?> .slick-list:focus
{
    outline: none;
}
.containblock_<?= $kunik?> .slick-list.dragging
{
    cursor: pointer;
    cursor: hand;
}

.containblock_<?= $kunik?> .slick-slider .slick-track,
.containblock_<?= $kunik?> .slick-slider .slick-list
{
    -webkit-transform: translate3d(0, 0, 0);
       -moz-transform: translate3d(0, 0, 0);
        -ms-transform: translate3d(0, 0, 0);
         -o-transform: translate3d(0, 0, 0);
            transform: translate3d(0, 0, 0);
}

.containblock_<?= $kunik?> .slick-track
{
    position: relative;
    top: 0;
    left: 0;
    display: block;
}
.containblock_<?= $kunik?> .slick-track:before,
.containblock_<?= $kunik?> .slick-track:after
{
    display: table;
    content: '';
}
.containblock_<?= $kunik?> .slick-track:after
{
    clear: both;
}
.containblock_<?= $kunik?> .slick-loading .slick-track
{
    visibility: hidden;
}

.containblock_<?= $kunik?> .slick-slide
{
    display: none;
    float: left;
    height: 100%;
    min-height: 1px;
}
[dir='rtl'] .slick-slide
{
    float: right;
}
.containblock_<?= $kunik?> .slick-slide img
{
    display: block;
}
.containblock_<?= $kunik?> .slick-slide.slick-loading img
{
    display: none;
}
.containblock_<?= $kunik?> .slick-slide.dragging img
{
    pointer-events: none;
}
.containblock_<?= $kunik?> .slick-initialized .slick-slide
{
    display: block;
}
.containblock_<?= $kunik?> .slick-loading .slick-slide
{
    visibility: hidden;
}
.containblock_<?= $kunik?> .slick-vertical .slick-slide
{
    display: block;
    height: auto;
    border: 1px solid transparent;
}
.containblock_<?= $kunik?> .slick-arrow.slick-hidden {
    display: none;
}


@media (min-width: 1200px) {
    .containblock_<?= $kunik?> .main .wrapper {
        width: 145px;
        height: 145px;
    }
    .containblock_<?= $kunik?> .main {
        position:relative;
        width:135px;
        height:135px;
    }
    .containblock_<?= $kunik?> .main img, .containblock_<?= $kunik?> .main .info, .containblock_<?= $kunik?> .main p {
        width:125px;
        height:125px;
    }
}

.containblock_<?= $kunik?> .main {
    position:relative;
    width:140px;
    height:140px;
    border-radius:<?= $paramsData["aparence"]?>;
}
.containblock_<?= $kunik?> .main .wrapper {
    width: 150px;
    height: 150px;
    border-width: 5px;
    border-style: solid;
    border-color: <?= $paramsData["borderLogo2"]?> <?= $paramsData["borderLogo1"]?> <?= $paramsData["borderLogo1"]?> <?= $paramsData["borderLogo2"]?>;
    border-radius: <?= $paramsData["aparence"]?>;
    transition: all 0.8s ease-in-out 0s;
    posistion:absolute;
    top:0;
    left:0;
}

.containblock_<?= $kunik?> .main img, .containblock_<?= $kunik?> .main .info, .containblock_<?= $kunik?> .main p {
    position:absolute;
    top:5px;
    right:0;
    bottom:0;
    left:5px;
    border-radius:<?= $paramsData["aparence"]?>;
    width:140px;
    height:140px;
    transition: all 0.8s ease-in-out 0s;
}

@media (max-width: 1199px) {
    .containblock_<?= $kunik?> .main .wrapper {
        width: 115px;
        height: 115px;
    }
    .containblock_<?= $kunik?> .main {
        position:relative;
        width:105px;
        height:105px;
    }
    .containblock_<?= $kunik?> .main img, .containblock_<?= $kunik?> .main .info, .containblock_<?= $kunik?> .main p {
        width:105px;
        height:105px;
    }
}
@media (max-width: 991px) {
    .containblock_<?= $kunik?> .main .wrapper {
        width: 75px;
        height: 75px;
    }
    .containblock_<?= $kunik?> .main {
        position:relative;
        width:65px;
        height:65px;
    }
    .containblock_<?= $kunik?> .main img, .containblock_<?= $kunik?> .main .info,  .containblock_<?= $kunik?> .main p {
        width:65px;
        height:65px;
    }
}

.containblock_<?= $kunik?> .main .info {
    text-align:center;
   /* padding:10px;*/
    /*padding-top:40px;*/
    background:#fff;
    transition: all 0.8s ease-in-out 0s;
    backface-visibility:hidden;
    opacity:0;
    
}
.containblock_<?= $kunik?> .main h4, .containblock_<?= $kunik?> .main p{
    word-break: break-all;
    text-transform: none;
    border-radius: 50%;
    padding: 5px;
    font-size: 14px;
    text-align: center;
    font-weight: 700;
    padding: 5px;
}


.containblock_<?= $kunik?> .main a:hover  .wrapper {
    transform:rotate(180deg);
    transition: all 0.8s ease-in-out 0s;
}

.containblock_<?= $kunik?> .main a:hover img, .containblock_<?= $kunik?> .main a:hover p {
    opacity:0;
    transition: all 0.8s ease-in-out 0s;
}

.containblock_<?= $kunik?> .main a:hover .info {
    opacity:1;
    transition: all 0.8s ease-in-out 0s;
}
</style>

<div class="containblock_<?= $kunik?>">
    <div class="container">
      <h2 class="title"><?= $paramsData["title"]?></h2>
          <?php
            $orgas = Organization::getMembersByOrganizationId($this->costum["contextId"],"organizations");
            $orga = array();

            $where = array("links.memberOf.".$this->costum["contextId"].".roles" => array('$in' => array($paramsData["role"])));
            foreach ($orgas as $keyElt => $valueElt) {
                $isset = Element::getElementSimpleById($keyElt,"organizations",$where,["name", "profilMediumImageUrl"]);
                 if ($isset != null) {
                     array_push($orga, $isset);
                 }
                
            }
            if (!empty($orga)) {
            ?>
            <section class="orga-logo slider">
            <?php
                foreach ($orga as $key => $value) { ?>
              <div class="slide">
                <div class="main clearfix">
                    <a href="#">
                        <div class="wrapper clearfix"></div>

                        <?php if (empty($value["profilMediumImageUrl"]) || $value["profilMediumImageUrl"]== null ) {?>
                            <p><?=  $value["name"];?></p>
                        <?php
                        }else{
                            $img = Yii::app()->request->baseUrl.$value["profilMediumImageUrl"];
                        ?>
                        <img src="<?=  $img ?>">
                        <?php
                    }
                    ?>
                        
                        <div class="info">
                            <h4><?=  $value["name"];?></h4>
                        </div>
                    </a>
                </div>
              </div>
              <?php } ?>
            </section>
          <?php }else { ?>

            <div class="text-center padding-30">
                Aucune Organisation
            </div>
          <?php } ?>

    </div>
</div>
<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
$(document).ready(function(){
    $('.orga-logo').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1500,
        arrows: false,
        dots: false,
        pauseOnHover: true,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 4
            }
        }, {
            breakpoint: 520,
            settings: {
                slidesToShow: 3
            }
        }]
    });
    // "title" => "Liste des Organisations",
    // "colorTitle" =>"",
    // "borderLogo1" => "#f33",
    // "borderLogo2" => "#000",
    // "aparence" => "50%"
    sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {    
                "title" : "Configurer votre section",
                "description" : "Personnaliser votre section",
                "icon" : "fa-cog",
                "properties" : {
                    "title" : {
                        label : "titre",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.title
                    },
                    "colorTitle":{
                        label : "Couleur du titre",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.colorTitle
                    },
                    role : {
                        label : "Role",
                        inputType : "select",
                        options : {
                            "Organisateur" : "Organisateur",
                            " Partenaire" : " Partenaire",
                            "Financeur" : "Financeur",
                            "Sponsor" : "Sponsor",
                            "Président" : "Président",
                            "Directeur" : "Directeur",
                            "Conférencier" : "Conférencier",
                            "Intervenant" : "Intervenant"
                        },
                        values : sectionDyf.<?php echo $kunik?>ParamsData.role
                    },
                    aparence : {
                        label : "Forme du logo",
                        inputType : "select",
                        options : {
                            "50%" : "Rond",
                            "0%" : "Carré",
                        },
                        values : sectionDyf.<?php echo $kunik?>ParamsData.aparence
                    },
                    "borderLogo1":{
                        label : "Premier Couleur du bordur Logo",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.borderLogo1
                    },
                    "borderLogo2":{
                        label : "Deuxieme Couleur du bordur Logo",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.borderLogo2
                    }
                },
                beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
                save : function () {  
                    tplCtx.value = {};

                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        tplCtx.value[k] = $("#"+k).val();
                    });

                    console.log("save tplCtx",tplCtx);

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                      dataHelper.path2Value( tplCtx, function(params) {
                        dyFObj.commonAfterSave(params,function(){
                          toastr.success("Élément bien ajouter");
                          $("#ajax-modal").modal('hide');
                          urlCtrl.loadByHash(location.hash);
                        });
                      } );
                    }
                }
            }

        };
        mylog.log("paramsData",sectionDyf);
        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
});
</script>

