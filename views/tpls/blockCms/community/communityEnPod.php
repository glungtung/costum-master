<?php
$cssAnsScriptFilesModule = array(
      '/js/default/profilSocial.js'
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->assetsUrl);
?>
<?php 
  $loremIpsum = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting";
  $keyTpl ="communityEnPOd";
  $paramsData = [ 
    "title"=>"Ma liste",
    "listIconColor" =>"#008037",
    "description"=>$loremIpsum,
    "descriptionColor" =>"#565656",

  ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) {
        $paramsData[$e] = $blockCms[$e];
      }
    }
  } 

 ?>

<style>
  .container<?php echo $kunik ?> .title-color{
    position: relative;
  }
  .container<?php echo $kunik ?> .text-color{

  }
  .container<?php echo $kunik ?> h2{
    padding-top:15px;
  }
  .description<?php echo $kunik ?> {
      list-style: none;
      margin: 0;
      padding: 10px 0;
      z-index: 99;
  }
  .description<?php echo $kunik ?> p {
      position: relative;
      padding: 5px 10px;
      border: 0px solid;
      /*background: RGBA(255, 255, 255, 0.72);*/
      word-wrap: break-word;
      /*border-radius: 50%;*/
      margin: 0 15px 0px 10px;
      /*box-shadow: -5px 5px 10px -5px rgba(0, 0, 0, 0.4);*/
  }
  .description<?php echo $kunik ?> li {
    margin-bottom: 20px
  }
  /*.list<?php //echo $kunik ?> p:before {
      content: "";
      display: block;
      width: 15px;
      height: 15px;
      border: 2px solid;
      border-radius: 50%;
  }*/
  .div-icon-right{
    height: 400px
  }
  #filters-nav {
    display:none !important;
  }
</style>
<style>
  .container<?php echo $kunik ?> .block-edit-delete{
    display: none;
    position: absolute;
    bottom:0%;
    left: 50%;
    transform: translate(-50%,0);
    z-index: 999;

  }
  .container<?php echo $kunik ?>:hover .block-edit-delete{
    display: block;
    cursor: pointer;
  }
  /*.container<?php echo $kunik ?> #central-container{
    height: auto !important;
  }*/
</style>

<div class="container<?php echo $kunik ?> col-md-12">
  <h2 class="title title-color"><?php echo $paramsData["title"] ?></h2>

  <div class="markdown description description<?php echo $kunik ?> col-md-12 text-explain"><?php echo $paramsData["description"] ?></div>
</div>
<div id="community" class="col-sm-12 col-md-12 col-xs-12 no-padding" style="max-width:100%; float:left;">
    <div id="central-container" style="min-height: auto !important;"></div> 
</div>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {    
            "title" : "Configurer votre section",
            "description" : "Personnaliser votre liste",
            "icon" : "fa-cog",
            
            "properties" : {
            
            "title" : {
                "inputType" : "text",
                "label" : "Titre",
                
                values :  sectionDyf.<?php echo $kunik ?>ParamsData.title
            },
            "description" :{
                  "inputType" : "textarea",
                  "label" : "description",
                  "markdown" : true,
                  value : sectionDyf.<?php echo $kunik ?>ParamsData.description 
              },
            },
            beforeBuild : function(){
                uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;

                if(k == "items")
                  tplCtx.value[k] = data.items;
                  mylog.log("andrana",data.items)
              });
              console.log("save tplCtx",tplCtx);

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                dataHelper.path2Value( tplCtx, function(params) {
                  dyFObj.commonAfterSave(params,function(){
                    toastr.success("Élément bien ajouter");
                    $("#ajax-modal").modal('hide');
                    urlCtrl.loadByHash(location.hash);
                  });
                } );
              }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          $(this).html('<i class="fa fa-spinner fa-spin" aria-hidden="true"></i>')
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
    });
</script>

<script type="text/javascript">
  var hashUrlPage="#community";
  var connectTypeElement="<?php echo Element::$connectTypes[Organization::COLLECTION] ?>";
  var contextData = {type:thisContextType,id:thisContextId,name:"Le collectif éco-citoyen"};

  var openEdition = false;
  var canEdit = false; 

  jQuery(document).ready(function() {
    setTimeout(function(){ 
      pageProfil.views.directory = function(callBack){
        mylog.log("pageProfil.views.directory");
        var dataIcon = (!notEmpty(pageProfil.params.dir)) ? "users" : $(".smma[data-type-dir="+pageProfil.params.dir+"]").data("icon");
        pageProfil.params.dir=(!notEmpty(pageProfil.params.dir)) ? links.connectType[contextData.type] : pageProfil.params.dir;
        var sub=(!notEmpty(pageProfil.params.sub)) ? "" : "/sub/"+pageProfil.params.sub;

        getAjax('', baseUrl+'/'+moduleId+'/element/getdatadetail/type/'+thisContextType+
              '/id/'+thisContextId+'/dataName/'+pageProfil.params.dir+sub+'?tpl=json',
              function(data){ 
                var type = ($.inArray(pageProfil.params.dir, ["poi","ressources","vote","actions","discuss"]) >=0) ? pageProfil.params.dir : null;
                mylog.log("pageProfil.views.directory canEdit" , canEdit);
                var editLink=(typeof canEdit != "undefined" && canEdit) ? pageProfil.params.dir : null;
                mylog.log("pageProfil.views.directory edit" , canEdit);
                displayInTheContainer(data, pageProfil.params.dir, dataIcon, type, editLink);
                if(typeof mapCO != "undefined"){
                  mapCO.clearMap();
                        mapCO.addElts(data);
                        mapCO.map.invalidateSize();
                }
                coInterface.bindButtonOpenForm();

                /* if(typeof callBack != "undefined" && callBack != null)
                  callBack(); */
              }
        ,"html");
    }
  },1000);

    setTitle("Le collectif");
    getAjax("", baseUrl+"/"+moduleId+"/element/getdatadetail/type/"+thisContextType+"/id/"+thisContextId+"/dataName/members",
    function(data){
      displayInTheContainer(data,"guests","group",null,true);
    },
    "html");
  });
</script>