<?php
$keyTpl = "communityblock";

$paramsData = [ 
    "title"         => "La communautée en block",
    "icon"          => "",
    "color"         => "#000000",
    "background"    => "#FFFFFF"
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}

$element = array();
$i = 1;
?>
<style type="text/css">
    #community_<?= $kunik?> h1 {
        background-color: <?= $paramsData['background']; ?> ;
        color:<?= $paramsData['color']; ?>
    }
    #community_<?= $kunik?> .card .image{
        border-radius: 10px;
        box-shadow: 0vw 0vw 1vw -5px gray;
    }
    #community_<?= $kunik?> .card img{
        padding-top: 29px;
        padding-bottom: 29px;
    }
    #community_<?= $kunik?> .card p{
       margin-top:1vw;
       color:black
    }
   
    @media (max-width:768px){
        #community_<?= $kunik?> h1 {
            font-size: 20px;
        }
    }
</style>
<div id="community_<?= $kunik?>" class="container tplsCommunity ">
    <h1 class="text-center"> 
        <i class="fa <?= $paramsData['icon'] ?>"></i> 
        <?= $paramsData["title"]; ?> 
    </h1>
    <?php
    $community = Organization::getMembersByOrganizationId($this->costum["contextId"],"organizations");

    foreach ($community as $keyElt => $valueElt) {
        array_push($element,Element::getElementSimpleById($keyElt,"organizations",null,["name", "profilMediumImageUrl"]));
    }

    if (!empty($element)) {
        $costum = CacheHelper::getCostum();
        ?>
        <div id="communityblock" class="col-xs-12">
            <?php
            foreach ($element as $k => $v) {
                if (empty($v["profilMediumImageUrl"]) || $v["profilMediumImageUrl"]== null ) {
                    $img =  Yii::app()->getModule('costum')->assetsUrl."/images/templates/default_directory.png";
                }else{
                    $img = Yii::app()->baseUrl.$v["profilMediumImageUrl"];
                }

                if ($i <= 3) {
                    ?>
                    <div class="card">
                        <div class="card-color col-md-4">
                            <div class="info-card text-center">
                                <a class="<?= $v["name"]?> bold text-dark add2fav  lbh-preview-element" href="#page.type.organizations.id.<?= $v["_id"]?>">
                                    <div class="text-center image" >
                                            <img  class="img-responsive" src="<?= $img ?>"><br>
                                    </div>
                                    <p><?= $v["name"] ?> </p>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php
                    $i++;
                }else{

                }
            }
            ?>
        </div>
            <?php 
    }else{ ?>
            <div class="col-xs-12">Aucune organisation membre dans la communauté</div>
    <?php }?>
</div>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

    jQuery(document).ready(function(){

        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {    
                "title" : "Configurer votre section",
                "description" : "Personnaliser votre section e la carte de la communautée",
                "icon" : "fa-cog",
                "properties" : {
                    "title" : {
                        label : "Titre",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.title
                    },
                    icon : { 
                        label : "Icone",
                        inputType : "select",
                        options : {
                            "fa-newspaper-o"    : "Newspapper",
                            "fa-calendar " : "Calendar",
                            "fa-lightbulb-o "  :"Lightbulb"
                        },
                        values : sectionDyf.<?php echo $kunik ?>ParamsData.icon
                    },
                    "color" : {
                        label : "Couleur du titre",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.color
                    },
                    "background" : {
                        label : "Couleur du fond du titre",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.background
                    }
                },
                beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
                save : function () {  
                    tplCtx.value = {};

                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        tplCtx.value[k] = $("#"+k).val();
                    });
                    
                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                      dataHelper.path2Value( tplCtx, function(params) {
                        dyFObj.commonAfterSave(params,function(){
                          toastr.success("Élément bien ajouter");
                          $("#ajax-modal").modal('hide');
                          urlCtrl.loadByHash(location.hash);
                        });
                      } );
                    }

                }
            }
        };
        
        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";

            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
    });
</script>
