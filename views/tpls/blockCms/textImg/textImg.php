<style type="text/css">
  .seen-plus{
    background-color:white;
    color:#e6344d;
    padding:12px;
    border-radius:24px;
    top:5%;
    position:relative;
    font-size: 17px;
  }

  @font-face{
    font-family: "DINAlternate-Bold";
    src: url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/coevent/DINAlternate-Bold.ttf")
  }

  .dinalternate{
    font-family: 'DINAlternate-Bold';
  }

  .title{
    color: <?php echo @$color ?>;
    margin-top: 3vw;
  }

  .containInfo{
    text-align:left;
    position:absolute;
    height:550px;
    background-color: rgba(238, 48, 44, 0.5);
  }

</style>

<?php 
$structField = "structags";

if( count(Cms::getCmsByStruct($cmsList,$tag,$structField ))!=0 )
{   
    echo '<div style="margin-top:2vw;" class="no-padding row">';
        $result = Cms::getCmsByStruct($cmsList,$tag,$structField)[0];
        $img = PHDB::findOne(Document::COLLECTION, array("id" => (String) $result["_id"]));

        echo'<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center">';
            echo '<h1 style="color:black;">'.@$result["name"].'</h1>';
            echo '<span style="color:black !important;text-align:left;" class="markdown">'.@$result["description"].'</span>';
        echo '</div>';

        if (isset($img)) 
        {
            echo '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 no-padding">';
                echo '<img src="'.Yii::app()->baseUrl.'/upload/'.$img["moduleId"].'/'.$img["folder"].'/'.$img["name"].'" class="img-responsive">';
            echo '</div>';
        }
    echo "</div>";

    $edit = "update";
}
   echo '<center>';
      echo $this->renderPartial("costum.views.tpls.openFormBtn",
        array(
            'edit' => @$canEdit,
            'tag' => $tag,
            'id' => (string)@$result["_id"]),true);  

    // echo $this->renderPartial("costum.views.tpls.dynFormCostumCMS"); 
    // echo $this->renderPartial("costum.views.tpls.editTplBtns", ["canEdit" => $canEdit, "keyTpl"=>$keyTpl, "page" => @$page]);
    echo '</center>';
?>

<script type="text/javascript">
jQuery(document).ready(function() {
    mylog.log("render","costum.views.tpls.text",'tag : <?php echo $tag ?>');
    //OPTIM : ce code est répété autant de fois qu'il y a de btn 
    //il devrait etre sur l'appelant mais du il sera répété un peu partout 
    
    
    // /alert("costum.views.tpls.text");
    $(".editThisBtn").off().on("click",function (){
        mylog.log("editThisBtn");
        var id = $(this).data("id");
        var type = $(this).data("type");
        dyFObj.editElement(type,id,null,dynFormCostumCMS)
    });

    $(".createBlockBtn").off().on("click",function (){
        mylog.log("createBtn");
        dataObj = {parent : {} };
        dataObj.parent[userConnected._id.$id] = {collection : userConnected.type , name : userConnected.name}; 
        dataObj.structags = $(this).data("tag");

        dyFObj.openForm('cms',null,dataObj,null,dynFormCostumCMS);
    });

    $(".deleteThisBtn").off().on("click",function (){
        mylog.log("deleteThisBtn click");
          $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
          var btnClick = $(this);
          var id = $(this).data("id");
          var type = $(this).data("type");
          var urlToSend = baseUrl+"/co2/element/delete/type/"+type+"/id/"+id;
          
          bootbox.confirm(trad.areyousuretodelete,
            function(result) 
            {
                if (!result) {
                  btnClick.empty().html('<i class="fa fa-trash"></i>');
                  return;
                } else {
                  $.ajax({
                        type: "POST",
                        url: urlToSend,
                        dataType : "json"
                    })
                    .done(function (data) {
                        if ( data && data.result ) {
                          urlCtrl.loadByHash(location.hash);
                          toastr.info("élément effacé");
                          $("#"+type+id).remove();
                        } else {
                           toastr.error("something went wrong!! please try again.");
                        }
                    });
                }
            });

    });
});
</script>