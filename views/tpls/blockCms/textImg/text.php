<?php 
$structField = "structags";
if( count(Cms::getCmsByStruct($cmsList,@$tag,$structField))!=0 )
{   
    $p = Cms::getCmsByStruct($cmsList,@$tag,$structField)[0];
    echo '<h1 style="color:'.@$color.'">'.@$p["name"].'</h1>';
    echo "<div class='markdown'>".@$p["description"]."</div>";
    
    if(isset($p["documents"])){
          echo "<br/><h4>Documents</h4>";
          foreach ($p["documents"] as $key => $value) {
            $dicon = "fa-file";
            $fileType = explode(".", $p["name"])[1];
            if( $fileType == "png" || $fileType == "jpg" || $fileType == "jpeg" || $fileType == "gif" )
              $dicon = "fa-file-image-o";
            else if( $fileType == "pdf" )
              $dicon = "fa-file-pdf-o";
            else if( $fileType == "xls" || $fileType == "xlsx" || $fileType == "csv" )
              $dicon = "fa-file-excel-o";
            else if( $fileType == "doc" || $fileType == "docx" || $fileType == "odt" || $fileType == "ods" || $fileType == "odp" )
              $dicon = "fa-file-text-o";
            else if( $fileType == "ppt" || $fileType == "pptx" )
              $dicon = "fa-file-text-o";
            echo "<a href='".$p["path"]."' target='_blanck'><i class='text-red fa "+$dicon+"'></i> ".$p["name"]."</a><br/>";
          }
    }
              
    $edit ="update";
} 
else 
    $edit ="create";

echo "<center>";
  echo $this->renderPartial("costum.views.tpls.openFormBtn",
                                  array(
                                      'edit' => $edit,
                                      'tag' => @$tag,
                                      'page'  => @$page,
                                      'id' => (isset($p["_id"])) ? (string)$p["_id"] : null
                                   ),true);

  echo $this->renderPartial("costum.views.tpls.dynFormCostumCMS");
echo "</center>"; 
?>

<script type="text/javascript">
jQuery(document).ready(function() {
    mylog.log("render","costum.views.tpls.text",'tag : <?php echo @$tag ?>');

    $(".deleteThisBtn").off().on("click",function (){
        mylog.log("deleteThisBtn click");
          $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
          var btnClick = $(this);
          var id = $(this).data("id");
          var type = $(this).data("type");
          var urlToSend = baseUrl+"/co2/element/delete/type/"+type+"/id/"+id;
          
          bootbox.confirm(trad.areyousuretodelete,
            function(result){
                if (!result) {
                    btnClick.empty().html('<i class="fa fa-trash"></i>');
                    return;
                } else {
                    $.ajax({
                        type: "POST",
                        url: urlToSend,
                        dataType : "json"
                    })
                    .done(function (data) {
                        if ( data && data.result ) {
                          urlCtrl.loadByHash(location.hash);
                          toastr.success("élément supprimer");
                          $("#"+type+id).remove();
                        } else {
                           toastr.error("something went wrong!! please try again.");
                        }
                    });
                }
            });

    });
});
</script>