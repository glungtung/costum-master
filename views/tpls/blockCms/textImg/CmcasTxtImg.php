<style type="text/css">
  .seen-plus{
    background-color:white;
    color:#e6344d;
    padding:12px;
    border-radius:24px;
    top:5%;
    position:relative;
    font-size: 17px;
  }

  @font-face{
    font-family: "DINAlternate-Bold";
    src: url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/coevent/DINAlternate-Bold.ttf")
  }

  .dinalternate{
    font-family: 'DINAlternate-Bold';
  }

  .title{
    color: <?php echo @$color ?>;
    margin-top: 3vw;
  }

</style>

<?php 
$structField = "structags";

if( count(Cms::getCmsByStruct($cmsList,$tag,$structField))!=0 )
{   
    $p = Cms::getCmsByStruct($cmsList,$tag,$structField)[0];
    $img = PHDB::findOne(Document::COLLECTION, array("id" => (String) $p["_id"]));
    $description = (!empty($p["description"])) ? $p["description"] : "Aucune description";

    echo '<div class="row">';
      echo '<div class="text-left col-lg-5 col-md-5 col-sm-6 col-xs-10 div-r">';
          echo '<h3>'.@$p["name"].'</h3>';
          echo '<hr>';
          echo '<p>'.$description.'</p>';
      echo '</div>';
      echo '<div class="col-lg-7 col-md-7 col-sm-6 col-xs-2 div-l">';
          if (isset($img)) {
              echo '<div class="carousel-img" style="background-image: url('.Yii::app()->baseUrl.'/upload/'.$img["moduleId"].'/'.$img["folder"].'/'.$img["name"].');">';
              echo '</div>';
          }else{
              echo "Ajouter une image a votre";
          }
      echo '</div>';
    echo '</div>'; 


    if(isset($p["documents"])){
          echo "<br/><h4>Documents</h4>";
          foreach ($p["documents"] as $key => $value) {
            $dicon = "fa-file";
            $fileType = explode(".", $p["name"])[1];
            if( $fileType == "png" || $fileType == "jpg" || $fileType == "jpeg" || $fileType == "gif" )
              $dicon = "fa-file-image-o";
            else if( $fileType == "pdf" )
              $dicon = "fa-file-pdf-o";
            else if( $fileType == "xls" || $fileType == "xlsx" || $fileType == "csv" )
              $dicon = "fa-file-excel-o";
            else if( $fileType == "doc" || $fileType == "docx" || $fileType == "odt" || $fileType == "ods" || $fileType == "odp" )
              $dicon = "fa-file-text-o";
            else if( $fileType == "ppt" || $fileType == "pptx" )
              $dicon = "fa-file-text-o";
            echo "<a href='".$p["path"]."' target='_blanck'><i class='text-red fa "+$dicon+"'></i> ".$p["name"]."</a><br/>";
          }
    }
              
    $edit ="update";
} 
else 
    $edit ="create";


echo $this->renderPartial("costum.views.tpls.openFormBtn",
                                array(
                                    'edit' => $edit,
                                    'tag' => $tag,
                                    'id' => (isset($p["_id"])) ? (string)$p["_id"] : null
                                 ),true);

echo $this->renderPartial("costum.views.tpls.dynFormCostumCMS"); 
?>

<script type="text/javascript">
jQuery(document).ready(function() {
    mylog.log("render","costum.views.tpls.text",'tag : <?php echo $tag ?>');
    //OPTIM : ce code est répété autant de fois qu'il y a de btn 
    //il devrait etre sur l'appelant mais du il sera répété un peu partout 
    
    
    // /alert("costum.views.tpls.text");
    $(".editThisBtn").off().on("click",function (){
        mylog.log("editThisBtn");
        var id = $(this).data("id");
        var type = $(this).data("type");
        dyFObj.editElement(type,id,null,dynFormCostumCMS)
    });

    $(".createBlockBtn").off().on("click",function (){
        mylog.log("createBtn");
        dyFObj.openForm('cms',null,{structags:$(this).data("tag")},null,dynFormCostumCMS);
    });

    $(".deleteThisBtn").off().on("click",function (){
        mylog.log("deleteThisBtn click");
          $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
          var btnClick = $(this);
          var id = $(this).data("id");
          var type = $(this).data("type");
          var urlToSend = baseUrl+"/co2/element/delete/type/"+type+"/id/"+id;
          
          bootbox.confirm(trad.areyousuretodelete,
            function(result) 
            {
                if (!result) {
                  btnClick.empty().html('<i class="fa fa-trash"></i>');
                  return;
                } else {
                  $.ajax({
                        type: "POST",
                        url: urlToSend,
                        dataType : "json"
                    })
                    .done(function (data) {
                        if ( data && data.result ) {
                          toastr.info("élément effacé");
                          $("#"+type+id).remove();
                        } else {
                           toastr.error("something went wrong!! please try again.");
                        }
                    });
                }
            });

    });
});
</script>