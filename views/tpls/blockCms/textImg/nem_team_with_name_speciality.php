
<?php 
$keyTpl = "nem_team_with_name_speciality";
$content = $content;
$paramsData = [
    "title" => "OUR TEAM",
    "btnColor" => "#ffffff",
    "background_color" => "#108d6f",
    "borderColor"   => "#119c39"
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}
?>


<!-- ****************get image uploaded************** -->
<?php 
  $blockKey = (string)$blockCms["_id"];
 ?>
<style type="text/css">  
   

    .team<?php echo $kunik ?> .team-desc-cont {
	    display:inline-block;
	    float:none;
	    vertical-align: text-top;
	    text-align:center;
	    margin-right:-4px;
	    padding-bottom: 30px;
    }
    .team<?php echo $kunik ?> .row-centered {
	    text-align:center;
	}


    .team<?php echo $kunik ?> .btn-action-team {
        background-color: <?php echo $paramsData["background_color"]; ?>;
	    border: 1px solid <?php echo $paramsData["borderColor"]; ?>;
	    color: <?php echo $paramsData["btnColor"]; ?>
	    text-align: center;
	    border-radius: 20px;
    }



    section.team<?php echo $kunik ?>  {
        padding: 25px 0;
    }

    .team<?php echo $kunik ?> .frontside {
        position: relative;
        -webkit-transform: rotateY(0deg);
        -ms-transform: rotateY(0deg);
        z-index: 2;
        margin-bottom: 30px;
    }

    .team<?php echo $kunik ?> .frontside .card-body .card-text {
    	min-height: 85px;
    }

    .team<?php echo $kunik ?> .frontside .card-title {
        text-transform: none;
        font-weight: 500;
    }

    .team<?php echo $kunik ?> .frontside .card-body img {
        width: 300px;
        height: 300px;
        border-radius: 50%;
    }

    @media (max-width: 767px) {
    	.team<?php echo $kunik ?> .frontside .card-body img {
	        width: 250px;
	        height: 250px;
	        border-radius: 50%;
	    }
    }
    </style>
  


<!-- Team -->
<section class="team<?php echo $kunik ?>" class="pb-5">
    <div class="container">
        <h5 class="section-title title-1"><?php echo $paramsData["title"]; ?></h5>
        <div class="row row-centered">
            <!-- Team member -->

         <?php 
        if (isset($content)) {
            foreach ($content as $key => $value) {?>   
            <div class="col-xs-12 col-sm-6 col-md-4 team-desc-cont  col-center-block">
                <div class="frontside">
                    <?php  
                        $initFiles = Document::getListDocumentsWhere(
                            array(
                              "id"=> $blockKey,
                              "type"=>'cms',
                              "subKey"=> (string)$key 
                            ), "image"
                        );
                        $arrFile = [];
                        //var_dump($initFiles);
                        foreach ($initFiles as $k => $v) {
                            $arrFile[] =$v['imagePath'];
                        }
                    ?> 
                    <div class="card-body text-center">
                        
                            <?php if (!empty($arrFile[0])){ ?>
                                <img class=" img-fluid" src=" <?php echo $arrFile[0] ?>" alt="card image">
                            <?php }else { ?>
                                <img src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/blockCmsImg/Avatar.png"> 
                            <?php } ?>
                        
                        <h4 class="card-title  title-2"><?= $value["nameteam"] ?></h4>
                        <p class="card-text  title-3"><?= $value["speciality"] ?></p>
                        <div class="col-xs-12 text-center mt-4">
                       
	                        <?php if(Authorisation::isInterfaceAdmin()){ ?>
	                            <a href="javascript:;" class="btn btn-primary btn-action-team editElement<?= $blockCms['_id'] ?> editSectionBtns"
	                                    data-key="<?= $key ?>" 
	                                    data-img='<?php echo json_encode($initFiles) ?>' 
	                                    data-nameteam="<?= $value["nameteam"] ?>"
	                                    data-speciality="<?= $value["speciality"] ?>"
	                                    data-descriptionteam="<?= $value["descriptionteam"] ?>"
	                                    data-labelbtn="<?= $value["labelbtn"] ?>"
	                                ><i class="fa fa-pencil"></i></a>
	                            <?php } ?>
	                            <?php if (isset($value["descriptionteam"]) && !empty($value["descriptionteam"])){ ?>
	                            	<a href="<?= $value["descriptionteam"] ?>" class="btn btn-primary btn-action-team" target="_blank"><?= $value["labelbtn"] ?></a>
	                            <?php } ?>
	                            <?php if(Authorisation::isInterfaceAdmin()){ ?>
	                            <a  href="javascript:;" class="btn btn-danger btn-action-team deletePlan<?= $blockKey ?> "
	                                data-key="<?= $key ?>" 
	                                data-id ="<?= $blockKey ?>"
	                                data-path="content.<?= $key ?>"
	                                data-collection = "cms">
	                                <i class="fa fa-trash"></i>
	                            </a>
	                            
	                        <?php } ?>
	                        
	                    </div>
                    </div>

                </div>


            </div>
            <!-- ./Team member -->
            <?php } 
            } ?>

        </div>
        <div class="text-center editSectionBtns">
            <div class="" style="width: 100%; display: inline-table; padding: 10px;">
                <?php if(Authorisation::isInterfaceAdmin()){?>
                    <div class="text-center addElement<?= $blockCms['_id'] ?>">
                        <button class="btn btn-primary"><i class="fa fa-plus-circle"></i> Ajouter</button>
                    </div>  
                <?php } ?>
            </div>
        </div>

    </div>
</section>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {    
                "title" : "Configurer la section",
                "description" : "Personnaliser votre section",
                "icon" : "fa-cog",
                "properties" : { 
                    "title" : {
                        label : "titre du bloc",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.title
                    },    
                    "btnColor" : {
                        label : "Couleur du label bouton",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.btnColor
                    },            
                    "background_color" : {
                        label : "Couleur du fond bouton",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.background_color
                    },
                    "borderColor": {
                        label : "Couleur du bourdur bouton",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.borderColor
                    }
                     
                },
                save : function () {  
                    tplCtx.value = {};
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        tplCtx.value[k] = $("#"+k).val();
                    });
                    mylog.log("save tplCtx",tplCtx);
                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) { 
                                toastr.success("élement mis à jour");
                                $("#ajax-modal").modal('hide');
                                urlCtrl.loadByHash(location.hash);
                            } );
                        }
                }
            }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";

            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params, null , sectionDyf.<?php echo $kunik ?>ParamsData);
        });

        

        $(".deletePlan<?= $blockCms['_id'] ?>").click(function() { 
            var deleteObj ={};
            deleteObj.id = $(this).data("id");
            deleteObj.path = $(this).data("path");          
            deleteObj.collection = $(this).data("collection");
            deleteObj.value = null;
            bootbox.confirm("Etes-vous sûr de vouloir supprimer cet élément ?",
            function(result){
              if (!result) {
                return;
              }else {
                dataHelper.path2Value( deleteObj, function(params) {
                    mylog.log("deleteObj",params);
                    toastr.success("Element effacé");
                    urlCtrl.loadByHash(location.hash);
                });
              }
            }); 
        });
        
        $(".editElement<?= $blockCms['_id'] ?>").click(function() {  
            //var contentLength = Object.keys(<?php //echo json_encode($content); ?>).length;
            var key = $(this).data("key");
            var tplCtx = {};
            tplCtx.id = "<?= $blockCms['_id'] ?>"
            tplCtx.collection = "cms";
            tplCtx.path = "content."+(key);
            var obj = {
                nameteam :         $(this).data("nameteam"),
                descriptionteam:    $(this).data("descriptionteam"),
                img:            $(this).data("img"),
                speciality:     $(this).data("speciality"),
                labelbtn:     $(this).data("labelbtn")
            };
            var activeForm = {
                "jsonSchema" : {
                    "title" : "Ajouter nouveau bloc CMS",
                    "type" : "object",
                    onLoads : {
                        onload : function(data){
                            $(".parentfinder").css("display","none");
                        }
                    },
                    "properties" : getProperties(obj,key),
                    beforeBuild : function(){
                        uploadObj.set("cms","<?= $blockCms['_id'] ?>");
                    },
                    save : function (data) {  
                      tplCtx.value = {};
                      $.each( activeForm.jsonSchema.properties , function(k,val) { 
                        tplCtx.value[k] = $("#"+k).val();
                      });

                      if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                      else {
                          dataHelper.path2Value( tplCtx, function(params) { 
                               dyFObj.commonAfterSave(null, function(){
                                    if(dyFObj.closeForm()){
                                    $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
                                    urlCtrl.loadByHash(location.hash);
                                    }else{
                                        urlCtrl.loadByHash(location.hash);
                                    }
                                });
                          } );
                      }

                    }
                }
                };          
                dyFObj.openForm( activeForm );
            });


        $(".addElement<?= $blockCms['_id'] ?>").click(function() { 
            var keys = Object.keys(<?php echo json_encode($content); ?>);
            var lastContentK = 0; 
            if (keys.length!=0) 
                lastContentK = parseInt((keys[(keys.length)-1]), 10);
            var tplCtx = {};
            tplCtx.id = "<?= $blockCms['_id'] ?>";
            tplCtx.collection = "cms";
            tplCtx.path = "content."+(lastContentK+1);
            var obj = {
                nameTeam :         $(this).data("nameTeam"),
                descriptionTeam:    $(this).data("descriptionTeam"),
                speciality:     $(this).data("speciality"),
                labelbtn:     $(this).data("labelbtn")
            };

            var activeForm = {
                "jsonSchema" : {
                    "title" : "Ajouter nouveau bloc CMS",
                    "type" : "object",
                    onLoads : {
                        onload : function(data){
                            $(".parentfinder").css("display","none");
                        }
                    },
                    "properties" : getProperties(obj,lastContentK+1),
                    beforeBuild : function(){
                        uploadObj.set("cms","<?= $blockCms['_id'] ?>");
                    },
                    save : function (data) {  
                      tplCtx.value = {};
                      $.each( activeForm.jsonSchema.properties , function(k,val) { 
                        tplCtx.value[k] = $("#"+k).val();
                      });

                      if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                      else {
                          dataHelper.path2Value( tplCtx, function(params) { 
                               dyFObj.commonAfterSave(null, function(){
                                    if(dyFObj.closeForm()){
                                    $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
                                    urlCtrl.loadByHash(location.hash);
                                    }else{
                                    urlCtrl.loadByHash(location.hash);
                                    }
                                });
                          } );
                      }

                    }
                }
                };          
                dyFObj.openForm( activeForm );
            });


        
        function getProperties(obj={},subKey){
            var props = {
                img : {
                    "inputType" : "uploader",
                    "label" : "image",
                    "docType" : "image",
                    "contentKey" : "slider",
                    "itemLimit" : 1,
                    "endPoint": "/subKey/"+subKey,
                    "domElement" : "image",
                    "filetypes": ["jpeg", "jpg", "gif", "png"],
                    "label": "Image :",
                    "showUploadBtn": false,
                    initList : obj["img"]
                },
                nameteam : {
                    label : "Nom du personne",
                    "inputType" : "text",
                value : obj["nameteam"]
                },

                 speciality : {
                    label : "Spécialité",
                    "inputType" : "text",
                    value : obj["speciality"]
                },
                
                descriptionteam : {
                    label : "Lien du bouton",
                    "inputType" : "text",
                    value :  obj["descriptionteam"]
                },
                labelbtn : {
                    label : "Label du bouton",
                    "inputType" : "text",
                    value : obj["labelbtn"]
                }, 
                
            };
            return props;
        }
    });
</script>