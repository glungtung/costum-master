<?php 
$keyTpl = "blocimgrightcustom";
$paramsData=[
	"title" => "Lorem ipsum?",
	"titleColor" => "#ffffff",
	"text" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
	"titleSize"	  => "24",
	"textSize"	  => "18",
	"textColor"	  => "#000",
	"background_color"=> "#f9576d"
];

if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}
?>
<!-- ****************get image uploaded************** -->
<?php 
  $initImage = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>'block',
    ), "image"
  );
  $latestImg = isset($initImage["0"]["imageMediumPath"])?$initImage["0"]["imageMediumPath"]:"empty" ;
  if($latestImg == ""){
    $latestImg = isset($initImage["0"]["imagePath"])?$initImage["0"]["imagePath"]:"empty" ;
  }
   ?>
<!-- ****************end get image uploaded************** -->

<style type="text/css">
	.text<?php echo $kunik ?>{
		font-size: <?php echo $paramsData["textSize"]; ?>px;
		color: <?php echo $paramsData["textColor"]; ?>; 
	}
	.text<?php echo $kunik ?> p{
		font-size: <?php echo $paramsData["textSize"]; ?>px;
		color: <?php echo $paramsData["textColor"]; ?>; 
	}
	.text<?php echo $kunik ?> p, li{
		font-size: <?php echo $paramsData["textSize"]; ?>px;
		color: <?php echo $paramsData["textColor"]; ?>; 
	}


	@media screen and (min-width: 992px) {
		.circle-contain{
			border: 4px solid <?= $paramsData["background_color"]; ?>;
			border-radius: 100%;
			display: inline-table;
			padding: 20px;
			top: -40px;
			position: relative;
		}
	}
	@media screen and (max-width: 992px) {
		.circle-contain{
			border: 4px solid <?= $paramsData["background_color"]; ?>;
			border-radius: 100%;
			display: inline-table;
			padding: 20px;
			top: -120px;
		}
	}
	
	.container<?php echo $kunik ?> .btn-edit-delete{
		display: none;
	}
	.container<?php echo $kunik ?> .btn-edit-delete .btn{
		box-shadow: 0px 0px 20px 3px #ffffff;
	}
	.container<?php echo $kunik ?>:hover .btn-edit-delete{
		display: block;
		position: absolute;
		top:50%;
		left: 50%;
		transform: translate(-50%,0%);
	}
	.container<?php echo $kunik ?> .bold { 
		position:relative !important;
		font-size: <?= $paramsData["titleSize"]; ?>px;
		width: 60%; 
		background-color: <?= $paramsData["background_color"]; ?>;
		color: <?= $paramsData["titleColor"]; ?>
	}
</style>
<div class="container<?php echo $kunik ?>">
	<div class="" style="padding-top: 50px!important;padding-left: 10%!important;padding-right: 10%!important;">
		<div class="row">
			<div class="col-md-6 padding-top-25">
				<div class="text-center" style="padding-bottom: 40px">
				<font class="bold text-center padding-20 title" style="">
					<?= $paramsData["title"]; ?>
				</font>
				</div>
				<div class="padding-20 text<?php echo $kunik ?> markdown description bg-blur"><?= $paramsData["text"]; ?></div>
			</div>
			<div class="col-md-6 text-center">
				<div class="circle-contain">
					<?php if (!empty($latestImg)) { ?>
						<p class="p-img">
							<img style="width: 25vw;height: 25vw; border-radius: 100%;" src="<?php echo $latestImg ?>">
						</p>
					<?php }else{ ?>

					<?php } ?>
					
				</div>
			</div>			
		</div>
	</div>
</div>
<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {    
            "title" : "Configurer votre section",
            "description" : "Personnaliser votre bloc",
            "icon" : "fa-cog",
            
            "properties" : {
                "title" :{                
                "inputType" : "text",
                "label" : "Votre Titre",
                values :  sectionDyf.<?php echo $kunik ?>ParamsData.title
              },
              "titleSize" :{                
                "inputType" : "text",
                "label" : "Taille du titre",
                values :  sectionDyf.<?php echo $kunik ?>ParamsData.titleSize
              },
              "titleColor" : {
                "inputType" : "colorpicker",
                "label" : "Couleur du Titre",
                values :  sectionDyf.<?php echo $kunik ?>ParamsData.titleColor
              },
                "title" :{                
                "inputType" : "text",
                "label" : "Votre Titre",
                values :  sectionDyf.<?php echo $kunik ?>ParamsData.title
              },
                "title" :{                
                "inputType" : "text",
                "label" : "Votre Titre",
                values :  sectionDyf.<?php echo $kunik ?>ParamsData.title
              },
                "title" :{                
                "inputType" : "text",
                "label" : "Votre Titre",
                values :  sectionDyf.<?php echo $kunik ?>ParamsData.title
              },
                "title" :{                
                "inputType" : "text",
                "label" : "Votre Titre",
                values :  sectionDyf.<?php echo $kunik ?>ParamsData.title
              },
                "title" :{                
                "inputType" : "text",
                "label" : "Votre Titre",
                values :  sectionDyf.<?php echo $kunik ?>ParamsData.title
              },
              "text" : {
                "inputType" : "textarea",
                "markdown" : true,
                "label" : "Votre texte",
                values :  sectionDyf.<?php echo $kunik ?>ParamsData.text
              },
              "textColor" : {
                "inputType" : "colorpicker",
                "label" : "Couleur du texte",
                values :  sectionDyf.<?php echo $kunik ?>ParamsData.textColor
              },
               "background_color" : {
                "inputType" : "colorpicker",
                "label" : "Couleur du fond du titre",
                values :  sectionDyf.<?php echo $kunik ?>ParamsData.background_color
              },
              "textSize" :{                
                "inputType" : "text",
                "label" : "Taille du texte",
                values :  sectionDyf.<?php echo $kunik ?>ParamsData.textSize
              }, 
              "image" :{
                 "inputType" : "uploader",
                  "label" : "image",
                  "docType": "image",
                  "contentKey" : "slider",
                  "itemLimit" : 1,
                  "endPoint": "/subKey/block",
                  "domElement" : "image",
                  "filetypes": ["jpeg", "jpg", "gif", "png"],
                  "label": "Image :",
                  "showUploadBtn": false,
                  initList : <?php echo json_encode($initImage) ?>
              },
            },
            beforeBuild : function(){
                uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;

                if(k == "items")
                  tplCtx.value[k] = data.items;
              });
              
              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("Élément bien ajouter");
                      $("#ajax-modal").modal('hide');
                      urlCtrl.loadByHash(location.hash);
                    });
                  } );
              }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.subKey = "imgParent";
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
    });
</script>