<?php 
  $keyTpl ="e_contact";
    $loremIpsum = "lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";
  $paramsData = [ 
    "title" => "",
    "subTitle" => "SOUS TITRE",
    "description" => $loremIpsum,
    "btnBgColor" => "#008037",
    "btnColor" => "#ffffff",
    "btnLabel" => "EN SAVOIR PLUS",
    "btnUrl" => "#about",
  ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) {
              $paramsData[$e] = $blockCms[$e];
      }
    }
  }
 ?>
 <!-- ****************get image uploaded************** -->
<?php 
  $assetsUrl = Yii::app()->getModule('costum')->assetsUrl."/images/blockCmsImg/defaultImg";
    $initImage = Document::getListDocumentsWhere(
        array(
          "id"=> $blockKey,
          "type"=>'cms',
          "subKey" => 'imageLeft'
        ),"image"
    );
 ?>
<!-- ****************end get image uploaded************** -->
<style>
  .sub-container-right<?=$kunik ?>{
      background-size: cover;
      background-position: center;
      /*height: 500px;*/
      background:rgb(255,255,255,0.7);
  }
  .sub-container-right<?=$kunik ?> img{
    position: absolute;
    top: 50%;
    left:50%;
    transform: translate(-50%,-50%);
    height: 50%;
  }
  .sub-container-left<?= $kunik ?>{
        /*background-image:url('<?=  $assetsUrl ?>/apropos_illu-02.png') !important;*/
    /*height: 125px; */     
    background-size: cover;
      background-position: center;
      /*height: 500px; */
  }
  .sub-container-left<?= $kunik ?> img{
      height: 577px;
      position: relative;
      left: -154px; 
  }

    .block-container-<?= $kunik?> .btn-contactez-nous{
      color: <?php echo $paramsData["btnColor"] ?> !important;
      background-color: <?php echo $paramsData["btnBgColor"] ?> !important;
      font-weight: bold;
      margin: 0px 10px 38px 29px;
      padding: 14px 36px;
      border-radius: 12px;
      font-size: large;
  }
  .block-container-<?= $kunik?> .btn-contactez-nous:hover{
      background: <?php echo $paramsData["btnBgColor"] ?>;
    letter-spacing: 1px;
    -webkit-box-shadow: 0px 5px 40px -10px rgba(0,0,0,0.57);
    -moz-box-shadow: 0px 5px 40px -10px rgba(0,0,0,0.57);
    box-shadow: 5px 40px -10px rgba(0,0,0,0.57);
    transition: all 0.4s ease 0s;
  }
  .block-container-<?= $kunik?> .btn-contactez-nous{
    margin:0px;
  }
  @media (max-width: 360px){
     .block-container-<?= $kunik?> .btn-savoir-plus{
        width:100%;
        display: block;
      }
  }
  
  .block-container-<?= $kunik?>  .hr-title-apropos{
      display: inline-block;
      width: 50%;
      height: 5px;
      background-color:<?php echo @$blockCms["blockCmsColorSubtitle"] ?>
  }
.block-container-<?= $kunik?> .btn-savoir-plus:hover{
      background: <?php echo $paramsData["btnBgColor"] ?>;
      color: <?php echo $paramsData["btnColor"] ?>;
    letter-spacing: 1px;
    -webkit-box-shadow: 0px 5px 40px -10px rgba(0,0,0,0.57);
    -moz-box-shadow: 0px 5px 40px -10px rgba(0,0,0,0.57);
    box-shadow: 5px 40px -10px rgba(0,0,0,0.57);
    transition: all 0.4s ease 0s;
  }
.block-container-<?= $kunik?> .btn-savoir-plus{
      color: <?php echo $paramsData["btnColor"] ?>;
      background-color:  <?php echo $paramsData["btnBgColor"] ?>;
      font-weight: bold;
      margin: 44px 0px 38px 0px;
      padding: 14px 36px;
      border-radius: 12px;
      font-size: large;
  }
    .block-container-<?= $kunik?> .description{
      margin-top: 26px;
      line-height: 1.6;
    }
  @media (max-width: 992px){
    /*.block-container-<?= $kunik?> .description{
      position: relative;
      background: rgb(255,255,255,0.9);
      width: 100%;
      margin-top: 26px;*/
    }
    .sub-container-right<?=$kunik ?>{
      /*background:rgb(255,255,255,0.9);*/
    }
  }
</style>
<h1 class="title"><?= $paramsData["title"] ?></h1>
<div class="col-md-5 col-xs-12 sub-container-left<?= $kunik ?>">

      <?php if(!empty($initImage)){ ?>
        <img src="<?=  $initImage[0]["imageThumbPath"] ?>">
      <?php }else{ ?>
            <img src="<?=  $assetsUrl ?>/apropos_illu-02.png" alt="">
      <?php }?>
</div>
<div class="col-md-7 col-xs-12 sub-container-right<?= $kunik ?>">
    <h2 class="subtitle title-apropos"><?php echo $paramsData["subTitle"] ?></h2>
    <hr class="title hr-title-apropos ChangoRegular">
    <div class="description markdown"><?= $paramsData["description"] ?></div>
    <a href="<?php echo $paramsData["btnUrl"] ?>" class="lbh btn btn-savoir-plus"><?php echo $paramsData["btnLabel"] ?> 
    </a>
</div>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
      var urlOptions = {}
      $.each(costum.app,function(k,v){
          urlOptions[k] = exists(v.subdomainName) ? v.subdomainName : "";
      })
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {    
            "title" : "Configurer votre bloc",
            "description" : "Personnaliser votre bloc",
            "icon" : "fa-cog",
            
            "properties" : {
                "title" : {
                    "inputType" : "text",
                    "label" : "Titre",
                    
                    value :  sectionDyf.<?php echo $kunik ?>ParamsData.title
                },
                "subTitle" : {
                    "inputType" : "text",
                    "label" : "Sous titre",
                    
                    value :  sectionDyf.<?php echo $kunik ?>ParamsData.subTitle
                },
                "description" :{
                      "inputType" : "textarea",
                      "markdown" : true,
                      "label" : "Description",
                      values: sectionDyf.<?php echo $kunik ?>ParamsData.description
                },
                "btnLabel" : {
                    "inputType" : "Texte du boutton",
                    "label" : "Label du boutton",
                    value :  sectionDyf.<?php echo $kunik ?>ParamsData.btnLabel
                },
                "btnColor" : {
                    "inputType" : "colorpicker",
                    "label" : "Couleur du label du boutton",
                    value :  sectionDyf.<?php echo $kunik ?>ParamsData.btnColor
                },
                "btnBgColor" : {
                    "inputType" : "colorpicker",
                    "label" : "Couleur du fond du boutton",
                    value :  sectionDyf.<?php echo $kunik ?>ParamsData.btnBgColor
                },
                "btnUrl" : {
                    "inputType" : "select",
                    "label" : "Lien du boutton",
                    "options" : urlOptions,
                    value :  sectionDyf.<?php echo $kunik ?>ParamsData.btnUrl
                },
                "imageLeft" :{
                  "inputType" : "uploader",
                  "label" : "Image à gauche",
                  "docType": "image",
                  "contentKey" : "slider",
                  "itemLimit" : 1,
                  "endPoint": "/subKey/imageLeft",
                  "domElement" : "imageLeft",
                  "filetypes": ["jpeg", "jpg", "gif", "png"],
                  "showUploadBtn": false,
                  initList : <?php echo json_encode($initImage) ?>
                }         
            },
            beforeBuild : function(){
              uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;
              });
              console.log("save tplCtx",tplCtx);

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("Élément bien ajouter");
                      $("#ajax-modal").modal('hide');
                      urlCtrl.loadByHash(location.hash);
                    });
                  } );
              }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
    });
</script>