
<?php
$keyTpl     = "costumActifs";
$paramsData = [
    "title"      => "Les cocitys actifs",
    "colorTitle" =>"#005E6F",
    "costumSlug" => "cocity"
];
if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if ( isset($blockCms[$e])) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}

?>
<style type="text/css">
	.costumActif_<?= $kunik?> h1 {
	  color: <?= $paramsData["colorTitle"]?>;
	}

</style>
<div class=" costumActif_<?= $kunik?> text-center">
  	<h1> <i class="fa fa-users"> </i> <?= $paramsData["title"]?></h1>
  	<div class="container">
  		<div class="row">
    		<div class="main_<?= $kunik?> col-md-12 col-sm-12 col-xs-12 vertical" >
     			<div class="col-xs-12 bodySearchContainer  ">
      				<div id="listCostum" class="no-padding col-xs-12 listCostum<?= $kunik?>" id="dropdown_search" style="position: relative">

      				</div>
    			</div>
  			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	jQuery(document).ready(function() {    
	    var blocCostumObj ={
			costumList:function(){
				var params = {slug : "<?= $paramsData["costumSlug"]?>"};
	        	ajaxPost(
	        		null,
	        	 	baseUrl+"/costum/cocity/getlistCocityaction",
		          	params,
		          	function(data){
		            mylog.log("nomcity",data);
		            var src = '';
		            if (data.length != 0){
		              	$.each(data, function( index, value ) {
		              		var image = typeof value.profilImageUrl!= "undefined" ? value.profilImageUrl : "<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/cocity/photo/aide.jpg";
			                src += '<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 smartgrid-slide-element classifieds classifieds">';
			                src += '<div class="item-slide">';
			                src += '<div class="entityCenter">';
			                src += ' <a href="" class="pull-right" ';
			    			            src += '<i class="fa fa-rocket bg-azure"></i> </a>';
			                src += '</div>';
			                src += '<div class="img-back-card">';
			                src += '<div class="container-img-card">';
			                src += '<img class="img-responsive img-profil-entity"  src="'+image+'">';
			                src += ' </div>';
			                  src += '  <div class="text-wrap searchEntity">';
			                src += '<div class="entityRight profil no-padding">';
			                src += '<a href="<?php  echo Yii::app()->createUrl("/costum")?>/co/index/slug/'+ value.slug +'" class="entityName letter-turq" target="_blank">';
			                src += '<font style="vertical-align: inherit;">';
			                src += '<font style="vertical-align: inherit;">';
			                src += '<i class="fa fa-rocket"></i>  '+ value.name;

			                src += '</font>';
			                src += '</font>';
			                src += '</a>';
			                src += '</div>';
			                src += '</div>';
			                src += '</div>';
			                src += '<div class="slide-hover co-scroll">';
			                src += '<div class="text-wrap">';
			                src += '<h4 class="entityName">';
			                src += '<a href="<?php  echo Yii::app()->createUrl("/costum")?>/co/index/slug/'+ value.slug +'" class=" letter-turq  " target="_blank">';
			                src += '<font style="vertical-align: inherit;">';
			                src += '<font style="vertical-align: inherit;">';
			                src += '<i class="fa fa-rocket"> </i> ' + value.name ;
			                $
			                src += '</font>';
			                src += '</font>';
			                src += '</a>';
			                src += '</h4>';
			                src += '<div class="entityType col-xs-12 no-padding">';
			                src += '<span class="type">';
			                src += '<font style="vertical-align: inherit;">';
			                src += '<font style="vertical-align: inherit;">';
			                src += '</font>';
			                src += '</font>';
			                src += '</span>';
			                src += '</div>';
			                src += '<div class="desc">';
			                src += '<div class="socialEntityBtnActions btn-link-content">';
			                src += '<a href="<?php  echo Yii::app()->createUrl("/costum")?>/co/index/slug/'+ value.slug +'"   class="btn btn-info btn-link btn-share-panel" target="_blank"> ';
			                src += '<font style="vertical-align: inherit;">';
			                src += '<font style="vertical-align: inherit;"> Voir le costum'; 
			                src += '</font>';
			                src += '</font>';
			                src += '</a>';
			                src += '</div>';
			                src += '</div>';
			                src += '</div>';
			                src += '</div>';
			                src += '</div>';
			                src += '</div>';
		              	});
		            }
		            else {
		              src += "<p class='text-center'>Il n'éxiste aucune cocity </p>";
		            }
	            	$(".listCostum<?= $kunik?>").html(src);
	          	}
	        );
	      	}
	  	}
  		blocCostumObj.costumList();
  		sectionDyf.<?php echo $kunik?>Params = {
			"jsonSchema" : {    
				"title" : "Configurer la section1",
				"description" : "Personnaliser votre section1",
				"icon" : "fa-cog",
				"properties" : {
	          		"title" : {
	            		"inputType" : "text",
	            		"label" : "Titre",
	            		values :  sectionDyf.<?php echo $kunik ?>ParamsData.title
          			},
					"colorTitle":{
						label : "Couleur du titre",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.colorTitle
					},
	          		"costumSlug" : {
	            		"inputType" : "text",
	            		"label" : "Costum.key",
	            		values :  sectionDyf.<?php echo $kunik ?>ParamsData.costumSlug
          			},
				},
				beforeBuild : function(){
		               uploadObj.set("cms","<?= $blockCms['_id'] ?>");
		            },
				save : function () {  
					tplCtx.value = {};
					$.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
						if (k == "parent") {
							tplCtx.value[k] = formData.parent;
						}
					});
					mylog.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
		                  dataHelper.path2Value( tplCtx, function(params) {
		                    dyFObj.commonAfterSave(params,function(){
		                      toastr.success("Modification enregistrer");
		                      $("#ajax-modal").modal('hide');
		                      urlCtrl.loadByHash(location.hash);
		                    });
		                  } );
		              }

				}
			}
		};
		mylog.log("sectiondyfff",sectionDyf);
		$(".edit<?php echo $kunik?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";
			dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
		});
	})	
</script>