<?php 
$keyTpl = "blockarianemembre";
$content = $content;
$paramsData = [
	"paddingTop"	  		=> "2",
	"paddingBottom"	  		=> "2",
	"paddingLeft"		=> "10",
	"paddingRight"		=> "10",
	"size"	  		=> "4",
	"titleAlign"		=> "center",
	"titleSize"    		=> "24",
	"titleColor"   		=> "#ffffff",
	"textAlign"		  	=> "center",
	"textSize"	  		=> "24",
	"textColor"	  		=> "#ffffff",
	"nbrCol"	  		=> "1",
	"imagePosition"		=> "center",
	"titlePolice"		=> "Lato-Italic"
];

if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}

?>
<style type="text/css">
	.container<?php echo $kunik ?> .btn-edit-delete{
		display: none;
	}
	.container<?php echo $kunik ?> .btn-edit-delete .btn{
		box-shadow: 0px 0px 20px 3px #ffffff;
	}
	.container<?php echo $kunik ?>:hover .btn-edit-delete{
		display: block;
		position: absolute;
		top:65%;
		left: 50%;
		transform: translate(-50%,0%);
	}
</style>
<div id="blockarianemembre<?php echo $kunik ?>" class="row container<?php echo $kunik ?> padding-25">
	<?php 
	if (!empty($content)) {
		foreach ($content as $key => $value) { 
			?>
			<div class="col-md-<?= $value["size"] ?>" style="padding-right: <?= $value["paddingRight"] ?>% !important;padding-left: <?= $value["paddingLeft"] ?>% !important;padding-top: <?= $value["paddingTop"] ?>% !important;padding-bottom: <?= $value["paddingBottom"] ?>% !important;">
				<?php  
				 	$initFiles = Document::getListDocumentsWhere(
					    array(
					      "id"=> $blockKey,
					      "type"=>'cms',
					      "subKey"=> (string)$key 
					    ), "image"
				  	);
				  	$arrFile = [];
				  	foreach ($initFiles as $k => $v) {
				  		if ($v['imageMediumPath']=='') {
				  			$arrFile[] =$v['imagePath'];
				  		}else{
				  			$arrFile[] =$v['imageMediumPath'];
				  		}
				  	}
					if (count($arrFile)!=0) {?>
						<div class="text-<?= $value['imagePosition'] ?>" style="">
							<img style="
							border: 4px solid #c0cb1f;
							display: inline-table;
							padding: 10px;
							width: 220px;
							height: 220px;
							border-radius: 100%;
							object-fit: cover;" src="<?php echo $arrFile[0] ?>">
						</div>
				<?php } ?> 
				<div class="text-center padding-25">
					<div style="font-size: <?= $value['titleSize'] ?>px; width: 100%; text-align: <?= $value['titleAlign'] ?>; font-family: <?= $value['titlePolice'] ?>;color: <?php echo $value["titleColor"]; ?>;">
						<font class="bold"><?= $value["title"] ?></font><br>
					</div>
					<div style="width: 100%; text-align: <?= $value['textAlign'] ?>;color: <?php echo $value["textColor"]; ?>;font-family: <?= $value['textPolice'] ?>">
						<p style="font-size: <?= $value['textSize'] ?>px;" class="markdown"><?= $value["description"] ?></p>
					</div>
				</div>
				<?php if(Authorisation::isInterfaceAdmin()){ ?>
					<div 
					class="text-center editSectionBtns" >
					<a class="editElement<?= $blockCms['_id'] ?> btn btn-danger btn material-button btn-primary"
					data-size="<?= $value["size"] ?>" 
					data-key="<?= $key ?>" 
					data-title="<?= $value["title"] ?>" 
					data-img='<?php echo json_encode($initFiles) ?>'
					data-description="<?= $value["description"] ?>" 
					data-titleAlign="<?= $value["titleAlign"] ?>" 
					data-titlePolice="<?= $value["titlePolice"] ?>" 
					data-titleSize="<?= $value["titleSize"] ?>" 
					data-titleColor="<?= $value["titleColor"] ?>" 
					data-textSize="<?= $value["textSize"] ?>" 
					data-textColor="<?= $value["textColor"] ?>" 
					data-textAlign="<?= $value["textAlign"] ?>" 
					data-textPolice="<?= $value["textPolice"] ?>" 
					data-imagePosition="<?= $value["imagePosition"] ?>" 
					data-paddingLeft="<?= $value["paddingLeft"] ?>"
					data-paddingRight="<?= $value["paddingRight"] ?>"
					data-paddingTop="<?= $value["paddingTop"] ?>"
					data-paddingBottom="<?= $value["paddingBottom"] ?>" ><i class="fa fa-edit"></i></a>
						<a  href="javascript:;"
						class="btn material-button bg-red text-center deletePlan<?= $blockKey ?> "
						data-id ="<?= $blockKey ?>"
						data-path="content.<?= $key ?>"
						data-collection = "cms"
						>
						<i class="fa fa-trash"></i>
					</a>
					</div>	
				<?php } ?>
			</div>
		<?php }	
	} else { 
		if(Authorisation::isInterfaceAdmin()){ ?>	
			<p class="text-center">Les contenus sera affiché ici.</p>
		<?php } } ?>
	<div class="text-center editSectionBtns">
		<div class="" style="width: 100%; display: inline-table; padding: 10px;">
			<?php if(Authorisation::isInterfaceAdmin()){?>
				<div class="text-center addElement<?= $blockCms['_id'] ?>">
					<button class="btn btn-primary">Ajouter du contenu</button>
				</div>	
			<?php } ?>
		</div>
	</div>
</div>
<script type="text/javascript">
	sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	jQuery(document).ready(function() {
		sectionDyf.<?php echo $kunik ?>Params = {
			"jsonSchema" : {    
				"title" : "Configurer la section",
				"description" : "Personnaliser votre section",
				"icon" : "fa-cog",
				"properties" : {
				},
				beforeBuild : function(){
					uploadObj.set("cms","<?php echo $blockKey ?>");
				},
				save : function () {  
					tplCtx.value = {};
					$.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
					});
					mylog.log("save tplCtx",tplCtx);
						if(typeof tplCtx.value == "undefined")
							toastr.error('value cannot be empty!');
						else {
							dataHelper.path2Value( tplCtx, function(params) { 
								dyFObj.commonAfterSave(params,function(){
									toastr.success("Élément bien ajouter");
									$("#ajax-modal").modal('hide');
									urlCtrl.loadByHash(location.hash);
								});
							} );
						}
				}
			}
		};

		$(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";

			dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params, null , sectionDyf.<?php echo $kunik ?>ParamsData);
		});

		$(".deletePlan<?= $blockCms['_id'] ?>").click(function() { 
			var deleteObj ={};
			deleteObj.id = $(this).data("id");
			deleteObj.path = $(this).data("path");			
			deleteObj.collection = "cms";
			deleteObj.value = null;
			bootbox.confirm("Etes-vous sûr de vouloir supprimer cet élément ?",

				function(result){
					if (!result) {
						return;
					}else {
						dataHelper.path2Value( deleteObj, function(params) {
							mylog.log("deleteObj",params);
							toastr.success("Element effacé");
							urlCtrl.loadByHash(location.hash);
						});
					}
				}); 
			
		});

	$(".editElement<?= $blockCms['_id'] ?>").click(function() {  
			var contentLength = Object.keys(<?php echo json_encode($content); ?>).length;
			var key = $(this).data("key");
			var tplCtx = {};
			tplCtx.id = "<?= $blockCms['_id'] ?>"
			tplCtx.collection = "cms";
			tplCtx.path = "content."+(key);
			var obj = {
				title : 		$(this).data("title"),
				description:    $(this).data("description"),
				titlealign:     $(this).data("titlealign"),
				titlepolice:    $(this).data("titlepolice"),
				titlesize:      $(this).data("titlesize"),
				titlecolor:     $(this).data("titlecolor"),
				textsize:       $(this).data("textsize"),
				textcolor:      $(this).data("textcolor"),
				textalign:      $(this).data("textalign"),
				textpolice:     $(this).data("textpolice"),
				img:      		$(this).data("img"),
				imageposition:  $(this).data("imageposition"),
				paddingright:   $(this).data("paddingright"),
				paddingleft:    $(this).data("paddingleft"),					
				paddingtop:     $(this).data("paddingtop"),
				paddingbottom:  $(this).data("paddingbottom"),
				size:           $(this).data("size")
			};
			var activeForm = {
				"jsonSchema" : {
					"title" : "Modification du contenu",
					"type" : "object",
					onLoads : {
						onload : function(data){
							$(".parentfinder").css("display","none");
						}
					},
					"properties" : getProperties(obj,key),
					beforeBuild : function(){
						uploadObj.set("cms","<?php echo $blockKey ?>");
					},
		            save : function (data) {  
		              tplCtx.value = {};
		              $.each( activeForm.jsonSchema.properties , function(k,val) { 
		                tplCtx.value[k] = $("#"+k).val();
		              });

		              tplCtx.value.description = tplCtx.value.description.replace(/'/g, '’');
		              if(typeof tplCtx.value == "undefined")
		                toastr.error('value cannot be empty!');
		              else {
		                  dataHelper.path2Value( tplCtx, function(params) {
		                    dyFObj.commonAfterSave(params,function(){
		                      toastr.success("Élément bien ajouter");
		                      $("#ajax-modal").modal('hide');
		                      urlCtrl.loadByHash(location.hash);
		                    });
		                  } );
		              }

		            }
				}
				};          
				dyFObj.openForm( activeForm );
				alignInput2(activeForm.jsonSchema.properties,"title",4,6,null,null,"Titre","#1da0b6");
				alignInput2(activeForm.jsonSchema.properties,"text",4,6,null,null,"Configuration du texte","#1da0b6");
				alignInput2(activeForm.jsonSchema.properties,"description",12,12,null,null,"Texte","#1da0b6");
				alignInput2(activeForm.jsonSchema.properties,"image",4,6,null,null,"Configuration de image","#1da0b6");
				alignInput2(activeForm.jsonSchema.properties,"img",12,12,null,null,"Image","#1da0b6");
				$('.fieldsettext,.fieldsettitle,.fieldsetimage,.fieldsetdescription,.fieldsetimg').show();
			});


		$(".addElement<?= $blockCms['_id'] ?>").click(function() { 
			var keys = Object.keys(<?php echo json_encode($content); ?>);
			var	lastContentK = 0; 
			if (keys.length!=0) 
				lastContentK = parseInt((keys[(keys.length)-1]), 10);
			var tplCtx = {};
			tplCtx.id = "<?= $blockCms['_id'] ?>";
			tplCtx.collection = "cms";
			tplCtx.path = "content."+(lastContentK+1);
			var obj = {
					title : 		$(this).data("title"),
					description:    $(this).data("description"),
					titlealign:     "left",
					titlepolice:    "Arial",
					titlesize:      "24",
					titlecolor:     "#2C3E50",
					textsize:       "18",
					textcolor:      "#2C3E50",
					textalign:      "justify",
					textpolice:    	"Arial",
					img:      		"",
					imageposition:  "center",
					paddingright:   "2",
					paddingleft:    "2",					
					paddingtop:     "2",
					paddingbottom:  "2",
					size:           "4"
			};
			var activeForm = {
				"jsonSchema" : {
					"title" : "Ajouter nouveau contenu",
					"type" : "object",
					onLoads : {
						onload : function(data){
							$(".parentfinder").css("display","none");
						}
					},
					"properties" : getProperties(obj,lastContentK+1),
					beforeBuild : function(){
						uploadObj.set("cms","<?php echo $blockKey ?>");
					},
		            save : function (data) {  
		              tplCtx.value = {};
		              $.each( activeForm.jsonSchema.properties , function(k,val) { 
		                tplCtx.value[k] = $("#"+k).val();
		              });

		              tplCtx.value.description = tplCtx.value.description.replace(/'/g, '’');
		              if(typeof tplCtx.value == "undefined")
		                toastr.error('value cannot be empty!');
		              else {
		                  dataHelper.path2Value( tplCtx, function(params) {
		                    dyFObj.commonAfterSave(params,function(){
		                      toastr.success("Élément bien ajouter");
		                      $("#ajax-modal").modal('hide');
		                     urlCtrl.loadByHash(location.hash);
		                    });
		                  } );
		              }

		            }
				}
				};          
				dyFObj.openForm( activeForm );
				alignInput2(activeForm.jsonSchema.properties,"title",4,6,null,null,"Titre","#1da0b6");
				alignInput2(activeForm.jsonSchema.properties,"text",4,6,null,null,"Configuration du texte","#1da0b6");
				alignInput2(activeForm.jsonSchema.properties,"description",12,12,null,null,"Texte","#1da0b6");
				alignInput2(activeForm.jsonSchema.properties,"image",4,6,null,null,"Configuration de image","#1da0b6");
				alignInput2(activeForm.jsonSchema.properties,"img",12,12,null,null,"Image","#1da0b6");
				$('.fieldsettext,.fieldsettitle,.fieldsetimage,.fieldsetdescription,.fieldsetimg').show();
			});


		
		function getProperties(obj={},key){
			var props = {
				title : {
					label : "Titre",
					"inputType" : "text",
				value : obj["title"]
				},
				
				description : {
					label : "Description",
					"inputType" : "textarea",
					"markdown" : true,
					value :  obj["description"]
				},	

				titleAlign : {
					label : "Alignement du titre",
					inputType : "select",
					options : {
						"left"    : "À Gauche",
						"right" : "À droite",
						"center"  :"Centré",
						"justify"  :"Justifié"
					},
					value : obj["titlealign"]
				},
				titlePolice : {
					label : "Police du titre",
					inputType : "select",
					options : {
						"Lato-Italic"    : "Lato-Italic",
						"Arial" : "Arial"
					},
					value : obj["titlepolice"]
				},
				titleSize : {
					label : "Taille du titre",
					value :  obj["titlesize"]
				},
				titleColor : {
					label : "Couleur du titre",
					inputType : "colorpicker",
					value : obj["titlecolor"]
				},
				textSize : {
					label : "Taille du text",
					value : obj["textsize"]
				},
				textColor : {
					label : "Couleur du text",
					inputType : "colorpicker",
					value : obj["textcolor"]
				},	
				textAlign : {
					label : "Alignement du texte",
					inputType : "select",
					options : {
						"left"    : "À Gauche",
						"right" : "À droite",
						"center"  :"Centré",
						"justify"  :"Justifié"
					},
					value : obj["textalign"]
				},	
				textPolice : {
					label : "Police du texte",
					inputType : "select",
					options : {
						"Lato-Italic"    : "Lato-Italic",
						"Arial" : "Arial"
					},
					value : obj["textpolice"]
				},	
				img : {
					"inputType" : "uploader",
					"label" : "image",
					"docType": "image",
					"contentKey" : "slider",
					"itemLimit" : 1,
					"endPoint": "/subKey/"+key,
					"domElement" : "image",
					"filetypes": ["jpeg", "jpg", "gif", "png"],
					"label": "Image :",
					"showUploadBtn": false,
					initList : obj["img"]
				},
				imagePosition : {
					label : "Position d'image",
					inputType : "select",
					options : {
						"left"    : "À Gauche",
						"right" : "À droite",
						"center"  :"Centré"
					},
					value : obj["imageposition"]
				},	
				paddingRight : {
					label : "Marge à droite",
					inputType : "text",
					value :  obj["paddingright"]
				},		
				paddingLeft : {
					label : "Marge à Gauche",
					inputType : "text",
					value :  obj["paddingleft"]
				},		
				paddingTop : {
					label : "Marge en haut",
					inputType : "text",
					value :  obj["paddingtop"]
				},		
				paddingBottom : {
					label : "Marge en bas",
					inputType : "text",
					value :  obj["paddingbottom"]
				},
							
				size : {
					label : "Taille",
					inputType : "select",
					options : {
						"2"    : "Plus petit",
						"4" : "Petit",
						"6"  :"Moyenne",
						"12"  :"Grand"
					},
					value : obj["size"]
				},
				
			};
			return props;
		}
	});
</script>