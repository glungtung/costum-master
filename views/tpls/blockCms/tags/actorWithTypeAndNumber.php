<?php 
$keyTpl = "actorWithTypeAndNumber";
$paramsData = [];

if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (  isset($blockCms[$e]) ) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
}  
$projects = PHDB::find(Project::COLLECTION, array("parent.".$this->costum["contextId"] => array('$exists'=>1)));
$tag = PHDB:: find(Tags::COLLECTION,array("tag" =>$this->costum["contextSlug"]));
$agricultures = PHDB::find(Organization::COLLECTION,["source.key" =>$this->costum["contextSlug"],"tags"=>"agricultures"]);
$association = PHDB::find(organization::COLLECTION, array("parent.".$this->costum["contextId"] => array('$exists'=>1)));
?>
<style type="text/css">
  .vagueCocity_<?=$kunik ?>{
    margin-left: 5%;
  }
  .vagueCocity_<?=$kunik ?> h3{
    height: 80px;
    background-color: #8ABF32;
    color: #fff;
    line-height: 70px;
  }

  .searchCocity_<?=$kunik ?> form {
    width: 100%;
    max-width: 700px;
    padding-top: 3%;
    margin-left: 20%;
  }

  .searchCocity_<?=$kunik ?> form .inner-form {
    display: -ms-flexbox;
    display: flex;
    width: 100%;
    -ms-flex-pack: justify;
    justify-content: space-between;
    -ms-flex-align: center;
    align-items: center;
    box-shadow: 0px 8px 20px 0px rgba(0, 0, 0, 0.15);
    border-radius: 5px;
    overflow: hidden;
    margin-bottom: 30px;
    height: 50px;
    border: 1px solid #0A96B5
  }

  .searchCocity_<?=$kunik ?> form .inner-form .input-field {
    height: 68px;
  }

  .searchCocity_<?=$kunik ?> form .inner-form .input-field input {
    height: 100%;
    border: 0;
    display: block;
    width: 100%;
    padding: 10px 0;
    font-size: 16px;
    color: #000;
  }

  .searchCocity_<?=$kunik ?> form .inner-form .input-field input.placeholder {
    color: #222;
    font-size: 14px;
  }

  .searchCocity_<?=$kunik ?> form .inner-form .input-field input:-moz-placeholder {
    color: #222;
    font-size: 14px;
  }

  .searchCocity_<?=$kunik ?> form .inner-form .input-field input::-webkit-input-placeholder {
    color: #222;
    font-size: 14px;
  }

  .searchCocity_<?=$kunik ?> form .inner-form .input-field input:hover, .searchCocity_<?=$kunik ?> form .inner-form .input-field input:focus {
    box-shadow: none;
    outline: 0;
  }

  .searchCocity_<?=$kunik ?> form .inner-form .input-field.first-wrap {
    -ms-flex-positive: 1;
    flex-grow: 1;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-align: center;
    align-items: center;
    background: #fff;
  }

  .searchCocity_<?=$kunik ?> form .inner-form .input-field.first-wrap input {
    -ms-flex-positive: 1;
    flex-grow: 1;
  }

  .searchCocity_<?=$kunik ?> form .inner-form .input-field.first-wrap .svg-wrapper {
    min-width: 60px;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-pack: center;
    justify-content: center;
    -ms-flex-align: center;
    align-items: center;
    font-size: 23px;
    color: #0A96B5;
  }

  .searchCocity_<?=$kunik ?> form .inner-form .input-field.first-wrap svg {
    width: 36px;
    height: 36px;
    fill: #222;
  }

  .searchCocity_<?=$kunik ?> form .inner-form .input-field.second-wrap {
    min-width: 50px;
  }

  .searchCocity_<?=$kunik ?> form .inner-form .input-field.second-wrap .btn-search {
    height: 100%;
    width: 100%;
    white-space: nowrap;
    font-size: 21px;
    color: #fff;
    border: 0;
    cursor: pointer;
    position: relative;
    z-index: 0;
    background: #0A96B5;
    transition: all .2s ease-out, color .2s ease-out;
    font-weight: 300;
  }

  .searchCocity_<?=$kunik ?> form .inner-form .input-field.second-wrap .btn-search:hover {
    background: #0A96B5;
  }

  .searchCocity_<?=$kunik ?> form .inner-form .input-field.second-wrap .btn-search:focus {
    outline: 0;
    box-shadow: none;
  }

  .tagsCocity{
    text-align: left;
    margin-left: 20%;
    margin-top: 2%;  
  }
  .tagsCocity a{
   padding: 10px;
 }
 .tagsCocity blockquote:before{
  content: "";
  position: absolute;
  top: -10%;
  left: 35px;
  box-shadow: -1px 1px 1px 0px #0A96B5;
  transform: rotate(135deg);
  border-bottom: 30px solid #fff;
  border-top: 0px solid #fff;
  border-top-color: #fff;
  border-top-color: #ffffff;
  border-left: -5px solid transparent;
  border-right: 30px solid transparent;
}
.tagsCocity blockquote {
  width: 79%;
  height: 150px;
  display: flex;
  margin-top: 5%;
  padding-left: 0;
  border-left: none;
  padding: 39px;
  background: #fff;
  box-shadow: 0px 0px 6px #0A96B5;
  font-size: 20px;
  font-weight: 300;
  position: relative;
  -webkit-border-radius: 7px;
  -moz-border-radius: 7px;
  -ms-border-radius: 7px;
  border-radius: 7px;
}

.searchCocity_<?=$kunik ?> blockquote .result{
  padding: 6px;
  margin-left: 9%;
  color:#005E6F;
}

.vagueCocity_<?=$kunik ?> img{
  font-size: 50px;
  height: 40px;
  width: 40px;
} 
@media (max-width: 414px) {
  .vagueCocity_<?=$kunik ?> h3{
   height: 40px;
   background-color: #8ABF32;
   color: #fff;
   line-height: 30px;
   font-size: 20px;
 }
 .searchCocity_<?=$kunik ?> form {
  width: 100%;
  max-width: 700px;
  padding-top: 3%;
  margin-left: 0%;
}

.searchCocity_<?=$kunik ?> form .inner-form {
  margin-bottom: 6%;
  height: 40px;
}
.tagsCocity {
  text-align: left;
  margin-left: 2%;
  margin-top: 2%;
}
.tagsCocity blockquote {
 width: 100%;
 height: 120px;
 display: flex;
 margin-top: 5%;
 padding-left: 0;
 border-left: none;
 padding-top: 6%;
 background: #fff;
 box-shadow: 0px 0px 6px #0A96B5;
 font-size: 17px;
 font-weight: 300;
 position: relative;
 -webkit-border-radius: 7px;
 -moz-border-radius: 7px;
 -ms-border-radius: 7px;
 border-radius: 7px;
}


.searchCocity_<?=$kunik ?> blockquote .result{
  margin-left: 1%;
}
.vagueCocity_<?=$kunik ?>{
  margin-left: 0;
}

}
}
</style>
<div class=" searchCocity_<?=$kunik ?> vagueCocity_<?=$kunik ?> text-center">

  <h3>
    <i class="fa fa-users"></i>
    Les acteurs
  </h3>
  <form>
    <div class="inner-form">
      <div class="input-field first-wrap">
        <div class="svg-wrapper">
          <i class="fa fa-search"> </i>              
        </div>
        <input  id="nameActeur"  type="text" placeholder="Chercher un tag # ..."   name="nameActe ur" value="" onkeypress="searchActeur() "/>
      </div>
      <div class="input-field second-wrap">
        <button class="btn-search "  onclick="searchActeur()" type="button">
          <i class="fa fa-arrow-right"></i>
        </button>
      </div>
    </div>
  </form>
  <div class="tagsCocity">
    <blockquote class="to-animate-2 text-center">
      <div class="result">
        <i class="fa fa-users"></i><br>
        <?= count($association) ?> <br>Associations
      </div>
      <div class="result">
        <i class="fa fa-tree"></i><br>
        <?= count($agricultures) ?> <br>Agricultures
      </div>
      <div class="result">
        <i class="fa fa-rocket"></i><br>
        <?= count($projects) ?> <br>  Projets
      </div>
      <div class="result">
        <i class="fa fa-tag"></i> <br>
        <?= count($tag) ?><br> Tags
      </div>
    </blockquote>
  </div>

</div>

<script type="text/javascript">
  function searchActeur(){
    var nameActeur =document.getElementById("nameActeur").value;
    var params = {
      count: true,
      indexStep:10,
      countType : [ "citoyens", "organizations", "projects" ],
      searchType : [ "citoyens", "organizations", "projects" ],
      name : nameActeur  
    };
    var src ="";

    //alert(nameActeur);
    ajaxPost(
      null,
      baseUrl+"/" + moduleId + "/search/globalautocomplete",
      params,
      function(data){
        src += 
          '<blockquote class="to-animate-2 text-center">'+
            '<div class="result">'+
              '<i class="fa fa-users"></i><br>'+
              data.count.organizations+' <br>Associations'+
           ' </div>'+
            '<div class="result">'+
              '<i class="fa fa-tree"></i><br>'+
              ' <br>Agricultures'+
            '</div>'+
            '<div class="result">'+
              '<i class="fa fa-rocket"></i><br>'+
              data.count.projects+' <br>Projets'+
            '</div>'+
            '<div class="result">'+
              '<i class="fa fa-tag"></i> <br>'+
              '<br> Tags'+
            '</div>'+
          '</blockquote>';
          $(".tagsCocity").html(src);
     }      
   );
    
  }
  
</script>