    <style>

        .carto-<?= $kunik?>{
            margin-top : -7.9%;
        }
        .dropmenu-<?= $kunik?>{
            margin-top: -10%;
            position: relative;
            margin-left: -5%;
        }
        .form-control{
            padding-left: 0.8em !important;
        }
        .dropdown-menu{
            padding : 0px 0px !important;
        }
        .dropdown-menu > li >  a {
            padding: 5px 10px !important;
            font-size: 1.2em;
        }
        .carto-h1-<?= $kunik?> {
            color : white;
        }
        .carto-d-<?= $kunik?> {
            box-shadow: 0px 0px 20px -2px black;
            width: 80%;
            left: 10%;
            background : white;
            padding: 2%;
            font-size : 1.5vw;
        }
    	.title-carto-<?= $kunik?>{
    		font-size : 200%;
    		text-shadow: 0px 0px 7px black;
    	}
        .c-description-<?= $kunik?>{
            width: 80%;
            left: 10%;
            font-size: 2vw;
            margin-top: 3%;
    	}

        .px-4{
            padding: 30px !important;
        }

    </style>


    <?php

        

        $typeObj = PHDB::findOne("organizations", array("slug"=>$this->costum["slug"]));
        
        if(isset($typeObj["costum"]["typeObj"])){
            $this->costum["typeObj"] = $typeObj["costum"]["typeObj"];
        }
        
        if(isset($typeObj["costum"]["typeObj"]["organization"]["dynFormCostum"]["beforeBuild"]["properties"]["category"]["options"])){
            $typeActeurs = $typeObj["costum"]["typeObj"]["organization"]["dynFormCostum"]["beforeBuild"]["properties"]["category"]["options"];
        }else{
            $typeActeurs = [];
        }

        $types = array();

        $index = 0;
        foreach ($typeActeurs as $tkey => $type){
            $types["typeActeurs$index"] = ["category" => $type];
            $index++;
        }
        # print_r($typeActeurs);
    ?>

    <?php 
        $keyTpl ="annuaireDropdown";
        
        $paramsData = [
            "textSlogan"=>"Lorem ipsum dolor sit amet...",
            "iconTitre"=>"",
            "sectionTitle"=>"CARTO/ANNUAIRE",
            "typeActeurs"=> $types,
            "imageCarte"=> "",
            "content" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Dicta voluptas explicabo nesciunt exercitationem placeat? Cupiditate harum debitis nesciunt deleniti hic? Similique obcaecati dicta hic aspernatur officia dolor eum ut. Sit."
        ];

      if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
          if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
          }
        }
      }

      //$paramsData["typeActeurs"] = $types;
    ?>

    <!-- ****************get image uploaded************** -->
    <?php 
        $initFiles = Document::getListDocumentsWhere(
            array(
                "id"=> $blockKey,
                "type"=>'cms',
                "subKey"=>  $kunik
            ),
            "image"
        );
        $imageCarte = [];

        foreach ($initFiles as $key => $value) {
            $imageCarte[]= $value["imagePath"];
        }

        if($imageCarte!=[]){
            $paramsData["imageCarte"] = $imageCarte[0];
        }
        
     ?>
    <!-- ****************end get image uploaded************** -->

    <div class="carto-<?= $kunik?> row content-<?= $kunik?>">
    	<div class="carto-h1-<?= $kunik?> col-xs-6 col-sm-6 no-padding">
    		<p class="title-2 title-carto-<?= $kunik?> hidden-sm hidden-xs" style="margin-left: 19.5%;">
                <i><?php echo $paramsData["textSlogan"]; ?></i>
            </p>
    	</div>
    	<div class="carto-img col-xs-6 col-sm-6" >

    	</div>
    	<div class="carto-d-<?= $kunik?> col-xs-12 col-sm-12">
    		<div class="row">
    			<div class=" <?php echo ($paramsData['imageCarte']!='')?'col-md-7':'col-md-12' ?> px-4">
    				<a href="javascript:;" data-hash="#filiere" aria-expanded="false" style="text-decoration : none;" class="lbh-menu-app">
    					<h1 class="title">
                            <?php if($paramsData["iconTitre"]!=""){
    						  echo '<i class="fa '.$paramsData["iconTitre"].'"></i>';
                            } ?>
    						<?php echo $paramsData["sectionTitle"]; ?>
    						<br>
    					</h1>
    				</a> 
    				<p class="p-mobile-description description">
                        <?php echo $paramsData["content"]; ?>
                    </p>
                    <div class="text-center">
                    <?php foreach($typeActeurs as $aKey => $aType){ ?>
                        <span class="badge badge-secondary" style="text-decoration : none; padding: 4px; margin: 2px">
                            #<?php echo $aType ?>
                        </span>
                    <?php } ?>
                    </div>
    			</div>
                <?php if($paramsData["imageCarte"]!=""){ ?>
        			<div class="carto-img col-md-5">
        				<a href="javascript:;" data-hash="#search" class="lbh-menu-app" style="text-decoration : none;">
        					<img src="<?php echo $paramsData["imageCarte"]; ?>" class="img-responsive">
        				</a>
        			</div>
                <?php } ?>
    		</div>
    	</div>

    	<!-- Carto NEWS -->
    	<div class="row">
    		<div class="c-description-<?= $kunik?> col-sm-12 col-xs-12">
    			<div class="description col-md-6">
    			</div>
    			<div class="col-md-6">
    			</div>
    		</div>
    	</div>
        
    </div>

    <script type="text/javascript">
        
        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
        jQuery(document).ready(function() {
            sectionDyf.<?php echo $kunik ?>Params = {
              "jsonSchema" : {    
                "title" : "Configurer votre section",
                "description" : "Personnaliser votre section",
                "icon" : "fa-cog",
                
                "properties" : {
                    "textSlogan" : {
                        "inputType" : "text",
                        "label" : "Text de slogan",
                        value :  sectionDyf.<?php echo $kunik ?>ParamsData.textSlogan
                    },
                    "iconTitre" : {
                        label : "Icone",
                        inputType : "select",
                        options : <?= json_encode(Cms::$icones); ?>,
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.iconTitre
                    },
                    "sectionTitle" : {
                        "inputType" : "text",
                        "label" : "Titre de lannuaire",
                        value :  sectionDyf.<?php echo $kunik ?>ParamsData.sectionTitle
                    },
                    "content" : {
                        "inputType" : "contents",
                        "label" : "Text de description de l'annuaire :",
                        value :  sectionDyf.<?php echo $kunik ?>ParamsData.content
                    },
                    "imageCarte":{
                        "inputType" : "uploader",
                        "label" : "Image à droite :",
                        "docType" : "image",
                        "contentKey" : "slider",
                        "domElement" : "imageCarte",
                        "endPoint" :"/subKey/<?= $kunik?>",
                        value : sectionDyf.<?php echo $kunik ?>ParamsData.imageCartes,
                        "initList": <?php echo json_encode($initFiles) ?>
                    },
                    "typeActeurs" : {
                        inputType : "lists",
                        label : "Types d'acteurs",
                        entries: {
                            category: {
                                type:"text",
                                label:"Type d'acteurs",
                                placeholder: ""
                            }
                        },
                        values: sectionDyf.<?php echo $kunik ?>ParamsData.typeActeurs
                    }
                },
                beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
                save : function (data) {  
                  tplCtx.value = {};
                  let types = {
                    "placeholder" : "Identifiez-vous à un acteur",
                    "inputType" : "select",
                    "label" : "Catégorie d'acteur*",
                    "class" : "form-control",
                    "order" : 1,
                    "labelInformation" : "Catégorie d'acteur",
                    "options":{}
                  };
                  let apps = {};
                  $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                    tplCtx.value[k] = $("#"+k).val();
                    if(k=="typeActeurs"){
                        $.each(data.typeActeurs, function(index, va){

                            let hashKey = "";
                            
                            var match = /[*/~#&"'-?+@`_^${}[=\]().|\\]/.exec(va.category);
                            
                            if (match) {
                                hashKey = toCamelCase(va.category.substring(0, match.index)).trim();
                            }else{
                                hashKey = toCamelCase(va.category).trim();
                            }

                            types["options"][""+hashKey] = va.category;

                            apps["#"+hashKey] = {
                                    "hash" : "#app.search",
                                    "icon" : "search",
                                    "filterObj" : "costum.views.custom.<?php echo $this->costum["slug"] ?>.filters",
                                    "urlExtra" : "/page/"+hashKey,
                                    "subdomainName" : "Tous les acteurs "+hashKey,
                                    "placeholderMainSearch" : "what are you looking for ?",
                                    "useHeader" : false,
                                    "useFilter" : true,
                                    "useFooter" : true,
                                    "filters" : {
                                        "types" : [ 
                                            "organizations"
                                        ]
                                    },
                                    "searchObject" : {
                                        "indexStep" : "0"
                                    }
                                };
                        });
                        //tplCtx.value[k] = types;
                    }

                    if (k == "parent")
                      tplCtx.value[k] = formData.parent;

                    if(k == "items")
                      tplCtx.value[k] = data.items;
                      mylog.log("Test",data.items)
                  });
                  console.log("save tplCtx",tplCtx);

                  if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                  else {
                    var params = {"slug": '<?php echo $this->costum["slug"]; ?>', "category": types, "filters":apps}
                    ajaxPost(
                        null,
                        '<?= Yii::app()->baseUrl; ?>/costum/filiere/updatecategory',
                        params,
                        function(data){
                            toastr.success("Enregistrement des donées");
                        },
                        function(xhr,textStatus,errorThrown,data){
                            toastr.error("Fail :"+JSON.stringify(xhr)+textStatus+JSON.stringify(errorThrown)+JSON.stringify(data));
                        });

                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("Élément bien ajouter");
                      $("#ajax-modal").modal('hide');
                      urlCtrl.loadByHash(location.hash);
                    });
                  });
                }
            }
        }
    };

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = "allToRoot";
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
    });

    //Dropdown
    $('.dropdown-a').mouseover(function() {
        $(this).attr("aria-expanded",true);
        $(".description").addClass("open");
    });

    // $('.img-dropdown').mouseover(function() {
    //     $(".description").addClass("open");
    // });

    $(".dropdown-menu").mouseover(function(){
        $(".description").addClass("open");
    });

    $(".dropdown-a").mouseleave(function(){
        if($('.description').hasClass('open')){
            $(".dropdown-a").attr("aria-expended",false);
            $(".description").removeClass("open");
        }
    });

    $(".dropdown-menu").mouseleave(function(){
        $(".dropdown-a").attr("aria-expended",false);
        $(".description").removeClass("open");
    });

    function toCamelCase(str) {
        return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, function(match, index) {
        if (+match === 0) return ""; // or if (/\s+/.test(match)) for white spaces
        return index == 0 ? match.toLowerCase() : match.toUpperCase();
      });
    }
    </script>
