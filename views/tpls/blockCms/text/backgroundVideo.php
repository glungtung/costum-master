<?php 
$keyTpl = "backgroundVideo";
$paramsData = [ 
	"lienVideo" =>"https://youtu.be/U98iMq-4oAk",
	"logo"  => "",
	"text1" => "Kosasa met en valeur les acteurs locaux de l'île de la Réunion, avec la réalisation de vidéos, photos, web documentaires et lors d'événements Kosasa met en valeur les acteurs locaux de l'île de la Réunion, avec la réalisation de vidéos, photos, web documentaires et lors d'événements",
	"text2" => "C'est aujourd’hui qu’on se rencontre et c’est maintenant qu’on agit !",
	"colorText1"=>"#327753",
	"colorText2"=>"#f5833c"
];

if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}
$youtubeId = array_reverse(explode("/", $paramsData["lienVideo"]))[0];
?>
<style type="text/css">
	.<?= $kunik?> iframe .ytp-impression-link{
		display: none !important;
	}
	.<?= $kunik?> iframe{
	    object-fit: cover;
	    margin-bottom: 2%;
	    width: 100%;
	    height: 342px;
	}
	.<?= $kunik?> .cont .text1 p{	
		text-transform: none;
		font-size : 22px ;
		color: <?= $paramsData["colorText1"] ?>;
	}
	.<?= $kunik?> .cont .text2 p{
		text-transform: none;
		font-size : 32px ;
		margin: 0;
		color: <?= $paramsData["colorText2"] ?>;
	}
	
	@media (max-width: 414px) {
		.<?= $kunik?> iframe{
			height: auto;
		}
		
		.<?= $kunik?> .cont .text1 p{
			font-size : 14px !important ;

		}
		.<?= $kunik?> .cont .text2 p{
			font-size : 18px !important;
		}
	}
	
	@media screen and (min-width: 1500px){
		.<?= $kunik?> .cont .text1 p{	
    		font-size: 25px !important;

		}
		.<?= $kunik?> .cont .text2 p{
			font-size: 36px !important;
		}
		
	}
	
</style>
	<div class="<?= $kunik?> " > 
			<iframe id="player" frameborder="0" allowfullscreen="1" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" title="YouTube video player"  src="https://www.youtube.com/embed/yRWaLApRR44?rel=0&amp;enablejsapi=1&amp;widgetid=1"></iframe>
		<div class="cont container">
			<h2 class="text1 text-center markdown subtitle"> <?= $paramsData["text1"]?></h2>
			<h1 class="text2 text-center markdown title"> <?= $paramsData["text2"]?></h1>
		</div> 
	</div>

	<script type="text/javascript">		
		sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
		jQuery(document).ready(function() {
			sectionDyf.<?php echo $kunik ?>Params = {
				jsonSchema : {
					"title" : "Configurer votre section",
					"description" : "Personnaliser votre section",
					"icon" : "fa-cog",
					properties : {						
						"lienVideo" : {
							"inputType" : "url",
							"label" : "Url de la vidéo",
							"values" :  sectionDyf.<?php echo $kunik ?>ParamsData.lienVideo
						},
						"text1" : {
							"inputType" : "textarea",
							"label" : "text1",
							"markdown" : true,
							"values" :  sectionDyf.<?php echo $kunik ?>ParamsData.text1
						},
						"colorText1":{
							"label" : "Couleur du text1",
							"inputType" : "colorpicker",
							"values" :  sectionDyf.<?php echo $kunik?>ParamsData.colorText1
						},
						"text2" : {
							"inputType" : "textarea",
							"label" : "text2",
							"markdown" : true,
							"values" :  sectionDyf.<?php echo $kunik ?>ParamsData.text2
						},
						"colorText2":{
							"label" : "Couleur du text2",
							"inputType" : "colorpicker",
							"values" :  sectionDyf.<?php echo $kunik?>ParamsData.colorText2
						}
					},
					save : function () {  
						tplCtx.value = {};

						$.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
							tplCtx.value[k] = $("#"+k).val();
						});

						console.log("save tplCtx",tplCtx);

						if(typeof tplCtx.value == "undefined")
							toastr.error('value cannot be empty!');
						else {
		                  dataHelper.path2Value( tplCtx, function(params) {
		                    dyFObj.commonAfterSave(params,function(){
		                      toastr.success("Élément bien ajouter");
		                      $("#ajax-modal").modal('hide');
		                      urlCtrl.loadByHash(location.hash);
		                    });
		                  } );
						}
					}
				}

			};
			mylog.log("paramsData",sectionDyf);
			$(".edit<?php echo $kunik?>Params").on("click",function() {  
				tplCtx.id = $(this).data("id");
				tplCtx.collection = $(this).data("collection");
				tplCtx.path = "allToRoot";
				dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
			});

		});
		var tag = document.createElement('script');
      tag.src = "https://www.youtube.com/iframe_api";
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
      var player;
      function onYouTubeIframeAPIReady() {
        player = new YT.Player('player', {
        	height: '360',
		    width: '640',
		    videoId: '<?= $youtubeId?>',
		    playerVars : {
             	'rel':0,
             	'origin' : ''
		    },
	        events: {
	        	'onReady': onPlayerReady,
	            'onStateChange': onPlayerStateChange
	        }

        });
      }

      function onPlayerReady(event) {
        event.target.playVideo();
      }

      var done = false;
      function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.PLAYING && !done) {
          setTimeout(stopVideo, 10000);
          done = true;
        }
      }
      function stopVideo() {
        player.stopVideo();
      }
	</script>

