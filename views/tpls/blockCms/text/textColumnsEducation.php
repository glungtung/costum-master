<?php 
$keyTpl = "textColumnsEducation";
$paramsData=[
	"title"=>"Je suis Admin",
	"title2"=>"Je suis etudiant",
	"content1"=>"Lorem Ipsum is simply dummy text of the printing and typesetting industry",
	"content2"=>"Lorem Ipsum is simply dummy text of the printing and typesetting industry",	
	"logo1"=>"",
	"logo2"=>""
];
if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}
?>
<?php 
$blockKey = (string)$blockCms["_id"];
$initImage1 = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>'logo1',
    ), "image"
  );
$initImage2 = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>'logo2',
    ), "image"
  );
?>
<style type="text/css">

	.rounded-<?= $kunik?>{
		border-radius: 20px;
		padding: 20px;
		width: 80%;
		background-color: #039eb6;
	}

	.rounded-<?= $kunik?> p, button{
		color: #ffffff;
		font-family: 'Circular-Loom' !important;
		font-size: 25px;
	}
	.rounded-<?= $kunik?> img{
		max-width: 200px;
		cursor: pointer;
	}
	.centered{ 
		margin-left: auto;
		margin-right: auto;
		width: 50%
		padding: 10px;
	}
	.btn-<?= $kunik?>{
		background-color: #fdd767;
		color: #61605c !important;
		border-radius: 50px;
		box-shadow: 0px 0px 10px 0px #00000087;
		padding-left: 30px;
		padding-right: 30px;
	}
</style>
<div class="container">
	<div class="row">
			<div class="col-md-12">
				<div class="col-md-6 col-sm-6 padding-25">
					<div class="fadeInUp animated rounded-<?= $kunik?>">		
						<p class="text-center markdown" style="cursor: pointer;" onclick="dyFObj.openForm('organization');"><?= $paramsData["title"]?></p>	
						<p class="text-center" onclick="dyFObj.openForm('organization');">
							<i class="icon fadeInUp animated-2 text-center">
								<?php if (!empty(Document::getLastImageByKey($blockKey, "cms", "profil","logo2"))){ ?>
									<img src="<?php echo Document::getLastImageByKey($blockKey, "cms", "profil","logo2"); ?>">
								<?php }else { ?>
									<img max-width="400px" src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/blockCmsImg/AUTRE-02.svg">
								<?php } ?>

							</i>		
						</p>						
						<p class="text-center">
							<button class="btn text-center btn-<?= $kunik?>" type="button" data-toggle="collapse" data-target="#collapse1" aria-expanded="false" aria-controls="collapseExample">
								Voir plus
							</button>
						</p>

						<div class="collapse" id="collapse1">
							<div class="card card-body">
								<p class="markdown description text-left"><?= $paramsData["content1"]?></p>
							</div>
						</div>						
					</div>
				</div>
				<div class="col-md-6 col-sm-6 padding-25">
					<div class="fadeInUp animated rounded-<?= $kunik?>">
						<a style="text-decoration: none;" href="#answer.index.id.604f11d1da83ef25058b456e.mode.w" class="lbh">
							<p class="text-center markdown" style="cursor: pointer;"><?= $paramsData["title2"]?></p>	
							<p class="text-center">
								<i class="icon fadeInUp animated-2 text-center">
									<?php if (!empty(Document::getLastImageByKey($blockKey, "cms", "profil","logo2"))){ ?>
										<img src="<?php echo Document::getLastImageByKey($blockKey, "cms", "profil","logo2"); ?>">
									<?php }else { ?>
										<img width="400px" src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/blockCmsImg/AUTRE-02.svg">
									<?php } ?>

								</i>		
							</p>
						</a>						
						<p class="text-center">
							<button class="btn btn-<?= $kunik?> text-center" type="button" data-toggle="collapse" data-target="#collapse2" aria-expanded="false" aria-controls="collapseExample">
								Voir plus
							</button>
						</p>

						<div class="collapse" id="collapse2">
							<div class="card card-body">
								<p class="markdown description"><?= $paramsData["content2"]?></p>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>

</div>
<script type="text/javascript">
	
	sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	jQuery(document).ready(function() {
		sectionDyf.<?php echo $kunik?>Params = {
			"jsonSchema" : {    
				"title" : "Configurer votre section",
				"description" : "Personnaliser votre section",
				"icon" : "fa-cog",
				"properties" : {
					
					"title" : {
						label : "titre",
						"inputType" : "textarea",
						"markdown" : true,
						values :  sectionDyf.<?php echo $kunik?>ParamsData.title
					},
					
					"content1" : {
						label : "Contenu1",
						"inputType" : "textarea",
						"markdown" : true,
						values :  sectionDyf.<?php echo $kunik?>ParamsData.content1
					},
					"logo1" : {
						"inputType" : "uploader",
						"label" : "photo 1",
						"showUploadBtn" : false,
						"docType" : "image",
						"itemLimit" : 1,
							"contentKey" : "slider",
						"domElement" : "logo1",
						"placeholder" : "image logo",
						"afterUploadComplete" : null,
						//"template" : "qq-template-manual-trigger",
						"endPoint" : "/subKey/logo1",
						"filetypes" : [
							"png","jpg","jpeg","gif"
						],
                        initList : <?php echo json_encode($initImage1) ?>
					},
					
					"title2" : {
						label : "Titre 2",
						"inputType" : "textarea",
						"markdown" : true,
						values :  sectionDyf.<?php echo $kunik?>ParamsData.title
					},
					
					"content2" : {
						label : "Contenu 2",
						"inputType" : "textarea",
						"markdown" : true,
						values :  sectionDyf.<?php echo $kunik?>ParamsData.content2
					},
					"logo2" : {
						"inputType" : "uploader",
						"label" : "photo 2",
						"showUploadBtn" : false,
						"docType" : "image",
						"itemLimit" : 1,
						"contentKey" : "slider",
						"domElement" : "logo2",
						"placeholder" : "image logo",
						"afterUploadComplete" : null,
						//"template" : "qq-template-manual-trigger",
						"endPoint" : "/subKey/logo2",
						"filetypes" : [
						"png","jpg","jpeg","gif"
						],
                        initList : <?php echo json_encode($initImage2) ?>
					}
				},
				beforeBuild : function(){
					uploadObj.set("cms","<?php echo $blockKey ?>");
				},
				save : function () {  
					tplCtx.value = {};

					$.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
					});

					mylog.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
		                  dataHelper.path2Value( tplCtx, function(params) {
		                    dyFObj.commonAfterSave(params,function(){
		                      toastr.success("Élément bien ajouter");
		                      $("#ajax-modal").modal('hide');
		                      urlCtrl.loadByHash(location.hash);
		                    });
		                  } );
					}
				}
			}

		};
		mylog.log("paramsData",sectionDyf);
		$(".edit<?php echo $kunik?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";
			dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
		});
	});
</script>