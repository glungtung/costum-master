<?php 
   $keyTpl ="blockFeatures";
  $paramsData = [ 
    "titre" => " FONCTIONNALITES",
    "description"=>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco "
  ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) {
        $paramsData[$e] = $blockCms[$e];
      }
    }
  } 
 ?>
<style type="text/css">
/*============ Service Features style ============*/
.service-heading-block{
	display:block;
	margin-bottom:30px;
	}
.title {
  display: block;
  font-size: 30px;
  font-weight: 200;
  margin-bottom: 10px;
}
.sub-title {
  font-size: 18px;
  font-weight: 100;
  line-height: 24px;
}
.feature-block {
  background-color: #fff;
  border-radius: 2px;
  padding: 15px;
  box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12);
  margin-bottom: 15px;
  transition:all ease .5s
}
.ms-feature:hover, 
.ms-feature:focus {
  background-color: #fafafa;
  box-shadow: 0 3px 4px 3px rgba(0, 0, 0, 0.14), 0 3px 3px -2px rgba(0, 0, 0, 0.2), 0 1px 8px 3px rgba(0, 0, 0, 0.12);
}
.fb-icon {
  border-radius: 50%;
  display: block;
  font-size: 36px;
  height: 80px;
  line-height: 80px;
  text-align:center;
  margin:1rem auto;
  width: 80px;
  transition: all 0.5s ease 0s;
}
.feature-block:hover .fb-icon,
.feature-block:focus .fb-icon {
  box-shadow: 0 4px 5px 0 rgba(0, 0, 0, 0.14), 0 1px 10px 0 rgba(0, 0, 0, 0.12), 0 2px 4px -1px rgba(0, 0, 0, 0.2);
  transform: rotate(360deg);
}
.fb-icon.color-info {
  background-color: #5bc0de;
  color: #fff;
}
.fb-icon.color-warning {
  background-color: #eea236;
  color: #fff;
}
.fb-icon.color-success {
  background-color: #5cb85c;
  color: #fff;
}
.fb-icon.color-danger {
  background-color: #d9534f;
  color: #fff;
}
.feature-block h4 {
  text-transform: none;
  font-weight: 500;
  margin: 3rem 0 1rem;
}
.color-info {
  color: #46b8da;
}
.color-warning {
  color: #f0ad4e;
}
.color-success {
  color: #4cae4c;
}
.color-danger {
  color: #d43f3a;
}
.btn-custom{
  border: medium none;
  border-radius: 2px;
  cursor: pointer;
  font-size: 14px;
  font-weight: 500;
  letter-spacing: 0;
  margin: 10px 1px;
  outline: 0 none;
  padding: 8px 30px;
  position: relative;
  text-decoration: none;
  text-transform: uppercase;
}
.icone<?= $kunik?> span{
  color: white;
}

</style>
<div class="container text-center  pb-5">
  <div class="row">
    <div class="service-heading-block">
      <h2 class="text-center text-primary title"><?= $paramsData["titre"]?></h2>
      <div class="text-center sub-title description markdown" ><?= $paramsData["description"]?></div>
    </div> 
    <div class="text-center editSectionBtns">
      <div class="" style="width: 100%; display: inline-table; padding: 10px;">
        <?php if(Authorisation::isInterfaceAdmin()){?>
          <div class="text-center addElement<?= $blockCms['_id'] ?>">
            <button class="btn btn-primary">Ajouter du contenu</button>
          </div>  
        <?php } ?>
      </div>
    </div>          
    <?php if (isset($blockCms["content"])) {
          $content = $blockCms["content"];
          foreach ($content as $key => $value) {
            $title = isset($value["title"])?$value["title"]:"";
            $description = isset($value["description"])?$value["description"]:"";
            $icon = isset($value["titleIcon"])?$value["titleIcon"]:"";
            $titlecolor = isset($blockCms["blockCmsColorTitle2"])?$blockCms["blockCmsColorTitle2"]:"";
    ?>
      <div class="col-lg-3 col-md-6 col-sm-6 col-center-block" >
        <div class="text-center feature-block">
          <div class="icone<?= $kunik?>">            
            <span class="fb-icon" style="background: <?= $titlecolor?>; color: white">
              <i class="fa fa-<?= $icon?>" aria-hidden="true"></i>
            </span>
          </div>
          <h4 class=" title-2" style ="color:<?= $titlecolor?>;  ">
            <?= $title?>
          </h4>
          <div class="title-4 markdown" style="color:<?= $titlecolor?>"><?= $description?></div>
          <?php if(Authorisation::isInterfaceAdmin()){ ?>
            <div class="editdelete<?= $blockKey?>">
              <a  href="javascript:;"
                class="btn material-button text-center editElement<?= $blockKey ?> "
                data-key="<?= $key ?>" 
                data-title="<?= $title?>" 
                data-icon ='<?= $icon ?>'
                data-description="<?= $description?>" >
                <i class="fa fa-edit"></i>
              </a>
              <a  href="javascript:;"
                class="btn material-button btn-danger text-center deletePlan<?= $blockKey ?> "
                data-key="<?= $key ?>" 
                data-id ="<?= $blockKey ?>"
                data-path="content.<?= $key ?>"
                data-collection = "cms"
                >
                <i class="fa fa-trash"></i>
              </a>
          </div>  
        <?php } ?>
        </div>
      </div>
    <?php } 
      }
    ?>
  </div> 
</div>
<script type="text/javascript">
 
  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
  jQuery(document).ready(function() {
    $(".addElement<?= $blockCms['_id'] ?>").click(function() {  
      
        var keys = Object.keys(<?php echo json_encode($content); ?>);
        var lastKey = 0; 
        if (keys.length!=0) 
          var lastKey = parseInt((keys[(keys.length)-1]), 10);
        var tplCtx = {};
        tplCtx.id = "<?= $blockKey ?>";
        tplCtx.collection = "cms";

        tplCtx.path = "content."+(lastKey+1);
        var obj = {
            subKey : (lastKey+1),
            icon :     $(this).data("icon"),
            title :     $(this).data("title"),
            description:    $(this).data("description")
            
        };
        var activeForm = {
          "jsonSchema" : {
            "title" : "Ajouter nouveau bloc CMS",
            "type" : "object",
            onLoads : {
              onload : function(data){
                $(".parentfinder").css("display","none");
              }
            },
            "properties" : getProperties(obj),
            beforeBuild : function(){
              uploadObj.set("cms","<?= $blockCms['_id'] ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( activeForm.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
              });
              mylog.log("save tplCtx",tplCtx);

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("Élément bien ajouter");
                      $("#ajax-modal").modal('hide');
                     urlCtrl.loadByHash(location.hash);
                    });
                  } );
              }
            }
          }
        };
        dyFObj.openForm( activeForm );
        alignInput2(activeForm.jsonSchema.properties,"title",4,6,null,null,"Titre","#1da0b6");
        alignInput2(activeForm.jsonSchema.properties,"description",12,12,null,null,"Texte","#1da0b6");
        
        $(',.fieldsettitle,.fieldsetdescription').show();
    }); 
    $(".editElement<?= $blockCms['_id'] ?>").click(function() { 

      var contentLength = Object.keys(<?php echo json_encode($content); ?>).length;
      var key = $(this).data("key");
      var tplCtx = {};
      tplCtx.id = "<?= $blockCms['_id'] ?>"
      tplCtx.collection = "cms";
      tplCtx.path = "content."+(key);
      var obj = {
        subKey : key,
        icon : $(this).data("icon"),
        title :     $(this).data("title"),
        description:    $(this).data("description")
      };
      var activeForm = {
        "jsonSchema" : {
          "title" : "Ajouter nouveau bloc CMS",
          "type" : "object",
          onLoads : {
            onload : function(data){
              $(".parentfinder").css("display","none");
            }
          },
          "properties" : getProperties(obj),
          beforeBuild : function(){
                    uploadObj.set("cms","<?= $blockCms['_id'] ?>");
                },
                save : function (data) {  
                  tplCtx.value = {};
                  $.each( activeForm.jsonSchema.properties , function(k,val) { 
                    tplCtx.value[k] = $("#"+k).val();
                  });
                  console.log("save tplCtx",tplCtx);

                  if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                  else {
                      dataHelper.path2Value( tplCtx, function(params) {
                        dyFObj.commonAfterSave(params,function(){
                          toastr.success("Modification enregistrer");
                          $("#ajax-modal").modal('hide');
                          urlCtrl.loadByHash(location.hash);
                        });
                      } );
                  }

                }
        }
        };    
        dyFObj.openForm( activeForm );
        alignInput2(activeForm.jsonSchema.properties,"title",4,6,null,null,"Titre","#1da0b6");
        alignInput2(activeForm.jsonSchema.properties,"description",12,12,null,null,"Description","#1da0b6");
        $('.fieldsettitle,.fieldsetdescription').show();
      });
    $(".deletePlan<?= $blockCms['_id'] ?>").click(function() { 
      var deleteObj ={};
      deleteObj.id = $(this).data("id");
      deleteObj.path = $(this).data("path");      
      deleteObj.collection = "cms";
      deleteObj.value = null;
      bootbox.confirm("Etes-vous sûr de vouloir supprimer cet élément ?",
        function(result){
          if (!result) {
            return;
          }else {
            dataHelper.path2Value( deleteObj, function(params) {
              mylog.log("deleteObj",params);
              toastr.success("Element effacé");
              urlCtrl.loadByHash(location.hash);
            });
          }
        }); 
    });
    function getProperties(obj={},newContent){
      mylog.log("objjjjj",obj);
      var props = {
        "titleIcon":{
           "inputType" : "select",
            "label" : "icon",
            "options" : fontAwesome,
            "value" : obj["icon"]
        },
        "title" : {
          label : "Titre",
          "inputType" : "text",
          value : obj["title"]
        },
        "description" : {
          "inputType" : "textarea",
          "markdown" : true,
          value :  obj["description"]
        }   
      };
      return props;
    }
     sectionDyf.<?php echo $kunik ?>Params = {
        "jsonSchema" : {    
          "title" : "Configurer votre section",
          "description" : "Personnaliser votre section",
          "icon" : "fa-cog",

          "properties" : {
             
          },
          beforeBuild : function(){
            uploadObj.set("cms","<?php echo $blockKey ?>");
          },
          save : function (data) {  
            tplCtx.value = {};
            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
              tplCtx.value[k] = $("#"+k).val();
              if (k == "parent")
                tplCtx.value[k] = formData.parent;

              if(k == "items")
                tplCtx.value[k] = data.items;
            });

            if(typeof tplCtx.value == "undefined")
              toastr.error('value cannot be empty!');
            else {
              dataHelper.path2Value( tplCtx, function(params) {
                dyFObj.commonAfterSave(params,function(){
                  toastr.success("Modification enregistré!");
                  $("#ajax-modal").modal('hide');
                  urlCtrl.loadByHash(location.hash);
                });
              } );
            }

          }
        }
    };

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
      tplCtx.subKey = "imgParent";
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
     
      dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });
  });


</script>