<?php 
$keyTpl = "backgroundPhotoParallax";
$paramsData = [
	"title" => "Lorem Ipsum",
	"sizeTitle"=>"35",	
	"colorTitle"=>"#000",	
	"sizeContent"=>"20",
	"content"=> "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
	"colorContent"=>"#000",
	"photo"=>""
];
if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}  
?>
<style type="text/css">
	.contenu_<?= $kunik ?> h2 {
		font-size: <?= $paramsData["sizeTitle"]?>px;
		color: <?= $paramsData["colorTitle"]?>;
	}
	.contenu_<?= $kunik ?> p {	
		font-size: <?= $paramsData["sizeContent"]?>px ;
		color: <?= $paramsData["colorContent"]?>;
	}
	.block-container-<?=$kunik?> {
		background-attachment: fixed;
		background-position: center;
		background-repeat: no-repeat;
		background-size: cover;
	}
	
	
	@media (max-width: 414px) {
		.contenu_<?= $kunik ?> h2 {
			font-size:20px;
			margin-top: 36px ;
			margin-bottom: 0px;
		}
		.contenu_<?= $kunik ?> p {
			font-size: 13px !important;
			margin-bottom: 20px;
			padding: 0;
		}
	}
	@media screen and (min-width: 1500px){
		.contenu_<?=$kunik?> h2{
		    font-size: 40px;
		    line-height: 35px;
		    text-transform: none;
		}
		.contenu_<?=$kunik?> p{
		    line-height: 35px;
		    font-size: 25px;
		    text-transform: none;
		    margin-bottom: -90px;
	  	}
	}
</style>

	<div class=" parallax_<?= $kunik?>" >
	<div class="container contenu_<?= $kunik?>">
		<div class="text-center">
			<h2 class="title"><?= $paramsData["title"]?></h2>
			<br>
			<p class="description"><?= $paramsData["content"]?></p> 
		</div>
	</div>
	
</div>
<script type="text/javascript">
	sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	jQuery(document).ready(function() {
		sectionDyf.<?php echo $kunik?>Params = {
			"jsonSchema" : {    
				"title" : "Configurer votre section",
				"description" : "Personnaliser votre section",
				"icon" : "fa-cog",
				"properties" : {					
					"title" : {
						label : "titre",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.title
					},
					"sizeTitle" : {
						"label" : "taille du titre",
						"inputType":"number",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.sizeTitle
					},
					"colorTitle":{
						label : "Couleur du titre",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.colorTitle
					},
					"content":{
						"inputType" : "textarea",
						"markdown" : true,
						"label":"contenu",
						values : sectionDyf.<?php echo $kunik?>ParamsData.content
					},
					"sizeContent" : {
						"label" : "taille du contenu",
						"inputType":"number",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.sizeContent
					},
					"colorContent":{
						label : "Couleur du contenu",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.colorContent
					}
				},
				beforeBuild : function(){
					uploadObj.set("cms","<?php echo $blockKey ?>");
				},
				save : function () {  
					tplCtx.value = {};

					$.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
					});

					mylog.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
		                  dataHelper.path2Value( tplCtx, function(params) {
		                    dyFObj.commonAfterSave(params,function(){
		                      toastr.success("Élément bien ajouter");
		                      $("#ajax-modal").modal('hide');
		                      urlCtrl.loadByHash(location.hash);
		                    });
		                  } );
					}
				}
			}
		};
		$(".edit<?php echo $kunik?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";
			dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
		});

	});
</script>