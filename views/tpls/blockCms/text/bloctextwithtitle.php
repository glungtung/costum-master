<?php 
$keyTpl = "bloctextwithtitle";
$kunik = $keyTpl.(string)$blockCms["_id"];
$blockKey = $blockCms["_id"];
$paramsData = [
	"title"			=> "Cet accompagnement vous permettra de :",
	"titleAlign"	=> "left",
	"titleSize"    	=> "22",
	"titleColor"   	=> "#ffffff",
	"textAlign"		=> "left",
	"textSize"	  	=> "18",
	"textColor"	  	=> "#ffffff",
	"bg_color"		=> "#44536a",
	"text"			=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut"
];

if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}
?>
<style type="text/css">
	
  .container<?php echo $kunik ?> .btn-edit-delete{
    display: none;
  }
  .container<?php echo $kunik ?> .btn-edit-delete .btn{
    box-shadow: 0px 0px 20px 3px #ffffff;
  }
  .container<?php echo $kunik ?>:hover .btn-edit-delete{
    display: block;
    position: absolute;
    top:20%;
    left: 50%;
    transform: translate(-50%,0%);
  }
  .text<?php echo $kunik ?>{
  	font-size: <?php echo $paramsData["textSize"]; ?>px;
  	color: <?php echo $paramsData["textColor"]; ?>; 
  	text-align: <?= $paramsData['textAlign'] ?>;
  }
  .text<?php echo $kunik ?> p{
  	font-size: <?php echo $paramsData["textSize"]; ?>px;
  	color: <?php echo $paramsData["textColor"]; ?>;
  	text-align: <?= $paramsData['textAlign'] ?> 
  }
  .text<?php echo $kunik ?> li{
  	font-size: <?php echo $paramsData["textSize"]; ?>px;
  	color: <?php echo $paramsData["textColor"]; ?>;
  	text-align: <?= $paramsData['textAlign'] ?> 
  }
  .text<?php echo $kunik ?> p, li{
  	font-size: <?php echo $paramsData["textSize"]; ?>px;
  	color: <?php echo $paramsData["textColor"]; ?>;
  	text-align: <?= $paramsData['textAlign'] ?> 
  }
</style>
<div class="container<?php echo $kunik ?> padding-20" style=" background-color: <?= $paramsData['bg_color'] ?>;">
	<div class="container">
		<div style="text-align: <?= $paramsData['titleAlign'] ?>">
		<p class="bold title" style="font-size: <?= $paramsData['titleSize'] ?>px;color: <?= $paramsData['titleColor'] ?>;padding-left: 55px"><?= $paramsData['title'] ?></p>
		</div>
		<div style="padding-bottom: 50px!important;padding-top: 50px!important;padding-left: 10% !important;padding-right: 10% !important;">
			<div class="markdown description text<?php echo $kunik ?>"><?= $paramsData['text'] ?></div>
		</div>
	</div>
</div>

<script type="text/javascript">
	sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

	jQuery(document).ready(function() {

		sectionDyf.<?php echo $kunik ?>Params = {
			"jsonSchema" : {    
				"title" : "Configurer votre section",
				"description" : "Personnaliser votre section",
				"icon" : "fa-cog",
				"properties" : {
					title : {
						label : "Titre",
						values :  sectionDyf.<?php echo $kunik ?>ParamsData.title
					},
					titleSize : {
						label : "Taille du titre",
						values :  sectionDyf.<?php echo $kunik ?>ParamsData.titleSize
					},
					titleColor : {
						label : "Couleur du titre",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik ?>ParamsData.titleColor
					},
					titleAlign : {
						label : "Alignement du titre",
						inputType : "select",
						options : {
							"left"    : "À Gauche",
							"right" : "À droite",
							"center"  :"Centré",
							"justify"  :"Justifié"
						},
						values :  sectionDyf.<?php echo $kunik ?>ParamsData.titleAlign
					},
					text : {
						label : "Taille du text",
						"inputType" : "textarea",
						"markdown" : true,
						values :  sectionDyf.<?php echo $kunik ?>ParamsData.textSize
					},
					textSize : {
						label : "Taille du text",
						values :  sectionDyf.<?php echo $kunik ?>ParamsData.textSize
					},
					textColor : {
						label : "Couleur du text",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik ?>ParamsData.textColor
					},	
					textAlign : {
						label : "Alignement du texte",
						inputType : "select",
						options : {
							"left"    : "À Gauche",
							"right" : "À droite",
							"center"  :"Centré",
							"justify"  :"Justifié"
						},
						values :  sectionDyf.<?php echo $kunik ?>ParamsData.textAlign
					},
					bg_color : {
						label : "Couleur du fond",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik ?>ParamsData.bg_color
					}
				},
				beforeBuild : function(){
					uploadObj.set("cms","<?php echo $blockKey ?>");
				},
				save : function () {  
					tplCtx.value = {};

					$.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
					});

					mylog.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
	                  dataHelper.path2Value( tplCtx, function(params) {
	                    dyFObj.commonAfterSave(params,function(){
	                      toastr.success("Élément bien ajouter");
	                      $("#ajax-modal").modal('hide');
	                      urlCtrl.loadByHash(location.hash);
	                    });
	                  } );
					}
				}
			}
		};

		$(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";

			dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params, null , sectionDyf.<?php echo $kunik ?>ParamsData);
		});
	});
</script>