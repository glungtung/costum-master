<?php 
$keyTpl = "text2Column";
$paramsData=[
	"title"=>"Lorem Ipsum",
	"sousTitle1" => "Lorem Ipsum",
	"content1"=>"Lorem Ipsum is simply dummy text of the printing and typesetting industry",
	"sousTitle2" => "Lorem Ipsum",
	"content2"=>"Lorem Ipsum is simply dummy text of the printing and typesetting industry",	
	"colorTitle" =>"#000",
	"colorsousTitle" =>"#000",
	"colorContent" =>"#000",
	"logo1"=>"",
	"logo2"=>""
];
if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}
?>
<?php 
$blockKey = (string)$blockCms["_id"];
$initImage1 = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>'logo1',
    ), "image"
  );
$initImage2 = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>'logo2',
    ), "image"
  );


$image1= [];
foreach ($initImage1 as $k => $v) {
	$image1[] =$v['imageThumbPath'];
}
$image2= [];
foreach ($initImage2 as $k => $v) {
	$image2[] =$v['imageThumbPath'];
}

?>
<style type="text/css">
	
	#fh5co-services_<?=$kunik?> {
		overflow: hidden;
		position: relative;
		color: <?= $paramsData["colorContent"]?>;
	}
	#fh5co-services_<?=$kunik?> .fh5co-service {
		padding-right: 30px;
	}
	#fh5co-services_<?=$kunik?> h3 {
		text-transform: uppercase;
		font-size: 24px;
		color: color: <?= $paramsData["colorsousTitle"]?>;;
	}
	#fh5co-services_<?=$kunik?> .icon {
		font-size: 70px;
		margin-bottom: 30px;
		display: -moz-inline-stack;
		display: inline-block;
		zoom: 1;
		*display: inline;
	}
	#fh5co-services_<?=$kunik?> .icon img{
		width: 100px;
		height: 100px;
	}
	#fh5co-services_<?=$kunik?> .section-heading_<?=$kunik?> h2 {
		color:  <?= $paramsData["colorTitle"]?>;;
		font-size: 30px;
		margin-top: 70px;
		margin-right: 40px;
		margin-left: 80px;

	}
	.section-heading_<?=$kunik?> {
		float: left;
		width: 100%;
		padding-bottom: 50px;
		margin-bottom: 50px;
		clear: both;
	}
	.section-heading_<?=$kunik?> h2 {
		margin: 55px 0 31px 0;
		font-size: 39px;
		font-weight: 300;
		position: relative;
		display: block;
		padding-bottom: 0px;
		text-align: center;
		line-height: 1.5;
		color: <?= $paramsData["colorTitle"]?>;
	}
	h2 p {
		font-size: 40px;
	}
	.section-heading_<?=$kunik?> h2.left-border:after {
		content: "";
		position: absolute;
		display: block;
		width: 80px;
		height: 2px; 
		color: <?= $paramsData["colorTitle"]?>;
		left: 0%;
		margin-left: 0px;
		bottom: 0;
	}
	.section-heading_<?=$kunik?> h2:after {
		content: "";
		position: absolute;
		display: block;
		width: 80px;
		height: 2px;
		color: <?= $paramsData["colorTitle"]?>;
		left: 50%;
		margin-left: -40px;
		bottom: 0;
	}
	.section-heading_<?=$kunik?> h3 {
		font-weight: 300;
		line-height: 1.5;
		color: <?= $paramsData["colorsousTitle"]?>;
	}
	@media screen and (max-width: 414px) {
		.section-heading_<?=$kunik?> h3 {
			font-size: 18px !important;
			line-height: 34px;
			margin-top: -15px;
		}
		#fh5co-services_<?=$kunik?> .section-heading_<?=$kunik?> h2 {
			font-size: 20px;
			margin-top: 40px;
			margin-bottom: 10px;

		}
		.section-heading_<?=$kunik?> p {
			font-size: 13px;
			line-height: 15px;
			text-align: justify;
		}
	}
	 @media (max-width: 768px) {
	 	#fh5co-services_<?=$kunik?>   .icon img {
		    width: 60px;
		    height: 60px;
		}
		#fh5co-services_<?=$kunik?> h3 {
		    text-align: center;
		}
		#fh5co-services_<?=$kunik?> .icon {
			display: flex;
    		justify-content: center;
		}
	 }
	/*.btn-edit-delete-<?= $kunik?>{
		display: none;
	}*/
	#fh5co-services_<?=$kunik?>:hover  .btn-edit-delete-<?= $kunik?> {
		display: block;
		-webkit-transition: all 0.9s ease-in-out 9s;
		-moz-transition: all 0.9s ease-in-out 9s;
		transition: all 0.9s ease-in-out 0.9s;
		position: absolute;
		top:50%;
		left: 50%;
		transform: translate(-50%,-50%);
	}
</style>
<div id="fh5co-services_<?=$kunik?>" data-section="services">
	<div class="container">
		<div class="row">
			<div class="col-md-12 section-heading_<?=$kunik?> text-left">
				<h2 class="  markdown fadeInUp animated"><?= $paramsData["title"]?></h2>
				<div class="row">
					<div class="col-md-6 col-sm-6 fh5co-service fadeInUp animated">
						<i class="col-md-3 icon fadeInUp animated-2 icon-anchor">
								<img src="<?= isset($image1[0]) ? $image1[0] :  Yii::app()->getModule('costum')->assetsUrl.'/images/blockCmsImg/VIDEO-02.svg'?>">
						</i>
						<div class="col-md-9">
							<h3>​<?= $paramsData["sousTitle1"]?></h3>
							<div class="markdown"><?= $paramsData["content1"]?></div>
						</div>
						
					</div>
					<div class="col-md-6 col-sm-6 fh5co-service fadeInUp animated">
						<i class="col-md-3 icon fadeInUp animated-2 icon-layers2">
							<img src="<?= isset($image2[0]) ? $image2[0] :  Yii::app()->getModule('costum')->assetsUrl.'/images/blockCmsImg/AUTRE-02.svg'?>">
							
						</i>
						<div class="col-md-9">
							<h3><?= $paramsData["sousTitle2"]?></h3>
							<div class="markdown"><?= $paramsData["content2"]?></div>
						</div>
						
					</div>
					<div class="clearfix visible-sm-block"></div>
				</div>
			</div>

		</div>
		
	</div>
</div>
<script type="text/javascript">
	
	sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	jQuery(document).ready(function() {
		sectionDyf.<?php echo $kunik?>Params = {
			"jsonSchema" : {    
				"title" : "Configurer votre section",
				"description" : "Personnaliser votre section",
				"icon" : "fa-cog",
				"properties" : {
					
					"title" : {
						label : "titre",
						"inputType" : "textarea",
						"markdown" : true,
						values :  sectionDyf.<?php echo $kunik?>ParamsData.title
					},
					"sousTitle1" : {
						label : "title1",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.sousTitle1
					},
					
					"content1" : {
						label : "Contenu1",
						"inputType" : "textarea",
						"markdown" : true,
						values :  sectionDyf.<?php echo $kunik?>ParamsData.content1
					},
					"logo1" : {
						"inputType" : "uploader",
						"label" : "photo de Banniere",
						"showUploadBtn" : false,
						"docType" : "image",
						"itemLimit" : 1,
						"contentKey" : "slider",
						"domElement" : "logo1",
						"placeholder" : "image logo",
						"afterUploadComplete" : null,
						//"template" : "qq-template-manual-trigger",
						"endPoint" : "/subKey/logo1",
						"filetypes" : [
							"png","jpg","jpeg","gif"
						],
                        initList : <?php echo json_encode($initImage1) ?>
					},
					
					"sousTitle2" : {
						label : "title2",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.sousTitle2
					},
					
					"content2" : {
						label : "Contenu2",
						"inputType" : "textarea",
						"markdown" : true,
						values :  sectionDyf.<?php echo $kunik?>ParamsData.content2
					},
					"logo2" : {
						"inputType" : "uploader",
						"label" : "logo",
						"showUploadBtn" : false,
						"docType" : "image",
						"itemLimit" : 1,
						"contentKey" : "slider",
						"domElement" : "logo2",
						"placeholder" : "image logo",
						"afterUploadComplete" : null,
						//"template" : "qq-template-manual-trigger",
						"endPoint" : "/subKey/logo2",
						"filetypes" : [
						"png","jpg","jpeg","gif"
						],
                        initList : <?php echo json_encode($initImage2) ?>
					},

					"colorTitle":{
						label : "Couleur du title",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.colorTitle
					},
					"colorsousTitle":{
						label : "Couleur du sous titre",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.colorsousTitle
					},
					"colorContent":{
						label : "Couleur du contenu",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.colorContent
					}
				},
				beforeBuild : function(){
					uploadObj.set("cms","<?php echo $blockKey ?>");
				},
				save : function () {  
					tplCtx.value = {};

					$.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
					});

					mylog.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
		                  dataHelper.path2Value( tplCtx, function(params) {
		                    dyFObj.commonAfterSave(params,function(){
		                      toastr.success("Élément bien ajouter");
		                      $("#ajax-modal").modal('hide');
		                      urlCtrl.loadByHash(location.hash);
		                    });
		                  } );
					}
				}
			}

		};
		mylog.log("paramsData",sectionDyf);
		$(".edit<?php echo $kunik?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";
			dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
		});
	});
</script>