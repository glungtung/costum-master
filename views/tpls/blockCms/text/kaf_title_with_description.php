<?php 
  $keyTpl ="kaf_title_with_description";
  $paramsData = [ 
    "title" => "Lorem ipsum",
    "description" => "Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum"/*,
    "background" => "transparent",
    "backgroundAlign" => "center"*/
  ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) {
        $paramsData[$e] = $blockCms[$e];
      }
    }
  }
 ?>

<style>
  
  .item<?php echo $kunik ?>{
    text-transform: none !important;
  }
  .container<?php echo $kunik ?> {
    padding-top: 30px;
  }

</style>

<div class="container<?php echo $kunik ?> col-md-12">
  
  <h2 class="title<?php echo $kunik ?> title-1">
    <?php echo $paramsData["title"] ?>
  </h2>
  <div class="title-2 markdown"><?php echo $paramsData["description"] ?></div>
</div>

 <script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {    
            "title" : "Configurer votre section",
            "description" : "Personnaliser votre section",
            "icon" : "fa-cog",
            
            "properties" : {
                "title" : {
                    "inputType" : "text",
                    "label" : "Titre",
                    
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.title
                },
                "description" :{
                      "label" : "Description",
                      "inputType" : "textarea",
                      "markdown" : true,
                      values :  sectionDyf.<?php echo $kunik?>ParamsData.description
                }         
            },
            beforeBuild : function(){
              uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;

                if(k == "description")
                  tplCtx.value[k] = data.description;
              });
              console.log("save tplCtx",tplCtx);

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("Élément bien ajouter");
                      $("#ajax-modal").modal('hide');
                      urlCtrl.loadByHash(location.hash);
                    });
                  } );
              }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
    });
</script>

