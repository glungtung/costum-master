<?php
$keyTpl     = "textWithButton";
$paramsData = [
    "title"             => "Lorem Ipsum",
    "sizeTitle"         => "35",
    "content"           => "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
    "sizeContent"       => "20",
    "lienButton"        => "",
    "labelButton"       => "Boutton",
    "colorlabelButton"  => "#000000",
    "colorBorderButton" => "#000000",
    "colorButton"       => "#ffffff",
    "class"             => "lbh",
    "link"              => ""
];
if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if ( isset($blockCms[$e])) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}
?>
<style type="text/css">
  .textBack_<?=$kunik?> h2{
    margin-bottom: 10px;
    margin-top: 10px;
  }
  .button_<?=$kunik?> {
    background-color: <?=$paramsData["colorButton"]?>;
    border:1px solid <?=$paramsData["colorBorderButton"]?>;
    color: <?=$paramsData["colorlabelButton"]?>;
    margin-bottom: 4%;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    padding: 10px;
    font-size: 16px;
    cursor: pointer;
    border-radius: 20px ;
    padding: 8px 8px;
  }
  .block-container-<?=$kunik?> {
    background-attachment: fixed;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
  }

@media (max-width: 414px) {
  .textBack_<?=$kunik?> h2{
   font-size: 20px;
  }
  .textBack_<?=$kunik?> p{
    font-size: 14px
  }

  .button_<?=$kunik?> {
    padding: 8px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 11px;
    cursor: pointer;
    border-radius: 20px;
  }
}

@media screen and (min-width: 1500px){
  .textBack_<?=$kunik?> h2{
    font-size: 40px;
  }
  .textBack_<?=$kunik?> p{
    line-height: 35px;
    font-size: 25px;
  }
  .button_<?=$kunik?> {
    padding: 10px 20px;
    margin-bottom: 2%;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 25px;
    cursor: pointer;
    border-radius: 35px;
  }
}
</style>

  <section  class="parallax_<?=$kunik?>" >
    <div class="textBack_<?=$kunik?> text-center container"  >
      <h2 class="title"> <?=$paramsData["title"]?></h2>
      <p class="markdown description" ><?=$paramsData["content"]?></p>

      <a href="<?=$paramsData["lienButton"]?>" class="<?=$paramsData["link"]?> button_<?=$kunik?>"  target="_blank">
       <?=$paramsData["labelButton"]?>
     </a>
   </div>
   
 </section>
<script type="text/javascript">
  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode($paramsData); ?>;
  jQuery(document).ready(function() {
    sectionDyf.<?php echo $kunik ?>Params = {
      "jsonSchema" : {
        "title" : "Configurer votre section",
        "description" : "Personnaliser votre section",
        "icon" : "fa-cog",
        "properties" : {
          "title" : {
            "label" : "titre",
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.title
          },
          "content" : {
            "inputType" : "textarea",
            "label" : "Contenu",
            "markdown" : true,
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.content
          },
          "labelButton" : {
            "label" : "Labelle du Boutton",
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.labelButton
          },
          "link":{ 
            "label" : "Lien interne ou externe ",
            inputType : "select",
            options : {              
              "lbh " : "Interne",
              " " : "Externe",
            },
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.link
          },
          "lienButton" : {
            "label" : "lien du boutton",
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.lienButton
          },
          "colorlabelButton":{
            label : "Couleur du labelle de boutton",
            inputType : "colorpicker",
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.colorlabelButton
          },
          "colorButton":{
            label : "Couleur du boutton",
            inputType : "colorpicker",
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.colorButton
          },
          "colorBorderButton":{
            label : "Couleur du bordure de boutton",
            inputType : "colorpicker",
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.colorBorderButton
          }
        },

        beforeBuild : function(){
          uploadObj.set("cms","<?php echo $blockKey ?>");
        },
        save : function () {
          tplCtx.value = {};
          $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
            tplCtx.value[k] = $("#"+k).val();
            if (k == "parent") {
              tplCtx.value[k] = formData.parent;
            }
          });
          mylog.log("save tplCtx",tplCtx);

          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
          else {
              dataHelper.path2Value( tplCtx, function(params) {
                dyFObj.commonAfterSave(params,function(){
                  toastr.success("Élément bien ajouter");
                  $("#ajax-modal").modal('hide');
                  urlCtrl.loadByHash(location.hash);
                });
              } );
          }

        }
      }
    };
    mylog.log("sectiondyfff",sectionDyf);
    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });


  });
</script>