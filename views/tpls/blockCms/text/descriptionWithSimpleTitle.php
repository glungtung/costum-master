<?php 
  $keyTpl ="descriptionWithSimpleTitle";
  $paramsData = [ 
    "title" => "Lorem ipsum",
    "titleColor" => "black",
    "titleAlign" => "center",
    "description" => [],
    "descriptionColor" => "black",
    "descriptionAlign" => "center"/*,
    "background" => "transparent",
    "backgroundAlign" => "center"*/
  ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) {
          if($e=="description")
              $paramsData[$e] = array_merge($paramsData[$e],$blockCms[$e]);
          else
              $paramsData[$e] = $blockCms[$e];
      }
    }
  } 
 ?>

<style>
  @font-face {
    font-family: "ReenieBeanie";
    src: url("<?php  echo Yii::app()->getModule('costum')->assetsUrl?>/font/glazBijoux/ReenieBeanie-Regular.ttf");
  }
  @font-face {
    font-family: "Oswald";
    src: url("<?php  echo Yii::app()->getModule('costum')->assetsUrl?>/font/glazBijoux/Oswald-VariableFont_wght.ttf");
  }
  @font-face {
    font-family: "JosefinSansRegular";
    src: url("<?php  echo Yii::app()->getModule('costum')->assetsUrl?>/font/glazBijoux/JosefinSans-Regular.ttf");
  }
  @font-face {
    font-family: "JosefinSansLight";
    src: url("<?php  echo Yii::app()->getModule('costum')->assetsUrl?>/font/glazBijoux/JosefinSans-Light.ttf");
  }
  .ReenieBeanie{
    font-family: "ReenieBeanie";
  }
  .Oswald{
    font-family: Oswald;
  }
  .JosefinSansRegular{
    font-family: JosefinSansRegular;
  }
  .JosefinSansLight{
    font-family: JosefinSansLight;
  }
 /* .container<?php// echo $kunik ?> .btn-edit-delete{
    display: none;
  }
  .container<?php echo $kunik ?>:hover .btn-edit-delete{
    display: block;
    position: absolute;
    top:0%;
    left: 50%;
    transform: translate(-50%,0%);
  }*/
  .title<?php echo $kunik ?>{
    text-transform: none !important;
    text-align: <?php echo $paramsData["titleAlign"] ?> !important;
    color:<?php echo $paramsData["titleColor"] ?> !important;
    margin-bottom: 30px;
  }
  .item<?php echo $kunik ?>{
    text-transform: none !important;
    text-align: <?php echo $paramsData["descriptionAlign"] ?>;
    font-size:23px;
    color:<?php echo $paramsData["descriptionColor"] ?>;
  }
  .container<?php echo $kunik ?> {
    padding-top: 30px;
  }
  hr.<?php echo $kunik ?> {
    border: 2px solid;
    border-color: <?php echo $paramsData["titleColor"] ?> ;
    display: block;
    width: 30%;
    margin: 0 auto!important;
    
}
</style>

<div class="container<?php echo $kunik ?> col-md-12">
  
  <h2 class="title<?php echo $kunik ?> Oswald title">
    <?php echo $paramsData["title"] ?>
  </h2>
  <?php foreach ($paramsData['description'] as $key => $value) { ?>
      <div class="item<?php echo $kunik ?> JosefinSansLight description"><?php echo $value["item"]?></div>
  <?php } ?>
  <hr class="<?php echo $kunik ?>">
</div>

 <script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {    
            "title" : "Configurer votre section",
            "description" : "Personnaliser votre section",
            "icon" : "fa-cog",
            
            "properties" : {
                "title" : {
                    "inputType" : "text",
                    "label" : "Titre",
                    
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.title
                },
                "titleColor" : {
                    "inputType" : "colorpicker",
                    "label" : "Couleur du titre",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.titleColor
                },
                "titleAlign" : {
                    "inputType" : "select",
                    "label" : "Alignement du titre",
                    "options":{
                      "left":"à gauche",
                      "center":"au centre",
                      "right":"à droite"
                    }
                },
                "description" :{
                      "inputType" : "lists",
                      "label" : "Description",
                      "entries" :{
                        "item" :{
                          "label" :"Paragraphe",
                          "type" : "textarea",
                        }
                      }
                },
                "descriptionColor" : {
                    "inputType" : "colorpicker",
                    "label" : "Couleur de la description",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.descriptionColor
                },
                "descriptionAlign" : {
                    "inputType" : "select",
                    "label" : "Alignement du texte",
                    "options":{
                      "left":"à gauche",
                      "center":"au centre",
                      "right":"à droite"
                    }
                },
                /*"background" : {
                    "inputType" : "colorpicker",
                    "label" : "Couleur du fond",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.background
                }*/          
            },
            beforeBuild : function(){
              uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;

                if(k == "description")
                  tplCtx.value[k] = data.description;
              });
              console.log("save tplCtx",tplCtx);

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("Élément bien ajouter");
                      $("#ajax-modal").modal('hide');
                      urlCtrl.loadByHash(location.hash);
                    });
                  } );
              }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
    });
</script>

