
<?php 
    $keyTpl ="textWithCoformButton";
    $paramsData = [
        "contentTitle"=>"S'associer avec Open Atlas",
        "buttonCoform"=>"bouton coform",
        "buttonInfos"=>"En savoir plus",
        "linkInfos"=>"",
        "introduction"=>"Continuité naturelle de votre parcours au sein de Open Atlas, le sociétariat vous permet de prendre part à la gouvernance et aux orientations stratégiques de la coopérative.",
        "formId"=>""
    ];

    $childForm = PHDB::find("forms", array("parent.".$this->costum['contextId']=>['$exists'=>true]));

    if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
            if (  isset($blockCms[$e]) ) {
                $paramsData[$e] = $blockCms[$e];
            }
        }
    }
?>

<!-- ****************get image uploaded************** -->
<?php 

  $initImage = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>'image',
    ), "image"
  );
    $image = [];

    foreach ($initImage as $key => $value) {
        $image[]= $value["imagePath"];
    }

    if($image!=[]){
        $paramsData["image"] = $image[0];
    }
    
 ?>
<!-- ****************end get image uploaded************** -->

<?php 

    $userId = "";

    if(isset(Yii::app()->session["userId"])){
        $userId = Yii::app()->session["userId"];
    }

    # If user is admin
    #$is_admin = false;
    #$admins = PHDB::find("organizations", array("links.members.".Yii::app()->session["userId"].".isAdmin"=>true));
    #if(count($admins)!=0){
    #    $is_admin = true;
    #}

    $formId = "";

    # If user has already joined
    $members = PHDB::find("organizations", array("links.members.".$userId=>['$exists' => true ], "source.keys"=>$this->costum['slug']));
    
    $is_member = false;
    if(count($members)!=0){
        $is_member = true;
    }

    if($paramsData["formId"]!=""){
        $formId = $paramsData["formId"];
    }else{
        # Get the form id
        $form = PHDB::find("forms", array("parent.".$this->costum['contextId']=>['$exists'=>true]));
        foreach($form as $key => $value) { $formId = $key; }
    }

    # If user has submited the coform
    $answers = PHDB::find("answers", array("source.keys" => $this->costum['slug'], 'form' => $formId ,  "user" => $userId, "draft"=>['$exists' => false ]));
    $has_answered = false;
    
    if(count($answers)!=0){
        $has_answered = true;
    }

    # Get the user's answer Id
    $myAnswer = "";
    foreach ($answers as $key => $value) {
        if($userId == $value["user"] && isset($value['_id']->{'$id'})){
            $myAnswer = $value['_id']->{'$id'};
        }
    }

    if($userId!=""){
        
        $data = PHDB::findByIds("citoyens", [$userId] ,["links.follows", "links.memberOf"]);
        
        $memberOf_ids = array();

        if(isset($data[$userId]["links"]["memberOf"])){
            $links2 = $data[$userId]["links"]["memberOf"];
            foreach ($links2 as $key => $value) {
                array_push($memberOf_ids, $key);
            }
            $organizations = PHDB::findByIds("organizations", $memberOf_ids ,["name", "profilImageUrl"]);
        }
    }
?>

<section id="AssoLibre" class="text-center container">
    <h1 class="title-1 col-xs-12 col-lg-12" class="col-lg-9 padding-20 text-center" > 
        <?php echo $paramsData["contentTitle"]; ?>
    </h1>
    <div class="text-center col-xs-12 description markdown" style="margin-top: 2vw">
        <?php echo "\n".$paramsData["introduction"]; ?>
    </div>

    <?php if(isset($_SESSION["userId"])){ 
        $myAnswer = PHDB::findOne("answers", array("user"=>$userId, "form"=>$formId, "draft" => ['$exists' => false ]));
        $formString = "";
        if(isset($myAnswer["_id"]) && !empty($myAnswer["_id"])){
            $formString = "#answer.index.id.".$myAnswer["_id"]->{'$id'}.".mode.w";
        }else{
            $formString = "#answer.index.id.new.form.".$formId;
        }
    ?>
    <div class="col-xs-12 margin-top-20">
        <?php if($formId!=""){ ?>
            <a class="btn btn-success btn-lg lbh" href="<?php echo $formString ?>" style="text-decoration : none;"> 
                <?php echo $paramsData["buttonCoform"]; ?>
            </a>
        <?php } ?>

        <?php if($paramsData["buttonInfos"]!=""){ ?>
        <?php 
            $urlAction = "href='javascript:void(0);'";
            if(filter_var($paramsData["linkInfos"], FILTER_VALIDATE_URL)){
                $urlAction = 'target="_blank" href="'.$paramsData["linkInfos"].'"';
            }

        ?>
            &nbsp;
            <a class="btn btn-primary btn-lg btn-plusinfo"  <?php echo $urlAction; ?> style="text-decoration : none;"> 
                <?php echo $paramsData["buttonInfos"] ?>
            </a>
        <?php } ?>
    </div>
<?php }else{ ?>
    <div class="text-center">
        <a href="javascript:;" class="btn btn-primary" data-toggle="modal" data-target="#modalLogin" style="padding: .8em; margin-top: 1em">
            <i class="fa fa-sign-in" style="margin-right: .6em"></i>Se connecter ou Créer un compte
        </a>
    </div>
<?php } ?>
</section>

<script type="text/javascript">

    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

    jQuery(document).ready(function() {
        
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {    
            "title" : "Configurer votre bloc",
            "description" : "Personnaliser votre bloc",
            "icon" : "fa-cog",
            
            "properties" : {
                "contentTitle" : {
                    "inputType" : "text",
                    "label" : "Text du titre",
                    value :  sectionDyf.<?php echo $kunik ?>ParamsData.contentTitle
                },
                "buttonCoform" : {
                    "inputType" : "text",
                    "label" : "Text du bouton pour l'accès au coform",
                    value :  sectionDyf.<?php echo $kunik ?>ParamsData.buttonCoform
                },
                "buttonInfos" : {
                    "inputType" : "text",
                    "label" : "Texte du bouton pour plus d'informations",
                    value :  sectionDyf.<?php echo $kunik ?>ParamsData.buttonInfos
                },
                "linkInfos" : {
                    "inputType" : "text",
                    "label" : "Lien vers plus d'informations",
                    value :  sectionDyf.<?php echo $kunik ?>ParamsData.linkInfos
                },
                "formId": {
                    "label" : "Formulaire :",
                    "class" : "form-control",
                    "inputType" : "select",
                    "options": {
                        <?php 
                            foreach($childForm as $key => $value) { 
                                echo  '"'.$key.'" : "'.$value["name"].'",';
                            }    
                        ?>
                    }
                },
                "introduction":{
                    "inputType" : "textarea",
                    "markdown" : true,
                    "label" : "Text",
                    value : sectionDyf.<?php echo $kunik ?>ParamsData.introduction
                },
            },
           beforeBuild : function(){
              uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;

                if(k == "items")
                  tplCtx.value[k] = data.items;
              });

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                   dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("Élément bien ajouter");
                      $("#ajax-modal").modal('hide');
                      urlCtrl.loadByHash(location.hash);
                    });
                  });
                }
            }
        }
    }

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });

        
    });
    
</script>
