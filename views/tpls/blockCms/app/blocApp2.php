<?php 
$keyTpl = "blocApp2";
$paramsData = [];

if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (  isset($blockCms[$e]) ) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
}  
?>
<style type="text/css">
	.bloc-app-<?= $kunik?> h2{
	    margin-top: 5%;
	    margin-bottom: 5%;
	  }
  	.bloc-app-<?= $kunik?> p{
	    margin-top: 5%;
	    margin-left: 5%;
	    margin-right: 9%;
	}
	.bloc-app-<?= $kunik?> img {
		height: 250px;
		width: 100%;
	}
	.bloc-app-<?= $kunik?> .bouton{
	    margin-top: 2%; 
	    width: 85%;
	    left: 8%;
	    margin-bottom: 8%;
	 }
	.app-<?= $kunik?>{
		max-height: 600px; 
		min-height: 520px;
	 }

</style>
<section class="bloc-app-<?= $kunik?> slider">
	<?php if (isset($blockCms["content"])) {
		$content = $blockCms["content"];
		foreach ($content as $key => $value) {
			$initImage= Document::getListDocumentsWhere(
			array(
				"id"=> $blockKey,
				"type"=>'cms',
				"subKey"=> (string)$key 
			), "image"
			);
			$arrImage= [];
			foreach ($initImage as $k => $v) {
				$arrImage[] =$v['imagePath'];
			}
			$bgColor = isset($value["bgColor"])?$value["bgColor"]:"";
			$icon = isset($value["icon"])?$value["icon"]:"";
			$title = isset($value["title"])?$value["title"]:"";
			$description = isset($value["description"])?$value["description"]:"";
			$labelBtn1 = isset($value["labelBtn1"])?$value["labelBtn1"]:"";
			$linkBtn1 = isset($value["linkBtn1"])?$value["linkBtn1"]:"";
			$labelBtn2 = isset($value["labelBtn2"])?$value["labelBtn2"]:"";
			$linkBtn2 = isset($value["linkBtn2"])?$value["linkBtn2"]:"";

		?>
		<div class="col-md-4 col-xs-12  no-padding text-center app-<?= $kunik?> " style="background-color:<?= $bgColor ?>" >
			<?php if (count($arrImage)!=0) {?>
				<img src="<?= $arrImage[0]?>">
			<?php }else { ?>
		    	<img src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/cocity/photo/aide.jpg" class="img-responsive img-carousel " >
			<?php } ?>
		    <h2 class="h2-bloc title">
		      <i class="fa <?= $icon?>"></i> <?= $title?>
		    </h2>
		    <p class="para-bloc description">
		      <?= $description?> 
		    </p>
		    <div class="col-xs-12">
		      <a href="javascript:;" onclick="dyFObj.openForm('ressources', null, {'section' : 'need'});" class="btn btn-lg btn-default">
		        <?= $labelBtn1?>
		      </a>
		    </div>
		    <div class="col-xs-12 bouton" >
		      <a href="javascript:;" data-hash="#annonce"  class="btn btn-lg btn-block btn-default lbh-menu-app">
		        <?= $labelBtn2?>
		      </a>
		    </div>
		    <?php if(Authorisation::isInterfaceAdmin()){ ?>
			<div class="editdelete<?= $blockKey?>">
				<a  href="javascript:;"
					class="btn material-button text-center editElement<?= $blockKey ?> "
					data-key="<?= $key ?>" 
					data-title="<?= $title ?>" 
					data-icon="<?= $icon ?>" 
					data-bgColor="<?= $bgColor ?>"
					data-labelBtn1="<?= $labelBtn1 ?>"
					data-linkBtn1="<?= $linkBtn1 ?>"
					data-labelBtn2="<?= $labelBtn2 ?>"
					data-linkBtn2="<?= $linkBtn2 ?>"
					data-image ='<?= json_encode($initImage)?>'
					data-description="<?= $description ?>" >
					<i class="fa fa-edit"></i>
				</a>
				<a  href="javascript:;"
					class="btn material-button btn-danger text-center deletePlan<?= $blockKey ?> "
					data-key="<?= $key ?>" 
					data-id ="<?= $blockKey ?>"
					data-path="content.<?= $key ?>"
					data-collection = "cms"
					>
					<i class="fa fa-trash"></i>
				</a>
			</div>	
			<?php } ?>
		 </div>
		<?php
		}
	}
	?>
</section>
<div class="text-center ">
	<div class="" style="width: 100%; display: inline-table; padding: 10px;">
		<?php if(Authorisation::isInterfaceAdmin()){?>
			<div class="text-center addElement<?= $blockKey?>">
				<?php if (!isset($blockCms["content"])) { ?>
					<p>Le contenu sera afficher ici</p>
				<?php } ?>
				<button class="btn btn-primary">Ajouter contenu</button>
			</div>	
		<?php } ?>
	</div>
</div>

<script type="text/javascript">
	jQuery(document).ready(function() {

		$(".addElement<?= $blockCms['_id'] ?>").click(function() {  
			var keys = Object.keys(<?php echo json_encode($content); ?>);
			var	lastKey = 0; 
			if (keys.length!=0) 
				var	lastKey = parseInt((keys[(keys.length)-1]), 10);
			var tplCtx = {};
			tplCtx.id = "<?= $blockKey ?>";
			tplCtx.collection = "cms";

			tplCtx.path = "content."+(lastKey+1);
			var obj = {
				subKey : (lastKey+1),
				title : 		$(this).data("title"),
				icon:    $(this).data("icon"),
				bgColor : $(this).data("bgColor"),
				labelBtn1:    $(this).data("labelBtn1"),
				linkBtn1:    $(this).data("linkBtn1"),
				labelBtn2:    $(this).data("labelBtn2"),
				linkBtn2:    $(this).data("linkBtn2"),
				description:    $(this).data("description")
						
			};
			var activeForm = {
				"jsonSchema" : {
					"title" : "Ajouter nouveau section",
					"type" : "object",
					onLoads : {
						onload : function(data){
							$(".parentfinder").css("display","none");
						}
					},
					"properties" : getProperties(obj),
					beforeBuild : function(){
			            uploadObj.set("cms","<?= $blockCms['_id'] ?>");
			        },
			        save : function (data) {  
			            tplCtx.value = {};
			            $.each( activeForm.jsonSchema.properties , function(k,val) { 
			              tplCtx.value[k] = $("#"+k).val();
			            });
			            mylog.log("save tplCtx",tplCtx);

			            if(typeof tplCtx.value == "undefined")
			              toastr.error('value cannot be empty!');
			            else {
			               	dataHelper.path2Value( tplCtx, function(params) {
				                dyFObj.commonAfterSave(params,function(){
				                    toastr.success("Élément bien ajouter");
				                    $("#ajax-modal").modal('hide');
				                    urlCtrl.loadByHash(location.hash);
				                });
			                });
			            }

			        }
				}
			};          
			dyFObj.openForm( activeForm );
		});
		$(".editElement<?= $blockCms['_id'] ?>").click(function() {  
			var contentLength = Object.keys(<?php echo json_encode($content); ?>).length;
			var key = $(this).data("key");
			var tplCtx = {};
			tplCtx.id = "<?= $blockCms['_id'] ?>"
			tplCtx.collection = "cms";
			tplCtx.path = "content."+(key);
			var obj = {
				subKey : key,
				title : 		$(this).data("title"),
				icon:    $(this).data("icon"),
				bgColor : $(this).data("bgColor"),
				image : $(this).data("image"),
				labelBtn1:    $(this).data("labelBtn1"),
				linkBtn1:    $(this).data("linkBtn1"),
				labelBtn2:    $(this).data("labelBtn2"),
				linkBtn2:    $(this).data("linkBtn2"),				
				description:    $(this).data("description")
			};
			var activeForm = {
				"jsonSchema" : {
					"title" : "Modifier",
					"type" : "object",
					onLoads : {
						onload : function(data){
							$(".parentfinder").css("display","none");
						}
					},
					"properties" : getProperties(obj),
					beforeBuild : function(){
		                uploadObj.set("cms","<?= $blockCms['_id'] ?>");
		            },
		            save : function (data) {  
		              tplCtx.value = {};
		              $.each( activeForm.jsonSchema.properties , function(k,val) { 
		                tplCtx.value[k] = $("#"+k).val();
		              });
		              console.log("save tplCtx",tplCtx);

		              if(typeof tplCtx.value == "undefined")
		                toastr.error('value cannot be empty!');
		              else {
		                  dataHelper.path2Value( tplCtx, function(params) {
		                    dyFObj.commonAfterSave(params,function(){
		                      toastr.success("Modification enregistrer");
		                      $("#ajax-modal").modal('hide');
		                      urlCtrl.loadByHash(location.hash);
		                    });
		                  } );
		              }

		            }
				}
				};          
				dyFObj.openForm( activeForm );
			});
	});	
	function getProperties(obj={}){
		mylog.log("bbbbbb", obj);
		alert(obj["labelBtn1"]);
		var props = {

			title : {
				label : "Titre",
				"inputType" : "text",
				value : obj["title"]
			},
			icon : { 
	            label : "Icone",
	            inputType : "select",
	            options : <?= json_encode(Cms::$icones); ?>,
				value : obj["icon"]
	        },	
	        description : {
				label : "Description",
				"inputType" : "textarea",
				"markdown" : true,
				value :  obj["description"]
			},			
			labelBtn1 : {
				label : "label du bouton 1",
				value :  obj["labelBtn1"]
			},
			linkBtn1 : {
				label : "lien du bouton 1",
				value :  obj["linkBtn1"]
			},	
			labelBtn2 : {
				label : "label du bouton 2",
				value :  obj["labelBtn2"]
			},	
			linkBtn2 : {
				label : "lien du bouton 2",
				value :  obj["linkBtn2"]
			},
			bgColor:{
				label : "Couleur du background",
				inputType : "colorpicker",
				value :  obj["bgColor"]
			},			
			img : {
				"inputType" : "uploader",	
				"label" : "image ",
	            "docType": "image",
				"contentKey" : "slider",
				"domElement" : obj["subKey"],		
	            "filetypes": ["jpeg", "jpg", "gif", "png"],
	            "showUploadBtn": false,
	            "endPoint" :"/subKey/"+obj["subKey"],
	            initList : obj["image"]		
			}
		};
		return props;
	}
</script>