<?php 
$keyTpl = "blocApp";
$paramsData = [];

if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (  isset($blockCms[$e]) ) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
}  
?>
<style type="text/css">
  .bloc-app-cocity_<?= $kunik?>{
    margin-top: 5%;
    color: white;
    margin-bottom: 10%;
  }
  .bloc-app-cocity_<?= $kunik?> h2{
    margin-top: 5%;
    color: white;
    margin-bottom: 5%;
  }
  .bloc-app-cocity_<?= $kunik?> p{
    margin-top: 5%;
    color: white;
    font-size: 16px;
    margin-left: 5%;
    margin-right: 9%;
  }
  .bloc-app-cocity_<?= $kunik?> .bouton{
    margin-top: 2%; 
    width: 85%;
    left: 8%;
    margin-bottom: 8%;
  }
  
  .bloc-app-cocity_<?= $kunik?> .carousel-control .glyphicon-chevron-left,.carousel-control .glyphicon-chevron-right{
    color: #000;
    top: 108%;
    font-size: 20px;
  }
  .img-carousel {
    height: 250px;
    width: 100%; 
  }
  .colonne1 {
    background: #0A96B5;
    margin-bottom: 2%;
    max-height: 600px; 
    min-height: 520px;
  }
  .colonne2{
    background: #8ABF32;
    max-height: 600px; 
    min-height: 520px;
  }

  @media (max-width: 978px) {
    .img-carousel {
      height: 250px;
      width: 100%;
    }
    .bloc-app-cocity_<?= $kunik?> h2{
      margin-top: 3%;
      font-size: 20px;
      margin-bottom: 5%;
    }
    .bloc-app-cocity_<?= $kunik?> p{
      font-size: 16px;
      margin-bottom: 4%;
    }
    .colonne1 {
      margin-top: 2%;
      margin-bottom: 0;
      height: 490px;
      background: #0A96B5;
    }
    .colonne2{
      margin-top: 2%;
      height: 490px;
      background: #8ABF32;
    }
    .carousel-inner a{
      font-size: 14px;
    } 
    .carousel-inner .bouton{
      margin-top: 2%;
      width: 60%;
      left: 20%;
      margin-bottom: 2%;
    }
    .bloc-app-cocity_<?= $kunik?> .carousel-indicators{
      top: 100%;
    }
    .bloc-app-cocity_<?= $kunik?> .carousel-control .glyphicon-chevron-left,.carousel-control .glyphicon-chevron-right{
      color: #000;
      top: 50%;
      font-size: 20px;
    }
    .bloc-app-cocity_<?= $kunik?> .carousel-control.left {
      background-image: initial;
      left: -5%;
    }
    .bloc-app-cocity_<?= $kunik?> .carousel-control.right{
      background-image: initial;
      right: -5%;
    }
  }
</style>
<div class="bloc-app-cocity_<?= $kunik?>">

  <div class="col-md-4 col-sm-6 col-xs-12  no-padding text-center colonne1" >
    <img src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/cocity/photo/aide.jpg" class="img-responsive img-carousel " >
    <h2 class="h2-bloc">
      <i class="fa fa-rocket"></i> J'ai besoin d'aide
    </h2>
    <p class="para-bloc">
      besoin d'aide dans vos projets ? faites appel à nous! 
    </p>
    <div class="col-xs-12">
      <a href="javascript:;" onclick="dyFObj.openForm('ressources', null, {'section' : 'need'});" class="btn btn-lg btn-default">
        Proposer une demande
      </a>
    </div>
    <div class="col-xs-12 bouton" >
      <a href="javascript:;" data-hash="#annonce"  class="btn btn-lg btn-block btn-default lbh-menu-app">
        Voir les demandes en cours
      </a>
    </div>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 no-padding text-center colonne2" style="">
      <img src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/cocity/photo/MainHomme.jpg" class="img-responsive img-carousel" >
      <h2 class="h2-bloc">
        <i class="fa fa-calendar"></i> 
        J'offre mes services
      </h2>
      <p class="para-bloc">
        Vous souhaiter organiser un événement et vous chercher un moyen de le partager?
      </p>
      <div class="col-xs-12">

        <a href="javascript:;" onclick="dyFObj.openForm('ressources', null, {'section' : 'offer'});" class="btn btn-lg btn-default">
          Proposer une offre
        </a>
      </div>
      <div class="col-xs-12 bouton" >
       <a href="javascript:;" data-hash="#annonce"  class="lbh-menu-app btn btn-lg btn-block btn-default">
         Voir les évènements en cours
       </a>

     </div>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 no-padding text-center colonne1" >
    <img src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/cocity/photo/alimentation.jpg" class="img-responsive img-carousel" >
    <h2 class="h2-bloc">
      <i class="fa fa-rocket"></i> Ressources locales
    </h2>
    <p class="para-bloc">
      Découvrez les ressources en vraks disponible dans la commune 
    </p>
    <div class="col-xs-12">
      <a href="javascript:;" onclick="dyFObj.openForm('ressources', null, {'section' : 'need'});" class="btn btn-lg btn-default">Proposer une nouvelle ressource</a>
    </div>
    <div class="col-xs-12 bouton" >
      <a href="javascript:;"  data-hash="#annonce" class=" lbh-menu-app btn btn-lg btn-block btn-default">Voir les organises</a>
    </div>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 no-padding text-center colonne2 ">
    <img src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/cocity/photo/Photo-Citoyen.jpg" class="img-responsive img-carousel" >
    <h2 class="h2-bloc">
      <i class="fa fa-rocket"></i> 
      Projet
    </h2>
    <p class="para-bloc">
      Vous avez un projet qui vous tient à coeur ? Partager la 
    </p>
    <div class="col-xs-12">
      <a href="javascript:;" onclick="dyFObj.openForm('project');"  class="btn btn-lg btn-default">Proposer un projet</a>
    </div>
    <div class="col-xs-12 bouton" >
      <a href="javascript:;" data-hash="#project"  class="lbh-menu-app btn btn-lg btn-block btn-default">Voir les projets en cours</a>  
    </div>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 no-padding text-center colonne1 ">
    <img src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/cocity/photo/cover.jpg" class="img-responsive img-carousel" >
    <h2 class="h2-bloc">
      <i class="fa fa-calendar"></i> 
      Évènement
    </h2>
    <p class="para-bloc">
      Vous souhaitez organiser un évènement et vous cherchez un moyen de le partager ?  
    </p>
    <div class="col-xs-12">
      <a href="javascript:;" onclick="dyFObj.openForm('event');"  class="btn btn-lg btn-default">Proposer un évènement</a>
      
    </div>
    <div class="col-xs-12 bouton" >
      <a href="javascript:;" data-hash="#agenda"  class="lbh-menu-app btn btn-lg btn-block btn-default">Qu'est-ce qui se passe ici?</a>
    </div>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 no-padding text-center colonne2 ">
    <img src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/cocity/photo/Outils.jpg" class="img-responsive img-carousel" >
    <h2 class="h2-bloc">
      <i class="fa fa-calendar"></i> 
      Organisation
    </h2>
    <p class="para-bloc">
      Vous êtes une organisation et vous souhaitez être répertorier ? Partager la
    </p>
    <div class="col-xs-12">
      <a href="javascript:;" onclick="dyFObj.openForm('organization');" class="btn btn-lg btn-default">Proposer une organisation</a>
    </div>
    <div class="col-xs-12 bouton" >
      <a href="javascript:;" data-hash="#organization"  class="lbh-menu-app btn btn-lg btn-block btn-default">Qui sont les acteurs?</a> 
    </div>
  </div>
</div>
<script type="text/javascript">
   sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
  jQuery(document).ready(function() {
    sectionDyf.<?php echo $kunik ?>Params = {
      "jsonSchema" : {    
        "title" : "Configurer votre section",
        "description" : "Personnaliser votre section",
        "icon" : "fa-cog",

        "properties" : {
          
        },
        beforeBuild : function(){
          uploadObj.set("cms","<?php echo $blockKey ?>");
        },
        save : function () {  
          tplCtx.value = {};
          $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
            tplCtx.value[k] = $("#"+k).val();
            if (k == "parent") {
              tplCtx.value[k] = formData.parent;
            }
          });
          console.log("save tplCtx",tplCtx);

          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
          else {
            dataHelper.path2Value( tplCtx, function(params) {
              dyFObj.commonAfterSave(params,function(){
                toastr.success("Élément bien ajouter");
                $("#ajax-modal").modal('hide');
                urlCtrl.loadByHash(location.hash);
              });
            });
          }

        }
      }
    };

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });

   

  });
</script>