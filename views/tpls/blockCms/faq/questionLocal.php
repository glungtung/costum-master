<?php 
$keyTpl = "questionLocal";
$paramsData = [
	"backgroundQuestion"=>"#8ABF32",
	"backgroundButton" =>"#8ABF32",
	"colorLabelButton" =>"#fff",
	"questionSize" =>"18"
];
if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}  
?>
<style type="text/css">
	.faq-img {
		width:40%;
		height:auto;
		vertical-align:middle;
		float: left;
		padding-right:15px;
	}
	.template_faq {
		background: #edf3fe none repeat scroll 0 0;
	}
	.panel-group<?=$kunik ?> {
		margin-left: 5%;
		padding: 30px;
	}
	.panel-group<?=$kunik ?> #accordion .panel {
		border: medium none;
		border-radius: 0;
		box-shadow: none;
		margin: 0 0 15px 0px;
	}
	.panel-group<?=$kunik ?> #accordion .panel-heading {
		border-radius: 30px;
		padding: 0;
	}
	.panel-group<?=$kunik ?> #accordion .panel-title {
		text-transform: none;
	}
	.panel-group<?=$kunik ?> #accordion .panel-title a {
		background: <?= $paramsData["backgroundQuestion"]?> ;
		border: 1px solid transparent;
		border-radius: 30px;
		color: #fff;
		display: block;
		font-size:<?= $paramsData["questionSize"]?>px;
		font-weight: 600;
		padding: 12px 20px 12px 50px;
		position: relative;
		transition: all 0.3s ease 0s;
	}
	.panel-group<?=$kunik ?> #accordion .panel-title a.collapsed {
		background: #fff none repeat scroll 0 0;
		border: 1px solid #ddd;
		color: #333;
	}
	.panel-group<?=$kunik ?> #accordion .panel-title a::after, #accordion .panel-title a.collapsed::after {
		background: <?= $paramsData["backgroundQuestion"]?>  ;
		border: 1px solid transparent;
		border-radius: 50%;
		box-shadow: 0 3px 10px rgba(0, 0, 0, 0.58);
		color: #fff;
		font-size: 25px;
		height: 55px;
		left: -20px;
		line-height: 55px;
		position: absolute;
		text-align: center;
		top: -5px;
		transition: all 0.3s ease 0s;
		width: 55px;
	}
	.panel-group<?=$kunik ?> #accordion .panel-title a.collapsed::after {
		background: #fff none repeat scroll 0 0;
		border: 1px solid #ddd;
		box-shadow: none;
		color: #333;
		content: "";
	}
	.panel-group<?=$kunik ?> #accordion .panel-body {
		background: transparent none repeat scroll 0 0;
		border-top: medium none;
		padding: 20px 25px 10px 9px;
		position: relative;
	}
	.panel-group<?=$kunik ?> #accordion .panel-body p {
		border-left: 1px dashed #8c8c8c;
		padding-left: 25px;
		font-size: <?= $paramsData["questionSize"]?>px;
	}
	.panel-group<?=$kunik ?> .text-response-faq {
		text-align: justify;
	}

	
	@media (max-width: 767px) {
		.faq-img {
			width:100%;
			height:auto;
			vertical-align:middle;
			float: left;
			padding-right:0px;
			padding-bottom: 15px;
		}
		.faq-container .panel-default {
			padding-right: 0px;
			padding-left: 0px;
		}
	}
	.questionLocal_<?=$kunik?> {
		padding-bottom: 5%; 
		box-shadow: 0px 0px 10px -8px silver;

	}
	.questionLocal_<?=$kunik?> .addFaq{
		background: <?= $paramsData["backgroundButton"]?>;	
		margin-top: 5%;	
		margin-left: 14%;
		color: <?= $paramsData["colorLabelButton"]?>;
		padding: 10px;
		text-align: center;
		font-size: 20px;
	}
	@media (max-width: 978px) {
		.questionLocal_<?=$kunik?> .addFaq{
			margin-top: 5%;
			margin-left: 10%;
			padding: 10px;
			text-align: center;
			font-size: 17px;
		}


	}
</style>
<div class=" questionLocal_<?=$kunik?> ">
	<a href="javascript:;" class="btn btn-xs addFaq" onclick="addList()">
		<i class="fa fa-plus"></i>
		Ajouter une question
	</a>
	<div class="panel-group<?=$kunik ?>">

	</div>


</div>

<script type="text/javascript">	
	getFaqs();
	function addList(){
		var dyfPoi={
			"beforeBuild":{
				"properties" : {
					"name" : {
						"label" : "Votre question",	
						"inputType" : "textarea",
						"placeholder" : "",
						"order" : 1
					},
					"shortDescription" : {
						"inputType" : "textarea",
						"label" : "Réponse",
						"order" : 2
					}
				}
			},
			"onload" : {
				"actions" : {
					"setTitle" : "Ajouter un(e) Question",
					"src" : {
						"infocustom" : "<br/>Remplir le champ"
					},
					"presetValue" : {
						"type" : "faq",
					},
					"hide" : {
						"locationlocation" : 1,
						"breadcrumbcustom" : 1,
						"urlsarray" : 1,
						"parent" : 1,
						"tagstags" : 1,
						"formLocalityformLocality" : 1,
						"descriptiontextarea" : 1,
					}
				}
			}
			
		};
		dyfPoi.afterSave = function(data){
          dyFObj.commonAfterSave(data, function(){
           urlCtrl.loadByHash(location.hash);
          });
        }
		dyFObj.openForm('poi',null, null,null,dyfPoi);
	}
	function getFaqs(){
		var params = {
			source : thisContextSlug,
				type: "faq",
				limit : ""
		}
		ajaxPost(
			null,
			 baseUrl+"/costum/costumgenerique/getpoi",
			params,
			function(data){
				mylog.log("blockcms poi",data);
				var src = "";
				src +='<div class="row">';
				src +='<div class="col-md-12">';
				src += '<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">';				
				if (data.length != 0){
					var i = 0;
					$.each(data.element, function( index, value ) {
						//mylog.log("imageeee", value.profilImageUrl);

						if (typeof value.shortDescription != "undefined"){
							shortDescription = value.shortDescription;
						} else {
							shortDescription ="";
						}
						if (i==0) {
							src += '<div class="panel panel-default col-xs-12 col-sm-12 col-md-6">';
							src += '<div class="panel-heading" role="tab" id="heading'+index+'">';
							src += '<h4 class="panel-title">';
							src += '<a   role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse'+index+'" aria-expanded="false" aria-controls="collapse'+index+'">' +
							value.name;

							src += '</a>';
							src += '</h4>';
							src += '</div>';
							src += '<div id="collapse'+index+'" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading'+index+'">';
							src += '<div class="panel-body">';
							mylog.log("profill", value.profilImageUrl);

							if (value.profilImageUrl != null) {
								src += '<img class="faq-img" src="'+value.profilImageUrl+'">';
							}

							
							src += '<p class="text-response-faq">'+shortDescription;
							src += '</p>';
							src += '</div>';
							src += '</div>';
							src += '</div>';
						} else {
							src += '<div class="panel panel-default col-xs-12 col-sm-12 col-md-6">';
							src += '<div class="panel-heading" role="tab" id="heading'+index+'">';
							src += '<h4 class="panel-title">';
							src += '<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse'+index+'" aria-expanded="false" aria-controls="collapse'+index+'">' +
							value.name;
							src += '</a>';
							src += '</h4>';
							src += '</div>';
							src += '<div id="collapse'+index+'" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading'+index+'">';
							src += '<div class="panel-body">';
							if (value.profilImageUrl != null) {
								src += '<img class="faq-img" src="'+value.profilImageUrl+'">';
							}
							
							src += '<p class="text-response-faq">'+shortDescription;
							src += '</p>';
							src += '</div>';
							src += '</div>';
							src += '</div>';
						}						
						i+=1;
					});
					src += '</div>';
					src += '</div>';
					src += '</div>';					
				}

				else {
					src += "<p class='text-center'>Il n'éxiste aucun question </p>";
				}
				$(".panel-group<?php echo $kunik ?>").html(src);
			} 
		);
	}  
</script>
<script type="text/javascript">
	(function($) {
		'use strict';
		jQuery(document).on('ready', function(){
			$('a.page-scroll').on('click', function(e){
				var anchor = $(this);
				$('src, body').stop().animate({
					scrollTop: $(anchor.attr('href')).offset().top - 50
				}, 1500);
				e.preventDefault();
			});		
		}); 	
	})(jQuery);
</script>
<script type="text/javascript">
	sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	jQuery(document).ready(function() {
		sectionDyf.<?php echo $kunik?>Params = {
			"jsonSchema" : {    
				"title" : "Configurer votre section",
				"description" : "Personnaliser votre section1",
				"icon" : "fa-cog",
				"properties" : {
					"backgroundQuestion" : {
						"label" : "Couleur du background question",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.backgroundQuestion
					},
					"questionSize" : {
						"label" : "Taille de la question",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.questionSize
					},
					"backgroundButton" : {
						"label" : "Couleur du background du bouton",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.backgroundButton
					},					
					"colorLabelButton" : {
						"label" : "Couleur du label du bouton",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.colorLabelButton
					}	
				},
				beforeBuild : function(){
	                uploadObj.set("cms","<?php echo $blockKey ?>");
	            },
				save : function () {  
					tplCtx.value = {};

					$.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
					});

					mylog.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
	                  dataHelper.path2Value( tplCtx, function(params) {
	                    dyFObj.commonAfterSave(params,function(){
	                      toastr.success("Élément bien ajouter");
	                      $("#ajax-modal").modal('hide');
	                      urlCtrl.loadByHash(location.hash);
	                    });
	                  } );
	                }
					
				}
			}
		};
		$(".edit<?php echo $kunik?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";
			dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
		});

	});
</script>