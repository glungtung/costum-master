<?php
$keyTpl = "basicmaps";

$paramsData = [ 
    "title" => "",
    "icon"  =>  "",
    "color" => "",
    "background" => "",
    "defaultcolor" => "white",
    "tags" => "structags"
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}
?>

<style type="text/css">
    #menuRightmapBasic{
        position: absolute !important;
    }
</style>
<div>
    <div style="height: 500px;" class="col-xs-12 mapBackground no-padding" id="mapBasic">
        
    </div>
    
</div>
<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    $(document).ready(function(){

        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {    
                "title" : "Configurer la section bloc map basic",
                "description" : "Personnaliser votre section sur les blocs d'évènement",
                "icon" : "fa-cog",
                "properties" : {
                    "title" : {
                        label : "Titre",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.title
                    },
                    icon : { label : "Icone",
                    inputType : "select",
                    options : {
                        "fa-newspaper-o"    : "Newspapper",
                        "fa-calendar " : "Calendar",
                        "fa-lightbulb-o "  :"Lightbulb",
                    },
                    value : "text" 
                },
                "color" : {
                    label : "Couleur du texte",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.color
                },
                "background" : {
                    label : "Couleur du fond",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.background
                },
                tags : {
                    inputType : "tags",
                    label : "Tags",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.tags
                }
            },
            save : function () {  
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                    tplCtx.value[k] = $("#"+k).val();
                });
                console.log("save tplCtx",tplCtx);
                
                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#ajax-modal").modal('hide');
                        urlCtrl.loadByHash(location.hash);
                    } );
                }
            }
        }
    };
    
    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });


    var mapBasicHome = {};
    var paramsMapBasic  = {};

    initBasicMapView();
});

    function initBasicMapView(){
        basic.initScopeObj();
        paramsMapBasic = {
            zoom : 5,
            container : "mapBasic",
            activePopUp : true,
            tile : "mapbox",
            menuRight : true,
            mapOpt:{
                latLon : ["-21.115141", "55.536384"],
            }
        };

        mapBasicHome = mapObj.init(paramsMapBasic);
        dataSearchMapBasic=searchInterface.constructObjectAndUrl();
        dataSearchMapBasic.searchType = ["organizations","events","projects"];
        dataSearchMapBasic.indexStep=0;
        basic.mapDefault();
        dataSearchMapBasic.private = true;
        dataSearchMapBasic.sourceKey = costum.slug;
        ajaxPost(
            null,
            baseUrl+'/'+moduleId+'/search/globalautocomplete',
            dataSearchMapBasic,
            function(data){ 
            mylog.log(">>> success autocomplete search map !!!! ", data); //mylog.dir(data);
            if(!data){ 
                toastr.error(data.content); 
            } 
            else{ 
                // $('#mapContent').html('');
                mapBasicHome.addElts(data.results, true);
                setTimeout(function(){
                    mapBasicHome.map.panTo([-21.115141,55.536384]);
                    mapBasicHome.map.setZoom(10);
                },2000);
            }
        },
        function(data){
            mylog.log(">>> error autocomplete search"); 
            mylog.dir(data);   
            $("#dropdown_search").html(data.responseText);  
            //signal que le chargement est terminé
            loadingData = false; 
        }
        );  
        
    }

    var basic={
        initScopeObj : function(){
            $(".content-input-scope-basic").html(scopeObj);
            var params = {
                subParams : {
                    cities : {
                        type : ["cities"],
                        country : ["FR"]
                    }
                }
            }
            scopeObj.initVar(params);
            scopeObj.init();
        },
        mapDefault : function(){
            mapCustom.popup.default = function(data){
                mylog.log("mapCO mapCustom.popup.default", data);
                var id = (typeof data.id != "undefined") ? data.id :  data._id.$id ;

                var imgProfil = mapCustom.custom.getThumbProfil(data) ;

                var popup = "";
                popup += "<div class='padding-5' id='popup"+id+"'>";
                popup += "<img src='" + imgProfil + "' height='30' width='30' class='' style='display: inline; vertical-align: middle; border-radius:100%;'>";
                popup += "<span style='margin-left : 5px; font-size:18px'>" + data.name + "</span>";
                
                if (typeof data.email != "undefined" && data.email != null ){
                    popup += "<div id='pop-contacts' class='popup-section'>";
                    popup += "<div class='popup-subtitle'>Contact</div>";
                    popup += "<div class='popup-info-profil'>";
                    popup += "<i class='fa fa-envelope fa_email'></i> <a href='mailto:"+data.email+"'>" + data.email+"</a>";
                    popup += "</div>";
                    popup += "</div>";
                    popup += "</div>";
                }
                popup += "<div class='popup-section'>";
                popup += "<a href='#page.type."+data.type+".id."+id+"' target='_blank' class='item_map_list popup-marker' id='popup"+id+"'>";
                popup += '<div class="btn btn-sm btn-more col-xs-12">';
                popup +=  "Accéder à la page";
                popup += '</div></a>';
                popup += '</div>';
                popup += '</div>';
                console.log(popup);
                return popup;
            };
        }
    }
</script>