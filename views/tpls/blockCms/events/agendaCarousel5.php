<?php 
  $keyTpl ="agendaCarousel1";
  $paramsData = [ 
    "title" => "AGENDA",
    "subTitle" => "Ce que je propose",
  ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) {
              $paramsData[$e] = $blockCms[$e];
      }
    }
  } 
 ?>
<?php
    $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;

    HtmlHelper::registerCssAndScriptsFiles(["/css/blockcms/swiper/swiper-bundle.min.css","/js/blockcms/swiper/swiper-bundle.min.js"], $assetsUrl);
?>
<style>

  .container<?php echo $kunik ?> .btn-edit-delete{
    display: none;
    z-index: 9999;
  }
  .container<?php echo $kunik ?>:hover .btn-edit-delete{
    display: block;
    position: absolute;
    top:50%;
    left: 50%;
    transform: translate(-50%,-50%);
  }
  .title<?php echo $kunik ?>{
    text-transform: none !important;
  }
  .subTitle<?php echo $kunik ?>{
    text-transform: none !important;
    margin-bottom: 50px;
  }
  .container<?php echo $kunik ?> .owl-item img{
    vertical-align: middle;
    height: 100%;
    border: 2px solid grey;
  }
  .container<?php echo $kunik ?> .owl-item .item{
    height: 327px;
  }
  .container<?php echo $kunik ?> .owl-item {
    transition: all .2s ease-in-out;
    height: 500px !important
  }
  .container<?php echo $kunik ?> .owl-item:hover {
    transform: scale(1.1);
  }
 .container<?php echo $kunik ?> .owl-stage-outer {
    padding: 35px 0px 0px 0px;
  }
  .container<?php echo $kunik ?> .event-place{
      color:#ffffff;
      text-transform: none !important;
  }
.container<?php echo $kunik ?> .event-name{
    color: #ffffff;
    text-transform: none !important;
}
.container<?php echo $kunik ?> .letter-orange{
  color:#000000 !important;
  text-transform: capitalize !important;
  font-weight: normal !important;
}
.container<?php echo $kunik ?> .info{
  padding: 0px 46px !important;
}

  .container<?php echo $kunik ?> .swiper-container {
    width: 100%;
    height: 531px;
    margin-left: auto;
    margin-right: auto;
  }

  .container<?php echo $kunik ?> .swiper-slide {
    background-size: cover;
    background-position: center;
  }
  .container<?php echo $kunik ?> .swiper-slide:hover .fa-eye{
    display: inline-block;
  }
  .container<?php echo $kunik ?> .swiper-slide .fa-eye{
    display: none;
    padding: 0 5px 0 5px;
    background: rgb(0,0,0,0.8);
    color: white;
    border-radius: 0 0 16px 0;
    font-size: 24px;
  }

  .container<?php echo $kunik ?> .info-events{
    background-color: rgb(8 8 8 / 60%);
    position: absolute;
    width:100%;
    height: auto;
    bottom: 0;
    left: 50%;
    transform: translate(-50%, 0%);
  }
  .container<?php echo $kunik ?> .info-events .fa-map-marker{
    color: #fff;
  }
  @media (max-width: 450px){
    .container<?php echo $kunik ?> .info-events{
      width: 100%;
    }
  }
  .container<?php echo $kunik ?> .description{
    color: white;
      padding:0 5px 0 5px;
     overflow: hidden;
     text-overflow: ellipsis;
     display: -webkit-box;
     -webkit-line-clamp: 2; /* number of lines to show */
     -webkit-box-orient: vertical;
     font-size: 13px;
     margin-top: 20px;
  }
  .container<?php echo $kunik ?> h3 small,.container<?php echo $kunik ?> .letter-orange{
    color: white !important;
  }
  .container<?php echo $kunik ?> h3{
        padding:0 10px 0 10px !important;
  }
  .container<?php echo $kunik ?> .add-event-caroussel{
    visibility: hidden
  }
  .container<?php echo $kunik ?>:hover .add-event-caroussel{
    visibility: visible;
  }
  .container<?php echo $kunik ?> .img-agenda{
    position: relative;
    width: 100%;
    height: 315px;
    box-shadow: 0 2px 5px 0 #3f4e58,0 2px 10px 0 #3f4e58
  }
</style>
<div class="container<?php echo $kunik ?> col-md-12">
  <h2 class="title title<?php echo $kunik ?> Oswald">
      <?php echo $paramsData["title"] ?>
    </h2>
    <h3 class="subtitle subTitle<?php echo $kunik ?> ReenieBeanie">
      <?php echo $paramsData["subTitle"] ?>
  </h3>
  <?php if(Authorisation::isInterfaceAdmin()){ ?>
    <p class="text-center">
        <button class="btn btn-primary add-event-caroussel">Ajouter un evenement</button>
    </p>
  <?php } ?>
  <div class="swiper-container">
    <div class="swiper-wrapper"></div>
     <!-- Add Pagination -->
    <div class="swiper-pagination"></div>
    <div class="swiper-button-next"></div>
    <div class="swiper-button-prev"></div>
  </div>
</div>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {    
            "title" : "Configurer votre agenda",
            "description" : "Personnaliser votre agenda",
            "icon" : "fa-cog",
            
            "properties" : {
                "title" : {
                    "inputType" : "text",
                    "label" : "Titre",
                    
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.title
                },
                "subTitle" : {
                    "inputType" : "text",
                    "label" : "Sous titre",
                    
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.subTitle
                }          
            },
              beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;

                if(k == "description")
                  tplCtx.value[k] = data.description;
              });
              console.log("save tplCtx",tplCtx);

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
                else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("Élément bien ajouter");
                      $("#ajax-modal").modal('hide');
                      urlCtrl.loadByHash(location.hash);
                    });
                  } );
                }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });

        //add-event*****************
        $(".container<?php echo $kunik ?> .add-event-caroussel").on('click',function(){
          var eventsObj ={
            afterSave : function(data){
              dyFObj.commonAfterSave(data, function(){
                urlCtrl.loadByHash(location.hash);
              });
            }
          };
          dyFObj.openForm('event',null,null,null,eventsObj)
        })
  });

</script>
<script>
$(function ($) {
var html = "";
  getAjax('',baseUrl+'/'+moduleId+'/element/getdatadetail/type/'+thisContextType+
            '/id/'+thisContextId+'/dataName/events',
          function(data){ 
              mylog.log("andrana events",data);
              $.each(data,function(k,v){
                html += 
                      /*`<div class="swiper-slide" style="background-image:url(${v.profilImageUrl})">
                       <a href="${v.profilImageUrl}" data-lightbox="all"><i class="fa fa-eye fa-2x"></i></a>
                          <div class="col-xs-12">
                            <div style="position:relative;width:100%;height:100%;">
                              <img src="${v.profilImageUrl}" alt="">
                            </div>
                          </div>
                          <div class="col-xs-12">
                                  <h4 class="text-center">
                                    <a href="#page.type.${v.collection}.id.${v.id}" class="lbh event-place text-center Oswald other">
                                    ${v.name}
                                    </a>
                                  </h4>
                                  <h5 class="text-center">
                                  <i class="fa fa-map-marker"></i>
                                    <a href="#page.type.${v.collection}.id.${v.id}" class="lbh event-place text-center Oswald other">
                                    ${((typeof v.address.level4Name !="undefined") ? v.address.level4Name : "Addresse non renséigné")}
                                    ${((typeof v.address.streetAddress !="undefined") ? v.address.streetAddress : "Addresse non renséigné")}
                                    </a>
                                  </h5>
                                  ${directory.getDateFormated(v)}
                                  <p class="description">${v.shortDescription != null ? v.shortDescription : ""}</p>
                          </div>
                      </div>`;*/
                      `<div class="swiper-slide">
                       <a href="${v.profilImageUrl}" data-lightbox="all"></a>
                          <div class="row">
                            <div class="col-xs-12">
                                <div class="img-agenda" style="background-image:url(${v.profilImageUrl});background-position:center;background-size:cover;"></div>
                            </div>
                            <div class="col-xs-12 info-agenda">
                                  <h5 class="text-center">
                                  <i class="fa fa-map-marker"></i>
                                    <a href="#page.type.${v.collection}.id.${v.id}" class="lbh event-place text-center Oswald other">
                                    ${((typeof v.address.level4Name !="undefined") ? v.address.level4Name : "Addresse non renséigné")}
                                    ${((typeof v.address.streetAddress !="undefined") ? v.address.streetAddress : "Addresse non renséigné")}
                                    </a>
                                  </h5>                            
                                  <h4 class="text-center">
                                    <a href="#page.type.${v.collection}.id.${v.id}" class="lbh event-place text-center Oswald title-6">
                                    ${v.name}
                                    </a>
                                  </h4>
                                  ${getDateFormated<?php echo $kunik ?>(v)}
                                  <p class="description">${v.shortDescription != null ? v.shortDescription : ""}</p>
                                  <?php if(Authorisation::isInterfaceAdmin()){ ?>
                                    <button class="btn btn-sm btn-danger delete-events-<?= $kunik ?> hiddenPreview" data-id="${v.id}">
                                      Supprimer
                                    </button>
                                    <button class="btn btn-sm btn-success edit-events-<?= $kunik ?> hiddenPreview" data-id="${v.id}">
                                      Modifier
                                    </button>
                                 <?php } ?>
                            </div>
                          </div>
                      </div>`;
                
              });
              $('.container<?php echo $kunik ?> .swiper-wrapper').html(html);
              $('.delete-events-<?= $kunik ?>').on('click',function(){
                  var idEvent = $(this).data('id');
                  bootbox.confirm(trad.areyousuretodelete, function(result){ 
                      if(result){
                        var url = baseUrl+"/"+moduleId+"/element/delete/id/"+idEvent+"/type/events";
                        var param = new Object;
                        ajaxPost(null,url,param,function(data){ 
                                if(data.result){
                                  toastr.success(data.msg);
                                  urlCtrl.loadByHash(location.hash);
                                }else{
                                  toastr.error(data.msg); 
                              }
                        })
                      }
                  })
              });
              $('.edit-events-<?= $kunik ?>').on('click',function(){
                dyFObj.editElement('events',$(this).data('id'));
              });
              var swiper = new Swiper(".container<?php echo $kunik ?> .swiper-container", {
                slidesPerView: 1,
                spaceBetween: 0,
                // init: false,
                pagination: {
                  el: '.swiper-pagination',
                  clickable: true,
                },
                navigation: { nextEl: '.swiper-button-next', prevEl: '.swiper-button-prev' },
                keyboard: {
                  enabled: true,
                },
               /* autoplay: {
                  delay: 2500,
                  disableOnInteraction: false,
                },*/
                breakpoints: {
                  640: {
                    slidesPerView: 1,
                    spaceBetween: 20,
                  },
                  768: {
                    slidesPerView: 2,
                    spaceBetween: 40,
                  },
                  1024: {
                    slidesPerView: 3,
                    spaceBetween: 50,
                  },
                }
              });
              coInterface.bindLBHLinks();
          }
  ,"");

      function getDateFormated<?php echo $kunik ?>(params, onlyStr, allInfos){
        if(typeof params.recurrency != 'undefined' && params.recurrency){
          return initOpeningHours<?php echo $kunik  ?>(params, allInfos);
        }else{
          params.startDateDB = notEmpty(params.startDate) ? params.startDate : null;
          params.startDay = notEmpty(params.startDate) ? moment(params.startDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('DD') : '';
          params.startMonth = notEmpty(params.startDate) ? moment(params.startDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('MM') : '';
          params.startYear = notEmpty(params.startDate) ? moment(params.startDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('YYYY') : '';
          params.startDayNum = notEmpty(params.startDate) ? moment(params.startDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('d') : '';
          params.startTime = notEmpty(params.startDate) ? moment(params.startDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('HH:mm') : '';
            
          params.endDateDB = notEmpty(params.endDate) ? params.endDate: null;
          params.endDay = notEmpty(params.endDate) ? moment(params.endDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('DD') : '';
          params.endMonth = notEmpty(params.endDate) ? moment(params.endDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('MM') : '';
          params.endYear = notEmpty(params.startDate) ? moment(params.endDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('YYYY') : '';
          params.endDayNum = notEmpty(params.startDate) ? moment(params.endDate/*,"YYYY-MM-DD HH:mm"*/).format('d') : '';
          params.endTime = notEmpty(params.endDate) ? moment(params.endDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('HH:mm') : '';
          params.startDayNum = directory.getWeekDayName(params.startDayNum);
          params.endDayNum = directory.getWeekDayName(params.endDayNum);

          params.startMonth = directory.getMonthName(params.startMonth);
          params.endMonth = directory.getMonthName(params.endMonth);
          params.color='orange';
            

          var startLbl = (params.endDay != params.startDay) ? trad['fromdate'] : '';
          var endTime = ( params.endDay == params.startDay && params.endTime != params.startTime) ? ' - ' + params.endTime : '';
          mylog.log('params.allDay', !notEmpty(params.allDay), params.allDay);
           
            
          var str = '';
          var dStart = params.startDay + params.startMonth + params.startYear;
          var dEnd = params.endDay + params.endMonth + params.endYear;
          mylog.log('DATEE', dStart, dEnd);

          if(params.startDate != null){
            if(notNull(onlyStr)){
              str+='<h5 class="" style="padding:0 19px !important">';
              if(params.endDate != null && dStart != dEnd)
                str +=  trad.fromdate;
              str += '<span class="">'+params.startDay+'</span>';
              if(params.endDate == null || dStart == dEnd || (params.startMonth != params.endMonth || params.startYear != params.endYear))
                str += ' <span class="">'+ params.startMonth+'</span>';
              if(params.endDate == null || dStart == dEnd || params.startYear != params.endYear)
                str += ' <span class="">'+ params.startYear+'</span>';
              if(params.endDate != null && dStart != dEnd)
                str += ' <span class="">'+trad.todate+'</span> <span class="">'+params.endDay +'</span> <span class="">'+ params.endMonth +' '+ params.endYear+'</span>';
              str+='</h5>';

              str +=  '<span class="margin-top-5"><b><i class="fa fa-clock-o"></i> '+
                                    params.startTime+endTime+'</b></span>';
            
            }else{ 
              str += '<h5 class="title-5" style="padding:0 19px !important">'+
                        '<span>'+startLbl+' </span>'+
                        params.startDayNum+' '+params.startDay + ' ' + params.startMonth + 
                        ' <span class="">' + params.startYear + '</span>';
              if(params.collection == "events"){
              str +=  ' <span class=""><b><i class="fa fa-clock-o margin-left-10"></i> '+
                                    params.startTime+'</span>';
                  }
              str +=  '</h5>';

            }
          }    
            
          if(params.endDate != null && dStart != dEnd && !notNull(onlyStr)){
            str += '<h5 class="title-5" style="padding:0 19px !important">'+
                            '<span>'+trad['todate']+' </span>'+
                            params.endDayNum+' '+params.endDay + ' ' + params.endMonth + 
                            ' <span class="">' + params.endYear + '</span>';
            if(params.collection == "events"){
            str += ' <span class=""><b><i class="fa fa-clock-o margin-left-10"></i> ' + 
                                      params.endTime+'</span>';
            }
            str +=  '</h5>';
          }   
          return str;
        }
  }

  function initOpeningHours<?php echo $kunik  ?> (params, allInfos) {
    mylog.log('initOpeningHours', params, allInfos);
    var html = '<h5 class="title-5">';
    html += (notNull(allInfos)) ? '<span class=\' ttr-desc-4 bold uppercase" style="padding-left: 15px; padding-bottom: 10px;"\'><i class="fa fa-calendar"></i>&nbsp;'+trad.eachWeeks+'</span>' : '<span class=\' uppercase bold no-padding\'>'+trad.each+' </span>' ;
    mylog.log('initOpeningHours contextData.openingHours', params.openingHours);
    if(notNull(params.openingHours) ){
      var count=0;

      var openHour = '';
      var closesHour = '';
      $.each(params.openingHours, function(i,data){
        mylog.log('initOpeningHours data', data, data.allDay, notNull(data));
        mylog.log('initOpeningHours notNull data', notNull(data), typeof data, data.length);
        if( (typeof data == 'object' && notNull(data) ) || (typeof data == 'string' && data.length > 0) ) {
          var day = '' ;
          var dayNum=(i==6) ? 0 : (i+1);
          if(notNull(allInfos)){
            mylog.log('initOpeningHours data.hours', data.hours);
            day = '<li class="tl-item">';
            day += '<div class="item-title">'+moment().day(dayNum).local().locale(mainLanguage).format('dddd')+'</div>';
            $.each(data.hours, function(i,hours){
              mylog.log('initOpeningHours hours', hours);
              day += '<div class="item-detail">'+hours.opens+' : '+hours.closes+'</div>';
            });
            day += '</li>';
            if( moment().format('dd') == data.dayOfWeek )
              html += day;
            else
              html += day;
          }else{
            if(count > 0) html+=', ';

          
            var color = '';
          
            if( typeof agenda != 'undefined' && agenda != null && 
            (moment(agenda.getStartMoment(agenda.dayCount)).isoWeekday() - 1 ) == i &&
            typeof data.hours[i] != 'undefined' && 
            typeof data.hours[i].opens != 'undefined' && 
            typeof data.hours[i].closes != 'undefined' ) {

              openHour = data.hours[i].opens;
              closesHour = data.hours[i].closes;
            }
  

            html+= '<b style=\'font-variant: small-caps;\' class=\''+color+'\'>'+moment().day(dayNum).local().locale(mainLanguage).format('dddd')+'</b>';
          
          }
          count++;
        }
        
      });

      html +=  '&nbsp<small class=" margin-top-5"><b><i class="fa fa-clock-o"></i> '+openHour+'-'+closesHour+'</b></small>';
    } else 
      html = '<i>'+trad.notSpecified+'</i>'; 

    return html+'</h5>';
  }
});
</script>

