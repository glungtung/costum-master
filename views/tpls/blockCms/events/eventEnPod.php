<?php 
  $keyTpl ="eventEnPod";
  $kunik = $keyTpl.(string)$blockCms["_id"];
  $blockKey = $blockCms["_id"];
   $paramsData = [ 
    
  ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if ( !empty($blockCms[$e]) && isset($blockCms[$e]) ) {
              $paramsData[$e] = $blockCms[$e];
      }
    }
  } 
 ?>
 <style type="text/css">
    .add<?= $kunik?> {
      padding: 5px;
      font-size: 18px;
      background: #24284d;
      margin-left: 12px;

    }
 </style>
 <div class="<?= $kunik?> ">
  <?php if(Authorisation::isInterfaceAdmin()){ ?>
          <a href="javascript:;" class="btn btn-primary btn-xs add<?= $kunik?>">
            <i class="fa fa-plus"></i>  Ajouter un évènement</a>        
      <?php } ?>
    <div id="<?= $kunik?>">
 </div>

   
 </div>
 <script type="text/javascript">
    function allEvents<?= $kunik?>() {
      var str ='';
      getAjax('',baseUrl+'/'+moduleId+'/element/getdatadetail/type/'+costum.contextType+
          '/id/'+costum.contextId+'/dataName/events',
      function(data){ 
        mylog.log(" events",data);
        $.each(data,function(k,v){
          mylog.log(" rrrrrevents",v);
          var tags =[];
          tags+=v.tags;
          if (tags.includes('infos-events')) {
              str += '<div class=\'col-lg-4 col-md-4 col-sm-6 col-xs-12 searchEntityContainer smartgrid-slide-element events contain_'+v.collection+'_'+v._id.$id+' \'>';
              str +=    '<div class=\'searchEntity card-event\' id=\'entity'+v._id.$id+'\'>';
              var dateFormated = directory.getDateFormated(v);
                var countSubEvents = ( v.links && v.links.subEvents ) ? '<br/><i class=\'fa fa-calendar\'></i> '+Object.keys(v.links.subEvents).length+' '+trad['subevent-s']  : '' ; 
                var defaultImgDirectory = '<div class="div-img"><i class="fa fa-picture-o fa-5x"></i></div>';
                if('undefined' != typeof directory.costum && notNull(directory.costum)  
                  && typeof directory.costum.results != 'undefined' 
                  && typeof directory.costum.results[v.collection] != 'undefined' 
                  && typeof directory.costum.results[v.collection].defaultImg != 'undefined')
                    defaultImgDirectory= '<img class=\'img-responsive\' src=\''+assetPath+directory.costum.results[v.collection].defaultImg+'\'/>';      
                if('undefined' != typeof v.profilMediumImageUrl && v.profilMediumImageUrl != '')
                  defaultImgDirectory= '<img class=\'img-responsive\' src=\''+baseUrl+v.profilMediumImageUrl+'\'/>';

                str += '<div class="card-image">'+defaultImgDirectory;
                str +=  '<div class="box-content co-scroll">';
                if(typeof v.type != "undefined"){      
                  var typeEvent = (typeof tradCategory[v.type] != 'undefined') ? tradCategory[v.type] : v.type;
                  str += '<h5 class="text-white lbh-preview-element add2fav"><i class="fa fa-angle-right text-orange"></i>&nbsp; '+
                                  typeEvent;
                  str += '</h5>';
                }
                if (v.address.addressLocality != "Adresse non renseignée") {
                  str += '<div class="col-xs-12"><span class="h4 event-locality margin-left-5"><i class="fa fa-map-marker text-bold"></i> '+v.address.streetAddress+' '+v.address.postalCode+' '+v.address.addressLocality+'</span></div>';
                } 
                var thisTags = '';
                var countTagsInClass=0;
                var countFourTags0nly = 1;
                if(typeof v.tags != 'undefined' && v.tags != null){
                  $.each(v.tags, function(key, value){
                    if(typeof value != 'undefined' && value != '' && value != 'undefined'){
                      var tagTrad = typeof tradCategory[value] != 'undefined' ? tradCategory[value] : value;
                      //thisTags += '<li class="tag" data-tag-value="'+slugify(value, true)+'" data-tag-label="'+tagTrad+'">#' + tagTrad + '</li> ';
                      if(countFourTags0nly < 5 ){
                        thisTags += '<span class="badge bg-transparent btn-tag tag" data-tag-value="'+slugify(value, true)+'" data-tag-label="'+tagTrad+'">#' + tagTrad + '</span> ';
                        countFourTags0nly++;
                      }
                      if(countTagsInClass < 6){
                        v.containerClass += slugify(value, true)+' ';
                        countTagsInClass++;
                      }
                    }
                  });
                  if(countFourTags0nly >4 && (v.tags.length-4)!=0)
                    thisTags += '<span class="badge bg-transparent btn-tag tag">+ '+(v.tags.length-4)+'</span> ';
                    v.tagsHtml = thisTags;
                }
                if(notNull(v.shortDescription))
                  str +=  '<p class="event-short-description  text-white col-xs-12 margin-top-10 margin-bottom-10">'+v.shortDescription+'</p>';
    
                if(typeof v.tagsHtml != "undefined")
                  str +=  '<ul class="tag-list">'+v.tagsHtml+'</ul>';

                              
                  str +=  '</div>';
                  str +=  '</div>';
                  str +=  '<div class="col-xs-12 card-content entityName">';
                  str += '<h4> <a style="color: gray; font-size: 16px" href="#page.type.'+v.collection+'.id.'+v._id.$id+'" class=" letter-turq  lbh-preview-element">'+ v.name + '</a></h4>';
                  str += '</div>';
                  str += '<div class="col-xs-12 card-date">'+dateFormated+countSubEvents;
                  str += '</div>';
                        
                  str += '</div>';
                  str += '</div>';
          }
            
        });
        $('#<?= $kunik?> ').html(str);
        coInterface.bindLBHLinks();
      }
    );
  }
  function addEvents<?= $kunik?>(){
    var dyfEvent={
          "beforeBuild":{
            "properties" : {
              "tags" : {
                "label" : "mot clef ",
                "inputType" : "tags",
                "tagsList" : "motClef",
                "value" :["infos-events"],
                "rules":{
                "required":true
                },
                "order" : 4
              },
            }
          },
          "onload" : {
            "actions" : {
                "setTitle" : "Ajouter un évènement",
                "src" : { 
                  "infocustom" : "Remplir le champ"
                },
              "hide" : {
                  "breadcrumbcustom" : 1,
                    "urlsarrayBtn" : 1,
                    "parentfinder" : 1,
                    "removePropLineBtn" : 1
              }
          }
           }

          }
        dyfEvent.afterSave = function(data){
          dyFObj.commonAfterSave(data, function(){
            mylog.log("dataaaa", data);
            urlCtrl.loadByHash(location.hash);
          });
        }
        dyFObj.openForm('event',null, null,null,dyfEvent);
  }
  jQuery(document).ready(function() {
    $(".add<?= $kunik?>").click(function() {
      addEvents<?= $kunik?>();
     });
    allEvents<?= $kunik?>();
  })
 </script>