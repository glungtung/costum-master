<?php  
$keyTpl = "affiche";
$paramsData=[
	"titleBlock" => "AFFICHE",
    "colorDecor" => "#0dab76",
    "colorlabelButton"  => "#ffffff",
	"colorBorderButton" => "#0dab76",
	"colorButton"       => "#0dab76",
	"widthImg" => "300",
	"heightImg" => "300",
	"radius" => "0",
	"borderImg" => '0',
	"borderImgColor" => "#0dab76"

];

if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (  isset($blockCms[$e]) ) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
}  


?>

<style type="text/css">  
    .box-container-<?=$kunik?>:after,.box-container-<?=$kunik?>:before{content:""}
    .box-container-<?=$kunik?>{overflow:hidden}
    .box-container-<?=$kunik?> .title{letter-spacing:1px}
    /*.box-container .post{font-style:italic}*/
    .mt-30{margin-top:30px}
    .mt-40{margin-top:40px}
    .mb-30{margin-bottom:30px}


    /*********************** Demo - 7 *******************/
    .box-container-<?=$kunik?>{position:relative}
    .box-container-<?=$kunik?>:after,.box-container-<?=$kunik?>:before{width:100%;height:100%;background:rgba(11,33,47,.9);position:absolute;top:0;left:0;opacity:0;transition:all .5s ease 0s}
    .box-container-<?=$kunik?>:after{background:rgba(255,255,255,.3);border:2px solid <?php echo $paramsData["colorDecor"]; ?>;top:0;left:170%;opacity:1;z-index:1;transform:skewX(45deg);transition:all 1s ease 0s}
    .both-box:hover .box-container-<?=$kunik?>:before{opacity:1}
    .both-box:hover .box-container-<?=$kunik?>:after{left:-170%}
    /*.box-container-<?=$kunik?> img{width:auto;height:400px;}*/
    .box-container-<?=$kunik?> img.img-responsive{
        height: 400px;
        float: inherit;
        width: 100%;
        object-fit: cover;
    }
    .box-container-<?=$kunik?> .box-content{width:100%;position:absolute;bottom:-100%;left:0;transition:all .5s ease 0s}
    .both-box:hover .box-container-<?=$kunik?> .box-content{bottom:30%}
    .box-container-<?=$kunik?> .title,.box-footer-<?=$kunik?> .title {display:block;font-weight:500;color:#fff;margin:0 0 10px}
    .box-container-<?=$kunik?> .post{display:block;font-size:15px;font-weight:500;color:#fff;margin:10px}
    .box-container-<?=$kunik?> .icon-<?=$kunik?>{margin:0}
    .box-container-<?=$kunik?> .icon-<?=$kunik?> li{display:inline-block}
    .box-container-<?=$kunik?> .icon-<?=$kunik?> li a{
    	display:block;width:35px;
    	height:35px;
    	line-height:35px;
    	border-radius:50%;
    	background:<?php echo $paramsData["colorButton"]; ?>;
    	font-size:18px;
    	color:<?php echo $paramsData["colorlabelButton"]; ?>;
    	border:1px solid <?php echo $paramsData["colorBorderButton"]; ?>; 
    	margin-right:10px;transition:all .5s ease 0s
    }
    .<?php echo $kunik ?> .btn-add {
        background: <?php echo $paramsData["colorButton"]; ?>;
        color:<?php echo $paramsData["colorlabelButton"]; ?>;
        border:1px solid <?php echo $paramsData["colorBorderButton"]; ?>;
    }
    .box-container-<?=$kunik?> .icon-<?=$kunik?> li a:hover{transform:rotate(360deg)}
    .box-container-<?=$kunik?> .title{text-transform:none}
    .box-container-<?=$kunik?>{box-shadow:0 0 3px rgba(0,0,0,.3)}
    .box-container-<?=$kunik?> .icon-<?=$kunik?>{padding:0;list-style:none}
    .box-container-<?=$kunik?>,.box-container-<?=$kunik?> .icon-<?=$kunik?> li a{text-align:center}
    .box-container-<?=$kunik?> {
    	width: <?php echo $paramsData["widthImg"]; ?>px;
    	height: <?php echo $paramsData["heightImg"]; ?>px;
    	border-radius: <?php echo $paramsData["radius"]; ?>%;
    	margin: auto;
    	border: <?php echo $paramsData["borderImg"]; ?>px solid;
    	border-color: <?php echo $paramsData["borderImgColor"]; ?>;
    }
    @media only screen and (max-width:990px){
        .box{margin-bottom:30px}
    }
    @media (max-width: 767px) {
    	.box-container-<?=$kunik?> {
    		max-width: 300px;
    	}
    }

    .box-footer-<?=$kunik?> {
        padding: 15px 5px 10px 5px;
        background:<?php echo $paramsData["colorDecor"]; ?>;
        text-align: center;
    }

</style>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-10 col-lg-offset-1 <?php echo $kunik ?>">
    <h3 class="title-1"><?php echo $paramsData["titleBlock"]; ?></h3>
    <div class="row mt-30 card_<?=$kunik?>">


	</div>

	<div class="text-center">
		<?php if(Authorisation::isInterfaceAdmin()){ ?>
			<div class="text-center padding-top-20">
				
				<button id="add-social" class="btn btn-primary btn-add ">
					<i class="fa fa-plus-circle"></i> Ajouter Affiche
				</button>	
			</div>
		<?php } ?>
	</div>
</div>

<script type="text/javascript">

    	var dyfPoiAffiche={
    		"beforeBuild":{
		        "removeProp" : {
		            "image": 1,
		        },
		        "properties" : {
	                "shortDescription" : {
	                	"label":"Description courte",
	                	"inputType" : "text"
	                },
	                "category" : {
	                	"label":"Categorie",
	                	"inputType" : "text"
	                }
		           
		        }
		    },
			 "onload" : {
		        "actions" : {
	                "setTitle" : "Ajouter une affiche",
	                "html" : {
	                    "infocustom" : "<br/>Remplir le formulaire"
	                },
	                "presetValue" : {
	                    "type" : "affiche",
	                },
	                "hide" : {
	                    "breadcrumbcustom" : 1,
	                    "parentfinder" : 1,
	                }
	            }
		    }
		};
		dyfPoiAffiche.afterSave = function(data){
			dyFObj.commonAfterSave(data,function(){
				mylog.log("data", data);
				 //location.hash = "#documentation."+data.id;
				 urlCtrl.loadByHash(location.hash);

			});
		};
		
   
	sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	jQuery(document).ready(function() {

		$("#add-social").click(function(){
			dyFObj.openForm('poi',null, null,null,dyfPoiAffiche);
		})
		

			sectionDyf.<?php echo $kunik?>Params = {
		      "jsonSchema" : {    
		        "title" : "Configurer votre bloc",
		        "description" : "Personnaliser votre bloc",
		        "icon" : "fa-cog",
		        "properties" : { 
					"titleBlock" : {
                        label : "titre du bloc",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.titleBlock
                    },    
                    "colorDecor" : {
                        label : "Couleur du pied de section",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.colorDecor
                    },
                    "widthImg"  : {
                        label : "Largeur de l'image (en px)",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.widthImg
                    },
					"heightImg" : {
                        label : "Hauteur de l'image (en px)",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.heightImg
                    },
					"radius" : {
                        label : "Rayon du bordure de l'image (en %)",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.radius
                    },
                    "borderImgColor" :{
			            label : "Couleur du bordur de l'image",
			            inputType : "colorpicker",
			            values :  sectionDyf.<?php echo $kunik ?>ParamsData.borderImgColor
			         },
			         "borderImg" :{
			            label : "Epaisseur du bordur de l'image",
			            values :  sectionDyf.<?php echo $kunik ?>ParamsData.borderImg
			         },
                    "colorlabelButton":{
			            label : "Couleur du labelle de bouton",
			            inputType : "colorpicker",
			            values :  sectionDyf.<?php echo $kunik ?>ParamsData.colorlabelButton
			         },
			         "colorButton":{
			            label : "Couleur du bouton",
			            inputType : "colorpicker",
			            values :  sectionDyf.<?php echo $kunik ?>ParamsData.colorButton
			         },
			         "colorBorderButton":{
			            label : "Couleur du bordure de bouton",
			            inputType : "colorpicker",
			            values :  sectionDyf.<?php echo $kunik ?>ParamsData.colorBorderButton
			         }
				},
            	 beforeBuild : function(){
		          uploadObj.set("cms","<?php echo $blockKey ?>");
		        },
		        save : function () {  
		          tplCtx.value = {};

		          $.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
		            tplCtx.value[k] = $("#"+k).val();
		          });

		          mylog.log("save tplCtx",tplCtx);

		          if(typeof tplCtx.value == "undefined")
		            toastr.error('value cannot be empty!');
		          else {
		              dataHelper.path2Value( tplCtx, function(params) {
		                dyFObj.commonAfterSave(params,function(){
		                  toastr.success("Élément bien ajouter");
		                  $("#ajax-modal").modal('hide');
		                  urlCtrl.loadByHash(location.hash);
		                });
		              } );
		          }
		        }
		      }
		    };
		mylog.log("paramsData",sectionDyf);
		$(".edit<?php echo $kunik?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";
			dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
		});


		/**************************** list**************************/  
		var isInterfaceAdmin = false
		<?php 
			if(Authorisation::isInterfaceAdmin()){ ?>
				isInterfaceAdmin = true
		 <?php } ?>
		 
        getAffiche();
        
        function getAffiche(){  
        	var params = {
	          "source" : thisContextSlug,
	          "type"  : "affiche"
	        };
	        ajaxPost(
	          null,
	          baseUrl+'/'+moduleId+'/element/getdatadetail/type/'+thisContextType+'/id/'+thisContextId+'/dataName/poi/sort/1',
	          params,
	          function(data){
	              console.log("blockcms poi",data);
	              var html = "";
	              $.each(data, function( index, value ) {
	                html += 

	                	'<div class="col-md-4 col-sm-6 both-box mb-30">'+
		                    '<div class="box-container-<?=$kunik?>">';
		                
		                         if (typeof value.profilMediumImageUrl && value.profilMediumImageUrl != null) 
		             html +=         '<img class="img-responsive" src="'+value.profilMediumImageUrl+'" alt="card image">';
		                         else
		             html +=         '<img class="img-responsive" src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/blockCmsImg/Avatar.png"> ';
		                        

		             html +=    '<div class="box-content">';
		             			if (typeof value.category && value.category != null)
		             html +=        '<h3 class="title title-3">'+value.category+'</h3>';
		                        if (typeof value.shortDescription && value.shortDescription != null)
		            html +=          '<div class="post title-4">'+value.shortDescription+'</div>';
		                            if(isInterfaceAdmin == true)
		            html +=             '<ul class="icon-<?=$kunik?>">'+
		                                    '<li>'+
		                                        '<a href="javascript:;" class="edit" data-type="'+value.type+'" data-id="'+value._id.$id+'"><i class="fa fa-edit"></i></a>'+
		                                    '</li>'+
		                                    '<li>'+
		                                        '<a href="javascript:;" class="delete" data-type="'+value.type+'" data-id="'+value._id.$id+'"><i class="fa fa-trash"></i></a>'+
		                                    '</li>'+
		                                '</ul>';
		             html +=                
		                        '</div>'+
		                    '</div>'+
		                    '<div class="box-footer-<?=$kunik?>">'+
		                        '<a href="#page.type.poi.id.'+value._id.$id+'" class="lbh-preview-element">'+
		                            '<h3 class="title title-2">'+value.name+'</h3>'+
		                        '</a>'+
		                    '</div>'+
		                '</div>';

	              });
	              $(".card_<?=$kunik?>").html(html);
	              $(".card_<?=$kunik?> .edit").off().on('click',function(){
		            	var id = $(this).data("id");
				     	var type = $(this).data("type");
		              dyFObj.editElement('poi',id,type,dyfPoiAffiche);
		            });
	              $(".card_<?=$kunik?> .delete").off().on("click",function () {
		                $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
		                var btnClick = $(this);
		                var id = $(this).data("id");
		                var type = "poi";
		                var urlToSend = baseUrl+"/"+moduleId+"/element/delete/type/"+type+"/id/"+id;
		                
		                bootbox.confirm("voulez vous vraiment supprimer cette actualité !!",
		                function(result) 
		                {
		              if (!result) {
		                btnClick.empty().html('<i class="fa fa-trash"></i>');
		                return;
		              } else {
		                ajaxPost(
		                      null,
		                      urlToSend,
		                      null,
		                      function(data){ 
		                          if ( data && data.result ) {
		                          toastr.success("élément effacé");
		                          $("#"+type+id).remove();
		                          getAffiche();
		                        } else {
		                           toastr.error("something went wrong!! please try again.");
		                        }
		                      }
		                  );
		              }
		            });
		          });
	              $('.post-module_<?=$kunik?>').hover(function() {
					$(this).find('.description').stop().animate({
					      height: "toggle",
					      opacity: "toggle"
					    }, 300);
					});
	           }
	        );
	    }
       
        
	});


</script>