<?php 
  $keyTpl ="music_album";
  $paramsData = [ 
    "title" => "ALBUM",
    "musicBorderSwitcher" => "#F0AD16",
    "musicColor" => "#FFFFFF",
    "musicActive" => "#F0AD16",
    "musicHover" => "#F0AD16",

    "downloadBtnIconColor" => "#FFFFFF",
    "downloadBtnBgColor" => "#000000",
    "items" => array()
  ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) {
              $paramsData[$e] = $blockCms[$e];
      }
    }
  }
  $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;
  HtmlHelper::registerCssAndScriptsFiles([
  	"/css/blockcms/soundcloud/colors.css",
  	"/css/blockcms/soundcloud/structure.css",

  	"/js/blockcms/soundcloud/soundcloud.player.api.js",
  	"/js/blockcms/soundcloud/sc-player.js"],
  	 $assetsUrl);

    $lastKey = 0;

  $itemHeaderSize = array();
  if(isset($paramsData["items"]) && is_array($paramsData["items"])){
    $itemHeaderSize = array_keys($paramsData["items"]);
    $lastKey = end($itemHeaderSize);
  }
  $initPhotos = Document::getListDocumentsWhere(
    array(
      "id"=> (string)$blockCms["_id"],
      "type"=>'cms',
    ), "image"
  );
  //var_dump($initbackgroundPhoto);
    krsort($paramsData["items"]);
?>
<style>

	/* controls */
	.sc-player .sc-controls a {
	  color: transparent;
	  background: url('<?php  echo $assetsUrl?>/images/blockCmsImg/defaultImg/play.png') no-repeat top /26px;
	}

	.sc-player .sc-controls a:hover {
	  background: url('<?php  echo $assetsUrl?>/images/blockCmsImg/defaultImg/play-hover.png') no-repeat top /26px;
	}

	.sc-player .sc-controls a.sc-pause {
	  background: url('<?php  echo $assetsUrl?>/images/blockCmsImg/defaultImg/play.png') no-repeat top /26px;
	}

	.sc-player .sc-controls a.sc-pause:hover{
	  background: url('<?php  echo $assetsUrl?>/images/blockCmsImg/defaultImg/play-hover.png') no-repeat top /26px;
	}

	.sc-player.playing .sc-controls a.sc-pause{
	  background: url('<?php  echo $assetsUrl?>/images/blockCmsImg/defaultImg/pause.png') no-repeat top /26px;
	}

	.sc-player.playing .sc-controls a.sc-pause:hover{
	  background: url('<?php  echo $assetsUrl?>/images/blockCmsImg/defaultImg/pause-hover.png') no-repeat top /26px;
	}  

	.sc-scrubber .sc-time-indicators{
	  background: #fff;
	  color: #000;
	  -moz-border-radius: 4px;
	  -webkit-border-radius: 4px;
	  padding: 2px;
	  font-size: 0.4em;
	  font-weight: normal;
	  line-height: 1em;
	}
	.sc-player.playing ol.sc-trackslist li.active a {
	  color: <?= $paramsData["musicActive"] ?>;
	}
	.sc-player a {
	  text-decoration: none;
	  color: <?= $paramsData["musicColor"] ?>;
	}
	.sc-player:hover a {
	  color: <?= $paramsData["musicHover"] ?>;
	}
	.sc-download-track {
	    float: right;
	    margin-top: 8px;
	    position: relative;
	    z-index: 111;
	    color:<?= $paramsData["downloadBtnIconColor"] ?> !important;
	    background-color: <?= $paramsData["downloadBtnBgColor"] ?> !important;
	}
	.sc-download-track:hover {
	    float: right;
	    margin-top: 8px;
	    position: relative;
	    z-index: 111;
	    color:<?= $paramsData["downloadBtnBgColor"] ?> !important;
	    background-color: <?= $paramsData["downloadBtnIconColor"] ?> !important;
	}
	.ablum-container-<?= $kunik ?>{
		display: flex;
		flex-direction: row;
		flex-wrap: wrap;
		width: 100%;
		height: auto;
		margin-top: 80px;
	}
	.switch-album-<?= $kunik ?>{
		display: flex;
		flex-direction: row;
		flex-wrap: wrap;
		width: 100%;
		height: auto;
		justify-content: center;
		align-items: center;
	}
	.switch-album-<?= $kunik ?>>div{
	    height: 100%;
	    position: relative;
	    display: flex;
	    flex-direction: column;
	    justify-content: center;
	    align-items: center;
	    cursor: pointer;
	    border-radius: 5px;
		box-shadow: 0 2px 5px 0 rgb(63 78 88), 0 2px 10px 0 rgb(63 78 88);
		margin: 4px; 
	}
	.switch-album-<?= $kunik ?>>div:hover{
		border:2px solid <?= $paramsData["musicBorderSwitcher"] ?>;
		box-shadow: 0 2px 5px 0 rgb(63 78 88), 0 2px 10px 0 rgb(63 78 88);
	}
	.switch-album-<?= $kunik ?>>div>img{
		height: 155px;
		object-fit: contain;
    	width: 155px;
	}
	.img-container-<?= $kunik ?>{
		text-align: -webkit-center !important;
	}
	.img-container-<?= $kunik ?> img{
		height: 500px;
		width: 100%;
		object-fit: contain;
		object-position: center;
		padding-left:7%;
	}
	.table-container-<?= $kunik ?>{
		padding: 0 20px 0 20px;
	}
	.block-container-<?= $kunik ?> .table>tbody>tr{
		font-size: 23px;
    	font-family: 'Montserrat';
	}
	.block-container-<?= $kunik ?> .table>tbody>tr>td{
		padding: 5px !important;
		background: #000 !important;
    	border-top-color: #000 !important;
	}
	@media (max-width:500px){
		.img-container-<?= $kunik ?> img{
			height: 100%;
		    width: 100%;
		    object-fit: contain;
		    object-position: center;
			padding: 0 5px;
		}
	}
	@media (max-width:765px){
		.ablum-container-<?= $kunik ?>{
			flex-direction: column;
		}
	}
</style>
<h1 class="title"><?= $paramsData["title"] ?></h1>
<?php if(Authorisation::isInterfaceAdmin()){ ?>
	<p class="text-center">
		<button class="btn btn-success add-audio-<?=$kunik ?> hiddenPreview">Créer album</button>
	</p>
<?php } ?>
<?php 
	$albumImgSwitch = "";
	$albumImgSwitchImgId ="";
?>
<div class="switch-album-<?= $kunik ?>">
	<?php $pj = 0; foreach ($paramsData["items"] as $ksw => $vsw) { ?>
	<?php     foreach ($initPhotos as $kis => $vis) {
      if($vis["subKey"] == "albumImgSwitch".@$ksw ){
        $albumImgSwitch = $vis["imagePath"];
        $albumImgSwitchImgId =  (string)$vis["_id"];
      }
    } ?>
		<div class="album-audio-menu" data-id="<?= $ksw ?>">
			<h6 class="title-6"><?= @$vsw["album"] ?></h6>
			<img src="<?= !empty($albumImgSwitch) ? $albumImgSwitch : $assetsUrl."/images/blockCmsImg/defaultImg/Optimized-CD-117mm.png" ?>" loading="lazy" alt="">
			<?php if(Authorisation::isInterfaceAdmin()){ ?>
				<div class="btn-group hiddenPreview" style="width:100%">
				  <a href="javascript:;" class="btn btn-sm btn-primary edit-album-<?= $kunik ?>" data-content='<?= json_encode(@$vsw); ?>' data-id="<?= $ksw ?>"  style="width:50%">
				  	<i class="fa fa-edit"></i>
				  </a>
				  <a href="javascript:;" class="btn btn-sm btn-danger delete-album-<?= $kunik ?>" data-id="<?= @$ksw ?>" style="width:50%">
				  	<i class="fa fa-trash"></i>
				  </a>
				</div>
			<?php } ?>
		</div>
	<?php $pj++; } ?>
</div>

<?php $pi = 0; foreach ($paramsData["items"] as $kpl => $vpl) { ?>
	<?php     foreach ($initPhotos as $kij => $vij) {
      if($vij["subKey"] == "albumImgJacket".$kpl ){
        $albumImgJacket = $vij["imagePath"];
        $albumImgJacketImgId =  (string)$vij["_id"];
      }
    } ?>
	<div class="ablum-audio-container ablum-container-<?= $kunik ?>  audio-<?= $kpl ?>" style="<?= ($pi != 0) ? "display:none" : ""; ?>">
		<div class="img-container-<?= $kunik ?> bg-grey text-center" style="flex:50%">
			<!-- <img src="<?= $assetsUrl ?>/images/blockCmsImg/defaultImg/Optimized-CD-117mm.png" alt=""> -->
			<img src="<?= !empty($albumImgJacket) ? $albumImgJacket :  $assetsUrl."/images/blockCmsImg/defaultImg/Optimized-CD-117mm.png"  ?>" loading="lazy" alt="">
		</div>
		<div class="table-container-<?= $kunik ?>" style="flex:50%">
		<?php foreach ($vpl["urls"] as $kurl => $vurl) { ?>
			<a download="<?= @$vurl["downloadAt"] ?>" href="<?= @$vurl["audio"] ?>" class="sc-player">My dub track</a> 
		<?php } ?>
		</div>
	</div>
<?php $pi++; } ?>


	


<script>
	  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	  $(function(){
	  	var initPhotos<?= $kunik ?> = <?= json_encode($initPhotos); ?>;
	      sectionDyf.<?php echo $kunik ?>Params = {
	        "jsonSchema" : {    
	          "title" : "Configurer votre section",
	          "description" : "Personnaliser votre gallerie",
	          "icon" : "fa-cog",
	          
	          "properties" : {
	              title:{
	              	inputType:"text",
	              	label:"Titre",
	              	value: "<?= $paramsData["title"] ?>"
	              },
	              musicBorderSwitcher:{
	              	inputType:"colorpicker",
	              	label:"Bordure de la navigation",
	              	value: <?= json_encode($paramsData["musicBorderSwitcher"]) ?>
	              },
	              musicColor:{
	              	inputType:"colorpicker",
	              	label:"Couleur des pistes musicales",
	              	value: <?= json_encode($paramsData["musicColor"]) ?>
	              },
	              musicHover:{
	              	inputType:"colorpicker",
	              	label:"Couleur d'u piste musicale au survol de souris",
	              	value: <?= json_encode($paramsData["musicHover"]) ?>
	              },
	              musicActive:{
	              	inputType:"colorpicker",
	              	label:"Couleur d'un piste musicale en lecture",
	              	value: <?= json_encode($paramsData["musicActive"]) ?>
	              },
	              downloadBtnIconColor:{
	              	inputType:"colorpicker",
	              	label:"Couleur de l'icon du bouton téléchargement",
	              	value: <?= json_encode($paramsData["downloadBtnIconColor"]) ?>
	              },
	              downloadBtnBgColor:{
	              	inputType:"colorpicker",
	              	label:"Couleur de fond du bouton téléchargement",
	              	value: <?= json_encode($paramsData["downloadBtnBgColor"]) ?>
	              }
	          },
	          beforeBuild : function(){
	              uploadObj.set("cms","<?php echo (string)$blockCms["_id"] ?>");
	          },
	          save : function (data) {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
	              tplCtx.value[k] = $("#"+k).val();
	              if (k == "parent")
	                tplCtx.value[k] = formData.parent;
	            });
	            console.log("save tplCtx",tplCtx);

	            if(typeof tplCtx.value == "undefined")
	              toastr.error('value cannot be empty!');
	              else {
	                dataHelper.path2Value( tplCtx, function(params) {
	                  dyFObj.commonAfterSave(params,function(){
	                    toastr.success("Élément bien ajouter");
	                    $("#ajax-modal").modal('hide');
	                    urlCtrl.loadByHash(location.hash);
	                  });
	                } );
	              }

	          }
	        }
	      };


	      $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
	        tplCtx.id = $(this).data("id");
	        tplCtx.collection = $(this).data("collection");
	        tplCtx.path = "allToRoot";
	        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
	        alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"music",3,12,null,null,"Musique","blue","");
	        alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"downloadBtn",6,6,null,null,"Bouton téléchargement","blue","");
	      });

	      $(".sc-player").click(function(){
	      	setTimeout(function(){
	      		$('.sc-player.playing').find('.sc-pause').removeClass('hidden');
	      	},300)
	      })
	      $(".album-audio-menu").click(function(){
	      	var id = $(this).data("id");
	      	$(".ablum-audio-container").fadeOut(1000);
	      	$(".audio-"+id).fadeIn(1000);
	      	$('.sc-player.playing a.sc-pause').click();
	      	if($('.audio-'+id).length)
		      	$('html,body').animate(
	                { scrollTop: $('.audio-'+id).offset().top-70},800
	            );
	      })

		  $('.add-audio-<?=$kunik ?>').off().on('click',function(){
	         <?= $kunik ?>afficheDynForm();
	      })
	      $('.edit-album-<?= $kunik ?>').off().on('click',function(e){
	      	e.stopPropagation();
	        var id = $(this).data('id');
	        var content= $(this).data('content');
	        var albumImgSwitch = [];
	        var albumImgJacket = [];
	        $.each(initPhotos<?= $kunik ?>,function(k,v){
	        	if(v.subKey == "albumImgSwitch"+id)
	            	albumImgSwitch.push(v);
	        	if(v.subKey == "albumImgJacket"+id)
	            	albumImgJacket.push(v);
	        });
	         <?= $kunik ?>afficheDynForm(id,content,albumImgSwitch,albumImgJacket);
	      })
	      $('.delete-album-<?= $kunik ?>').off().on('click',function(){
	          tplCtx.id = "<?= (string)$blockCms["_id"] ?>";
	          tplCtx.collection = "cms";
	          tplCtx.path =  "items."+$(this).data('id') ;
	          tplCtx.value = null;
	          bootbox.confirm(trad["areyousuretodelete"], function(result){ 
	              if(result)
	                dataHelper.path2Value( tplCtx, function(params) {
	                    toastr.success("Suppression réussie");
	                    urlCtrl.loadByHash(location.hash);
	                });
	          });
	      });
	      function <?= $kunik ?>afficheDynForm(id=null,content=null,albumImgSwitch=null,albumImgJacket=null){
	        var headerItem<?= $kunik ?> = {
	            "jsonSchema" : {    
	              "title" : notNull(id) ? "Modifier les urls" : "Ajouter des urls",
	              "description" : "Exemple : https://soundcloud.com/brunomars/24k-magic",
	              "icon" : "fa-cog",
	              "properties" : {
	                  "album" : {
	                    "inputType" : "text",
	                    "label" : "Nom de l'album"    
	                  },
	                  "albumImgSwitch" : {
	                    "inputType" : "uploader",
	                    "label" : "Petite image",
	                    "domElement" : "albumImgSwitch",
	                    "docType": "image",
	                    "contentKey" : "slider",
	                    "itemLimit" : 1,
	                    "filetypes": ["jpeg", "jpg", "gif", "png"],
	                    "showUploadBtn": false,
	                    "endPoint" : notNull(id) ? "/subKey/albumImgSwitch"+id : "/subKey/albumImgSwitch<?= $lastKey+1 ?>"
	                  },
	                  "albumImgJacket" : {
	                    "inputType" : "uploader",
	                    "label" : "Grande image",
	                    "domElement" : "albumImgJacket",
	                    "docType": "image",
	                    "contentKey" : "slider",
	                    "itemLimit" : 1,
	                    "filetypes": ["jpeg", "jpg", "gif", "png"],
	                    "showUploadBtn": false,
	                    "endPoint" : notNull(id) ? "/subKey/albumImgJacket"+id : "/subKey/albumImgJacket<?= $lastKey+1 ?>"
	                  },
	                  "urls" : {
	                    "label" : "URLS",
	                    "inputType" : "lists",
	                    "entries":{
	                        "key":{
	                            "type":"hidden",
	                            "class":""
	                        },
	                        "audio":{
	                            "label":"Url de l'audio",
	                            "type":"text",
	                            "placeholder" : "https://soundcloud.com/brunomars/24k-magic",
	                            "class":"col-md-5 col-sm-5 col-xs-10"
	                        },
	                        "downloadAt":{
	                            "label":"Url de téléchargement",
	                            "type":"text",
	                            "class":"col-md-5 col-sm-5 col-xs-10"
	                        }
	                    }
	                  }
	              },
	              beforeBuild : function(){
	                  uploadObj.set("cms","<?php echo (string)$blockCms["_id"] ?>");
	              },
	              save : function (data) {  
	                tplCtx.id = "<?= (string)$blockCms["_id"] ?>";
	                tplCtx.collection = "cms";
	                tplCtx.path =  notNull(id) ? "items."+id : "items.<?= $lastKey+1 ?>";
	                tplCtx.value = {};
	                $.each(headerItem<?= $kunik ?>.jsonSchema.properties , function(k,val) { 
	                  tplCtx.value[k] = $("#"+k).val();
		                if (k == "parent")
		                    tplCtx.value[k] = formData.parent;
		                if(k == "urls")
		                  	 tplCtx.value[k] = data.urls;
	                });
	                console.log("save tplCtx",tplCtx);

	                if(typeof tplCtx.value == "undefined")
	                  toastr.error('value cannot be empty!');
	                  else {
	                    dataHelper.path2Value( tplCtx, function(params) {
	                      dyFObj.commonAfterSave(params,function(){
	                        toastr.success("Élément bien ajouter");
	                        $("#ajax-modal").modal('hide');
	                        urlCtrl.loadByHash(location.hash);
	                      });
	                    } );
	                  }

	              }
	            }
	          };
	          mylog.log("audio",headerItem<?= $kunik ?>);
	          if(notNull(albumImgSwitch)){
	              headerItem<?= $kunik ?>.jsonSchema.properties.albumImgSwitch["initList"] = albumImgSwitch;
	          }
	          if(notNull(albumImgJacket)){
	              headerItem<?= $kunik ?>.jsonSchema.properties.albumImgJacket["initList"] = albumImgJacket;
	          }
	          if(notNull(id) && notNull(content))
	          	dyFObj.openForm(headerItem<?= $kunik ?>,null,content);
	          else
	          	dyFObj.openForm(headerItem<?= $kunik ?>);
	      }
});
</script>