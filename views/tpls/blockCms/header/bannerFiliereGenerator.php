<?php 
$keyTpl = "bannerFiliereGenerator";

$paramsData = [
	"titre" => "Lorem ipsum dolor",
	"content" => "Une costum filière c'est quoi ? Lorem ipsum dolor sit amet, consectetur
				adipiscing elit. Nullam a ultricies metus. Sed nec molestie eros. Sed viverra
				velit venenatis fermentum luctus.",
	"btnGenerateText" => "Commencer Maintenant"
];

if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
} 

?>

	<style type="text/css">
		.banner{
			position:relative;
			height: 70vh;
			overflow: hidden;
		}
		.banner:before{
			content: '';
			position:absolute;
			background: #111111 !important;
			z-index: 1;
			top: 0;
			left: 0;
			right: 0;
			width: 100%;
			height: 100%;
			opacity: 0.8;

		}
		.banner img{
			width:100%;
			height: 100%;
		}
		.banner .content{
			position: absolute; 
			left:0;
			right: 0;
			top:45%;
			text-align:center; 
			transform:translateY(-50%);
			z-index: 2;
		}

		.text-green{
			color: #AFCB21;
			font-weight: bolder;
		}

		#addFiliere, .btn-get-started{
			background: #AFCB21;
			color: #2C3E50;
			font-size: 14pt;
			padding: 10px 15px 15px 15px;
			font-weight: bolder !important;
			border-radius: 40px !important;
			margin-top: 10px;
		}

	</style>
	<div  class="banner"> 
		<div class="content container">

			<h1 class="text-green"><?php echo $paramsData["titre"] ?></h1>
			<br>
			<div class="description lead text-white container col-md-8 markdown col-md-offset-2">
				<?php echo "\n ".$paramsData["content"] ?>
			</div>
			<br>
			<!--button id="addFiliere" class='btn' data-id="<?php echo $this->costum['contextId'] ?>" data-collection="<?php echo $this->costum['contextType'] ?>"> 
				Commencer Maintenant &nbsp; <i class="fa fa-arrow-right"></i>
			</button-->
			<button id="addFiliere" class='btn btn-get-started'> 
				<?php echo $paramsData["btnGenerateText"] ?>&nbsp; <i class="fa fa-arrow-right"></i>
			</button>
		</div>

		<div>
			<?php  ?>
		</div>
	</div>

	<script type="text/javascript">
		var cocityParam = window.location.href.split("#")[1];
		var cocityParamArray;

		if(cocityParam){
			cocityParamArray = cocityParam.split(".")
		}

		var cocity = {};
		var cocityId = "";
		var ville = "";
		var thematic = "";

		
		if(typeof cocityParam != "undefined" && typeof cocityParamArray != "undefined" && cocityParamArray.length <= 3){
			
			if(cocityParamArray[0]){
				cocityId = cocityParamArray[0]
			}

			if(cocityParamArray[1]){
				ville = cocityParamArray[1]
			}

			if(cocityParamArray[2]){
				thematic = cocityParamArray[2];
				ajaxPost(
					null, 
					"/co2/organization/getbyid?id="+cocityParamArray[0], 
					{},
					function(data){
						cocity = data;
					},
	                function(xhr,textStatus,errorThrown,data){
	                    toastr.error("Une erreur s'est produite, veuillez signaler ce problème à notre administrateur");
	                }
	            );
				$("#addFiliere").text("Générer Filière "+ville+""+thematic);
			}
		}

	    jQuery(document).ready(function() {
	    	sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

	    	sectionDyf.<?php echo $kunik?>Params = {
				"jsonSchema" : {    
					"title" : "Configurer la section",
					"description" : "Personnaliser votre section",
					"icon" : "fa-cog",
					"properties" : {
						"titre" : {
							"label" : "Titre",
							"inputType":"text",
							values :  sectionDyf.<?php echo $kunik?>ParamsData.titre
						},
						"content":{
							inputType: "textarea",
							markdown: true,
							label : "Introduction :",
							values :  sectionDyf.<?php echo $kunik?>ParamsData.content
						},
						"btnGenerateText" : {
							"label" : "Texte du bouton :",
							"inputType":"text",
							values :  sectionDyf.<?php echo $kunik?>ParamsData.btnGenerateText
						},
						"colorBtn":{
							label : "Couleur :",
							inputType : "colorpicker",
							values :  sectionDyf.<?php echo $kunik?>ParamsData.colorBtn
						}
					},				
					beforeBuild : function(){
						uploadObj.set("cms","<?php echo $blockKey ?>");
					},
					save : function () {  
						tplCtx.value = {};

						$.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
							tplCtx.value[k] = $("#"+k).val();
						});

						mylog.log("save tplCtx",tplCtx);

						if(typeof tplCtx.value == "undefined")
							toastr.error('value cannot be empty!');
						else {
			                  dataHelper.path2Value( tplCtx, function(params) {
			                    dyFObj.commonAfterSave(params,function(){
			                      toastr.success("Élément bien ajouter");
			                      $("#ajax-modal").modal('hide');
			                      urlCtrl.loadByHash(location.hash);
			                    });
			                  } );
						}
						
					}
				}
			};

			$(".edit<?php echo $kunik?>Params").off().on("click",function() {  
				tplCtx.id = $(this).data("id");
				tplCtx.collection = $(this).data("collection");
				tplCtx.path = "allToRoot";
				dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
			});

	    	$("#addFiliere").off().on('click', 
	    		function() {
	    			let defaultName = "";
	    			let defaultTags = [];
	    			let defaultLocation = {};

	    			defaultName = ville+" "+thematic;

	    			if(cocity.filiere && cocity.filiere[thematic.toLowerCase()] && cocity.filiere[thematic.toLowerCase()]["tags"]){
	    				defaultTags = cocity.filiere[thematic.toLowerCase()]["tags"];
	    			}

	    			if(cocity.address && cocity.geo && cocity.geoPosition){
	    				defaultLocation = {
	    					"address": cocity.address,
	    					"geo":cocity.geo,
	    					"geoPosition": cocity.geoPosition
	    				}

	    				dyFInputs.formLocality("Ndao ary soloana", "Eto ilay Izy", null, defaultLocation);
	    			}

	    			dyFObj.openForm(
		    			'organization', 
		    			null, 
		    			{
		    				name: defaultName,
		    				tags: defaultTags,
		    				formLocality: dyFInputs.formLocality(defaultLocation)
		    			}, 
		    			null, 
		    			{
							afterSave : function(orga){
								var data = {"cocity":cocityId, "ville":ville, "thematic":thematic, ...orga};
								ajaxPost(
									null,
									'<?= Yii::app()->baseUrl; ?>/costum/filiere/generate',
									data,
									function(data){
										window.location = data.url;
									},
									function(error){
										toastr.error("Une erreur s'est produite, veuillez réessayer et si le problème persiste, contecter l'administrateur")
									},
									"json"
								)
							}
						}
					)
	    		}
			);
	    });
    
	</script>
