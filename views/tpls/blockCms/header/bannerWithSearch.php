<?php 
  $keyTpl ="bannerWithSearch";
  $paramsData = [ 
    "titre" => " Cocity présentation",
    "description"=>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco "
  ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) {
        $paramsData[$e] = $blockCms[$e];
      }
    }
  } 
   $initBg= Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=> "background"
    ), "image"
  );

  $arrayBackground= [];
  foreach ($initBg as $k => $v) {
    $arrayBackground[] =$v['imagePath'];
  }
 
 ?>
<style type="text/css">

#homeCocityPrez<?= $kunik?>  {
  overflow: hidden;
  position: relative;
}
#homeCocityPrez<?= $kunik?> form {
  width: 100%;
  max-width: 700px;
  padding-top: 3%;
  margin-left: 23%;
}

#homeCocityPrez<?= $kunik?> form .inner-form {
  display: -ms-flexbox;
  display: flex;
  width: 100%;
  -ms-flex-pack: justify;
  justify-content: space-between;
  -ms-flex-align: center;
  align-items: center;
  box-shadow: 0px 8px 20px 0px rgba(0, 0, 0, 0.15);
  border-radius: 5px;
  overflow: hidden;
  margin-bottom: 30px;
  height: 50px;
}

#homeCocityPrez<?= $kunik?> form .inner-form .input-field {
  height: 68px;
}

#homeCocityPrez<?= $kunik?> form .inner-form .input-field input {
  height: 100%;
  border: 0;
  display: block;
  width: 100%;
  padding: 10px 0;
  font-size: 16px;
  color: #000;
}

#homeCocityPrez<?= $kunik?> form .inner-form .input-field input.placeholder {
  color: #222;
  font-size: 14px;
}

#homeCocityPrez<?= $kunik?> form .inner-form .input-field input:-moz-placeholder {
  color: #222;
  font-size: 14px;
}

#homeCocityPrez<?= $kunik?> form .inner-form .input-field input::-webkit-input-placeholder {
  color: #222;
  font-size: 14px;
}

#homeCocityPrez<?= $kunik?> form .inner-form .input-field input:hover, #homeCocityPrez<?= $kunik?> form .inner-form .input-field input:focus {
  box-shadow: none;
  outline: 0;
}

#homeCocityPrez<?= $kunik?> form .inner-form .input-field.first-wrap {
  -ms-flex-positive: 1;
  flex-grow: 1;
  display: -ms-flexbox;
  display: flex;
  -ms-flex-align: center;
  align-items: center;
  background: #fff;
}

#homeCocityPrez<?= $kunik?> form .inner-form .input-field.first-wrap input {
  -ms-flex-positive: 1;
  flex-grow: 1;
}

#homeCocityPrez<?= $kunik?> form .inner-form .input-field.first-wrap .svg-wrapper {
  min-width: 60px;
  display: -ms-flexbox;
  display: flex;
  -ms-flex-pack: center;
  justify-content: center;
  -ms-flex-align: center;
  align-items: center;
  font-size: 23px;
  color: #0A96B5;
}

#homeCocityPrez<?= $kunik?> form .inner-form .input-field.first-wrap svg {
  width: 36px;
  height: 36px;
  fill: #222;
}

#homeCocityPrez<?= $kunik?> form .inner-form .input-field.second-wrap {
  min-width: 50px;
}

#homeCocityPrez<?= $kunik?> form .inner-form .input-field.second-wrap .btn-search {
  height: 100%;
  width: 100%;
  white-space: nowrap;
  font-size: 21px;
  color: #fff;
  border: 0;
  cursor: pointer;
  position: relative;
  z-index: 0;
  background: #0A96B5;
  transition: all .2s ease-out, color .2s ease-out;
  font-weight: 300;
}

#homeCocityPrez<?= $kunik?> form .inner-form .input-field.second-wrap .btn-search:hover {
  background: #0A96B5;
}

#homeCocityPrez<?= $kunik?> form .inner-form .input-field.second-wrap .btn-search:focus {
  outline: 0;
  box-shadow: none;
}
.initialiseOrga {
  background-color: #0A96B5;
}
.initialiseCocity{
   background-color: #0A96B5;
}
#homeCocityPrez<?= $kunik?> .caption{
  margin: 10% 10%;
}
#homeCocityPrez<?= $kunik?>{
  <?php  if (count($arrayBackground)!=0) { ?>
    background-image: url(<?= $arrayBackground[0] ?>); 
  <?php }else { ?>
    background-image: url(<?= Yii::app()->getModule("costum")->assetsUrl ; ?>/images/cocity/etang4-2.jpg); 
  <?php } ?>
  background-size: cover;
}
@media (max-width: 978px) {
  #homeCocityPrez<?= $kunik?> form {
    width: 100%;
    max-width: 500px;
    height: 40px;
  }
  #homeCocityPrez<?= $kunik?> .caption h1{
    font-size: 45px;
    margin-top: 10%;
  }

  .#homeCocityPrez<?= $kunik?> .caption p {
    font-size: 18px;
    margin-top: 13%;
  }
}
@media (max-width: 716px) {
    #homeCocityPrez<?= $kunik?> form {
      margin-left: 0%;
    }
  }
</style>
       
<header id="homeCocityPrez<?= $kunik?>">
  <div class="caption"> 
    <h1 class="animated fadeInLeftBig title">
      <span><?= $paramsData["titre"]?></span>
    </h1>
    <div class="animated container fadeInRightBig description markdown"> <?= $paramsData["description"]?></div>
      <form id="formCocity">
        <div class="inner-form">
          <div class="input-field first-wrap">
            <div class="svg-wrapper">
              <i class="fa fa-search"> </i>              
            </div>
            <input type="text" id="nameCity" name="nameCity" placeholder="Taper votre ville ..." value="" onkeypress="cocityObj.searchCity() " />
          </div>
          <div class="input-field second-wrap">
            <button onclick="cocityObj.searchCity() " class="btn-search" type="button">
              <i class="fa fa-arrow-right"></i>
            </button>
          </div>
        </div>
      </form>
  </div>
</header>
<div id="costumActif<?= $kunik?>">
  
</div>
  <script type="text/javascript">
  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode($paramsData); ?>; 
  jQuery(document).ready(function() {
    sectionDyf.<?php echo $kunik ?>Params = {
      "jsonSchema" : {
        "title" : "Configurer votre section",
        "description" : "Personnaliser votre section",
        "icon" : "fa-cog",
        "properties" : {
          "titre" : {
            "label" : "titre ",
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.titre
          },
          "description" : {
            "inputType" : "textarea",
            "label" : "description",
            "markdown" : true,
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.description
          },
          background : {
            "inputType" : "uploader", 
            "label" : "background ",
            "docType": "image",
            "domElement" : "background",   
            "filetypes": ["jpeg", "jpg", "gif", "png"],
            "showUploadBtn": false,
            "endPoint" :"/subKey/background",
            initList : <?= json_encode($initBg)?>     
          },
        },

        beforeBuild : function(){
          uploadObj.set("cms","<?php echo $blockKey ?>");
        },
        save : function () {
          tplCtx.value = {};
          $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
            tplCtx.value[k] = $("#"+k).val();
            if (k == "parent") {
              tplCtx.value[k] = formData.parent;
            }
          });
          mylog.log("save tplCtx",tplCtx);

          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
          else {
              dataHelper.path2Value( tplCtx, function(params) {
                dyFObj.commonAfterSave(params,function(){
                  toastr.success("Élément bien ajouter");
                  $("#ajax-modal").modal('hide');
                  urlCtrl.loadByHash(location.hash);
                });
              } );
          }

        }
      }
    };
    mylog.log("sectiondyfff",sectionDyf);
    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });
    cocityObj ={
       // ********************** search a city**********************
      searchCity:function(){

        var nameCity =document.getElementById("nameCity").value;
        var data = {
          nameCity : nameCity
        };
        coInterface.showLoader("#costumActif<?= $kunik?>", "Recherche en cours");
        ajaxPost(
          null,
          baseUrl+"/costum/cocity/getcityaction",
          data,
          function(data){
            mylog.log("nomcity",data);
           
           // alert(image);
            var hsrc = '';
            if (data.length != 0){
               hsrc += '<div class=" text-center">';
               hsrc += '<a  class="initialiseCocity btn btn-default  "> Initialiser le costum pour la ville de '+nameCity;
                hsrc += '</a>';
               hsrc += '</div>';
              $.each(data, function( index, value ) {
                var image = typeof value.profilImageUrl!= "undefined" ? value.profilImageUrl : "<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/cocity/photo/aide.jpg";
                  //mylog.log("type slug",typeof value.costum);
                  //alert(value.costum.slug);
                if (typeof value.costum == "undefined") {
                  hsrc += "";
                } 
                if (typeof value.costum != "undefined" && value.costum.slug != "cocity") {
                  hsrc += "";
                }
                if (typeof value.costum != "undefined" && value.costum.slug == "cocity") {

                  hsrc += '<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 smartgrid-slide-element classifieds classifieds">';
                  hsrc += '<div class="item-slide">';
                  hsrc += '<div class="entityCenter">';
                  hsrc += ' <a href="" class="pull-right" ';
                  hsrc += '<i class="fa fa-rocket bg-azure"></i> </a>';
                  hsrc += '</div>';
                  hsrc += '<div class="img-back-card">';
                  hsrc += '<div class="container-img-card">';

                  hsrc += '<img class="img-responsive img-profil-entity"  src="'+ image +'">';
                  hsrc += ' </div>';
                  hsrc += '  <div class="text-wrap searchEntity">';
                  hsrc += '<div class="entityRight profil no-padding">';
                  hsrc += '<a href="<?php  echo Yii::app()->createUrl("/costum")?>/co/index/slug/'+ value.slug +'" class="entityName letter-turq" target="_blank">';
                  hsrc += '<font style="vertical-align: inherit;">';
                  hsrc += '<font style="vertical-align: inherit;">';
                  hsrc += '<i class="fa fa-rocket"></i>  '+ value.name;

                  hsrc += '</font>';
                  hsrc += '</font>';
                  hsrc += '</a>';
                  hsrc += '</div>';
                  hsrc += '</div>';
                  hsrc += '</div>';
                  hsrc += '<div class="slide-hover co-scroll">';
                  hsrc += '<div class="text-wrap">';
                  hsrc += '<h4 class="entityName">';
                  hsrc += '<a href="<?php  echo Yii::app()->createUrl("/costum")?>/co/index/slug/'+ value.slug +'" class=" letter-turq  " target="_blank">';
                  hsrc += '<font style="vertical-align: inherit;">';
                  hsrc += '<font style="vertical-align: inherit;">';
                  hsrc += '<i class="fa fa-rocket"> </i> ' + value.name ;
                  hsrc += '</font>';
                  hsrc += '</font>';
                  hsrc += '</a>';
                  hsrc += '</h4>';
                  hsrc += '<div class="entityType col-xs-12 no-padding">';
                  hsrc += '<span class="type">';
                  hsrc += '<font style="vertical-align: inherit;">';
                  hsrc += '<font style="vertical-align: inherit;">';
                  hsrc += '</font>';
                  hsrc += '</font>';
                  hsrc += '</span>';
                  hsrc += '</div>';
                  hsrc += '<div class="desc">';
                  hsrc += '<div class="socialEntityBtnActions btn-link-content">';
                  hsrc += '<a href="<?php  echo Yii::app()->createUrl("/costum")?>/co/index/slug/'+ value.slug +'"   class="btn btn-info btn-link btn-share-panel" target="_blank">';
                  hsrc += '<font style="vertical-align: inherit;">';
                  hsrc += '<font style="vertical-align: inherit;"> Voir le costum de cette ville'; 
                  hsrc += '</font>';
                  hsrc += '</font>';
                  hsrc += '</a>';
                  hsrc += '</div>';
                  hsrc += '</div>';
                  hsrc += '</div>';
                  hsrc += '</div>';
                  hsrc += '</div>';
                  hsrc += '</div>';
                 }
              });
            } else {

              hsrc += "<p class='text-center'>Il n'éxiste aucune cocity </p>";
              hsrc += '<div class=" text-center">';
               hsrc += '<a  class="initialiseCocity btn btn-default  "> Initialiser le costum pour la ville de '+nameCity;
                hsrc += '</a>';
               hsrc += '</div>';
            }
            $("#costumActif<?= $kunik?>").html(hsrc);
            $(".initialiseCocity").click(function(){
              var dyfOrga={
                "beforeBuild":{
                  "properties" : {
                    "name" : {
                      "label" : "Nom de votre ville", 
                      "inputType" : "textarea",
                      "value" :nameCity,
                      "order" : 1
                    },
                    "formLocality" : {
                      "label" : "Adresse de votre ville",
                      "rules" : {
                        "required" : true
                      }
                    },
                    "email" : {
                      "order" : 3,
                      "rules" : {
                        "required" : true
                      }
                    }
                  }
                },
                "onload" : {
                  "actions" : {
                    "setTitle" : "Initialiser une ville",
                    "src" : {
                      "infocustom" : "<br/>Remplir le champ"
                    }          
                  }
                }   
              };

              dyfOrga.afterSave = function(orga){
                dyFObj.commonAfterSave(data, function(){
                  var data = {"cocity":"cocity", ...orga};
                  ajaxPost(
                    null,
                    '<?= Yii::app()->baseUrl; ?>/costum/cocity/generate',
                    data,
                    function(data){
                      window.location = data.url;
                    },
                    function(error){
                      toastr.error("Une erreur s'est produite, veuillez réessayer et si le problème persiste, contecter l'administrateur")
                    },
                    "json"
                  );

                });
              }
              dyFObj.openForm('organization',null, null,null,dyfOrga);
           })
          }
        );
      }
    }
    $("#formCocity").on('submit', function(event){ 
      event.preventDefault();
      var data = $(this).serialize();
      cocityObj.searchCity();
      return false;      
    });
  });

</script>