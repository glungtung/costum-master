<?php 
$keyTpl = "headerEducation";
$paramsData=[
  "text" => "Université",
  "textColor" =>"#000000"
];


if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (  isset($blockCms[$e]) ) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
}
?>

<style type="text/css">
  .row<?php echo $kunik ?>{
/*    padding-top: 100px!important;*/
    color: #fff;
    height: 500px;
    background-position: center;
    background-repeat: no-repeat; 
    background-size: cover;
  }

.block-container-<?php echo $kunik ?>{
    border-bottom-left-radius: 1000px 150px;
    border-bottom-right-radius: 1000px 150px;
}
 
 .title-<?php echo $kunik ?>{
 	top: 40%;
 	width: 30%;
 	padding: 20px;
 	color: #000;
 	background-color: white;
    border-bottom-left-radius: 100px;
    border-top-left-radius: 100px;
    box-shadow: 0px 0px 10px 0px #00000087;
 } 

 .title-container-<?php echo $kunik ?> {
 	top:25%;
 	position: relative;
 	right: -10px;
 }

 @media (max-width: 768px) {
 	.title-<?php echo $kunik ?>{
 		top: 40%;
 		width: 100%;
 		padding: 20px;
 		color: #000;
 		background-color: white;
 		border-radius: 100px;
 		box-shadow: 0px 0px 10px 0px #00000087;
 	} 
 	.title-container-<?php echo $kunik ?> {
 		top:25%;
 		position: relative;
 		right: -10px;
 	}
 }
</style>
<div class="row row<?php echo $kunik ?> container<?php echo $kunik ?>">
  <div class="text-center padding-20 title-container-<?php echo $kunik ?>">
    <div class="title-<?php echo $kunik ?> pull-right">
      <h1 class="title" style=" font-family: Lato-Italic;color: <?= $paramsData["textColor"]; ?>"><?= $paramsData["text"]; ?></h1>
      <button style="display: none;  background-color: #44536a;"><span><i class="fa fa-plus"></i></span></button>
    </div>
  </div>
</div>
<script type="text/javascript">
  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
  jQuery(document).ready(function() {
    sectionDyf.<?php echo $kunik ?>Params = {
      "jsonSchema" : {    
        "title" : "Configurer votre section",
        "description" : "Personnaliser votre section",
        "icon" : "fa-cog",

        "properties" : {
          
          "text" : {
            "inputType" : "textarea",
            "label" : "Votre text",
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.text
          },
          "textColor" : {
            "inputType" : "colorpicker",
            "label" : "Couleur de la description",
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.textColor
          }
        },
        beforeBuild : function(){
          uploadObj.set("cms","<?php echo $blockKey ?>");
        },
        save : function (data) {  
          tplCtx.value = {};
          $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
            tplCtx.value[k] = $("#"+k).val();
            if (k == "parent")
              tplCtx.value[k] = formData.parent;

            if(k == "items")
              tplCtx.value[k] = data.items;
          });

          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
          else {
            dataHelper.path2Value( tplCtx, function(params) {
              dyFObj.commonAfterSave(params,function(){
                toastr.success("Modification enregistré!");
                $("#ajax-modal").modal('hide');
                urlCtrl.loadByHash(location.hash);
              });
            } );
          }

        }
      }
    };

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
      tplCtx.subKey = "imgParent";
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });
  });
</script>