<?php 
$keyTpl = "bannerWithSearchGlobal";
$paramsData = [
	"descriptionVille" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Nam convallis enim a finibus congue.Phasellus et tristique dolor. Integer volutpat viverra ornare."
];
if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
} 

$parent = Element::getElementSimpleById($this->costum["contextId"],$this->costum["contextType"],null,["name", "description","descriptionVille","shortDescription","slug","profilMediumImageUrl","email","socialNetwork","profilBannerUrl"]);



?>
<style type="text/css">
	
	.headerCocity_<?= $kunik?> .contentCocity{
		margin-top: 10%;
	}
	.headerCocity_<?= $kunik?> .contentCocity p {
		margin-top: 5%;
	}
	.headerCocity_<?= $kunik?> .contentCocity .bouton{
		background-color: #fff;
		text-transform: uppercase;
		padding: 1%;
		width: 20%;
		border-radius: 10px;
		height: 50px;
	    margin-top: 5%;
	    margin-bottom: 5%;
		font-weight: 800;
	}


	.searchCocity_<?= $kunik?> form {
		width: 100%;
		max-width: 700px;
		padding-top: 3%;
		margin-left: 20%;
	}

	.searchCocity_<?= $kunik?> form .inner-form {
		display: -ms-flexbox;
		display: flex;
		width: 100%;
		-ms-flex-pack: justify;
		justify-content: space-between;
		-ms-flex-align: center;
		align-items: center;
		box-shadow: 0px 8px 20px 0px rgba(0, 0, 0, 0.15);
		border-radius: 5px;
		overflow: hidden;
		margin-bottom: 30px;
		height: 50px;
	}

	.searchCocity_<?= $kunik?> form .inner-form .input-field {
		height: 68px;
	}

	.searchCocity_<?= $kunik?> form .inner-form .input-field input {
		height: 100%;
		border: 0;
		display: block;
		width: 100%;
		padding: 10px 0;
		font-size: 16px;
		color: #000;
	}

	.searchCocity_<?= $kunik?> form .inner-form .input-field input.placeholder {
		color: #222;
		font-size: 14px;
	}

	.searchCocity_<?= $kunik?> form .inner-form .input-field input:-moz-placeholder {
		color: #222;
		font-size: 14px;
	}

	.searchCocity_<?= $kunik?> form .inner-form .input-field input::-webkit-input-placeholder {
		color: #222;
		font-size: 14px;
	}

	.searchCocity_<?= $kunik?> form .inner-form .input-field input:hover, .searchCocity_<?= $kunik?> form .inner-form .input-field input:focus {
		box-shadow: none;
		outline: 0;
	}

	.searchCocity_<?= $kunik?> form .inner-form .input-field.first-wrap {
		-ms-flex-positive: 1;
		flex-grow: 1;
		display: -ms-flexbox;
		display: flex;
		-ms-flex-align: center;
		align-items: center;
		background: #fff;
	}

	.searchCocity_<?= $kunik?> form .inner-form .input-field.first-wrap input {
		-ms-flex-positive: 1;
		flex-grow: 1;
	}

	.searchCocity_<?= $kunik?> form .inner-form .input-field.first-wrap .svg-wrapper {
		min-width: 60px;
		display: -ms-flexbox;
		display: flex;
		-ms-flex-pack: center;
		justify-content: center;
		-ms-flex-align: center;
		align-items: center;
		font-size: 23px;
		color: #0A96B5;
	}

	.searchCocity_<?= $kunik?> form .inner-form .input-field.first-wrap svg {
		width: 36px;
		height: 36px;
		fill: #222;
	}

	.searchCocity_<?= $kunik?> form .inner-form .input-field.second-wrap {
		min-width: 50px;
	}

	.searchCocity_<?= $kunik?> form .inner-form .input-field.second-wrap .btn-search {
		font-size: 35px;
	    margin-top: 8px;
	    color: #fff;
	    padding: 10%;
	    cursor: pointer;
	    background: #0A96B5;
	}

	.searchCocity_<?= $kunik?> form .inner-form .input-field.second-wrap .btn-search:hover {
		background: #0A96B5;
	}

	.searchCocity_<?= $kunik?> form .inner-form .input-field.second-wrap .btn-search:focus {
		outline: 0;
		box-shadow: none;
	}
	
	@media (max-width: 978px) {
		.headerCocity_<?= $kunik?> .contentCocity{
			margin-top: 0%;
		}
		.headerCocity_<?= $kunik?> .contentCocity h1{
			font-size: 50px;
			margin-top: 10%;
		}

		.headerCocity_<?= $kunik?> .contentCocity p {
			font-size: 18px;
			margin-top: 13%;
		}
		.searchCocity_<?= $kunik?> form {
			width: 100%;
			max-width: 500px;
			height: 40px;
		}
		
		.headerCocity_<?= $kunik?> .contentCocity .bouton{
			width: 39%;
			border-radius: 10px;
			height: 40px;
			margin-top: 7%;
			font-weight: 600;
		}
	}
	@media (max-width: 716px) {
		.searchCocity_<?= $kunik?> form {
			margin-left: 0%;
		}
	}
</style>
<div class="headerCocity_<?= $kunik?> searchCocity_<?= $kunik?>" >
	<div class="text-center  container  ">
		<div class="contentCocity" >
			<h1 class="title"><?= $parent["name"]?></h1>
			<form id="search-form<?= $kunik?>" action="#">
				<div class="inner-form">
					<div class="input-field first-wrap">
						<div class="svg-wrapper">
							<i class="fa fa-search"> </i>              
						</div>
						<input type="text" id="otherInput" name="otherInput"  placeholder="Taper votre recherche ..." />
					</div>
					<div class="input-field second-wrap">
						<a href="javascript:;" class="btn btn-search" id="btn-search-<?= $kunik?>"><span class="glyphicon glyphicon-search"></span></a>
					</div>
				</div>
			</form>
			<div class="description markdown"> <?= $paramsData["descriptionVille"]?> </div>
			<a href="#@<?= $parent["slug"]?>" class="btn bouton" target="_blank">En savoir plus </a>
		</div>
	</div>
	
</div>
<script type="text/javascript">	
	sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	jQuery(document).ready(function() {
		$("#btn-search-<?= $kunik?>").click(function(){
			 $( "#search-form<?= $kunik?>" ).submit();
        	var str = $("#otherInput").val();
        	location.hash = "#search?text="+str;
			urlCtrl.loadByHash(location.hash);
    	});
    	$( "#search-form<?= $kunik?>" ).submit(function( event ) {
		  var str = $("#otherInput").val();
        	location.hash = "#search?text="+str;
			urlCtrl.loadByHash(location.hash);
		  event.preventDefault();
		});
		sectionDyf.<?php echo $kunik?>Params = {
			"jsonSchema" : {    
				"title" : "Configurer la section",
				"description" : "Personnaliser votre section",
				"icon" : "fa-cog",
				"properties" : {
					"descriptionVille" : {
						"label" : "description de la ville",
						"inputType":"textarea",
						"markdown":"true",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.descriptionVille
					}
				},				
				beforeBuild : function(){
					uploadObj.set("cms","<?php echo $blockKey ?>");
				},
				save : function () {  
					tplCtx.value = {};

					$.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
					});

					mylog.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
		                  dataHelper.path2Value( tplCtx, function(params) {
		                    dyFObj.commonAfterSave(params,function(){
		                      toastr.success("Élément bien ajouter");
		                      $("#ajax-modal").modal('hide');
		                      urlCtrl.loadByHash(location.hash);
		                    });
		                  } );
					}
					
				}
			}
		};
		$(".edit<?php echo $kunik?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";
			dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
		});

	});
</script>