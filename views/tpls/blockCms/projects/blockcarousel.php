<?php 
/**
 * TPLS QUI PERMET AFFICHAGE DES PROJETS
 */
$keyTpl = "blockcarousel";

$paramsData = [
    "title"         => "Block caroussel d'éléments",
    "description"   => "",
    "elementType"   => "projects",
    "background"    => "#FFFFFF",
    "icon"          => " ",
    "color"         => "",
    "limit"         => 3,
    "iconActionColor" =>  "#ABB76B"
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}

if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
    $el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );
}

?>
<style type="text/css">

    .active{
        width: 100%;
        margin: auto !important;
    }

    .truncate {
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
    }

    li.indicator_<?=$kunik?>{
        background-color: #dddddd !important;
        width: 15px !important;
        height: 15px !important;
        margin-right: .3em !important;
        margin-left: .3em !important;
    }

    li.active{
        background-color: #555555 !important;
    }

    .projet_<?= $kunik ?> .color-h2{
        margin-top: 5%;
    }
    .projet_<?= $kunik ?> .a-caroussel{
        color: <?= $paramsData["iconActionColor"] ?>;;
        text-decoration : none;
    }
    .projet_<?= $kunik ?> .modal-footer{
        border : none !important;
    }
    .projet_<?= $kunik ?> .carousel-indicators{
        bottom: 0px;
        position: relative;
    }
    .projet_<?= $kunik ?> .carousel-indicators li{
        border: 1px solid #8fcd52;
    }

    .projet_<?= $kunik ?> .arrow{
        margin-top: 2%; 
        font-size: 3rem; 
        color: #abb76b;
    }
    .projet_<?= $kunik?> .btn-edit-delete{
        display: none;
        z-index: 1000;
    }
    .projet_<?= $kunik?>:hover  .btn-edit-delete {
        display: block;
        -webkit-transition: all 0.9s ease-in-out 9s;
        -moz-transition: all 0.9s ease-in-out 9s;
        transition: all 0.9s ease-in-out 0.9s;
        position: absolute;
        left: 50%;
        transform: translate(-50%,-50%);
    }
    @media (max-width:768px){
        
        .projet_<?= $kunik ?>  .color-h2{
            margin-top: 5%;
        }
        .projet_<?= $kunik ?>  .a-caroussel{
            color:<?= $paramsData["iconActionColor"] ?>;;
            text-decoration : none;
        }
        .projet_<?= $kunik ?>  .modal-footer{
            border : none !important;
        }
        .projet_<?= $kunik ?>  .carousel-indicators{
            bottom: 0px;
            position: relative;
        }
        .projet_<?= $kunik ?>  .carousel-indicators li{
            border: 1px solid #8fcd52;
        }
        .projet_<?= $kunik ?>  .carousel-indicators .active{
            background-color: #8fcd52;
        }
    }
    
</style>
<section class="projet_<?= $kunik ?> text-center">
    <h1 class="text-center title">
        <i class="fa <?= $paramsData["icon"] ?>"></i>
        <span><?= $paramsData["title"]; ?></span>
    </h1>
    <div class="text-center btn-edit-delete ">
        <?php if(Authorisation::isInterfaceAdmin()){ ?>
            <button class="btn btn-primary btn-xs" onclick="dyFObj.openForm('project')">
                Ajouter un projet
            </button>
        <?php } ?>
    </div>
    <p class="markdown description">
        <?= "\n".$paramsData["description"]; ?>
    </p>
    <div id="elementsCarousel<?= $kunik ?>" class="carousel slide text-center" data-ride="carousel" data-type="multi">
        <div id="slider<?= $kunik ?>" class="text-center arrow hidden" style="">
            <a class="left a-caroussel" href="#elementsCarousel<?= $kunik ?>" data-slide="prev">  
                <i class="fa fa-arrow-left" aria-hidden="true"></i>
            </a>
            <a class="right a-caroussel" href="#elementsCarousel<?= $kunik ?>" data-slide="next">
                <i class="fa fa-arrow-right" aria-hidden="true"></i>
            </a>
        </div>
        <br>

        <div id="content-results-project<?= $kunik ?>" class="carousel-inner"></div>
       
        <div class="col-md-12" style="margin-top: 1%;">
           <ol class="carousel-indicators" id="indactors-elementsCarousel<?= $kunik ?>"></ol>
       </div>
   </div>
    
</section>

<script type="text/javascript">
    contextData = {
        id : "<?php echo $this->costum["contextId"] ?>",
        type : "<?php echo $this->costum["contextType"] ?>",
        name : "<?php echo $el['name'] ?>",
        slug : "<?php echo $el['slug'] ?>",
        profilThumbImageUrl : "<?php echo $el['profilThumbImageUrl'] ?>"
    };

    let nbItem<?= $kunik ?> = <?= $paramsData["limit"] ?>;
    let elementType<?= $kunik ?> = "<?= $paramsData["elementType"] ?>";

    tplCtx = {};
    
    sectionDyf = (typeof sectionDyf == "undefined") ? {} : sectionDyf;

    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

    $(document).ready(function(){
    sectionDyf.<?php echo $kunik ?>Params = {
        "jsonSchema" : {    
            "title" : "Configurer la section des projets sous forme carousel",
            "description" : "Personnaliser votre section sur des projets sous forme carousel",
            "icon" : "fa-cog",
            "properties" : {
                elementType : {
                    label : "Element",
                    inputType : "select",
                    options : {
                        "projects":"Liste projets de la communauté",
                        "citoyens":"Les personnes de la communauté"
                    },
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.elementType
                },
                title : {
                    label : "Titre",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.title
                },
                description : {
                    inputType: "textarea",
                    label : "Sous titre ou Description",
                    markdown: true,
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.description
                },
                icon : { 
                    label : "Icone",
                    inputType : "select",
                    options : <?= json_encode(Cms::$icones); ?>,
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.icon
                },
                limit : {
                    label : "Nombre de project afficher",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.limit
                },
                iconActionColor : { 
                    label : "Couleur des icones sur le caroussel",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.iconActionColor
                },
                parent : {
                    inputType : "finder",
                    label : tradDynForm.whoiscarrytheproject,
                    multiple : true,
                    rules : { required : true, lengthMin:[1, "parent"]}, 
                    initType: ["organizations"],
                    openSearch :true
                }
            },
            beforeBuild : function(){
              uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function () {  
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                    tplCtx.value[k] = $("#"+k).val();
                    if (k == "parent") {
                        tplCtx.value[k] = formData.parent;
                    }
                });
                mylog.log("save tplCtx",tplCtx);

                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) { 
                       dyFObj.commonAfterSave(params,function(){
                          toastr.success("Élément bien ajouter");
                          $("#ajax-modal").modal('hide');
                          urlCtrl.loadByHash(location.hash);
                      });
                    } );
                }
            }
        }
    }

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = "allToRoot";
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });
    // Affichage
    var params<?= $kunik ?> = {
        source : costum.contextSlug,
        type : contextData.type,
        id : contextData.id
    };
    // getcommunity, getprojects
    let request<?= $kunik ?> = "getprojects";
    if(elementType<?= $kunik ?>=="citoyens"){
        request<?= $kunik ?> = "getcommunity";
    }

    ajaxPost(
        null,
        baseUrl+"/costum/costumgenerique/"+request<?= $kunik ?>,
        params<?= $kunik ?>,
        function(data){
            mylog.log("success", data);
            var html = "";
            var li = "";
            var y= 0;

            if(data.result == true){

                i = 0;
                url = "<?= Yii::app()->getModule('costum')->assetsUrl; ?>/images/templateCostum/no-banner.jpg";
                html += "<div class='item active'>";
                li += "<li data-target='#elementsCarousel<?= $kunik ?>' data-slide-to='"+y+"' class='active indicator_<?=$kunik?>'></li>";

                $(data.elt).each(function(k,v){
                    let imgThumb = url;
                    
                    if(typeof v.profilMediumImageUrl != "undefined" && v.profilMediumImageUrl != null && v.profilMediumImageUrl != "none"){
                        imgThumb = v.profilMediumImageUrl; 
                    }

                    let descript = "";

                    if(data.elt.length <= nbItem<?= $kunik ?>){
                        $("#slider<?= $kunik ?>").remove();
                    }

                    if(i >= nbItem<?= $kunik ?>){
                        html += "</div>";
                        html += "<div class='item'>";
                        y++;
                        li += "<li data-target='#elementsCarousel<?= $kunik ?>' data-slide-to='"+y+"' class='indicator_<?=$kunik?>'></li>";
                        i = 0;
                        $(".arrow").removeClass("hidden");
                    }

                    i++;
                                
                    html += "<div class='card-info col-sm-12 col-md-"+(12/nbItem<?= $kunik ?>)+"'>";
  
                    html += "<center><img src='"+imgThumb+"' class='img-responsive' style='border-radius : 50%; width: 150px; height: 150px; object-fit: cover;'>";
                    
                    if(v.name){
                        name = v.name;
                    }else if(v[Object.keys(v)[0]]){
                        v = v[Object.keys(v)[0]];
                        v["id"] = v._id;
                    }
                    html += "<h2 class='color-h2 title-2 text-center truncate'>"+v.name+"</h2></center>";
                            
                    if(v.shortDescription){
                        descript = v.shortDescription;
                    }else if(v.email){
                        descript = v.email
                    }

                    html += "<p class='description'>"+descript.substring(0, 90)+" ...</p>";
                    
                    html += "<p><a href='#@"+v.slug+"' class='lbh-preview-element a-caroussel' style='font-size: 5rem;' data-hash='#page.type."+elementType<?= $kunik ?>+".id."+v.id+"'> <i class='fa fa-eye' aria-hidden='true'></i></a></p>";
                    html += "</div>";
                    
                    }
                );
            }else{
                html += "Pas de projet en cours";
                li += "";
            }
            $("#content-results-project<?= $kunik ?>").html(html);
            $("#indactors-elementsCarousel<?= $kunik ?>").html(li);
            }
        );

        $('.carousel[data-type="multi"] .item').each(function() {
            var next = $(this).next();
            if (!next.length) {
                next = $(this).siblings(':first');
            }
            next.children(':first-child').clone().appendTo($(this));

            for (var i = 0; i < nbItem; i++) {
                next = next.next();
                if (!next.length) {
                    next = $(this).siblings(':first');
                }

                next.children(':first-child').clone().appendTo($(this));
            }
        });
    });
</script>