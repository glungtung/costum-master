<?php 
  $keyTpl ="e_contact";
    $loremIpsum = "lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";
  $paramsData = [ 
    "title" => "",
    "subTitle" => "",
    "description" => $loremIpsum,
    "btnBgColor" => "#008037",
    "btnColor" => "#ffffff",
    "btnLabel" => "ME CONTACTER"
  ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) {
              $paramsData[$e] = $blockCms[$e];
      }
    }
  }

  $cssAnsScriptFiles = array(
    '/assets/vendor/jquery_realperson_captcha/jquery.realperson.css',
    '/assets/vendor/jquery_realperson_captcha/jquery.plugin.js',
    '/assets/vendor/jquery_realperson_captcha/jquery.realperson.min.js'
  //  '/assets/css/referencement.css'
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFiles, Yii::app()->theme->baseUrl); 
 ?>
 <!-- ****************get image uploaded************** --> 
<?php 
  $assetsUrl = Yii::app()->getModule('costum')->assetsUrl."/images/blockCmsImg/defaultImg";

    $LatestInitImageInText = [];
    $initImageInText = [];
    $LatestInitImageLogo = [];
    $initImageLogo = [];
    $LatestInitImageLogoFont = [];
    $initImageLogoFont = [];

    $initImage = Document::getListDocumentsWhere(
        array(
          "id"=> $blockKey,
          "type"=>'cms',
        ),"image"
    );
    //var_dump($initImage);
    foreach ($initImage as $key => $value) {
      if ($value["subKey"] == "imageInText") {
        $initImageInText[] = $value; 
        $LatestInitImageInText[]= $value["imageThumbPath"];
      }
      if ($value["subKey"] == "Logo") {
        $initImageLogo[] = $value; 
         $LatestInitImageLogo[]= $value["imageThumbPath"];
      }
      if ($value["subKey"] == "logoFont") {
        $initImageLogoFont[] = $value; 
         $LatestInitImageLogoFont[]= $value["imageMediumPath"];
      }
    }
 ?>
<!-- ****************end get image uploaded************** -->
<style>
    .invalid-feedback {
    display: none;
    width: 100%;
    margin-top: 0.25rem;
    font-size: 80%;
    color: #dc3545;
  }
  .sub-container-right<?=$kunik ?>{
      <?php if(!empty($LatestInitImageLogoFont)){ ?>
        background-image:url('<?=  $LatestInitImageLogoFont[0] ?>') !important;
      <?php }else{ ?>
        background-image:url('<?=  $assetsUrl ?>/bg_header.png') !important;
      <?php }?>
      background-size: cover;
      background-position: center;
      position: absolute;
      top: 0;
      width: 577px;
      height: 577px;
      right: 0px;
  }
  .sub-container-right<?= $kunik ?> img{
    position: absolute;
    top: 50%;
    left:61%;
    transform: translate(-50%,-50%);
    height: 50%;
  }
  .sub-container-left<?= $kunik ?> img{
    height: 237px;
  }


  .block-container-<?= $kunik ?> .btn-contactez-nous{
      color: <?php echo $paramsData["btnColor"] ?> !important;
      background-color: <?php echo $paramsData["btnBgColor"] ?> !important;
      font-weight: bold;
      margin: 0px 10px 38px 29px;
      padding: 14px 36px;
      border-radius: 12px;
      font-size: large;
  }
  .block-container-<?= $kunik ?> .btn-contactez-nous:hover{
      background: <?php echo $paramsData["btnBgColor"] ?>;
    letter-spacing: 1px;
    -webkit-box-shadow: 0px 5px 40px -10px rgba(0,0,0,0.57);
    -moz-box-shadow: 0px 5px 40px -10px rgba(0,0,0,0.57);
    box-shadow: 5px 40px -10px rgba(0,0,0,0.57);
    transition: all 0.4s ease 0s;
  }
    .block-container-<?= $kunik ?> .btn-contactez-nous{
      margin:0px;
    }
    .block-container-<?= $kunik ?> .btn-contactez-nous,.content<?php echo $kunik ?> .btn-savoir-plus{
        margin-top: 45px;
    }
    .block-container-<?= $kunik ?> .img-container{
      padding: 0px !important;
    }
    .sub-container-left<?=$kunik ?> .img-xs{
      display: none;
    }
    @media (max-width: 992px){
      .block-container-<?= $kunik ?> .img-container{
        display: none;
      }
      .sub-container-left<?=$kunik ?> .img-xs{
        display: block;
        float: right;
      }
    }
    @media (max-width: 360px){
      .sub-container-left<?=$kunik ?> .img-xs{
        display: initial;
        float: none;
      }
      .sub-container-left<?=$kunik ?> .p-img{
        text-align: center;
      }
        .block-container-<?= $kunik ?> .btn-contactez-nous,.content<?php echo $kunik ?> .btn-savoir-plus{
          margin-top: 45px;
          width: 100%
        }
    }
</style>
<h1 class="title"><?= $paramsData["title"] ?></h1>
<h2 class="subtitle"><?= $paramsData["subTitle"] ?></h2>
<div class="col-md-6 col-xs-12 sub-container-left<?=$kunik ?>">
  <p class="p-img">
      <?php if(!empty($LatestInitImageInText[0])){ ?>
          <img class="" src="<?=  $LatestInitImageInText[0] ?>">
      <?php }else{?>
          <img class="" src="<?=  $assetsUrl ?>/people_tree-02.png">
      <?php }?>


      <?php if(!empty($LatestInitImageLogo[0])){ ?>
        <img class="pull-right img-xs" src="<?=  $LatestInitImageLogo[0] ?>">
      <?php }else{ ?>
        <img class="img-xs" src="<?= $assetsUrl ?>/logo-02.png">
      <?php }?>
  </p>
  <div class="description markdown" style="position: relative;z-index: 1;line-height: 1.6;"><?= $paramsData["description"] ?></div>
    <p class="">
       <a href="javascript:;" class="btn tooltips openFormContact btn-contactez-nous" data-toggle="modal" data-target="#myModal-contact-us" data-id-receiver="" data-email="" data-name="">
        <?php echo $paramsData["btnLabel"] ?>
      </a>
    </p>
</div>
<div class="col-md-6 col-xs-12 img-container">
  <div style="" class="sub-container-right<?= $kunik ?>">
      <?php if(!empty($LatestInitImageLogo[0])){ ?>
        <img class=" pull-right" src="<?=  $LatestInitImageLogo[0] ?>">
      <?php }else{?>
        <img class=" pull-right" src="<?=  $assetsUrl ?>/logo-02.png">
      <?php }?>
  </div>
</div>

<div class="portfolio-modal modal fade" id="formContact" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content padding-top-15">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>

        <div class="container bg-white">

      <div id="form-group-contact">
        <div class="col-md-10 text-left padding-top-60 form-group">
          <h3>
            <i class="fa fa-send letter-blue"></i> 
            <small class="letter-blue">
            Envoyer un e-mail à : </small>
            <span id="contact-name" style="text-transform: none!important;"></span>
            <?php echo @$element["organizer"]["name"]; ?><br>
            <small class="">
              <small class="">Ce message sera envoyé à 
              </small>
              <b><span class="contact-email"></span></b>
            </small>
          </h3>
          <hr><br>
          <div class="col-md-6">
            <label for="email"><i class="fa fa-angle-down"></i> Votre addresse e-mail*</label>
              <input type="email" class="form-control" placeholder="votre addresse e-mail : exemple@mail.com" id="emailSender">
              <div class="invalid-feedback emailSender">Email invalide.</div><br>
          </div>

          <div class="col-md-6">
            <label for="senderName"><i class="fa fa-angle-down"></i> Nom / Prénom</label>
              <input class="form-control" placeholder="comment vous appelez-vous ?" id="senderName">
              <div class="invalid-feedback senderName">Champ requis.</div><br>
          </div>

          <div class="col-md-12">
            <label for="objet"><i class="fa fa-angle-down"></i> Objet de votre message</label>
              <input class="form-control" placeholder="c'est à quel sujet ?" id="subject">
              <div class="invalid-feedback subject">Objet requis.</div><br>

          </div>
        </div>
        <div class="col-md-12 text-left form-group">
          <div class="col-md-12">
            <label for="message"><i class="fa fa-angle-down"></i> Votre message</label>
            <textarea placeholder="Votre message..." class="form-control txt-mail" 
                  id="message" style="min-height: 200px;"></textarea>
            <div class="invalid-feedback message">Votre message est vide.</div><br>
            <div class="col-md-12 margin-top-15 pull-left">
              <button type="submit" class="btn btn-success pull-left" id="btn-send-mail">
                <i class="fa fa-send"></i> Envoyer le message
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {    
            "title" : "Configurer votre bloc",
            "description" : "Personnaliser votre bloc",
            "icon" : "fa-cog",
            
            "properties" : {
                
                "title" : {
                    "inputType" : "text",
                    "label" : "Titre",
                    
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.title
                },
                "subTitle" : {
                    "inputType" : "text",
                    "label" : "Sous titre",
                    
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.subTitle
                },
                "description" :{
                      "inputType" : "textarea",
                      "markdown" : true,
                      "label" : "Description",
                      values: sectionDyf.<?php echo $kunik ?>ParamsData.description
                },
                "btnLabel" : {
                    "inputType" : "Texte du boutton",
                    "label" : "Label du boutton",
                  
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnLabel
                },
                "btnColor" : {
                    "inputType" : "colorpicker",
                    "label" : "Couleur du boutton",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnColor
                },
                "btnBgColor" : {
                    "inputType" : "colorpicker",
                    "label" : "Couleur du fond du boutton",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnBgColor
                },
                "imageInText" :{
                  "inputType" : "uploader",
                  "label" : "Image sur le texte",
                  "docType": "image",
                  "contentKey" : "slider",
                  "itemLimit" : 1,
                  "endPoint": "/subKey/imageInText",
                  "domElement" : "imageInText",
                  "filetypes": ["jpeg", "jpg", "gif", "png"],
                  "showUploadBtn": false,
                  initList : <?php echo json_encode($initImageInText) ?>
                },
                "Logo" :{
                    "inputType" : "uploader",
                    "label" : "Logo",
                    "docType": "image",
                    "contentKey" : "slider",
                    "itemLimit" : 1,
                    "endPoint": "/subKey/Logo",
                    "domElement" : "Logo",
                    "filetypes": ["jpeg", "jpg", "gif", "png"],
                    "showUploadBtn": false,
                    initList : <?php echo json_encode($initImageLogo) ?>
                },
                "logoFont" :{
                    "inputType" : "uploader",
                    "label" : "Image font du logo",
                    "docType": "image",
                    "contentKey" : "slider",
                    "itemLimit" : 1,
                    "endPoint": "/subKey/logoFont",
                    "domElement" : "logoFont",
                    "filetypes": ["jpeg", "jpg", "gif", "png"],
                    "showUploadBtn": false,
                    initList : <?php echo json_encode($initImageLogoFont) ?>
                }          
            },
            beforeBuild : function(){
              uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;
              });
              console.log("save tplCtx",tplCtx);

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("Élément bien ajouter");
                      $("#ajax-modal").modal('hide');
                      urlCtrl.loadByHash(location.hash);
                    });
                  } );
              }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });

<?php // ***************************send mail************************************* ?>
  $("#btn-send-mail").click(function(){
    sendEmail();
  });

  $('#emailSender').filter_input({regex:'[^<>#\"\`/\(|\)/\\\\]'});
  $('#name').filter_input({regex:'[^<>#\"\`/\(|\)/\\\\]'});
  $('#message').filter_input({regex:'[^<>#\"\`/\(|\)/\\\\]'});
  $('#subject').filter_input({regex:'[^<>#\"\`/\(|\)/\\\\]'});

  $(".openFormContact").click(function(){
    mylog.log("openFormContact");
    if (exists(costum) && exists(costum.admin) && exists(costum.admin.email) && costum.admin.email != ""){
      $("#formContact .contact-email").html(costum.admin.email);
      $("#formContact").modal("show");
    }else{
      bootbox.alert("Veuillez réessayer plus tard");
    }
  })
});
function validateEmail(email) {
  const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}
function sendEmail(){
  var acceptFields = true;
  $.each(["emailSender","senderName","subject","message"],(k,v)=>{
      if($("#"+v).val() == ""){
        $("."+v).show();
        acceptFields=false
      }
      if(validateEmail($("#emailSender").val())==false){
        acceptFields=false;
        $(".emailSender").show();
      }

  });
  var seconds = Math.floor(new Date().getTime() / 1000);
  var allowedTime = localStorage.getItem("canSend");
  if(acceptFields){
    if(seconds < allowedTime){
      return bootbox.alert("<p class='text-center text-dark'>Renvoyer après "+(Math.floor((allowedTime-seconds)/60)==0 ? allowedTime-seconds +" seconde(s)" : Math.floor((allowedTime-seconds)/60)+" minute(s)")+ "</p>");
    }
    localStorage.removeItem("canSend");
        var emailSender = $("#emailSender").val();
        var subject = $("#subject").val();
        var senderName = $("#senderName").val();
        var message = $("#message").val();
        var emailFrom = $(".contact-email").html();

        var params = {
          tpl : "contactForm",
          tplMail : emailFrom,
          fromMail: emailSender, 
          emailSender:emailSender,
          tplObject:subject,
          subject :subject, 
          names:senderName,
          message : message,
          logo:"",
        };

        ajaxPost(
          null,
          baseUrl+'/'+moduleId+'/mailmanagement/createandsend',
          params,
              function(data){ 
                if(data.result == true){
                  localStorage.setItem("canSend", (seconds+300));
                  toastr.success("Votre message a bien été envoyé");
                  bootbox.alert("<p class='text-center text-green-k'>Email envoyé à "+emailFrom+" !</p>");
                  $.each(["emailSender","senderName","subject","message"],(k,v)=>{$("#"+v).val("")})
                }else{
                  toastr.error("Une erreur est survenue pendant l'envoie de votre message - error");
                  bootbox.alert("<p class='text-center text-red'>Email non envoyé à "+emailFrom+" !</p>");
                }   
              },
              function(xhr, status, error){
                  toastr.error("Une erreur est survenue pendant l'envoie de votre message - error");
              }
        );  
  }
}
</script>