<?php 
$keyTpl = "contacterNous";
$paramsData = [ 
	"title"=>"Lorem Ipsum",
	"sizeTitle"=>"35",
	"content" => "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
	"sizeContent"=>"20",
	"lienButton"=>"",
	"labelbutton"=>"Contacter-Nous",
	"colorTitle"=>"#000",
	"colorContent"=>"#000",
	"colorLabelButton"=>"#000",
	"colorbutton"=>"#ffffff",
	"colorBorderButton"=>"#000"
];
if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	} 
}

  // //$latestImg = Document::getLastImageByKey($blockKey,"cms","presentation");
  // $cssAnsScriptFiles = array(
  //   '/assets/vendor/jquery_realperson_captcha/jquery.realperson.css',
  //   '/assets/vendor/jquery_realperson_captcha/jquery.plugin.js',
  //   '/assets/vendor/jquery_realperson_captcha/jquery.realperson.min.js'
  // //  '/assets/css/referencement.css'
  //   );
  //   HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFiles, Yii::app()->theme->baseUrl); 

?>

<style type="text/css">
	.invalid-feedback {
	    display: none;
	    width: 100%;
	    margin-top: 0.25rem;
	    font-size: 80%;
	    color: #dc3545;
	}
	.textBack_<?= $kunik ?>{
		width: 100%;
		padding: 0 15px;		
	}
	.textBack_<?= $kunik ?> h2{
		text-align: center;
	    margin-top: 4%;
	    margin-bottom: -6%;
		font-size: <?= $paramsData["sizeTitle"]?>px;
		color: <?= $paramsData["colorTitle"]?>;
		
	}	
	.textBack_<?= $kunik ?> .container h3{
		font-size: <?= $paramsData["sizeContent"]?>px;
		color: <?= $paramsData["colorContent"]?>;
		
	}
	.textBack_<?= $kunik ?> p{
		text-align: left;
	    line-height: 30px;
	    text-transform: none;
	    padding-top: 8%;
	    padding-left: 2%;
		font-size: <?= $paramsData["sizeContent"]?>px;
		color: <?= $paramsData["colorContent"]?>;
	}
	.button_<?= $kunik ?> {
		background-color: <?= $paramsData["colorbutton"]?>; 
		border:1px solid <?= $paramsData["colorBorderButton"]?>;
		color: <?= $paramsData["colorLabelButton"]?>;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		font-size: 16px;
		padding: 8px 26px;
		margin-bottom: 6%;
		cursor: pointer;
		border-radius: 20px ;
	}
	.textBack_<?= $kunik ?> li {
		font-size: <?= $paramsData["sizeContent"]?>px;
		color: <?= $paramsData["colorContent"]?>;

	}
	

	@media screen and (min-width: 1500px){
		.textBack_<?=$kunik?> h2{
			margin-top: 0;
    		padding: 5% 0% 8% 1%;
		    font-size: 40px;
		    line-height: 35px;
		    text-transform: none;
		}
		.textBack_<?=$kunik?> p{
		    line-height: 35px;
		    font-size: 25px;
		    text-transform: none;
    		padding: 10% 0% 10% 1%;
		    margin-bottom: -90px;
	  	}
	  	.button_<?= $kunik ?> {
			text-align: center;
		    text-decoration: none;
		    display: inline-block;
		    font-size: 25px;
		    padding: 8px 26px;
		    margin-bottom: 5%;
		    cursor: pointer;
		    border-radius: 40px;
		}
	}

	@media (max-width: 414px) {
		.textBack_<?= $kunik ?> h2{
			margin-top: 20px;
			font-size: 20px;
		}
		.textBack_<?= $kunik ?> p{
			line-height: 15px;
			text-transform: none;
			margin-bottom: -6px;
			font-size: 13px;
		}
		.textBack_<?= $kunik ?> li{
			font-size: 13px;
		}
		.button_<?= $kunik ?> {
			padding: 8px;
			text-align: center;
			text-decoration: none;
			display: inline-block;
			font-size: 11px;
			margin: 4px 2px;
			cursor: pointer;
			border-radius: 20px;
		}
	}
</style>
<section  class="textBack_<?= $kunik ?>">
	<div class="container " >  
		<h2 class="text-center"> <?= $paramsData["title"]?></h2>
		<div class="markdown" ><?= $paramsData["content"]?></div>
		<a href="javascript:;" class="button_<?= $kunik ?> tooltips btn openFormContact text-center " data-toggle="modal" data-target="#myModal-contact-us"
	    data-id-receiver="" 
	    data-email=""
	    data-name="">
			<?= $paramsData["labelbutton"]?>
		</a>
	</div>
	<div class="portfolio-modal modal fade" id="formContact" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content padding-top-15">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>

        <div class="container bg-white">

      <div id="form-group-contact">
        <div class="col-md-10 text-left padding-top-60 form-group">
          <h3>
            <i class="fa fa-send letter-blue"></i> 
            <small class="letter-blue">
            Envoyer un e-mail à : </small>
            <span id="contact-name" style="text-transform: none!important;"></span>
            <?php 

            //echo @$element["organizer"]["name"];  ?><br>
            <small class="">
              <small class="">Ce message sera envoyé à 
              </small>
              <b><span class="contact-email"></span></b>
            </small>
          </h3>
          <hr><br>
          <div class="col-md-6">
            <label for="email"><i class="fa fa-angle-down"></i> Votre addresse e-mail*</label>
              <input class="form-control" placeholder="votre addresse e-mail : exemple@mail.com" id="emailSender">
              <div class="invalid-feedback emailSender">Email invalide.</div><br>
            <br>
          </div>
          <div class="col-md-6">
            <label for="name"><i class="fa fa-angle-down"></i> Nom / Prénom</label>
              <input class="form-control" placeholder="comment vous appelez-vous ?" id="name">
              <div class="invalid-feedback senderName">Champ requis.</div><br>
            <br>
          </div>
          <div class="col-md-12">
            <label for="objet"><i class="fa fa-angle-down"></i> Objet de votre message</label>
              <input class="form-control" placeholder="c'est à quel sujet ?" id="subject">
              <div class="invalid-feedback subject">Objet requis.</div><br>
          </div>
        </div>
        <div class="col-md-12 text-left form-group">
          <div class="col-md-12">
            <label for="message"><i class="fa fa-angle-down"></i> Votre message</label>
            <textarea placeholder="Votre message..." class="form-control txt-mail" 
                  id="message" style="min-height: 200px;"></textarea>
            <div class="invalid-feedback message">Votre message est vide.</div><br>
            <br>
           

            <div class="col-md-12 margin-top-15 pull-left">            
            <button type="submit" class="btn btn-success pull-right" id="btn-send-mail">
              <i class="fa fa-send"></i> Envoyer le message
            </button>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>	
</section>

<script type="text/javascript">
	sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	var currentCategory = "";
	jQuery(document).ready(function() {
		sectionDyf.<?php echo $kunik?>Params = {
			"jsonSchema" : {    
				"title" : "Configurer la section1",
				"description" : "Personnaliser votre section1",
				"icon" : "fa-cog",
				"properties" : {
					
					"title" : {
						"label" : "titre",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.title
					},
					"sizeTitle" : {
						"label" : "taille du titre",
						"inputType":"number",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.sizeTitle
					},
					"content" : {
						"inputType" : "textarea",
						"label" : "Contenu",
						"markdown" : true,
						values :  sectionDyf.<?php echo $kunik?>ParamsData.content
					},
					"sizeContent" : {
						"label" : "taille du contenu",
						"inputType":"number",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.sizeContent
					},
					"labelbutton" : {
						"label" : "Label du Boutton",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.labelbutton
					},
					"lienButton" : {
						"label" : "lien du boutton",
						"inputType" :"textarea",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.lienButton
					},
					"colorTitle":{
						label : "Couleur du titre",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.colorTitle
					},
					"colorContent":{
						label : "Couleur du text",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.colorContent
					},
					"colorLabelButton":{
						label : "Couleur du label de boutton",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.colorLabelButton
					},
					"colorbutton":{
						label : "Couleur du boutton",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.colorContent
					},
					"colorBorderButton":{
						label : "Couleur du bordure de boutton",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.colorBorderButton
					}
				},
				beforeBuild : function(){
					uploadObj.set("cms","<?php echo $blockKey ?>");
				},
				save : function () {  
					tplCtx.value = {};
					$.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
					});
					mylog.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
						dataHelper.path2Value( tplCtx, function(params) {
							dyFObj.commonAfterSave(params,function(){
								toastr.success("Élément bien ajouter");
								$("#ajax-modal").modal('hide');
								urlCtrl.loadByHash(location.hash);
							});
						} );
					}
				}
			}
		};
		mylog.log("sectiondyfff",sectionDyf);
		$(".edit<?php echo $kunik?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";
			dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
		});
	  $("#btn-send-mail").click(function(){
	    sendEmail();
	  });


  $('#emailSender').filter_input({regex:'[^<>#\"\`/\(|\)/\\\\]'});
  $('#name').filter_input({regex:'[^<>#\"\`/\(|\)/\\\\]'});
  $('#message').filter_input({regex:'[^<>#\"\`/\(|\)/\\\\]'});
  $('#subject').filter_input({regex:'[^<>#\"\`/\(|\)/\\\\]'});
  $(".openFormContact").click(function(){
    mylog.log("openFormContact");
    if (exists(costum) && exists(costum.admin) && exists(costum.admin.email) && costum.admin.email != ""){
      $("#formContact .contact-email").html(costum.admin.email);
      $("#formContact").modal("show");
    }else{
      bootbox.alert("Veuillez réessayer plus tard");
    }
  });
});

function validateEmail(email) {
  const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function sendEmail(){
  var acceptFields = true;
  $.each(["emailSender","senderName","subject","message"],(k,v)=>{
      if($("#"+v).val() == ""){
        $("."+v).show();
        acceptFields=false
      }
      if(validateEmail($("#emailSender").val())==false){
        acceptFields=false;
        $(".emailSender").show();
      }

  });
   var seconds = Math.floor(new Date().getTime() / 1000);
  var allowedTime = localStorage.getItem("canSend");
  if(acceptFields){
    if(seconds < allowedTime){
      return bootbox.alert("<p class='text-center text-dark'>Renvoyer après "+(Math.floor((allowedTime-seconds)/60)==0 ? allowedTime-seconds +" seconde(s)" : Math.floor((allowedTime-seconds)/60)+" minute(s)")+ "</p>");
    }
    localStorage.removeItem("canSend");
      var emailSender = $("#emailSender").val();
        var subject = $("#subject").val();
        var senderName = $("#senderName").val();
        var message = $("#message").val();
        var emailFrom = $(".contact-email").html();

        var params = {
          tpl : "contactForm",
          tplMail : emailFrom,
          fromMail: emailSender, 
          tplObject:subject,
          subject :subject, 
          names:senderName,
          emailSender:emailSender,
          message : message,
          logo:"",
        };
  
  		ajaxPost(
		    null,
		    baseUrl+'/'+moduleId+'/mailmanagement/createandsend',
		    params,
		        function(data){ 
		           	if(data.result == true){
		               	localStorage.setItem("canSend", (seconds+300));
		               	toastr.success("Votre message a bien été envoyé");
		               	bootbox.alert("<p class='text-center text-green-k'>Email envoyé à "+emailFrom+" !</p>");
		                $.each(["emailSender","senderName","subject","message"],(k,v)=>{$("#"+v).val("")});
		                urlCtrl.loadByHash(location.hash);
		            }else{
		                toastr.error("Une erreur est survenue pendant l'envoie de votre message - error");
		                bootbox.alert("<p class='text-center text-red'>Email non envoyé à "+emailFrom+" !</p>");
		            }   
		        },
        		function(xhr, status, error){
                  	toastr.error("Une erreur est survenue pendant l'envoie de votre message - error");
              	}
    	);
  	}  
  
}
</script>