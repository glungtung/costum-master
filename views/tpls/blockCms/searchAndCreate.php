<style type="text/css">
</style>
<div class="col-xs-12 col-sm-10 col-sm-offset-1">
	<div class="no-padding col-xs-12 text-left headerSearchContainer"></div>   

	<div id="filterCMS"></div>
	<div id="appCmsCurrent">
		
	</div>
	<div class="no-padding col-xs-12 text-left footerSearchContainer smartgrid-slide-element"></div>   
</div>
  
<script type="text/javascript">
	
	var paramsFilter= {
	 	container : "#filterCMS",
	 	loadEvent : {
	 		default : "pagination"
	 	},
	 	defaults : {
 			types : ["cms"],
 			fields : ["path"],
 			notSourceKey: true,
 			filters : {
 			 	"type" : "blockCms"
 			}
 			// 	parent : { 
	 		// 		"5ca1b2bb40bb4e9352ba351b" : {'$exists': 1} 
	 		// 	}
	 		// }
 		}, 
 		results : {
 			dom : '#appCmsCurrent',
 			renderView : "directory.cmsPanelHtml"
 		},
	 	filters : {
	 		text : true
	 	/*	type : {
	 			view : "selectList",
	 			event : "selectList",
	 			type : "type",
	 			list : eventList
	 		}*/
	 	}
	};
	var cmsSearch={};
	jQuery(document).ready(function() {
		cmsSearch = searchObj.init(paramsFilter);

		<?php $userId = isset(Yii::app()->session["userId"]) ? Yii::app()->session["userId"] : "";
        	if(Authorisation::isUserSuperAdmin($userId)){ ?>
		$(".headerSearchContainer .headerSearchright").append("<a href='javascript:;' class='createCMS btn btn-success'><i class='fa fa-plus padding-right-10'></i>Créer un bloc</a>");
		<?php } ?>
	
		$(".headerSearchContainer .headerSearchright .btn-show-map").remove();
		$("#appCmsCurrent").mouseover(function(){
		    $("html").css("overflow-y", "hidden");
		});
		cmsSearch.pagination.callBack = function(fObj){
			cmsBuilder.bindEvents();
		};
		cmsSearch.search.init(cmsSearch,0);
		
	});
</script>