<?php 
$keyTpl = "blockDocumentation";
$paramsData = [
	"question"=>"",
	"reponse"=>"",
	"photo"=>""
];
if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if ( !empty($blockCms[$e]) && isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}  
?>
<style type="text/css">
	.faq-img {
		width:40%;
		height:auto;
		vertical-align:middle;
		float: left;
		padding-right:15px;
	}
	.template_faq {
		background: #edf3fe none repeat scroll 0 0;
	}
	.panel-group<?=$keyTpl ?> {
		margin-left: 5%;
		padding: 30px;
	}
	/*#accordion .panel {
		border: medium none;
		border-radius: 0;
		box-shadow: none;
		margin: 0 0 15px 0px;
	}
	#accordion .panel-heading {
		border-radius: 30px;
		padding: 0;
	}
	#accordion .panel-title {
		text-transform: none;
	}
	#accordion .panel-title a {
		background: #8ABF32 ;
		border: 1px solid transparent;
		border-radius: 30px;
		color: #fff;
		display: block;
		font-size: 18px;
		font-weight: 600;
		padding: 12px 20px 12px 50px;
		position: relative;
		transition: all 0.3s ease 0s;
	}
	#accordion .panel-title a.collapsed {
		background: #fff none repeat scroll 0 0;
		border: 1px solid #ddd;
		color: #333;
	}
	#accordion .panel-title a::after, #accordion .panel-title a.collapsed::after {
		background: #8ABF32  ;
		border: 1px solid transparent;
		border-radius: 50%;
		box-shadow: 0 3px 10px rgba(0, 0, 0, 0.58);
		color: #fff;
		content: "";
		font-family: fontawesome;
		font-size: 25px;
		height: 55px;
		left: -20px;
		line-height: 55px;
		position: absolute;
		text-align: center;
		top: -5px;
		transition: all 0.3s ease 0s;
		width: 55px;
	}
	#accordion .panel-title a.collapsed::after {
		background: #fff none repeat scroll 0 0;
		border: 1px solid #ddd;
		box-shadow: none;
		color: #333;
		content: "";
	}
	#accordion .panel-body {
		background: transparent none repeat scroll 0 0;
		border-top: medium none;
		padding: 20px 25px 10px 9px;
		position: relative;
	}
	#accordion .panel-body p {
		border-left: 1px dashed #8c8c8c;
		padding-left: 25px;
	}*/
	.text-response-faq {
		text-align: justify;
	}

	/*Test*/

	.news_block {
		margin: 20px;
		padding: 10px;
		min-height: 200px;
		background: #F5F5F5;
	}
	.news_block img {
		float: left;
		padding-right:15px;
	}
	.title_news_date {
		float:right;
	}
	@media (max-width: 767px) {
		.faq-img {
			width:100%;
			height:auto;
			vertical-align:middle;
			float: left;
			padding-right:0px;
			padding-bottom: 15px;
		}
		.faq-container .panel-default {
			padding-right: 0px;
			padding-left: 0px;
		}
	}
	.questionLocal_<?=$keyTpl?> {
		padding-bottom: 5%; 
		box-shadow: 0px 0px 10px -8px silver;

	}
	.questionLocal_<?=$keyTpl?> .addFaq{
		background: #8ABF32;	
		margin-top: 5%;	
		margin-left: 14%;
		color: #fff;
		padding: 10px;
		text-align: center;
		font-size: 20px;
	}
	@media (max-width: 978px) {
		.questionLocal_<?=$keyTpl?> .addFaq{
			margin-top: 5%;
			margin-left: 10%;
			padding: 10px;
			text-align: center;
			font-size: 17px;
		}


	}
</style>



<div class="portfolio-modal modal fade" id="formDocumentation" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content padding-top-15">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>

        <div class="container bg-white">

			





        	
<div class=" questionLocal_<?=$keyTpl?> ">
	<a href="javascript:;" class="btn btn-xs addFaq" id="addFaqs">
		<i class="fa fa-plus"></i>
		Ajouter une question
	</a>
	
	<div class="panel-group<?=$keyTpl ?>">

	</div>


</div>

<script type="text/javascript">	
	getFaqs();



      sectionDyf.<?php echo $keyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
      jQuery(document).ready(function() {
      	$("#addFaqs").click(function(){
		alert("hello");
      		$("#formDocumentation").modal("hide");
      		sectionDyf.<?php echo $keyTpl ?>Params = {
          "jsonSchema" : {    
            "title" : "Ajouter une Question",
            "description" : "Ajouter une question",
            "icon" : "fa-info",
            
            "properties" : {
            	"type" : {
	                "inputType" : "hidden",
	                value : "documentation"
	             },
             	"question" : {
					label : "Question",
					values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.question
				},
				"reponse" : { 
					label : "Reponse",
					"inputType" : "textarea",
					"markdown" : true,
					values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.reponse
				},
				"photo" : {
					label : "Image Correspondant",
					values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.photo
				},
            },
            save : function () {  
              tplCtx.path = "allToRoot";
              tplCtx.collection = "cms";
              $.each( sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent") {
                  tplCtx.value[k] = formData.parent;
                }
              });
              console.log("save tplCtx",tplCtx);

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                dataHelper.path2Value( tplCtx, function(params) { 
                  $("#ajax-modal").modal('hide');
                  //location.reload();
                } );
              }

            }
          }
        };

        dyFObj.openForm(sectionDyf.<?php echo $keyTpl ?>Params);
      	});



      	});

      jQuery(document).ready(function() {
        getAjax("", baseUrl+"/costum/blockcms/getdocumentationaction",
            function(data){
            	//alert(data.type);

            	var src = "";
				src +='<div class="row">';
				src +='<div class="col-md-12">';
				src += '<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">';				
				if (data.length != 0){
					$.each(data, function( index, value ) {
						//mylog.log("imageeee", value.profilImageUrl);
						if (typeof value.question != "undefined"){
							question = value.question;
						} else {
							question ="";
						}
						if (typeof value.reponse != "undefined"){
							reponse = value.reponse;
						} else {
							reponse ="";
						}
						src += '<div class="panel panel-default col-xs-12 col-sm-12 col-md-6">';
						src += '<div class="panel-heading" role="tab" id="heading'+index+'">';
						src += '<h4 class="panel-title">';
						src += '<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse'+index+'" aria-expanded="true" aria-controls="collapse'+index+'">' +
						question;

						src += '</a>';
						src += '</h4>';
						src += '</div>';
						src += '<div id="collapse'+index+'" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">';
						src += '<div class="panel-body">'
						src += '<img class="faq-img" src="'+value.photo+'">';
						src += '<p class="text-response-faq">'+reponse;
						src += '</p>';
						src += '</div>';
						src += '</div>';
						src += '</div>';

					});
					src += '</div>';
					src += '</div>';
					src += '</div>';
				}

				else {
					src += "<p class='text-center'>Il n'éxiste aucun question </p>";
				}
				$(".panel-group<?php echo $keyTpl ?>").html(src);

            });
    });
        

	

	function getFaqs(){
		

	}  
</script>






			

			
		</div>
	</div>
</div>
<script type="text/javascript">
jQuery(document).ready(function() {
    

	$(".openFormDocumentation").click(function(){
		//alert("hello");
		$("#formDocumentation").modal("show");
	});
});



(function($) {
		'use strict';
		jQuery(document).on('ready', function(){
			$('a.page-scroll').on('click', function(e){
				var anchor = $(this);
				$('src, body').stop().animate({
					scrollTop: $(anchor.attr('href')).offset().top - 50
				}, 1500);
				e.preventDefault();
			});		
		}); 	
	})(jQuery);
</script>
<script type="text/javascript">
	sectionDyf.<?php echo $keyTpl?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	jQuery(document).ready(function() {
		sectionDyf.<?php echo $keyTpl?>Params = {
			"jsonSchema" : {    
				"title" : "Configurer la section1",
				"description" : "Personnaliser votre section1",
				"icon" : "fa-cog",
				"properties" : {
					"backgroundQuestion" : {
						"label" : "Couleur du background question",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $keyTpl?>ParamsData.backgroundQuestion
					}
					
				},
				save : function () {  
					tplCtx.value = {};

					$.each( sectionDyf.<?php echo $keyTpl?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
					});

					mylog.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
						dataHelper.path2Value( tplCtx, function(params) { 
							$("#ajax-modal").modal('hide');
							toastr.success("élement mis à jour");
							location.reload();
						} );
					}
					
				}
			}
		};
		$(".edit<?php echo $keyTpl?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";
			dyFObj.openForm( sectionDyf.<?php echo $keyTpl?>Params,null, sectionDyf.<?php echo $keyTpl?>ParamsData);
		});

	});

</script>
