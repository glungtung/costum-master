<div class="col-xs-12 col-xs-10 col-xs-offset-1 padding-10" >
	<center><h4>Mention légale</h4></center>
	<!-- TEST Pull Request <span class="text-center col-xs-12"><b>Editeur du site :</b></span><br/><br/> -->
	
	La Raffinerie<br/>
	Adresses : <br/>
	E-mail : contact@laraffinerie.re <br/>
	La Raffinerie est un projet de développement d’une friche éco-culturelle dans le quartier prioritaire de Savanna à Saint Paul de la Réunion, groupement d’espaces autonomes et interdépendants, ce projet se positionne comme un vrai levier de promotion du territoire et d’émergence d’activité à l’échelle locale et régionale.<br/>
	Les modérateurs du site sont les administrateurs de la Raffinerie.<br/><br/>

	<span class="text-center col-xs-12"><b>Développeur du site :</b></span><br/><br/>
	
	Association loi 1901 Open Atlas. <br/>
	Numéro de SIRET : 513 381 830 00027. <br/>
	56 RUE ANDY, 97422, La Réunion. <br/>
	Email : contact@communecter.org <br/>
	Site Web : <a href="https://www.open-atlas.org/" >https://www.open-atlas.org/</a> <br/>

	<span class="text-center col-xs-12"><b>Hébergeur du site :</b></span><br/><br/>

	Hébergeur : OVH.net, <a href="https://www.ovh.com/" >https://www.ovh.com/</a><br/>

	<span class="text-center col-xs-12"><b>Conditions d'utilisation :</b></span><br/><br/>

	Le site ne pourra, en aucun cas, être tenu responsable du contenu des documents (textes, photos et autres) fournis par les contributeurs. <br/>
	Les modérateurs du site se réservent le droit de supprimer, sans préavis, toute information qui ne serait pas conforme à la charte du site et/ou qui serait susceptible de porter atteinte à un tiers.
	Il est permis à tout utilisateur (acteur-contributeur ou visiteur) de nous signaler un contenu qui lui semble abusif et/ou contraire à la charte.<br/>
</div>