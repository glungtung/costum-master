<?php
	if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
		$el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );
		$cmsList = PHDB::find(Cms::COLLECTION,array( "parent.".$this->costum["contextId"] => array('$exists'=>1)));
	}
	
	$params = [
		"tpl" => $el["slug"],
		"slug"=>$this->costum["slug"],
		"canEdit"=>true,
		"blockCms" => $cmsList,
		"el"=>$el 
	]; 
	echo $this->renderPartial("costum.views.tpls.tplsEngine", $params,true );
?>
<input type="hidden" id="assets-url" value="<?= Yii::app()->getModule('costum')->assetsUrl ?>">
<script>
	var ASSETS_URL = null;
	var USER_ID = $('#user-id').val();

	var DIR_DEGREE = {
		e:90,
		n:0,
		ne:45,
		nw:315,
		s:180,
		se:135,
		sw:225,
		w:270,
		getWindDegree: (dir) => {
			var degree = DIR_DEGREE[dir];
			return (degree > 180)?(degree - 180):(degree + 180);
		} 
	}

	$(function(){
		ASSETS_URL = $('#assets-url').val();
	})
</script>