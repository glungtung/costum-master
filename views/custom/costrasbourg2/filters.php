<?php 
    $cssAnsScriptFilesModule = array(
    	'/css/filters.css',
	);
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());
?> 
<style type="text/css">

	#filters-nav #input-sec-search .input-global-search{
		border-radius: 0px 20px 20px 0px !important;
	}

	#filters-nav .dropdown-menu-complexe {
	    border-radius: 5px;
	    border: 1px solid #2c407a;
	    padding: 10px;
	}
	
	#filters-nav .dropdown-menu-complexe .list-filters .button-complexeSelectList{
    background: transparent;
    border: transparent;
	}

	#filters-nav .dropdown-menu-complexe .list-filters .button-complexeSelectList-subsub{
	    background: transparent;
	    border: transparent;
	}

	#filters-nav .dropdown-menu-complexe .list-filters .button-complexeSelectList-subsub:hover{
	    background-color : #2c407a;
	    color: white;
	}

	#filters-nav .dropdown-menu-complexe .list-filters .button-complexeSelectList:hover{
	    background-color : #2c407a;
	    color: white;
	}

	#filters-nav .dropdown-menu-complexe{
	    color: #2c407a;
	}

	#filterContainer .dropdown .btn-menu, #filters-nav .complexeSelect .btn-menu ,#filters-nav .dropdown .btn-menu, #filters-nav .filters-btn {
	    padding: 10px 15px;
	    font-size: 16px;
	    border: 1px solid #2c407a;
	    color: #2c407a;
	    top: 0px;
	    position: relative;
	    border-radius: 20px;
	    text-decoration: none;
	    background: white;
	    line-height: 20px;
	}

	#filterContainer #input-sec-search .input-group-addon, #filters-nav #input-sec-search .input-group-addon, .searchBar-filters .input-group-addon {
	    padding: 10px 0px 10px 10px;
	    font-size: 18px;
	    font-weight: 400;
	    line-height: 1;
	    color: #2c407a;
	    margin-top: 5px;
	    text-align: center;
	    background-color: white !important;
	    border-right-width: 0px;
	    border: 1px solid #2c407a;
	    border-right-width: 1px;
	    border-radius: 0px 20px 20px 0px;
	    border-right-width: 0px;
	    width: 30px;
	}

	#filters-nav .dropdown-menu-complexe .list-filters .lvlOne{
	    background: transparent;
	    border: transparent;
	}

	#filters-nav .dropdown-menu-complexe .list-filters .lvlOne:hover{
	    background-color : #2c407a;
	    color: white;
	}

	#filters-nav .dropdown-menu-complexe .list-filters .lvlTwo{
	    background: transparent;
	    border: transparent;
	}

	#filters-nav .dropdown-menu-complexe .list-filters .lvlTwo:hover{
	    background-color : #2c407a;
	    color: white;
	}


	#filterContainer #input-sec-search .input-global-search, #filters-nav #input-sec-search .input-global-search, .searchBar-filters .search-bar {
	    border-left-width: 0px;
	    height: 40px;
	    margin-top: 5px;
	    border-radius: 20px 0px 0px 20px;
	    box-shadow: none;
	    width: inherit;
	    border-color: #2c407a;
	}
</style>

<script type="text/javascript">
	var pageApp=<?php echo json_encode(@$page); ?>;

	if (pageApp == "search"){
		var paramsFilter= {
		 	container : "#filters-nav",	
		 	defaults : {
		 		types : ["organizations"]
		 	},
		 	filters : {
	            types : {
		 			view : "selectList",
		 			type : "filters",
		 			field : "types",
		 			name : "Status",
		 			event : "filters",
		 			options : {
		 				"NGO" :"Assocoation" ,
		 				"Group" : "Groupe", 
		 				"GovernmentOrganization" : "Service public", 
		 				"Cooperative" : "Cooperative"
	 				}
	 			},
	 			community : {
	                view : "selectList",
	                type : "filters",
	                field : "community",
	                name : "Communauté",
	                event : "filters",
	                list : costum.lists.community
	            },
	            category : {
	                view : "selectList",
	                type : "filters",
	                field : "category",
	                name : "Services",
	                event : "filters",
	                list : costum.lists.services
	            }
	        }
		};
	}

	 if (pageApp == "annonces") {
		var paramsFilter= {
		 	container : "#filters-nav",
		 	defaults : {
		 		types : ["classifieds"]
		 	},
		 	filters : {
	            type : {
	                view : "selectList",
	                type : "filters",
	                field : "subtype",
	                name : "Type",
	                event : "filters",
	                list : [
	                	"benevolat",
	                	"material",
	                	"vente",
	                	"donate",
	                	"ecolocation"
	                ]
	            },
	            devise : {
	                view : "selectList",
	                type : "filters",
	                field : "devise",
	                name : "Monnaie",
	                event : "filters",
	                list : [
	                	"Ğ1",
	                	"Stück",
	                	"Temps",
	                	"Aucun"
	                ]
	            }
	        }  
	    };
 	}

 	if (pageApp == "projects") {
		var paramsFilter= {
		 	container : "#filters-nav",
		 	defaults : {
		 		types : ["classifieds"]
		 	},
		 	filters : {
	            type : {
	                view : "selectList",
	                type : "filters",
	                field : "avancement",
	                name : "État",
	                event : "filters",
	                list : {
	                	"Idée" : "Idée",
            			"Actif" : "Actif",
            			"Archive" : "Archive"
            		}
	            }
	        }  
	    };
 	}

 	if (pageApp == "dda") {
		var paramsFilter= {
		 	container : "#filters-nav",
		 	defaults : {
		 		types : ["proposals"]
		 	},
		 	filters : {
	            type : {
	                view : "selectList",
	                type : "filters",
	                field : "sourceProp",
	                name : "Source",
	                event : "filters",
	                list : {
	                	"Ville ou Eurometropole" : "Ville ou Eurométropole",
            			"Citoyen" : "Citoyen"
            		}
	            }
	        }  
	    };
 	}

	var filterSearch={};
	jQuery(document).ready(function() {
		var pageApp=<?php echo json_encode(@$page); ?>;

		filterSearch = searchObj.init(paramsFilter);
	});
</script>