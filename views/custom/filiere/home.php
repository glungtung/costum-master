<?php
	if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
		$el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );

		$cmsList = PHDB::find(Cms::COLLECTION,array( "parent.".$this->costum["contextId"] => array('$exists'=>1)));
	}
	$param=["canEdit"=>true];
	$params = [
		"tpl" => $el["slug"],
		"slug"=>$this->costum["slug"],
		"canEdit"=>true,
		"blockCms" => $cmsList,
		"el"=>$el ]; 
	echo $this->renderPartial("costum.views.tpls.tplsEngine", $params,true );
?>

<style type="text/css">
	.p-4{
		padding-left: 2em;
		padding-right: : 2em;
		padding-top: 2em;
		padding-bottom: 2em;
	}

	.p-3{
		padding: 1.5em;
	}

	.mb-4{
		margin-bottom: 1.5em;
	}
</style>

<footer class="bg-dark text-white text-lg-start">
	<br>
	<br>
  <!-- Grid container -->
  <div class="container p-4">
    <!--Grid row-->
    <div class="row">
      <!--Grid column-->
      <div class="col-lg-6 col-md-12 mb-4 mb-md-0">
        <h5 class="text-uppercase title5">Footer Content</h5>

        <p>
          Lorem ipsum dolor sit amet consectetur, adipisicing elit. Iste atque ea quis
          molestias. Fugiat pariatur maxime quis culpa corporis vitae repudiandae aliquam
          voluptatem veniam, est atque cumque eum delectus sint!
        </p>
      </div>
      <!--Grid column-->

      <!--Grid column-->
      <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
        <h5 class="text-uppercase">Links</h5>

        <ul class="list-unstyled mb-0">
          <li>
            <a href="#!" class="text-white">Link 1</a>
          </li>
          <li>
            <a href="#!" class="text-white">Link 2</a>
          </li>
          <li>
            <a href="#!" class="text-white">Link 3</a>
          </li>
          <li>
            <a href="#!" class="text-white">Link 4</a>
          </li>
        </ul>
      </div>
      <!--Grid column-->

      <!--Grid column-->
      <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
        <h5 class="text-uppercase title-5 mb-0">Links</h5>

        <ul class="list-unstyled">
          <li>
            <a href="#!" class="text-white">Link 1</a>
          </li>
          <li>
            <a href="#!" class="text-white">Link 2</a>
          </li>
          <li>
            <a href="#!" class="text-white">Link 3</a>
          </li>
          <li>
            <a href="#!" class="text-white">Link 4</a>
          </li>
        </ul>
      </div>
      <!--Grid column-->
    </div>
    <!--Grid row-->
  </div>
  <!-- Grid container -->

  <!-- Copyright -->
  <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
    Copyright © 2020 
  </div>
  <!-- Copyright -->
</footer>