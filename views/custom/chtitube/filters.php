<script type="text/javascript">
	var pageApp=<?php echo json_encode(@$page); ?>;
	var paramsFilter= {
	 	container : "#filters-nav",
	 	filters : {
	 		text : {
	 			view : "text",
	 			type : "text",
	 			action : "text"
	 		},
	 		scope : {
	 			view : "scope",
	 			type : "scope",
	 			action : "scope"
	 		},
	 		types : {
	 			view : "types",
	 			type : "types",
	 			action : "types",
	 			lists : ["NGO", "LocalBusiness", "Group", "GovernmentOrganization", "projects", "events", "poi", "proposals"]
	 		}
	 	}
	};
	 
	function lazyFilters(time){
	  if(typeof filterObj != "undefined" )
	    filterGroup = filterObj.init(paramsFilter);
	  else
	    setTimeout(function(){
	      lazyFilters(time+200)
	    }, time);
	}

	jQuery(document).ready(function() {
		lazyFilters(0);
	});
</script>