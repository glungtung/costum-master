<?php 
$cssAnsScriptFilesTheme = array(
		
	'/plugins/jQCloud/dist/jqcloud.min.js',
	'/plugins/jQCloud/dist/jqcloud.min.css',

);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme,Yii::app()->request->baseUrl);
$tagsPoiList = array();
if(!empty($allTags)){
	shuffle($allTags);
	foreach ($allTags as $key => $elem){
		$found = false;
		foreach ($tagsPoiList as $ix => $value) {
			$value["text"]; 
			if(	$value["text"] == $elem)
				$found = $ix;
		}
		if ( !$found )
			array_push($tagsPoiList,array(
				"text"=>$elem,
				"weight"=>1,
				"link"=>array(
					"href" => "javascript:;",
					"class" => "favElBtn ".InflectorHelper::slugify2($elem)."Btn",
					"data-tag" => InflectorHelper::slugify2($elem)
				)
			));
		else
			$tagsPoiList[$found]["weight"]++;
	}
}
 ?>
<style type="text/css">
	.stat-card {
		background: #e2e2e2;
		margin-bottom: 15px;
	}
	.obsIcon {
		font-size: 34px;
	    border-radius: 50%;
	    padding: 15px;
	    margin-top: 19px;
	    margin-bottom: auto;
	    color: #005E6F;
	    background: #ddd;
	}
	.observacity h4 {
		font-size: 14px ; 
		color: #005E6F; 
		font-weight: bold ;
	}
	.counter {
		color: #8ABF32; 
		font-weight: bold !important; 
	}

</style>

<div class="container observacity">
	<div class="">
	    <div class="">
	        <article class="">
	          <div class="col-md-12" style="margin-bottom: 40px; ">
	              <div class="col-md-offset-2 col-md-8">
	                  <h1 class="" style="text-transform: none; text-align: center">OBSERVACITY </h1>
	              </div>
	          </div>
	      </article>
	  </div>
	</div>
	<div class="col-md-12 text-center">

	        <div class=" col-md-3   col-sm-6 col-xs-12 ">
	        	<div class="container-fluid  stat-card">
   					<div class="row">
			        	<div class="col-md-5">
			        		<i class="fa fa-rocket obsIcon " style="">
			        		</i>
			        	</div>
			        	<div class="col-md-7">
			        		<div class="card-box card tilebox-one" >
				                <h4 class=""  >Projets</h4>
				                <h3 class="counter" data-plugin="counterup" ><?php echo count($projects) ?></h3>
			            	</div>
			        	</div>
			        </div>
			    </div>
	        </div>
	        <div class=" col-md-3   col-sm-6 col-xs-12 ">
	        	<div class="container-fluid  stat-card">
   					<div class="row">
			        	<div class="col-md-5">
			        		<i class="fa fa-users  obsIcon" >
			        		</i>
			        	</div>
			        	<div class="col-md-7">
			        		<div class="card-box card tilebox-one" >
				                <h4 class=""  >Organisations</h4>
				                <h3 class="counter" data-plugin="counterup" ><?php echo count($organization) ?></h3>
			            	</div>
			        	</div>
			        </div>
			    </div>
	        </div>
	        <div class=" col-md-3   col-sm-6 col-xs-12 ">
	        	<div class="container-fluid  stat-card">
   					<div class="row">
			        	<div class="col-md-5">
			        		<i class="fa fa-user obsIcon " >
			        		</i>
			        	</div>
			        	<div class="col-md-7">
			        		<div class="card-box card tilebox-one" >
				                <h4 class=""  >Personnes</h4>
				                <h3 class="counter" data-plugin="counterup" > 0<?php  //echo count($projects) ?></h3>
			            	</div>
			        	</div>
			        </div>
			    </div>
	        </div>
	        <div class=" col-md-3   col-sm-6 col-xs-12 ">
	        	<div class="container-fluid  stat-card">
   					<div class="row">
			        	<div class="col-md-5">
			        		<i class="fa fa-calendar obsIcon" >
			        		</i>
			        	</div>
			        	<div class="col-md-7">
			        		<div class="card-box card tilebox-one" >
				                <h4 class=""  >Evènement</h4>
				                <h3 class="counter" data-plugin="counterup"><?php echo count($event) ?></h3>
			            	</div>
			        	</div>
			        </div>
			    </div>
	        </div>
	       
	       
	</div>	
</div>
<div class="col-sm-6" id="canvas"></div>
	<div class=" col-md-6 stat-card">
                      <div class="card-box card tilebox-one">
                <?php foreach ($blocks as $key => $value) {
                  if(isset($value["graph"])){
                        if($value["graph"]["key"] == "pieManynombrecocity"){
                      ?>
                  
                            <div><h6 class="" style="font-size: 14px !important; color: #3ea3b4; font-weight: bold !important;" ><?php echo $value["title"]?> </h6></div>
                            <div class="counterBlock <?php //echo $borderClass; ?>" id="<?php echo $key?>" >
                                <?php //if( isset($d["html"]) ) 
                                    //echo $d["html"];?>
                          
                  </div> 
                  <?php
                       
                        }
                      }
                        
                      }; ?>
              </div>
</div>
<script type="text/javascript">
	 <?php  
		foreach ($blocks as $id => $d) {
		    if( isset($d["graph"]) ) {
		        ?>
		        var <?php echo $d["graph"]["key"]."Data" ?> = <?php echo ( isset( $d["graph"]['data'] ) ) ? json_encode( $d["graph"]["data"] ) : "null" ?>;
		        console.log('url', 'graphs data', '<?php echo $d["graph"]["key"] ?>Data',<?php echo $d["graph"]["key"] ?>Data);
		<?php }
		} ?>
	var targetDataSet = null;
	jQuery(document).ready( function() { 
		<?php  foreach ($blocks as $id => $d) {
	        if( isset($d["graph"]) ) { ?>
	            console.log('url graphs1',baseUrl+'<?php echo $d["graph"]["url"]?>'+"/id/<?php echo $d["graph"]["key"] ?>");
	            ajaxPost('#<?php echo $id?>', baseUrl+'<?php echo $d["graph"]["url"]?>'+"/id/<?php echo $d["graph"]["key"] ?>", null, function(){},"html");
	    <?php }
	    } ?>
	var poiListTags = <?= json_encode($tagsPoiList)?>;
	
	console.log(" poiListTags", poiListTags);
	var newListP =  [];
	
	$.each(poiListTags,function(kT, vT){

			var nElt = {
				text : vT.text,
				weight : vT.weight,
				link : vT.link,
				handlers: {
					click: function(e) {
						mylog.log("notragora here ", e);
						addItemsToSly(e.target.dataset.tag);
					}
				},
			};
			newListP.push(nElt);
		

	});
	$('#canvas').jQCloud(newListP, {
	    height: 500,

	    autoResize: true,
	    shape: 'rectangular',
	    colors: ["#8ABF32", "#1aa5b7", "#005E6F"],
	    fontSize: { 
		    from: 0.1,
		    to: 0.02
		}
	});
	
});

</script>