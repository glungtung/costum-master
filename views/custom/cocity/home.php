<style >
  #bg-homepage{
    width: 100%;
    border-top: 1px solid #ccc;
  }

  .saveTemplate{
    margin-left:4%;
  }
  .toolbar-bottom-adds{
    display: block;
  }
  #menuLeft{
    height: 685px !important;
    padding-top: 0px !important;
  }
  #show-bottom-add {
    top: 45%;
  }
  #menuLeft .toolbar-bottom-adds {
    left: 80% !important;
  }
</style>

<?php 

if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
  $el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );
  $cmsList = PHDB::find(Cms::COLLECTION,array( "parent.".$this->costum["contextId"] => array('$exists'=>1)));
}
$params = [
  "tpl" => $el["slug"],
  "slug"=>$this->costum["slug"],
  "canEdit"=>true,
  "blockCms" => $cmsList,
  "el"=>$el ]; 
  echo $this->renderPartial("costum.views.tpls.tplsEngine", $params,true );
  ?>
