<script type="text/javascript">
	var pageApp=<?php echo json_encode(@$page); ?>;
	var paramsFilter= {
	 	container : "#filters-nav",
	 	results : {
	 		smartGrid : true,
	 		renderView : "directory.elementPanelHtml"
	 	},
	 	loadEvent : {
	 		default : "scroll"
	 	},
	 	filters : {
	 		domainAction : {
	 			view : "selectList",
	 			type : "tags",
	 			name : "Domaine d'action",
	 			event : "tags",
	 			list : costum.lists.domainAction
	 		},
	 		cibleDD:{
	 			view : "selectList",
	 			type : "tags",
	 			name : "Objectif",
	 			event : "tags",
	 			list : costum.lists.cibleDD
	 		},
	 		scope : true,
	 		scopeList : {
	 			name : "Région",
	 			params : {
	 				countryCode : ["FR","RE","MQ","GP","GF","YT"], 
	 				level : ["3"]
	 			}
	 		}
	 	}
	 };

	if(pageApp=="territoires"){
	 	paramsFilter.filters.status={
	 		view : "selectList",
 			type : "filters",
 			field : "source.status.ctenat",
 			name : "Statuts",
 			event : "selectList",
 			keyValue : true,
 			list : [ 
                "<?php echo Ctenat::STATUT_CTER_LAUREAT; ?>", 
                "<?php echo Ctenat::STATUT_CTER_SIGNE; ?>"
            ]
	 	};
	 	paramsFilter.defaults = {
	 		types : [ "projects" ],
	 		filters : {
		 		"category" : "cteR"
			}
		};
	}else if(pageApp=="projects"){
	 	paramsFilter.defaults = {
	 		types : [ "projects" ],
	 		filters : {
		 		"category" : "ficheAction"
			}
		};


		if(typeof contextData != "undefined" && contextData != null &&
			contextData.id != costum.contextId ){
			paramsFilter.defaults.filters["links.projects."+contextData.id] = {
				'$exists' : 1 
			};
		}
	}

	
	jQuery(document).ready(function() {
		filterSearch = searchObj.init(paramsFilter);
	});
</script>