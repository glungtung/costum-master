<?php
/*
if ( !function_exists('renderArrayFormPropertiesHeader') ) {
	function renderArrayFormPropertiesHeader($df, $step,$key,$table=null, $canAdmin=false){
		$props =  (isset($df["properties"])) ? $df["properties"] : array();
		$titleH = (isset($df["title"])) ? @$df["title"] : "";
		echo '<h3 style="color:#16A9B1">'.$titleH.'</h3>';
		echo '<table class="table table-striped table-bordered table-hover directoryTable">';
			echo '<thead>';
			echo '<tr style="border:none">';
			foreach ( $props as $iik => $iiv) {
				echo "<th>".( ( isset($iiv["placeholder"]) ) ? $iiv["placeholder"] : $iik )."</th>";
			}
			echo '</tr>';
			echo '</thead><tbody class="directoryLines">';
	}
}*/
if (!function_exists('renderArrayFormPropertiesHeaderClose')) {
	function renderArrayFormPropertiesHeaderClose(){
		echo "</tbody></table>";
	}
}
if (!function_exists('renderArrayFormPropertiesRows')) {
	function renderArrayFormPropertiesRows($step,$key,$answer , $pos, $canAdmin=false)
	{
		//echo "<br/>renderArrayFormPropertiesRows : ".$step."|".$key;
		//var_dump($answer);
		if(isset($answer) && is_array($answer))
		{
			echo '<tr>';
				$titleComment = $key." ".(intval($pos)+1);
				foreach ($answer as $sa => $sv) {
					echo "<td>";
					if(is_array($sv))
						echo implode(",",$sv);
					else
						echo $sv;
					if(in_array($sa, ["poste", "qui"]))
						$titleComment.=" : ".$sv;
					echo "</td>";
				}
			echo '</tr>';
		}
	}
}

//Yii::import("parsedown.Parsedown", true);
//$Parsedown = new Parsedown();
//var_dump($answers); exit;?>
<style type="text/css">
	
h1 {
	font-size: 24px;
}
.img-band{
margin-top: -50px;
}
.titleEncart{
width: 80%;border:1px solid #616161;margin:auto;text-align:center;
}
h3{
	color:#50b796;
	text-transform:uppercase;
}

.comment-text{
	white-space: pre-line;
}
.noborder{
	border:1px solid white;
}
.blue{
	color : #195391;
}

.lightgreen{
	color : #a9ce3f;
}

.darkgreen{
	color : #4db88c;
}

.body { 
	font-family: Arial,Helvetica Neue,Helvetica,sans-serif; 
}

table {
    color: #616161;
	text-align: center;
}

table td, table th {
    border: 1px solid #616161;
}
table th {
	border-width: 2px;
}
table tr td{
	font-size: 10pt;
}
table tr th{
	font-size: 12px;
	font-weight: bold;
}
table.no-boder th, table.no-boder td{
	border:0px solid white;
}

	
h1 {
	font-size: 24px;
}

.blue{
	color : #195391;
}

.lightgreen{
	color : #a9ce3f;
}

.darkgreen{
	color : #4db88c;
}

.body { 
	font-family: Arial,Helvetica Neue,Helvetica,sans-serif; 
}

.pdftittlecolor {
	color: #195391 ;
}

.table-bordered {
    border: 1px solid #ddd;
}

.table {
    width: 100%;
    max-width: 100%;
    margin-bottom: 20px;
}

.table-bordered>thead>tr>th, .table-bordered>thead>tr>td {
    border-bottom-width: 2px;
}

.table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td {
    border: 1px solid #ddd;
}

.calendar-table tr th:nth-child(1){
    width: 100px;
}

	.calendar-table {
		padding: 10px;
		width: 100%;
		table-layout: fixed;
		border-collapse: collapse;
		font-size: 15px;
	}

	.calendar-table th {
		border-bottom: 2px solid #195391; 
		text-align: center
		color : #59bba7;
	}

	.calendar-table tr th {
		color : #59bba7;
	}

	.calendar-table td {
		border-bottom:  2px solid #ddd;
	}

	.calendar-table tr th:nth-child(1){
       width: 100px;
    }

    .calendar-table tr td:nth-child(1){
       width: 100px;
       border-right: 3px solid #ddd;
    }

    .calendar-table tr td+td{
       font-size: 15px;
    }

    .calendar-table tr th:last-child{
       width: 50px;
    }

    .calendar-table .col-bordered {
    	border-right: 3px solid #ddd;
    }

    .specialword {
    	color: grey;
    	font-weight: bold;
    }


</style>

<?php 
	$server = ((isset($_SERVER['HTTPS']) AND (!empty($_SERVER['HTTPS'])) AND strtolower($_SERVER['HTTPS'])!='off') ? 'https://' : 'http://').$_SERVER['HTTP_HOST'];
	$imgHead =  $server.Yii::app()->getModule("costum")->assetsUrl."/images/ctenat/city.png" ;
?>
<div class="col-xs-12 img-band">
	<img class="img-responsive" src="<?php echo $imgHead ?>"/>
</div>
<div class="col-xs-8 titleEncart" >
	<h2 style="color:#2c6aa1;text-transform:uppercase;"><?php echo $title ?></h2>
	<h4 style="">COPIL du <?php echo date("d/m/Y",strtotime($dateComment)) ?></h4>
	<h4 style=""><?php echo $participants ?></h4>
	<h4 style="color:#2c6aa1;text-transform:uppercase;">Action : <?php echo $actionName ?></h4>
	<h4 style="color:#2c6aa1;text-transform:uppercase;">Territoire : <?php echo $parentName ?></h4>
</div>
<?php if(isset($comment)){ 
		if(isset($subKey)){ ?>
			<h3 style="color:#16A9B1;">Commentaire global de la validation</h3>
			<div class="col-xs-12">
	
			<?php if($subKey=="copilReunionFinalisation" && isset($validation["cter"])){
				if(isset($validation["cter"]["description"])){ ?>
					<span><?php echo $validation["cter"]["description"] ?></span>
				<?php }
			}
			if($subKey=="copilCTENat" && isset($validation["ctenat"])){
				if(isset($validation["ctenat"]["description"])){ ?>
					<span><?php echo $validation["ctenat"]["description"] ?></span>
				<?php }
			}
			if($subKey=="copilEtatRegional" && isset($validation["etat"])){
				if(isset($validation["etat"]["description"])){ ?>
					<span><?php echo $validation["etat"]["description"] ?></span>
				<?php }
			} ?> 
			</div>
		<?php }

	?>
	<h3 style="color:#16A9B1;">Commentaire général</h3>
	<div class="col-xs-12 commentPod">
		<?php 
			//$ct=0;
			//foreach($dataClAns as $q => $a){
				echo Ctenat::buildLineCopilComment($idAnswer, "", $dateComment, "murir", "", "");
			//	$ct++;
			//} ?>
	</div>
<?php } ?>
<?php
$params = array(
	//"author" => @$answer["name"],
	"idAnswer"=>$idAnswer,
	"answer" => $answer,
	//"saveOption"=>"F",
	//"urlPath"=>$res["uploadDir"],
	"title" => $title,
	"participants" => $participants,
	"dateComment" => $dateComment,
	"subject" => "CTE",
	//"custom" => $form["custom"],
	"footer" => true,
	"tplData" => "cteDossier",
	"form" => $form,
	"forms" => $forms,
	"canAdmin"=>null
);
if(isset($comment)){
	$params["comment"]=true;
}
if(isset($answer['answers']))
	$params["answers"] = $answer['answers'];


echo $this->renderPartial('costum.views.custom.ctenat.pdf.murir', $params, true);

?>