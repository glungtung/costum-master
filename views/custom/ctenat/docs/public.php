<div class="col-xs-12">
    <h1 class="text-center">Acteurs Publics</h1>

    <div class="col-xs-12 margin-top-20">
        <?php 
        
        $params = [
            "canEdit" => Authorisation::isCostumAdmin(),
            "cmsList" => PHDB::find( Cms::COLLECTION, [
                                "parent.".$this->costum["contextId"] => ['$exists'=>1], 
                                "parent.".$this->costum["contextId"].".type"=>$this->costum["contextType"],
                               "page"=>"public"] ),
            "listSteps" => ["one","two","three","four","five","six"]
        ];

        foreach ($params['cmsList'] as $id => $value) {
            $documents  = PHDB::find(Document::COLLECTION, ['id' => $id, "type" => Cms::COLLECTION ]);
            if( count( $documents ) )
            {
                $params['cmsList'][$id]["documents"] = [];
                foreach ( $documents as $key => $doc ) 
                {
                    $params['cmsList'][$id]["documents"][] = [
                        "name" => $doc['name'], 
                        "doctype" => $doc['doctype'], 
                         "path" => Yii::app()->baseUrl."/".Yii::app()->params['uploadUrl'].$doc["moduleId"]."/".$doc["folder"]."/".$doc['name']
                    ];
                }
            }
        }
        
        //var_dump($params);exit;
        echo $this->renderPartial("costum.views.tpls.blockCms.wizard",$params,true);
        ?>
    </div>
</div>
