<div class="col-xs-12" style="text-align: justify;">
	
	<h3>Informations Éditeurs</h3><br/>
<h4>Service gestionnaire</h4> <br/>

Ministère de la Transition écologique<br/>
Délégation au développement durable<br/>
Equipe nationale des contrats de transition écologique<br/>
244, Boulevard Saint Germain<br/>
75007 Paris<br/>
France<br/>
Téléphone : +33 1 40 81 21 22<br/>
<br/>
<h4>Direction et coordination :</h4> Fournis Sandrine et Millard Frédérique.<br/>
<br/>
<!-- <h4>Gestion du portail :</h4> A supprimer ?<br/>
<br/> -->
<h4>Gestion éditoriale :</h4> Fournis Sandrine, Millard Frédérique, Juliette Maitre <br/>
<br/>
<h4>Développement web :</h4> <br/>
Open Atlas<br/>
SIRET : 513 381 830 00027<br/>
56 rue Andy, 97460, ST PAUL
<br/><br/>
<h4>Graphisme : </h4><br/>
Open Atlas<br/>
SIRET : 513 381 830 00027<br/>
56 rue Andy, 97460, ST PAUL<br/>
<br/>

<h4>Contact - Saisir les services de l'État</h4>

<a href="http://contact.ecologique-solidaire.gouv.fr/" target='_black' >Formulaire de saisine des services de l'État</a><br/>


<h3>Informations techniques</h3><br/>
<h4>Site Internet :</h4>
Site réalisé sous Communecter.org<br/><br/>
<h4>Hébergement du site</h4>
OVH SAS <br/>
2, rue Kellermann, 59100 Roubaix.<br/>
Conception et développement<br/><br/>
<h4>Réalisation technique et graphique :</h4>
Open Atlas<br/>
SIRET : 513 381 830 00027<br/>
56 rue Andy, 97460, ST PAUL


<h3>Propriété intellectuelle</h3><br/>

Les contenus présentés sur ce site sont soumis à la législation relative au droit des informations publiques et sont couverts par le droit d'auteur. Toute réutilisation des vidéos, des photographies, des créations graphiques, des illustrations et des lexiques, ainsi que de l'ensemble des contenus éditoriaux produits pour l'animation éditoriale du site est conditionnée à l'accord de l'auteur. Le répertoire des informations publiques du ministère de la Transition écologique précise la charte de réutilisation des informations publiques.<br/><br/>

Conformément au droit public de la propriété intellectuelle et notamment selon l'article L122-5 du Code de la propriété intellectuelle, les ''documents officiels'' sont librement réutilisables :<br/><br/>
	<ul style="margin-left: 35px;">
		<li>les discours, les dossiers de presse et les communiqués,</li>
		<li>les circulaires, directives et autres documents règlementaires,</li>
		<li>les formulaires CERFA.</li>
	</ul>

La réutilisation non commerciale, et notamment pédagogique, est autorisée à la condition de respecter l'intégrité des informations et de n'en altérer ni le sens, ni la portée, ni l'application et d'en préciser l'origine et la date de publication.<br/>

Les informations ne peuvent être utilisées à des fins commerciales ou promotionnelles sans l'autorisation expresse et l'obtention d'une licence de réutilisation des informations publiques. Est considérée comme réutilisation à des fins commerciales ou promotionnelles, l'élaboration à partir des informations publiques, d'un produit ou d'un service destiné à être mis à disposition de tiers, à titre gratuit ou onéreux.<br/><br/>
L'utilisation des marques déposées utilisées sur ce site sur tout autre support ou réseau est interdite.
<br/><br/>


<h3>Liens hypertextes</h3>

Tout site public ou privé est autorisé à établir, sans autorisation préalable, un lien vers les informations diffusées par le ministère de la Transition écologique. En revanche, les pages du site ne doivent pas être imbriquées à l’intérieur des pages d’un autre site.<br/><br/>
L’autorisation de mise en place d’un lien est valable pour tout support, à l’exception de ceux diffusant des informations à caractère polémique, pornographique, xénophobe ou pouvant, dans une plus large mesure porter atteinte à la sensibilité du plus grand nombre.<br/><br/>

<h3>Crédits photographiques</h3>
Les photos présentes sur ce site proviennent de sources différentes et sont fournies par les utilisateurs et restent de leur propriété.<br/>
Toute image publiée sur le site est automatiquement considérée comme CC-BY-SA.<br/><br/>

<b>Photographes du ministère :</b> Arnaud Bouissou, Damien Valente, Manuel Bouquet, Damien Carles.Contributions : Olivier Brosseau<br/><br/>

<a href="http://contact.ecologique-solidaire.gouv.fr" target='_black' >Formulaire de saisine des services de l'État</a><br/>

<h3>Responsabilité du ministère</h3>

Les informations proposées sur ce site le sont au titre de service rendu au public. Malgré tout le soin apporté à l’actualisation des textes officiels et à la vérification des contenus, les documents mis en ligne ne sauraient engager la responsabilité du ministère de la Transition écologique.<br/><br/>

Les informations et/ou documents disponibles sur ce site sont susceptibles d’être modifiés à tout moment, et peuvent faire l’objet de mises à jour.<br/><br/>

Le ministère de la Transition écologique ne pourra en aucun cas être tenu responsable de tout dommage de quelque nature qu’il soit résultant de l’interprétation ou de l’utilisation des informations et/ou documents disponibles sur ce site.<br/><br/>

<h3>Données personnelles</h3>
Lorsque des données présentes sur ce site ont un caractère nominatif, les utilisateurs doivent en faire un usage conforme aux réglementations en vigueur et aux recommandations de la Commission nationale de l'informatique et des libertés (CNIL).<br/><br/>
Conformément à la loi Informatique et Liberté 78-17 du 6 janvier 1978 modifiée, vous disposez d'un droit d'opposition (art. 38), d'accès (art. 39), de rectification ou de suppression (art. 40) des données qui vous concernent. Vous pouvez exercer ce droit en vous adressant au ministère de la Transition écologique.<br/><br/>
Ce droit s'exerce, en justifiant de son identité :<br/><br/>
<b>Par voie postale :</b><br/><br/>
Ministère de la Transition écologique<br/>
Délégation au développement durable<br/>
Equipe nationale CTE<br/>
244, Boulevard Saint Germain<br/>
75007 Paris<br/>
France<br/><br/>
<b>Via le formulaire de saisine des services de l'État :</b><br/>
<a href="http://contact.ecologique-solidaire.gouv.fr" target='_black' >Formulaire de saisine des services de l'État</a><br/><br/>

Toutes les données personnelles qui sont recueillies sont traitées avec la plus stricte confidentialité. En particulier, le ministère de la Transition écologique s’engage à respecter la confidentialité des messages e-mails transmis au moyen d’une messagerie électronique.<br/><br/>
<h4>Les lettres électroniques du ministère</h4>
Les adresses électroniques recueillies dans le cadre des formulaires d'abonnement ne sont utilisées que pour l'envoi des informations pour lesquelles l'usager s'est expressément abonné. Il est possible à tout moment de se désabonner en ligne.<br/><br/>
 
<h3>Informations sur les cookies</h3>
Le site que vous visitez utilise des traceurs (cookies). Ainsi, le site est susceptible d'accéder à des informations déjà stockées dans votre équipement terminal de communications électroniques et d’y inscrire des informations.<br/><br/>
<h4>Mesure d'audience</h3>
La navigation des usagers sur le site www.ecologique-solidaire.gouv.fr (pages d'entrée, durée et sources des visites, fréquence des visites, etc.) sont analysé par enregistrement en base de donnée.<br/><br/>
<h4>Cookie interne</h4>
L’usage des cookies sert à amélioré l’expérience utilisateur et l’ergonomie et au fonctionnement des requetes pour leur aspect géographique.<br/><br/>
<h4>Paramétrage</h4>
Ces traceurs ne peuvent pas, techniquement, être désactivés depuis le site. Vous pouvez néanmoins vous opposer à l'utilisation de ces traceurs, exclusivement en paramétrant votre navigateur. Ce paramétrage dépend du navigateur que vous utilisez, mais il est en général simple à réaliser : en principe, vous pouvez soit activer une fonction de navigation privée soit uniquement interdire ou restreindre les traceurs (cookies).<br/><br/>
Attention, il se peut que des traceurs aient été enregistrés sur votre périphérique avant le paramétrage de votre navigateur : dans ce cas il est possible de supprimer les cookies depuis les options de votre navigateur.<br/><br/>
<h3>Accessibilité du site</h3>
Le site de la plateforme CTE  utilise des outils Open Source, et a été conçu pour être pleinement accessible, en accord avec les principes d'accessibilité de contenu web.<br/><br/>
Nous utilisons HTML 5 et CSS 3 conformément aux spécifications édictées par le W3C car nous accordons une grande importance à la facilité d'utilisation et à l'accessibilité. Nous avons pour philosophie de nous adresser à tous nos visiteurs quels qu'ils soient, et dans cette optique nous appliquons du mieux possible le principe de séparation entre le code HTML et la présentation CSS.



<a href="http://references.modernisation.gouv.fr/rgaa/" target='_black' >Référentiel Général d'Accessibilité pour les Administrations (references.modernisation.gouv.fr)</a><br/>

<h3>Disponibilité du site</h3>
L’éditeur s’efforce de permettre l’accès au site 24 heures sur 24, 7 jours sur 7, sauf en cas de force majeure ou d’un événement hors du contrôle du ministère de la Transition écologique, et sous réserve des éventuelles pannes et interventions de maintenance nécessaires au bon fonctionnement du site et des services.<br/><br/>
Par conséquent, le ministère de la Transition écologique ne peut garantir une disponibilité du site et/ou des services, une fiabilité des transmissions et des performances en terme de temps de réponse ou de qualité. Il n’est prévu aucune assistance technique vis-à-vis de l’utilisateur que ce soit par des moyens électronique ou téléphonique.<br/><br/>
La responsabilité de l’éditeur ne saurait être engagée en cas d’impossibilité d’accès à ce site et/ou d’utilisation des services.<br/><br/>
Le ministère de la Transition écologique peut être amené à interrompre le site ou une partie des services, à tout moment sans préavis, le tout sans droit à indemnités. L’utilisateur reconnaît et accepte que le ministère de la Transition écologique ne soit pas responsable des interruptions, et des conséquences qui peuvent en découler pour l’utilisateur ou tout tiers.<br/><br/>


<h3>Droit applicable</h3>

Quel que soit le lieu d’utilisation, le présent site est régi par le droit français. En cas de contestation éventuelle, et après l’échec de toute tentative de recherche d’une solution amiable, les tribunaux français seront seuls compétents pour connaître de ce litige.<br/><br/>
Pour toute question relative aux présentes conditions d’utilisation du site, vous pouvez nous écrire à l’adresse suivante :<br/><br/>
Ministère de la Transition écologique<br/>
Délégation au développement durable<br/>
Equipe nationale CTE <br/>
244, Boulevard Saint Germain<br/>
75007 Paris<br/>
France<br/>


<h3> Acceptation des conditions d’utilisation </h3>
L’utilisateur reconnaît avoir pris connaissance des conditions d’utilisation, au moment de sa connexion vers le site du ministère et déclare expressément les accepter sans réserve.<br/><br/>

<h3> Modifications des conditions générales d’utilisation</h3>
Le ministère de la Transition écologique se réserve la possibilité de modifier, à tout moment et sans préavis, les présentes conditions d’utilisation afin de les adapter aux évolutions du site et/ou de son exploitation.





</div>