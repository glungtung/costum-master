
<style type="text/css">


.content {
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 0 auto;
  /*max-width: 500px;*/
  /*padding: 35vh 20px 20px 20px*/
}

.logo {
  max-width: 300px;
}

p {
  /*font-family: sans-serif;*/
  text-align: center;
}

.message {
  margin-top: 20px;
  margin-bottom: 35px;
  font-size: 30px;
  position: relative;
}

.message:after {
        content: '';
        position: absolute;
        margin: auto;
        right: 0;
        bottom: 0;
        left: 0;
        width: 50%;
        height: 2px;
        background-color: rgb(142, 191, 39);
      }

.message-text {
    /*margin: 5px;
    padding: 5px;*/
}

.message-text .text {
    font-weight: bold;
    text-align: center;
    font-size: 20px;
}

.btn-start {
    background-color: transparent;
    border: 2px solid #7d98c7;
    color: #1e56a2;
    border-bottom-left-radius: 10px;
    font-weight: bold;
}

.btn-start:hover {
    background-color: #7d98c7;
    border: 2px solid #7d98c7;
    color: #fff;
    border-bottom-left-radius: 10px;
    font-weight: bold;
}

.s-center {
    margin: 0px;
    text-align: center;
    width: 100%;
    font-size: 40px;
    margin-bottom: 10px;
}

.devenirmembre {
    margin-bottom: 30px;
}
</style>

<?php
    $adminStatus = false;
    $membre = false;
    $partenaire = false;
    $title = "Devenir partenaire";

    if (isset($this->costum["contextType"]) and isset($this->costum["contextId"])) {
        $communityLinks = Element::getByTypeAndId($this->costum["contextType"],$this->costum["contextId"]);
    }
    
    $cetacelist = PHDB::findOne(Lists::COLLECTION, array('name' => 'cetaces'));


    if (isset($this->costum["admins"])) {
        if(is_array($this->costum["admins"])){
            foreach ($this->costum["admins"] as $key => $value) {
                if($key == Yii::app()->session["userId"]){
                    $adminStatus = true;
                }

            }
        }
    }

    if(isset(Yii::app()->session["userId"]) and !$adminStatus and !$partenaire){
        $membre = true;
    }

    if (isset($communityLinks["links"]["members"])) {
      foreach ($communityLinks["links"]["members"] as $memberId => $memberValue) {
                  if(isset(Yii::app()->session["userId"])){
                      if($memberId != ""){
                        if($memberId == Yii::app()->session["userId"]){
                            if(isset($memberValue["roles"])){
                            foreach ($memberValue["roles"] as $rolesId => $rolesValue) {
                              if($rolesValue == "Partenaire"){
                                  $partenaire = true;
                              }
                            }
                        }
                        }
                      }
                  }
                }
    }

    $message = "En devenant Membre de la communauté, votre profil vous donne accès à :";
    $submessage = "";
    $btnAction = '<button class="btn btn-start b-partenaire">S\'inscrire à communtecter</button>';

    if ($adminStatus or $partenaire) {
        $message = "Vous êtes déjà membre";
        $submessage = "";
        $btnAction = "";
    } elseif ($membre and !$adminStatus and !$partenaire) {
        $message = "Vous êtes déjà membre";
        $submessage = "";
        $btnAction = "";
    } elseif (!$membre and !$adminStatus and !$partenaire) {
        $submessage = "";
        $btnAction = '<button class="btn btn-start devenirmembre">s\'inscrire</button>';
    }


?>

<!-- <div class="object">
    <div class="object-rope"></div>
    <div class="object-shape">
      Bientôt <span class="soon">disponible</span>
    </div>
</div> -->

<div class="content">
  <img class="logo" src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/sommom/cdv-cedtm-blanc.png"/>

  <p class="message"><?php echo $message ?></p>
</div>

<div class="content">
     <?php echo $btnAction ?>
</div>

<div class="col-md-4">
    <div class="message-text">
      <div class="col-md-12"><i class="fa fa-file-text s-center" style="color: burlywood"></i></div>
      <div class="text col-md-12">L’ensemble des informations disponibles sur l’activité d’observation des cétacés sur chaque territoire, téléchargeable en fichier PDF.</div>
    </div>
</div>

<div class="col-md-4">
    <div class="message-text">
      <div class="col-md-12"><i class="fa fa-comments-o s-center" style="color: #8ebf27"></i></div>
      <div class="text col-md-12">La possibilité de communiquer avec les autres membres du réseau d’acteurs de l’observation des cétacés dans les territoires français et partenaires du projet « SOMMOM », directement via la messagerie instantanée de l’outil « Communecter ».</div>
    </div>
</div>

<div class="col-md-4">
    <div class="message-text">
      <div class="col-md-12"><i class="fa fa-group s-center" style="color: darkgrey"></i></div>
      <div class="text col-md-12">L’ensemble des fonctionnalités de l’outil « Communecter » (événements, réseaux, projets, chat…), et la possibilité d’y contribuer.</span>
    </div>
</div>


<script type="text/javascript">
  setTitle("SOMMOM : Membres de la communauté");
  
  jQuery(document).ready(function() {
    $(".devenirmembre").off().on("click",function() {
      $('#modalRegister').modal("show");
    });
  });
</script>