<div id="homeSommom" class="col-xs-12">
        <div style="width: 100%; position: fixed;" id="loaderSommom">
            
        </div>
</div>
<?php 

$adminStatus = false;

if (isset($this->costum["admins"])) {
    if(is_array($this->costum["admins"])){
        foreach ($this->costum["admins"] as $key => $value) {
            if($key == Yii::app()->session["userId"]){
                $adminStatus = true;
            }
        }
    }
}

?>

<script type="text/javascript">
    //to edit costum page pieces
    jQuery(document).ready(function() {
        coInterface.showLoader("#loaderSommom");
    });

    <?php if($adminStatus){ ?>
        $("#btn-nextcloud").remove();
        $("#menuLeft").append(`
            <a id="btn-nextcloud" class="text-center text-dark menu-app hidden-xs btn btn-link menu-button btn-menu btn-menu-tooltips pull-left" href="https://conextcloud.communecter.org/" style="text-decoration : none;" target="_blank"> 
                <i class="fa fa-file-text-o" aria-hidden="true"></i>
                <span class="tooltips-menu-btn" style="display: none;">Partage de documents</span>
            </a>
        `);
    <?php } ?>


    if ( location.hash == "#" || location.hash == "#welcome" || location.hash == "") {
    	// window.location.replace(baseUrl+"/costum/co/index/slug/sommom#dashboard");
        urlCtrl.loadByHash("#dashboard");
    }
</script>
