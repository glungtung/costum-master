<?php 
$cssJS = array(
    // '/plugins/gridstack/css/gridstack.min.css',
    // '/plugins/gridstack/js/gridstack.js',
    // '/plugins/gridstack/js/gridstack.jQueryUI.min.js'

);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl);


HtmlHelper::registerCssAndScriptsFiles(array( 
  '/css/graphbuilder.css',
  '/js/form.js'
  ), Yii::app()->getModule( Survey::MODULE )->getAssetsUrl() );
?>

<style type="text/css">
    .pageContent{
        background-color: #ebebeb;
        font-family: 'Raleway', sans-serif !important;
    }
    .ocecoform-body{
        background-color: #ebebeb;
        font-family: 'Raleway', sans-serif;
    }

    .ocecoform-body .tilelabel{
        font-weight: bold !important;
    }

    .squareTile{
        width: 20%;
        height: 180px !important;
    }

    .squareTileL{
        width: 20%;
        height: 260px !important;
    }

    .headerTile{
        width: 70%;
        height: 180px !important;
    }

    .flex1{
        flex: 1!important;
    }

    .flex2{
        flex: 2!important;
    }

    .flex8{
        flex: 8!important;
    }

    .flex3{
        flex: 3!important;
    }


    .ocecotitle{
        display : flex;
        -webkit-box-pack: center!important;
        -ms-flex-pack: center!important;
        justify-content: center!important;
    }

    .pdt15{
        padding-top: 25px;
    }

    .outeroceco {
      position: relative;
    }
    canvas {
      position: absolute;
    }
    .ocecopercent {
     position: absolute;
        left: 50%;
        transform: translate(-50%, 0);
        font-size: 20px;
        font-weight: bold;
        top: 60%;
        /*bottom: 0px;*/
    }
    .progress {
        border-radius: 20px;
    }
    .progress-bar {
        background-color: #61cd73;
    }
    .dividerT {
      width: 1px;
      margin: 6px 0;
      background: #dddddd;
    }

    .divitemheader{
        display: flex;
        flex-direction: row;
        flex: 1;
    }

    .itemheaderTile {
      flex: 1 auto;
      display: flex;
      flex-direction: column;
      padding-top: 15px;
    }

    .ocecotitle i{
      font-size: 50px;
    }

    .ocecotitle.step{
        font-size: 20px;
        font-weight: bold;
    }

    .ocecotitle.step2{
        font-size: 17px;
        font-weight: bold;
    }

    .ocecotitle .state{
        font-size: 15px;
    }

    .ocecotitle i.success, .ocecotitle .state.success{
      color: #61cd73;
    }

    .ocecotitle i.info{
      color: #61cd73;
    }

    .ocecotitle i.warning{
      color: #ecb22f;
    }

    .fifi{
      height: 50px;
      width: 100px;
    }

    .fifi .ocecoouter{
        position: relative !important;
    }

    .ocecopercentfi {
        position: absolute;
        left: 50%;
        transform: translate(-50%, 0);
        font-size: 15px;
        font-weight: bold;
        top: 12px;
        /*bottom: 0px;*/
    }

    .successSquare{
        background-color: #e5ffe5;
    }

    .itemheaderTileI {
        padding-top: 10px !important;
    }

</style>
<!-- #ebebeb -->
<div class="ocecoform-body">
    <div class="col-md-12" style="margin-bottom: 40px; ">
        <div class="col-md-offset-2 col-md-8">
            <h1 class="" style="text-transform: none; text-align: center">Observatoire des process Opal </h1>
        </div>
    </div>

    <div class="col-md-12" id="bodygraph" >
        <div class="surveys grid survey-grid" style="display: flex;background-color: #ebebeb">

              <!-- change to dymamic  -->
              <div id="" class="survey-item smallTile flex1 visiblehovercontainer grid-stack-item" data-gs-width="4" data-gs-height="2" style="height: 100px;" id="">
                    <div class="grid-stack-item-content" >
                        <span class="survey-name tilelabel responsedoctitle" style="">
                          <span class="lstick label"></span>
                              nombre de proposition validée
                        </span>

                        <div id="nombrePropValidee" style="    font-family: 'Nunito', 'Raleway', sans-serif; font-size: 25px; color: #74b976;">
                          14
                        </div>
                    </div>
              </div>

              <div class="survey-item smallTile flex1 visiblehovercontainer grid-stack-item" data-gs-width="4" data-gs-height="2" style="height: 100px;" id="">
                    <div class="grid-stack-item-content" >
                        <span class="survey-name tilelabel responsedoctitle" style="">
                          <span class="lstick"></span>
                              nombre de proposition financée
                        </span>

                        <div id="" style="    font-family: 'Nunito', 'Raleway', sans-serif; font-size: 25px; color: #74b976;">
                          10
                        </div>
                    </div>
              </div>

              <div class="survey-item smallTile flex1 visiblehovercontainer grid-stack-item" data-gs-width="4" data-gs-height="2" style="height: 100px;" id="">
                    <div class="grid-stack-item-content" >
                        <span class="survey-name tilelabel responsedoctitle" style="">
                          <span class="lstick "></span>
                              nombre de tache en cours
                        </span>

                        <div id="" style="    font-family: 'Nunito', 'Raleway', sans-serif; font-size: 25px; color: #74b976;">
                          0
                        </div>
                    </div>
              </div>
        </div>
        <div class="surveys grid survey-grid" style="display: flex;background-color: #ebebeb">

              <!-- change to dymamic  -->
              <div class="survey-item squareTile flex1 visiblehovercontainer grid-stack-item" data-gs-width="4" data-gs-height="2" id="">
                    <div class="grid-stack-item-content" >
                        <div class="tilelabel ocecotitle" style="">
                              Progression
                        </div>
                        <div class="tilelabel ocecotitle" style="">
                              globale
                        </div>

                        <div id="overallProgressChart" class="pdt15">
                        </div>
                    </div>
              </div>

              <div class="survey-item headerTile flex8 visiblehovercontainer grid-stack-item" data-gs-width="4" data-gs-height="2" style="height: 100px;" id="">
                    <div class="grid-stack-item-content" >
                        <div class="" style="">
                          <div class="progress">
                              <div class="progress-bar" role="progressbar" aria-valuenow="72"
                              aria-valuemin="0" aria-valuemax="100" style="width:69%">
                                <span class="sr-only">79% Complete</span>
                              </div>
                            </div>
                        </div>

                        <div id="" class="divitemheader">
                            <div class="itemheaderTile">
                                <div class="ocecotitle step"> Proposer</div>
                                <div class="ocecotitle"><i class="fa fa-3 fa-check-circle success" aria-hidden="true"></i></div>
                                <div class="ocecotitle state success">Completé</div>
                            </div>
                            
                            <div class="dividerT"></div>
                                
                            <div class="itemheaderTile">
                                <div class="ocecotitle step"> Décider</div>
                                <div class="ocecotitle"><i class="fa fa-3 fa-check-circle success" aria-hidden="true"></i></div>
                                <div class="ocecotitle state success">Completé</div>
                            </div>

                            <div class="dividerT"></div>

                            <div class="itemheaderTile">
                                <div class="ocecotitle step"> Financer</div>
                                <div class="ocecotitle" style="position: relative;"><div class="fifi" id="headerFinancerChart"></div></div>
                                <div class="ocecotitle state success">En cours</div>
                            </div>

                            <div class="dividerT"></div>
                            
                            <div class="itemheaderTile">
                                <div class="ocecotitle step"> Suivre</div>
                                <div class="ocecotitle"><i class="fa fa-3 fa-circle warning" aria-hidden="true"></i></div>
                                <div class="ocecotitle state success">En attente</div>
                            </div>

                        </div>
                    </div>
              </div>

              <div class="survey-item squareTile successSquare flex1 visiblehovercontainer grid-stack-item" data-gs-width="4" data-gs-height="2" id="">
                    <div class="grid-stack-item-content" >
                        <div class="tilelabel ocecotitle" style="">
                              Date de
                        </div>
                        <div class="tilelabel ocecotitle" style="">
                              démarrage
                        </div>

                        <div class="tilelabel ocecotitle">
                                <i class="fa fa-3 fa-flag-checkered success" aria-hidden="true"></i>
                        </div>

                        <div class="itemheaderTile">
                                <div class="ocecotitle state success">25 Fev 2021</div>
                                <div class="ocecotitle step"> 26j</div>
                        </div>
                        </div>
                    </div>
        </div>
         <div class="surveys grid survey-grid" style="display: flex;background-color: #ebebeb">

              <!-- change to dymamic  -->

              <div class="survey-item squareTileL flex2 visiblehovercontainer grid-stack-item" data-gs-width="4" data-gs-height="2" style="height: 100px;" id="">
                    <div class="grid-stack-item-content" >
                        <div class="tilelabel ocecotitle" style="">
                              High task financed
                        </div>

                        <div id="" class="">
                            <div class="itemheaderTile">
                                   
                                <div class="itemheaderTile">
                                    <div class="ocecotitle state success"><span style="flex: 1;">Coder </span> <span class="step pull-left ocecotitle step2">24 000</span></div>
                                </div>

                                <div class="itemheaderTile">
                                    <div class="ocecotitle state success"><span style="flex: 1;">Design </span> <span class="step pull-left ocecotitle step2">24 000</span></div>
                                </div>

                                <div class="itemheaderTile">
                                    <div class="ocecotitle state success"><span style="flex: 1;">Debug </span> <span class="step pull-left ocecotitle step2">54 000</span></div>
                                </div>

                                <div class="itemheaderTile">
                                    <div class="ocecotitle state success"><span style="flex: 1;">Design </span> <span class="step pull-left ocecotitle step2">24 000</span></div>
                                </div>
                                
                        </div>
                    </div>
                </div>
              </div>
              <div class="survey-item squareTileL flex3 visiblehovercontainer grid-stack-item" data-gs-width="4" data-gs-height="2" style="height: 100px;" id="">
                    <div class="grid-stack-item-content" >
                        <div class="tilelabel ocecotitle" style="">
                              Financement
                        </div>

                                <div class="itemheaderTile itemheaderTileI">
                                    <div class="ocecotitle state success"><span style="flex: 1;">financé  </span> <span class="step pull-left ocecotitle step2">54 000</span></div>
                                </div>

                                <div class="itemheaderTile itemheaderTileI">
                                    <div class="ocecotitle state success"><span style="flex: 1;">Reste  </span> <span class="step pull-left ocecotitle step2">20 000</span></div>
                                </div>

                        <div id="" class="">

                            <div id="" class="">
                                <div style="
                                    height: 180px;
                                    width: 260px;
                                " class="" id="fibarChart">
                                    
                                </div>
                               
                            </div>
                        </div>

                    </div>
              </div>
              <div class="survey-item squareTileL flex3 visiblehovercontainer grid-stack-item" data-gs-width="4" data-gs-height="2" style="height: 100px;" id="">
                    <div class="grid-stack-item-content" >
                         <div class="tilelabel ocecotitle" style="">
                              Tache
                        </div>
                        <div class="itemheaderTile itemheaderTileI">
                                    <div class="ocecotitle state success"><span style="flex: 1;">All  </span> <span class="step pull-left ocecotitle step2">22</span></div>
                                </div>

                                <div class="itemheaderTile itemheaderTileI">
                                    <div class="ocecotitle state success"><span style="flex: 1;">To do  </span> <span class="step pull-left ocecotitle step2">95</span></div>
                                </div>

                        <div id="" class="">

                            <div id="" class="">
                                <div style="
                                    height: 180px;
                                    width: 260px;
                                " class="" id="bartChart">
                                    
                                </div>
                               
                            </div>
                        </div>
                    </div>
              </div>
              <div class="survey-item squareTileL flex1 visiblehovercontainer grid-stack-item" data-gs-width="4" data-gs-height="2" style="height: 100px;" id="">
                         <div class="tilelabel ocecotitle" style="">
                              Type de
                        </div>
                        <div class="tilelabel ocecotitle" style="">
                               Travaux
                        </div>
                         <div class="itemheaderTile itemheaderTileI">
                                    <div class="ocecotitle state success"><span style="flex: 1;">Costum  </span> <span class="step pull-left ocecotitle step2">14</span></div>
                                </div>

                        <div class="itemheaderTile itemheaderTileI">
                            <div class="ocecotitle state success"><span style="flex: 1;">Maintenance  </span> <span class="step pull-left ocecotitle step2">26</span></div>
                        </div>
                        <div id="" class="">
                                <div style="
                                    height: 180px;
                                    width: 260px;
                                " class="" id="pietaChart">
                                    
                                </div>
                        </div>
              </div>
        </div>
        <div class="surveys grid survey-grid" style="display: flex;background-color: #ebebeb">

              <!-- change to dymamic  -->

              <div class="survey-item squareTile flex1 visiblehovercontainer grid-stack-item" data-gs-width="4" data-gs-height="2" style="height: 100px;" id="">
                    <div class="grid-stack-item-content" >
                        <div class="tilelabel ocecotitle" style="">
                              Proposition
                        </div>

                        <div id="" class="">
                            <div class="itemheaderTile">
                                    <div class="ocecotitle state success"><span style="flex: 1;">validated Proposals</span> <span class="label label-success pull-left">24</span></div>

                                   <div class="ocecotitle state success"><span style="flex: 1;">pending decision Proposals</span> <span class="label label-success pull-left">24</span></div>

                                    <div class="ocecotitle state success"><span style="flex: 1;">pending financing Proposals</span> <span class="label label-success pull-left">24</span></div>

                                    <div class="ocecotitle state success"><span style="flex: 1;">Proposals with pending intents no commited</span> <span class="label label-success pull-left">24</span></div>

                                    <div class="ocecotitle state success"><span style="flex: 1;">Proposals pending evaluations and intent proposals</span> <span class="label label-success pull-left">24</span></div>

                                    <div class="ocecotitle state success"><span style="flex: 1;">Completé</span> <span class="label label-success pull-left">24</span></div>

                            </div>
                        </div>
                    </div>
              </div>

              <div class="survey-item squareTile flex1 visiblehovercontainer grid-stack-item" data-gs-width="4" data-gs-height="2" style="height: 100px;" id="">
                    <div class="grid-stack-item-content" >
                        <div class="tilelabel ocecotitle" style="">
                              Projets
                        </div>

                        <div id="" class="">
                            <div class="itemheaderTile">
                                <div class="ocecotitle state success"><span style="flex: 1;">active Projects</span> <span class="label label-success pull-left">24</span></div>
                            </div>
                            <div class="itemheaderTile">
                                <div class="ocecotitle state success"><span style="flex: 1;">finished Projects</span> <span class="label label-success pull-left">24</span></div>
                            </div>
                            <div class="itemheaderTile">
                                <div class="ocecotitle state success"><span style="flex: 1;">pending Projects</span> <span class="label label-success pull-left">24</span></div>
                            </div>
                                <div class="itemheaderTile">
                                    <div class="ocecotitle state success"><span style="flex: 1;">for open projects Tasks distribution</span> <span class="label label-success pull-left">24</span></div>
                                </div>
                        </div>
                    </div>
              </div>
        </div>
    </div>
    
</div>

<script type="text/javascript">
    jQuery(document).ready(function() {

        //overall progress
        ajaxPost('#overallProgressChart', baseUrl+'/graph/co/dash/g/graph.views.co.observatory.halfDoughnut'+"/id/overallProgressChart", null, function(){},"html");

        //header financer
        ajaxPost('#headerFinancerChart', baseUrl+'/graph/co/dash/g/graph.views.co.observatory.doughnut'+"/id/headerFinancerChart", null, function(){},"html");

        // ajaxPost('#fibarChart', baseUrl+'/graph/co/dash/g/graph.views.co.ocecoform.bar'+"/id/fibarChart", null, function(){},"html");
        ajaxPost('#pietaChart', baseUrl+'/graph/co/dash/g/graph.views.co.ocecoform.pie'+"/id/pietaChart", null, function(){},"html");
        ajaxPost('#bartChart', baseUrl+'/graph/co/dash/g/graph.views.co.ocecoform.bart'+"/id/bartChart", null, function(){},"html");
    });

    <?php 
    if (!empty($allData)) {
    ?>

    var allData = <?php echo json_encode($allData) ?>;

    var ocecoform = {
      allData : {},

      tiles : {
        "nombrePropValidee" : {
            type : "html",
            datapath : "validFinal.valid",
            getandsetDatafunc : function(ocecoform){
              var returnObj = countReducedData(allData, "validFinal.valid", "pathtoattribute");
              if(typeof returnObj.validated !== "undefined"){
                  ocecoform.arData["nombrePropValidee"] = returnObj.validated;
              }
            },
        },
        "financementgraph" : {
          type : "graph",
          datapath : "",
          getandsetDatafunc : function(ocecoform){
              var totalfi = sumReducedData(sumReduceData(allData, "financer.amount", "roottoarray"));

              var totaldepense = sumReducedData(sumReduceData(allData, "price", "root"));

              var reste = totaldepense - totalfi;

              var fibarChartData = [
                  {
                      label: 'Financé',
                      backgroundColor: "rgba(0, 0, 0, 1)",
                      borderColor: "rgba(0, 0, 0, 1)",
                      data: [totalfi]
                  }, {
                      label: 'Total à financer',
                      backgroundColor: "rgba(46, 204, 113, 1)",
                      borderColor: "rgba(46, 204, 113, 1)",
                      data: [totaldepense]
                  },
                  {
                      label: 'Reste',
                      backgroundColor: "rgba(221, 221, 221, 1)",
                      borderColor : 'rgba(221, 221, 221, 1)',
                      data: [reste]
                  }
              ];

              var fibarChartDataLabels = ["Total financement", "financé", "Reste"];

              ajaxPost('#fibarChart', baseUrl+'/graph/co/dash/g/graph.views.co.ocecoform.bar'+"/id/fibarChart", null, function(){},"html");

          }
        }
      },

      arData : {
        "nombrePropValidee" : 0,
      },

      init : function(pInit = null){
          var copyFilters = jQuery.extend(true, {}, formObj);
          copyFilters.initVar(pInit);
          return copyFilters;
      },

      initvalues : function(ocecoform){
          $.each( ocecoform.arData , function( dataId, dataValue ) {
              if (typeof ocecoform.arData[dataId] !== null) {
                 ocecoform.tiles[dataId].getandsetDatafunc(ocecoform);
              }
          });
      },

      initviews : function(ocecoform){
          $.each( ocecoform.tiles , function( tilesId, tilesValue ) {
              if (typeof ocecoform.arData[tilesId] !== null) {
                  if(tilesValue.type == "html"){
                      $("#"+tilesId).html(ocecoform.arData[tilesId]);
                  }
              }
          });
      }

    };

    ocecoform.initvalues(ocecoform);
    ocecoform.initviews(ocecoform);

    // mylog.log("azeee", projectsdashboard);
    // alert(JSON.stringify(projectsdashboard));

    // projectsdashboard.initviews(projectsdashboard);

    function updateviewData(arData, chart){
        if (typeof tiles[chart] !== "undefined" && arData[chart]){
            if (typeof tiles[chart]["type"] == "html" ) {
                $('#'.chart).html(arData[chart]);
            }
        }
    }

    function countReducedData(allD,datakey,type) {
        var r;
        if (type == "root") {

            r = allD.reduce(function(sums,entry){
               sums[entry[datakey]] = (sums[entry[datakey]] || 0) + 1;
               return sums;
            },{});
            return r;

        } else if (type == "pathtoattribute"){

            var str = datakey.split(".");
            r = allD.reduce(function(sums,entry){
              if (typeof entry[str[0]] !== "undefined" && typeof entry[str[0]][str[1]] !== "undefined") {
                    sums[entry[str[0]][str[1]]] = (sums[entry[str[0]][str[1]]] || 0) + 1;
                }
               return sums;
            },{});
            return r;

        } else if (type == "pathtoattribu"){

        }
    }

    function sumReduceData(allD,datakey,type) {
        if (type == "root") {

            r = allD.reduce(function(sums,entry){
                if (entry[datakey] != "") {
                    sums[entry[datakey]] = (sums[entry[datakey]] || 0) + 1;
                }
               return sums;
            },{});
            return r;

        } else if (type == "pathtoattribute"){

            var str = datakey.split(".");
            r = allD.reduce(function(sums,entry){
              if (typeof entry[str[0]] !== "undefined" && typeof entry[str[0]][str[1]] !== "undefined") {
                    sums[entry[str[0]][str[1]]] = (sums[entry[str[0]][str[1]]] || 0) + 1;
                }
               return sums;
            },{});
            return r;

        } else if (type == "roottoarray"){

            r = allD.reduce(function(sums,entry){
                var str = datakey.split(".");
                if (typeof entry[str[0]] !== "undefined") {
                    sums2 = sumReduceData(entry[str[0]], str[1], "root");
                    sums = sumObjectsByKey(sums,sums2);
                }
               return sums;
            },{});
            return r;

        }
    }

    function sumObjectsByKey(...objs) {
        return objs.reduce((a, b) => {
            for (let k in b) {
                if (b.hasOwnProperty(k))
                    a[k] = (a[k] || 0) + b[k];
            }
        return a;
        }, {});
    }



    function havAtleastOne(countReducedData, objkey){
        if (objkey in countReducedData)
          return true;
    }

    function cumulReduceData(sumReducedData, datakey){

      sumReducedData.reduce(function(accumulator, entry){
        // return entry
      });

    }

    function sumReductedData(Data){
        var sum = 0;
        $.each( Data , function( dataId, dataValue ) {
              sum = sum + ( parseInt(dataId) * dataValue);
        });
    }

    <?php 
    }
    ?>

</script>
