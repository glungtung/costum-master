<?php
$where = array(
            "type" => $_GET["thematique"],
            "parent"   =>  array('$exists'   =>  array($_GET["parentId"]))
        );

$eventOrga = PHDB::find(Event::COLLECTION,$where);
$poiList = PHDB::find(Poi::COLLECTION, 
array( "parent.".$_GET["parentId"] => array('$exists'=>1), 
"parent.".$_GET["parentId"].".type"=>$_GET["parentType"],
"type"=>"cms") );
// var_dump($poiList);exit;
$structField = "structags";
?>
<div style="text-align: left;">
	<div style="text-align: center;">
		<b style="font-size: 30px;"><?= $_GET["thematique"]; ?></b>
		<?php
		if(count ( Poi::getPoiByStruct($poiList,"blockimg0",$structField ) ) != 0){
			$result = Poi::getPoiByStruct($poiList,"blockimg0",$structField)[0];
			echo "<div class='markdown'>".@$result["description"]."</div>";
		}else{
			echo "Aucune description liée a ce type d\'événement";
		}
		?>
	</div>
	<div>
		<?php foreach ($eventOrga as $key => $value) {
			$formatStartDate = date(DateTime::ISO8601, $value["startDate"]->sec+14400);
			$formatEndDate = date(DateTime::ISO8601, $value["endDate"]->sec+14400);
			
			$start = date('d.m.y', strtotime($formatStartDate));
			$startDate = date('d', strtotime($formatStartDate));
			?>
			<u><h2><?= $start ?></h2></u>
			<p>
				<h3>Le Programme</h3>
			</p>
			<?php if(count ( Poi::getPoiByStruct($poiList,"minDesc".$startDate,$structField ) ) != 0){
				$result = Poi::getPoiByStruct($poiList,"minDesc".$startDate,$structField)[0];
				echo "<div class='markdown'>".@$result["description"]."</div>";
			}else{
				echo 'Aucun programme encore définis à ce jour pour le '.$start;
			} ?>
			<p>
				<h2>Le déroulement</h2>
			</p>
			<?php if(count ( Poi::getPoiByStruct($poiList,"longDesc".$startDate,$structField ) ) != 0){ 
				$results = Poi::getPoiByStruct($poiList,"longDesc".$startDate,$structField)[0];
				echo "<div class='markdown'>".@$results["description"]."</div>";
			}else{
				echo 'Aucun déroulement encore à ce jour pour le '.$start;
			}?>
		<?php } ?>
	</div>
</div>