<?php 
$cssAnsScriptFilesTheme = array(
      // SHOWDOWN
      '/plugins/showdown/showdown.min.js',
      // MARKDOWN
      '/plugins/to-markdown/to-markdown.js'            
    );

	$cssAndScriptFilesModule = array(
		'/js/default/profilSocial.js',
	);

    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
     
	HtmlHelper::registerCssAndScriptsFiles($cssAndScriptFilesModule, Yii::app()->getModule( "co2" )->getAssetsUrl());

	if (isset($this->costum["contextType"]) && isset($this->costum["contextId"])) {
		$subevent = array();

		if(isset($this->costum["communityLinks"]["subEvents"])){
			foreach ($this->costum["communityLinks"]["subEvents"] as $key => $value) {
				array_push($subevent,Element::getByTypeAndId("event",$key));
			}
		} else 
			echo "<h1 class='text-red center'> Ce costum nécessite un programme donc la création de sous évennement. </h1>";
	}

	if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
  		$el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );

  		$cmsList = PHDB::find(Poi::COLLECTION, 
                  array( "parent.".$this->costum["contextId"] => array('$exists'=>1), 
                         "parent.".$this->costum["contextId"].".type"=>$this->costum["contextType"],
                         "type"=>"cms") );
  	}

  	$bannerImg = @$el["profilRealBannerUrl"] ? Yii::app()->baseUrl.$el["profilRealBannerUrl"] : Yii::app()->getModule("costum")->assetsUrl."/images/coevent/no-banner.jpg";
  	$logo = @$el["profilMediumImageUrl"] ? Yii::app()->baseUrl.$el["profilMediumImageUrl"] : Yii::app()->getModule("costum")->assetsUrl."/images/coevent/logo.png";

    $params = [  "tpl" => "coevent","slug"=>$this->costum["slug"],"canEdit"=>false,"el"=>$el ];
    echo $this->renderPartial("costum.views.tpls.acceptAndAdmin", $params,true );
?>

<style type="text/css">
	@font-face{
	    font-family: "Montserrat-md";
	    src: url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/coevent/Montserrat-Medium.ttf")
	}

	@font-face{
	    font-family: "DIN-Regular";
	    src: url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/coevent/DIN-Regular.ttf")
	}

	@font-face{
	    font-family: "DINAlternate-Bold";
	    src: url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/coevent/DINAlternate-Bold.ttf")
	}
</style>

<div class="text-center row">

	<!-- Header -->
	<div class="header">
		<?php 
        $params =  array(
            "type"=> @$el["type"], 
            "id"=>(string) @$el["_id"], 
            "name"=> @$el["name"],
            "canEdit" => $canEdit,
            "profilBannerUrl"=> @$el["profilRealBannerUrl"]);

        echo $this->renderPartial("costum.views.tpls.modalHeader", $params); 
        ?>
		<div class="col-md-12">
			<div style="position: absolute;z-index: 10;top: 190px;" class="logoText col-md-12">
			<div class="col-md-6">
				<img style="width: 40%;" class="img-responsive pull-right" src="<?= $logo ?>">
			</div>
			<p style="font-size: 31px;" class="col-md-6 text-left text-white">CA TRAVAILLE<br>
				LE TRAVAIL EN TRANSITION ?<br>
				LES RÉCLUSIENNES<br></p>
			<p class="col-md-6 text-left text-white dinalternate" style="font-size: 31px;">Festival de la pensée</p>
			</div>
			<div style="position: absolute;z-index: 10;margin-left: 20%;width: 59%;top: 41vw;" class="hidden-xs container" id="searchBar">
				<a data-type="filters" href="javascript:;">
        			<span style="margin-left: 0%;position: absolute;width: 1%;background: transparent;border: transparent;" id="second-search-bar-addon-alternatibaRe" class="text-white input-group-addon pull-left main-search-bar-addon-mednum">
            			<i style="color: #EE302C;font-size: 29px;" id="fa-search" class="fa fa-search"></i>
        			</span>
    			</a>
    			
    			<input type="text" class="form-control pull-left text-center main-search-bars" id="second-search-bar" placeholder="Une recherche">

    			<a data-type="filters" href="javascript:;">
        			<span style="background-color: #EE302C;width: 10%;height: 40px;border-radius: 28px;position: relative;left: -3%;" id="second-search-bar-addon-alternatibaRe" class="text-white input-group-addon pull-left main-search-bar-addon-mednum">
            			<i style="font-size: 26px;" id="fa-search" class="fa fa-search"></i>
        			</span>
    			</a>
			</div>

			<!-- Dropdown -->
		    <div style="margin-left: 22.7vw;top: 44.9vw;" id="dropdown" class="hidden-xs dropdown-result-global-search hidden-xs col-md-5 no-padding">
		    </div>
		</div>
	</div>
	
	<!-- Infos -->
	<div style="margin-bottom: 3%;" class="row text-center infos">
		<div class="col-md-12">
			<h1 class="dinalternate" style="color: black;">
			  Qui sommes nous ?
			</h1>
		</div>
		<?= $this->renderPartial("costum.views.tpls.blockwithimgmenu", [
					"cmsList" => $cmsList, 
					"canEdit" => true, 
					"tags" => "block0",
					"subevent" => $subevent
				]); 
		?>
		
		<img style="width:100%;" class='margin-top-20' src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/coevent/trait.png">

		 <?php echo $this->renderPartial("costum.views.tpls.events.blockProgramme",[
		 			"cmsList" => $cmsList, 
		 			"canEdit" => true, 
		 			"subevent" => $subevent
		 		]); 
 		?>
	</div>


	<div style="margin-top : 5%;" class="col-md-12">
		<img style="width:100%;" class='margin-top-20' src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/coevent/trait.png">
		<?php 	
	        echo $this->renderPartial( "costum.views.tpls.multiblocksCaroussel",[
		            "cmsList"   => $cmsList,
		            "blockName" => "cc",
		            "titlecolor"=> "black",
		            "eltcolor" => "#e6344d",
		            "blockCt"   => 1
		        ] , true );
         ?>
         <img style="width:100%;" class='margin-top-20' src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/coevent/trait.png">
	</div>

	<!-- Communautée -->
	<div id="Community" style="margin-top: 0vw;margin-bottom: 7vw;position:relative;" class="col-lg-12 col-md-12 col-md-12">
		<div style="margin-top: 6vw" class="no-padding carousel-border" >
        	<div id="docCarousel" class="carousel slide" data-ride="carousel">
            	<div class="carousel-inner itemctr">
            		
                </div>
                <ol style="margin-bottom: -6%;" class="carousel-indicators">
		      		
				</ol>		          
	            <div style="position: relative;" id="arrow-caroussel">
	                <a style="opacity: 1;margin-left: 36%;margin-top: 1%;width: 5%;" class="carousel-control" href="#docCarousel" data-slide="prev">
	                    <?php echo $this->renderPartial("costum.assets.images.coevent.backLeft"); ?>
	                    <span class="sr-only">Previous</span>
	                </a>
	                <a style="opacity:1;margin-left: 57vw;margin-top: 1%;width: 5%;" class="carousel-control" href="#docCarousel" data-slide="next">
	                     <?php echo $this->renderPartial("costum.assets.images.coevent.backRight"); ?>
	                    <span class="sr-only">Next</span>
	                </a>
	            </div>   
        	</div>
    	</div> 
	</div>

<?php $canEdit = false ; if($canEdit){ ?> 
<!-- ESPACE ADMIN --> 
<center>
	<hr>
	<div class="container">
		<a href="javascript:;" class="addTpl btn btn-primary" data-key="blockevent" data-id="<?= $this->costum["contextId"]; ?>" data-collection="<?= $this->costum["contextType"]; ?>"> <i class="fa fa-plus"></i> Ajouter une section</a>
	</div>
</center>
<?php } ?>

<script type="text/javascript">
	jQuery(document).ready(function(){
		//data of this event
		// eventOrga =<?php //echo json_encode(@$eventOrga) ?>;
		// subevent =<?php //echo json_encode(@$subevent) ?>;
		// day = <?php //echo json_encode(@$day) ?>;


		// type = "";
		// desc = "";
		// date = "";
		// longdate = "";
		// star_hour = "";
		// end_hour = "";
		// descevent = "";
		// descshort = "";
		// community = "";
		// i = 0;
		// y = 0;

		// $.each(subevent,function(key,value){
			
		// 	type += '<a data-key="'+key+'" class="type" style="font-size:2vw;color: white;cursor:pointer;" title="'+value.type+'">'+value.type+'</a><br>';

		// 	description = (typeof value.description != "undefined") ? value.description : "Aucune description";
		// 	shortDesc = (typeof value.shortDescription != "undefined") ? value.shortDescription : "Aucune description courte pour cette èvènement";

		// 	if (i == 0) {
		// 		desc += '<h2 class="description item'+key+' col-md-10 dinalternate" style="color:#EE302C" title="'+value.name+'">'+value.name+'</h2>';
		// 		desc += '<div style="margin-top: 4%;font-size: 1.3vw;" class="description item'+key+' col-md-10 dinregular">';
		// 			desc += description;
		// 		desc += '</div>';
		// 		desc += '<div  class="item'+key+' col-md-12 see-plus">';
		// 		desc +=	'<a class="col-md-2 text-center lbh dinalternate" style="background-color:#EE302C;color:white;border-style: solid;border-color: #EE302C;border-radius: 51px;padding: 14px;margin-top: 3%;" href="#@'+value.slug+'"> Voir plus </a>';
		// 		desc += '</div>';

		// 		descshort += '<li style="margin-top: 1%;"  class ="liFirst'+i+' li dinalternate">';
		// 			descshort += shortDesc;
		// 		descshort += '</li>';
		// 	}else{
		// 		desc += '<h2 class="description item'+key+' col-md-10 " style="color:#EE302C;display:none;" title="'+value.name+'">'+value.name+'</h2>';
		// 		desc += '<div style="display:none;margin-top: 4%;font-size: 1.1em;" class="description item'+key+' col-md-10">';
		// 		desc += description;
		// 		desc += '</div>';
		// 		desc += '<div style="display:none;" class="see-plus col-md-12 item'+key+'">';
		// 		desc +=	'<a class="col-md-2 text-center lbh dinalternate" style="background-color:#EE302C;color:white;border-style: solid;border-color: #EE302C;border-radius: 51px;padding: 14px;margin-top: 3%;" href="#@'+value.slug+'"> Voir plus </a>';
		// 		desc += '</div>';

		// 		descshort += '<li style="margin-top: 1%;display:none;"  class ="liFirst'+i+' li">';
		// 			descshort += shortDesc;
		// 		descshort += '</li>';
		// 	}

		// 	i++;
		// });

		// countRes = 0;

		// date += "<div class='item active'>";

		// $.each(day,function(key,value){
		// 	if (y == 0) {
		// 		longdate += '<h2 class="test longdate'+y+' col-md-12 dinalternate">'+value.daymonthyear+'</h2>';
		// 		star_hour += "<p class='hourStart hourStart"+y+"'>"+value.heure_debut+"H"+"</p>";
		// 		end_hour += "<p class='hourEnd hourEnd"+y+"'>"+value.heure_fin+"H"+"</p>";
		// 	}else{
		// 		longdate += '<h2 style="display:none;" class="test longdate'+y+' col-md-12 dinalternate">'+value.daymonthyear+'</h2>';
		// 		star_hour += "<p style='display:none;' class='hourStart hourStart"+y+"'>"+value.heure_debut+"H"+"</p>";
		// 		end_hour += "<p style='display:none;' class='hourEnd hourEnd"+y+"'>"+value.heure_fin+"H"+"</p>";
		// 		// hour += '';
		// 	}

  //   		if (countRes >= 4) {
  //       		date += '</div>';
  //       		date += '<div class="item">';
  //       		countRes = 0;
  //   		}

		// 	date += "<div data-key='"+y+"' class='col-md-2 containjourney'>";
		// 		date += '<p class="date dinalternate">'+value.day+'</p>';
		// 		date += '<p class="journey dinalternate">'+value.jour+'</p>';
		// 	date += "</div>";

		// 	y++;
		// 	countRes++;
		// });

		// date += '</div>';

		// desceventdad = (typeof eventOrga.description != "undefined") ? eventOrga.description : "Aucune description sur l'évenement parent";

		// descevent += desceventdad;
		
		// $('#containdescriptionevent').html(descevent);
		// $('.containStartHour').html(star_hour);
		// $('.containEndHour').html(end_hour);
		// // $('.shortdescsub').html(descshort);
		// $('.longDate').html(longdate);
		// $(".itemctrDate").html(date);
		// $(".typeEvent").html(type);
		// $("#descriptionevent").html(desc);
		// $(".containStartHour").html();
		// $(".containEndHour").html();

		//Get all of informations on the citoyens
		ajaxPost('#containCommunity', 
		baseUrl+"/costum/coevent/getcommunityaction",
		costum.communityLinks.subEvents,
		function(data){
			mylog.log("success : ",data);

			var i = 0;
			var count = 0;
			var ctr = "";
			var str = "";
			var url = "<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>/images/coevent/default_directory.png";


			ctr += "<div class='item active'>";
			ctr += '<img style="width:11%;" class="img-responsive col-md-3" src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/coevent/points_block_membre.png">';
			str += '<li style="background-color:#EE302C;border-color:ligthgrey;"  data-target="#myCarousel" data-slide-to="'+count+'" class="active">';

			$.each(data.element,function(key,value){

				var imgProf = (value.imgMedium != "none") ? baseUrl+value.imgMedium: url;

				var type = (typeof value.typeSig != "undefined") ? value.typeSig : value.type;

				mylog.log("typeof typeSig or type",type); 

        		if (i >= 3) {
            		ctr += '</div>';
            		ctr += '<div class="item">';
            		ctr += '<img style="width:11%;" class="img-responsive col-md-3" src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/coevent/points_block_membre.png">';
            		i = 0;
            		count++;
        		}

        		if (count >= 1) {
        			str += "</li>";
        			str += '<li style="background-color:#EE302C;border-color:ligthgrey;" data-target="#myCarousel" data-slide-to="'+count+'">';
        			// str += '</li>';
        			count = 0;
        		}

				ctr += '<div class="card text-center">';
					ctr += '<div style="margin-left:3%;margin-top:3%;box-shadow:0px 0px 8px 0px;border-radius:10px;margin-bottom:5%;height:25vw;" class="col-md-3 no-padding">';
						ctr += '<center>';
						ctr += "<img class='imgProfil img-responsive' src='"+imgProf+"'>";
						ctr += '</center>';
							ctr += '<div class="col-md-12">';
								ctr += '<i style="font-size: 35px;color: white;background: #EE302C;padding: 17px;border-radius: 13px;margin-top: -16%;position: relative;" class="fa fa-user pull-left"></i>';
								ctr += "<a class='lbh' href='#page.type."+type+".id."+value.id+"'>";
									ctr += '<p class="col-md-12 dinalternate" style="color:#EE302C;">'+value.name+'</p>';
								ctr += '</a>';
								ctr += "<p class='col-md-12 dinregular'>"+value.shortDescription+'</p>';
							ctr += '</div>';
					ctr += '</div>';
				ctr += '</div>';
				i++;
				$(".carousel-indicators").html(str);
				$(".itemctr").html(ctr);
			});
		},"html");

		contextData = {
			id : costum.contextId,
			type : costum.contextType,
			name : costum.title,
			profilThumbImageUrl : "http://127.0.0.1/ph/themes/CO2/assets/img/LOGOS/CO2/logo-min.png"
		};
	});

$("#second-search-bar").off().on("keyup",function(e){ 
    $("#input-search-map").val($("#second-search-bar").val());
    $("#second-search-xs-bar").val($("#second-search-bar").val());
    if(e.keyCode == 13){
            searchObject.text=$(this).val();
            searchObject.sourceKey=[costum.contextSlug];
            myScopes.type="open";
            myScopes.open={};
            startGlobalSearch(0, indexStepGS);
            $("#dropdown").css('display','block');
     }  
});

$("#second-search-xs-bar").off().on("keyup",function(e){ 
    $("#input-search-map").val($("#second-search-xs-bar").val());
    $("#second-search-bar").val($("#second-search-xs-bar").val());
    if(e.keyCode == 13){
            searchObject.text=$(this).val();
            searchObject.sourceKey=[costum.contextSlug];
            myScopes.type="open";
            myScopes.open={};
            startGlobalSearch(0, indexStepGS);
            $("#dropdown").css('display','block');            
    }
});

$("#second-search-bar-addon-alternatibaRe, #second-search-xs-bar-addon").off().on("click", function(){
    $("#input-search-map").val($("#second-search-bar").val());
    searchObject.text=$("#second-search-bar").val();
    searchObject.sourceKey=[costum.contextSlug];
    myScopes.type="open";
    myScopes.open={};
    startGlobalSearch(0, indexStepGS);
    $("#dropdown").css('display','block');
});
</script>