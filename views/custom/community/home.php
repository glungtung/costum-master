<?php
$cssJS = array(
    '/plugins/jQuery-Knob/js/jquery.knob.js',
    '/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
    //'/plugins/jQuery-Smart-Wizard/styles/smart_wizard.css',
    // SHOWDOWN
    '/plugins/showdown/showdown.min.js',
    // MARKDOWN
    '/plugins/to-markdown/to-markdown.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl);
$poiList = array();

if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
    $poiList = PHDB::find(Poi::COLLECTION,
        array( "parent.".$this->costum["contextId"] => array('$exists'=>1),
            "parent.".$this->costum["contextId"].".type"=>$this->costum["contextType"],
            "type"=>"cms") );
}
?>

<style type="text/css">
    @font-face{
        font-family: "Typo Grotesk";
        src: url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/community/Typo Grotesk Demo.otf") format("otf");
    }.mst{font-family: 'Typo Grotesk'!important;}


    #customHeader{
        margin-top: 0px;
    }
    #costumBanner{
        display: none;
        /* max-height: 375px; */
    }
    #costumBanner h1{
        position: absolute;
        color: white;
        background-color: rgba(0,0,0,0.4);
        font-size: 29px;
        bottom: 0px;
        padding: 20px;
    }
    #costumBanner h1 span{
        color: #eeeeee;
        font-style: italic;
    }
    #costumBanner img{
        min-width: 100%;
    }
    .btn-main-menu{
        background: <?php echo $this->costum["colors"]["pink"]; ?>;
        border-radius: 10px;
        padding: 10px !important;
        color: white;
        cursor: pointer;
        border:3px solid transparent;
        font-size: 1.5em
        /*min-height:100px;*/
    }
    .btn-main-menuW{
        background: white;
        color: <?php echo $this->costum["colors"]["pink"]; ?>;
        border:none;
        cursor:text ;
    }
    .btn-main-menu:hover{
        border-color: : black;
        
        background-color: white !important;
        color: <?php echo Yii::app()->session["costum"]["colors"]["pink"]; ?>;
    }
    .btn-main-menuW:hover{
        border:none;
    }
    @media screen and (min-width: 450px) and (max-width: 1024px) {
        .logoDescription{
            width: 60%;
            margin:auto;
        }
    }

    @media (max-width: 1024px){
        #customHeader{
            margin-top: -1px;
        }
    }
    @media (max-width: 768px){
        h1, h2, h3, h4, h5, h6 {
            display: block;
            font-size: 1.9em;
        }
    }
    #customHeader #newsstream .loader{
        display: none;
    }
    
    .bgFormColor{
        background-color:<?php echo Yii::app()->session["costum"]["colors"]["dark"]; ?>;
    }

</style>

<?php
$params = [  "tpl" => $this->costum["slug"],
    "slug"=>$el["slug"],
    "canEdit"=>$canEdit,
    "el"=>$el  ];
echo $this->renderPartial("costum.views.tpls.acceptAndAdmin", $params, true );
?>

<div class="col-xs-12 no-padding bgFormColor" id="customHeader ">


    </div>
    <?php
    $color1 = "#e45f35";
    if(isset($this->costum["cms"]["color1"]))
        $color1 = "#e45f35";
    if($canEdit)
        echo "<a class='btn btn-xs btn-danger editBtn' href='javascript:;' data-key='color1' data-type='color'  data-path='costum.cms.color1' data-label='Couleur Principale '><i class='fa fa-pencil'></i></a>";
    ?>

    <div class="col-sm-12 col-md-12 col-xs-12 no-padding" style="background-color:<?php echo $this->costum["colors"]["grey"]; ?>; max-width:100%;">

        <div class="col-xs-12 no-padding" style="">

            <?php if( !isset(Yii::app()->session["userId"])){ ?>
                <div class="col-xs-12 no-padding">
                    <div class="col-md-12 col-sm-12 col-xs-12 padding-20" style="padding-left:100px;background-color: <?php echo $this->costum["colors"]["grey"]; ?>; ">
                        <div class="col-xs-12 col-sm-10 col-sm-offset-1 pull-left padding-20 shadow2" style="font-family: montserrat; margin-top:-200px;background-color: #fff;font-size: 14px;z-index: 5; padding-top: 240px !important;">
                            <div class="col-xs-12  ourvalues" style="text-align:center;">
                                <h2 class="mst col-xs-12 text-center">
                                    <br>
                                    <?php
                                    if(isset($this->costum["cms"]["title1"])){
                                        echo htmlentities($this->costum["cms"]["title1"]);
                                    } else { ?> L’observation des cétacés en France et dans les territoires français d’outre-mer<?php }
                                    if($canEdit)
                                        echo "<a class='btn btn-xs btn-danger editBtn' href='javascript:;' data-key='title1' data-path='costum.cms.title1'><i class='fa fa-pencil'></i></a>";
                                    ?>
                                </h2>

                                <p class="mst" style="color:<?php echo $this->costum["colors"]["dark"]; ?>">
                                    <?php
                                    if(isset($this->costum["cms"]["subtitle1"])){
                                        echo htmlentities($this->costum["cms"]["subtitle1"]);
                                    } else { ?>
                                        Cette plateforme a vocation à élaborer et à tenir à jour un résumé par territoire français de l’activité d’observation des cétacés pratiquée.
                                    <?php }
                                    if($canEdit)
                                        echo "<a class='btn btn-xs btn-danger editBtn' href='javascript:;' data-key='subtitle1' data-path='costum.cms.subtitle1'><i class='fa fa-pencil'></i></a>";
                                    ?>
                                    <br/>
                                </p>


                                <br/>
                                <div class="col-xs-12" style="margin-bottom:40px;">
                                    <div class="col-xs-12 col-sm-4">
                                        <?php if( Person::logguedAndValid() ){ ?>
                                            <span class="col-xs-12 text-center btn-main-menu btn-main-menuW ">Je suis Connecté !! <br>et je veux l'utopie</span>
                                        <?php } else { ?>
                                            <button  data-toggle="modal" data-target="#modalLogin" class="col-xs-12 btn-main-menu text-center" style="background-color: #1B4F95;" >Je suis partenaire</button>
                                            <span> Je crée une fiche territoire </span>
                                        <?php } ?>
                                    </div>

                                    <div class="col-xs-12 col-sm-4">
                                       
                                    </div>

                                    <div class="col-xs-12 col-sm-4">
                                        <a href="#results" class="col-xs-12 btn-main-menu" style="background-color: #1B4F95;">Je suis visiteur</a>
                                         <span> Je consulte les résumés par territoire </span>
                                    </div>
                                </div>

                                <div class="col-xs-12" style="margin-bottom:40px;">
                                    Cette plateforme est réalisée dans le cadre du projet SOMMOM “Suivi et encadrement de l’activité d’observation des cétacés dans les territoires français d’outre-mer”, financé par l’Office Français de la Biodiversité (OFB)
                                </div>  

                            </div>


                        </div>

                        <div class="col-xs-12 text-center " style="font-size: 1.5em; padding-top: 20px;" >


                            <?php
                            if(isset($this->costum["cms"]["textIntro"])){
                                echo htmlentities($this->costum["cms"]["textIntro"]);
                            } else { ?> <b>Lorem ipsum dolor sit amet</b>, consectetur adipisicing elit, sed do eiusmod
                                                                         tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                                                         quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                                                         consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                                                         cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                                                         proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                <br/><br/>
                                <b>Lorem ipsum dolor sit amet</b>, consectetur adipisicing elit, sed do eiusmod
                                                                 tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                                                 quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                                                 consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                                                 cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                                                 proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                <br/><br/>
                                <b>Lorem ipsum dolor sit amet</b>, consectetur adipisicing elit, sed do eiusmod
                                                                 tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                                                 quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                                                 consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                                                 cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                                                 proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                <br/><br/>
                                <b>Lorem ipsum dolor sit amet</b>, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            <?php }
                            if($canEdit)
                                echo "<a class='btn btn-xs btn-danger editBtn' href='javascript:;' data-key='textIntro' data-type='textarea' data-markdown='1'  data-path='costum.cms.textIntro' data-label='Expliquez les objectifs de ce formulaire ? '><i class='fa fa-pencil'></i></a>";
                            ?>
                        </div>

                    </div><br/>

                </div>
            <?php } else { ?>
                <style type="text/css">
                    .monTitle{
                        border-top: 1px dashed <?php echo $this->costum["colors"]["pink"]; ?>;
                        border-bottom: 1px dashed <?php echo $this->costum["colors"]["pink"]; ?>;
                        margin-top: -20px;
                    }

                .dash-content {
  padding: 60px;
}

.titre4 {
  font-family: "Roboto";
  font-weight: 500;
  font-size: 25px;
  color: #aeaeae;
  text-transform: uppercase;
  margin: 0;
}

hr {
  margin-bottom: 16px;
  border: 1px solid #eaeaea;
}

.border-card {
  background: #fff;
  margin-bottom: 30px;
  /*display: flex;*/
  align-items: center;
  font-family: "Roboto";
  font-size: 14px;
  padding: 12px 16px;
  cursor: pointer;
  border-radius: 4px;
  border: 1px solid #eaeaea;
  box-shadow: 0px 2px 1px 0px rgba(0, 0, 0, 0.1);
  transition: all 0.25s ease;
}
.border-card:hover {
  -webkit-transform: translateY(-1px);
          transform: translateY(-1px);
  box-shadow: 0 5px 10px 0 rgba(0, 0, 0, 0.1), 0 5px 10px 0 rgba(0, 0, 0, 0.1);
}
.border-card.over {
  background: rgba(70, 222, 151, 0.15);
  padding-top: 24px;
  padding-bottom: 24px;
  border: 2px solid #47DE97;
  box-shadow: 0 5px 10px 0 rgba(0, 0, 0, 0), 0 5px 10px 0 rgba(0, 0, 0, 0);
}
.border-card.over .card-type-icon {
  color: #47DE97 !important;
}
.border-card.over p {
  color: #47DE97 !important;
}

.content-wrapper {
  display: flex;
  justify-content: flex-start;
  width: 100%;
  white-space: nowrap;
  overflow: hidden;
  transition: all 0.25s ease;
}

.min-gap {
  flex: 0 0 40px;
}

.card-type-icon {
  flex-grow: 0;
  flex-shrink: 0;
  margin-right: 16px;
  font-weight: 400;
  color: #323232;
  border-radius: 50%;
  width: 40px;
  height: 40px;
  text-align: center;
  line-height: 40px;
  font-size: 14px;
  transition: all 0.25s ease;
}
.card-type-icon.with-border {
  color: #aeaeae;
  border: 1px solid #eaeaea;
}
.card-type-icon i {
  line-height: 40px;
}

.label-group {
  white-space: nowrap;
  overflow: hidden;
}
.label-group.fixed {
  flex-shrink: 0;
}
.label-group p {
  margin: 0px;
  line-height: 21px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}
.label-group p.title {
  color: #323232;
  font-weight: 500;
}
.label-group p.title.cta {
  text-transform: uppercase;
}
.label-group p.caption {
  font-weight: 400;
  /*color: #aeaeae;*/
  color: orange;
}

.end-icon {
  margin-left: 16px;
}

fieldset {
  position: relative;
  border: 0;
  padding: 0;
}

.control {
  display: inline-flex;
  position: relative;
  margin: 5px;
  cursor: pointer;
  border-radius: 99em;
}

.control input {
  position: absolute;
  opacity: 0;
  z-index: -1;
}

.control svg {
  margin-right: 6px;
  border-radius: 50%;
  box-shadow: 3px 3px 16px rgba(0, 0, 0, .2)
}

.control__content {
  display: inline-flex;
  align-items: center;
  padding: 6px 12px;
  font-size: 20px;
  line-height: 32px;
  color: rgba(0, 0, 0, .55);
  background-color: rgba(0, 0, 0, .05);
  border-radius: 99em;
}

.control:hover .control__content {
  background-color: rgba(0, 0, 0, .1);
}

.control input:focus ~ .control__content {
  box-shadow: 0 0 0 .25rem rgba(12, 242, 143, .2);
  background-color: rgba(0, 0, 0, .1);
}

.control input:checked ~ .control__content {
  background-color: rgba(12, 242, 143, .2);
}

.banner-img {
    display: block;
    margin-left: auto;
    margin-right: auto;
}

@media only screen and (min-width: 1000px) {
  .banner-img {
    height: 200px;
  }
}

@media only screen and (max-width: 1200px){
  .banner-img {
    height: 100px;
  }
}

.description-head {
  padding: 60px;
  padding-bottom: 0px important;
}

.description-block {
    font-size: 20px;
    border-left: 4px solid <?php echo Yii::app()->session["costum"]["colors"]["pink"]; ?>;
    padding-left: 15px;
    font-weight: bold;
}



</style>

<div class="bgFormColor" style=" z-index: 5; ">
  <img  class="banner-img"  src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/community/lartevolution.jpg" >
</div>

<style type="text/css">
  .form-group ol{
    color:white;
  }
</style>

<?php $formSmallSize =  12; ?>
<div class="col-md-12 col-lg-<?php echo $formSmallSize?> bgFormColor no-padding "><br/>

    <div class="margin-top-20   padding-20 bgFormColor">

        <div class="col-xs-12">

            <script type="text/javascript">
                var formInputs = {};
                var answerObj = <?php echo (!empty($answer)) ? json_encode( $answer ) : "null"; ?>;

            </script>
            <div class="col-xs-12 margin-top-20 bgFormColor">
                
                <?php

                $wizardUid="communityForm";

                $params = [
                    "el" => $el,
                    "color1" => $color1,
                    "canEdit" => $canEdit,
                    "allAnswers"=> $allAnswers,
                    "what" => "un organisme",
                    "btnn1" => "territoires",
                    "wizid"=> $wizardUid, 
                    "mode" => $mode
                ];
                echo $this->renderPartial("survey.views.tpls.forms.cplx.answers",$params); 

                if($showForm){

                    $params = [
                        "formList"=>$formList,
                        "el" => $el,
                        "active" => "all",
                        "color1" => "orange",
                        "canEdit" => $canEdit,
                        "answer"=>$answer,
                        "showForm" => $showForm,
                        "wizid" => $wizardUid,
                        "mode" => $mode
                    ];

                    echo $this->renderPartial("survey.views.tpls.forms.costum.community.wizard",$params);

                } else {
                    echo "<h4 class='text-center' style='color:".$color1."'><i class='fa fa-warning'></i> Une seul réponse n'est possible.</h4>";
                    echo "<a class='btn btn-primary' href='/costum/co/index/slug/".$el["slug"]."/answer/".$myAnswers[0]."'>Votre réponse</a>";
                }


                ?>

            </div>
        </div>
    </div>

    

</div>

            

        </div>
    </div>


</div>

<script type="text/javascript">
    $("#wizardLinks").hide();
    //to edit costum page pieces
    var configDynForm = <?php echo json_encode(Yii::app()->session['costum']['dynForm']); ?>;
    //information and structure of the form in this page
    var tplCtx = {};

    jQuery(document).ready(function() {

        mylog.log("render","/modules/costum/views/custom/community/home.php");
        contextData = {
            id : "<?php echo $this->costum["contextId"] ?>",
            type : "<?php echo $this->costum["contextType"] ?>",
            name : '<?php echo htmlentities($el['name']) ?>',
            profilThumbImageUrl : "http://127.0.0.1/ph/themes/CO2/assets/img/LOGOS/CO2/logo-min.png"
        };
        
    });
</script>
<?php } } ?>


