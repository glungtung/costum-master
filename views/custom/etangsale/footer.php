<style>
.footer-li{
    list-style: none;
    font-size: 0.9rem;
}
</style>
<footer style="background: #8e8dca; padding:1%;">
    <div class="text-center container">
        <div class="col-md-4" style="padding-top:1%;">
            <ul>
                <li class="footer-li"><h5>Plan du site</h5></li>
                <li class="footer-li">
                <a href="javascript:;" data-hash="#accueil" class="lbh-menu-app" style="text-decoration : none;">
                    Accueil
                </a>
                </li>
                <li class="footer-li">
                <a href="javascript:;" data-hash="#search" class="lbh-menu-app" style="text-decoration : none;">                
                    Annuaire
                </a>
                </li>
                <li class="footer-li">
                <a href="javascript:;" data-hash="#agenda" class="lbh-menu-app" style="text-decoration : none;">                                
                    Évènement
                </a>
                </li>
                <li class="footer-li">
                <a href="javascript:;" data-hash="#live" class="lbh-menu-app" style="text-decoration : none;">                                                
                    Actualité
                </a>
                </li>
                <li class="footer-li">
                    <a href="javascript:;" data-hash="#annonce" class="lbh-menu-app" style="text-decoration : none;">                                
                    Tous les demandes/offres
                    </a>
                    </li>
                <li class="footer-li">
                <a href="javascript:;" data-hash="#article" class="lbh-menu-app" style="text-decoration : none;">                                                
                    Tous les articles
                </a>
                </li>
                <li class="footer-li">
                <a href="javascript:;" data-hash="#project" class="lbh-menu-app" style="text-decoration : none;">                                                
                    Tous les projets
                </a>
                </li>
                <li class="footer-li">
                <a href="javascript:;" data-hash="#organization" class="lbh-menu-app" style="text-decoration : none;">                                
                    Tous les organisations
                </a>
                </li>
            </ul>
        </div>
        <div class="col-md-4" style="padding-top:1%;">
            <ul>
                <li class="footer-li"><h5>Documentation</h5></li>
                <li class="footer-li"><h5>Mentions légales</h5></li>
                <li class="footer-li"><h5>Politique de confidentialité</h5></li>
                <li class="footer-li"><h5>Politique et gestion des Cookies</h5></li>
            </ul>
        </div>
        <div class="col-md-4" style="padding-top:1%;">
            <ul>
                <li class="footer-li"><h5>Réseaux sociaux</h5></li>
                <li class="footer-li"><h5>
                <a href="https://communecter.org" target="_blank">
 Communecter </a></h5></li>
                <li class="footer-li"><h5>
                <a href="https://twitter.com/communecter" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i>
 Twitter </a></h5></li>
                <li class="footer-li"><h5><a href="https://www.facebook.com/communecter/">  <i class="fa fa-facebook-official" aria-hidden="true"></i>
 Facebook </a> </h5></li>
                <li class="footer-li"><h5>
                <a href="https://mamot.fr/@communecter" target="_blank"><i class="fa fa-mastodon"></i> Mamot </a></h5></li>
            </ul>
        </div>
    </div>
</footer>