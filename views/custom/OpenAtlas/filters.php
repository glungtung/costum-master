<?php 
    $cssAnsScriptFilesTheme = array(
    	'/css/filters.css',
	);

    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, $this->module->getParentAssetsUrl());
?> 
<style type="text/css">

	#filters-nav #input-sec-search .input-global-search{
		border-radius: 0px 20px 20px 0px !important;
	}

	#filters-nav .dropdown-menu-complexe {
	    border-radius: 5px;
	    border: 1px solid #00bcbe;
	    padding: 10px;
	}
	
	#filters-nav .dropdown-menu-complexe .list-filters .button-complexeSelectList{
    background: transparent;
    border: transparent;
	}

	#filters-nav .dropdown-menu-complexe .list-filters .button-complexeSelectList-subsub{
	    background: transparent;
	    border: transparent;
	}

	#filters-nav .dropdown-menu-complexe .list-filters .button-complexeSelectList-subsub:hover{
	    background-color : #00bcbe;
	    color: white;
	}

	#filters-nav .dropdown-menu-complexe .list-filters .button-complexeSelectList:hover{
	    background-color : #00bcbe;
	    color: white;
	}

	#filters-nav .dropdown-menu-complexe{
	    color: #00bcbe;
	}

	#filterContainer .dropdown .btn-menu, #filters-nav .complexeSelect .btn-menu ,#filters-nav .dropdown .btn-menu, #filters-nav .filters-btn {
	    padding: 10px 15px;
	    font-size: 16px;
	    border: 1px solid #00bcbe;
	    color: #00bcbe;
	    top: 0px;
	    position: relative;
	    border-radius: 20px;
	    text-decoration: none;
	    background: white;
	    line-height: 20px;
	}

	#filterContainer #input-sec-search .input-group-addon, #filters-nav #input-sec-search .input-group-addon, .searchBar-filters .input-group-addon {
	    padding: 10px 0px 10px 10px;
	    font-size: 18px;
	    font-weight: 400;
	    line-height: 1;
	    color: #00bcbe;
	    margin-top: 5px;
	    text-align: center;
	    background-color: white !important;
	    border-right-width: 0px;
	    border: 1px solid #00bcbe;
	    border-right-width: 1px;
	    border-radius: 0px 20px 20px 0px;
	    border-right-width: 0px;
	    width: 30px;
	}

	#filters-nav .dropdown-menu-complexe .list-filters .lvlOne{
	    background: transparent;
	    border: transparent;
	}

	#filters-nav .dropdown-menu-complexe .list-filters .lvlOne:hover{
	    background-color : #00bcbe;
	    color: white;
	}

	#filters-nav .dropdown-menu-complexe .list-filters .lvlTwo{
	    background: transparent;
	    border: transparent;
	}

	#filters-nav .dropdown-menu-complexe .list-filters .lvlTwo:hover{
	    background-color : #00bcbe;
	    color: white;
	}


	#filterContainer #input-sec-search .input-global-search, #filters-nav #input-sec-search .input-global-search, .searchBar-filters .search-bar {
	    border-left-width: 0px;
	    height: 40px;
	    margin-top: 5px;
	    border-radius: 20px 0px 0px 20px;
	    box-shadow: none;
	    width: inherit;
	    border-color: #00bcbe;
	}
</style>

<script type="text/javascript">
	var pageApp=<?php echo json_encode(@$page); ?>;

	if (pageApp == "search") {
		var paramsFilter= {
		 	container : "#filters-nav",
		 	defaults : {
		 		types : ["organizations","projects"]
		 	}
		};
	}
	
	var filterSearch={};

	jQuery(document).ready(function() {
		filterSearch = searchObj.init(paramsFilter);
	});

</script>