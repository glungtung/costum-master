<footer class="text-center col-xs-12 pull-left no-padding bg-purple">
     <div class="">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 text-center col-footer col-footer-step">
                    <a href="#mentions" class="lbh text-white"><?php echo Yii::t("home","Mentions légales") ?></a><br><br>
                   
                </div>
                <div class="col-xs-12 col-sm-6 col-footer">
                    <span style="font-size:20px;"><a href="mailto:contact@mayenne-demain.fr" style="color: #FFF !important;">contact@mayenne-demain.fr</a></span>
                    <ul class="list-inline">
                        <li>
                            <a href="https://www.facebook.com/MayenneDemain/" target="_blank" 
                               class="btn-social btn-outline">
                                <i class="fa fa-fw fa-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/JCLavandier" target="_blank" 
                               class="btn-social btn-outline">
                                <i class="fa fa-fw fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/jclavandier/" target="_blank" 
                               class="btn-social btn-outline">
                                <i class="fa fa-fw fa-instagram"></i>
                            </a>
                        </li>
                         <li>
                            <a href="https://www.youtube.com/channel/UCGo1A-ig2Bv1R3d2VZYB77g" target="_blank" 
                               class="btn-social btn-outline">
                                <i class="fa fa-fw fa-youtube"></i>
                            </a>
                        </li>
                     </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
