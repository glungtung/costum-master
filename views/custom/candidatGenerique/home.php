<?php 
$cssAnsScriptFilesTheme = array(
    // SHOWDOWN
    '/plugins/showdown/showdown.min.js',
    // MARKDOWN
    '/plugins/to-markdown/to-markdown.js'            
  );

  HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl); 

  $cssAndScriptFilesModule = array(
      '/js/default/profilSocial.js',
      '/js/default/editInPlace.js',
  );
  
  HtmlHelper::registerCssAndScriptsFiles($cssAndScriptFilesModule, Yii::app()->getModule( "co2" )->getAssetsUrl());

    $cmsList = array();

    $page = ( isset($page) && !empty($page) ) ? $page : "welcome";

    if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
        $el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );
       
        $cmsList = PHDB::find(Cms::COLLECTION, 
                            array( "parent.".$this->costum["contextId"] => array('$exists'=>1)));
    } 
    $bannerImg = @$el["profilRealBannerUrl"] ? Yii::app()->baseUrl.$el["profilRealBannerUrl"] : Yii::app()->getModule("costum")->assetsUrl."/images/filiereCostum/no-banner.jpg";
  
    ?>
<style>
    section{
        background: #f6f6f6;
    }

    #contentBanner{
        max-height: 500px;
        background: yellow;
        background-size : cover;
    }

    #banner_element {
        background: #1a242f;
        color: white;
        border-color: #161f29;
    }

    h1{
        font-size: 45px;
    }

    @media (max-width:768px){
        h1{
            font-size: 35px;
        }
    }
</style>

<!-- header -->
<?php
$params = [  "tpl" => "candidatGenerique","slug"=>$this->costum["slug"],"canEdit"=> $canEdit,"el"=>$el, "page" => @$page ];
echo $this->renderPartial("costum.views.tpls.acceptAndAdmin",$params,true); 
?>
<div id="contentBanner" class="col-xs-12 col-md-12 no-padding">

        <!-- Edit Banniere -->
        <?php 
        $this->renderPartial("co2.views.element.modalBanner", array(
            "edit" => $canEdit,
            "openEdition" => false,
            "profilBannerUrl"=> @$el["profilBannerUrl"],
            "element"=> $el)); 
        if (@$el["profilBannerUrl"] && !empty($el["profilBannerUrl"])){   
            $imgHtml='<img class="col-md-12 col-sm-12 col-xs-12 no-padding img-responsive" alt="'.Yii::t("common","Banner").'"
                src="'.Yii::app()->createUrl('/'.$el["profilBannerUrl"]).'">';
            if (@$el["profilRealBannerUrl"] && !empty($el["profilRealBannerUrl"])){
                $imgHtml='<a href="'.Yii::app()->createUrl('/'.$el["profilRealBannerUrl"]).'"
                            class="thumb-info"  
                            data-title="'.Yii::t("common","Cover image of")." ".$el["name"].'"
                            data-lightbox="all">'.
                            $imgHtml.
                        '</a>';
            }
            echo $imgHtml;
        }else{     
            $url=Yii::app()->theme->baseUrl.'/assets/img/background-onepage/connexion-lines.jpg';   
            echo '<img class="col-md-12 col-sm-12 col-xs-12 no-padding img-responsive" alt="'.Yii::t("common","Banner").'"
                src="'.$url.'">';
        } 
        ?>
</div>

<!-- info -->
<div class="col-xs-12 col-md-12" style="background: <?= $this->costum["css"]["menuTop"]["background"]; ?>; height:105px;"></div>

<div class="col-xs-12 col-md-12" style="margin-top: -50px; z-index: 1000;">
    <div class="info container text-center">
        <p class="f-para"><?= $this->renderPartial("costum.views.tpls.blockCms.textImg.text", 
                                                    array("cmsList" => $cmsList,"tag" => "info")
                                                );
        ?>
        <p>
    </div>
</div>

<!-- Agenda -->
<!-- <div id="agenda" class="col-xs-12 col-md-12 text-center">
    <h1><i class="fa fa-calendar"></i> L'Agenda</h1>
</div> -->
<!-- <center>
    <div class="col-xs-12" style="margin-top: 2%; margin-bottom: 2%;"> -->
        <!-- <?php
        $params = array(
            "cmsList"=>$cmsList,
            "listSteps" => array("1","2","3","4","5"),
            "el" => $el,
            "color1" => "blue"
        );
        echo $this->renderPartial("costum.views.tpls.wizardAgenda",$params);  ?> -->
    <!-- </div>
</center> -->

<!-- Je veux participer --> 
<div style="background: <?= $this->costum["css"]["menuTop"]["background"]; ?>;color: white; " id="participe" class="bloc col-xs-12 col-md-12 text-center">
    <h1><i class="fa fa-connectdevelop"></i> Je veux participer</h1>
</div>

<div class="col-xs-12 col-md-12">
    <div class="container text-center" style="margin-top: 2%; margin-bottom : 2%">
    <p class="f-para"><?= $this->renderPartial("costum.views.tpls.blockCms.textImg.text", array("cmsList" => $cmsList, "tag" => "participate"));
    ?><p>
        </div>
</div>

<?php 
foreach ($cmsList as $e => $v) {
//     // var_dump($v);exit;
    if (!empty($v["type"]) && isset($v["type"])) {

        $params = [
            "cmsList"   =>  $cmsList,
            "blockCms"  =>  $v,
            "page"      =>  $page,
            "canEdit"   =>  $canEdit,
            "type"      =>  $v["type"]
        ];
        echo $this->renderPartial("costum.views.".$v["type"],$params);
    }
}
?>

<!-- ESPACE ADMIN --> 
<?php if(Authorisation::isInterfaceAdmin()){ ?>
    <hr>
    <div class="col-xs-12">
        <center>
            <a href="javascript:;" class="addTpl btn btn-success" data-key="blockevent" data-collection="cms">
                <i class="fa fa-plus"></i> Ajouter un template
            </a>
        </center>
    </div>
<?php } ?>

<script>
 jQuery(document).ready(function() {

    $("#show-bottom-add").click(function(){
        $(".toolbar-bottom-adds").css('display','grid');
    });

    $.each($(".markdown"), function(k,v){
        descHtml = dataHelper.markdownToHtml($(v).html()); 
        $(v).html(descHtml);
    });

    setTimeout(function(){
        $(".coop-wraper").removeClass("col-xs-12");
        $(".coop-wraper").addClass("col-xs-4");
    },2000);

    contextData = <?php echo json_encode($el); ?>;
    var contextId=<?php echo json_encode((string) $el["_id"]); ?>;
    var contextType=<?php echo json_encode($this->costum["contextType"]); ?>;
    var contextName=<?php echo json_encode($el["name"]); ?>;
    contextData.id=<?php echo json_encode((string) $el["_id"]); ?>;

    setTitle(costum.title);
 });
</script>