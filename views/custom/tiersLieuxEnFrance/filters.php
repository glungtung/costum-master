<script type="text/javascript">
	var pageApp=<?php echo json_encode(@$page); ?>;
	var paramsFilter= {
	 	container : "#filters-nav",
	 	filters : {
	 		scope : {
	 			view : "scope",
	 			type : "scope",
	 			action : "scope"
	 		},
	 		typePlace : {
	 			view : "selectList",
	 			type : "tags",
	 			name : "Type",
	 			action : "tags",
	 			list : costum.lists.typePlace
	 		},
	 		services:{
	 			view : "selectList",
	 			type : "tags",
	 			name : "Services",
	 			action : "tags",
	 			list : costum.lists.services
	 		},
	 		manageModel : {
	 			view : "selectList",
	 			type : "tags",
	 			name : "Modèle",
	 			action : "tags",
	 			list : costum.lists.manageModel
	 		},
	 		state : {
	 			view : "selectList",
	 			type : "tags",
	 			name : "Etat",
	 			action : "tags",
	 			list : costum.lists.state
	 		},
	 		spaceSize : {
	 			view : "selectList",
	 			type : "tags",
	 			name : "Taille",
	 			action : "tags",
	 			list : costum.lists.spaceSize
	 		}
	 	}
	};
	 
	function lazyFilters(time){
	  if(typeof searchObj != "undefined" )
	    filterGroup = searchObj.init(paramsFilter);
	  else
	    setTimeout(function(){
	      lazyFilters(time+200)
	    }, time);
	}

	jQuery(document).ready(function() {
		lazyFilters(0);
	});
</script>