	<style>
		a:hover, a:focus{
			text-decoration: none;
			outline: none;
		}

      .project-item{
         min-height: 10em;
      }

      .list-item{
         margin-left: 0.5em;
         padding-top: 0.4em;
         padding-bottom: 0.3em;
      }

      .acteur-item{
         margin-bottom: 1.5em;
      }

      #answerModal{
         z-index: 10000000 !important;
      }

		.text-green-theme{
			color: #7cb927;
		}

      #searchInput {
        background-image: url('/images/search.png'); 
        background-position: 15px center;  
        background-repeat: no-repeat;
        background-color: #f1f1f1;
        border-radius: 30px;
        font-size: 16px; 
        padding: 5px 20px 5px 40px; 
        border: 1px solid #ddd;
        margin-bottom: 12px;
        margin-left: 12px;
    }

	.effectif{
      padding: 0.2em;
      background: #eee;
      border-radius: 0.3em;
    }

		.dash-icon{
			font-size: 2em;
			border-radius: 50%;
			padding: 0.8em;
			margin-top: 0.3em;
			margin-bottom: auto;
			background: #ddd;
		}

		.nav-tabs > li {
		    float:none;
		    display:inline-block;
		    zoom:1;
		}

		.nav-tabs {
		    text-align:center;
		}

		.tabbable-panel {
         border:1px solid #eee;
         padding: 10px;
      }

       /* Default mode */
       .tabbable-line > .nav-tabs {
           border: none;
           margin: 0px;
       }
       .tabbable-line > .nav-tabs > li {
           margin-right: 2px;
       }
       .tabbable-line > .nav-tabs > li > a {
           border: 0;
           margin-right: 0;
           color: #737373;
       }
       .tabbable-line > .nav-tabs > li > a > i {
           color: #a6a6a6;
       }

       .tabbable-line > .nav-tabs > li {
           display: inline-block;
           color: #000;
           text-decoration: none;
       }

       .tabbable-line > .nav-tabs > li::after {
           content: '';
           display: block;
           width: 0;
           height: 4px;
           background: #7cb927;
           margin-bottom: -4px;
           transition: width .3s;
       }

       .tabbable-line > .nav-tabs > li:hover::after {
           width: 100%;
       }

       .tabbable-line > .nav-tabs > li.open {
           border-bottom: 4px solid #7cb927;
       }
       .tabbable-line > .nav-tabs > li.open > a, .tabbable-line > .nav-tabs > li:hover > a {
           border: 0;
           background: none !important;
           color: #333333;
       }
       .tabbable-line > .nav-tabs > li.open > a > i, .tabbable-line > .nav-tabs > li:hover > a > i {
           color: #a6a6a6;
       }
       .tabbable-line > .nav-tabs > li.open .dropdown-menu, .tabbable-line > .nav-tabs > li:hover .dropdown-menu {
           margin-top: 0px;
       }
       .tabbable-line > .nav-tabs > li.active {
           border-bottom: 4px solid #ff6600;
           position: relative;
       }
       .tabbable-line > .nav-tabs > li.active > a {
           border: 0;
           color: #333333;
       }
       .tabbable-line > .nav-tabs > li.active > a > i {
           color: #404040;
       }
       .media{
         border: 1px solid #eee;
         padding: 0.5em;
       }
       .tabbable-line > .tab-content {
           margin-top: -3px;
           background-color: #fff;
           border: 0;
           border-top: 1px solid #eee;
           padding: 13px 0;
       }</style>
   	
   	<?php

   		if($this->costum["contextType"] && $this->costum["contextId"]){
   			$el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );
   		}

   		function count_distinct(&$array, $label){
   			$exist = false;
   			foreach ($array as $k => &$v) {
   				if($v["label"]==$label){
   					$v["number"]++;
   					$exist = true;
   				}
   			}

   			if(!$exist){
   				array_push($array, array("label"=>$label, "number"=>1));
   			}
   		}

   		$acteursId = array();
   		$acteurs = array();
   		$orgaMembers = array();

   		$answers = array();
   		$entreprises = array();
   		$independants = array();
   		$projects = array();

   		$graphe_data = array();
   		$carto_data = array();

   		$is_member = false;

         # Get Events
         $events = PHDB::find("events", array('source.key' => $this->costum["slug"]));

   		# Get members (organization) 
   		$orgaMembers = PHDB::find("organizations", array("source.key"=>$this->costum["slug"]));

   		# Get form parent
   		$form = PHDB::findOne("forms", array("parent.".$this->costum['contextId']=>['$exists'=>true]));
   		$formId = $form['_id']->{'$id'};

   		foreach ($orgaMembers as $km => $aMember) {
   			
   			# Collect statistic data
   			count_distinct($graphe_data, $aMember["category"]);

   			# Get the anwers of a member
   			$myAnswers = PHDB::find("answers", array("source.key"=>$this->costum['slug'], "user" => $aMember["creator"], "draft"=>['$exists' => false ]));

   			foreach ($myAnswers as $key => $answer) {

   					array_push($acteursId, $answer["user"]);

					
						$eliste = array();
						$cliste = array();

						if(isset($answer["answers"]["entreprises"]) && !empty($answer["answers"]["friends"])){
							#print_r($answer["answers"]["entreprises"])
							foreach ($answer["answers"]["entreprises"] as $ek => $ev) {
								array_push($eliste, $ek);
							}
						}

						if(isset($answer["answers"]["friends"]) && !empty($answer["answers"]["friends"])){
							#print_r($answer["answers"]["friends"])
							foreach ($answer["answers"]["friends"] as $ck => $cv) {
								array_push($cliste, $ck);
							}
						}

						$me = PHDB::findOneById("citoyens", $answer["user"]);

						# Parrainage friends
						$pf = array();
						if(count($cliste)!=0){
							$pf = PHDB::findByIds("citoyens", $cliste);
						}

						# Parrainage organizations
						$pe = array();
						if(count($eliste)!=0){
							$pe = PHDB::findByIds("organizations", $eliste);
						}

						$besoins = array();
						if(isset($answer["answers"]["FormActeur2012021_1032_3"]["question31"])){
							$besoins = $answer["answers"]["FormActeur2012021_1032_3"]["question31"];
						}

						$ressources = array();
						if(isset($answer["answers"]["FormActeur2012021_1032_4"]["question41"])){
							$ressources = $answer["answers"]["FormActeur2012021_1032_4"]["question41"];
						}
					
   				}

               $acteurs[$aMember['name']] = array(
                  "member" => $aMember,
                  "citoyen" => $me,
                  "entreprises" => $pe,
                  "friends" => $pf,
                  "besoins" => $besoins,
                  "ressources" => $ressources,
                  "answers" => array(
                     "date" => $answer["source"]["date"],
                     "presentation" => []
                  )
               );
   			}
   		
   			# Get answers
   			$projects = PHDB::find("projects", array("source.key"=>$this->costum['slug']));
   			
   		?>

   			<?php
   					$cssAndScriptFilesModule = array(
   						'/js/default/profilSocial.js'
   					);
   					HtmlHelper::registerCssAndScriptsFiles($cssAndScriptFilesModule, Yii::app()->getModule( "co2" )->getAssetsUrl());
   			?>

   			<div class="container">
   				<br><br>
   				<div class="row text-center">
   					<div class="col-md-4 col-sm-6 col-xs-12">
   						<div class="container-fluid effectif">
   							<div class="row">
   							<div class="col-md-5">
   								<i class="fa text-green-theme fa-group dash-icon"></i>
   							</div>
   							<div class="col-md-7">
   								<h5>Acteurs</h5>
   								<h1 class="text-green-theme"><?= count($orgaMembers) ?></h1>
   							</div>
   						</div>
   						</div>
   					</div>
   					<div class="col-md-4 col-sm-6 col-xs-12">
   						<div class="container-fluid  effectif">
   							<div class="row">
   								<div class="col-md-5">
   									<i class="fa text-green-theme fa-lightbulb-o dash-icon" style="padding-right: 1em; padding-left: 1em;"></i>
   								</div>
   								<div class="col-md-7">
   									<h5>Projets</h5>
   									<h1 class="text-green-theme"><?= count($projects) ?></h1>
   								</div>
   							</div>
   						</div>
   					</div>
   					<div class="col-md-4 col-sm-6 col-xs-12">
   						<div class="container-fluid effectif">
   							<div class="row">
   								<div class="col-md-5">
   									<i class="fa text-green-theme fa-calendar dash-icon"></i>
   								</div>
   								<div class="col-md-7">
   									<h5>Evenements</h5>
   									<h1 class="text-green-theme"><?= count($events) ?></h1>
   								</div>
   							</div>
   						</div>
   					</div>
   				</div>
   			</div>
   <br><br>
   <?php  if(isset($_SESSION["userId"])){ ?>
   	<div class="container">
   		<div class="carto-n row">
   			<div class="col-md-12">
   				<div class="tabbable-panel">
   					<div class="tabbable-line">
   						<ul class="nav nav-tabs text-center">
   							<li class="active">
   								<a href="#tab_default_1" data-toggle="tab">
   								Liste des acteurs </a>
   							</li>
   							<li>
   								<a href="#tab_default_4" data-toggle="tab">
   								Graphique </a>
   							</li>
   							<li>
   								<a href="#tab_default_2" data-toggle="tab">
   								Liste des projets </a>
   							</li>
   						</ul>
   						
   						<div class="tab-content">
   							<div class="tab-pane active" id="tab_default_1">
   								<br>
                           <div class="text-center">
                              <input type="text" id="searchInput" onkeyup="search()" placeholder="Recherche ...">
                           </div>
                           <br>

                           <div class="container">
   									<div class="row" id="actors-list">
   										<?php foreach ($acteurs as $id => $acteur) { ?>
   											<div class="col-12 col-md-6 col-lg-4 acteur-item">
                                       <div class="media">
                                         <div class="media-left pull-left">
                                          <a href="<?php echo Yii::app()->createUrl("/#@".$acteur["member"]["slug"]) ?>" target="_blank">
                                           <img src="<?= (isset($acteur["member"]["profilImageUrl"]))?$acteur["member"]["profilImageUrl"]:Yii::app()->getModule("co2")->assetsUrl.'/images/thumbnail-default.jpg' ?>" class="media-object" style="width:60px">
                                          </a>
                                         </div>
                                         <div class="media-body">
                                             <a href="<?php echo Yii::app()->createUrl("/#@".$acteur["member"]["slug"]) ?>" target="_blank">
                                             <h4 class="media-heading"><?php echo $acteur["member"]["name"]; ?></h4>
                                             <p><?php echo $acteur["member"]["category"]; ?></p>
                                           </a>
                                           <button type="button" onclick="getMemberAnswer('<?php echo $id; ?>')" class="btn btn-default" data-toggle="modal" data-target="#answerModal">Voir réponse</button>
                                         </div>
                                       </div>
                                    </div>
   										<?php } ?>
   									</div>
   								</div>
   								<div class="text-center">
   									<button class="btn btn-default" id="loadMoreAnswers">Afficher plus</button>
   								</div>
   							</div>
   							<div class="tab-pane" id="tab_default_2">
   								<br><br>
									<div class="container grid-container">
										<div class="row">
											<?php foreach($projects as $p => $project) { ?>
												<div class="col-12 col-md-6 col-lg-4 project-item">
                                       <div class="media">
                                          <div class="media-left pull-left">
                                             <a href="<?php echo Yii::app()->createUrl("/#@".$project["slug"]) ?>" target="_blank">
                                                <img src="<?= (isset($project["profilImageUrl"]))?$project["profilImageUrl"]:Yii::app()->getModule("co2")->assetsUrl.'/images/thumbnail-default.jpg' ?>" class="media-object" style="width:60px">
                                             </a>
                                          </div>
                                          <div class="media-body">
                                             <a href="<?php echo Yii::app()->createUrl("/#@".$project["slug"]) ?>" target="_blank">
                                                <h4 class="media-heading"><?php echo $project["name"]; ?></h4>
                                             </a>
                                             <p style="font-size: 11pt">
                                                <i>Porté par : <?php echo $project["parent"][array_keys($project["links"]["contributors"])[0]]["name"] ?></i>
                                             </p>
                                             <button class="btn btn-default" type="button" data-toggle="collapse" data-target="#collapseDescription<?php echo $p; ?>" aria-expanded="false" aria-controls="collapseDescription<?php echo $p; ?>">
                                               Déscription
                                             </button>
                                             <div class="collapse" id="collapseExampleDescription<?php echo $p; ?>">
                                               <div class="well">
                                                 <?php echo isset($project["shortDescription"])?$project["shortDescription"]:"Pas de description"; ?>
                                               </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
										  <?php } ?>
										</div>
									</div>
								   <div class="text-center">
   								   <button class="btn btn-default" id="loadMoreProjects">Afficher plus</button>
   								</div>
   							</div>

   							<div class="tab-pane" id="tab_default_4">
   								<br><br>
   								<canvas id="bar-chart" width="800" height="250"></canvas>
   							</div>
							</div>
						</div>
					</div>
				</div>
			</div>
   	</div>
   </div>

   <!-- Modal Answers-->
   <div id="answerModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header" style="border-bottom: none;">
           <button type="button" class="close" data-dismiss="modal">&times;</button>
           <h4 class="modal-title">Réponse sur <?php echo $form["name"] ?> </h4>
         </div>
         <div class="modal-body">
            <div class="tabbable-panel">
               <div class="tabbable-line">
                  <ul id="answerTab" class="nav nav-tabs text-center">
                     <li class="active">
                        <a href="#tab_answer_1" data-toggle="tab">
                        Parrainage </a>
                     </li>
                     <li>
                        <a href="#tab_answer_2" data-toggle="tab">
                        Besoins </a>
                     </li>
                     <li>
                        <a href="#tab_answer_3" data-toggle="tab">
                        Ressources</a>
                     </li>
                  </ul>
                  
                  <div id="answerContent" class="tab-content">
                     <div class="tab-pane active" id="tab_answer_1">
                        
                     </div>
                     <div class="tab-pane" id="tab_answer_2">
                        
                     </div>
                     <div class="tab-pane" id="tab_answer_3">
                        
                     </div>
                  </div>
               </div>      
            </div>
         </div>
         <div class="modal-footer"  style="border-top: none;">
           <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
         </div>
       </div>

     </div>
   </div>

	<!--script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script-->
	<script src="/plugins/Chart-2.6.0/Chart.js"></script>
	<script type="text/javascript">

      let acteurs = <?php echo json_encode($acteurs) ?>;
      
		$(document).ready(function(){

         // Hide read more button
         $(".read-more").hide();

		    var ctx = $("#bar-chart");

		new Chart(ctx, {
			type: 'pie',
			data: {
				labels: <?=json_encode(array_column($graphe_data, "label"))?>,
				datasets: [
					{
						label: "Acteurs",
						backgroundColor: ["#CCE428", "#FEEF33", "#FBCBA9", "#F69854","#CCE428", "#e84c3d", "#f1c40f"],
						data: <?=json_encode(array_column($graphe_data, "number"))?>
					}
				]
			},
			options: {
				responsive: true,
				legend: { 
					display: true,
					position: "bottom",
					align: "center"
				},
				title: {
					display: true,
					text: 'Type des acteurs \n'
				},
				cutoutPercentage: 50
			}
		});

		
		
			var x=10;
			var answer_size = $(".afn-card").length;

			(answer_size > x)? $("#loadMoreAnswers").show(): $("#loadMoreAnswers").hide();

			$(".afn-card").hide();
			$(".afn-card:lt("+x+")").show();

			$('#loadMoreAnswers').click(function () {
				x= (x+5 <= answer_size)? x+5: answer_size;
				$('.afn-card:lt('+x+')').show();
			});


			var x=10;
			var answer_size = $(".pfn-card").length;

			(answer_size > x)? $("#loadMoreProjects").show(): $("#loadMoreProjects").hide();

			$(".pfn-card").hide();
			$(".pfn-card:lt("+x+")").show();

			$('#loadMoreProjects').click(function () {
				x= (x+5 <= answer_size)? x+5: answer_size;
				$('.pfn-card:lt('+x+')').show();
			});



		$('.effectif h1').each(function () {
			$(this).prop('Counter',0).animate({
				Counter: $(this).text()
			}, {
				duration: 900,
				easing: 'swing',
				step: function (now) {
					$(this).text(Math.ceil(now));
				}
			});
		});
	});

   function search() {
      var input, filter, liste, item, i, txtValue;
      input = $('#searchInput');
      filter = input.val().toUpperCase();
      liste = $("#acteurs-list");
      item = $('.answer-list');

      for (i = 0; i < item.length; i++) {
         paragraph = item[i].getElementsByTagName("p")[0];
         txtValue = paragraph.textContent || paragraph.innerText;
         if (txtValue.toUpperCase().indexOf(filter) > -1) {
            item[i].style.display = "";
         } else {
            item[i].style.display = "none";
         }
      }
   }

   function getMemberAnswer(memberId){
      
      let memberAnswer = null, friends = null, organizations=null, besoins = null, ressources = null;

      memberAnswer = acteurs[memberId];

      // alert(JSON.stringify(memberAnswer));
      
      friends = memberAnswer["friends"];
      organizations = memberAnswer["entreprises"];
      besoins = memberAnswer["besoins"];
      ressources = memberAnswer["ressources"];

      $("#tab_answer_1").empty();
      $("#tab_answer_2").empty();
      $("#tab_answer_3").empty();

      for (const [key, friend] of Object.entries(friends)) {
         const listItem = `
            <div class="list-item">
               <div><i class="fa fa-user text-success"></i> ${friend["name"]}</div>
            </div>
         `;

         $("#tab_answer_1").append(listItem);
      }; 

      for (const [key, orga] of Object.entries(organizations)) {

         const listItem = `
            <div class="list-item">
               <div><i class="fa fa-group text-success"></i> ${orga["name"]}</div>
            </div>
         `;

         $("#tab_answer_1").append(listItem);
      }; 

      for (var i = besoins.length - 1; i >= 0; i--) {
         const listItem = `
            <div class="list-item">
               <div><i class="fa fa-arrow-right text-success"></i> ${besoins[i]["liste_row"]+" (ajouté le "+besoins[i]["date"]+")"}</div>
            </div>
         `;

         $("#tab_answer_2").append(listItem);
      }

      for (var i = ressources.length - 1; i >= 0; i--) {
         const listItem = `
            <div class="list-item">
               <div><i class="fa fa-arrow-right text-success"></i>  ${ressources[i]["liste_row"]+" (ajouté le "+ressources[i]["date"]+")"}</div>
            </div>
         `;

         $("#tab_answer_3").append(listItem);
      }
   }
</script>
<?php }else{ ?>
	<div class="container">
		<hr>
	</div>
	<div class="text-center">
		<h4 class="text-light text-center">Veuillez vous connecter pour plus d'informations</h4>
		<br>
		<div>ou</div>
		<br>
		<button class="btn btn-danger btn-lg" data-toggle="modal" data-target="#modalRegister">
			<i class="fa fa-plus-circle"></i> Créer Un Compte <b>Citoyen</b>
		</button>
	</div>
<?php } ?>
