<style type="text/css">
	.container {
		background: #f8f8f8;
		font-family: Helvetica, Arial, Verdana, sans-serif;
		font-size: 20px;
	}
	#bg-homepage{
		width: 100%;
		border-top: 1px solid #ccc;
	}
	.card {
		/* Add shadows to create the "card" effect */
		box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
		transition: 0.3s;
		background: #fff;
		margin-bottom: 20px;
		border-radius: 10px;
		width: 99%;
		min-height: 250px;
		text-align: center;
	}
	.card-title{
		padding:20px;
		font-size: 25px;
	}
	.row{
		margin-top: 30px;
	}
	.h1{
		background: #ccc;
	}
	.well .label-question{
		background-color: #d6d7cf;
		width: fit-content;
		padding: 10px 20px;
		border-radius: 20px;
		text-align: inherit;
		margin: auto;
		margin-top: -30px;
		color: #5a656e;
		font-size: 20px;
	}
	.well .paragraph {
		padding-top: 15px;
		font-size: 20px;
	}
	h4{
		text-align: center;
		font-size: 30px;

	}
	p {
		font-weight: bold;
	}
	
	table
	{
		border-collapse: collapse;
		border-width:1px; 
		border-style:solid; 
		border-color:#ddd;
		width:100%;
		margin-bottom: 20px;
		margin-top: 15px; 
	}
	td {
		border: 1px solid #ddd;
		padding: 8px;
		text-align: center;
	}
	th {
		padding-bottom: 12px;
		text-align: center;
		border: 1px solid #ddd;
	}
	

	hr {
		display: block;
		clear: both;
		font-family: arial;
		text-align: center;
		line-height: 1;
		border: dashed 0.5px #b7b8b5;
	}
	.card1 {
		/* Add shadows to create the "card" effect */
		box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
		transition: 0.3s;
		margin-bottom: 20px;
		border-radius: 10px;
		width: 99%;
		min-height: 250px;
	}

</style>
<div >
<?php 
$ficheId = PHDB::findOne(Form::COLLECTION, array('id' =>$this->costum["formId"]));
	//var_dump($ficheId);
$reponse = PHDB::find(Form::ANSWER_COLLECTION,array('user' =>Yii::app()->session["userId"], "form" =>(string)$ficheId["_id"] ));
//var_dump($reponse);
$fiche = $reponse["5f3a4f83c629700c008b4573"];
//$fiche = $reponse[$dataId];
//var_dump($fiche);exit();
$reponseFiche = $fiche["answers"];
//var_dump($reponseFiche);exit();

/********** FICHE PROJET****/
$ficheProjet = isset($reponseFiche["projet2872020_1530_0"])?$reponseFiche["projet2872020_1530_0"]:"";
$nomProjet = isset($ficheProjet["projet2872020_1530_01"])?$ficheProjet["projet2872020_1530_01"]:"";
$slogan = isset($ficheProjet["projet2872020_1530_02"])?$ficheProjet["projet2872020_1530_02"]:"";
//$referent = isset($ficheProjet["projet2872020_1530_03"])?$ficheProjet["projet2872020_1530_03"]:"";
$description = isset($ficheProjet["projet2872020_1530_04"])?$ficheProjet["projet2872020_1530_04"]:"";
$cibleCoeur = isset($ficheProjet["projet2872020_1530_05"])?$ficheProjet["projet2872020_1530_05"]:"";
$cibleElargie = isset($ficheProjet["projet2872020_1530_06"])?$ficheProjet["projet2872020_1530_06"]:"";
//$budgetGlobal = isset($ficheProjet["projet2872020_1530_07"])?$ficheProjet["projet2872020_1530_07"]:"";
$planning = isset($ficheProjet["projet2872020_1530_09"])?$ficheProjet["projet2872020_1530_09"]:"";
//$action =$ficheProjet["projet2872020_1530_11"];
//$tache = isset($ficheProjet["projet2872020_1530_12"])?$ficheProjet["projet2872020_1530_12"]:"";
//13 adresse
$commentaire = isset($ficheProjet["projet2872020_1530_14"])?$ficheProjet["projet2872020_1530_14"]:"";
//var_dump($ficheProjet);


/****** GRAPHISME ***/
$graphisme = isset($reponseFiche["projet2972020_164_1"])?$reponseFiche["projet2972020_164_1"]:"";
//$tonalite = isset($graphisme["projet2972020_164_12"])?$graphisme["projet2972020_164_12"]:"";
//$valeurTransm = isset($graphisme["projet2972020_164_13"])?$graphisme["projet2972020_164_13"]:"";
//$elementRecur = isset($graphisme["projet2972020_164_15"])?$graphisme["projet2972020_164_15"]:"";
//$elementDisparait = isset($graphisme["projet2972020_164_16"])?$graphisme["projet2972020_164_16"]:"";
//$idee = isset($graphisme["projet2972020_164_17"])?$graphisme["projet2972020_164_17"]:"";
//$elementAFournir = isset($graphisme["projet2972020_164_18"])?$graphisme["projet2972020_164_18"]:"";
//var_dump($elementRecur);


/**** DEVELOPPEMENT*/
$dev = isset($reponseFiche["projet2972020_187_2"])?$reponseFiche["projet2972020_187_2"]:"";
//$moduleNec = isset($dev["projet2972020_187_21"])?$dev["projet2972020_187_21"]:"";
$objectif = isset($dev["projet2972020_187_25"])?$dev["projet2972020_187_25"]:"";
//var_dump($dev); exit();


/**** ANIMATION***/
$animation = isset($reponseFiche["projet3072020_1652_3"])?$reponseFiche["projet3072020_1652_3"]:"";
$publiCible = isset($animation["projet3072020_1652_31"])?$animation["projet3072020_1652_31"]:"";
//$moyenMateriel = isset($animation["projet3072020_1652_32"])?$animation["projet3072020_1652_32"]:"";
$dateAnimation = isset($animation["projet3072020_1652_34"])?$animation["projet3072020_1652_34"]:"";
//var_dump($animation);exit();


/** DOCUMENT**/
$documentsId = PHDB::find(Document::COLLECTION);
$cahierDeCharge = Document::getListDocumentsWhere(array(
	"id"=>(string)$fiche["_id"], 
	"type"=>'answers',
	"subKey"=>"projet2872020_1530_0.projet2872020_1530_10"), "file");

$charteGRaphisme = Document::getListDocumentsWhere(array(
	"id"=>(string)$fiche["_id"], 
	"type"=>'answers',
	"subKey"=>"projet2972020_164_1.projet2972020_164_11"), "file");
$logo = Document::getListDocumentsWhere(array(
	"id"=>(string)$fiche["_id"], 
	"type"=>'answers',
	"subKey"=>"projet2972020_187_2.projet2972020_187_22"), "file");
$ressourceGraphique = Document::getListDocumentsWhere(array(
	"id"=>(string)$fiche["_id"], 
	"type"=>'answers',
	"subKey"=>"projet2972020_187_2.projet2972020_187_23"), "file");
$contenu = Document::getListDocumentsWhere(array(
	"id"=>(string)$fiche["_id"], 
	"type"=>'answers',
	"subKey"=>"projet2972020_187_2.projet2972020_187_24"), "file");

	/*
				foreach ($logo as $key => $value) {
					echo "<img src='".$value["docPath"]."'height=250>";
				}*/?>
				<div class="container ">
					<div class="col-md-12 col-sm-12 col-xs-12 ">
						<a href="#answer.index.id.<?php echo ((string)$fiche['_id'])?>.mode.w" class="btn btn-danger lbh"> edit </a>
						<h1 class="letter-turq text-center"> Fiche projet : <?php echo $nomProjet?></h1>

						<div class=" well ">
							<p class="paragraph text-center" ><?php echo $description ?> </p>
						</div>
					</div>

					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-4 padding-20" >
							
							<div>
								<u><p>Slogan: </p></u>
								<?php echo $slogan ?>
							</div>
							<hr>
							<div >
								<u><p>Cible coeur: </p></u>
								<?php echo $cibleCoeur ?>
							</div>
							<hr>
							<div >
								<u><p>Cible élargie:</p></u>
								<?php echo $cibleElargie ?>
							</div>
							<hr>
							<div>
								<p> Actions à mettre en oeuvre : </p>
								<li> 
									<?php 
									if(isset($ficheProjet["projet2872020_1530_11"])){
										foreach ($ficheProjet["projet2872020_1530_11"] as $keyA => $valueA) {
											echo ($valueA["liste_row"]);

										}
									}?>
								</li>
							</div>
							<hr>
							<div>
								<p>Liste des tâches et besoins :</p>
								<?php 
								if(isset($ficheProjet["projet2872020_1530_12"])){
									foreach ($ficheProjet["projet2872020_1530_12"] as $keyT => $valueT) {?>
										<li> 
											<?php echo ($valueT["liste_row"]);

										}
									}?>
								</li>
							</div>
						</div>
						<div class="col-md-8 col-sm-8 col-xs-8 padding-20" >
							<p> Référents </p>
							<table>
								<tr>
									<th>Nom</th>
									<th>Email</th>
									<th>Type</th>
								</tr>
								<?php 
								if(isset($ficheProjet["projet2872020_1530_03"])){
									foreach ($ficheProjet["projet2872020_1530_03"] as $q => $a) {
										?>
										<tr>
											<td> <?php echo $a['nom']; ?> </td>
											<td>  <?php echo $a['email']; ?></td>
											<td> <?php echo $a['type']; ?></td>
										</tr>
										<?php
									}
								}
								?>
							</table>


							<p> Budget global :
								<table>
									<tr>
										<th>Groupe</th>
										<th>Nature de l'action</th>
										<th>Poste de dépense</th>
										<th>Montant</th>
									</tr>
									<?php if(isset($ficheProjet["projet2872020_1530_07"])){
										foreach ($ficheProjet["projet2872020_1530_07"] as $q => $a) {
											?>
											<tr>
												<td> <?php echo $a['group']; ?> </td>
												<td>  <?php echo $a['nature']; ?></td>
												<td> <?php echo $a['poste']; ?> </td>
												<td>  <?php echo $a['price']; ?></td>
											</tr>
											<?php
										}
									}?>
								</table>
							</p>

						</div>
					</div>
					<div class=" well ">
					<p class="label-question">Commentaires de la personne ayant lancé le projet:</p>
					<p class="paragraph"> <?php echo $commentaire?></p>
				</div>




					<h4 >Spécifique pour Graphisme</h4>
					<p> Charte graphique: <?php foreach ($charteGRaphisme as $key => $value) {
						echo "<a src='".$value["docPath"]."'> ".$value["name"]."
						</a>";

					}?>
				</p>
				<div class="row">
					<div class="col-md-4">
						<div  class="card1"  >

							<div class="col-md-11">
								<p>Tonalité :</p>
								<?php 
								if(isset($graphisme["projet2972020_164_12"])){
									foreach ($graphisme["projet2972020_164_12"] as $keyTo => $valueTo) {?>
										<li> 
											<?php echo ($valueTo["liste_row"]);
										}
									}
									?>
								</li>
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div  class="card1"  >
							<p>Valeurs à transmettre:</p>
							<?php 
							if(isset($graphisme["projet2972020_164_13"])){
								foreach ($graphisme["projet2972020_164_13"] as $keyTr => $valueTr) {?>
									<li> 
										<?php echo ($valueTr["liste_row"]);
									}
								}
								?>
							</li>
						</div>
					</div>
				</div>

				<p> Si des éléments existent déjà, qu’est-ce qui doit être gardé ?</p>
				<div class="row">
					<div class="col-md-6">
						<div  class="card1"  >

							<div class="col-md-11">
								<p> Eléments récurrents :</p>
								<?php
								if(isset($graphisme["projet2972020_164_15"])){
									foreach ($graphisme["projet2972020_164_15"] as $keyEr => $valueEr) {?>
										<li> 
											<?php echo ($valueEr["liste_row"]);
										}
									}
									?>
								</li>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div  class="card1"  >
							<p> Eléments qui peuvent disparaître : </p>
							<?php
							if(isset($graphisme["projet2972020_164_16"])){
								foreach ($graphisme["projet2972020_164_16"] as $keyEd => $valueEd) {?>
									<li> 
										<?php echo ($valueEd["liste_row"]);
									}
								}
								?>
							</li>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div  class="card1"  >

							<div class="col-md-11">
								<p>Idée qu'on peut voir en visuel:</p>
								<?php
								if(isset($graphisme["projet2972020_164_17"])){
									foreach ($graphisme["projet2972020_164_17"] as $keyI => $valueI) {?>
										<li> 
											<?php echo ($valueI["liste_row"]);
										}
									}
									?>
								</li>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div  class="card1"  >
							<P>Elément à fournir:</P>
							<?php
							if(isset($graphisme["projet2972020_164_18"])){
								foreach ($graphisme["projet2972020_164_18"] as $keyEf => $valueEf) {?>
									<li> 
										<?php echo ($valueEf["liste_row"]);
									}
								}
								?>
							</li>
						</div>
					</div>
				</div>
				<h4> Spécifique pour les développement</h4>
				
				<div class="row">
					<div class="col-md-4">
						<p>
							Logo : 
							<?php foreach ($logo as $key => $value) {
								echo "<img src='".$value["docPath"]."'height=250>";
							}?>

						</p>

					</div>
					<div class="col-md-8">
						<p>Ressources graphiques : 
							<?php foreach ($ressourceGraphique as $keyG => $valueG) {
								echo "<a src='".$valueG["docPath"]."'> ".$valueG["name"]."
								</a>";
							}?> 
						</p>
						<p>Contenu : 
							<?php foreach ($contenu as $keyC => $valueC) {
								echo "<a src='".$valueC["docPath"]."'> ".$valueC["name"]."
								</a>";
							}?> 
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div  class="card1"  >

							<div class="col-md-11">
								<p> Modules nécessaires :</p>
								<?php
								if(isset($dev["projet2972020_187_21"])){
									foreach ($dev["projet2972020_187_21"] as $keyM => $valueM) {?>
										<li> 
											<?php echo ($valueM["liste_row"]);
										}
									}
									?>
								</li>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div  class="card1"  >
							<p>Objectif : <?php echo $objectif?></p>
						</div>
					</div>
				</div>

				<h4>Spécifique pour des animations</h4>
				<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-4 padding-20" >
							<div>
								<p>Date de l'animation: </p>
									<?php echo $dateAnimation?>
							</div>
							<hr>
					<div>
				<p>Public cible:</p> 
				<?php $publiCible?>

			</div>

			<hr>
		</div>
		<div class="col-md-8 col-sm-8 col-xs-8 padding-20">
				<p>Moyens matériels:</p>
				<?php
				if(isset($animation["projet3072020_1652_32"])){
					foreach ($animation["projet3072020_1652_32"] as $keyMm => $valueMm) {?>
						<li> 
							<?php echo ($valueMm["liste_row"]);
						}
					}
					?>
				</li>
				
			</div>
		</div>
	</div>

</div>