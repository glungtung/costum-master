adminPanel.views.ressources=function(){
	var data={
		title : "Liste des ressources",
		// types : [ "poi" ],
		// forced : {
		// 	"type" : "indicator"
		// },
		table : {
            name: {
                name : "Nom",
                preview : true
            },
            section: {
            	name : "Offres/besoins"
            },
            category: {
            	name : "Catégorie"
            },
            subtype : {
            	name : "sous-catégorie"
            }
            
        },
		paramsFilter : {
			container : "#filterContainer",
			defaults : {
				types : [ "classifieds" ],
				//private : true
			},
			filters : {
				text : true
			}
		},
        actions : {
        	private : true,
        	delete : true
        }
	};

	ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
};

