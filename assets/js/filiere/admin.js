adminPanel.views.badges = function(){
	var data={
		title : "Badges",
		paramsFilter : {
            container : "#filterContainer",
            defaults : {
                types : [ "badges"]
            },
            filters : {
                text : true
            
            }
        },
		table : {
			type:{
				name : "Type",
				notLink : true
			},
			name: {
                name : "Nom",
                notLink : true
            },
			description:{
                name : "Description"
            },
            category:{
                name : "Categorie"
            },
            tags:{
                name : "Mots clés"
            }
		}
	};

	if(typeof costum.isCostumAdmin != "undefined" && costum.isCostumAdmin){
		data.actions={
			update : true,
			delete : true
		};
	}
	
	ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
};
