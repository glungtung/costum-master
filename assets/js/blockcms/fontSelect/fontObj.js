var fontObj = fonts;

/*var fontStyles = "<style>";
var fontOptions = "";
$.each(fontObj,function(k,v){
	v = v.split(" ").join("_");
	fontStyles += `
	    @font-face {
	        font-family: "${v}";
	        src: url("${costumAssetsPath}/font/blockcms/fontpack/${k}");
	  }
	`;
	fontOptions +=`<option style="font-family:'${v.split(' ').join('_')}'" value="${k}">${v}</option>`;
});
fontStyles += "</style>";

$(function(){
	$("head").append(fontStyles);
})*/

var fontStyles = "<style>";
var fontOptions = "";
if(fonts != null)
$.each(fonts,function(k,v){
	v = v.split(" ").join("_");
	fontStyles += `
	    @font-face {
	        font-family: "${v}";
	        src: url("${costumAssetsPath+k}");
	  }
	`;
	fontOptions +=`<option style="font-family:'${v.split(' ').join('_')}'" value="${v.split(' ').join('_')}">${v}</option>`;
});
fontStyles += "</style>";

$(function(){
	$("head").append(fontStyles);
})