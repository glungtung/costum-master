var widthLabel = {
  "modeLg":"Larges",
  "modeMd":"Desktops",
  "modeSm":"Tablettes",
  "modeXs":"Phones"
},marginLabel = {
  "marginTop":"Haut",
  "marginBottom":"Bas",
  /*"marginLeft":"Gauche",
  "marginRight":"Droite"*/
},paddingLabel = {
  "paddingTop":"Haut",
  "paddingBottom":"Bas",
  "paddingLeft":"Gauche",
  "paddingRight":"Droite"
},
blockCmsBgLabel = {
"blockCmsBgTarget" : "Cible",
"blockCmsBgType" : "Type",
"blockCmsBgColor": "Couleur",
"blockCmsBgImg"  : "Image",
"blockCmsBgPaint": "Paint",
"blockCmsBgSize"  : "Taille",
"blockCmsBgPosition"  : "Position",
"blockCmsBgRepeat"  : "Repétition",
},
blockCmsPoliceLabel = {
  "blockCmsPoliceTitle1" : "Titre 1",
  "blockCmsPoliceTitle2" : "Titre 2",
  "blockCmsPoliceTitle3" : "Titre 3",
  "blockCmsPoliceTitle4" : "Titre 4",
  "blockCmsPoliceTitle5" : "Titre 5",
  "blockCmsPoliceTitle6" : "Titre 6",
},
blockCmsTextSizeLabel = {
"blockCmsTextSizeTitle1" : "Titre 1 (px)",
"blockCmsTextSizeTitle2" : "Titre 2 (px)",
"blockCmsTextSizeTitle3" : "Titre 3 (px)",
"blockCmsTextSizeTitle4" : "Titre 4 (px)",
"blockCmsTextSizeTitle5" : "Titre 5 (px)",
"blockCmsTextSizeTitle6" : "Titre 6 (px)",
},
blockCmsTextLineHeightLabel = {
"blockCmsTextLineHeightTitle1" : "Titre 1",
"blockCmsTextLineHeightTitle2" : "Titre 2",
"blockCmsTextLineHeightTitle3" : "Titre 3",
"blockCmsTextLineHeightTitle4" : "Titre 4",
"blockCmsTextLineHeightTitle5" : "Titre 5",
"blockCmsTextLineHeightTitle6" : "Titre 6",
},
blockCmsTextUnderlineLabel = {
"blockCmsUnderlineTitle1": "Titre 1 (T1)",
"blockCmsUnderlineTitle2": "Titre 2 (T2)",
"blockCmsUnderlineTitle3": "Titre 3 (T3)",
"blockCmsUnderlineTitle4": "Titre 4 (T4)",
"blockCmsUnderlineTitle5": "Titre 5 (T5)",
"blockCmsUnderlineTitle6": "Titre 6 (T6)",

"blockCmsUnderlineColorTitle1" : "Couleur T1",
"blockCmsUnderlineColorTitle2" : "Couleur T2",
"blockCmsUnderlineColorTitle3" : "Couleur T3",
"blockCmsUnderlineColorTitle4" : "Couleur T4",
"blockCmsUnderlineColorTitle5" : "Couleur T5",
"blockCmsUnderlineColorTitle6" : "Couleur T6",

"blockCmsUnderlineWidthTitle1" : "Taille T1",
"blockCmsUnderlineWidthTitle2" : "Taille T2",
"blockCmsUnderlineWidthTitle3" : "Taille T3",
"blockCmsUnderlineWidthTitle4" : "Taille T4",
"blockCmsUnderlineWidthTitle5" : "Taille T5",
"blockCmsUnderlineWidthTitle6" : "Taille T6",


"blockCmsUnderlineHeightTitle1" : "Hauteur T1",
"blockCmsUnderlineHeightTitle2" : "Hauteur T2",
"blockCmsUnderlineHeightTitle3" : "Hauteur T3",
"blockCmsUnderlineHeightTitle4" : "Hauteur T4",
"blockCmsUnderlineHeightTitle5" : "Hauteur T5",
"blockCmsUnderlineHeightTitle6" : "Hauteur T6",

"blockCmsUnderlineSpaceTitle1" : "Espace entre T1",
"blockCmsUnderlineSpaceTitle2" : "Espace entre T2",
"blockCmsUnderlineSpaceTitle3" : "Espace entre T3",
"blockCmsUnderlineSpaceTitle4" : "Espace entre T4",
"blockCmsUnderlineSpaceTitle5" : "Espace entre T5",
"blockCmsUnderlineSpaceTitle6" : "Espace entre T6",

"blockCmsUnderlineMargeBottomTitle1" : "Marge en bas T1",
"blockCmsUnderlineMargeBottomTitle2" : "Marge en bas T2",
"blockCmsUnderlineMargeBottomTitle3" : "Marge en bas T3",
"blockCmsUnderlineMargeBottomTitle4" : "Marge en bas T4",
"blockCmsUnderlineMargeBottomTitle5" : "Marge en bas T5",
"blockCmsUnderlineMargeBottomTitle6" : "Marge en bas T6",
},
blockCmsBorderLabel = {
  "blockCmsBorderTop" : "Haut",
  "blockCmsBorderBottom" : "Bas",
  "blockCmsBorderLeft" : "Gauche",
  "blockCmsBorderRight": "Droite",
  "blockCmsBorderColor": "Couleur",
  "blockCmsBorderWidth": "épaisseur",
  "blockCmsBorderType": "Types"
},
blockCmsLineSeparatorLabel = {
  "blockCmsLineSeparatorTop" : "Haut",
  "blockCmsLineSeparatorBottom" : "Bas",
  "blockCmsLineSeparatorWidth" : "Longueur(%)",
  "blockCmsLineSeparatorHeight" : "Hauteur",
  "blockCmsLineSeparatorBg" : "Couleur",
  "blockCmsLineSeparatorPosition" : "Position",
  "blockCmsLineSeparatorIcon" : "Petite icône au milieu de la ligne(optionnel) :"
},
blockCmsTextAlignLabel = {
"blockCmsTextAlignTitle1" : "Titre 1",
"blockCmsTextAlignTitle2" : "Titre 2",
"blockCmsTextAlignTitle3" : "Titre 3",
"blockCmsTextAlignTitle4" : "Titre 4",
"blockCmsTextAlignTitle5" : "Titre 5",
"blockCmsTextAlignTitle6" : "Titre 6",

},
blockCmsColorLabel = {
  "blockCmsColorTitle1" : "Titre 1",
  "blockCmsColorTitle2" : "Titre 2",
  "blockCmsColorTitle3" : "Titre 3",
  "blockCmsColorTitle4" : "Titre 4",
  "blockCmsColorTitle5" : "Titre 5",
  "blockCmsColorTitle6" : "Titre 6",
},
checkboxSimpleParams={
  "onText" : "Oui",
  "offText" : "Non",
  "onLabel" : "Oui",
  "offLabel" : "Non",
  "labelText" : "label"
}

var fieldsetClass=[];
function lazyWelcomeDyFObj(time){
    if(typeof dyFObj != "undefined" && typeof dyFObj.openForm != "undefined")
       surchargeDyFObj();
    else
      setTimeout(function(){
        lazyWelcomeDyFObj(time+200)
      }, time);
}

/*surcharge openForm*/
function surchargeDyFObj(){
  dyFObj.openForm = function  (type, afterLoad,data, isSub, dynFormCostumIn,mode) { 
      dyFObj.dynFormCostum = null; //was some times persistant between forms 
      mylog.warn("openForm","--------------- Open Form ",type, afterLoad,data, isSub, dynFormCostumIn);
      $.unblockUI();
      $("#openModal").modal("hide");
      
      mylog.dir(data);
      uploadObj.contentKey="profil"; 
      if(notNull(data)){
        if(typeof data.images != "undefined")
          uploadObj.initList.image=data.images;
        if(typeof data.files != "undefined" )
          uploadObj.initList.file=data.files;

        data = dyFObj.prepData(data);

      }else{
        uploadObj.initList={};
      }
      dyFObj.activeElem = (isSub) ? "subElementObj" : "elementObj";
      dyFObj.activeModal = (isSub) ? "#openModal" : "#ajax-modal";

      if( typeof dynFormCostumIn != "undefined"){
        mylog.warn("openForm", "dynFormCostum",dynFormCostumIn);
        dyFObj.dynFormCostum = dynFormCostumIn;
      } else {
        mylog.warn("openForm", "no dynFormCostum");
      }

      if(notNull(finder))
        finder.initVar();

      if(notNull(scopeObj))
        scopeObj.selected = {};

      dyFInputs.locationObj.initVar();

      if(dyFObj.unloggedMode || userId)
      {
        if(typeof formInMap != 'undefined')
          formInMap.formType = type;

        if(typeof dyFObj.formInMap != 'undefined')
          dyFObj.formInMap.formType = type;

        dyFObj.getDynFormObj(type, function() { 
          dyFObj.startBuild(afterLoad,data);
        },afterLoad, data, dynFormCostumIn);
      } else {
        dyFObj.openFormAfterLogin = {
          type : type, 
          afterLoad : afterLoad,
          data : data
        };
        toastr.error(tradDynForm.mustbeconnectforcreateform);
        $('#modalLogin').modal("show");
      }

      if (typeof type.jsonSchema != "undefined" && type.jsonSchema.properties != "undefined" && isInterfaceAdmin) {
        //setTimeout(function(){
          createFieldsets(type.jsonSchema.properties);
          $(".fieldsetblockCmsPolice").before(`<div class="col-xs-12">
            <button type="button" class="btn  bg-green-k text-white more-config"><i class="fa fa-arrow-down"></i>&nbsp;Plus de config <i class="fa fa-arrow-down"></i></button>
            <button type="button" class="btn  btn-block bg-green-k text-white less-config" style='display:none'><i class="fa fa-arrow-up"></i>&nbsp;Cacher <i class="fa fa-arrow-up"></i></button>
            </div>`);
          $('.more-config').click(function(){
            $(this).hide();
            $('.less-config').show();
            $('.fieldset.toHide').css({
              "margin": "15px 0 15px 0",
              "padding":"15px 0px 15px 0",
              "visibility" :"visible",
              "height" : "auto", 
            });
          })
          $('.less-config').click(function(){
            $(this).hide();
            $('.more-config').show();
            $('.fieldset.toHide').css({
              "margin": "0px",
              "padding":"0px",
              "visibility" :"hidden",
              "height" : "0px",
            });
          })

          //toogle background choice
          $('#blockCmsBgType').click(function(){
            if($(this).val() == 'color'){
              $(".blockCmsBgColorcolorpicker").fadeIn("slow");
              $(".blockCmsBgPaintselect,.blockCmsBgSizeselect,.blockCmsBgPositionselect,.blockCmsBgRepeatselect").fadeOut("slow");
              $(".blockCmsBgImguploader").css({
                  'visibility':'hidden',
                  'height' : '0px'
              });
            }
            if($(this).val() == 'image'){
              $(".blockCmsBgImguploader").css({
                  'visibility':'visible',
                  'height' : 'auto'
              });
              $(".blockCmsBgColorcolorpicker,.blockCmsBgPaintselect").fadeOut("slow");
              $('.blockCmsBgSizeselect,.blockCmsBgPositionselect,.blockCmsBgRepeatselect').fadeIn("slow");
            }
            if($(this).val() == 'paint'){
              $(".blockCmsBgPaintselect,.blockCmsBgSizeselect,.blockCmsBgPositionselect,.blockCmsBgRepeatselect").fadeIn("slow");
              $(".blockCmsBgColorcolorpicker").fadeOut("slow");
              $(".blockCmsBgImguploader").css({
                  'visibility':'hidden',
                  'height' : '0px'
              });
            }
          })

          
          $("select[id^='blockCmsPoliceTitle'],select[id*=' blockCmsPoliceTitle']").append(fontOptions);
          $('#blockCmsLineSeparatorIcon').append(fontAwesomeOptions);
          $(".fieldset.fieldsetmode").append("<div class='info tooltips' data-toggle='tooltip' data-placement='left' data-html='true' data-original-title='- (12) occupe toute la largeur.<br>- (6) occupe la moitié de la largeur.'><i class='fa fa-question-circle'></i></div>");
          $('.tooltips').tooltip();
          paperPaintObj.init();

        //},1000)   
      }
    }
}

function alignInput2(sectionDyfProperties,beginWith,column,columnXs,offsetBefore=null,offsetRepeat=null,label="",color="",hidden=""){
  var count=0;
  var entry = beginWith;
  var beginWith = new RegExp('^'+beginWith);
  var arrayClass = [];
  var styleWrapped="";
  $.each(sectionDyfProperties,function(k,v){      
    if(beginWith.test(k)){
      count++;
      arrayClass.push('.'+k+v.inputType);
      $('.'+k+v.inputType+' .fa-chevron-down').hide();

      if(count==1 && offsetBefore != null){
          $('.'+k+v.inputType).removeClass('form-group').addClass('col-xs-'+columnXs+' col-md-'+column+' col-md-offset-'+offsetBefore);
      }else{
          $('.'+k+v.inputType).removeClass('form-group').addClass('col-xs-'+columnXs+' col-md-'+column+' col-md-offset-'+offsetRepeat);
      }        
    }
  });
  if(hidden=="hidden")
    $(arrayClass.join()).wrapAll("<div class='col-xs-12 fieldset toHide fieldset"+entry+"' style='visibility:hidden;height:0px;margin: 0px;padding:0px'></div>");
  else
    $(arrayClass.join()).wrapAll("<div class='col-xs-12 fieldset fieldset"+entry+"'></div>");

  fieldsetClass.push(".fieldset"+entry);
  styleWrapped =   `.modal-content .fieldset${entry}:before {
                      content: "${label}";
                      color: ${color};
                      position: absolute;
                      top: -15px;
                      left: 14px;
                      
                      background: white;
                      font-size: medium;
                      font-weight: bold;
                      padding: 0 15px;
                    }`;
  $("head style").last().append(styleWrapped);


}



function deleteBlockNotFound(id,type){
    $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
    var btnClick = $(this);
    var urlToSend = baseUrl+"/"+moduleId+"/cms/delete/id/"+id;

    bootbox.confirm("Etes vous sur de vouloir supprimer cet block ?",
    function(result)
    {
      if(result==true)
          ajaxPost(
            null,
            urlToSend,
            {},
            function(data){ 
                if (data.result ) {
                    toastr.success("Block effacé avec succès.");
                    $("#"+id).remove();
                    urlCtrl.loadByHash(location.hash);
                } else {
                     toastr.error("Une erreur est survenue");
                }
            }
          );
    });
}

function sortBlock(classItem){
  setTimeout(function(){
    $(classItem).wrapAll('<div id="sortable"></div>');
        $("#sortable" ).sortable({
            handle : ".handleDrag",
            placeholder: "ui-state-highlight",
            stop: function(event, ui) {
              $('.block-parent').css("border","0px solid black");
              var idAndPosition = $(this).sortable('toArray');
              $.ajax({
                  type : 'POST',
                  data : {
                    id : thisContextId,
                    collection : thisContextType,
                    idAndPosition : idAndPosition
                  },
                  url : baseUrl+"/costum/blockcms/dragblock",
                  success : function(data){
                    if(data.result)
                      toastr.success("Bien déplacé");
                    else
                      toastr.success(trad.somethingwrong);
                  }
              });
            },
            sort: function(event,ui){
                $('.block-parent').css("border","2px dotted red")
            }
        });
        //*$( "#sortable" ).disableSelection();
    },900);
}

var paperPaintObj = {
  init : function(){
      this.append(function(){
        $("#blockCmsBgPaint").imagepicker({
          hide_select : false,
          show_label  : true
        });
      })
  },
  append : function(callback){
    $("#blockCmsBgPaint").addClass("show-html").append(`
    `);

    $('.more-config').click(function(){
        $(".blockCmsBgPaintselect").animate({
            scrollTop: ((exists($(".thumbnail.selected").offset()) && exists($(".thumbnail.selected").offset().top)) ? $(".thumbnail.selected").offset().top :null)
        }, 1000);
    })

    if(typeof callback == "function")
      callback();
  }
}

var createFieldsets = function(objProperties){ 
    alignInput2(objProperties,"margin",2,6,2,null,"Marge (Margin) en px","blue","hidden");
    alignInput2(objProperties,"padding",2,6,2,null,"Rembourrage (Padding) en px","blue","hidden");
    alignInput2(objProperties,"mode",2,6,2,null,"Taille de l'écran","blue","hidden");
    alignInput2(objProperties,"blockCmsBg",8,12,2,2,"Fond du block (couleur ou image)","blue","");
    alignInput2(objProperties,"blockCmsPolice",2,6,null,null,"Police (Font)","#fa0470","hidden");
    alignInput2(objProperties,"blockCmsTextAlign",2,6,null,null,"Alignement du texte","#fa0470","hidden");
    alignInput2(objProperties,"blockCmsTextSize",2,6,null,null,"Taille du texte","#fa0470","hidden");
    alignInput2(objProperties,"blockCmsTextLineHeight",2,6,null,null,"Interligne","#fa0470","hidden");
    alignInput2(objProperties,"blockCmsUnderline",2,3,null,null,"Soulignement du texte (px ou %)","#fa0470","hidden");
    alignInput2(objProperties,"blockCmsColor",2,6,null,null,"Couleur du texte","#fa0470","hidden");
    alignInput2(objProperties,"blockCmsBorder",2,6,null,null,"Bordure du section","blue","hidden");
    alignInput2(objProperties,"blockCmsLineSeparator",2,6,null,null,"Ligne séparatrice entre section","blue","hidden");
}
