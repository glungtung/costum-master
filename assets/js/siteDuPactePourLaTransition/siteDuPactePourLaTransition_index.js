poi.filters["contract"] = {
	"label": "Contract","key":"contract","icon":"file"
};
dyFObj.unloggedMode="admin";
costum.pacte={
	successAfterLoad : function(editMode, col, slugElt, idElt){
			dyFObj.closeForm();
			if(!editMode){
				if(col=="organizations"){
					var successCollectif = '<div class="portfolio-modal modal fade text-center py-5"  id="successCollectif" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">'+
						    '<div class="modal-dialog modal-md" role="document">'+
						        '<div class="modal-content">'+
						        	'<div class="close-modal" data-dismiss="modal">'+
					                    '<div class="lr">'+
					                        '<div class="rl">'+
					                        '</div>'+
					                    '</div>'+
					                '</div>'+
						            '<div class="modal-body" style="font-size: 22px">'+
						                '<div class="text-center contain-logo">'+
						                	'<img src="'+assetPath+'/images/siteDuPactePourLaTransition/PACTE-TRANSITION-LOGOTYPEcouleurs.png" alt="image"/>'+
						                '</div>'+
						                '<div id="text-head-collectif" class="justify-marge">L\’ajout de collectif est bien enregistré, il apparaîtra sur la carte une fois validé par l\’équipe du Pacte.</div>'+
						                '<a href="#@'+slugElt+'.edit.'+idElt+'" class="dont-break-out lbh"><div class="link-to btn">Voir la page du collectif</div></a>'+
						                '<div id="text-head-collectif" class="small save-link-email justify-marge">Conservez-bien le lien que vous recevrez par mail, qui sera nécessaire pour modifier la page de votre collectif.</div>'+
						                '<div id="linkForm" class="padding-link">';
		
		successCollectif += 				'</div>'+
						            '</div>'+
						        '</div>'+
						    '</div>'+
						'</div>';

					$(".main-container").append(successCollectif);
					$("#successCollectif").modal("show");
					coInterface.bindLBHLinks();
				}else{
					urlCtrl.loadByHash(location.hash);
					var successContract = '<div class="portfolio-modal modal fade text-center py-5"  id="successContract" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">'+
									    '<div class="modal-dialog modal-md" role="document">'+
									        '<div class="modal-content">'+
									        	'<div class="close-modal" data-dismiss="modal">'+
								                    '<div class="lr">'+
								                        '<div class="rl">'+
								                        '</div>'+
								                    '</div>'+
								                '</div>'+
									            '<div class="modal-body" style="font-size: 22px">'+
									                '<div class="text-center contain-logo">'+
									                    '<img src="'+assetPath+'/images/siteDuPactePourLaTransition/PACTE-TRANSITION-LOGOTYPEcouleurs.png" alt="image"/>'+
									                '</div>'+
									                '<div id="text-head-collectif" class="justify-marge">L\’ajout du contrat est bien enregistré, il sera validé par l\'équipe du pacte.<br/>À la suite de cette validation, il sera visible de tous en mode public.</div>'+
									            '</div>'+
									        '</div>'+
									    '</div>'+
									'</div>';
					$(".main-container").append(successContract);
					$("#successContract").modal("show");
				}
			}else{
				toastr.success("Vos modifications ont bien été prises en compte");
				urlCtrl.loadByHash(location.hash);
			}	
	},
	"contract" : {
		formData : function(data){
			//if(dyFObj.editMode){
			//if(typeof data.measures != "undefined"){
			//	data.links={"measures":data.measures};
			//	delete data.measures;
			//};
			$.each(data, function(e, v){
				if(e.indexOf("level") > -1)
					delete data[e];
			});
			return data;
		},
		afterSave : function(data){
			dyFObj.commonAfterSave(data, function(){
				var currentEditMode=(dyFObj.editMode) ? true : false;
				if(!costum.isCostumAdmin){
					ajaxPost(null, baseUrl+'/'+moduleId+'/person/logout?slug='+costum.slug, 
						null,
						function(data){
							costum.pacte.successAfterLoad(currentEditMode, "poi", contextData.slug, contextData.id);
								
					});
				}else
					costum.pacte.successAfterLoad(currentEditMode, "poi", contextData.slug, contextData.id);
			});
		}
	},
	"organizations":{
		formData : function(data){
			mylog.log("organizations.formData",data);
			if(typeof data.checkCharte != "undefined" && data.checkCharte == "on"){
				delete data.checkCharte;
				return data;
			}else if(!dyFObj.editMode){
				return toastr.error("Vous devez respecter la charte des collectifs locaux")
			}else
				return data;
		},
		afterSave : function(data){
			var slugCollectif=data.map.slug;
			var idCollectif=data.id;
			dyFObj.commonAfterSave(data, function(){
				$(".main-container").removeClass("vertical");
				var currentEditMode=(dyFObj.editMode) ? true : false;
				if(!costum.isCostumAdmin){
					ajaxPost(null, baseUrl+'/'+moduleId+'/person/logout?slug='+costum.slug, 
						null,function(data){
								costum.pacte.successAfterLoad(currentEditMode, "organizations",slugCollectif, idCollectif);								
					});
				}else
					costum.pacte.successAfterLoad(currentEditMode, "organizations", slugCollectif, idCollectif);
			});
		},
		afterBuild : function(data){
			$("#ajaxFormModal .publicMailtext label").after("<span class='col-xs-12 text-left margin-bottom-10 no-padding'>"+
				"- la liste de diffusion associée au code postal de la première commune référencée plus haut (par défaut)<br/>"+
				"- adresse mail secondaire (optionnelle) :"+
 			"</span>");
			if(!dyFObj.editMode){
				$("#ajaxFormModal .form-actions").before(
					'<div class="checkbox-content pull-left col-xs-12 no-padding">'+
	                    '<label for="checkCharte" class="">'+
	                       '<input type="checkbox" class="checkCharte" id="checkCharte" name="checkCharte">'+
	                        '<span class="cr pull-left"><i class="cr-icon fa fa-check"></i></span>'+
	                    	'<span class="newsletterMsg checkbox-msg no-padding col-xs-11 pull-right">'+
	                    		'Nous respectons la <a class="text-purple" href="https://nextcloud.transition-citoyenne.org/index.php/s/7ywrCwNPF8TckPN" target="_blank"> charte des collectifs locaux </a> (entre autres, notre collectif a au moins 3 membres, et est indépendant des partis et des élu·es)'+
	                    	'</span>'+
	                   '</label>'+
	                '</div>');
				//$('#checkCharte').prop('checked', true);

				$('#ajaxFormModal .form-actions').html(
					'<button id="btn-submit-form" class="btn btn-default text-bold pull-right btn-add-collectif">'+
						'J’inscris mon collectif sur la carte du Pacte '+ 
						'<i class="fa fa-arrow-circle-right"></i>'+
					'</button>'+
					'<p class="text-info-collectif">'+
						'Après avoir inscrit votre collectif sur la carte, l’ajout sera validé par l’équipe du Pacte puis vous recevrez un mail comportant un lien vous permettant de modifier ces informations dans le futur, ne le perdez pas !'+
					'</p>'
				);
		       $('#ajaxFormModal #checkCharte').click(function(){
		            if($(this).is(":checked")){
		                $(this).val("on");
		            }
		            else if($(this).is(":not(:checked)")){
		                $(this).val("off");
		            }
		        });
		    }
		    if(dyFObj.editMode){
		    	$(".portfolio-modal .modal-title").html("Modifier le collectif");
		    	$(dyFObj.activeModal+" .form-group.infocustom p:first-child").text("Mettre à jour les informations du collectif");
		    }
				 
		}

	}
};
var pacte={
	initScopeObj : function(){
		$(".content-input-scope-pacte").html(scopeObj.getHtml("Code postal"));
		var params = {
			subParams : {
				cities : {
					type : ["cities"],
					country : ["FR", "RE", "GP", "GF", "MQ", "YT", "NC", "PM"],
					cp : true
				}
			}
		}
		scopeObj.initVar(params);
		scopeObj.init();
	},
	mapDefault : function(){
		mapCustom.markers.getMarker = function (data) {
			var imgM = modules.map.assets + '/images/markers/ngo-marker-default.png';
			return imgM;
		};
		mapCustom.popup.default = function(data){
			var id = (typeof data.id != "undefined") ? data.id :  data._id.$id ;
			var imgProfil = modules.map.assets + "/images/thumb/default_organizations.png"; ;

			var popup = "";
			popup += "<div class='padding-5' id='popup"+id+"'>";
				popup += "<img src='" + imgProfil + "' height='30' width='30' class='' style='display: inline; vertical-align: middle; border-radius:100%;'>";
				popup += "<span style='margin-left : 5px; font-size:18px'>" + data.name + "</span>";
				
				if (typeof data.email != "undefined" && data.email != null ){
					popup += "<div id='pop-contacts' class='popup-section'>";
						popup += "<div class='popup-subtitle'>Contact</div>";
							popup += "<div class='popup-info-profil'>";
								popup += "<i class='fa fa-envelope fa_email'></i> <a href='mailto:"+data.email+"'>" + data.email+"</a>";
							popup += "</div>";
						popup += "</div>";
					popup += "</div>";
				}
				popup += "<div class='popup-section'>";
					popup += "<a href='javascript:;' onclick='pacte.joinGroupMap(\""+id+"\");' class='item_map_list popup-marker' id='popup"+id+"'>";
						popup += '<div class="btn btn-sm btn-more col-xs-12">';
						popup += 	"Rejoindre le collectif"
					popup += '</div></a>';
				popup += '</div>';
			popup += '</div>';
			return popup;
		};
	},
	joinGroupMap : function(id){
		groupSelected=resultMapGroup[id];
		scopeObj.selected=groupSelected.scope;
		$.each(groupSelected.scope, function(e,v){
			zoneName= (typeof v.cityName != "undefined") ? v.cityName : v.name ;
		});
		pacte.launchRegister({exist : true}, zoneName);
	},
	capitalizeFirstLetter : function (string) {
		  string = string.toLowerCase();
		  var array_string = string.split(" ");
          var array_result= [];
		  array_string.forEach(function(value,key){
            array_result.push(value.charAt(0).toUpperCase() + value.slice(1))
		  })
		 
		  return array_result.join(" ");
	},
	caseCollectif : function(result, city){
		//4 pupop cas 1 -> cas 4
		//pacte.launchRegister(result,city);
		mylog.log("caseCollectif",result,city);
		$("#popupCollectif").modal("show");
		var msgHeaderCollectif="";
		var linkToFormModal="";
		if(typeof result != "undefined" && typeof result.elt != "undefined"){
			var id = (typeof result.elt._id.$id  != "undefined") ? result.elt._id.$id : result.elt.id ;
			msgHeaderCollectif += '<div class="text-center">';

			if (result.elt.category == "actifSigned"){
				msgHeaderCollectif += '<span class="title-col">';
				if (typeof result.elt.links != "undefined" && result.elt.links != null &&
					typeof result.elt.links.contracts != "undefined" && result.elt.links.contracts != null) {
					msgHeaderCollectif += result.elt.name;
				}
				msgHeaderCollectif += '</span>';
				msgHeaderCollectif += '<div class="padding-link margin-top-20"><a href="#page.type.organizations.id.'+id+'" class="link-to lbh btn">Plus d’informations sur le<br> collectif et ses engagements</a></div>';
				if (typeof result.elt.email != "undefined" && result.elt.email != null) {						
					msgHeaderCollectif += '<div class="padding-link"><a href="mailto:'+result.elt.email+'" class="link-to btn">Je contacte le collectif</a></div>';
				}                    	                
			}else if (result.elt.category == "actif") {
				msgHeaderCollectif += '<span class="title-col"><strong>'+result.elt.name+'</strong></span>'+
					                    '<div class="padding-link margin-top-20"><a href="#page.type.organizations.id.'+id+'" class="link-to lbh btn">Plus d’informations</a></div>';
				if (typeof result.elt.email != "undefined" && result.elt.email != null) {
					msgHeaderCollectif += '<div class="padding-link"><a href="mailto:'+result.elt.email+'" class="link-to btn btn">Je contacte le collectif</a></div>';
				}
					                                   
			}else {
				msgHeaderCollectif += '<span class="title-col">A '+this.capitalizeFirstLetter(city)+', des personnes soutiennent le Pacte pour la Transition</span>';
				if (typeof result.elt.email != "undefined" && result.elt.email != null) {
					msgHeaderCollectif += '<div class="padding-link margin-top-20"><a href="mailto:'+result.elt.email+'" class="link-to btn">Je les contacte</a></div>';
				}
				
			}
			msgHeaderCollectif += ' </div>';
			//lien vers la formulaire
			linkToFormModal += "<a href='javascript:;' onclick='pacte.joinGroupMap(\""+id+"\");' class='link-to item_map_list popup-marker btn' style='margin-top:2px'>Je m\'engage pour la transition<br> citoyenne de ma commune</a>";
  		}else if(result.exist == false){
  			msgHeaderCollectif += 'Personne ne s’est encore signalé à '+this.capitalizeFirstLetter(city)+',<br>'+
									' peut-être serez-vous le ou la première.';

			//lien vers la formulaire
			linkToFormModal += "<a href='javascript:;' onclick='pacte.launchRegister(\""+result+"\", \""+city+"\");' class='link-to btn'>Je m\'engage pour la transition<br> citoyenne de ma commune</a>";
  		}

  		$( '#text-head-collectif' ).html(msgHeaderCollectif);
  		$( '#linkForm' ).html(linkToFormModal);
  		coInterface.bindLBHLinks();
	  	
	},
	
	launchRegister : function(result, city){
		var msgHeader="<span class='text-justify'>Bienvenue ! <br/><br/></span>";
		$(".form-register .footerNoneCandidate").remove();
		$(".form-register .msgGroup").remove();
	    $('#modalRegister').modal("show");
	    var params={};
	    $('.form-register .agreeContent').show();
	    $('.form-register').find(".createBtn").html("<i class='fa fa-sign-in'></i> JE SOUTIENS LA TRANSITION CITOYENNE DE MA COMMUNE");
	 	
		footerStr="<div class='footerNoneCandidate text-center' style='width:100%;margin-top:110px;'>"+
			"<a href='https://my.sendinblue.com/users/subscribe/js_id/3t7aq/id/1' target='_blank' class='bg-purple' style='width: 100%;border-radius: 10px;font-size: 22px;padding: 10px 25px;color: white !important;text-decoration: none;'>"+
				"Rester seulement informé·e des avancées du Pacte"+
			"</a>";
	    if(result.exist){
	    	params.elt=result.elt;
	    	footerStr+="<br/><br/><span class='text-justify margin-top-10 text-purple' style='font-size:16px;'>Pour écrire directement au collectif, envoyer un mail à<br/>"+
				"<a href='mailto:"+result.elt.email+"' class='text-orange bold'>"+result.elt.email+"</a>.</span>";
	
  			params.msgHeader=msgHeader+'<span class="text-justify">Plusieurs habitant·es de <b>'+city+'</b> se mobilisent déjà !<br/><br/>'
	  				+'Pour suivre leurs avancées, ou les rejoindre, inscrivez-vous à la liste de diffusion locale en leur écrivant ici.</span>';
  			$(".form-register .emailRegister").after("<div class='msgGroup'>"+
				"<label class='letter-black'><i class='fa fa-pencil'></i> Ecrire un message au groupe *</label>"+
				"<textarea class='form-control' id='textMsgGroup' name='textMsgGroup' placeholder='Ecrire un message'></textarea>"+
    		"</div>");
  		}else{
  			params.elt=result.elt;
  			params.msgHeader=msgHeader+'<span class="text-justify">Vous êtes le·la premièr·e habitant·e de <b>'+city+'</b> à vous signaler !<br/><br/>'+
  				'Si vous souhaitez créer le collectif local de '+city+', remplissez ce formulaire. Nous vous mettrons en contact avec les prochaines personnes de votre commune qui s’inscriront.<br/><br/>'+
				'</span>';
  		}
  		footerStr+="</div>";
  		$(".form-register .form-register-inputs").append(footerStr);
  		//if($(".form-register .info-register-form").length > 0){
  			/*$('.form-register .info-register-form').html(params.msgHeader);*/
  		//}
  		//else{
			/*$('.form-register').find(".surnameRegister").before("<div class='col-xs-12 bg-purple text-white text-center info-register-form'>"+params.msgHeader+"</div>");*/
  		//}
		Login.runRegisterValidator();
	},
	contractsHtml : function(data){
		html="";
		$.each(data, function(e,v){
			if(typeof v.links != "undefined" && typeof v.links.measures != "undefined")
				countMeasures=Object.keys(v.links.measures).length;
			descriptionMes='';
			if(typeof v.shortDescription != "undefined")
				descriptionMes=v.shortDescription;
			else if(typeof v.description != "undefined")
				descriptionMes=v.description;
			html+="<div class='searchEntity poi poi"+e+" contracts shadow2 col-xs-12' data-hash='#page.type.poi.id."+e+"'>"+
					"<h3 class='entityName col-xs-12'><span class='text-purple'>"+v.name+"</span>";
					if(descriptionMes != "" || typeof v.ownerList != "undefined"){
						html+="<span class='subTiltle'> <b>-</b> ";
						if(descriptionMes != ""){
							html+="<b class='text-orange'>"+descriptionMes+"</b>";
						}
						if(typeof v.ownerList != "undefined"){
							if(descriptionMes!="") html+=" (";
							html+="Tête de liste : <b>"+v.ownerList+"</b>";
							if(descriptionMes!="") html+=" )";
						}
						html+="</span>";
					}
					html+="</h3>";
					html+="<span class='col-xs-12'>Cette liste s’est engagée le <b class='text-purple'>"+v.signedOn+"</b> ";
					if(typeof countMeasures != "undefined")
						html+="sur <b class='text-orange'>"+countMeasures+" mesures.</b><br/></span>";
					html+="<br/></span>";
					if(typeof countMeasures != "undefined"){
						html+="<div class='col-xs-12 contentListMesure"+e+" no-padding' style='display:none;'>";
							$.each(v.links.measures,function(k, mes){
									html+="<div class='col-xs-12'>"+
										"<a href='#page.type.poi.id."+k+"' class='col-xs-12 no-padding lbh-preview-element' style='font-size:18px;'>"+mes.name+"</a>"+
										"<span>Niveau d'ambition "+mes.level+"</span>"+
									"</div>";
							});
							if(typeof v.localMeasures != "undefined" && Object.keys(v.localMeasures).length >0){
								$.each(v.localMeasures,function(k, mes){
									html+="<div class='col-xs-12'>"+
											"<span class='col-xs-12 no-padding bold' style='color: #2C3E50;font-size:18px;'>"+mes.name+"</span>"+
											"<span>"+mes.description+"</span>"+
										"</div>";
								});
							}
							
						html+="</div>";
					}
					html+="<div class='col-xs-12 margin-top-10'>";
							if(typeof countMeasures != "undefined")
								html+="<a href='javascript:;' data-id='"+e+"' data-show='true' class='showMeasures'><i class='fa fa-arrow-down'></i> Voir les mesures</a>";
							if(typeof v.urlContract != "undefined" && notEmpty(v.urlContract))
								html+="<a href='"+v.urlContract+"' target='_blank' class='"+((typeof countMeasures != "undefined") ? 'margin-left-10' : '')+"'><i class='fa fa-file-o'></i> Voir le contrat</a>";
					html+="</div>";
				html+="</div>";
		});
		return html;
	}
};
scopeObj.onclickScope = function (input, domTarget) {
		
	if(!$(input).parent().hasClass("content-input-scope-pacte")){
		mylog.log("scopeObj.onclickScope", input, domTarget);
		$(input+" .item-globalscope-checker").off().on('click', function(){
			$("#labelselected").removeClass("hidden");
			//alert("Fuck");
			$('#searchScopeDF').val('');
			if(scopeObj.limit == null || Object.keys(scopeObj.selected).length < scopeObj.limit ){
				var key = $(this).data("scope-value");
				mylog.log("item-globalscope-checker myScopes", key, myScopes.search[key] );
				if( typeof myScopes != "undefined" &&
					typeof myScopes.search != "undefined" &&
					typeof myScopes.search[key]  != "undefined" ){
					scopeObj.populateResults(key, myScopes.search[key],input, domTarget);
				}
			} else {
				toastr.error("Vous avez déjà selectionné le nombre max élèments")
			}
		});
	}else{
		$(".item-globalscope-checker[data-scope-value='54c09656f6b95c1418008e00cities'], .item-globalscope-checker[data-scope-value='54c09653f6b95c141800849ecities'], .item-globalscope-checker[data-scope-value='54c09653f6b95c141800849ecities'], .item-globalscope-checker[data-scope-value='54c09638f6b95c141800266ccities']").remove();
		if($(".content-input-scope-pacte .item-globalscope-checker").length==0){
			$(".content-input-scope-pacte #footerDropdownGS").html('<label class="text-dark margin-top-5"><i class="fa fa-times"></i> Aucun résultat</label>');
		}
		$(".content-input-scope-pacte .item-globalscope-checker").off().on('click', function(){
			var key = $(this).data("scope-value");
			if( typeof myScopes != "undefined" &&
					typeof myScopes.search != "undefined" &&
					typeof myScopes.search[key]  != "undefined" ){
						var scopeDF = myScopes.search[key];
						var nameZone = (typeof scopeDF.cityName != "undefined") ? scopeDF.cityName : scopeDF.name ;
			}
			scopeObj.selected={};
			scopeObj.selected[key] = myScopes.search[key];
			if(location.hash.indexOf("signatures") >= 0){
				coInterface.showLoader("#results-candidatures");
				ajaxPost(
		            null,
		            baseUrl+"/costum/pacte/searchsignatures",
		            {
		    	  		scope:scopeObj.selected,
		    	  	},
		            function(data){ 
		                countContracts=Object.keys(data.contracts).length;
		    	 		urlStr="";
		    	 		if(countContracts > 0){
		    	 			plur=(countContracts>1) ? "s" : "";
		    	 			if(typeof data.elt != "undefined" && typeof data.elt.url != "undefined" && notEmpty(data.elt.url)){
		    	 				var urlR = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/;
								var url= (data.elt.url.match(urlR)) ? data.elt.url : "http://"+data.elt.url;
								urlStr="<br/>Pour suivre ce collectif, rendez-vous sur <a href='"+url+"' class='text-orange' target='_blank'>"+data.elt.url+"</a>";
		    	 			}
		    	 			str="<span class='col-xs-12 resultTitle'>Le collectif local de <b class='text-purple'>"+nameZone+"</b> a signé un Pacte pour la Transition<br/>"+
			    	 				" avec <b class='text-orange'>"+countContracts+" liste"+plur+"</b> candidate"+plur+" !"+
	    	 					urlStr+
	    	 				"</span>";
		 					str+=pacte.contractsHtml(data.contracts);
		    	 		}else{
		    	 			if(data.exist){
		    	 				if(typeof data.elt.url != "undefined" && notEmpty(data.elt.url)){
			    	 				var urlR = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/;
									var url= (data.elt.url.match(urlR)) ? data.elt.url : "http://"+data.elt.url;
									urlStr="Pour suivre ce collectif, rendez-vous sur <a href='"+url+"' class='text-orange' target='_blank'>"+data.elt.url+"</a><br/>";
		    	 				}
		    	 				str="<span class='text-justify col-xs-12 resultTitle'>Le <b class='text-purple'>collectif local de</b> <b class='text-orange'>"+nameZone+"</b> n’a pas encore obtenu de signatures.<br/>";
		    	 				str+=	urlStr;
		    	 				str+=	"Pour soutenir <b class='text-purple'>leur action</b> et les aider à obtenir des <b class='text-purple'>signatures</b>,<br/> Écrivez leur via l'adresse <br/> <a href='mailto:"+data.elt.email+"'><b class='text-orange' style='font-size:25px;'>"+data.elt.email+"</b></a> !";
		    	 				str+="</span>";
		        	 		}else{
			    	 			str='<span class="text-justify col-xs-12 resultTitle">'+
			    	 					'Aucune liste n\'a signé le Pacte dans votre commune ! Par ailleurs, il n’existe pas encore de collectif du Pacte pour la Transition dans votre commune.<br/><br/> <b class="text-orange" style="font-size: 22px;">Vous souhaitez vous engager ?</b><br/>'+
			    	 					'<button class="joinGroup btn btn-link bg-purple text-white margin-top-10" style="border-radius:3px;text-decoration:none;font-size:20px;">C\'est par ici</button>'+
			    	 				'</span>';
			    	 		}
		    	 		}
		    	 		
		    	 		$("#results-candidatures").html(str);
		    	 		$(".showMeasures").off().on("click", function(){
		    	 			idContact=$(this).data("id");
		    	 			if($(".contentListMesure"+idContact).is(":visible")){
		    	 				$(".contentListMesure"+idContact).hide(300);
		    	 				$(this).html("<i class='fa fa-arrow-down'></i> Voir les mesures");
		    	 			}else{
		    	 				$(".contentListMesure"+idContact).show(300);
		    	 				$(this).html("<i class='fa fa-arrow-up'></i> Cacher les mesures");

		    	 			}
		    	 		});
		    	 		coInterface.bindLBHLinks();
		    	 		$(".joinGroup").off().on("click", function(){
		    	 			pacte.launchRegister(data, nameZone);
						});
		            }
		        );
			}else{
				ajaxPost(
		            null,
		            baseUrl+"/costum/pacte/checkexist",
		            {
		    	  		scope:scopeObj.selected,
		    	  	},
		            function(data){ 
		                //pacte.launchRegister(data, nameZone);
		                pacte.caseCollectif(data, nameZone);
		            }
		        );
			}			
		});
 	}
};

Login.runRegisterValidator = function(params) { 
	var form4 = $('.form-register');
	var errorHandler3 = $('.errorHandler', form4);
	var createBtn = null;
	$(".form-register .container").removeClass("col-lg-offset-3 col-sm-offset-2 col-lg-6 col-sm-8 col-xs-12");
	if($(".form-register .surnameRegister").length <= 0){
	$('.form-register').find(".nameRegister").before('<div class="surnameRegister">'+
                    '<label class="letter-black"><i class="fa fa-address-book-o"></i> Prénom</label>'+
                    '<input class="form-control" id="registerSurname" name="surnname" type="text" placeholder="Prénom"><br/>'+
               '</div>');
	}
	if($(".form-register .receiveNewsletter").length <= 0){
		$('.form-register').find(".agreeContent").before('<div class="form-group pull-left margin-bottom-10 receiveNewsletter" style="width:100%;">'+
                    '<div class="checkbox-content pull-left col-xs-12 no-padding margin-bottom-25">'+
                        '<label for="redirectLaunchCollectif" class="no-padding">'+
                            '<input type="checkbox" class="grey" id="redirectLaunchCollectif" name="redirectLaunchCollectif">'+
                            '<span class="cr pull-left"><i class="cr-icon fa fa-check"></i></span>'+
                        	'<span class="newsletterMsg checkbox-msg no-padding text-purple">Je souhaite lancer un collectif pour interpeller les élu·es et agir pour une transition citoyenne sur mon territoire.</span>'+
                       '</label>'+
                    '</div>'+
                    '<div class="checkbox-content pull-left col-xs-12 no-padding margin-bottom-10">'+
                        '<label for="newsletterCollectif" class="">'+
                           '<input type="checkbox" class="newsletterCollectif" id="newsletterCollectif" name="newsletterCollectif">'+
                            '<span class="cr pull-left"><i class="cr-icon fa fa-check"></i></span>'+
                        	'<span class="newsletterMsg checkbox-msg no-padding text-purple">Je souhaite <a href="https://www.pacte-transition.org/#thematiques?preview=poi.5f97e5bd6908649d598b45a5" target="_blank" style="    color: #ffa200;">communiquer et partager mon contact</a> avec les autres personnes intéressées par le Pacte pour la Transition dans ma commune</span>'+
                       '</label>'+
                    '</div>'+
                    '<div class="checkbox-content pull-left col-xs-12 no-padding margin-bottom-10">'+
                        '<label for="newsletter" class="no-padding">'+
                            '<input type="checkbox" class="grey newsletter" id="newsletter" name="newsletter">'+
                             '<span class="cr pull-left"><i class="cr-icon fa fa-check"></i></span>'+
                        	'<span class="newsletterMsg checkbox-msg no-padding text-purple">Je souhaite être tenu·e au courant des actualités du Pacte pour la Transition au niveau national</span>'+
                       '</label>'+
                    '</div>'
        );
		$(".form-register .agreeContent").remove();
	}
	$('#newsletter').prop('checked', true);
	$('#newsletterCollectif').prop('checked', true);
	$('#redirectLaunchCollectif').prop('checked', true);
					    
	$('.form-register').find(".nameRegister label").html('<i class="fa fa-address-book-o"></i> Nom');
	$('.form-register').find(".nameRegister input").attr("placeholder","Nom");
	$('.form-register').find(".usernameRegister").remove();
	$('.form-register').find(".passwordRegister").remove();
	$('.form-register').find(".passwordAgainRegister").remove();
	form4.validate({
		rules : {
			name : {
				required : true,
				minlength : 2
			},
			email3 : {
				required : { 
				 	depends:function(){
				 		$(this).val($.trim($(this).val()));
				 		return true;
				 	}
				},
				email : true
			}
		},
		submitHandler : function(form) { 
			errorHandler3.hide();
			$(".createBtn").prop('disabled', true);
    		$(".createBtn").find(".fa").removeClass("fa-sign-in").addClass("fa-spinner fa-spin");
			var params = { 
			   "name" : $('.form-register #registerSurname').val()+" "+$('.form-register #registerName').val(),
			   "email" : $(".form-register #email3").val(),
			   "pendingUserId" : pendingUserId
            };
            if($('.form-register #isInvitation').val())
            	params.isInvitation=true;
            if(Object.keys(scopeObj.selected).length > 0){
		  		params.scope = scopeObj.selected;
		  	}
		  	if($(".form-register #newsletter").is(":checked"))
		  		params.newsletter=true;
		  	if($(".form-register #newsletterCollectif").is(":checked"))
		  		params.newsletterCollectif=true;
		  	var redirectCallBack=($(".form-register #redirectLaunchCollectif").is(":checked"))? true : false;
		  	
		  	//if($('.form-register .msgGroup').length && $(".form-register #textMsgGroup").val() != "")
		  	//	params.msgGroup=$(".form-register #textMsgGroup").val();
		  	ajaxPost(
	            null,
	            baseUrl+"/costum/pacte/register",
	            params,
	            function(data){ 
	                if(data.result) {
	                	mylog.log("Formulaire",data.result);
		    		  	//createBtn.stop();
						$(".createBtn").prop('disabled', false);
	    				$(".createBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in");
						$("#registerName").val("");
						$("#username").val("");
						$("#email3").val("");
						$("#password3").val("");
						$("#passwordAgain").val("");
						$("#passwordAgain").val("");
						$("#registerSurname").val("");
						//$('#agree').prop('checked', false);
						$('#newsletter').prop('checked', true);
						$('#newsletterCollectif').prop('checked', true);
						$('#redirectLaunchCollectif').prop('checked', true);
					   	toastr.success("Merci, votre soutien a bien été prise en compte");
	    		  		$('.modal').modal('hide');
	    		  		if(redirectCallBack)
	    		  			urlCtrl.loadByHash("#guide?preview=poi.5f8ef61f690864ff638b46e4");
	    		  		scopeObj.selected={};
		    		}
		    		else {
		    		  	toastr.error(data.msg);
		    		  	$('.modal').modal('hide');	    		  	
	    		  		scopeObj.selected={};
		    		}
	            },function(data) {
		    	  	toastr.error(trad["somethingwentwrong"]);
		    	  	$(".createBtn").prop('disabled', false);
					$(".createBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in");
		    	  	//createBtn.stop();
		    	}
	        );
		  	return false;
		},
		invalidHandler : function(event, validator) {//display error alert on form submit
			errorHandler3.show();
			$(".createBtn").prop('disabled', false);
    		$(".createBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in");
			//createBtn.stop();
		}
	});
};
Login.runRegisterValidator();

function startSearch(indexMin, indexMax, callBack){
	mylog.log("startSearch directory.js", typeof callBack, callBack, loadingData);
	if(loadingData) return;
	loadingData = true;
	mylog.log("startSearch", searchObject.indexMin, indexMax, searchObject.indexStep, searchObject.types);
	searchObject.indexMin = (typeof indexMin == "undefined") ? 0 : indexMin;
	autoCompleteSearch(indexMin, indexMax, function(){
		inc=0;
		$(".bodySearchContainer.mesures .searchEntityContainer").each(function(){
			title=$(this).find(".entityName").text();
			if(title.indexOf("# ") >= 0)
				$(this).addClass("principes");
		});
		countPR=$(".bodySearchContainer.mesures .searchEntityContainer.principes").length;
		inc=1;
		$(".bodySearchContainer.mesures .searchEntityContainer.principes").each(function(){
			if(countPR==inc)
				$(this).addClass("margin-bottom-20");
			inc++;
		});
		if($(".app-mesures").length > 0 && $(".app-mesures #main-search-bar").length <= 0){
			inpSearch='<div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 text-center margin-top-20"><input type="text" class="form-control pull-left text-center main-search-bar" id="main-search-bar" placeholder="Rechercher parmi les mesures">'+
	    		'<span class="text-white input-group-addon pull-left main-search-bar-addon" id="main-search-bar-addon">'+
	        		'<i class="fa fa-arrow-circle-right"></i></div>'+
	    		'</span>';
			$(".app-mesures").prepend(inpSearch);
			searchInterface.setSearchbar();
		}
		/*if($(".bodySearchContainer.mesures .searchEntityContainer").length <= 0){
			$("#dropdown_search").append("<span class='text-purple col-xs-12 text-center margin-top-50' style='text-transform:uppercase;font-size:20px;'>Aucun résultat</span>");
        }*/ 	
    });
    
};
searchObj.results.end= function(fObj){
			return "";
			//return "<span class='text-purple col-xs-12 text-center margin-top-50' style='text-transform:uppercase;font-size:20px;'>Aucun résultat</span>";
		};
mapObj.dataMarker= function(mObj,key,params,latCom,lonCom,nameCity){

			if (typeof params.opt == "undefined" || params.opt == null)
								params.opt = {};

			params.opt.icon = mObj.addIcon(params);
			var latLon = [latCom, lonCom];
			mObj.distanceTo(latLon);
			var marker = L.marker(latLon, params.opt);

			mObj.markerList[key] = marker;

			if (typeof mObj.activePopUp != "undefined" && mObj.activePopUp === true)
				mObj.addPopUp(marker, params.elt);

			mObj.arrayBounds.push(latLon);
			if (mObj.activeCluster === true) {
				//mylog.log("giiiiiiiiiiiiiiiiii", marker);
				mObj.markersCluster.addLayer(marker);
			}
			else {
				marker.addTo(mObj.map);
				if (typeof params.center == "undefined" || params.center === true) {
					//mylog.log("mObj.panTo");
					mObj.map.panTo(latLon);
				}
			}	
			marker.off().on('click', function (e) {
				mylog.log("markerClick",params, nameCity);
	            pacte.caseCollectif(params, nameCity);
				coInterface.bindLBHLinks();
			});
		
			if(mObj.mapOpt.mouseOver === true){
				marker.on('mouseover', function (e) {
					mObj.openPopup();
					coInterface.bindLBHLinks();
				});
				
				marker.on('mouseout', function (e) {
					var thismarker = mObj;
					setTimeout(function(){
						thismarker.closePopup();
					}, 2000);
				});	
			}

		};
mapObj.addMarker = function (params) {
	var obj = this;
	if (typeof params.elt != "undefined" && params.elt != null){

		var findGeoInScope=false;
		if( typeof params.elt.scope != "undefined" && params.elt.scope != null){
			i = 0;
			$.each(params.elt.scope, function(e,v){
				if(	typeof v.geo != "undefined" && v.geo != null &&
					typeof v.geo.latitude != "undefined" && v.geo.latitude != null &&
					typeof v.geo.longitude != "undefined" && v.geo.longitude != null) {
					findGeoInScope=true;
					var latCom = v.geo.latitude;
					var lonCom = v.geo.longitude;
					var nameCity = v.cityName;
					obj.dataMarker(obj,(params.elt.id+i),params,latCom,lonCom,nameCity);
				}
				i++;
			});
		}
		if(typeof params.elt.geo != "undefined" && params.elt.geo != null && 
			typeof params.elt.geo.latitude != "undefined" && params.elt.geo.latitude != null &&
			typeof params.elt.geo.longitude != "undefined" && params.elt.geo.longitude != null && !findGeoInScope) {
				var latCom = params.elt.geo.latitude;
				var lonCom = params.elt.geo.longitude;
				var nameCity = "";
				if(typeof params.elt.scope != "undefined" && params.elt.scope != null){
					var keyScope = Object.keys(params.elt.scope)[0];
					nameCity = params.elt.scope[keyScope].cityName;
				}
				obj.dataMarker(obj,params.elt.id,params,latCom,lonCom,nameCity);
				
		}
	}
};
mapCustom.addIcon = function(elt){
	if (typeof elt.category != "undefined" && elt.category != null ){
		if(elt.category == "actifSigned"){
			var myCustomColour = '#428bca';
		}
		if (elt.category == "actif") {
			var myCustomColour = '#c6793d';
		}
		if (elt.category == "soutien") {
			var myCustomColour = '#ffa200';
		}
	}else{
		var myCustomColour = '#ffa200';
	}

	var markerHtmlStyles = `
		background-color: ${myCustomColour};
		width: 3.5rem;
		height: 3.5rem;
		display: block;
		left: -1.5rem;
		top: -1.5rem;
		position: relative;
		border-radius: 3rem 3rem 0;
		transform: rotate(45deg);
		border: 1px solid #FFFFFF`;

	var myIcon = L.divIcon({
		className: "my-custom-pin",
		iconAnchor: [0, 24],
		labelAnchor: [-6, 0],
		popupAnchor: [0, -36],
		html: `<span style="${markerHtmlStyles}" />`
	});
	return myIcon;
};
mapObj.addElts= function (data) {
		//mylog.log("mapObj.addElts", data);
		var mObj = this;
		//mapCustom.clearMap(this.parentContainer);
		$.each(data, function (k, v) {
			//mylog.log("mapObj.data", this);
			if (typeof mObj.data[k] == "undefined")
				mObj.data[k] = v;

			if (mObj.filtres.result(mObj, v)) {
				v.id = k;
				mObj.addMarker({ elt: v });
				mapCustom.lists.createItemList(mObj, k, v, mObj.container);
			}
		});
		// if(notNull(mObj.filtres.timeoutAddCity)) 
		// 	clearTimeout(mObj.filtres.timeoutAddCity);
		// mObj.filtres.timeoutAddCity = setTimeout(function(){
		if (mObj.arrayBounds.length > 0) {
			if (mObj.arrayBounds.length == 1) {
				var point = {
					x: mObj.arrayBounds[0][0],
					y: mObj.arrayBounds[0][1]
				}
				//mylog.log("mapObj.addElts if one POINT", point);
			} else if (mObj.arrayBounds.length > 1) {
				mObj.bounds = L.bounds(mObj.arrayBounds);
				var point = mObj.bounds.getCenter();
				//mylog.log("mapObj.addElts if bounds POINT", point);
			}
			
			if (!isNaN(point.x) && !isNaN(point.y)) {
				//if (typeof mObj.forced == "undefined" || !mObj.forced || mObj.forced.zoom == "undefined")
				mObj.getZoomByDistance();
				//mylog.log("mapObj.addElts POINT after NaN", point, mObj.mapOpt.offset.x, mObj.mapOpt.offset.y);
				var lat = parseFloat(point.x) + ((parseFloat(point.x) < 0) ? -Math.abs(mObj.mapOpt.offset.x) : mObj.mapOpt.offset.x);
				var lon = parseFloat(point.y) + ((parseFloat(point.y) < 0) ? -Math.abs(mObj.mapOpt.offset.y) : mObj.mapOpt.offset.y);
				//mylog.log("mapObj.addElts POINT after parseFloat ", [lat, lon], mObj.mapOpt.zoom);
				mObj.mapOpt.center = [lat, lon];
				//if (typeof mObj.forced == "undefined" || !mObj.forced || mObj.forced.latLon == "undefined")
				//mObj.map.panTo([lat, lon]);
				//if (mObj.forced && mObj.forced.latLon && mObj.forced.zoom) {
				//	mObj.map.setView(mObj.forced.latLon, mObj.forced.zoom);
				//} else
				//	mObj.map.setView([lat, lon], mObj.mapOpt.zoom);
			}
		}

		if (mObj.activeCluster === true)
			mObj.map.addLayer(mObj.markersCluster);
		mObj.actions.bindMap(mObj);

		//if (mObj.forced) {
			//mylog.log("mapObj.addElts mObj.forced ", mObj.forced);
		//	if ( mObj.forced.latLon && mObj.forced.zoom ){
				//mylog.log("mapObj.addElts mObj.forced if ", mObj.forced);
			//	mObj.map.setView(mObj.forced.latLon, mObj.forced.zoom);
		//	} else {
				//mylog.log("mapObj.addElts mObj.forced else ", mObj.forced);
			//	if (mObj.forced.latLon)
			//		mObj.map.panTo(mObj.forced.latLon);

			//	if (mObj.forced.zoom)
			//		mObj.map.setZoom(mObj.forced.zoom);
		//	}
		//	
		//}

		mObj.hideLoader();
		mObj.map.invalidateSize();
		//}, 500);
	};
mapCustom.clusters.default = function(cluster){
	var childCount = cluster.getChildCount();
	var c = ' marker-cluster-';
	if (childCount < 100) {
		c += 'small-pacte';
	} else if (childCount < 1000) {
		c += 'medium-pacte';
	} else {
		c += 'large-pacte';
	}
	return L.divIcon({ html: '<div>' + childCount + '</div>', className: 'marker-cluster' + c, iconSize: new L.Point(40, 40) });
}

finder.populateFinder = function(keyForm, obj, multiple, first){
	mylog.log("finder.populateFinder", keyForm, obj, multiple, first);
	str="";
	if(first && typeof finder.object[keyForm][userId] == "undefined" && typeof finder.initMe[keyForm] != "undefined" && finder.initMe[keyForm]){
		img= (userConnected.profilThumbImageUrl != "") ? baseUrl + userConnected.profilThumbImageUrl : modules.co2.url + "/images/thumb/default_citoyens.png";
		if(typeof finder.finderPopulation[keyForm][userId]=="undefined"){
			finder.finderPopulation[keyForm][userId]={
				name:userConnected.name + ' ('+tradDynForm.me+')',
				type:"citoyens",
				profilThumbImageUrl:userConnected.profilThumbImageUrl
			};
		}
		str+="<div class='population-elt-finder population-elt-finder-"+userId+" col-xs-12' data-value='"+userId+"'>"+
				'<div class="checkbox-content pull-left">'+
					'<label>'+
	    				'<input type="checkbox" class="check-population-finder checkbox-info" data-value="'+userId+'">'+
	    				'<span class="cr"><i class="cr-icon fa fa-check"></i></span>'+
					'</label>'+
				'</div>'+
				"<div class='element-finder element-finder-"+userId+"'>"+
					'<img src="'+img+'" class="thumb-send-to pull-left img-circle" height="40" width="40">'+
					'<span class="info-contact pull-left margin-left-20">' +
						'<span class="name-element text-dark text-bold" data-id="'+userId+'">' + userConnected.name + ' ('+tradDynForm.me+')</span>'+
						'<br/>'+
						'<span class="type-element text-light pull-left">' + trad.citoyens+ '</span>'+
					'</span>' +
				"</div>"+
			"</div>";
	}
	if(notNull(obj)){
			$.each(obj, function(e, v){
				if(typeof finder.object[keyForm][e] == "undefined" && e != userId){
					if(typeof finder.finderPopulation[keyForm][e]== "undefined")
						finder.finderPopulation[keyForm][e]=v;
					if($(".population-elt-finder-"+e).length <= 0){
						if(keyForm=="measures"){
							str+="<div class='population-elt-finder population-elt-finder-"+e+" col-xs-12' data-value='"+e+"'>"+
								'<div class="checkbox-content pull-left">'+
									'<label>'+
					    				'<input type="checkbox" class="check-population-finder checkbox-info" data-value="'+e+'">'+
					    				'<span class="cr"><i class="cr-icon fa fa-check"></i></span>'+
									'</label>'+
								'</div>'+
								"<div class='element-finder element-finder-"+e+" pull-left col-xs-11 no-padding'>"+
									'<span class="info-contact pull-left no-padding">' +
										'<span class="name-element text-dark text-bold" data-id="'+e+'">' + v.name + '</span>'+
									'</span>' +
								"</div>"+
							"</div>";
						}else{
		
							img= (v.profilThumbImageUrl != "") ? baseUrl + v.profilThumbImageUrl : modules.co2.url + "/images/thumb/default_"+v.type+".png";
						
							str+="<div class='population-elt-finder population-elt-finder-"+e+" col-xs-12' data-value='"+e+"'>"+
								'<div class="checkbox-content pull-left">'+
									'<label>'+
					    				'<input type="checkbox" class="check-population-finder checkbox-info" data-value="'+e+'">'+
					    				'<span class="cr"><i class="cr-icon fa fa-check"></i></span>'+
									'</label>'+
								'</div>'+
								"<div class='element-finder element-finder-"+e+"'>"+
									'<img src="'+ img+'" class="thumb-send-to pull-left img-circle" height="40" width="40">'+
									'<span class="info-contact pull-left margin-left-20">' +
										'<span class="name-element text-dark text-bold" data-id="'+e+'">' + v.name + '</span>'+
										'<br/>'+
										'<span class="type-element text-light pull-left">' + trad[v.type]+ '</span>'+
									'</span>' +
								"</div>"+
							"</div>";
						}
					}
				}
			});
		
	}
	if(first)
		$("#list-finder-selection").html(str);
	else
		$("#list-finder-selection").append(str);
	finder.bindSelectItems(keyForm, multiple);
};
finder.addInForm = function(keyForm, id, type, name, img, data){
	if(keyForm=="measures"){
		str="<div class='col-xs-12 element-finder element-finder-"+id+" shadow2 padding-10'>"+
					'<span class="info-contact pull-left col-xs-12 no-padding">' +
						'<span class="name-contact text-dark text-bold">' + name + '</span>'+
						'<br/>';
						if(name.indexOf("#19") < 0 && name.indexOf("#29") < 0){
						str+='<span class="info-contact text-dark"> Sélectionner le niveau d\'engagement pour cette mesure</span>'+
							'<span class="col-xs-12 text-center select-level-'+id+'">';
								for(i=1; i<=3; i++){
								  	checked="";
								  	if(notNull(data) && typeof data.level!= "undefined" && data.level==i)
								  		checked="checked";
								  	else if(i==1){
								  		checked="checked";
								  	}
								  	str+='<div class="inline margin-left-5"><input type="radio" name="level-'+id+'" class="levelRadio" data-key="'+keyForm+'" data-measure="'+id+'" data-level="'+i+'" value="'+i+'" '+checked+'><label>'+i+'</label></div>';
								}
							'</span>'+
						'</span>' ;
						}
					str+='<button class="bg-red text-white pull-right" style="border: none;font-size: 15px;border-radius: 6px;padding: 5px 10px !important;" onclick="finder.removeFromForm(\''+keyForm+'\', \''+id+'\')"><i class="fa fa-times"></i></button>'+
			"</div>";
		$(".finder-"+keyForm+" .form-list-finder").append(str);
		levelMeasure=(notNull(data) && typeof data.level!= "undefined") ? data.level : 1;
		finder.object[keyForm][id]={"type" : type, "name" : name, "level":levelMeasure};
		$(".levelRadio").off().on("click", function(){
			//$(".select-level-"+$(this).data("measure")+" .levelRadio").removeAttr("checked");
			//$(this).attr("checked", true);
			finder.object[$(this).data("key")][$(this).data("measure")]["level"]=$(this).data("level");
		});
	}else{
		mylog.log("finder.addInForm", keyForm, id, type, name, img);
		img= (notEmpty(img)) ? baseUrl + img : modules.co2.url + "/images/thumb/default_"+type+".png";
		//img= (img != "") ? img : modules.co2.url + "/images/thumb/default_"+type+".png";
		var str="";
		str="<div class='col-xs-12 element-finder element-finder-"+id+" shadow2 padding-10'>"+
					'<img src="'+ img+'" class="img-circle pull-left margin-right-10" height="35" width="35">'+
					'<span class="info-contact pull-left margin-right-5">' +
						'<span class="name-contact text-dark text-bold">' + name + '</span>'+
						'<br/>'+
						'<span class="cp-contact text-light pull-left">' + trad[type]+ '</span>'+
					'</span>' +
					'<button class="bg-red text-white pull-right" style="border: none;font-size: 15px;border-radius: 6px;padding: 5px 10px !important;" onclick="finder.removeFromForm(\''+keyForm+'\', \''+id+'\')"><i class="fa fa-times"></i></button>'+
			"</div>";
		$(".finder-"+keyForm+" .form-list-finder").append(str);

		finder.object[keyForm][id]={"type" : type, "name" : name};


		if(notNull(finder.finderPopulation[keyForm]) && notNull(finder.finderPopulation[keyForm][id]) && notNull(finder.finderPopulation[keyForm][id].email)){
			finder.object[keyForm][id].email = finder.finderPopulation[keyForm][id].email;
		}

		if(notNull(finder.roles) && notNull(finder.roles[keyForm])){
			finder.object[keyForm][id].roles = finder.roles[keyForm];
		}

		if(notNull(finder.search) && notNull(finder.search[keyForm]) && notNull(finder.search[keyForm].filterBy)){
			
			mylog.log("filterBy split", split, finder.selectedItems[keyForm][id][finder.search.filterBy], notNull( finder.selectedItems[keyForm][id][finder.search.filterBy]) );
			if(notNull( finder.selectedItems[keyForm][id][finder.search.filterBy] ) ) {
				var fBy = {} ;
				var split = id.split(".");
				fBy[finder.search[keyForm].filterBy] = finder.selectedItems[keyForm][id][finder.search.filterBy] ;
				finder.object[keyForm][split[0]]=Object.assign({}, finder.object[keyForm][id], fBy);
				delete finder.object[keyForm][id];
			}
		};
	}
};