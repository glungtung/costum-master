var cmsBuilder = {
	init : function(){
		cmsBuilder.bindEvents();
    cmsBuilder.manage.delete();
		$(".openListTpls").off().on("click", function(){
        cmsBuilder.openModal();
		});
	},
  openModal : function(){
      smallMenu.openAjaxHTML(baseUrl+'/'+moduleId+'/cms/directory');
  },
	bindEvents: function(){
    $(".deleteCms").off().on("click", function() {
      var id = $(this).data("id");
      bootbox.confirm(`<div role="alert">
        <p><b class="">Supprimer le bloc</b> <b class="text-red">`+$(this).data("name")+`</b> <b class="">pour toute l'éternité?</b><br><br>
        Nous ne pouvons pas le récupérer une fois que vous le supprimez.<br>
        Êtes-vous sûr de vouloir supprimer définitivement ce bloc?
        </p> 
        </div> `,
        function(result){
          if (!result) {
            return;
          }else{            
           var type = "cms";
           var urlToSend = baseUrl+"/co2/cms/delete/id/"+id;
           $.ajax({
            type: "POST",
            url: urlToSend,
            dataType : "json"
          })
           .done(function (data) {
            if ( data && data.result ) {
              toastr.success("Elément effacé");
              $("#"+type+id).remove();            
              cmsBuilder.openModal();
            } else {
             toastr.error("something went wrong!! please try again.");
           }
         });           
         }
       }); 
    });

		$(".editDescBlock").off().on("click", function() {
            var tplId = $(this).data("id");
            var tplCtx ={};

            var dynFormCostum = {
                "beforeBuild":{
                    "properties":{
                        image : dyFInputs.image(),
                        path : {
                        	label : "Chemin",
                        	inputType : "text",
                      		order: 2
                      	}
                    }
                },
                "onload" : {
                    "setTitle" : "Modifier le cms",
                    "actions" : {
                       "html" : {
                            "nametext>label" : "Nom du bloc",
                            "infocustom" : ""
                        },
                       
                        "hide" : {
                            "documentationuploader" : 1,
                            "structagstags" : 1
                        }
                    }
                },
                afterSave : function(){
                    dyFObj.commonAfterSave(params,function(){
                      dyFObj.closeForm();
                      //$("#ajax-modal").modal('hide');
                      cmsBuilder.openModal();
                    });
                  
                }
            };
            dyFObj.editElement("cms" , tplId, null, dynFormCostum);
          
          });

     
        $(".thisTpls").off().on("click",function() { 
            //tplCtx.key = $(this).data("key");
            tplCtx = {};
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            tplCtx.value = {
              name : $(this).data("name"),
              type : "blockCopy",
              path : $(this).data("path"),
              page : pageCms
            };

            tplCtx.value.parent={};

            tplCtx.value.parent[thisContextId] = {
              type : thisContextType, 
              name : thisContextSlug
            };
            tplCtx.value.haveTpl = false;
            if(typeof tplCtx.value == "undefined")
              toastr.error("value cannot be empty!");
            else {
              mylog.log("activeForm save tplCtx",tplCtx);
              dataHelper.path2Value( tplCtx, function(params) {
                if(params.result){
                  toastr.success("Bloc bien ajouter");
                  $("#ajax-modal").modal("hide");
                  urlCtrl.loadByHash(location.hash);
                  // location.reload();
                }
                else 
                  toastr.error(params.msg);
                  
                  
              } );
            }
        }); 
        $(".createCMS").off().on("click", function(e) {  
            e.preventDefault();       
            var tplCtx = {};
            var dynFormCostum = {
                beforeBuild:{
                    "properties":{
                        image : dyFInputs.image(),
                        path : {
                          label : "Chemin",
                          inputType : "text",
                          value : "tpls.blockCms.[type de block].[nom du block]",
                          order: 2
                        },
                        type : { 
                          "inputType" : "hidden",
                          value : "blockCms" 
                        }
                    }
                },
                onload : {
                    "setTitle" : "Créer un nouveau cms",
                    "actions" : {
                       "html" : {
                            "nametext>label" : "Nom du bloc cms",
                            "infocustom" : ""
                        },
                        "hide" : {
                            "documentationuploader" : 1,
                            "structagstags" : 1
                        }
                    }
                },
                afterSave : function(params){
                  dyFObj.commonAfterSave(params,function(){
                    //alert();
                      //toastr.success("Élément bien ajouter");
                      dyFObj.closeForm();
                      //$("#ajax-modal").modal('hide');
                      cmsBuilder.openModal();
                  });
                }
            };
             dyFObj.openForm("cms", null, null, null, dynFormCostum);
          });
           /* dyFObj.editElement("cms" , tplId, null, dynFormCostum);
            var activeForm = {
              "jsonSchema" : {
                "title" : "Ajouter un nouveau bloc CMS",
                "type" : "object",
                onLoads : {
                  onload : function(data){
                    $(".parentfinder").css("display","none");
                  }
                },
                "properties" : {
                  type : { 
                    "inputType" : "hidden",
                    value : "blockCms" 
                  },
                  path : {
                    label : "Chemin",
                    "inputType" : "text",
                    value : "tpls.blockCms.[type de block].[nom du block]"
                  },
                  name : { 
                    label : "Nom du bloc cms",
                    "inputType" : "text",
                    value : "" 
                  },
                    image : dyFInputs.image(),
                    description : {
                      label : "Description",
                      inputType : "text",
                      value : ""
                    },

                    parent : {
                      inputType : "finder",
                      label : tradDynForm.whoiscarrypoint,
                      multiple : true,
                      rules : { lengthMin:[1, "parent"]}, 
                      initType: ["organizations", "projects", "events"],
                      openSearch :true
                    }
                  }
                },

              };          

              activeForm.jsonSchema.afterBuild = function(){
                dyFObj.setMongoId('cms',function(data){
                  uploadObj.gotoUrl = location.hash;
                  tplCtx.id=dyFObj.currentElement.id;
                  tplCtx.collection=dyFObj.currentElement.type;
                });
              };


              activeForm.jsonSchema.save = function () {
                tplCtx.value = {};
                $.each( activeForm.jsonSchema.properties , function(k,val) { 
                  tplCtx.value[k] = $("#"+k).val();
                });
                if(typeof tplCtx.value == "undefined")
                  toastr.error('value cannot be empty!');
                else {
                  mylog.log("activeForm save tplCtx",tplCtx);
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("Élément bien ajouter");
                      $("#ajax-modal").modal('hide');
                      location.reload();
                    });
                  } );
                }
              }*/
	},
	manage : {
		create : function(){

		},

		delete :function(){
      $(".deleteLine").off().on("click",function (){
          $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
          var id = $(this).data("id");
          var type = $(this).data("collection");
          var urlToSend = baseUrl+"/co2/element/delete/type/"+type+"/id/"+id;
          bootbox.confirm(trad.areyousuretodelete,
            function(result){
              if (!result) {
                return;
              } 
              else {
                $.ajax({
                  type: "POST",
                  url: urlToSend,
                  dataType : "json"
                })
                .done(function (data) {
                  if ( data && data.result ) {
                    toastr.success("Element effacé");
                    $("#"+type+id).remove();
                  } else {
                   toastr.error("something went wrong!! please try again.");
                 }
                 urlCtrl.loadByHash(location.hash);
               });
              }
            }
            );
        });
		},

		addToTpl:function(){

		},
	}
}