directory.elementPanelHtml = function(params, edit, sObj){
	mylog.log("elementPanelHtml hubTerritoires","Params",params);

	
	var dateStr="";
	if(typeof params.updated != "undefined" && notNull(params.updated))
  		dateStr += '<div class="dateUpdated dateUpdated-sm date-position">'+directory.showDatetimePost(params.collection, params.id, params.updated)+'</div>';
	else if(typeof params.created != "undefined" && notNull(params.created))
  		dateStr += '<div class="dateUpdated dateUpdated-sm date-position">'+directory.showDatetimePost(params.collection, params.id, params.created)+'</div>';
	var str='';	
	str +='<div id="entity_'+params.collection+'_'+params.id+'" class="col-lg-4 col-md-4 col-sm-6 col-xs-12 searchEntityContainer '+params.containerClass+'">'+
			'<div class="item-slide">'+
					'<div class="entityCenter" style="position: absolute;">'+
						'<span><i class="fa fa-'+params.icon+' bg-'+params.color+'"></i></span>'+
					'</div>'+
					'<div class="img-back-card">'+
						params.imageProfilHtml +
						'<div class="text-wrap searchEntity">'+
							'<h4 class="entityName">'+
								'<a href="'+params.hash+'" class="uppercase '+params.hashClass+'">'+params.name+'</a>'+
							'</h4>'+
							'<div class="small-infos-under-title text-center">';
								if (typeof params.type != 'undefined') {
	str +=							'<div class="text-center entityType">'+	
										'<span class="text-white">'+((typeof tradCategory[params.type] != "undefined") ? tradCategory[params.type] : params.type)+'</span>'+
									'</div>';
								}
								if (typeof params.typePlace != "undefined"){
	str +=							'<span class="typePlace text-white">'+params.typePlace+'</span>';
								}
								if (notNull(params.localityHtml)) {
	str +=							'<div class="entityLocality">'+
										'<span>'+params.localityHtml+'</span>'+
									'</div>';
								}
	str +=					'</div>'+
							'<div class="entityDescription"></div>'+
						'</div>'+
					'</div>'+
					//hover
					'<div class="slide-hover co-scroll">'+
						'<div class="text-wrap">'+
							'<h4 class="entityName">'+
								'<a href="'+params.hash+'" class="'+params.hashClass+'">'+params.name+'</a>'+
							'</h4>';
							if(typeof edit  != 'undefined' && notNull(edit))
	str += 						directory.getAdminToolBar(params);

							if (typeof params.type != 'undefined') {
	str +=						'<div class="entityType">'+	
									'<span class="text-white">'+((typeof tradCategory[params.type] != "undefined") ? tradCategory[params.type] : params.type)+'</span>'+
								'</div>';
							}
							if (notNull(params.localityHtml)) {
	str +=						'<div class="entityLocality text-center">'+
									'<span> '+params.localityHtml+'</span>'+
								'</div>';
							}
	str +=					'<hr>'+
							'<p style="white-space: pre;" class="p-short">'+params.descriptionStr+'</p>';
							if (typeof params.typePlace != "undefined"){
	str +=						'<span class="typePlace text-white text-center">Type de lieu : '+params.typePlace+'</span>';
							}
							if(typeof params.tagsHtml != "undefined")
	str +=						'<ul class="tag-list">'+params.tagsHtml+'</ul>';
							if(typeof params.rolesHtml != "undefined")
	str += 						'<div class=\'rolesContainer\'>'+params.rolesHtml+'</div>';

	str +=					directory.countLinksHtml(params);
							if(userId != null && userId != '' && typeof params.id != 'undefined' && typeof params.collection != 'undefined') 
	str+=						directory.socialToolsHtml(params);
	str+=				'</div>'+
				'</div>'+
			'</div>'+
		'</div>';
	return str;
};