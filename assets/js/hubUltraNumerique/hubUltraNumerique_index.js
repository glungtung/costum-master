jQuery(document).ready(function() {
	setTitle(costum.contextSlug);
});

coInterface.bindLBHLinks = function() {
	mylog.log("coInterface.bindLBHLinks hubUltraNum");
	$("#second-search-bar").keyup(function(e){ 

	    if ( e.keyCode >= 65 || e.keyCode <= 90 || e.keyCode == 20) {
	    	$("#input-search-map").val($("#second-search-bar").val());
		    $("#second-search-xs-bar").val($("#second-search-bar").val());
		    myScopes.type="open";
		    searchObject.text=$(this).val();
		    $("#dropdown").show();
		    
	    	setTimeout(function(){
	    		startGlobalSearch(0, indexStepGS);
	    	},200);
		}

	    if (! notEmpty(searchObject.text) || searchObject.text == ""){
	        $(".dropdown-result-global-searchbar").css("display","none");
	    }

	    coInterface.scrollTo("#second-search-bar");
	});
	$(".lbh").unbind("click").on("click",function(e) {
		e.preventDefault();
		$("#openModal").modal("hide");
		mylog.warn("***************************************");
		mylog.warn("coInterface.bindLBHLinks",$(this).attr("href"));
		mylog.warn("***************************************");
		searchObject.reset();
		var h = ($(this).data("hash")) ? $(this).data("hash") : $(this).attr("href");
		urlCtrl.closePreview();
		dyFObj.closeForm();
	    urlCtrl.loadByHash( h );
	});
	$(".lbh-menu-app").unbind("click").on("click",function(e){
		e.preventDefault();
		coInterface.simpleScroll(0, 500);
		searchObject.reset();
		//if(!empty(filtersObj))
		if(typeof mapCO != "undefined")
			mapCO.clearMap();
		contextData = null;
		historyReplace=true;
		var h = ($(this).data("hash")) ? $(this).data("hash") : $(this).attr("href");
		urlCtrl.loadByHash(h);
	});
	// Open an url with specific module in url
	$(".lbh-module").unbind("click").on("click",function(e){
		e.preventDefault();
		coInterface.simpleScroll(0, 500);
		searchObject.reset();
		//if(!empty(filtersObj))
		if(typeof mapCO != "undefined")
			mapCO.clearMap();
		contextData = null;
		historyReplace=true;
		var h = ($(this).data("hash")) ? $(this).data("hash") : $(this).attr("href");
		urlCtrl.loadByHash(h, "survey/");
	});
	//open any url in a modal window
	$(".lbhp").unbind("click").on("click",function(e) {
		e.preventDefault();
		$("#openModal").modal("hide");
		mylog.warn("***************************************");
		mylog.warn("!coInterface.bindLBHLinks Preview", $(this).attr("href"),$(this).data("modalshow"));
		mylog.warn("***************************************");
		var h = ($(this).data("hash")) ? $(this).data("hash") : $(this).attr("href");
		if( $(this).data("modalshow") ){
			mylog.log("coInterface.bindLBHLinks Preview if");		
			url = (h.indexOf("#") == 0 ) ? urlCtrl.convertToPath(h) : h;
			if(h.indexOf("#page") >= 0)
				url="app/"+url
	    	smallMenu.openAjaxHTML( baseUrl+'/'+moduleId+"/"+url);
			
		}
		else {
			mylog.log("coInterface.bindLBHLinks Preview else");
			url = (h.indexOf("#") == 0 ) ? urlCtrl.convertToPath(h) : h;
	    	smallMenu.openAjaxHTML( baseUrl+'/'+moduleId+"/"+url);
		}
	});
	//open any url in a preview window
	$(".lbh-preview-element").unbind("click").on("click",function(e) {
		e.preventDefault();
		mylog.warn("***************************************");
		mylog.warn("coInterface.bindLBHLinks Preview ELEMENT", $(this).attr("href"),$(this).data("modalshow"));
		mylog.warn("***************************************");
		onchangeClick=false;
		link = (typeof $(this).data("hash") != "undefined" && notNull($(this).data("hash"))) ? $(this).data("hash") : $(this).attr("href");
		previewHash=link.split(".");
		if($.inArray(previewHash[2], ["proposals", "proposal", "actions", "resolutions"]) >= 0){
				ddaT=(previewHash[2]=="proposals") ? "proposal" : previewHash[2];
				uiCoop.getCoopDataPreview(ddaT,previewHash[4]);
		}else{
	     	hashT=location.hash.split("?");
			getStatus=searchInterface.getUrlSearchParams();     	
	     	urlHistoric=hashT[0].substring(0)+"?preview="+previewHash[2]+"."+previewHash[4];
	        if($("#entity"+previewHash[4]).length > 0) setTimeout(function(){$("#entity"+previewHash[4]).addClass("active");},200); 
	        if(getStatus != "") urlHistoric+="&"+getStatus; 
	        history.replaceState({}, null, urlHistoric);
        	urlCtrl.openPreview(link);
    	}
    });
    $(".open-xs-menu").off().on("click", function(){
		menuToShow=$(this).data("target");
		iconClass=$(this).data("icon");
		labelButton=$(this).data("label");
		if($(this).hasClass("close-menu")){
			urlCtrl.closeXsMenu();
			
		}else{
			$(this).addClass("close-menu");
			$(this).find("i").removeClass(iconClass).addClass("fa-times");
			if($(this).find(".labelMenu").length)
				$(this).find(".labelMenu").text(trad.close);
			$(".menu-xs-container."+menuToShow).show(400);
		}
	});		
};

// coInterface.bindEvents = function(){
// 	coInterface.bindResizeEvent();
// 	// Active button to get dynform
//     coInterface.bindButtonOpenForm();
//     coInterface.bindLBHLinks();
//     coInterface.bindTooltips();
//     $(".logoutBtn").off().on("click", function(){
// 		url=baseUrl+'/'+moduleId+'/person/logout';
// 		if(notNull(costum) && notNull(costum.slug)){
// 			url+="?slug="+costum.slug;
// 		}
// 		//alert(url);
// 		ajaxPost(null, url, 
// 			null,
// 			function(data){
// 				location.reload();
// 				//alert(data.url);
// 			});	
// 	});
// 	/*****Evenement pour montrer le menu d'ajout d'élement*****/
// 	$('.show-bottom-add').off().click(function(){
// 		if(!$(this).hasClass("opened")){
//             $(this).addClass("opened");
//             $(this).find(".toolbar-bottom-adds").show();
//             /*$('.toolbar-bottom-adds a').click(function(){
//                 $(this).find(".toolbar-bottom-adds").hide(200);
//                 $(this).removeClass("opened");
//             });*/
//         }else{
//             $(this).find(".toolbar-bottom-adds").hide(200);
//             $(this).removeClass("opened");
//         }
//     });
//     $('.toolbar-bottom-adds').unbind("mouseleave").mouseleave(function(){
//         //console.log(".toolbar-bottom-adds mouseleave");
//         $(this).parent().removeClass("opened");
//         $(this).hide();

//     });
//     /*****Fin d'évenement sur le bouton show button create elt *****/
	
//     //jQuery for page scrolling feature - requires jQuery Easing plugin
//     // This button can be used to get scroll to the container indicated in href
//     $('.page-scroll a').off().on('click', function(event) {
//         var $anchor = $(this);
//         $('html, body').stop().animate({
//             scrollTop: ($($anchor.attr('href')).offset().top - 50)
//         }, 1250, 'easeInOutExpo');
//         event.preventDefault();
//     });

//     // jQuery for page scrolling feature - requires jQuery Easing plugin
//     $('.btn-scroll').off().on('click', function(event) {
//         var target = $(this).data('targetid');
//         coInterface.scrollTo(target);
//         event.preventDefault();
//     });

//     //Floopdrawer is a div containing all links user did
//     $(".btn-open-floopdrawer").off().click(function(){ 
//         showNotif(false);
//         $("#dropdown-user").removeClass("open");
//         showFloopDrawer(true);
//     });
    

//     // 2 events for notifications
//     $('.btn-menu-notif').off().click(function(){
//       	if($('#notificationPanelSearch').is(":visible"))
//     		showNotif(false);
//     	else
//       		showNotif();
//     });
// 	$("#notificationPanelSearch").off().on("mouseleave",function(){
// 		showNotif(false);
// 	});

//     $(".btn-dashboard-dda").off().click(function(){
//         showFloopDrawer(false);
//         showNotif(false);
//         dashboard.loadDashboardDDA();
//         $("#dropdown-user, .dropdownApps-menuTop").removeClass("open");
//         $("#dropdown-dda").addClass("open");
//     });
  
//     $(".btn-show-map").off().click(function(){
//     	if(typeof mapCO != "undefined")
// 			showMap();

// 		if ( $("#mapContent").is(":visible") == true && typeof pageApp != "undefined" && pageApp == "annuaire") {
// 			alert( $("#mapContent").is(":visible") );
// 			mapCO.map.setZoom(1);
//             mapCO.map.panTo([-20.166667,57.516667]);
// 		}
//     });

//     $(".btn-hide-map").off().click(function(){
//     	mapCO.map.setZoom(1);
//         mapCO.map.panTo([-20.166667,57.516667]);
//     });

//     $(".tooltips").tooltip();
// };


costum[costum.slug] = {
	init : function() {
		$(".imgMenu").each(function() {
			parent = $(this).parent(".lbh-menu-app").attr("href");
			className = parent.split("#");
			$(this).addClass(className[1]+"app");
		});
	},
	autoCompleteSearchGS : function (search, indexMin, indexMax, input, callB){
		mylog.log("globalsearch.js autoCompleteSearchGS hubUltraNum", search, indexMin, indexMax, input, callB);

		options = [ "citoyens", "organizations", "projects", "events" ];

		var data = {"name" : search, "locality" : "", "searchType" : searchTypeGS, "searchBy" : "ALL",
		"indexMin" : indexMin, "indexMax" : indexMax, "category" : searchObject.category  };

		if(!notNull(input)){
			data.indexStep=10;
			data.count=true;
			data.countType = [ "organizations", "projects", "events" ];
			data.searchType = [ "organizations", "projects", "events" ];
		}

		if(typeof costum != "undefined" && notNull(costum) && typeof costum.filters != "undefined" && (!notNull(input) || $.inArray(input, ["#filter-scopes-menu", "#scopes-news-form"]) < 0)){
			if(typeof costum.filters.searchTypeGS != "undefined" && !notNull(input)){ 
				data.countType = costum.filters.searchTypeGS;
				data.searchType = costum.filters.searchTypeGS;
			}
			if(typeof costum.filters.sourceKey != "undefined"){ 
				data.sourceKey=["laReunionPourUnNumeriqueInclusif","mayottePourUnNumeriqueInclusif","wallisetfutunaPourUnNumeriqueInclusif","polynesieFrancaisePourUnNumeriqueInclusif","nouvellecaledoniePourUnNumeriqueInclusif"];
			}
			if (typeof costum.filters.sourceKey != "undefined" && typeof costum.isTemplate != "undefined" && costum.isTemplate) {
				data.sourceKey=[costum.contextSlug];
			}
		}

		var domTarget = (notNull(input)) ? input+" .dropdown-result-global-searchbar" : ".dropdown-result-global-searchbar";

		if($(domTarget+" .content-resultbar").length > 0)
        domTarget+=" .content-resultbar";
    	var dropDownVisibleDom = (notNull(input)) ? input+" .dropdown-result-global-searchbar" : ".dropdown-result-global-searchbar";

		showDropDownGS(true, dropDownVisibleDom);

		if(indexMin > 0)
			$("#btnShowMoreResultGS").html("<i class='fa fa-spin fa-circle-o-notch'></i> "+trad.currentlyresearching+" ...");
		else{
			$(domTarget).html("<h5 class='text-dark center padding-15'><i class='fa fa-spin fa-circle-o-notch'></i> "+trad.currentlyresearching+" ...</h5>");  
		}


		if(search.indexOf("co.") === 0 ){
			var searchT = search.split(".");
			if( searchT[1] && typeof co[ searchT[1] ] == "function" ){
				co[ searchT[1] ](search);
				return;
			} else {
				co.mands();
			}
		}

		ajaxPost(
	        null,
	        baseUrl+"/"+moduleId+"/search/globalautocomplete",
	        data,
	        function(data){
	        	spinSearchAddon();
				if(!data){ 
					toastr.error(data.content); 
				}
				else{
					mylog.log("DATA GS");
					mylog.dir(data);

					var countData = 0;
					if(typeof data.count != "undefined")
						$.each(data.count, function(e, v){countData+=v;});
					else
						$.each(data.results, function(i, v) { if(v.length!=0){ countData++; } });

					totalDataGS += countData;
					var str = "";
					var city, postalCode, totalDataGSMSG = "";

					if(totalDataGS == 0)      totalDataGSMSG = "<i class='fa fa-ban'></i> "+trad.noresult;
					else if(totalDataGS == 1) totalDataGSMSG = totalDataGS + " "+trad.result;   
					else if(totalDataGS > 1)  totalDataGSMSG = totalDataGS + " "+trad.results;   

					if(totalDataGS > 0){
						var labelSearch=(Object.keys(data.results).length == totalDataGS) ? trad.extendedsearch : "Voir tous les résultats";
						str += '<div class="text-left col-xs-12" id="footerDropdownGS" style="">';
							str += "<label class='text-dark margin-top-5'><i class='fa fa-angle-down'></i> " + totalDataGSMSG + "</label>";
							str += '<a href="#search" class="btn btn-default btn-sm pull-right lbh" id="btnShowMoreResultGS">'+
										'<i class="fa fa-angle-right"></i> <i class="fa fa-search"></i> '+labelSearch+
									'</a>';
						str += '</div>';
						str += "<hr style='margin: 0px; float:left; width:100%;'/>";
					}else{
						str += "<div class='padding-20'>";
							str += "<b style='font-style: italic;font-size:1.1em;'> <i class='fa fa-ban' aria-hidden='true'></i> Aucun Résultats </b>";
						str += "<div>";
					}

					$.each(data.results, function(i, o) {
						mylog.log("globalsearch res : ", o);
						var typeIco = i;
						var ico = "fa-"+typeObj["default"].icon;
						var color = mapColorIconTop["default"];

						mapElementsGS.push(o);
						typeIco = o.type;

						if(typeof o.typeOrga != "undefined")
							typeIco = o.typeOrga;

						var obj = (dyFInputs.get(typeIco)) ? dyFInputs.get(typeIco) : typeObj["default"] ;
						ico =  "fa-"+obj.icon;
						color = obj.color;
						
						htmlIco ="<i class='fa "+ ico +" fa-2x bg-"+color+"'></i>";
						if("undefined" != typeof o.profilThumbImageUrl && o.profilThumbImageUrl != ""){
							var htmlIco= "<img width='80' height='80' alt='' class='img-circle bg-"+color+"' src='"+baseUrl+o.profilThumbImageUrl+"'/>";
						}

						city="";

						var postalCode = o.postalCode;
						if (o.address != null) {
							city = o.address.addressLocality;
							postalCode = o.postalCode ? o.postalCode : o.address.postalCode ? o.address.postalCode : "";
						}

						var id = getObjectId(o);
						var type = o.type;
						if(type=="citoyens") type = "person";
						var url = (notEmpty(o.collection) && notEmpty(id)) ? 
						        '#page.type.'+o.collection+'.id.' + id : "";

						var onclickCp = "";
						var target = " target='_blank'";
						var dataId = "";
						if(type == "city"){
							dataId = o.name; //.replace("'", "\'");
						}


						var tags = "";
						if(typeof o.tags != "undefined" && o.tags != null){
							$.each(o.tags, function(key, value){
								if(value != "")
									tags +=   "<a href='javascript:' class='badge bg-red btn-tag'>#" + value + "</a>";
							});
						}

						var name = typeof o.name != "undefined" ? o.name : "";
						if(typeof o.title != "undefined")
							name =  o.title;
						postalCode = (	typeof o.address != "undefined" &&
											o.address != null &&
											typeof o.address.postalCode != "undefined") ? o.address.postalCode : "";

						if(postalCode == "") postalCode = typeof o.postalCode != "undefined" ? o.postalCode : "";
						var cityName = (typeof o.address != "undefined" &&
										o.address != null &&
										typeof o.address.addressLocality != "undefined") ? o.address.addressLocality : "";
	                  	var countryCode=(typeof o.address != "undefined" && notNull(o.address) && typeof o.address.addressCountry != "undefined") ? "("+o.address.addressCountry+")" : ""; 
						var fullLocality = postalCode + " " + cityName+" "+countryCode;
						if(fullLocality == " Addresse non renseignée" || fullLocality == "" || fullLocality == " ") 
							fullLocality = "<i class='fa fa-ban'></i>";
						mylog.log("fullLocality", fullLocality);

						var description = (	typeof o.shortDescription != "undefined" &&
											o.shortDescription != null) ? o.shortDescription : "";
						if(description == "") description = (	typeof o.description != "undefined" &&
																o.description != null) ? o.description : "";
	           
						// var startDate = (typeof o.startDate != "undefined") ? "Du "+dateToStr(o.startDate, "fr", true, true) : null;
						// var endDate   = (typeof o.endDate   != "undefined") ? "Au "+dateToStr(o.endDate, "fr", true, true)   : null;

						var followers = (typeof o.links != "undefined" && o.links != null && typeof o.links.followers != "undefined") ?
						                o.links.followers : 0;
						var nbFollower = 0;
						if(followers !== 0)                 
							$.each(followers, function(key, value){
							nbFollower++;
						});

						target = "";
						if(type=="proposals")
							url="javascript:;";
						var classA=(type=="proposals") ? "openCoopPanelHtml" : "lbh";
						var attrA="";
						if(type=="proposals"){ 
						 	attrA="data-coop-type='proposals' data-coop-id='"+id+"' data-coop-idparentroom='";
						 	if(typeof o.idParentRoom != "undefined") attrA+=o.idParentRoom;
						 	attrA+="' data-coop-parentid='"+data.parentId+"' data-coop-parenttype='"+data.parentType+"'";
						}
						mylog.log("type", type);
						if(type != "city" && type != "zone" ){ 
							str += "<a href='"+url+"' class='"+classA+" col-md-12 col-sm-12 col-xs-12 no-padding searchEntity' "+attrA+">";
							str += "<div class='col-md-2 col-sm-2 col-xs-2 no-padding entityCenter text-center'>";
							str +=   htmlIco;
							str += "</div>";
							str += "<div class='col-md-10 col-sm-10 col-xs-10 entityRight'>";

							str += "<div class='entityName text-dark'>" + name + "</div>";

							str += '<div data-id="' + dataId + '"' + "  class='entityLocality'>"+
							"<i class='fa fa-home'></i> " + fullLocality;

							if(nbFollower >= 1)
							str +=    " <span class='pull-right'><i class='fa fa-chain margin-left-10'></i> " + nbFollower + " follower</span>";

							str += '</div>';

							str += "</div>";

							str += "</a>";
						}else{
							o.input = input;
		         	 		mylog.log("Here",o, typeof o.postalCode);

		         	 		
							if(type == "city"){
								var valuesScopes = {
									city : o._id.$id,
									cityName : o.name,
									postalCode : (typeof o.postalCode == "undefined" ? "" : o.postalCode),
									postalCodes : (typeof o.postalCodes == "undefined" ? [] : o.postalCodes),
									country : o.country,
									allCP : o.allCP,
									uniqueCp : o.uniqueCp,
									level1 : o.level1,
									level1Name : o.level1Name
								};

								if( notEmpty( o.nameCity ) ){
									valuesScopes.name = o.nameCity ;
								}

								if( notEmpty( o.uniqueCp ) ){
									valuesScopes.uniqueCp = o.uniqueCp;
								}
								if( notEmpty( o.level5 ) && valuesScopes.id != o.level5){
									valuesScopes.level5 = o.level5 ;
									valuesScopes.level5Name = o.level5Name ;
								}
								if( notEmpty( o.level4 ) && valuesScopes.id != o.level4){
									valuesScopes.level4 = o.level4 ;
									valuesScopes.level4Name = o.level4Name ;
								}
								if( notEmpty( o.level3 ) && valuesScopes.id != o.level3 ){
									valuesScopes.level3 = o.level3 ;
									valuesScopes.level3Name = o.level3Name ;
								}
								if( notEmpty( o.level2 ) && valuesScopes.id != o.level2){
									valuesScopes.level2 = o.level2 ;
									valuesScopes.level2Name = o.level2Name ;
								}

								valuesScopes.type = o.type;
								valuesScopes.key = valuesScopes.city+valuesScopes.type+valuesScopes.postalCode ;
								o.key = valuesScopes.key;
								mylog.log("valuesScopes city", valuesScopes);
			         	 		myScopes.search[valuesScopes.key] = valuesScopes;
								str += directory.cityPanelHtml(o);
							}
							else if(type == "zone"){


								valuesScopes = {
									id : o._id.$id,
									name : o.name,
									country : o.countryCode,
									level : o.level
								};
								mylog.log("valuesScopes",valuesScopes);
								var typeSearchCity, levelSearchCity;
								if(o.level.indexOf("1") >= 0){
									typeSearchCity="level1";
									levelSearchCity="1";
									valuesScopes.numLevel = 1;
								}else if(o.level.indexOf("2") >= 0){
									typeSearchCity="level2";
									levelSearchCity="2";
									valuesScopes.numLevel = 2;
								}else if(o.level.indexOf("3") >= 0){
									typeSearchCity="level3";
									levelSearchCity="3";
									valuesScopes.numLevel = 3;
								}else if(o.level.indexOf("4") >= 0){
									typeSearchCity="level4";
									levelSearchCity="4";
									valuesScopes.numLevel = 4;
								}else if(o.level.indexOf("5") >= 0){
									typeSearchCity="level5";
									levelSearchCity="5";
									valuesScopes.numLevel = 5;
								}
								if(notNull(typeSearchCity))
									valuesScopes.type = typeSearchCity;				

								mylog.log("valuesScopes test", (valuesScopes.id != o.level1), valuesScopes.id, o.level1);

								if( notEmpty( o.level1 ) && valuesScopes.id != o.level1){
									mylog.log("valuesScopes test", (valuesScopes.id != o.level1), valuesScopes.id, o.level1);
									valuesScopes.level1 = o.level1 ;
									valuesScopes.level1Name = o.level1Name ;
								}

								var subTitle = "";
								if( notEmpty( o.level5 ) && valuesScopes.id != o.level5){
									valuesScopes.level5 = o.level5 ;
									valuesScopes.level5Name = o.level5Name ;
									subTitle +=  (subTitle == "" ? "" : ", ") +  o.level4Name ;
								}
								if( notEmpty( o.level4 ) && valuesScopes.id != o.level4){
									valuesScopes.level4 = o.level4 ;
									valuesScopes.level4Name = o.level4Name ;
									subTitle +=  (subTitle == "" ? "" : ", ") +  o.level4Name ;
								}
								if( notEmpty( o.level3 ) && valuesScopes.id != o.level3 ){
									valuesScopes.level3 = o.level3 ;
									valuesScopes.level3Name = o.level3Name ;
									subTitle +=  (subTitle == "" ? "" : ", ") +  o.level3Name ;
								}
								if( notEmpty( o.level2 ) && valuesScopes.id != o.level2){
									valuesScopes.level2 = o.level2 ;
									valuesScopes.level2Name = o.level2Name ;
									subTitle +=  (subTitle == "" ? "" : ", ") +  o.level2Name ;
								}
								//objToPush.id+objToPush.type+objToPush.postalCode
								valuesScopes.key = valuesScopes.id+valuesScopes.type ;
								mylog.log("valuesScopes.key", valuesScopes.key, valuesScopes);
								myScopes.search[valuesScopes.key] = valuesScopes;

								mylog.log("myScopes.search", myScopes.search);
								o.key = valuesScopes.key;
								str += directory.zonePanelHtml(o);
							}
						}
					}); //end each
					extendMsg=trad.extendedsearch;
					extendUrl="#search";
					if(typeof costum != "undefined" && notNull(costum) && typeof costum.searchOpenMenu != "undefined"){
						extendMsg=costum.searchOpenMenu.msg;
						extendUrl=costum.searchOpenMenu.url;
					}

		           	if(countData==0 && searchTypeGS == "cities"){
		                str="<div class='text-center col-md-12 col-sm-12 col-xs-12 padding-10'>"+
		                      '<label class="text-dark italic"><i class="fa fa-ban"></i> '+trad.nocityfoundfor+' "'+search+'"</label><br/>'+
		                      '<span class="info letter-blue"><i class="fa fa-info-circle"></i> '+trad.explainnofoundcity+'</span><br/>'+
		                      '<button class="btn btn-blue bg-blue text-white main-btn-create" '+
		                        'data-target="#dash-create-modal" data-toggle="modal">'+
		                          '<i class="fa fa-plus-circle"></i> '+trad.createpage+
		                      '</button>'+
		                    "</div>";
		            }


	                $(domTarget).html(str);
					$(domTarget).scrollTop(0);

					showDropDownGS(true, dropDownVisibleDom);
					bindScopesInputEvent();
					if(notEmpty(callB)){
						callB();
					}
		            coInterface.bindLBHLinks();

		            mylog.log("loadingDataGS false");
		            loadingDataGS = false;
	          	}

				if(indexMax - countData > indexMin){
					$("#btnShowMoreResultGS").remove(); 
					scrollEndGS = true;
				}else{
					scrollEndGS = false;
				}
	        },
	        function (data){
				mylog.log("error"); mylog.dir(data);          
			}
		);   
	}
}