directory.coopPanelHtml = function(params, key,size){
		params.chat = true;
		//params.invite = true;
		mylog.log('-----------proposalPanelHtml cmcas', params, key);
		var idParentRoom = typeof params.idParentRoom != 'undefined' ? params.idParentRoom : '';
		if(idParentRoom == '' && params.type == 'rooms') idParentRoom = params.id;
		//mylog.log("-----------idParentRoom", idParentRoom);
      
		var name = (typeof params.title != 'undefined' && params.title != 'undefined') ? params.title : params.name;

		name = escapeHtml(name);
		var thisId = typeof params['_id'] != 'undefined' &&
                   typeof params['_id']['$id'] != 'undefined' ? params['_id']['$id'] : 
			typeof params['id'] != 'undefined' ? params['id'] : '';

		var filterClass = '';
		var sortData = '';
		if(typeof params.votes != 'undefined'){
			if(typeof params.votes.up != 'undefined'){
				filterClass += ' upFilter';
				sortData += ' data-sort=\''+params.votes.up.length+'\' ';
			}
			if(typeof params.votes.down != 'undefined'){
				filterClass += ' downFilter';
				sortData += ' data-sort=\''+params.votes.down.length+'\'';
			}
		}

		var str = '';  
		if(size == 'S')     
			str += '<div class=\'col-lg-4 col-md-4 col-sm-6 col-xs-12 blockCoop'+thisId+' '+filterClass+' coopFilter coop-wraper margin-bottom-10 \' '+sortData+' style=\'word-wrap: break-word; \'>';
		else 
			str += '<div class=\'col-xs-12 coop-wraper margin-bottom-10 coopFilter '+filterClass+' \' '+sortData+' style=\'word-wrap: break-word;\'>';

		var linkParams = ' data-coop-type=\''+ params.type + '\' '+
                    ' data-coop-id=\''+ thisId + '\' '+
                    ' data-coop-idparentroom=\''+ idParentRoom + '\' '+
                    ' data-coop-parentid=\''+ params.parentId + '\' '+'data-coop-parenttype=\''+ params.parentType + '\' ';

		str += '<div class=\'blockCoop'+ params.parentId + ' searchEntity coopPanelHtml\' '+linkParams+'>';
		var diffColor = (params.sourceKey) ? 'border-left:3px solid red;' : '';
		str += '<div class=\'panel-heading border-light col-lg-12 col-xs-12\' style=\''+diffColor+'\'>';


		// IMAGE 
		if(typeof params.imgMediumProfil == 'undefined'){
			//quand on reload apres un vote on ne passe pas forcement par showResultsDirectoryHtml
			if(!params.useMinSize){
				params.imgProfil = '<i class=\'fa fa-image fa-2x\'></i>';
				params.imgMediumProfil = '<i class=\'fa fa-image fa-2x\'></i>';
			}
                
			if('undefined' != typeof params.profilMediumImageUrl && params.profilMediumImageUrl != '')
				params.imgMediumProfil= '<img class=\'img-responsive\' onload=\'directory.checkImage(this);\' src=\''+baseUrl+params.profilMediumImageUrl+'\'/>';
		}
		str += '<a href=\'javascript:;\' '+linkParams+' class=\'margin-bottom-10 all-coop-detail-desc'+ thisId + ' openCoopPanelHtml container-img-profil\'>' + params.imgMediumProfil +'</a>';
            
		// OPEN PANEL BTN
		// str += "<button class='btn btn-sm btn-default pull-right openCoopPanelHtml bold letter-turq' "+linkParams+
		//         "><i class='fa fa-chevron-right'></i> <span class='hidden-xs'>"+trad.Open+"</span></button>";

		// NAME
		if(name != ''){
			str += '<a href="javascript:;" class="openCoopPanelHtml" style="text-decoration:none;" '+linkParams+' >'+
                          '<h4 class="panel-title tooltips letter-turq" data-toggle="tooltip" data-placement="bottom" data-original-title="'+name+'">'+
                          '<i class="fa '+ params.ico + '"></i> '+ 
                          ( (name.length > 100) ? name.substring(0,100)+'...' : name ) + '</h4></a>';
		}                          

		if(	typeof params.producer != 'undefined' && 
				params.producer != null &&
				Object.keys(params.producer).length > 0 ) {
			var count=Object.keys(params.producer).length;
			var htmlAbout='';
			$.each(params.producer, function(e, v){
				var heightImg=(count>1) ? 35 : 25;
				var imgIcon = (typeof v.profilThumbImageUrl != 'undefined' && v.profilThumbImageUrl!='' ) ? baseUrl+'/'+v.profilThumbImageUrl: parentModuleUrl + '/images/thumb/default_'+v.type+'.png';  

				htmlAbout+='<span ';
				if(count>1) htmlAbout+= 'data-toggle="tooltip" data-placement="left" title="'+v.name+'"';
				htmlAbout+='>'+
						'<img src="'+imgIcon+'" class="img-circle margin-right-10" width='+heightImg+' height='+heightImg+' />';
				if(count==1) htmlAbout+=v.name;
				//htmlAbout+="</a>";
				htmlAbout+='</span>';
			});
			var htmlHeader = ((params.type == typeObj.event.col) ? trad['Planned on'] : tradCategory.carriedby ) ;
			htmlHeader += ' : '+htmlAbout;

			str += htmlHeader;
		}

		// STATE OF THE PROPOSAL
          
		if(params.type != 'rooms'){
			str += '<h5 class="col-sm-12">';
			var statusColor = 'red';
			if(params.status =='adopted')
				statusColor = 'green';
			else if(params.status =='refused')
				statusColor = 'red';
			else if(params.status =='amendable')
				statusColor = 'orange';
			str +=  '<small class="text-'+statusColor+'"><i class="fa fa-certificate"></i> '+trad[params.status]+'</small>';

			// YOUR VOTE STATUS
			if(typeof userId != 'undefined' && userId != null && userId != ''){
				if((params.status == 'tovote' || params.status == 'amendementAndVote') && params.hasVote ===false)
					str +=  '<small class="margin-left-15 letter-red"><i class="fa fa-ban"></i> '+trad['You did not vote']+'</small>';
				else if((params.status == 'tovote' || params.status == 'amendementAndVote') && params.hasVote !==false)
					str +=  '<small class="margin-left-15"><i class="fa fa-thumbs-up"></i> '+trad['You did vote']+'</small>';
			}

			str += '</h5>';
            
		}

          
          
		str += '<div class="all-coop-detail">';

		str += '<div class="all-coop-detail-desc'+ thisId + '">';            
                

		if( typeof params.shortDescription != 'undefined' && params.shortDescription != ''){
			str += '<hr>';
			str += '<span class="col-xs-12 no-padding text-dark descMD">'+params.shortDescription+'</span>';
		}

		if( typeof params.thematique != 'undefined' && params.thematique != ''){
			str += '<hr>';
			str += '<span style="font-size:12px;" class="col-xs-12 margin-bottom-20 no-padding text-dark thematique"> Thématique : '+params.thematique+'</span><br>';
		} 

		var voteCount = '';
		if(params.votes){
			var vc = 0;
			if(params.votes.up)
				vc += Object.keys(params.votes.up).length;
			if(params.votes.down)
				vc += Object.keys(params.votes.down).length;
			if(params.votes.uncomplet)
				vc += Object.keys(params.votes.uncomplet).length;
			if(params.votes.white)
				vc += Object.keys(params.votes.white).length;
			voteCount = ' ('+vc+')';
		}
		//SHOW HIDE VOTE BTNs
		var btnSize;
		if(typeof userId != 'undefined' && userId != null && userId != ''){
			btnSize = (params.status == 'amendementAndVote') ? '6' : '12';
			if( (params.status == 'tovote' || params.status == 'amendementAndVote') && params.hasVote ===false )
				str += '<a href="javascript:;" '+ linkParams + ' data-coop-section="vote"  class="bg-green  openCoopPanelHtml btn col-sm-'+btnSize+' "><i class="fa fa-gavel"></i> '+trad.Vote+voteCount+'</a>';

			else if( (params.status == 'tovote' || params.status == 'amendementAndVote') && params.hasVote !==false )
				str += '<a href="javascript:;" '+ linkParams + ' data-coop-section="vote"  class="openCoopPanelHtml btn btn-default col-sm-'+btnSize+' "><i class="fa fa-eye"></i> '+trad['See votes']+voteCount+'</a>';
		} else {
			btnSize = (params.status == 'amendementAndVote') ? '6' : '12';
			if( (params.status == 'tovote' || params.status == 'amendementAndVote') && params.hasVote ===false )
				str += '<a href="javascript:" data-toggle="modal" data-target="#modalLogin"  class="btn-menu-connect bg-green btn col-sm-'+btnSize+' "><i class="fa fa-gavel"></i> '+trad.Vote+voteCount+'</a>';
		}
                
		if( (params.status == 'amendementAndVote'  || params.status =='amendable') ){
			var amendCount = (params.amendements) ? ' ('+Object.keys(params.amendements).length+')': '';
			str += '<a href="javascript:;" '+ linkParams + ' data-coop-section="amendments"  class="openCoopPanelHtml btn btn-default text-purple col-sm-6 "><i class="fa fa-list"></i> '+trad.Amendements+amendCount+'</a>';
		}

		if(params.votes ){
			str += '<div class=\'col-sm-12 padding-10\'>';
			if(params.votes.up)
				str += '<div class=\'col-sm-3 text-green tooltips text-center vote-counter-entity\' data-toggle=\'tooltip\' data-placement=\'bottom\' data-original-title=\''+trad.Agree+'\'><i class=\'fa fa-thumbs-up\'></i> '+Object.keys(params.votes.up).length+'</div>';
			if(params.votes.down)
				str += '<div class=\'col-sm-3 text-red tooltips text-center vote-counter-entity\' data-toggle=\'tooltip\' data-placement=\'bottom\' data-original-title=\''+trad.Disagree+'\'><i class=\'fa fa-thumbs-down\'></i> '+Object.keys(params.votes.down).length+'</div>';
			if(params.votes.uncomplet)
				str += '<div class=\'col-sm-3 text-orange tooltips text-center vote-counter-entity\' data-toggle=\'tooltip\' data-placement=\'bottom\' data-original-title=\''+trad.Uncomplet+'\'><i class=\'fa fa-hand-grab-o\'></i> '+Object.keys(params.votes.uncomplet).length+'</div>';
			if(params.votes.white)
				str += '<div class=\'col-sm-3 text-dark tooltips text-center vote-counter-entity\' data-toggle=\'tooltip\' data-placement=\'bottom\' data-original-title=\''+trad.Abstain+'\'><i class=\'fa fa-circle-o\'></i> '+Object.keys(params.votes.white).length+'</div>';
			str += '</div>';
		}

                
               

		str += '</div>';

		//VOTE BTNS
		str += '<div class="all-coop-detail-votes'+ thisId + ' hide">';
                
		//BTN toggle description
		str += '<a href="javascript:" data-coop-id="'+ thisId + '"  class="btn-openVoteDetail"><i class="text-dark fa fa-arrow-circle-left"></i></a>';
                
		if((params.auth || typeof params.idParentRoom == 'undefined') 
                    && (params.status == 'tovote' || params.status == 'amendementAndVote') )
		{


			var isMulti = typeof params.answers != 'undefined';
			var answers = isMulti ? params.answers : 
				{ 'up':'up', 'down': 'down', 'white': 'down', 'uncomplet':'uncomplet'};
                  
			$.each(answers, function(key, val){
				var voteRes = ( typeof params.voteRes != 'undefined' &&
                                   typeof params.voteRes[key] != 'undefined' ) ? params.voteRes[key] : false;             
                     
				str += '<div class="col-xs-12 no-padding timeline-panel">';

				if((params.status == 'tovote' || params.status == 'amendementAndVote') && (!params.hasVote  || params.voteCanChange == 'true')){
					str += '<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2 no-padding text-right pull-left margin-top-20">';

					str += '  <button class="btn btn-send-vote btn-link btn-sm bg-vote bg-'+voteRes['bg-color']+'"';
					str += '     title="'+trad.clicktovote+'" ';
					str += '      data-idparentproposal="'+thisId+'"';
					str += '      data-idparentroom="'+ params.idParentRoom +'"';
					str += '      data-vote-value="'+key+'"><i class="fa fa-gavel"></i>';
					str += '  </button>';

					if(params.hasVote  === ''+key)
						str +=  '<br><i class="fa fa-user-circle padding-10" title="'+trad['You voted for this answer']+'"></i> ';
                          
					str += '</div>';
				}

				str += '<div class="col-lg-11 col-md-11 col-sm-11 col-xs-10">';
                      
				var hashAnswer = !isMulti ? trad[voteRes['voteValue']] : (key+1);

				str +=    '<div class="padding-10 margin-top-15 border-vote border-vote-'+key+'">';
				str +=      '<i class="fa fa-hashtag"></i><b>'+hashAnswer+'</b> ';
				if(isMulti) 
					str +=        voteRes['voteValue'];
				str +=    '</div>';

				if(voteRes !== false && voteRes['percent']!=0){
					str +=  '<div class="progress progress-res-vote">';
					str +=       '<div class="progress-bar bg-vote bg-'+voteRes['bg-color']+'" role="progressbar" ';
					str +=       'style="width:'+voteRes['percent']+'%">';
					str +=       voteRes['percent']+'%';
					str +=     '</div>';
					str +=     '<div class="progress-bar bg-transparent" role="progressbar" ';
					str +=       'style="width:'+(100-voteRes['percent'])+'%">';
					str +=      voteRes['votant']+' <i class="fa fa-gavel"></i>';
					str +=     '</div>';
					str +=  '</div>';
				}
				str += '</div>';

				str += '</div>';
			});
		} 
                
		str += '</div>';

		str += '</div>';
		str += '</div>';
		str += '</div>';  
		str += '</div>';
		return str;
};



directory.elementPanelHtml = function(params){
		mylog.log('directory.elementPanelHtml cmcas',params.type,params.name,params.elTagsList, params);
		var str = '';

		var grayscale = ( ( notNull(params.isInviting) && params.isInviting == true) ? 'grayscale' : '' ) ;
		var tipIsInviting = ( ( notNull(params.isInviting) && params.isInviting == true) ? trad['Wait for confirmation'] : '' ) ;
		var classType=params.type;
		classType =(typeof params.category != 'undefined' && notNull(params.category)) ? ' '+params.category : '';
		if(params.type=='events') classType='';

		str += '<div class=\'col-lg-4 col-md-4 col-sm-6 col-xs-12 searchEntityContainer '+grayscale+' '+classType+' '+params.elTagsList+' '+params.elRolesList+' contain_'+params.type+'_'+params.id+'\'>';
		str +=    '<div class="searchEntity" id="entity'+params.id+'">';

		if(typeof params.edit  != 'undefined' && notNull(params.edit))
			str += this.getAdminToolBar(params);

		if( params.tobeactivated == true ){
			str += '<div class=\'dateUpdated\'><i class=\'fa fa-flash\'></i> <span class=\'hidden-xs\'>'+trad['Wait for confirmation']+' </span></div>';
		}else{
			var timeAction= trad.actif;

			var update = params.updated ? params.updated : null; 


			if (params.type != null && 
            params.type == 'projects' &&
            (  typeof params.properties != 'undefined' &&
               typeof params.properties.avancement != 'undefined') ) {

				if (params.properties.avancement == 'inprogress') {
					str += '<div class=\'dateUpdated\'>'+
                        '<span class=\'status\' style=\'margin-left:23px;\'>'+
                          '<i style=\'position:absolute;margin-left:-24px;font-size:18px;\' class=\'fa fa-play-circle iconesStatus\'></i>'+
                            '<p class=\'textStatus\' style=\'position:absolute;color:black;visibility:hidden\'> En cours</p>'+
                        '</span> <i class=\'fa fa-flash\'></i>'+
                        '<span class=\'hidden-xs\'>'+timeAction+
                        '</span>' + update + 
                      '</div>';
				}
				else if (params.properties.avancement == 'stopped') {
					str += '<div class=\'dateUpdated\'>'+
                        '<span class=\'status\' style=\'margin-left:23px;\'>'+
                          '<i style=\'position:absolute;margin-left:-24px;font-size:18px;\' class=\'fa fa-pause-circle iconesStatus\'></i>'+
                            '<p class=\'textStatus\' style=\'position:absolute;color:black;visibility:hidden\'> Stoppé</p>'+
                        '</span> <i class=\'fa fa-flash\'></i>'+
                        '<span class=\'hidden-xs\'>'+timeAction+
                        ' </span>' + update + 
                      '</div>';
				}
				else if (params.properties.avancement == 'archived') {
					str += '<div class=\'dateUpdated\'>'+
                        '<span class=\'status\' style=\'margin-left:23px;\'>'+
                          '<i style=\'position:absolute;margin-left:-24px;font-size:18px;\' class=\'fa fa-check-circle iconesStatus\'></i>'+
                            '<p class=\'textStatus\' style=\'position:absolute;color:black;visibility:hidden\'> Archivé</p>'+
                        '</span> <i class=\'fa fa-flash\'></i>'+
                        '<span class=\'hidden-xs\'>'+timeAction+
                        '</span>' + update + 
                      '</div>';
				}
				else if (params.properties.avancement == 'upcoming'){
					str += '<div class=\'dateUpdated\'>'+
                        '<span class=\'status\' style=\'margin-left:23px;\'>'+
                          '<i style=\'position:absolute;margin-left:-24px;font-size:18px;\' class=\'fa fa-lightbulb-o iconesStatus\'></i>'+
                            '<p class=\'textStatus\' style=\'position:absolute;color:black;visibility:hidden\'> A venir</p>'+
                        '</span> <i class=\'fa fa-flash\'></i>'+
                        '<span class=\'hidden-xs\'>'+timeAction+
                        '</span>' + update + 
                      '</div>';
				}
			}
			else{
				str += '<div class=\'dateUpdated\'> <i class=\'fa fa-flash\'></i> <span class=\'hidden-xs\'>'+timeAction+' </span>' + update + '</div>';
			}
		}
      
		var linkAction = ( $.inArray(params.type, ['poi','classifieds'])>=0 ) ? ' lbh-preview-element' : ' lbh';

		if( typeof directory.costum != 'undefined' &&
        directory.costum != null &&
        typeof directory.costum.preview != 'undefined' && 
        directory.costum.preview === true ){
			linkAction = ' lbh-preview-element';
		}

		if(typeof params.imgType !='undefined' && params.imgType=='banner'){
			str += '<a href=\''+params.hash+'\' class=\'container-img-banner add2fav '+linkAction+'>' + params.imgBanner + '</a>';
			str += '<div class=\'padding-10 informations tooltips\'  data-toggle=\'tooltip\' data-placement=\'top\' data-original-title=\''+tipIsInviting+'\'>';

			str += '<div class=\'entityRight banner no-padding\'>';

			if(typeof params.size == 'undefined' || params.size == undefined || params.size == 'max'){
				str += '<div class=\'entityCenter no-padding\'>';
				str +=    '<a href=\''+params.hash+'\' class=\'container-thumbnail-profil add2fav '+linkAction+'\'>' + params.imgProfil + '</a>';
				str +=    '<a href=\''+params.hash+'\' class=\'add2fav pull-right margin-top-15 '+linkAction+'\'>' + params.htmlIco + '</a>';
				str += '</div>';
			}
		}else{
			str += '<a href=\''+params.hash+'\' class=\'container-img-profil add2fav '+linkAction+'\'>' + params.imgMediumProfil + '</a>';
			str += '<div class=\'padding-10 informations tooltips\'  data-toggle=\'tooltip\' data-placement=\'top\' data-original-title=\''+tipIsInviting+'\'>';

			str += '<div class=\'entityRight profil no-padding\'>';

			if(typeof params.size == 'undefined' || params.size == undefined || params.size == 'max'){
				str += '<div class=\'entityCenter no-padding\'>';
				str +=    '<a href=\''+params.hash+'\' class=\'add2fav pull-right '+linkAction+'\'>' + params.htmlIco + '</a>';
				str += '</div>';
			}
		}


		var iconFaReply ='';
		str += '<a  href=\''+params.hash+'\' class=\''+params.size+' entityName bold text-dark add2fav '+linkAction+'\'>'+
					iconFaReply + params.name +
				'</a>';  
		
		if(typeof(params.statusLink)!='undefined'){
			if(typeof(params.statusLink.isAdmin)!='undefined'
         && typeof(params.statusLink.isAdminPending)=='undefined'
          && typeof(params.statusLink.isAdminInviting)=='undefined'
            && typeof(params.statusLink.toBeValidated)=='undefined')
				str+='<span class=\'text-red\'>'+trad.administrator+'</span>';
			if(typeof(params.statusLink.isAdminInviting)!='undefined'){
				str+='<span class=\'text-red\'>'+trad.invitingToAdmin+'</span>';
			}
			if(typeof(params.statusLink.toBeValidated)!='undefined' || typeof(params.statusLink.isAdminPending)!='undefined')
				str+='<span class=\'text-red\'>'+trad.waitingValidation+'</span>';
		}

		if(params.rolesLbl != '')
			str += '<div class=\'rolesContainer\'>'+params.rolesLbl+'</div>';
 

	

		var thisLocality = '';
		if(params.fullLocality != '' && params.fullLocality != ' ')
			thisLocality = '<a href=\''+params.hash+'\' data-id=\'' + params.dataId + '\'  class=\'entityLocality add2fav'+linkAction+'\'>'+
							'<i class=\'fa fa-home\'></i> ' + params.fullLocality + '</a>';
		else thisLocality = '';

		str += thisLocality;

		var devise = (typeof params.devise != 'undefined') ? params.devise : '';
		if(typeof params.price != 'undefined' && params.price != '')
			str += '<div class=\'entityPrice text-azure\'><i class=\'fa fa-money\'></i> ' + params.price + ' ' + devise + '</div>';
 
		if($.inArray(params.type, ['classifieds','ressources'])>=0 && typeof params.category != 'undefined'){
			str += '<div class=\'entityType col-xs-12 no-padding\'><span class=\'uppercase bold pull-left\'>' + tradCategory[params.section] + ' </span><span class=\'pull-left\'>';
			if(typeof params.category != 'undefined' && params.type != 'poi') str += ' > ' + tradCategory[params.category];
			if(typeof params.subtype != 'undefined') str += ' > ' + tradCategory[params.subtype];
			str += '</span></div>';
		}
		if(notEmpty(params.typeEvent))
			str += '<div class=\'entityType\'><span class=\'uppercase bold\'>' + tradCategory[params.typeEvent] + '</span></div>';  
    
		if(params.type=='events'){
			var dateFormated = directory.getDateFormated(params, true);
			var countSubEvents = ( params.links && params.links.subEvents ) ? '<br/><i class=\'fa fa-calendar\'></i> '+Object.keys(params.links.subEvents).length+' '+trad['subevent-s']  : '' ;
			str += dateFormated+countSubEvents;
		}
		str += '<div class=\'entityDescription\'>' + ( (params.shortDescription == null ) ? '' : params.shortDescription ) + '</div>';
		if (params.itemType == "projects") {
			if (params.categ != null) {
			str += '<div style="font-size:12px;margin-bottom:1%;" class=\'entityCateg\'>' + ( (params.categ == null ) ? '' : params.categ ) + ( (params.subcateg == null ) ? '' : ' > ' + params.subcateg ) + ( (params.subsubcateg == null ) ? '' : ' > '+params.subsubcateg )+'</div>';
			}
   		}
		str += '<div class=\'tagsContainer text-red\'>'+params.tagsLbl+'</div>';
		if(typeof params.counts != 'undefined'){
			str+='<div class=\'col-xs-12 no-padding communityCounts\'>';
			$.each(params.counts, function (key, count){
				var iconLink=(key=='followers') ? 'link' : 'group';
				str +=  '<small class=\'pull-left lbh letter-light bg-transparent url elipsis bold countMembers margin-right-10\'>'+
                      '<i class=\'fa fa-'+iconLink+'\'></i> '+ count + ' ' + trad[key] +
                    '</small>';
			});
			str+='</div>';
		}

		if(userId != null && userId != '' && typeof params.id != 'undefined' && typeof params.type != 'undefined') 
			str+=directory.socialToolsHtml(params);  
		str += '</div>';
		str += '</div>';
		str += '</div>';
		str += '</div>';
		str += '</div>';
		return str;
}