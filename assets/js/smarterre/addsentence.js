
function addSentence(){

    
    
    var params = {
        "name" : $("#sentenceField").val(),
        "docType" : "smarterre."+window.location.href.split("#")[1],
        "targetId" : userId,
        "targetType" : "citoyens"
    };
        //async : false
    ajaxPost(
        null,
        baseUrl+"/"+moduleId+"/folder/crud/action/new",
        params,
        function(data){ 
            toastr.success("Votre phrase a bien été ajouté");
            $("#exampleModalCenter").modal('toggle');
        }
    );
    
}
