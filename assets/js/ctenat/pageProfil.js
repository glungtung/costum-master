var formParentId=null;
pageProfil.initCallB = function(){
	if(contextData.type=="projects"){
		rolesList=["Partenaire"];
		if(typeof contextData.category != "undefined" && contextData.category=="ficheAction")
			rolesList.push("Porteur d'action", "Financeur");
		if(typeof contextData.category != "undefined" && contextData.category=="cteR")
			rolesList.push("Porteur du CTE", "Financeur");
	}else if(contextData.type=="organizations"){
		rolesList=["Référent CTE", "Salarié"];
	}
}
pageProfil.menuLeftShow = function(){
	if(!$(".menu-xs-only-top").is(":visible"))
		$(".menu-xs-only-top").show(200);
	else
		$(".menu-xs-only-top").hide(200);
};

pageProfil.views.home = function(){
	if(contextData.type=="projects" && typeof contextData.category != "undefined"){
		if(typeof contextData.category != "undefined" && contextData.category=="cteR")
			formParentId=contextData.id;
		ajaxPost('#central-container', baseUrl+'/costum/ctenat/elementhome/type/'+contextData.type+"/id/"+contextData.id, 
					null,
					function(){},"html");
	}else{
		pageProfil.views.detail();
	}
};

pageProfil.views.ficheAction = function(){
	formParentId=contextData.id;
	// // ajaxPost('#central-container', baseUrl+"/survey/co/new/id/"+contextData.slug+"/session/1",
	// // 	null,
	// // 	function(){},"html");
	// var idF = null;
	// if(typeof costum.forms != "undefined"){
	// 	$.each(costum.forms, function(kF, vF){
	// 		idF = kF;
	// 	});
	// }
	// // if(idF != null)
	// // 	ajaxPost('#central-container', baseUrl+"/survey/answer/index/id/new/form/"+idF+"/contextId/"+contextData.id+"/contextType/"+contextData.collection,
	// // 		null,
	// // 		function(){},"html");
	// if(idF != null)
	// 	ajaxPost('#central-container', baseUrl+"/survey/answer/index/id/new/form/"+idF+"/contextId/"+contextData.id+"/contextType/"+contextData.collection,
	// 		null,
	// 		function(){},"html");
	if(typeof costum.forms != "undefined" && Object.keys(costum.forms)[0] != null){
		// ajaxPost('#central-container', baseUrl+"/survey/answer/new/id/"+costum.contextId+"/contextId/"+contextData.id+"/contextType/"+contextData.collection+"/tpl/costum.views.custom.ctenat.home_new_answer",
		// 	null,
		// 	function(){},"html");
		pageProfil.form = formObj.init({});
		pageProfil.form.urls.answer(pageProfil.form, "new", Object.keys(costum.forms)[0], null, contextData.id, contextData.collection);
	}
};

pageProfil.views.answers= function(){
	if(typeof contextData.links != "undefined" && typeof contextData.links.projects != "undefined")
		formParentId=Object.keys(contextData.links.projects)[0];
	if(typeof contextData.links != "undefined" && typeof contextData.links.answers != "undefined"){
		// ajaxPost('#central-container', baseUrl+"/survey/answer/index/id/"+Object.keys(contextData.links.answers)[0]+"/mode/w",
		// 	null,
		// 	function(){
		// 		//alert("pageProfil.params.subview : "+pageProfil.params.subview)
		// 		if(pageProfil.params.subview && $("#"+pageProfil.params.subview).length )
		// 			$("#"+pageProfil.params.subview).removeClass("hide");
		// 		else 
		// 			$("#explain").removeClass("hide");
		// 	},"html");
		pageProfil.form = formObj.init({});
		mylog.log("pageProfil.views.answers ", Object.keys(contextData.links.answers)[0]);
		pageProfil.form.urls.answer(pageProfil.form, Object.keys(contextData.links.answers)[0], null , "w");
	}else if(notEmpty(pageProfil.params.dir)){
		ajaxPost('#central-container', baseUrl+"/survey/co/answer/id/"+pageProfil.params.dir+"/view/answerEdit",
			null,
			function(){},"html");
	}else{
		$('#central-container').html( "<span class='alert alert-warning col-xs-12 padding-20 text-center'>"+
			"Désolé, nous n'avons trouvé aucune candidature correspondante à ce projet<br/>"+
			"Veuillez-vous rapprocher des administrateurs du site afin que l'on puisse régler le problème"+
		"</span>");
	}
};

pageProfil.views.projects= function(){
	ajaxPost('#central-container', baseUrl+'/'+moduleId+'/app/search/page/projects', 
		null,
		function(){
			$("#filters-nav").empty();
			$("#central-container").prepend("<div id='filterContainer' class='searchObjCSS'></div>");
			var paramsFilter= {
			 	container : "#filterContainer",
			 	defaults : {
			 		types : [ "projects" ],
			 		filters : {
				 		category : "ficheAction"
					},
					indexStep : 0
				},
				results : {
			 		smartGrid : true,
			 		renderView : "directory.elementPanelHtml"
			 	},
			 	filters : {
			 		text : true,
			 		domainAction : {
			 			view : "selectList",
			 			type : "tags",
			 			name : "Domaine d'action",
			 			event : "tags",
			 			list : costum.lists.domainAction
			 		},
			 		cibleDD:{
			 			view : "selectList",
			 			type : "tags",
			 			name : "Objectif",
			 			event : "tags",
			 			list : costum.lists.cibleDD
			 		},
			 		scope : true
			 	}
			 };	 
			 paramsFilter.defaults.filters["links.projects."+contextData.id] = {'$exists' :1};
		    filterGroup = searchObj.init(paramsFilter);					
		},"html");
};

//pageProfil.projectsOthers
pageProfil.views.dashboard= function(){
	ajaxPost('#central-container', baseUrl+'/costum/ctenat/dashboard/slug/'+contextData.slug, 
					null,
					function(){},"html");
};

pageProfil.views.actionDash= function(){
	ajaxPost('#central-container', baseUrl+'/costum/ctenat/dashboard/action/'+contextData.slug, 
					null,
					function(){},"html");
};
pageProfil.views.gallery= function(){
	pageProfil.params.dir="file";
	var url = "gallery/index/type/"+contextData.collection+"/id/"+contextData.id+"/docType/file";
	if(notNull(pageProfil.params.key))
		url+="/contentKey/"+pageProfil.params.key;

	if(notNull(pageProfil.params.folderId))
		url+="/folderId/"+pageProfil.params.folderId;
	ajaxPost('#central-container', baseUrl+'/'+moduleId+'/'+url, 
		null,
		function(){
			$(".dropdown-add-file").html(
				'<a class="btn show-nav-add" href="javascript:dyFObj.openForm(\'addFile\')"><i class="fa fa-plus-circle"> Ajouter des fichiers</a>');
		});
};
	
pageProfil.views.adminCter = function(){
	formParentId=contextData.id;
	authorizedSubview=["candidatures", "communityCter"];
	subUrl="";
	if(typeof pageProfil != "undefined" && typeof pageProfil.params.subview != "undefined"){
		if($.inArray(pageProfil.params.subview, authorizedSubview) >=0)
			subUrl=(typeof pageProfil != "undefined" && typeof pageProfil.params.subview != "undefined") ? "/subview/"+pageProfil.params.subview : "";
	}
	subUrl+="/contextType/"+contextData.type+"/contextId/"+contextData.id;
	$("#central-container").hide().parent().append("<div class='simulate-central-container col-xs-12 col-xs-12 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1 shadow2'></div>");
	coInterface.showLoader(".simulate-central-container");
	ajaxPost('#central-container', baseUrl+"/"+moduleId+"/app/admin"+subUrl,
		{
			subView : true,
			options : { "menu" : false, addButton : false}
		},
		function(){
			str='<li class="text-center list-group-item col-xs-12  col-sm-offset-1 col-sm-10">'+
					'<a href="javascript:;" class="btnNavAdmin text-green" data-view="candidatures" style="cursor:pointer;">'+
							'<i class="fa fa-tasks fa-2x"></i> Gestion des actions'+
					'</a>'+
				'</li>'+
				'<li class="text-center list-group-item col-xs-12  col-sm-offset-1 col-sm-10">'+
					'<a href="javascript:;" class="btnNavAdmin text-yellow" data-view="communityCter" style="cursor:pointer;">'+
							'<i class="fa fa-group fa-2x"></i> Membres'+
					'</a>'+
				'</li>'+
				'<li class="text-center list-group-item col-xs-12  col-sm-offset-1 col-sm-10">'+
					'<a href="javascript:;" class="btnNavAdmin text-blue" data-view="strategyCter" style="cursor:pointer;">'+
							'<i class="fa fa-tags fa-2x"></i> Gestion des orientations'+
					'</a>'+
				'</li>'+
				'<li class="text-center list-group-item col-xs-12  col-sm-offset-1 col-sm-10">'+
					'<a href="javascript:;" class="btnNavAdmin text-green-k" data-view="newRocketChat" style="cursor:pointer;">'+
							'<i class="fa fa-plus-circle fa-2x"></i> Créer un nouveau canal de chat personnalisé'+
					'</a>'+
				'</li>';
			
			$(".menuAdmin").html(str);
			copyFicheActionFrom=false;
			strExplain="";
			strMenu="";
			if(canEdit){
				if(notEmpty(contextData.links) 
				&& typeof contextData.links.forms != "undefined"){
					$.each(contextData.links.forms, function(e,v){
						if(typeof v.copyForm != "undefined" && v.copyForm=="ficheAction")
							copyFicheActionFrom=true;
					});
				}
				if(copyFicheActionFrom){
					isOnAAP=(typeof contextData.showAAP != "undefined" && contextData.showAAP) ? "checked" : ""; 
					strMenu+="<div class='col-xs-12 no-padding text-center'>"+
						"<span class='col-xs-12'>"+
							"<i class='fa fa-info-circle'></i> Rendre public à tous sur votre accueil le formulaire d'appel à projet"+
						"</span>"+
						'<div class="col-xs-12 no-padding activeAAP">'+
							'<input id="" type="checkbox" data-off-text="Off" data-on-text="On" class="BSswitch" '+isOnAAP+'>'+
						'</div>'+
			
						//'<input id="" type="checkbox" data-off-text="Désactivé" data-on-text="Activé" data-sub="invite" checked="false" class="activeAAP">'+
			
						"</div>";
				}
				strExplain=(copyFicheActionFrom) ? "Activer l'appel à projet" :"";
					strMenu+='<div class="col-xs-12 margin-top-20 margin-bottom-20 text-center">';
					if(copyFicheActionFrom){
						strMenu+='<button class="ssmla btn col-xs-12 col-sm-8 col-sm-offset-2 btn-cter-create" data-view="ficheAction"><i class="fa fa-plus-circle"></i> Ajouter une action</button>';
					}
					strMenu+='<button class="btn-add-organization btn-cter-create btn col-xs-12 col-sm-8 col-sm-offset-2 margin-top-10"><i class="fa fa-plus-circle"></i> Ajouter une organisation</button>';
					strMenu+='<button class="btn-cter-create btn col-xs-12 col-sm-8 col-sm-offset-2 margin-top-10" onclick="dyFObj.openForm(\'event\');"><i class="fa fa-plus-circle"></i> Ajouter un événement</button>';
					strMenu+='<button class="btn-add-badge btn-cter-create btn col-xs-12 col-sm-8 col-sm-offset-2 margin-top-10"><i class="fa fa-plus-circle"></i> Ajouter une orientation</button>';
					
				strMenu+='</div>';
			}

			$(".infoPanelAddContent").html(strExplain);
			$(".contain-admin-add").html(strMenu);
			$("#central-container").show();
			$(".simulate-central-container").remove();
			$(".activeAAP .BSswitch").bootstrapSwitch();
		   	$(".activeAAP .BSswitch").on("switchChange.bootstrapSwitch", function (event, state) {
		   		dataHelper.path2Value( {
                collection : contextData.type,
                id : contextData.id,
                path : "showAAP",
                value : state
            	}, function(data){ 
            		contextData.showAAP=data.elt.showAAP; 
            		msg=(typeof data.elt.showAAP != "undefined" && data.elt.showAAP) ? "L'appel à projet est désormais activé sur la page d'accueil" : "L'appel à projet est désormais désactivé";
            		toastr.success(msg)
            	});
		    	//settings.savePreferencesNotification("notifications",state, "citoyens", userId, $(this).data("sub"));
		    });
		   	
			adminPanel.params.hashUrl="#@"+contextData.slug+".view.adminCter";
			$(".btn-add-organization").click(function(){
				//var dyfbadge=jQuery.extend(true, {}, typeObj.badge.dynFormCostum);
				//var dyfbadge=typeObj['badge']["dynFormCostum"]);
				var dyforga={
					"beforeBuild":{
						"properties":{
							"roles" : {
								"inputType" : "tags",
								"label":tradDynForm["addroles"],
								"placeholder":tradDynForm["addroles"],
								"values":rolesList
							},
							"links[projects]":{
								"inputType" : "hidden"
							}
						}
					},
					 "onload" : {
                        "actions" : {
                        	"html":{
                        		"infocustom" : "<p style='font-size: 20px;'><i class='fa fa-info-circle'></i> Si l'organisation existe déjà, invitez depuis le bouton 'inviter des contributeurs' disponible dans le bandeau du territoire</p>"
                        	},
                            "presetValue" : {
                                "role" : "admin",
                                "links[projects]" : {
                                    "eval" : "contextData.id"
                                }
                            },
                            "hide" : {
                                "urltext" : 1,
                                "tagstags" : 1,
                                "parentfinder" : 1,
                                "categoryselect":1,
                                "publiccheckboxSimple" : 1,
                                "roleselect" : 1
                            }
                         }
                    }
				};
				//return false;
				dyFObj.openForm('organization', null, null,null,dyforga);
			});
			$(".btn-add-badge").click(function(){
				costum.ctenat.strategy.openForm();
				/*var dyfbadge=jQuery.extend(true, {}, typeObj.badge.dynFormCostum);
				//var dyfbadge=typeObj['badge']["dynFormCostum"]);
				dyfbadge.beforeBuild.properties.parent = {
					inputType : "finder",
					label : "Porteur de l'orientation stratégique",
					//multiple : true,
	                initMe:false,
	                placeholder:"Rechercher le porteur de l'orientation ?",
					rules : { required : false, lengthMin:[1, "parent"]}, 
					initType: ["projects"],
					openSearch :true
				};
				dyfbadge.beforeBuild.properties.name.label="Titre de l'orientation stratégique";
				dyfbadge.beforeBuild.properties.description={"label":"Principaux enjeux et objectifs de l’orientation"};
				dyfbadge.beforeBuild.properties.synergie = {
                    "inputType" : "textarea",
                    "markdown" : true,
                    "label" : "Synergie et articulation avec d’autres démarches territoriales",
                    "rules" : {
                        "maxlength" : 2000
                    }
                };
				dyfbadge.onload.actions.hide["categoryselect"]=1;
				dyfbadge.onload.actions.hide["infocustom"]=1;
				dyfbadge.onload.actions.presetValue["category"]="strategy";
				
				//return false;
				dyFObj.openForm('badge', null, null,null,dyfbadge);*/
			});
			adminPanel.bindViewActionEvent();
			pageProfil.bindViewActionEvent();
		},"html");
};
	
pageProfil.actions.generateAAP = function(){
	ajaxPost(
        null,
       	baseUrl+"/survey/co/index/id/"+contextData.slug+"/copy/ficheAction",
        {},
        function(data){ 
            toastr.success("L'appel à projet a été généré avec succès");
			urlCtrl.loadByHash(location.hash);
        }
    );
}