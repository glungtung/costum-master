adminPanel.views.group = function(){
	var data={
		title : "Groupe de travail",
		paramsFilter : {
            container : "#filterContainer",
            defaults : {
                types : [ "organizations"]
            },
            filters : {
                text : true
            
            }
        },
		table : {
            name: {
                name : "Nom"
            },
            categoryNA: {
                name : "Category"
            },
            tags : { 
            	name : "Mots clés"
            }
        },
        actions : {
        	update : true,
        	categoryNA:true,
        	delete : true
        }
	};
	ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
};

adminPanel.views.collections = function(){
	var data={
		title : "Collections",
		paramsFilter : {
            container : "#filterContainer",
            defaults : {
                types : [ "badges"]
        		forced : {
        			"category": "collections"
        		}
            },
            filters : {
                text : true
            
            }
        },
		table : {
            name: {
                name : "Nom"
            }
        },
        actions : {
        	update : {
				subType : "collections"
			},
        	delete : true
        }
	};
	ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
};

adminPanel.views.genres = function(){
	var data={
		title : "Genres",
		paramsFilter : {
            container : "#filterContainer",
            defaults : {
                types : [ "badges"]
                forced : {
                	"category": "genres"
                }
            },
            filters : {
                text : true
            
            }
        },
		table : {
            name: {
                name : "Nom"
            }
        },
        actions : {
        	update : {
				subType : "genres"
			},
        	delete : true
        }
	};
	ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
};

adminDirectory.values.categoryNA = function(e, id, type, aObj){
	mylog.log("adminDirectory.values categoryNA", e, id, type, aObj);
	var str = "";
	
	if( typeof e.categoryNA != "undefined" ){
		$.each(e.categoryNA, function(e,v){
			if(str != "")
				str += "<br>";
			str += v;
		});
	}
	return str;
};

adminDirectory.actions.categoryNA = function(e, id, type, aObj){
	mylog.log("adminDirectory.values categoryNA", e, id, type, aObj);
	var str = '<button data-id="'+id+'" data-type="'+type+'" class="ssmla categoryNA btn btn-categoryNA col-xs-12"><i class="fa fa-pencil"></i> Modifier la catégorie</button>';

	return str;
};

adminDirectory.bindCostum = function(aObj){
	mylog.log("adminDirectory.bindCostum ", aObj);

	$("#"+aObj.container+" .categoryNA").off().on("click", function(){
		mylog.log("adminDirectory.bindCostum .categoryNA ", $(this).data("id"), $(this).data("type"));
		var id = $(this).data("id");
		var type = $(this).data("type");
		var elt = aObj.getElt(id, type) ;
		var listStatus = {
			"group" : "Groupe",
			"producors" : "Producteur",
			"supports" : "Soutien",
			"partner" : "Partenaire",

		};
		var statusElt = "" ;

		

		if(	typeof elt != "undefined" && 
			typeof elt.categoryNA != "undefined" ){
			statusElt = elt.categoryNA;
		}
			

		var form = {
			saveUrl : baseUrl+"/costum/notragora/updatecategory",
			dynForm : {
				jsonSchema : {
					title : "Modifier les Statuts",
					icon : "fa-key",
					onLoads : {
						sub : function(){
							$("#ajax-modal #collection").val(type);
						}
					},
					afterSave : function(data){
						mylog.dir(data);
						dyFObj.closeForm();
						aObj.search(0);
						
						
					},
					properties : {
						//collection : dyFInputs.inputHidden(),
						id : dyFInputs.inputHidden(),
						path : dyFInputs.inputHidden(""),
						
						// value : dyFInputs.inputSelect("Choisir un statut", 
						// 	"Choisir un statut", 
						// 	listStatus, {required : true}),

						value : {
							inputType : "select",
                            label : "Categories",
                            options : listStatus,
                            rules : {required : true},
                            groupOptions : true,
                            groupSelected : false,
                            select2 : {
                                "multiple" : true
                            },
						}
					}
				}
			}
		};

		var dataUpdate = {
			value : statusElt,
			collection : type,
			id : id,
			path : "source.status."+costum.slug
		};
		mylog.log("adminDirectory .statusBtn form", form);
		dyFObj.openForm(form, "sub", dataUpdate);
	});


	
	return str;
} ;

adminPanel.views.productions=function(){
	var data={
		title : "Liste des productions",
		paramsFilter : {
            container : "#filterContainer",
            defaults : {
                types : [ "poi"],
                forced:{
                	type:"video"
                }
            },
            filters : {
                text : true
            
            }
        },
		table : {
            name: {
                name : "Nom"
            },
            tags : { 
            	name : "Mots clés"
            }
        },
        actions : {
        	update : true,
        	delete : true
        }
	};
	ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
};


adminDirectory.events.delete = function(aObj){
	mylog.log("adminDirectory.events.delete NA");
	$("#"+aObj.container+" .deleteBtn").off().on("click", function(){
		mylog.log("adminDirectory.events.delete NA click", $(this).data("id"), $(this).data("type"));
		directory.deleteElement($(this).data("type"), $(this).data("id"), $(this), function(){
			mylog.log("adminDirectory.events.delete NA click callback");
			window.location.reload();
		});
		
	});
}