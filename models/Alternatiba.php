<?php 
class Alternatiba{
    const COLLECTION = "costum";
    const CONTROLLER = "costum";
    const MODULE = "costum";

    public static function getCommunity($params){
        $community = Element::getCommunityByTypeAndId($params["contextType"],$params["contextId"],"organizations",null,"alternatiba");

        if (isset($community)) {
            if (@$params["affiche"] == "community") {
               $results = self::getDataCommunity($community);
            }
            else if (@$params["affiche"] == "poi"){
                $results = self::getCommunityPoi($community);
            }
            else{
                $results = self::getDataEventCommunity($community);
            }
        }
        return $results;
    }

    private static function getDataCommunity($data){
        $paramsResponse = array(
            "result" => false
        );

        $elements = [];

        foreach ($data as $k => $v) {
        $elements[$k] = Element::getElementSimpleById($k, $v["type"], null, array("_id","name", "profilImageUrl","profilRealBannerUrl","links","slug", "address","geo", "geoPosition", "profilThumbImageUrl", "profilMarkerImageUrl","type" )); 
        }
    // var_dump($elements);exit;

        if (isset($elements)) {
            $paramsResponse = array(
                "result" => true
            );

            $results  = self::createResultCommunity($elements);
            return array_merge($paramsResponse,$results);
        }
        return $results;
    }

    private static function createResultCommunity($paramsResponse){

        $resCitizen["elt"] = array();

        foreach($paramsResponse as $key => $value){
            if(@$value["type"] != "")
                $value["typeOrga"] = $value["type"];
                    $value["type"] = "organizations";
                    $value["typeSig"] = Organization::COLLECTION;
                    
            $imgMedium = (@$value["profilMediumImageUrl"] ? $value["profilMediumImageUrl"] : "none");
            $img = (@$value["profilImageUrl"] ? $value["profilImageUrl"] : "none");
            $imgBanner = (@$value["profilRealBannerUrl"] ? $value["profilRealBannerUrl"] : "none");
            $countActus = PHDB::count(News::COLLECTION,array("source.key" => $value["slug"]));
            
            $resCitizen["elt"][$key] = array(
            "id"               => (String) $value["_id"],
            "name"             =>  $value["name"],
            "imgMedium"        =>  $imgMedium,
            "img"              =>  $img,
            "imgBanner"        =>  $imgBanner,
            "type"             => $value["typeSig"],
            "profilMarkerImageUrl" => @$value["profilMarkerImageUrl"],
            "address"          =>  @$value["address"],
            "geo"              =>  @$value["geo"],
            "geoPosition"      =>  @$value["geoPosition"],
            "profilThumbImageUrl" => @$value["profilThumbImageUrl"],
            "slug"             =>  $value["slug"],  
            "countEvent"       =>  count(@$value["links"]["events"]),
            "countActeurs"     =>  count(@$value["links"]["members"]),
            "countProjet"      =>  count(@$value["links"]["projects"]),
            "countActus"       =>  @$countActus
            );
        }
    return $resCitizen;
    }

    public static function getCommunityPoi($resultats){
        $param = array(
            "result" => false
        );

        $pList = array();

        foreach ($resultats as $key => $value) {
            $resultsData[$key] = Element::getElementById($key,$value["type"]);
        }

        if (isset($resultsData) && !empty($resultsData)) {
            foreach ($resultsData as $key => $value) {
                  $pList += PHDB::findAndSort(Poi::COLLECTION, 
                        array( 
                          "source.key"=>$value["slug"],
                          "type"=>"article"), array("updated"=>-1), 3 );
            }
        }

        if (isset($pList)) {

            $param = array(
                "result" => true
            );

            $results = self::createResultPoi($pList);
            array_merge($results,$param);
        }

        return $results;
    }

    public static function createResultPoi($results){
        $res["element"] = array();

        foreach ($results as $key => $value) {

            array_push($res["element"],array(
                "id"  =>  (String) $value["_id"],
                "type"  =>  $value["type"],
                "name"  =>  $value["name"],
                "shortDescription"  =>  @$value["shortDescription"],
                "description"  =>  @$value["description"],
                "profilImageUrl"  =>  @$value["profilImageUrl"],
                "profilMediumImageUrl"  =>  @$value["profilMediumImageUrl"],
                "profilThumbImageUrl"  =>  @$value["profilThumbImageUrl"],
                "comment"  =>  @$value["comment"],
                "commentCount"  =>  @$value["commentCount"],
                "collectionCount"  =>  @$value["collectionCount"]
            ));

        }
        return $res;
    }
    private static function getDataEventCommunity($data){

        $elements = [];
        $tabres = [];

        foreach ($data as $k => $v) {
            $elements[$k] = Element::getElementSimpleById($k, $v["type"], null, array("name", "profilImageUrl","profilRealBannerUrl","links","slug"));

            $tabres += PHDB::find(Event::COLLECTION,array(
                                                        "source"    => array(
                                                        "insertOrign" =>    "costum",
                                                        "keys"         =>    array(
                                                        $elements[$k]["slug"]),
                                                        "key"          =>  $elements[$k]["slug"])));
        }

        if (isset($tabres)) {
            $results  = self::createResultEventCommunity($tabres);
        }
        return $results;
    }

    private static function createResultEventCommunity($test){

        $result["data"] = array();

        foreach($test as $key => $value){

            $imgMedium = (@$value["profilMediumImageUrl"] ? $value["profilMediumImageUrl"] : "none");
            $img = (@$value["profilImageUrl"] ? $value["profilImageUrl"] : "none");
            $resume = (@$value["shortDescription"] ? $value["shortDescription"] : "");

            array_push($result["data"], array(
            "id"               => (String) $value["_id"],
            "name"             =>  $value["name"],
            "startDate"        =>  date(DateTime::ISO8601, $value["startDate"]->sec),
            "type"             =>  Yii::t("event",$value["type"]),
            "sourceKey"        =>  $value["source"]["keys"],
            "imgMedium"        =>  $imgMedium,
            "img"              =>  $img,
            "resume"           =>  $resume,
            "slug"             =>  $value["slug"]
            ));
        }
        return $result;
    }
}
