<?php

class Notragora {
	const COLLECTION = "costums";
	const CONTROLLER = "costum";
	const MODULE = "costum";
	

    public static $dataBinding_allPoi  = array(
        "id"        => array("valueOf" => "id"),
        "name"      => array("valueOf" => "name"),
        "links"       => array("valueOf" => "links"),
        "parent"       => array("valueOf" => "parent"),
        "image"     => array("valueOf" => "image",
                             "type"     => "url"),
        "address"   => array("parentKey"=>"address", 
                             "valueOf" => array(
                                    "@type"             => "PostalAddress", 
                                    "streetAddress"     => array("valueOf" => "streetAddress"),
                                    "postalCode"        => array("valueOf" => "postalCode"),
                                    "addressLocality"   => array("valueOf" => "addressLocality"),
                                    "codeInsee"         => array("valueOf" => "codeInsee"),
                                    "addressRegion"     => array("valueOf" => "addressRegion"),
                                    "addressCountry"    => array("valueOf" => "addressCountry")
                                    )),
        "geo"   => array("parentKey"=>"geo", 
                             "valueOf" => array(
                                    "@type"             => "GeoCoordinates", 
                                    "latitude"          => array("valueOf" => "latitude"),
                                    "longitude"         => array("valueOf" => "longitude")
                                    )),
        "description"       => array("valueOf" => "description"),
        "tags"      => array("valueOf" => "tags"),
    );
	public static function setSourceAllElement($params){
        if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
            $colElts = array(Person::COLLECTION, Organisation::COLLECTION, Poi::COLLECTION);
            foreach ($colElts as $keyCol => $col) {
                $source = array(
                    'insertOrign' => "import",
                    'date'=> new MongoDate(time()),
                    "key" => "notragora",
                    "keys" => array("notragora") );
                      
                PHDB::update($col,
                    array() , 
                    array('$set' => array("source" => $source))
                );

            }
        }
        return $params;
    }

     public static function elementBeforeSave($data){
        if($data["collection"]==Badge::COLLECTION){
            $elt=Element::getElementById($data["id"], $data["collection"], null, array("name", "category"));
            if(!empty($data["elt"]["parent"])){
                unset($data["elt"]["parent"]);
            }


            if(!empty($elt["name"])){
                $where = array( "source.key" => "notragora",
                                "tags" => array('$in' => array($elt["name"]) ) ) ;

                //Rest::json( $where) ; exit; 
                $actionPush = array('$push' => array('tags' => $data["elt"]["name"] ) );

                $actionPull = array('$pull' => array('tags' => $elt["name"] ) );

                $options = array('multiple' => 1);
                //$rescount = PHDB::count(Poi::COLLECTION, $where);
                $resPush = PHDB::updateWithOptions( Poi::COLLECTION, $where, $actionPush, $options );
                $resPull = PHDB::updateWithOptions( Poi::COLLECTION, $where, $actionPull, $options );

                // $res = array("rescount" => $rescount,
                //     "resPush" => $resPush,
                //     "resPull" => $resPull,
                //     "actionPull" => $actionPull,
                //     "actionPush" => $actionPush,
                //     "options" => $options);
                //Rest::json($res); exit; 
            }
            //Rest::json($data); exit; 
        }
        //var_dump($data);
        return $data;
    }
    public static function elementBeforeDelete($data){
        
        if($data["collection"]==Badge::COLLECTION){
            $elt=Element::getElementById($data["id"], $data["collection"], null, array("name", "category"));

            if(!empty($elt["name"])){
                $where = array( "source.key" => "notragora",
                                "tags" => array('$in' => array($elt["name"]) ) ) ;

                //Rest::json( $where) ; exit; 
                //$actionPush = array('$push' => array('tags' => $data["elt"]["name"] ) );

                $actionPull = array('$pull' => array('tags' => $elt["name"] ) );

                $options = array('multiple' => 1);
                // $rescount = PHDB::count(Poi::COLLECTION, $where);
                // $resPush = PHDB::updateWithOptions( Poi::COLLECTION, $where, $actionPush, $options );
                $resPull = PHDB::updateWithOptions( Poi::COLLECTION, $where, $actionPull, $options );

                // $res = array("rescount" => $rescount,
                //     "resPush" => $resPush,
                //     "resPull" => $resPull,
                //     "actionPull" => $actionPull,
                //     "actionPush" => $actionPush,
                //     "options" => $options);
            }
        }

        return $data;
    }


    // db.students.update(
    //    { grades: 60 },
    //    { $push: { grades: 999 } },
    //    { multi: true }
    // )

    // db.students.update(
//    { grades: 60 },
//    { $pull: { grades: 60 } },
//    { multi: true }
// )
}
?>