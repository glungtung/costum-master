<?php 
/**
 * 
 */
class Cocity {
	const COLLECTION = "costum";
	const CONTROLLER = "costum";
	const MODULE = "costum";
	public static function getCity($post){
		$name = PHDB::find(Organization::COLLECTION,array("name"=>array('$regex' => $post["nameCity"])));
		return($name);
	}
	public static function getListCocity($post){
		$listCocity = PHDB::find(Organization::COLLECTION,array("costum.slug"=>$post["slug"]));
		return($listCocity);
	}

	public static function getFiliere($post){
		$orga = Element::getElementSimpleById($post["contextId"],"organizations",null,["name", "description","shortDescription","slug","profilMediumImageUrl","email","socialNetwork","profilBannerUrl","filiere","cocity"]);
		return ($orga["filiere"]);
	}
	public static function getOrgaFiliere ($post){
		$orgaFiliere = PHDB::find(Organization::COLLECTION,array("slug"=>$post["slug"]));
		return($orgaFiliere);
	}
	
}

?>