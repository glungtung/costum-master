<?php

class Deal {

    public static function getCheckCustomUser($params){

        $links=$params["userLinks"];
        $communityLinks=$params["communityLinks"]["links"];

        if( isset($links["links"][Link::$linksTypes[Person::COLLECTION][ Organization::COLLECTION]] ) && isset($communityLinks[Link::$linksTypes[$params["el"]["type"]][Person::COLLECTION]]))
        { 
                //var_dump($links["links"]['memberOf']);
                foreach($links["links"][Link::$linksTypes[Person::COLLECTION][ Organization::COLLECTION]] as $k =>$v){
                    if(!empty($v["isAdmin"]) && !isset($v["isAdminPending"]) && isset($communityLinks[Link::$linksTypes[$params["el"]["type"]][Person::COLLECTION]][$k]["roles"])){
                        if(in_array("Opérateur", $communityLinks[Link::$linksTypes[$params["el"]["type"]][Person::COLLECTION]][$k]["roles"])){     
                            $elt=Element::getElementById($k, $v["type"], null, array("name","slug", "profilThumbImageUrl"));
                            $operator=$elt;
                            $params["costumUserArray"]["hasRoles"] = ["Opérateur"];

                        }
                        if(in_array("Financeur", $communityLinks[Link::$linksTypes[$params["el"]["type"]][Person::COLLECTION]][$k]["roles"])){     
                            $elt=Element::getElementById($k, $v["type"], null, array("name","slug", "profilThumbImageUrl"));
                            $financor=$elt;
                            $params["costumUserArray"]["hasRoles"] = ["Financeur"];
                        }
                    }
                }
            
        }
        if(isset($operator))
            $params["costumUserArray"]["operatorOf"]=$operator;
        if(isset($financor))
            $params["costumUserArray"]["financorOf"]=$financor;
        //var_dump($params["costumUserArray"]);
        return $params["costumUserArray"];
    }
    public static function canAccessAnswer($ans){
        return Form::canAccess(array("roles"=>["Opérateur", "Financeur"]));
    }
    public static function canEditAnswer($ans){
        $params=@$_POST;
        $costum = CacheHelper::getCostum();
        if(!empty($params)){
            if(isset($params["path"]) && isset($params["value"])){
                if(strrpos($params["path"], "links.operators") !== false && $params["value"]==0 && isset(Yii::app()->session["costum"][$costum["slug"]]["hasRoles"]) && in_array("Opérateur", Yii::app()->session["costum"][$costum["slug"]]["hasRoles"])){
                    return true;
                }
            }
            if(self::isFinancor())
                return true;
            if(self::isOperatorOfAnswer($ans, $costum))
                return true;
        }
    }
    public static function canAdminAnswer($ans){
        $params=@$_POST;
        $costum = CacheHelper::getCostum();
        // if(!empty($params)){
            if(isset($params["path"]) && isset($params["value"])){
                if(strrpos($params["path"], "links.operators") !== false && $params["value"]==0 && isset(Yii::app()->session["costum"][$costum["slug"]]["hasRoles"]) && in_array("Opérateur", Yii::app()->session["costum"][$costum["slug"]]["hasRoles"])){
                    return true;
                }
            }
            if(self::isFinancor())
                return true;
            if(self::isOperatorOfAnswer($ans, $costum))
                return true;
        // }
    }
    public static function isFinancor(){
        $costum = CacheHelper::getCostum();
        if(isset(Yii::app()->session["costum"][$costum["slug"]]["hasRoles"]) && in_array("Financeur", Yii::app()->session["costum"][$costum["slug"]]["hasRoles"]))
            return true;
        return false;
    }
    public static function isOperatorOfAnswer($ans, $costum){
        $res=false;
        // if(isset($ans["answer"]["links"]) 
        //     && isset($ans["answer"]["links"]["operators"]) 
        //     && isset(Yii::app()->session["costum"][$costum["slug"]]["operatorOf"])
        //     && isset(Yii::app()->session["costum"][$costum["slug"]]["operatorOf"]["_id"])
        //     && isset($ans["answer"]["links"]["operators"][(string)Yii::app()->session["costum"][$costum["slug"]]["operatorOf"]["_id"]])
        //     && $ans["answer"]["links"]["operators"][(string)Yii::app()->session["costum"][$costum["slug"]]["operatorOf"]["_id"]]==1)
        $costum = CacheHelper::getCostum();
        if(isset(Yii::app()->session["costum"][$costum["slug"]]["hasRoles"]) 
            && in_array("Opérateur", Yii::app()->session["costum"][$costum["slug"]]["hasRoles"]) 
            && isset($ans["answer"]["links"]["operators"]) 
            // && isset(Yii::app()->session["costum"][$costum["slug"]]["operatorOf"]["_id"])
            // && isset($ans["answer"]["links"]["operators"][(string)Yii::app()->session["costum"][$costum["slug"]]["operatorOf"]["_id"]])
            // && $ans["answer"]["links"]["operators"][(string)Yii::app()->session["costum"][$costum["slug"]]["operatorOf"]["_id"]]==1
        ) 
            $res=true;
        return $res;
    }
    public static function generateAnswerBeforeSave($params){
        $costum = CacheHelper::getCostum();
        if(isset(Yii::app()->session["costum"][$costum["slug"]]["operatorOf"])){
            $params["answer"]["links"]["operators"]=array((string)Yii::app()->session["costum"][$costum["slug"]]["operatorOf"]["_id"]=>1);
            $params["answer"]["step"]="deal3";
        }
        return $params;
    }
    public static function AnswerMailProcess($params){
        $answer=$params["answer"];
        if(isset($answer) && isset($answer["links"]["operators"]) && $params["tpl"]=="validation"){
            $nameAnsw=(isset($answer["mappingValues"]["name"])) ? $answer["mappingValues"]["name"] : "";
            foreach($answer["links"]["operators"] as $k => $state){
                $community=Element::getCommunityByTypeAndId(Organization::COLLECTION, $k,Person::COLLECTION, "isAdmin");
                if(is_array($state)){
                    foreach($community as $k => $v){
                        $people=Element::getElementById($k, Person::COLLECTION, null, array("email","name","slug") );
                        if($params["step"] == "deal1") $subject="Vous avez été choisi comme opérateur sur la réponse ".$nameAnsw;
                        else if($params["step"] == "deal12") $subject="Le dossier ".$nameAnsw." vous a été attribué en tant qu'opérateur";
                        else $subject="Une nouvelle étape a été validée sur le dossier ".$nameAnsw;
                        $params = array (
                            "type" => Cron::TYPE_MAIL,
                            "tpl"=>Answer::$mailConfig[$params["tpl"]]["tpl"],
                            "subject" => "[".Mail::getAppName()."] ".$subject,
                            "from"=>Yii::app()->params['adminEmail'],
                            "to" => $people["email"],
                            "tplParams" => array(  "user"=> $people ,
                                                    "title" => Mail::getAppName() ,
                                                    "answer" => $answer,
                                                    "form"=>$params["form"],
                                                    "msg"=>"En tant qu'opérateur de ce dossier, nous avons le plaisir de informer que le dossier a été validé"));                                        
                        $params=Mail::getCustomMail($params); 
                        Mail::schedule($params);
                    }
                }else if($state=="0" && @$params["step"]=="deal1"){
                    foreach($community as $k => $v){
                        $people=Element::getElementById($k, Person::COLLECTION, null, array("email","name","slug") );
                        $subject="Vous n'avez été pas été retenu sur le dossier ".$nameAnsw;
                        
                        $params = array (
                            "type" => Cron::TYPE_MAIL,
                            "tpl"=>"answer.rejected",
                            "subject" => "[".Mail::getAppName()."] ".$subject,
                            "from"=>Yii::app()->params['adminEmail'],
                            "to" => $people["email"],
                            "tplParams" => array(  "user"=> $people ,
                                                    "title" => Mail::getAppName() ,
                                                    "answer" => $answer,
                                                    "form"=>$params["form"],
                                                    "msg"=>"Vous avez postulé en tant qu'opérateur. Nous avons le regret de vous informer que vous n'avez pas été retenu pour prendre en charge la suite de ce dossier"));                                        
                        $params=Mail::getCustomMail($params); 
                        Mail::schedule($params);
                    }
                }
            }

        }
    }

    
    

}
?>