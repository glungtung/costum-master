<?php 
class Miks{
	const COLLECTION = "costum";
	const CONTROLLER = "costum";
    const MODULE = "costum";

    public static function getProjects($type,$slug) {
        $params["result"] = false;

        $projects = PHDB::find(Project::COLLECTION, ["source.key" => $slug] );

        if (@$projects) {
            $res = array();
            $params["result"] = false;

            $res = self::CreateResult($projects);
            return array_merge($params,$res);
        }
        return $params; 
    }

    public static function CreateResult($data){
        $res["projects"] = array();

        $img = (@$value["profilImageUrl"] ? $value["profilImageUrl"] : "none");
        
        foreach ($data as $key => $value) {
            array_push($res["projects"], [
                    "id"    =>  (String) $value["_id"],
                    "name"  =>  $value["name"],
                    "img"   =>  $img
            ]);
        }
        return $res;
    }

}