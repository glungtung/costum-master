<?php
class GetEventCommunityAction extends CAction{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $params = hubMedNum::getEventCommunity($_POST["contextId"],$_POST["contextType"]);
        
        Rest::json($params);
    }
}