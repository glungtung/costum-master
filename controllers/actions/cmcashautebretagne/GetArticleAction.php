<?php

/**
 * 
 */
class GetArticleAction extends CAction
{
	public function run(){
		$controller = $this->getController();
		$params = CmcasHauteBretagne::getArticles($_POST["sourceKey"]);

		Rest::json($params);
	}
}

?>