<?php
/**
 * 
 */
class GetEventAction extends CAction
{
	
	public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $params = CmcasHauteBretagne::getEvents($_POST["sourceKey"]);
        
        Rest::json($params);
    }
}
?>