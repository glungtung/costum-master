<?php
class OcecodashboardAction extends CAction
{
    public function run($context=null)
    {
    	$controller = $this->getController();    	


    	$tpl = "costum.views.custom.cordCollaborativeOpenRd.globaldashboard";

        $formsParent = PHDB::find( Form::COLLECTION,  array( "id" => "opalProcess" ) );

        $formsParent = array_shift($formsParent);

        $answers = PHDB::find( Form::ANSWER_COLLECTION,  array( "context" => $context );


        if (isset($answers["answers"]["opalProcess1"]["depense"])) {
            $answers = $answers["answers"]["opalProcess1"]["depense"];
        }

    	$params = [
            "allData" => $answers
        ];	
		
    	if(Yii::app()->request->isAjaxRequest)
            echo $controller->renderPartial($tpl,$params,true);              
        else {
    		$this->getController()->layout = "//layouts/empty";
    		$this->getController()->render($tpl,$params);
        }
    }
}
    	