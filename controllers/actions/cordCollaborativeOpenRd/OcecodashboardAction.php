<?php
class OcecodashboardAction extends CAction
{
    public function run($answerId=null)
    {
    	$controller = $this->getController();    	


    	$tpl = "costum.views.custom.cordCollaborativeOpenRd.dashboard";

        $formsParent = PHDB::find( Form::COLLECTION,  array( "id" => "opalProcess" ) );

        $formsParent = array_shift($formsParent);

        $answers = PHDB::findOneById(Form::ANSWER_COLLECTION, "6023cf8a7cee4e1b2c8b4568");

        if (isset($answers["answers"]["opalProcess1"]["depense"])) {
            $answers = $answers["answers"]["opalProcess1"]["depense"];
        }

    	$params = [
            "allData" => $answers
        ];	
		
    	if(Yii::app()->request->isAjaxRequest)
            echo $controller->renderPartial($tpl,$params,true);              
        else {
    		$this->getController()->layout = "//layouts/empty";
    		$this->getController()->render($tpl,$params);
        }
    }
}
    	