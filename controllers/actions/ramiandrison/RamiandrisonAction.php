<?php 
class RamiandrisonAction extends CAction
{
    public function run(){
    	header('Content-Type: application/json');
    	ini_set('max_execution_time',10000000);
		ini_set('memory_limit', '512M');

        $params = Ramiandrison::getAnswer($_POST["id"]);
      
        return Rest::json($params);
    }
}
?>