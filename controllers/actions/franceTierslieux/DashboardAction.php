<?php
class DashboardAction extends CAction
{
    public function run($slug=null,$tag=null,$action=null,$format=null)
    {
    	
    	$controller = $this->getController();
    	$params=array("page"=>"pie");
    	  
    	$tpl = "costum.views.custom.franceTierslieux.dashboard";
    
    //compteur total
        $where = array('$or' =>
                    array(
                        array("source.key" => "franceTierslieux"),
                        array("reference.costum"=> array('$in' => 
                                                            array("franceTierslieux")
                                                        )
                        )
                    )
                );

        $orga = PHDB::find(Organization::COLLECTION,$where);
        $countOrga=0;
         foreach ($orga as $id => $value){

            $countOrga++;
         }

     // Compteur compagnon
         $nbComp=0;
         foreach($orga as $id => $value){
            if(isset($value["tags"])) {
                if(in_array("Compagnon France Tiers-Lieux",$value["tags"])){
                    $nbComp++;
                }
            }       
        }

      // debut label
        $nbFabrique = 0;
        $nbFabnum = 0;
      foreach($orga as $id => $value){
            if(isset($value["tags"])) {
                if(in_array("Fabrique de Territoire",$value["tags"])){
                    $nbFabrique++;
                }
                else{
                    if(in_array("Fabrique Numérique de Territoire",$value["tags"])){
                    $nbFabnum++;
                    }    
                }
            }       
        }
        $totFab = $nbFabnum + $nbFabrique ;
        $fabPart= round($totFab / $countOrga * 100) ;
        $nbFabrique = strval($nbFabrique);
        $nbFabnum = strval($nbFabnum);
        $fabLabel = ["Fabrique de Territoire","Fabrique Numérique de Territoire"];
        $fabValue= [$nbFabrique,$nbFabnum];
        $fabColor=["#E3A297","#B6325C"];
      // fin label  


       //debut TypePlace
        $colorPlace = [
    //"#0A2F62",
    "#0B2D66",
    "#064191",
    "#2C97D0",
    "#16A9B1",
    "#0AA178",
    "#74B976",
    // "#0AA178",
    // "#16A9B1",
     "#2A99D1"
    // "#064191",
    // "#0B2E68"
    ];

       $typePlace = ["Coworking",
            "Ateliers artisanaux partagés", 
            "Fablab / Atelier de Fabrication Numérique",
            "Tiers-lieu culturel",
            "Tiers-lieu agricole",
            "Cuisine partagée / Foodlab",
            "LivingLab / Laboratoire d’innovation sociale"
        ];

        $nbByTypes = [];


            foreach ($typePlace as $type){
                $nbType=0;
                foreach($orga as $id => $value){
                    if(isset($value["tags"])) {
                        if(in_array($type,$value["tags"])){
                            $nbType++;
                        }
                    }       
                }
                $nbType=strval($nbType);
                array_push($nbByTypes, $nbType);
            }

       //Fin TypePlace 
         
        //superficie debut
        $area = [];
            foreach($orga as $id => $value){
                if(isset($value["tags"])) {
                    foreach($value["tags"] as $tags){
                        if (ctype_digit($tags)){
                            array_push($area,$tags);
                                       
                        }    
                    }   
                }

            }
            $count = count($area);
                             sort($area);
                            $mid = floor(($count-1)/2);
                            $median = ($area)?($area[$mid]+$area[$mid+1-$count%2])/2:0;
            
         
        //superficie fin 


        //SERVICE DEBUT
        $services=[
            "Accompagnement des publics", 
            "Action sociale",
            "Aiguillage / Orientation",
            "Art thérapie",
            "Bar / café",
            "Boutique / Épicerie",
            "Coopérative d’Activités et d’Emploi / Groupement d’employeur",
            "Cantine / restaurant",
            "Centre de ressources",
            "Chantier participatif",
            "Complexe évènementiel",
            "Conciergerie ",
            "Domiciliation",
            "Espace de stockage",
            "Espace détente",
            "Espace enfants",
            "Formation / Transfert de savoir-faire / Éducation",
            "Habitat",
            "Incubateur",
            "Lieu d’éducation populaire et nouvelles formes d’apprentissage ",
            "Maison de services au public / France Services",
            "Marché",
            "Média et son",
            "MediaLab",
            "Médiation numérique",
            "Pratiques culturelles ou artistiques",
            "Démarches de création partagée",
            "Action culturelle, médiation artistique",
            "Pépinière d'entreprises",
            "Point d'appui à la vie associative",
            "Point Information Jeunesse",
            "Point d’information touristique",
            "Résidences d'artistes",
            "Ressourcerie / recyclerie",
            "point collecte de déchets ou produits usagers ",
            "Service enfance-jeunesse",
            "Services liés à la mobilité"
        ];
$countService = [];
        foreach($services as $service){ 
            $count=0; 
            foreach ($orga as $id => $value){
                if(isset($value["tags"])) {
                    if (in_array($service,$value["tags"])){
                        $count++;
                    }
                
                }
            }
            array_push($countService,$count);       
        } 
        $countedServices = array_combine($services,$countService);
        asort($countedServices);
        $ranked=count($countedServices);

        $majorServices=array_slice($countedServices,-5,5);
        $majorServicesVal=array_values($majorServices);
        $majorServicesLab=array_keys($majorServices);
        $colorMajServ = ["#0AA178","#16A9B1","#2A99D1","#064191","#0B2E68"];

        $minorServices=array_slice($countedServices,0,5);
        $minorServicesVal=array_values($minorServices);
        $minorServicesLab=array_keys($minorServices);
        $colorMinServ = ["#0AA178","#AC6A21","#AA16B1","#7588B9","#88C7AC"];
        
        //var_dump("<pre>",$majorServices,"</pre>");    exit;                      

        //SERVICE fin

     // Management Model 
      
   //     $typeModel = Yii::app()->request->baseUrl["lists"]["manageModel"];
        $typeModel = [
            "Association",
            "Collectif citoyen",
            "SARL-SA-SAS",
            "SCIC-SCOP",
            "Société Publique Locale",
            "Pôle d’Equilibre Territorial Rural",
            "Département",
            "Région",
            "Groupement d'Intérêt Public",
            "Intercommunalité",
            "Université",
            "Collège",
            "Lycée"
        ];
        $orderedModel = [
                "Education" => [
                                    "Université",
                                    "Collège",
                                    "Lycée",
                                    "Ecole"
                                ],  

                "Public" => [
                  "Pôle d’Equilibre Territorial Rural",
                  "Département",
                  "Région",
                  "Société Publique Locale",
                  "Groupement d'Intérêt Public",
                  "EPIC ",
                  "Intercommunalité" 
                ],
                "Association" => [
                    "Association",
                    "Collectif citoyen",
                    "Organisation d’utilité publique"
                ],

                "Coopérative" => [
                    "SCIC-SCOP"
                ],
                "Privé" => [
                "SARL-SA-SAS"
                ]
            ];

$nbByModels = [];
// $education = 
// $public = 0;
// $addociation= 0;
// $cooperation= 0;
// $prive= 0;

    foreach ($typeModel as $model){
        $nbModel=0;
        foreach($orga as $id => $value){
            if(isset($value["tags"])) {
                if(in_array($model,$value["tags"])){
                    $nbModel++;
                }
            }       
        }
        array_push($nbByModels, $nbModel);
    }

$education = $nbByModels[10]+$nbByModels[11]+$nbByModels[12];
$education=strval($education);
$public = $nbByModels[4]+$nbByModels[5]+$nbByModels[6]+$nbByModels[7];$nbByModels[8]+$nbByModels[9];
$public=strval($public);
$association=$nbByModels[0]+$nbByModels[1];
$association=strval($association);
$cooperation=$nbByModels[3];
$cooperation=strval($cooperation);
$prive=$nbByModels[2];
$prive=strval($prive);

$valueModel=[$education,$public,$association,$cooperation,$prive];

$labelModel=["Enseignement","Public","Association","Coopérative","Privé"];

$colorsModel = [
    // "#0A2F62",
    "#0B2D66",
    //"#064191",
    "#2C97D0",
    // "#16A9B1",
     "#0AA178",
    // "#74B976",
     //"#0AA178",
    // "#16A9B1",
     "#2A99D1",
     "#064191"
    // "#0B2E68"
    ];
// fin managementModel


    //début ZONE

   $colorZone = [
    // "#0A2F62",
    "#0B2D66",
    //"#064191",
    "#2C97D0",
    // "#16A9B1",
     "#0AA178",
    // "#74B976",
     //"#0AA178",
    // "#16A9B1",
    // "#2A99D1",
     "#064191"
    // "#0B2E68"
    ];
    $typeZone = [
            "En agglomérations",
            "En métropole",
            "En milieu rural",
            "En ville moyenne (entre 20000 et 100000 habitants)"
        ] ; 
$nbByZones = [];


    foreach ($typeZone as $zone){
        $nbZone=0;
        foreach($orga as $id => $value){
            if(isset($value["tags"])) {
                if(in_array($zone,$value["tags"])){
                    $nbZone++;
                }
            }       
        }
        $nbZone=strval($nbZone);
        array_push($nbByZones, $nbZone);
    }

    // FIn ZONE


// debut region


    $colorsRegion = [
    // "#0A2F62",
    "#0B2D66",
    //"#064191",
    "#2C97D0",
    // "#16A9B1",
     "#0AA178",
     "#AC6A21",
     "#AA16B1",
     "#7588B9",
     "#88C7AC",
     "#E6BE8B",
     "#6845F4",
     "#74B976",
     "#0AA178",
     "#16A9B1",
     "#2A99D1",
     "#064191",
     "#0B2E68",
     "#5A4189",
     "#2509EE",
     //"#070C20",
     "#ED0EF4"

    ];
$typeRegion = ["Auvergne-Rhône-Alpes","Bourgogne-Franche-Comté","Bretagne","Centre-Val de Loire","Corse","Grand Est","Hauts-de-France","Normandie","Nouvelle-Aquitaine","Occitanie","Pays de la Loire","Provence-Alpes-Côte d'Azur","Île-de-France","Guyane","Guadeloupe","Martinique","Réunion","Mayotte"]; 
$countRegion=count($typeRegion);
$nbByRegions = [];


    foreach ($typeRegion as $region){
        $nbRegion=0;
        foreach($orga as $id => $value){
            if(isset($value["address"]) && isset($value["address"]["level3Name"])) {
                if($region==$value["address"]["level3Name"]){
                    $nbRegion++;
                }
            }       
        }
        $nbRegion=strval($nbRegion);
        array_push($nbByRegions, $nbRegion);
    }


// fin region
        
    

    
            // params
             
            
            $params = [
                "elements" => null,
                "title" => "Observatoire national des Tiers-lieux",
                "blocks"    => [
                    "barType" => [
                        "title"   => "Typologie d'espaces de travail",
                        "counter" => "",
                        "graph" => [
                            "url"=>"/graph/co/dash/g/costum.views.custom.franceTierslieux.graph.barCommon/size/S",
                            "key"=>"barType",
                            "data" => [
                                "datasets"=> [
                                    [
                                        "data"=> $nbByTypes,
                                        "backgroundColor"=> $colorPlace
                                    ]
                                ],
                                "labels"=> $typePlace
                            ]
                        ],
                        "width" => 6     
                    ],
                    "barMajorServices" => [
                        "title"   => "Les 5 services les plus proposés",
                        "counter" => null,
                        "graph" => [
                            "url"=>"/graph/co/dash/g/costum.views.custom.franceTierslieux.graph.barCommon/size/S",
                            "key"=>"barMajorServices",
                            "data" => [
                                "datasets"=> [
                                    [
                                        "data"=> $majorServicesVal,
                                        "backgroundColor"=> $colorMajServ
                                    ]
                                ],
                                "labels"=> $majorServicesLab
                            ]
                        ],
                        "width" => 6     
                    ],
                    "PieModel" => [
                        "title"   => "modèles de gestion",
                        "counter" => 5,
                        "graph" => [
                            "url"=>"/graph/co/dash/g/costum.views.custom.franceTierslieux.graph.pieCommon/size/S",
                            "key"=>"pieModel",
                            "data" => [
                                "datasets"=> [
                                    [
                                        "data"=> $valueModel,
                                        "backgroundColor"=> $colorsModel
                                    ]
                                ],
                                "labels"=> $labelModel
                            ]    
                        ],
                        "width" => 6
                        
                    ],
                    "pieZone" => [
                        "title"   => "typologies de territoire",
                        "counter" => "4",
                        "graph" => [
                            "url"=>"/graph/co/dash/g/costum.views.custom.franceTierslieux.graph.pieCommon/size/S",
                            "key"=>"pieZone",
                            "data" => [
                                "datasets"=> [
                                    [
                                        "data"=> $nbByZones,
                                        "backgroundColor"=> $colorZone
                                    ]
                                ],
                                "labels"=> $typeZone
                            ]        
                        ],
                        "width" => 6
                    ],  
                    // "barMinorServices" => [
                    //     "title"   => "Les 5 services les moins proposés",
                    //     "counter" => null,
                    //     "graph" => [
                    //         "url"=>"/graph/co/dash/g/costum.views.custom.franceTierslieux.graph.barCommon/size/S",
                    //         "key"=>"barMinorServices",
                    //         "data" => [
                    //             "datasets"=> [
                    //                 [
                    //                     "data"=> $minorServicesVal,
                    //                     "backgroundColor"=> $colorMinServ
                    //                 ]
                    //             ],
                    //             "labels"=> $minorServicesLab
                    //         ] 
                    //     ]    
                    // ],
                    "pieGeo" => [
                        "title"   => "Régions / DOM d'implantation",
                        "counter" => $countRegion,
                        "graph" => [
                            "url"=>"/graph/co/dash/g/costum.views.custom.franceTierslieux.graph.pieCommon/size/S",
                            "key"=>"pieGeo",
                            "data" => [
                                "datasets"=> [
                                    [
                                        "data"=> $nbByRegions,
                                        "backgroundColor"=> $colorsRegion
                                    ]
                                ],
                                "labels"=> $typeRegion
                            ]        
                        ],
                        "width" => 8    
                    ],
                                      
                    "Fabrique" => [
                        "title"   => "fabriques (numériques) de territoire",
                        "counter" => $totFab,
                        "graph" => [
                            "url"=>"/graph/co/dash/g/costum.views.custom.franceTierslieux.graph.doughnutCommon/size/S",
                            "key"=>"Fabrique",
                            "data" => [
                                "datasets"=> [
                                    [
                                        "data"=> $fabValue,
                                        "backgroundColor"=> $fabColor
                                    ]
                                ],
                                "labels"=> $fabLabel
                            ]    
                        ],
                        "width" => 4                        
                    ],
                    "Compagnon" => [
                        "title"   => "Tiers-lieux compagnons",
                        "counter" => $nbComp,
                        "html" => "<img class='img-responsive' style='filter: drop-shadow(0px 0px 7px #FF286B) invert(100%) hue-rotate(82deg);  margin:30px auto 0px auto;height:275px' src='".Yii::app()->getModule("costum")->assetsUrl."/images/franceTierslieux/compagnon.png'>"
                    ],
                    "Superficie" => [
                        "title"   => "m2 de superficie médiane",
                        "counter" => $median,
                        "html" => "<img class='img-responsive' style='height:275px;  filter: hue-rotate( 115deg);margin:30px auto 0px auto;opacity:0,75;' src='".Yii::app()->getModule("costum")->assetsUrl."/images/franceTierslieux/superficie.png'>"
                    ],
                    "Label" => [
                        "title"   => "% de fabriques (numériques)",
                        "counter" => $fabPart,
                        "html" => "<img class='img-responsive' style='height:275px;  ' src='".Yii::app()->getModule("costum")->assetsUrl."/images/franceTierslieux/fabrique.png'>"
                    ]
                ]
            ];      

//            Yii::app()->cache->set('ftlDashboard',$params, 720);
         
        
        
        
            if(Yii::app()->request->isAjaxRequest)
                echo $controller->renderPartial($tpl,$params,true);              
            else {
                $this->getController()->layout = "//layouts/empty";
                $this->getController()->render($tpl,$params);
            }
        
        
    }
}
    	
