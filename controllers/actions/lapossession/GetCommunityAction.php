<?php
class GetCommunityAction extends CAction{
    public function run(){
        $controller = $this->getController();
        $params = LaPossession::getCommunity($_POST["contextType"],$_POST["contextSlug"]);
        
        Rest::json($params);
    }
}