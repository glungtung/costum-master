<?php
/**
* Update an information field for a element
*/
class GetOrgaAction extends CAction
{
    public function run()
    {
        $controller=$this->getController();
        $res = LaPossession::getOrganization($_POST["contextType"],$_POST["contextSlug"]);
		Rest::json($res);
    }
}