<?php
class GraphAction extends CAction
{
    public function run($sk=null,$answer=null,$aj="false", $territoires="[0]", $firstkey = "", $ter = "")
    {
    	$controller = $this->getController();   
    	$tpl = "costum.views.custom.cocity.observatory";    	

    	$projects = PHDB::find(Project::COLLECTION, array("parent.".$controller->costum["contextId"] => array('$exists'=>1)),["tags"]);
		//$agricultures = PHDB::find(Organization::COLLECTION,["source.key" =>$controller->costum["contextSlug"],"tags"=>"agricultures"]);
		$organization = PHDB::find(organization::COLLECTION, array("source.key"=>$controller->costum["slug"] ),["tags"]);

        $event = PHDB::find(Event::COLLECTION, array("organizer.".$controller->costum["contextId"] => array('$exists'=>1)),["tags"]);
        $classifieds = PHDB::find(Classified::COLLECTION, array("parent.".$controller->costum["contextId"] => array('$exists'=>1)),["tags"]);
        $elementLabel = ["organization","projects","event","classifieds" ];
        $elementTrueLabel = ["Organisation","projets","Evènement","annonces" ];
        $nombreElement = [];
        for($i = 0; $i < count($elementLabel); $i++){
            $nombreElement[$i] = count(${$elementLabel[$i]});
        }
        $allTags = [];
        $tagsOrga = [];

        foreach ($organization as $key => $value) {
            $tagsOrga = array_merge($value["tags"] ?? [],$tagsOrga);
        }
        $allTags += $tagsOrga;
        $tagsProjet = [];
        foreach ($projects as $key => $value) {
            $tagsProjet = array_merge($value["tags"] ?? [],$tagsProjet);
        }
        $allTags = array_merge($tagsProjet,$allTags);
        $tagsEvent = [];
        foreach ($event as $key => $value) {
            $tagsEvent = array_merge($value["tags"] ?? [],$tagsEvent);
        }
        $allTags = array_merge($tagsEvent,$allTags);
        $listTags = [];
        $blocks = [];
        $graph = [];

        $graph += ["nombrecocity" => [
            "title"=>"EXISTANT DANS COCITY",
            "data" => $nombreElement,
            "lbls" => $elementTrueLabel,
            "url"  => "/graph/co/dash/g/graph.views.co.costum.cocity.barMany",
            "yAxesLabel" => "Nombre "
        ]];
        $activiteCocity = PHDB::find(ActivityStream::COLLECTION, array("source.key"=>$controller->costum["slug"] ),["tags"]);
        foreach ($graph as $ki => $list) {
            $kiCount = 0;
            foreach ($list["data"] as $ix => $v) {
                if(is_numeric($v))
                    $kiCount += $v;
                else 
                    $kiCount ++;
            }
            $blocks[$ki] = [
                "title"   => $list["title"],
                "counter" => $kiCount,
            ];
            if(isset($list["tpl"])){
                $blocks[$ki] = $list;
            }
            else 
                $blocks[$ki]["graph"] = [
                    "url"=>$list["url"],
                    "key"=>"pieMany".$ki,
                    "data"=> [
                        "datasets"=> [
                            [
                                "data"=> $list["data"],
                                "backgroundColor"=> Ctenat::$COLORS
                            ]
                        ],
                        "labels"=> $list["lbls"]
                    ]
                ];

            if (isset($list["yAxesLabel"])) {
                $blocks[$ki]["graph"]["data"]["yAxesLabel"] = $list["yAxesLabel"];
            }

        }
		$params = [
			"projects" => $projects,
			"organization" => $organization,
            "event"    => $event,
            "blocks" => $blocks,
            "allTags"=> $allTags
		];
    	if(Yii::app()->request->isAjaxRequest)
            echo $controller->renderPartial($tpl,$params,true);              
        else {
    		$this->getController()->layout = "//layouts/empty";
    		$this->getController()->render($tpl,$params);
        }
    	
    }
}
