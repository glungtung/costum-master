<?php
class GetCityAction extends CAction{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $params = Cocity::getCity($_POST);
        
        Rest::json($params);
    }
}