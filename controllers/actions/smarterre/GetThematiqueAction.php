<?php
class GetThematiqueAction extends CAction{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $params = Smarterre::getThematique($_POST);
        
        Rest::json($params);
    }
}