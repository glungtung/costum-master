<?php
class GetArticlesCommunityAction extends CAction{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $params = CostumGenerique::getArticlesCommunity($_POST);
        Rest::json($params);
    }
}