<?php
class GetEventSlideAction extends CAction{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $params = CostumGenerique::getEventSlide($_POST);
        Rest::json($params);
    }
}