<?php
class GetFileAction extends CAction{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $params = FicheProjet::getFile($_POST);
        
        Rest::json($params);
    }
}