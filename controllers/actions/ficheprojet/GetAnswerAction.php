<?php
class GetAnswerAction extends CAction{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $params = FicheProjet::getAnswer($_POST);
        
        Rest::json($params);
    }
}