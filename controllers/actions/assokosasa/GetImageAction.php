<?php
class GetImageAction extends CAction{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $params = AssoKosasa::getImage($_POST);
        
        Rest::json($params);
    }
}