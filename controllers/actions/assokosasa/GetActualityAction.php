<?php
class GetActualityAction extends CAction{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $params = AssoKosasa::getActuality($_POST);
        
        Rest::json($params);
    }
}