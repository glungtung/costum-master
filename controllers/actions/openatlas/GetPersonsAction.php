<?php

class GetPersonsAction extends CAction {
    
    public function run() {
    	$controller = $this->getController();
        $params = OpenAtlas::getPersons($_POST["collection"],$_POST["id"]);
        
        Rest::json($params);
    }
	   
}