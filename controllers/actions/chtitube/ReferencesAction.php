<?php 
 /**
  * Display the directory of back office
  * @param String $id Not mandatory : if specify, look for the person with this Id. 
  * Else will get the id of the person logged
  * @return type
  */
class ReferencesAction extends CAction
{
	public function run(){
		$controller = $this->getController();
		$params = array() ;
		$page = "references";
		$getType=[Organization::COLLECTION, Project::COLLECTION, Event::COLLECTION, Poi::COLLECTION, Proposal::COLLECTION];
		$allResult=array();
		$count=array("total"=> 0, "toBeValidated"=>0);
		foreach($getType as $type){
			$allByType=PHDB::findAndSort($type, array("creator"=> Yii::app()->session["userId"], "source.key"=>"chtitube"), array("updated" => -1));
			$count[$type]=0;
			if(!empty($allByType)){
				foreach($allByType as $key => $value){
					$count["total"]++;
					$count[$type]++;
					if(isset($value["preferences"]) && isset($value["preferences"]["private"]) && $value["preferences"]["private"])
						$count["toBeValidated"]++;
					if(is_object(@$value["updated"]))
			 			$allByType[$key]["sorting"] = @$value["updated"]->sec;
			 		else
			 			$allByType[$key]["sorting"] = @$value["updated"];
					if($type==Organization::COLLECTION){
						$allByType[$key]["typeOrga"] = $value["type"];
						$allByType[$key]["type"] = "organizations";
						$allByType[$key]["typeSig"] = Organization::COLLECTION;
					}
					if($type == Project::COLLECTION){
						$allByType[$key]["type"] = Project::COLLECTION;
						$allByType[$key]["typeSig"] = Project::COLLECTION;
					}
					if($type==Event::COLLECTION){
						$allByType[$key]["typeEvent"] = @$allByType[$key]["type"];
						$allByType[$key]["type"] = "events";
						$allByType[$key]["typeSig"] = Event::COLLECTION;
						if(@$value["links"]["attendees"][Yii::app()->session["userId"]]){
				  			$allByType[$key]["isFollowed"] = true;
							}
						if(@$allByType[$key]["startDate"] && @$allByType[$key]["startDate"]->sec){
							$allByType[$key]["startDateTime"] = date(DateTime::ISO8601, $allByType[$key]["startDate"]->sec);
							$allByType[$key]["startDate"] = date(DateTime::ISO8601, $allByType[$key]["startDate"]->sec);
						}
						if(@$allByType[$key]["endDate"] && @$allByType[$key]["endDate"]->sec){
							$allByType[$key]["endDateTime"] = date(DateTime::ISO8601, $allByType[$key]["endDate"]->sec);
							$allByType[$key]["endDate"] = date(DateTime::ISO8601, $allByType[$key]["endDate"]->sec);
						}
						if(!empty($value["organizer"])){
							foreach($value["organizer"] as $k => $v){ 
								$elt=Element::getElementSimpleById($k, $v["type"],null, array("slug", "profilThumbImageUrl", "name"));
								if(!empty($elt)){
									$allByType[$key]["organizer"][$k]["name"] = $elt["name"];
									$allByType[$key]["organizer"][$k]["profilThumbImageUrl"] = ( !empty($elt["profilThumbImageUrl"]) ? $elt["profilThumbImageUrl"] : "" ) ;
									$allByType[$key]["organizer"][$k]["slug"] = $elt["slug"];
								}
							}
						}
					}
					if($type==Poi::COLLECTION){
						if(@$value["parentId"] && @$value["parentType"])
				  			$parent = Element::getElementSimpleById(@$value["parentId"], @$value["parentType"]);
				  		else if(!empty($value["parent"]))
				  			$parent=$value["parent"];
				  		else
				  			$parent=array();
						$allByType[$key]["parent"] = $parent;
						if(@$value["type"])
							$allByType[$key]["typeSig"] = Poi::COLLECTION;//.".".$value["type"];
						else
							$allByType[$key]["typeSig"] = Poi::COLLECTION;
						$allByType[$key]["typePoi"] = @$allByType[$key]["type"];
						$allByType[$key]["type"] = Poi::COLLECTION;
					}
					if($type==Proposal::COLLECTION){
						$allByType[$key]["type"] = $type;
						$allByType[$key]["voteRes"] = Proposal::getAllVoteRes($allByType[$key]);
						$allByType[$key]["hasVote"] = @$allByType[$key]["votes"] ? Cooperation::userHasVoted(
																	@Yii::app()->session['userId'], $allByType[$key]["votes"]) : false; 
						if(!empty($allByType[$key]["producer"])){
							$arrayKey = array();
							foreach($allByType[$key]["producer"] as $k => $v){
				                $elt=Element::getElementById( $k, $v["type"], null, array("name", "slug","profilThumbImageUrl"));
				                if(!empty($elt))
				                	$allByType[$key]["producer"][$k]=array_merge($allByType[$key]["producer"][$k], $elt);
				            }
						}
					}
				}
			}
			$allResult = array_merge($allResult,$allByType);
		}
		usort($allResult, "Chtitube::mySortByUpdated");
		$params["results"]=$allResult;
		$params["count"]=$count;
		if(Yii::app()->request->isAjaxRequest)
			echo $controller->renderPartial("/custom/chtitube/".$page, $params, true);
		
		
	}
}
