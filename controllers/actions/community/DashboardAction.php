<?php
//http://127.0.0.1/ph/costum/co/dashboard/sk/laCompagnieDesTierslieux
class DashboardAction extends CAction
{
    public function run($sk=null,$answer=null,$activegraph=null)
    {
    	$controller = $this->getController();    	

    	$tpl = "costum.views.custom.community.dashboard";
    	
    	$answers = PHDB::find( Form::ANSWER_COLLECTION,[ //"formId"     => $params["formId"],
                                              			 "parentSlug" => Yii::app()->session['costum']["contextSlug"] ] );
    	
    	
	
		$params = [
			"allAnswers" => $answers

    	];	
    	if(Yii::app()->request->isAjaxRequest)
            echo $controller->renderPartial($tpl,$params,true);              
        else {
    		$this->getController()->layout = "//layouts/empty";
    		$this->getController()->render($tpl,$params);
        }
    	
    }
}