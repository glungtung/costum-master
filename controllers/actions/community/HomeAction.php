<?php
class HomeAction extends CAction
{
    public function run($tpl=null)
    {
    	$controller = $this->getController();    	


    	
        $params = [];
    	if(Yii::app()->request->isAjaxRequest)
            echo $controller->renderPartial($tpl,$params,true);              
        else {
    		$this->getController()->layout = "//layouts/empty";
    		$this->getController()->render($tpl,$params);
        }
    	
    }
}