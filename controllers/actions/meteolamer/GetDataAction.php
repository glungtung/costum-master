<?php
class GetDataAction extends CAction{
    public function run($start_date, $end_date=NULL, $spot){
        if($end_date !== NULL){
            $where = [
                "date"=> [
                    '$gte'=> strtotime($start_date),
                    '$lte'=> strtotime($end_date)
                ]
            ];
        }else{
            $where = [
                "date" => strtotime($start_date)
            ];
        }
        $where["spot"] = $spot;
        $result = PHDB::find('--meteolamer-forecast', $where);
        Rest::json($result);
    }
}