<?php
class GetSpotsAction extends CAction{
    public function run(){
        $spots['spot'] = PHDB::findAndSort('--meteolamer-spots',  ["type" => "spot"], ["label"=>1]);
        $spots['region'] = PHDB::findAndSort('--meteolamer-spots',  ["type" => "region"], ["label"=>1]);
        Rest::json($spots);
    }
}