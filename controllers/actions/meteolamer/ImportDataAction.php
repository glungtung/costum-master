<?php
/**
 * ImportDataMeteolamerAction.php
 *
 * this action allows to import the data coming 
 * from the meteolamer FTP server into the CODB:--meteolamer
 * 
 * @author: Yorre Rajaonarivelo
 * Date: 20/20/2020
 */

class ImportDataAction extends CAction{
    private $collection = "--meteolamer-forecast";
    private $baseUrl = "http://www.meteolamer.re/dev/data/tables/";
    private $times = ["00", "06", "12", "18"];
    private $weather_sign_names = ["CLEAR", "PARTLY_CLOUDY", "CLOUDY", "SHOWERS", "HEAVY_RAIN"];

    public function run($start_date, $end_date=NULL){
        $this->importDataBetweenDates($start_date, $end_date);
        Rest::json(["status"=>1]);
    }

    /**
     * this function fetch the remote data from FTP server of Meteolamer
     * and filter the received data
     */
    private function fetchData($type, $date, $time){
        $filename = $this->baseUrl.$type."_".date("Ymd", strtotime($date))."_$time"."0000.js";

        /**
         * ensure that the allow_url_fopen module is enabled to use file_get_contents
        */
        $file_content = @file_get_contents($filename);
        if($file_content){
            //retrieve the json string inside the js file
            preg_match("#'{(.*?)}'#", $file_content, $matches);
            //get the first string matches
            $data = $matches[0];
            //remove "'" in the string to have a clean json
            $data = str_replace("'", "", $data);
            //solve the encoding problem and decode the json to array
            $data = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $data), true ); 
            
            return $data['spot'];
        }
        return NULL;
    }

    private function mergeData($date, $time, $data_to_merge){
        $all_data = [];
        $region_data = $data_to_merge["region_data"];
        $local_data = $data_to_merge["local_data"];
        $wind_data = $data_to_merge["wind_data"];

        foreach(array_merge($region_data, $local_data) as $data){
            $spot_name = $data['Name'];
            if(!array_key_exists($spot_name, $all_data)){
                $all_data[$spot_name] = [
                    "wave" => NULL,
                    "tide" => NULL,
                    "wind" => NULL
                ];
            }

            $all_data[$spot_name]["wave"] = [
                "map" => $data['Map'],
                "imap" => $data['Imap'],
                "height" => $data['H'],
                "swellHeight" => $data['Hswell'],
                "period" => $data['P'],
                "direction" => $data['D'],
                "windSpeed" => $data['Ws'],
                "windDirection" => $data['Wd']
            ];

            $tide_histr = explode(" ", $data['HiStr']);
            $tide_lostr = explode(" ", $data['LoStr']);
            $all_data[$spot_name]['tide'] = [
                "map" => in_array($spot_name, ["Reunion", "Home"]) ? 
                         "./data/maree/reunion_maree_".date("Ymd", strtotime($date))."_$time"."0000.png":NULL,
                "high" => [
                    [
                        "time" => isset($tide_histr[0])?str_replace(".", ":", $tide_histr[0]):"",
                        "level" => $data['HiLev']
                    ],
                    [
                        "time" => isset($tide_histr[1])?str_replace(".", ":", $tide_histr[1]): "",
                        "level" => $data['HiLev']
                    ]
                ],
                "low" => [
                    [
                        "time" => isset($tide_lostr[0])?str_replace(".", ":", $tide_lostr[0]):"",
                        "level" => $data['LoLev']
                    ],
                    [
                        "time" => isset($tide_lostr[1])?str_replace(".", ":", $tide_lostr[1]):"",
                        "level" => $data['LoLev']
                    ]
                ]
            ];
        }

        foreach($wind_data as $data){
            $spot_name = $data['Name'];
            if(array_key_exists($spot_name, $all_data)){
                $all_data[$spot_name]["wind"] = [
                    "map" => $data["Map"],
                    "imap" => $data['Imap'],
                    "speed" => $data['Ws'],
                    "direction" => $data['Wd'],
                    "atmosphericPressure" => $data['MSLP'],
                    "temperature" => $data['TEMP'],
                    "cloud" => $this->weather_sign_names[intval($data['CLOUD'])]
                ];
            }
        }

        return $all_data;
    }

    private function fetchAndMergeData($date){
        $output_data = [];
        
        foreach($this->times as $time){
            $data_to_merge = [
                "region_data" => $this->fetchData("region", $date, $time),
                "local_data" => $this->fetchData("spot", $date, $time),
                "wind_data" => $this->fetchData("vent", $date, $time)
            ];

            $data_merged = $this->mergeData($date, $time, $data_to_merge);
            foreach($data_merged as $spot_name => $data){
                if(!array_key_exists($spot_name, $output_data)){
                    $output_data[$spot_name] = [
                        "date" => strtotime($date),
                        "spot" => strtolower($spot_name),
                        "data" => []
                    ];
                }
                $output_data[$spot_name]["data"]["h$time"] = $data;
            } 
        }

        return $output_data;
    }

    //import data in DB
    private function importDataBetweenDates($start_date, $end_date=NULL){
        $end_date = ($end_date === NULL) ? $start_date:$end_date;
        $current_date = $start_date;
        while(strtotime($current_date) <= strtotime($end_date)){
            $all_data_to_save = $this->fetchAndMergeData($current_date);
            foreach($all_data_to_save as $data_to_save){
                Yii::app()->mongodb->selectCollection($this->collection)->insert($data_to_save);
            }
            $current_date = date("Y-m-d", strtotime($current_date." +1 day"));
        }
    }
}