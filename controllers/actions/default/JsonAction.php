<?php
class JsonAction extends CTKAction
{
    public function run($id=null,$file=null,$path=null)
    {
		$controller = $this->getController();
    	if($id)
    		$c = PHDB::findOne( Costum::COLLECTION , ["slug"=> $id]);
    	else if($file){
    		$docsJSON = file_get_contents("../../modules/costum/data/".$file.".json", FILE_USE_INCLUDE_PATH);
            $c = json_decode($docsJSON,true);
    	}
    	else 
    		$c = @$controller->costum;

    	//bug with # like in app
    	if($path){
    		$pathT = explode(".", $path);
    		foreach ($pathT as $ix => $p) {
    			if(isset($c[$p]))
    				$c = $c[$p];
    			else 
    				$c = ["BAD PATH SUPPLIED"];
    		}
    		echo Rest::json( $c );
    	}
    	else 
    		echo Rest::json( $c );

    }
}