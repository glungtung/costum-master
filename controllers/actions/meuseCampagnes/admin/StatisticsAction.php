<?php

class StatisticsAction extends CAction
{
    public function run()
    {
        $controller = $this->getController();
        if (isset($_POST['startDate']) && isset($_POST['endDate']))
        {
            $start = strtotime(str_replace("/", "-", $_POST['startDate']));
            $end = strtotime(str_replace("/", "-", $_POST['endDate']));

            $startnews = str_replace("/", "-", $_POST['startDate']);
            $endnews = str_replace("/", "-", $_POST['endDate']);

            $newStart = new MongoDate(strtotime($startnews));
            $newEnd = new MongoDate(strtotime($endnews));
            $res=array();

            $where =  array( "source.key" => "meuseCampagnes",
                "created" => array('$gt' => $start, '$lte' => $end));
            $wherenews =  array( "source.key" => "meuseCampagnes",
                "created" => array('$gt' => $newStart, '$lte' => $newEnd));
            $wherecom =  array("commentCount");
            $wherepoi =  array("voteCount");

            
            $nbreaction=array();
            
            $newslist = PHDB::find(News::COLLECTION, $wherenews);
            foreach ($newslist as $key => $reaction) {
                if (!empty($reaction["voteCount"])) {
                    $nbreaction = $nbreaction + $reaction["voteCount"];
                    //var_dump($reaction["voteCount"]);exit();
                }
            }

            $valiny=0;
            foreach ( $nbreaction as $key => $value )
                    {
                    $valiny=$valiny + $value;
                    }



            
            /*
            $nbcomm=0;
            $commslist = PHDB::find(News::COLLECTION, $wherenews);
            foreach ($commslist as $key => $comments) {
                if (!empty($comments["commentCount"])) {
                    $nbcomm = $nbcomm + $comments["commentCount"];

                }
            }
            */
            
            $res[Organization::COLLECTION] = PHDB::count(Organization::COLLECTION, $where );
            $res[Project::COLLECTION ] = PHDB::count(Project::COLLECTION, $where);
            $res[Event::COLLECTION] = PHDB::count(Event::COLLECTION, $where);
            $res[Classified::COLLECTION] = PHDB::count(Classified::COLLECTION, $where);
            $res[News::COLLECTION] = PHDB::count(News::COLLECTION, $wherenews);
            //$res[Comment::COLLECTION] = $nbcomm;
            $res["reactions"] = $valiny;
            $res[Comment::COLLECTION] = PHDB::count(Comment::COLLECTION, $where);
            $res[Poi::COLLECTION] = PHDB::count(Poi::COLLECTION, $where);
            //$res[Poi::COLLECTION] = PHDB::find(News::COLLECTION, $wherenews, array("voteCount"));

            return Rest::json(@$res);

        }
        else
        {
            echo $controller->renderPartial("costum.views.custom.meuseCampagnes.stattrim",array(),true);
        }
    }
}
