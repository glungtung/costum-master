<?php
class GetCommunityAction extends CAction{
    public function run(){
        $controller = $this->getController();
        $params = CoEvent::getCommunity($_POST);

        Rest::json($params);
    }
}