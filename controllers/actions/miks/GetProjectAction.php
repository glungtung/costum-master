<?php
class GetProjectAction extends CAction
{
	public function run( $tpl=null, $view=null ){
		$controller = $this->getController();
		$results = Miks::getProjects($_POST["contextType"],$_POST["contextSlug"]);

		Rest::json($results);
	}
}
