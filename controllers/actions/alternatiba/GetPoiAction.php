<?php
	class GetPoiAction extends CAction
	{
		public function run() {
			$controller = $this->getController();
			$params = Alternatiba::getCommunity($_POST);

			Rest::json($params);
		}
	}
