<?php
class GetEventCommunityAction extends CAction{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $params = Alternatiba::getCommunity($_POST);
        
        Rest::json($params);
    }
}