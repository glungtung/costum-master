<?php
class SearchSignaturesAction extends CAction
{
	public function run(){ 	
		
		$controller = $this->getController();
		
		$res = array(
			"result" => false,
			"msg"  => ""
		);

		if(!empty($_POST["scope"])){
			$name = "";
			foreach ($_POST["scope"] as $key => $value) {
				$checkScope=$key;
				$postalCode=$value["postalCode"];
				$city = City::getById($value["city"], array("postalCodes", "geo", "geoPosition"));
				if(count($city["postalCodes"]) > 1){
					$postalCode=$city["postalCodes"][0]["postalCode"];
					$name = (isset($city["postalCodes"][0]["name"]) && !empty($city["postalCodes"][0]["name"])) ? ucfirst(strtolower($city["postalCodes"][0]["name"])) : $name;
					$checkScope=(string)$city["_id"]."cities".$postalCode;
				}
			}
			$where = array( "source.key" => $controller->costum["slug"],
							"type"=>"contract",
							"scope.".$checkScope => array('$exists' => true )  );
			$res["contracts"]= PHDB::find(Poi::COLLECTION, $where);
			if(!empty($res["contracts"])){
				foreach($res["contracts"] as $k => $v){
					if(isset($v["links"]) && isset($v["links"]["measures"])){
						foreach($v["links"]["measures"] as $e => $o ){
							if(strlen($e) == 24 && ctype_xdigit($e)  ){
		                    	$measure = PHDB::findOne( Poi::COLLECTION , array("_id"=> new MongoId($e)), array("name"));
		                    	if(!empty($measure)){
		                    		$res["contracts"][$k]["links"]["measures"][$e]["name"]=$measure["name"];	
		                    	}
		                    }//else
		                    //	$res["contracts"][$k]["links"]["measures"][$e]["localMeasure"]=true;
		                }
					}
				}
			}
			if(!empty($city) && !empty($city["postalCodes"][0]["postalCode"]) ){
				$name = trim($name);
				$where = array( "source.key" => "siteDuPactePourLaTransition",
							"email" => "pacte-".mb_strtolower($city["postalCodes"][0]["postalCode"])."@listes.transition-citoyenne.org",
							"source.toBeValidated" => array('$exists' => false )  );
				$exist = PHDB::findOne(Organization::COLLECTION, $where);
				if(!empty($exist)){
					$res["exist"] = true;
					$res["elt"] = $exist;
				}else{
					$res["exist"] = false;
				}
			}
			
		}
		Rest::json($res) ;
	}
}