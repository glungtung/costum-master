<?php
class ValidateGroupAction extends CAction
{
    public function run($id=null, $type=null)
    {
    	$query=array( "source.toBeValidated" => 1);
    	$collectif=PHDB::findOne($_POST["type"], 
                                    array("_id" => new MongoId($_POST["id"])));
        if($_POST["type"]==Organization::COLLECTION){
        	if(isset($collectif["scope"]) && (!isset($collectif["category"]) || $collectif["category"]!= "soutien")){
        		foreach($collectif["scope"] as $k => $v){
        			SiteDuPactePourLaTransition::removeCollectifByScope($k);
        		}
        	}
            if(isset($collectif["links"]["contracts"])){
                foreach($collectif["links"]["contracts"] as $k => $v){
                    PHDB::update (Poi::COLLECTION, 
                                    array("_id" => new MongoId($k)), 
                                    array('$unset'=>$query));
                }
            }
        }
    	$res= PHDB::update ($_POST["type"], 
                                    array("_id" => new MongoId($_POST["id"])), 
                                    array('$unset'=>$query));
    	Rest::json(array("result"=>true));
                   
    }
}