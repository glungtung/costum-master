<?php 
class SaveCollectifAction extends CAction
{
    public function run(){
    	header('Content-Type: application/json');
    	ini_set('max_execution_time',10000000);
		ini_set('memory_limit', '512M');
		//Rest::json($_POST); exit;
        $params = SiteDuPactePourLaTransition::saveCollectif($_POST);
        //$params = Import::setWikiDataID($_POST);
        return Rest::json($params);
    }
}
?>