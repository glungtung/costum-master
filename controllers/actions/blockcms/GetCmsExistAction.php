<?php 
class GetCmsExistAction extends CAction{
	public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
		$controller = $this->getController();
		$params = Cms::getCmsExist($_POST);

		Rest::json($params);
	}
}
?>