<?php
class GetEventsAction extends CAction{
    public function run(){
        $controller = $this->getController();
        $where = [
        	"source.key" => $_POST["sourceKey"]
        ];

        if(isset($_POST["type"]) && !is_null($_POST["type"]))
        	$where["type"] = array('$in'=>$_POST["type"]);

        $result = PHDB::findAndSortAndLimitAndIndex(Event::COLLECTION,$where);
        
        Rest::json($result);
    }
}