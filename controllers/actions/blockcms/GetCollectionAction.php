<?php
class GetCollectionAction extends CAction{
    public function run(){
        $controller = $this->getController();
        $where = [
        	"source.key" => $_POST["sourceKey"],
        	"roles.tobeactivated" => array('$exists' => false )
        ];
        $result = PHDB::find(Person::COLLECTION,$where);
        
        Rest::json($result);
    }
}