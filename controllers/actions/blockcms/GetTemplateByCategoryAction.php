<?php
class GetTemplateByCategoryAction extends CAction{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $params = Cms::getTemplateByCategory($_POST);
  
        Rest::json($params);
    }
}