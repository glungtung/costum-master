<?php
class GetNameCreatorAction extends CAction{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $params = Cms::getNameCreator($_POST);
  
        Rest::json($params);
    }
}