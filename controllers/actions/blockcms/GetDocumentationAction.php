<?php
class GetDocumentationAction extends CAction{
    public function run(){
        $controller = $this->getController();
        $result = PHDB::find(Cms::COLLECTION,array("type" => "documentation"));
        
        Rest::json($result);
        exit();
    }
}