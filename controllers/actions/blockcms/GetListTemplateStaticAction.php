<?php
class GetListTemplateStaticAction extends CAction{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $params = Cms::getlistTemplateStatic($_POST);
  
        Rest::json($params);
    }
}