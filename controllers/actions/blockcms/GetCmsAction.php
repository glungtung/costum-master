<?php
class GetCmsAction extends CAction{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        if ($_POST["action"] == "duplicate") {        	
        	$array_result = [];
        	foreach ($_POST["ide"] as $key => $value) {
        		$params = Element::getElementById($value,Cms::COLLECTION);
        		$idParent = [];
        		unset($params["_id"]);
        		$idParent[$_POST["parentId"]] =  ["type" => $_POST["parentType"], "name" => $_POST["parentSlug"]];
        		$params["page"] = $_POST["page"];
        		$params["parent"] = $idParent;
        		$params["tplParent"] = $_POST["tplParent"];
        		$array_result[] = $params;
        	}
        	PHDB::batchInsert(Cms::COLLECTION,$array_result);
        }elseif($_POST["action"] == "removestat"){
          foreach ($_POST["ide"] as $key => $value) {
            $params = Element::getElementById($value,Cms::COLLECTION);
            $params["haveTpl"] = true;
            $params["tplParent"] = $_POST["tplId"];
            PHDB::update(Cms::COLLECTION,[ "_id" => new MongoId($value) ],['$set'=>$params]);
          }
        }elseif($_POST["action"] == "tplEmpty"){
            PHDB::update(Cms::COLLECTION,[ "_id" => new MongoId($_POST["tplId"]) ],[ '$unset' => ["cmsList" =>true ] ]);          
        }elseif($_POST["action"] == "upCmsList"){
            PHDB::update(Cms::COLLECTION,[ "_id" => new MongoId($_POST["tplId"]) ],['$set'=> ["cmsList" => $_POST["ide"]]]);          
        }elseif($_POST["action"] == "delete"){
            foreach ($_POST["ide"] as $key => $value) {
            $res = Element::deleteSimple($value,"cms", Yii::app()->session["userId"]); 
         }   
         Rest::json($res);    
        }
    }
}
