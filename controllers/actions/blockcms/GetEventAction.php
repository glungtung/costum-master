<?php
class GetEventAction extends CAction{
    public function run(){
        $controller = $this->getController();
        $where = [
        	"source.key" => $_POST["sourceKey"]
        ];
        $result = PHDB::find($_POST["collection"],$where);
        
        Rest::json($result);
    }
}