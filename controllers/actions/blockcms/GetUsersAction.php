<?php
class GetUsersAction extends CAction{
    public function run(){
        $controller = $this->getController();
        $where = [
        	"source.key" => $_POST["sourceKey"],
        ];
        $result = PHDB::findAndSortAndLimitAndIndex(Person::COLLECTION,$where);
        
        Rest::json($result);
    }
}