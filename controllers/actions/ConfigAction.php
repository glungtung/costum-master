<?php
class ConfigAction extends CAction
{
    
    public function run($id=null,$type=null,$slug=null, $view=null,$page=null,$test=null)
    { 	

        $controller=$this->getController();
        $controller->layout = "//layouts/mainSearch";
        
        if(empty(CacheHelper::get("appConfig"))){
          CacheHelper::set("appConfig", CO2::getThemeParams());
        }
        $this->getController()->appConfig = CacheHelper::get("appConfig");

        if($slug || $id){
            $slugCache = !empty($slug) ? $slug : $id;
        }else {
            $slugCache = !empty(@$_GET["host"]) ? $_GET["host"] : null;
        }
        if(!$this->getController()->cacheCostumInit($slugCache)){
        $returnCostum = Costum::init ($this->getController(),$id,$type,$slug,$view,$test,"costum/controllers/actions/IndexAction.php");
        $this->getController()->cacheCostumInit($slugCache,$returnCostum);
        }

        if(Yii::app()->request->isAjaxRequest)
            echo $controller->renderPartial("costum.views.co.config", ["slug"=>$slug, "el"=> Slug::getElementBySlug($slug)], true);
        else
	  		$controller->render("costum.views.co.config",["slug"=>$slug, "el"=> Slug::getElementBySlug($slug)]);
		

    }
}


