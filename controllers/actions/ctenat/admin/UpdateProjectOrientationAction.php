<?php 
 /**
  * Display the directory of back office
  * @param String $id Not mandatory : if specify, look for the person with this Id. 
  * Else will get the id of the person logged
  * @return type
  */
class UpdateProjectOrientationAction extends CAction
{
	public function run(){
		$controller = $this->getController();
		$project = PHDB::findOneById( Project::COLLECTION , $_POST["id"], array("links"));
		
		// "Territoire Candidat",
		// 				"Territoire Candidat refusé", 
		// 				"Territoire lauréat", 
		// 				"Dossier Territoire Complet",
		// 				"CTE Signé"
		if(isset($_POST["orientations"])){
			$linksOrientations=$_POST["orientations"];
			$orientationAlreadyLinks=array();
			if(!empty($project["links"]) && isset($project["links"]["orientations"])){
				foreach($project["links"]["orientations"] as $k => $v){
					if(!isset($linksOrientations[$k])){
						PHDB::update( Badge::COLLECTION, 
				                       array("_id" => new MongoId($k)) , 
				                       array('$unset' => array("links.projects.".(string)$project["_id"]=>"")));
					}else{
						array_push($orientationAlreadyLinks, $k);
					}
				}
			}
			foreach($linksOrientations as $k => $v){
				if(empty($orientationAlreadyLinks) || !in_array($k, $orientationAlreadyLinks)){
	  				PHDB::update( Badge::COLLECTION, 
	                   array("_id" => new MongoId($k)) , 
	                   array('$set' => array("links.projects.".(string)$project["_id"]=>array("type"=>Project::COLLECTION))));
					
				}
			}
			$project["links"]["orientations"]=$linksOrientations;
			PHDB::update( Project::COLLECTION, 
	                   array("_id" => $project["_id"]) , 
	                   array('$set' => array("links"=>$project["links"])));
		}else{
			if(isset($project["links"]["orientations"])) unset($project["links"]["orientations"]);
			PHDB::update( Project::COLLECTION, 
	                   array("_id" => $project["_id"]) , 
	                   array('$unset' => array("links.orientations"=>"")));
			PHDB::update( Badge::COLLECTION, 
	                   array("category" => "strategy", "links.projects.".(string)$project["_id"] => array('$exists'=>true)) , 
	                   array('$unset' => array("links.projects.".(string)$project["_id"]=>"")));
			$linksOrientations=[];
			
		}		
		$res = array(
			"project" => $project,
			"orientations"=>$linksOrientations
		);

		Rest::json($res);
	}
}
