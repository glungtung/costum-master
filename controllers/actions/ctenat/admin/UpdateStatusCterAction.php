<?php 
 /**
  * Display the directory of back office
  * @param String $id Not mandatory : if specify, look for the person with this Id. 
  * Else will get the id of the person logged
  * @return type
  */
class UpdateStatusCterAction extends CAction
{
	public function run(){
		$controller = $this->getController();
		$project = PHDB::findOneById( $_POST["collection"] , $_POST["id"], array("name", "preferences", "source", "links.forms"));
		
		// "Territoire Candidat",
		// 				"Territoire Candidat refusé", 
		// 				"Territoire lauréat", 
		// 				"Dossier Territoire Complet",
		// 				"CTE Signé"

		//Rest::json($controller->costum); exit;

		if(empty($project["source"]))
			$project["source"] = array();

		if(empty($project["source"]["status"]))
			$project["source"]["status"] = array();

		if(empty($project["source"]["status"]["ctenat"]))
			$project["source"]["status"]["ctenat"] = array();

		$project["source"]["status"]["ctenat"] = $_POST["value"];

		$set = array("source" => $project["source"]);

		if( $_POST["value"] == Ctenat::STATUT_CTER_LAUREAT ){
			unset($project["preferences"]["private"]);
			$set["preferences"] = $project["preferences"];
			if(!empty($controller->costum["forms"])){
				//Rest::json($set); exit;
				if(empty($project["links"]))
					$project["links"] = array();

				if(empty($project["links"]["forms"]))
					$project["links"]["forms"] = array();

				foreach ($controller->costum["forms"] as $keyF => $valF) {
					if(empty($project["links"]["forms"][$keyF])){
						$project["links"]["forms"][$keyF] = array(
							"type" => Form::COLLECTION,
							"copyForm"=>"ficheAction"
						);
					}
				}
				$set["links"] = $project["links"];
			}
		} else if($_POST["value"] == Ctenat::STATUT_CTER_REFUSE || 
			$_POST["value"] == Ctenat::STATUT_CTER_CANDIDAT){
			$project["preferences"]["private"] = true;
			$set["preferences"] = $project["preferences"];
			if(!empty($controller->costum["forms"])){
				if(empty($project["links"]))
					$project["links"] = array();
				if(!empty($project["links"]["forms"])){
					foreach ($controller->costum["forms"] as $keyF => $valF) {
						if(!empty($project["links"]["forms"][$keyF])){
							unset($project["links"]["forms"][$keyF]);
						}
					}
				}

				$set["links"] = $project["links"];
			}
		}

		$resUpdate = PHDB::update( Project::COLLECTION,
				    array("_id"=>new MongoId($_POST["id"])), 
				    array('$set' => $set) ) ;

		// PHDB::update($pType, 
  //                      array("_id" => new MongoId($pId)) , 
  //                      array('$set' => array("links.".self::COLLECTION.".".(string)$copyEl["_id"] => array("type"=>self::COLLECTION, "copyForm"=>$copy))));

		$res = array(
			"resUpdate" => $resUpdate,
			"project" => $project
		);

		Rest::json($res);
	}
}
