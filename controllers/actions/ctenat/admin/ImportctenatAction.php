<?php 
 /**
  * Display the directory of back office
  * @param String $id Not mandatory : if specify, look for the person with this Id. 
  * Else will get the id of the person logged
  * @return type
  */
class ImportctenatAction extends CAction
{
	public function run( $tpl=null, $view=null ){
		$controller = $this->getController();
		$params = array() ;
		$page = "importctenat";
		if(Yii::app()->request->isAjaxRequest)
			echo $controller->renderPartial("/custom/ctenat/".$page,$params,true);
		
		
	}
}
