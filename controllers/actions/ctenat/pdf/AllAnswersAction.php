<?php
class AllAnswersAction extends CAction{
	public function run($slug = null, $admin=null, $id=null, $idElt=null, $cterId=null) {
		$controller=$this->getController();
		ini_set('max_execution_time',1000);

		$params["controller"] = $controller;
		$params["slug"] = $slug; 
		$params["admin"] = $admin; 
		$params["id"] = $id; 
		$params["idElt"] = $idElt;
		$params["cterId"] = $cterId;
		Ctenat::pdfElement($params);

	}
}