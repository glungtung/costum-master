<?php
class HelpAction extends CAction
{
    public function run()
    {
    	$controller = $this->getController();    	

    	$tpl = "costum.views.custom.sommom.help";

    	$params = [];
    	
    	echo $controller->renderPartial($tpl,$params,true);
    }
}