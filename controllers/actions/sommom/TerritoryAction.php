<?php
//http://127.0.0.1/ph/costum/co/dashboard/sk/laCompagnieDesTierslieux
class TerritoryAction extends CAction
{
    public function run($answerId=null)
    {
    	$controller = $this->getController();    	

    	$answers = null;
    	$tpl = "costum.views.custom.sommom.territory";    	
    	if ($answerId != null) {
    		$answers = PHDB::findByIds( Form::ANSWER_COLLECTION, array($answerId));
    	}

		$params = [
    		"answers" => $answers
    	];	
    	if(Yii::app()->request->isAjaxRequest)
            echo $controller->renderPartial($tpl,$params,true);              
        else {
    		$this->getController()->layout = "//layouts/empty";
    		$this->getController()->render($tpl,$params);
        }
    	
    }
}
