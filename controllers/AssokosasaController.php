<?php 
class AssokosasaController extends CommunecterController {


	protected function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
	}

	public function actions()
	{
		return array(
			'getcommunityaction'    => 'costum.controllers.actions.assokosasa.GetCommunityAction',
			'getactualityaction'    => 'costum.controllers.actions.assokosasa.GetActualityAction',
			'getacteuraction'=>'costum.controllers.actions.assokosasa.GetActeurAction',
			'getallacteuraction'=>'costum.controllers.actions.assokosasa.GetAllActeurAction',
			'getvideoaction'=>'costum.controllers.actions.assokosasa.GetVideoAction',
			'getimagesaction'=>'costum.controllers.actions.assokosasa.GetImageAction'
			
		);
	}
}
?>