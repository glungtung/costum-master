<?php 
class BlockcmsController extends CommunecterController {


	protected function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
	}

	public function actions()
	{
		return array(
			'getpoiaction'    => 'costum.controllers.actions.blockcms.GetPoiAction',
			'geteventsaction' => 'costum.controllers.actions.blockcms.GetEventsAction',
			'getcollection'    => 'costum.controllers.actions.blockcms.GetCollectionAction',
			'geteventaction'    => 'costum.controllers.actions.blockcms.GetEventAction',
			'getcmsstaticexistaction' => 'costum.controllers.actions.blockcms.GetCmsStaticExistAction',
			'getcmsaction'=>'costum.controllers.actions.blockcms.GetCmsAction',
			'getusersaction'=>'costum.controllers.actions.blockcms.GetUsersAction',
			'getlisttemplatestaticaction'=>'costum.controllers.actions.blockcms.GetListTemplateStaticAction',
			'getnamecreator' =>'costum.controllers.actions.blockcms.GetNameCreatorAction',
			'getvideo' =>'costum.controllers.actions.blockcms.GetVideoAction',
			'dragblock' =>'costum.controllers.actions.blockcms.DragBlockAction',
			'gettemplatebycategory' =>'costum.controllers.actions.blockcms.GetTemplateByCategoryAction',
			'getdocumentationaction' =>'costum.controllers.actions.blockcms.GetDocumentationAction',
			'getcmscocityaction'=>'costum.controllers.actions.blockcms.GetCmsCocityAction',
		);
	}
}
?>