<?php 
class MeteolamerController extends CommunecterController {


	protected function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
	}

	public function actions()
	{
		return array(
			'importdata' => 'costum.controllers.actions.meteolamer.ImportDataAction',
			'getdata' => 'costum.controllers.actions.meteolamer.GetDataAction',
			'importspots' => 'costum.controllers.actions.meteolamer.ImportSpotsAction',
			'getspots' => 'costum.controllers.actions.meteolamer.GetSpotsAction',
			'savevote' => 'costum.controllers.actions.meteolamer.SaveVoteAction',
		);
	}
}
?>