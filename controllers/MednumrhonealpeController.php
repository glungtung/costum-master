<?php
/**
 * CoController.php
 *
 * Cocontroller always works with the PH base 
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class MednumrhonealpeController extends CommunecterController {


    protected function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
  	}

  	public function actions()
	{
	    return array(
            'geteventaction'    => 'costum.controllers.actions.mednumrhonealpe.GetEventAction',
            'element'     => 'costum.controllers.actions.mednumrhonealpe.ElementAction',
            'updateblock'  		=> 'costum.controllers.actions.mednumrhonealpe.UpdateBlockAction'
	    );
	}
}
