<?php 

class FiliereController extends CommunecterController {

	protected function beforeAction($action) {
		return parent::beforeAction($action);
	}

	public function actionGetSourceKeys(){
		$element = PHDB::findOne($_POST["type"],  array('slug' => $_POST["slug"]));

		echo Rest::json($element);
	}

	public function actionGenerate()
	{
		// cms blocks
		$block = ["article.actualiteTimeLine", "textImg.imgLeftWithBtnRight", "annuaire.annuaireDropdown", "header.banner"];

		foreach ($block as $value) {

			$arrayInsert = array(
				"name" => "actualite",
    			"path" => "tpls.blockCms.".$value,
    			"page" => "welcome",
    			"type" => "blockCopy",
    			"parent" => [
        			$_POST['id'] => [
            			"type" => "organizations",
            			"name" => $_POST['map']['slug']
        			]
    			],
    			"haveTpl" => "false",
    			"creator" => $_POST['map']['creator']
			);

			Yii::app()->mongodb->selectCollection("cms")->insert($arrayInsert);
			
		}

		$formArray = array(
			"name" => "Formulaire membre",
    		"what" => "Membre",
		    "parent" => [
		        $_POST['id'] => [
		            "type" => "organizations",
		            "name" => $_POST['map']['name']
		        ]
		    ],
    		"creator" => $_POST['map']['creator'],
    		"subForms" => [ 
        		"FormActeur2012021_1031_1", 
		        "FormActeur2012021_1031_2", 
		        "FormActeur2012021_1032_3", 
		        "FormActeur2012021_1032_4"
		    ],
		    "description" => "",
		    "active" => "true",
		    "private" => "false",
		    "canReadOtherAnswers" => "true",
		    "startDate" => "",
		    "endDate" => "31/12/2025",
		    "anyOnewithLinkCanAnswer" => "true",
		    "oneAnswerPerPers" => "true",
		    "canModify" => "true",
		    "showAnswers" => "true"
		);
		Yii::app()->mongodb->selectCollection("forms")->insert($arrayInsert);

		$htmlConstruct = array("menuLeft" => [
                "buttonList" => [
                    "app" => [
                        "label" => false,
                        "icon" => true,
                        "spanTooltip" => false,
                        "buttonList" => [
                            "#observatory" => true,
                            "#filiere" => true,
                            "#live" => true,
                            "#agenda" => true
                        ]
                    ]
                ],
                "addClass" => "transparent align-middle"
            ],
            "header" => [
                "menuTop" => [
                    "left" => [
                        "buttonList" => [
                            "logo" => [
                                "width" => "60",
                                "height" => "50"
                            ],
                            "searchBar" => [
                                "construct" => "searchBar",
                                "dropdownResult" => false,
                                "class" => "pull-left margin-top-5 margin-left-15 hidden-xs"
                            ]
                        ]
                    ],
                    "right" => [
                        "buttonList" => [
                            "networkFloop" => true,
                            "notifications" => true,
                            "chat" => true,
                            "login" => true,
                            "userProfil" => [
                                "name" => true,
                                "img" => true
                            ],
                            "dropdown" => [
                                "buttonList" => [
                                    "admin" => true,
                                    "logout" => true
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        );

		// default typeObj
        $typeObj = array("organization"=>[
    		"dynFormCostum"=>[
    			"beforeBuild"=>[
    				"properties"=>[
    					"category"=> [
    						"placeholder" => "Identifiez-vous à un acteur",
                            "inputType" => "select",
                            "label" => "Catégorie d'acteur*",
                            "class" => "form-control",
                            "order" => "1",
                            "labelInformation" => "Catégorie d'acteur",
                            "options" => [
                                "entreprises" => "Entreprises",
                                "associations" => "Associations"
                            ]
    					]
    				]
    			]
    		]
        ]);

		// base config costum
		PHDB::update("organizations", 
						array('slug' => $_POST['map']["slug"]), 
						array('$set' => ["cocity"=>$_POST['cocity'], "ville"=>$_POST['ville'], "thematic"=>$_POST['thematic'], "costum"=>["slug"=>"filiereGenerique", "htmlConstruct"=>$htmlConstruct, "typeObj"=>$typeObj]]));

		echo Rest::json(array('url' => Yii::app()->baseUrl.'/costum/co/index/slug/'.$_POST['map']["slug"]));
	}

    public function actionUpdateCategory(){
		
		$costum = PHDB::findOne("organizations", array("slug" => $_POST["slug"]));

		$categories = [];

		if(isset($_POST["category"])){
			 $category = $_POST["category"];
		}else{
			$category = [];
		}

		$categories = PHDB::update("organizations", ['_id' => $costum["_id"]], ['$set' =>array("costum.typeObj.organization.dynFormCostum.beforeBuild.properties.category" => $category)]);

		$_POST["filters"]["#Welcome"] = [
	            "subdomainName" =>"Accueil"
	        ];

	    $_POST["filters"]["#observatory"] = [
	            "hash" =>"#app.view",
	            "subdomainName" =>"OBSERVATORY",
	            "urlExtra" =>"page/observatory",
	            "icon" =>"tachometer",
	            "img" =>"",
	            "urlExtra" =>"/page/classement/url/costum.views.custom.".$_POST["slug"].".observatory",
	            "useHeader" =>false,
	            "useFooter" =>false,
	            "useFilter" =>false,
	            "inMenu" =>false
	        ];

	        $_POST["filters"]["#search"] = [
	            "hash" =>"#app.search",
	            "subdomainName" =>"CARTO",
	            "urlExtra" =>"page/search",
	            "filterObj" =>"costum.views.custom.".$_POST["slug"].".filters",
	            "img" =>"",
	            "icon" =>"map-marker",
	            "showMap" =>true,
	            "placeholderMainSearch" =>"Map",
	            "filters" => [
	                "types" =>[ 
	                    "organizations", 
	                    "events"
	                ]
	            ]
	        ];
	        $_POST["filters"]["#filiere"] = [
	            "hash" =>"#app.search",
	            "icon" =>"search",
	            "filterObj" =>"costum.views.custom.".$_POST["slug"].".filters",
	            "urlExtra" =>"/page/filiere",
	            "img" =>"",
	            "subdomainName" =>"ANNUAIRE",
	            "placeholderMainSearch" =>"I search ?",
	            "useFilter" =>true,
	            "useFooter" =>true,
	            "filters" => [
	                "types" =>[ 
	                    "organizations"
	                ]
	            ],
	            "searchObject" => [
	                "indexStep" =>"0"
	            ]
	        ];
	        
	        $_POST["filters"]["#live"] = [
	            "subdomainName" =>"ACTUS",
	            "slug" =>"".$_POST["slug"]."",
	            "img" =>"",
	            "formCreate" =>false,
	            "useFilter" =>false,
	            "viewMode" =>"list"
	        ];
	        $_POST["filters"]["#agenda"] = [
	            "subdomainName" =>"AGENDA",
	            "img" =>"",
	            "nameMenuTop" =>"Agenda"
	        ];

		PHDB::update("organizations", ['_id' => $costum["_id"]], ['$set' => ["costum.app" => $_POST["filters"]]]);
		
		Rest::json($categories);
	}
}
?>