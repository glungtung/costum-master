<?php
/**
 * CoController.php
 *
 * Cocontroller always works with the PH base 
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class PacteController extends CommunecterController {


    protected function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
  	}

  	public function actions()
	{
	    return array(
	        'register'  => 'costum.controllers.actions.pacte.RegisterAction',
	        'checkexist'  => 'costum.controllers.actions.pacte.CheckExistAction',
	        'validategroup'  => 'costum.controllers.actions.pacte.ValidateGroupAction',
	        'importcontrat'  => 'costum.controllers.actions.pacte.ImportcontratAction',
	        'importcollectif'  => 'costum.controllers.actions.pacte.ImportcollectifAction',
	        'savecontrat'  	=> 'costum.controllers.actions.pacte.SaveContratAction',
	        'savecollectif'  	=> 'costum.controllers.actions.pacte.SaveCollectifAction',
	        'searchsignatures'=>'costum.controllers.actions.pacte.SearchSignaturesAction',
	        'mapsearch'=>'costum.controllers.actions.pacte.MapSearchAction',
	        "collectifsignedcsv"=>"costum.controllers.actions.pacte.CollectifSignedCsvAction",

	    );
	}

	/*public function actionIndex() 
	{
    	if(Yii::app()->request->isAjaxRequest)
	        echo $this->renderPartial("../default/index");
	    else
    		$this->render("index");
    	//$this->redirect(Yii::app()->createUrl( "/".Yii::app()->params["module"]["parent"] ));	
  	}*/
}
