<?php
/**
 * AlternatibaController.php
 *
 */
class AlternatibaController extends CommunecterController {

  protected function beforeAction($action)
  {
	  return parent::beforeAction($action);
  }

  public function actions() {
      return array(
      	'getpoiaction'     => 'costum.controllers.actions.alternatiba.GetPoiAction',
        'getcommunityaction'     => 'costum.controllers.actions.alternatiba.GetCommunityAction',
      	'geteventcommunity'	=>	'costum.controllers.actions.alternatiba.GetEventCommunityAction',
      );
  }

}