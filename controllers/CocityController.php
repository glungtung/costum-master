<?php 
class CocityController extends CommunecterController {


	protected function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
	}

	public function actions()
	{
		return array(
			'getcityaction'    => 'costum.controllers.actions.cocity.GetCityAction',
			'getlistcocityaction'    => 'costum.controllers.actions.cocity.GetListCocityAction',
			'getcmslistaction' => 'costum.controllers.actions.cocity.GetCmsListAction',
			'getfiliere' => 'costum.controllers.actions.cocity.GetFiliereAction',
			'getorgafiliere' => 'costum.controllers.actions.cocity.GetOrgaFiliereAction',
			'getcmscocityaction' => 'costum.controllers.actions.cocity.GetCmsCocityAction',
			'graph' =>'costum.controllers.actions.cocity.GraphAction',

			
		);
	}
	public function actionGenerate()
	{
		// cms blocks
		$block = ["header.bannerWithSearchGlobal", "menu.menu", "menu.linkWithbg","tags.actorWithTypeAndNumber","app.blocApp", "faq.questionLocal","article.oneActualite", "footer.footer"];
		foreach ($block as $key => $value) {
			$arrayInsert = array(
		    	"path" => "tpls.blockCms.".$value,
		   		"page" => "welcome",
		    	"type" => "blockCopy",
		    	"parent" => [
		        	$_POST['id'] => [
		            	"type" => $_POST['map']['costumType'],
		            	"name" => $_POST['map']['slug']
		        	]
		    	],
		    	"haveTpl" => "false",
		    	"position" => $key,
		    	"creator" => $_POST['map']['creator']
			);
			Yii::app()->mongodb->selectCollection("cms")->insert($arrayInsert);
		}		
    	$htmlConstruct = array(
    	        "footer"=>false,
    	        "appRendering" => "horizontal",
    	        "header" => [
    	            "menuTop" => [
    	                "left" => [
    	                    "class"=>"text-right col-sm-6 col-lg-7 navigator ",
    	                    "buttonList" => [
    	                        "xsMenu"=>[
    	                            "buttonList"=>[                               
    	                                "app"=>[
    	                                    "label"=>true,
    	                                    "icon"=>true,
    	                                    "spanTooltip"=>true,
    	                                    "keyListButton"=>"pages",
    	                                    "buttonList"=>[

    	                                    ]
    	                                ]
    	                            ]
    	                        ],
    	                        "logo" => [
    	                            "assets" => "costum",
    	                            "url" => "/images/cocity/kosasa-logo-rvb-couleur_orig.png",
    	                            "urlMin"=>"/images/cocity/logo-point.png",
    	                            "heightMin"=>40,
    	                            "widthMin"=>45
    	                        ],
    	                        "app" => [
    	                            "label" => true,
    	                            "icon" => false,
    	                            "labelClass" =>"",
    	                            "class"=> "pull-right hidden-xs padding-5",
    	                            "tooltips" => true,
    	                            "buttonList"=>[

    	                            ]
    	                        ]
    	                    ]
    	                ],
    	                "right" => [
    	                    "addClass" => "",
    	                    "buttonList" => [
    	                        "login" => false,
    	                        "languages" => [
    	                            "connected" => false
    	                        ],
    	                        "home" => true,
    	                        "chat" => true,
    	                        "notifications" => true,
    	                        "userProfil" => [
    	                            "img" => true,
    	                            "name" => true,
    	                            "dashboard" => true
    	                        ]
    	                    ]
    	                ]
    	            ]
    	        ],

    	        "menuLeft"=>[
    	            "addClass"=>"align-middle",
    	            "buttonList"=>[
    	                "app"=>[
                            "label" => false,
                            "icon" => true,
                            "spanTooltip" => true,
                            "labelClass" => "padding-left-10",
    	                    "buttonList"=>[
    	                        "#search"=> true,
    	                        "#live"=>true,
    	                        "#agenda"=> true,
    	                        "#annonces"=>true,
    	                        "#dda"=>true
    	                    ]
    	                ],
    	                "add"=>true
    	            ]
    	        ],
    	        "element" => [
    	            "tplCss"=>[
    	                "v1.0_pageProfil.css"
    	            ],
    	            "banner"=>"co2.views.element.bannerHorizontal",
    	            "containerClass"=>[
    	                "centralSection"=>"col-xs-12 col-lg-10 col-lg-offset-1"
    	            ],
    	            "menuTop"=>[
    	                "class"=>"col-xs-12",
    	                "color"=> "#fff",
    	                "left"=>[
    	                    "class"=>"col-lg-3 col-md-3 col-sm-4 hidden-xs no-padding",
    	                    "buttonList"=>[
    	                        "imgUploader"=>true
    	                    ]   
    	                ],      
    	                "right"=>[
    	                    "class"=> "col-md-12 col-sm-12 col-lg-12 col-xs-12 text-center",
    	                    "buttonList"=>[
    	                        "xsMenu"=>[
    	                            "class"=>"visible-xs",
    	                            "buttonList"=>[
    	                                "detail"=>true,
    	                                "newspaper"=>true
    	                            ]
    	                        ],
    	                        "imgProfil"=>true,
    	                        "nameProfil"=>true,
    	                        "newspaper"=>true,
    	                        "detail"=>true,
    	                        "gallery"=>true,
    	                        "community"=>true,
    	                        "events"=>true,
    	                        "cospace"=>true,
    	                        "create"=>[
    	                            "class"=>"text-green-k visible-xs"
    	                        ],
    	                        "chat"=>[
    	                            "class"=>"text-red visible-xs"
    	                        ],
    	                        "share"=>true,
    	                        "params"=>[
    	                            "buttonList"=>[
    	                                "settings"=>true
    	                            ]
    	                        ]
    	                    ]   
    	                ]
    	            ]
    	        ],
    	        "directoryViewMode" => "block",
    	        "directory" => [
    	            "header" => [
    	                "map" => false,
    	                "viewMode" => false,
    	                "add" => [
    	                    "proposals" => true
    	                ]
    	            ]
    	        ],
    	        "redirect" => [
    	            "logged" => "welcome",
    	            "unlogged" => "welcome"
    	        ]
        );

    	$app = array(
            "#home" => [
                "hash" => "#app.view",
                "icon" => "",
                "urlExtra" => "/page/home/url/costum.views.custom.cocity.home",
                "useHeader" => true,
                "useFooter" => false,
                "useFilter" => false,
                "inMenu" => true,
                "subdomainName" => "Accueil"
            ],

            "#annonce" => [
                "hash" => "#app.search",
                "icon" => "",
                "urlExtra" => "/page/annonce",
                "subdomainName" => "Annonce",
                "placeholderMainSearch" => "what are you looking for ?",
                "inMenu"=>true,
                "useHeader" => false,
                "useFilter" => false,
                "useFooter" => true,
                "filters" => [
                    "types" => [
                        "classifieds"
                    ]
                ],
                "searchObject" => [
                    "indexStep" => "0"
                ]
            ],
            "#live" => [
                "icon" => "",
                "subdomainName" => "Actualité",
                "slug" => "etangsale",
                "formCreate" => false,
                "useFilter" => false,
                "inMenu" => true,
                "viewMode" => "list"
            ],
            "#article" => [
                "hash" => "#app.search",
                "icon" => "",
                "urlExtra" => "/page/article",
                "useHeader" => true,
                "useFilter" => false,
                "inMenu" => true,
                "open" => true,
                "subdomainName" => "Articles",
                "filters" => [
                    "types" => [ 
                        "poi"
                    ],
                    "category" => "article"
                ],
                "placeholderMainSearch" => "Quel article recherchez-vous ?"
            ],
            "#agenda" => [
                "icon" => "",
                "subdomainName" => "Évènement",
                "useFilter" => false,
                "inMenu" => true
            ],
            "#project" => [
                "hash" => "#app.search",
                "icon" => "",
                "urlExtra" => "/page/project",
                "subdomainName" => "Tous les projets",
                "placeholderMainSearch" => "what are you looking for ?",
                "inMenu"=>false,
                "useHeader" => false,
                "useFilter" => true,
                "useFooter" => true,
                "filters" => [
                    "types" => [
                        "projects"
                    ]
                ],
                "searchObject" => [
                    "indexStep" => "0"
                ]
            ],
            "#organization" => [
                "hash" => "#app.search",
                "icon" => "",
                "urlExtra" => "/page/organization",
                "subdomainName" => "Tous les organizations",
                "placeholderMainSearch" => "what are you looking for ?",
                "inMenu"=>false,
                "useHeader" => false,
                "useFilter" => true,
                "useFooter" => true,
                "filters" => [
                    "types" => [
                        "organizations"
                    ]
                ],
                "searchObject" => [
                    "indexStep" => "0"
                ]
            ],
            "#agriculture" => [
                "hash" => "#app.view",
                "icon" => "",
                "urlExtra" => "/page/agriculture/url/costum.views.custom.etangsale.pageStatic",
                "useHeader" => true,
                "useFooter" => true,
                "useFilter" => false,
                "useMap" => true,
                "inMenu" => true,
                "subdomainName" => "One",
                "bannerImg" => "/images/smarterre/construction/action_perso.jpeg",
                "css" => [
                    "color1" => "silver",
                    "color2" => "green",
                    "color3" => "black"
                ],
                "map" => [
                    "params" => [
                        "zoom" => 10,
                        "lat" => 10.32,
                        "long" => -12.32
                    ],
                    "filters" => [
                        "category"=>"cteR",
                        "types" => [
                            "organizations"
                        ]
                    ]
                ]
            ],
            "#search"=> [
                "results" => [
                    "smartGrid" => "",
                    "renderView" => "directory.elementPanelHtml"
                ]
            ],
            "#live"=>true,
            "#agenda"=> true,
            "#annonces"=>true,
            "#dda"=>true
        	
    	);
    	$typeObj = array(
            "organizations" => [
                    "add" => true
                ],
                "citoyens" => [
                    "add" => true
                ],
                "poi" => [
                    "add" => true,
                    "name" => "Actus",
                    "dynFormCostum" => [
                        "beforeBuild" => [
                            "properties" => [
                                "name" => [
                                    "label" => "Titre de l'actualité",
                                    "placeholder" => "Entrez le titre de votre Actualité..."
                                ],
                                "parent" => [
                                    "label" => "Auteur(s)"
                                ],
                                "shortDescription" => [
                                    "inputType" => "textarea",
                                    "label" => "Court résumé de l'actualité ",
                                    "placeholder" => "Entrez un résumé de votre actualité (250 caractères max.)...",
                                    "rules" => [
                                        "maxlength" => 250
                                    ],
                                    "order" => 5
                                ],
                                "description" => [
                                    "markdown" => true,
                                    "label" => "Texte à ajouter popur plus d'informations",
                                    "order" => 7
                                ],
                                "image" => [
                                    "label" => "Image(s)",
                                    "order" => 8
                                ],
                                "urls" => [
                                    "label" => "Lien(s) => vidéo, page internet ou document...",
                                    "order" => 9
                                ]
                            ]
                        ],
                        "onload" => [
                            "actions" => [
                                "setTitle" => "Ecrire un article",
                                "html" => [
                                    "infocustom" => "Formulaire de création d'un article"
                                ],
                                "presetValue" => [
                                    "type" => "article"
                                ],
                                "hide" => [
                                    "typeselect" => 1,
                                    "breadcrumbcustom" => 1,
                                    "locationlocation" => 1
                                ]
                            ]
                        ]
                    ]
                ] 
        	
    	);
        $filiere = [
            "food"=> array(
                "name" => "Food",
                "icon" => "fa-cutlery",
                "tags" => [ Yii::t("tags","agriculture"),Yii::t("tags","food"),Yii::t("tags","nutrition"),Yii::t("tags","AMAP") ]
            ),  
            "health"=> array(
                "name" => "Health",
                "icon" => "fa-heart-o",
                "tags" => [ Yii::t("tags","health") ]
            ),  
            "waste"=> array(
                "name" => "Waste",
                "icon" => "fa-trash-o ",
                "tags" => [ Yii::t("tags","waste") ]
            ),  
            "transport"=> array(
                "name" => "Transport",
                "icon" => "fa-bus",
                "tags" => [ Yii::t("tags","urbanism"),Yii::t("tags","transport"),Yii::t("tags","construction") ]
            ),  
            "education"=> array(
                "name" => "Education",
                "icon" => "fa-book",
                "tags" => [ Yii::t("tags","education"),Yii::t("tags","childhood") ]
            ),  
            "citizen"=> array(
                "name" => "Citizenship",
                "icon" => "fa-user-circle-o",
                "tags" => [ Yii::t("tags","citizen"),Yii::t("tags","society") ]
            ),  
            "economy"=> array(
                "name" => "Economy",
                "icon" => "fa-money",
                "tags" => [ Yii::t("tags","ess") ,Yii::t("tags","social solidarity economy") ]
            ),  
            "energy"=> array(
                "name" => "Energy",
                "icon" => "fa-sun-o",
                "tags" => [ Yii::t("tags","energy"),Yii::t("tags","climat") ]
            ),  
            "culture"=> array(
            "name" => "Culture",
            "icon" => "fa-universal-access",
            "tags" => [ Yii::t("tags","culture"),Yii::t("tags","animation") ]
            ),  
            "environnement"=> array(
            "name" => "Environnement",
            "icon" => "fa-tree",
            "tags" => [ Yii::t("tags","environment"),Yii::t("tags","biodiversity"),Yii::t("tags","ecology") ]
            ),  
            "numeric"=> array(
            "name" => "Numeric",
            "icon" => "fa-laptop",
            "tags" => [ Yii::t("tags","computer"),Yii::t("tags","ict"),Yii::t("tags","internet"),Yii::t("tags","web") ]
            ),
            "sport" => array( 
            "name" => "Sport",
            "icon" => "fa-futbol-o",
            "tags" => [ Yii::t("tags","sport") ]
            )
        ];

	    PHDB::update($_POST['map']["costumType"], 
    		array('slug' => $_POST['map']["slug"]), 
    		array('$set' => ["filiere"=>$filiere, "costum"=>["slug"=>"cocity", "htmlConstruct"=>$htmlConstruct,"app"=>$app, "typeObj"=>$typeObj]]));

    		echo Rest::json(array('url' => Yii::app()->baseUrl.'/costum/co/index/slug/'.$_POST['map']["slug"] ));
	    }
    }
?>
