<?php
/**
 * CoController.php
 *
 * Cocontroller always works with the PH base 
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class CmcashautebretagneController extends CommunecterController {


    protected function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
  	}

  	public function actions(){
	    return array(
	        'getevents'  		=> 'costum.controllers.actions.cmcashautebretagne.GetEventAction',
	        'getarticles'		=> 'costum.controllers.actions.cmcashautebretagne.GetArticleAction',
	        'element'			=> 'costum.controllers.actions.cmcashautebretagne.ElementAction',
	        'updateblock'  		=> 'costum.controllers.actions.cmcashautebretagne.UpdateBlockAction'
	    );
	}
	//public function actionPersondah
	/*public function actionIndex() 
	{
    	if(Yii::app()->request->isAjaxRequest)
	        echo $this->renderPartial("../default/index");
	    else
    		$this->render("index");
    	//$this->redirect(Yii::app()->createUrl( "/".Yii::app()->params["module"]["parent"] ));	
  	}*/
}
