<?php

class CommentController extends CommunecterController {

    protected function beforeAction($action) {
		return parent::beforeAction($action);
	}

  	public function actionIndex(){
		//$comments = PHDB::find("comments");
		$comments = PHDB::findAndSort("comments", ["parent"=>$_GET["comment"]], ["date"=> -1], 8);
        Rest::json($comments);
	}
	
    public function actionSave(){
		$_POST["date"] = date("d / m / Y - H:i");
		$comment = Yii::app()->mongodb->selectCollection("comments")->insert( $_POST);
		Rest::json($comment);
	}

    public function actionReply(){
		$_POST["reply"]["date"] = date("d / m / Y - H:i");
		$comment = PHDB::findOneById("comments", $_POST["comment"]);
		$reply = PHDB::update("comments", ['_id' => $comment["_id"]], ['$push'=>["replies" => $_POST["reply"]]]);
		Rest::json($reply);
	}
	//public function actionPersondah
	/*public function actionIndex() 
	{
    	if(Yii::app()->request->isAjaxRequest)
	        echo $this->renderPartial("../default/index");
	    else
    		$this->render("index");
    	//$this->redirect(Yii::app()->createUrl( "/".Yii::app()->params["module"]["parent"] ));	
  	}*/
}
