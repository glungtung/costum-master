<?php
/**
 * CoController.php
 *
 * Cocontroller always works with the PH base 
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class LapossessionController extends CommunecterController {


    protected function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
  	}

  	public function actions(){
	    return array(
	    	'getcommunityaction'	=> 	'costum.controllers.actions.lapossession.GetCommunityAction',
	    	'getorganization'		=>	'costum.controllers.actions.lapossession.GetOrgaAction',
	        'element'				=> 	'costum.controllers.actions.lapossession.ElementAction',
	        'updateblock'  			=> 	'costum.controllers.actions.lapossession.UpdateBlockAction'
	    );
	}
}
