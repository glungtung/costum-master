<?php
class CostumgeneriqueController extends CommunecterController {


    protected function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
  	}

  	public function actions(){
	    return array(
	        'getprojects'  		=> 'costum.controllers.actions.costumgenerique.GetProjectsAction',
	        'getarticles'  		=> 'costum.controllers.actions.costumgenerique.GetArticlesAction',
	        'getpoi'  		=> 'costum.controllers.actions.costumgenerique.GetPoiAction',
	 		'getarticlescommunity' => 'costum.controllers.actions.costumgenerique.GetArticlesCommunityAction',
	 		'getarticlescarousel' => 'costum.controllers.actions.costumgenerique.GetArticlesCarouselAction',
	        'geteventdesc'		=>	'costum.controllers.actions.costumgenerique.GetEventDescAction',
	        'getcommunity'		=>	'costum.controllers.actions.costumgenerique.GetCommunityAction',
	        'geteventslide'		=>	'costum.controllers.actions.costumgenerique.GetEventSlideAction',
	        'geteventaction'	=>  'costum.controllers.actions.costumgenerique.GetEventAction',
	        'geteventcommunity'	=>	'costum.controllers.actions.costumgenerique.GetEventCommunityAction',
	        'getressources'		=> 'costum.controllers.actions.costumgenerique.GetRessourcesAction',
	        'getdda'			=>	'costum.controllers.actions.costumgenerique.GetDdaAction'
	    );
	}
	//public function actionPersondah
	/*public function actionIndex() 
	{
    	if(Yii::app()->request->isAjaxRequest)
	        echo $this->renderPartial("../default/index");
	    else
    		$this->render("index");
    	//$this->redirect(Yii::app()->createUrl( "/".Yii::app()->params["module"]["parent"] ));	
  	}*/
}
