<?php
/**
 * CoController.php
 *
 * Cocontroller always works with the PH base 
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class CtenatController extends CommunecterController {


    protected function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
  	}

  	public function actions(){
	    return array(
	        'elementhome'  		=> 'costum.controllers.actions.ctenat.element.HomeAction',
	        'answersadmin'  	=> 'costum.controllers.actions.ctenat.admin.AnswersAdminAction',
	        'prio'  			=> 'costum.controllers.actions.ctenat.admin.PrioAction',
	        'updatestatuscter'	=> 'costum.controllers.actions.ctenat.admin.UpdateStatusCterAction',
	        'importctenat'  	=> 'costum.controllers.actions.ctenat.admin.ImportctenatAction',
	        'importindicateur'  => 'costum.controllers.actions.ctenat.admin.ImportindicateurAction',
	        'importorga'  		=> 'costum.controllers.actions.ctenat.admin.ImportorgaAction',
	        'saveorga'  		=> 'costum.controllers.actions.ctenat.admin.SaveOrgaAction',
	        'previewdata'  		=> 'costum.controllers.actions.ctenat.admin.PreviewDataAction',
	        'saveindicateur'  	=> 'costum.controllers.actions.ctenat.admin.SaveIndicateurAction',
	        'dashboard'  		=> 'costum.controllers.actions.ctenat.DashboardAction',
	    	"dashboardaccount" 	=> 'costum.controllers.actions.ctenat.element.DashboardAccountAction',
	    	"validata" 			=> 'costum.controllers.actions.ctenat.script.ValidataAction',
	    	"cter" 				=> 'costum.controllers.actions.ctenat.api.CterAction',
	    	"answerscsv" 		=> 'costum.controllers.actions.ctenat.api.AnswerscsvAction',
	    	"answersctercsv"	=> 'costum.controllers.actions.ctenat.api.AnswersctercsvAction',
	    	"generatecopil" 	=> 'costum.controllers.actions.ctenat.pdf.GenerateCopilAction',
	    	"generateorientation" 	=> 'costum.controllers.actions.ctenat.pdf.GenerateOrientationAction',
	    	"allanswers" 		=> 'costum.controllers.actions.ctenat.pdf.AllAnswersAction',
	    	"slide" 			=> 'costum.controllers.actions.ctenat.SlideAction',
	    	"notifinancer" 		=> 'costum.controllers.actions.ctenat.NotifinancerAction',
	    	"convertanswers" 	=> 'costum.controllers.actions.ctenat.script.AnswersAction',
	    	"updateorientationscandidatures" => "costum.controllers.actions.ctenat.admin.UpdateProjectOrientationAction",
	    	"getindicateur" 	=> 'costum.controllers.actions.ctenat.GetindicateurAction',
	    	"selectindicateur" 	=> 'costum.controllers.actions.ctenat.SelectindicateurAction',
	    	
	    );
	}
	//public function actionPersondah
	/*public function actionIndex() 
	{
    	if(Yii::app()->request->isAjaxRequest)
	        echo $this->renderPartial("../default/index");
	    else
    		$this->render("index");
    	//$this->redirect(Yii::app()->createUrl( "/".Yii::app()->params["module"]["parent"] ));	
  	}*/
}
